<?php
  function devolver_datos_esposa_apoderado($cli_id,$cli_bienes, $cli_firma, $tip_soc){
    require_once ("../cliente/Cliente.class.php");
    $oCliente = new Cliente();
    require_once ("../ubigeo/Ubigeo.class.php");
    $oUbigeo = new Ubigeo();
    require_once ("../zona/Zona.class.php");
    $oZona = new Zona();

    $esposa = ''; $sociedad_resultado = 0;
    $dts = $oCliente->mostrar_cliente_sociedad($cli_id, $tip_soc);
      if($dts['estado'] == 1){
        $sociedad_resultado = 1;
        $cli_doc = $dts['data']['tb_cliente_doc'];
        $cli_nom=$dts['data']['tb_cliente_nom'];
        $cli_dir=$dts['data']['tb_cliente_dir'];
        $cli_ema=$dts['data']['tb_cliente_ema'];
        $cli_fecnac=$dts['data']['tb_cliente_fecnac'];
        $cli_tel=$dts['data']['tb_cliente_tel'];
        $cli_cel=$dts['data']['tb_cliente_cel'];
        $cli_telref=$dts['data']['tb_cliente_telref'];
        $cli_ubigeo=$dts['data']['tb_ubigeo_cod'];
        $cli_zon_id = $dts['data']['tb_zonaregistral_id'];
        $cli_numpar = $dts['data']['tb_cliente_numpar']; //numero de partida del cliente
        $cli_estciv = $dts['data']['tb_cliente_estciv']; // 1 soltero, 2 casado
      }
    $dts = null;

    $dts=$oUbigeo->mostrarUbigeo($cli_ubigeo);
      if($dts['estado'] == 1){
        $ubi_dep = $dts['data']['Departamento'];
        $ubi_pro = $dts['data']['Provincia'];
        $ubi_dis = $dts['data']['Distrito'];
      }
    $dts=null;

    $esposa = 'Y SU CONYUGE <b>'.$cli_nom.'</b>, IDENTIFICADO CON DNI N° <b>'.$cli_doc.'</b> Y CON DOMICILIO EN <b>'.$cli_dir.'</b>, DEL DISTRITO DE <b>'.$ubi_dis.'</b>, PROVINCIA DE <b>'.$ubi_pro.'</b> Y DEPARTAMENTO DE <b>'.$ubi_dep.'</b>';
    $apoderado = 'DIBIDAMENTE REPRESENTADO(OS) POR SU APODERADO(A) <b>'.$cli_nom.'</b>, IDENTIFICADO CON DNI N° <b>'.$cli_doc.'</b> CON DOMICILIO EN <b>'.$cli_dir.'</b>, DEL DISTRITO DE <b>'.$ubi_dis.'</b>, PROVINCIA DE <b>'.$ubi_pro.'</b>, DEL DEPARTAMENTO DE <b>'.$ubi_dep.'</b>';

    //si el cliente quiere firmar solo pero sus bienes etán en conjunto
    if($cli_bienes == 2 && $cli_firma == 1){
      //información de la zona registral
      if(!empty($cli_zon_id)){
        $dts = $oZona->mostrarUno($cli_zon_id);
          if($dts['estado'] == 1){
          $zon_nom = $dts['data']['tb_zona_nom'];
          }
          $dts=null;
      }
      
      $esposa = $esposa.' DEBIDAMENTE REPRESENTADA POR SU CONYUGE MENCIONADO ANTERIORMENTE, CON PODERES INSCRITOS EN LA PARTIDA REGISTRAL N° <b>'.$cli_numpar.'</b> DEL REGISTRO DE PERSONAS JURÍDICAS DE LA '.$zon_nom;
    }

    if($sociedad_resultado > 0){
      if($tip_soc == 1)
        return $esposa;
      else
        return $apoderado;
    }
    else
      return '';
  }
  
  function devolver_datos_copropietarios($cli_id){
    require_once ("../cliente/Cliente.class.php");
    $oCliente = new Cliente();
    require_once ("../ubigeo/Ubigeo.class.php");
    $oUbigeo = new Ubigeo();

    $copro = ''; $cc = 0;
    $dts = $oCliente->mostrar_cliente_sociedad($cli_id, 2); //parametro 2 por ser copropietarios
      if($dts['estado'] == 1){ 
        $num_rows = count($dts['data']);
        foreach ($dts['data'] as $key => $dt) {  
        $cli_id_copro = $dts['estado']['tb_cliente_id'];
        $cli_doc = $dt['tb_cliente_doc'];
        $cli_nom=$dt['tb_cliente_nom'];
        $cli_dir=$dt['tb_cliente_dir'];
        $cli_ema=$dt['tb_cliente_ema'];
        $cli_fecnac=$dt['tb_cliente_fecnac'];
        $cli_tel=$dt['tb_cliente_tel'];
        $cli_cel=$dt['tb_cliente_cel'];
        $cli_telref=$dt['tb_cliente_telref'];
        $cli_ubigeo=$dt['tb_ubigeo_cod'];
        $cli_estciv = $dt['tb_cliente_estciv']; // 1 soltero, 2 casado
        $cli_bien = $dt['tb_cliente_bien']; // 1 separacion de bienes, 2 sin separacion
        $cli_firm = $dt['tb_cliente_firm']; // 1 firma solo, 2 firma con conyuge
        $cli_zon_id = $dt['tb_zonaregistral_id']; // id de la zona registral donde está el N° de partida
        $cli_numpar = $dt['tb_cliente_numpar']; //numero de partida del cliente

        $dts1 = $oUbigeo->mostrarUbigeo($cli_ubigeo);
          if($dts1['estado'] == 1){
            $ubi_dep = $dts1['data']['Departamento'];
            $ubi_pro = $dts1['data']['Provincia'];
            $ubi_dis = $dts1['data']['Distrito'];
          }
        $dts1=null;

        $cc ++;
        $conector = ';';
        if($cc == $num_rows)
          $conector = ' y';

        //obtenemos información d<b>EL CLIENTE</b>, sus nombres, su esposa(o), copropietario o apoderado
        $estado_civil = 'SOLTERO(A)';
        if($cli_estciv == 2) $estado_civil = 'CASADO(A)';

        $copro .= $conector.' <b>'.$cli_nom.'</b>, IDENTIFICADO CON DNI N° <b>'.$cli_doc.'</b>, ESTADO CIVIL '.$estado_civil.' CON DOMICILIO EN <b>'.$cli_dir.'</b>, DEL DISTRITO DE <b>'.$ubi_dis.'</b>, PROVINCIA DE <b>'.$ubi_pro.'</b>, DEL DEPARTAMENTO DE <b>'.$ubi_dep.'</b>';

        //si el copropietario es casado
        if($cli_estciv == 2){
          $esposa = devolver_datos_esposa_apoderado($cli_id_copro, $cli_bien, $cli_firm, 1); // 1 conyugal
          $copro = $copro.' '.$esposa;
        }

        $apoderado = devolver_datos_esposa_apoderado($cli_id_copro, $cli_bien, $cli_firm, 3); // 3 apoderado
        
        if($apoderado != '')
          $copro = $copro.' '.$apoderado;
      }
  }
    $dts=null;

    return $copro;
  }

  function listar_options_rango_estado_garantia($value){
    $list_opion = '';
    for ($i = 1; $i <= 10; $i++){
      if($i == $value)
        $list_opion .= '<option value="'.$i.'" selected>'.$i.' de 10</option>';
      else
        $list_opion .= '<option value="'.$i.'">'.$i.' de 10</option>';
    }

    return $list_opion;
  }

  function planilla_option_meses($mes_actual){
    $meses = array(
      'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
      'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    );
    $numeros_mes = array(
      '01', '02', '03', '04', '05', '06',
      '07', '08', '09', '10', '11', '12'
    );

    for ($i = 0; $i < 12; $i++) {
      $selected = ($numeros_mes[$i] == $mes_actual) ? 'selected' : '';
      echo '<option value="' . $numeros_mes[$i] . '" ' . $selected . '>' . $meses[$i] . '</option>';
    }
  }
  
  function planilla_option_anios($anio_actual){
    $anio_inicio = 2016;
    $anio_fin = $anio_actual + 5;

    for ($i = $anio_inicio; $i < $anio_fin; $i++) {
        $selected = ($anio_actual == $i) ? 'selected="true"' : '';
        echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
    }
  }
  /*
  SELECT cl.tb_cliente_id, cre.tb_credito_id, cuo.tb_cuota_id, cud.tb_cuotadetalle_id,cud.tb_cuotadetalle_fec, cud.tb_cuotadetalle_cuo,cud.tb_cuotadetalle_est FROM `tb_cliente` cl inner join tb_creditoasiveh asi on cre.tb_cliente_id = cl.tb_cliente_id inner join tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id inner join tb_cuotadetalle cud on cud.tb_cuota_id = cuo.tb_cuota_id where tb_credito_est NOT IN(1,2,4,7,8) and tb_credito_xac = 1 and cuo.tb_creditotipo_id = 2 and cud.tb_cuotadetalle_fec <= NOW() and cud.tb_cuotadetalle_est != 2 and cl.tb_cliente_id in (125,128)

  SELECT cud.tb_cuotadetalle_id, ing.tb_ingreso_id, cp.tb_cuotapago_id, SUM(ing.tb_ingreso_imp) as importe FROM tb_cuotadetalle cud LEFT join tb_cuotapago cp on cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id LEFT join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id where cp.tb_modulo_id = 2 and ing.tb_ingreso_xac =1 and tb_cuotapago_xac=1 and cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id GROUP by cud.tb_cuotadetalle_id order by 4 ASC

  SELECT cl.tb_cliente_id, cre.tb_credito_id, cuo.tb_cuota_id, cud.tb_cuotadetalle_id,cud.tb_cuotadetalle_fec, cud.tb_cuotadetalle_cuo,cud.tb_cuotadetalle_est, IFNULL((SELECT SUM(ing.tb_ingreso_imp) FROM tb_cuotapago cp inner join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id where cp.tb_modulo_id = 2 and ing.tb_ingreso_xac =1 and tb_cuotapago_xac=1 and cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id GROUP by cp.tb_cuotapago_modid), 0) AS importe FROM `tb_cliente` cl inner join tb_creditoasiveh asi on cre.tb_cliente_id = cl.tb_cliente_id inner join tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id inner join tb_cuotadetalle cud on cud.tb_cuota_id = cuo.tb_cuota_id where tb_credito_est NOT IN(1,2,4,7,8) and tb_credito_xac = 1 and cuo.tb_creditotipo_id = 2 and cud.tb_cuotadetalle_fec <= NOW() and cud.tb_cuotadetalle_est != 2 and cl.tb_cliente_id in (125,128)

  //mas adecuada
  SELECT cl.tb_cliente_id,cl.tb_cliente_nom, COUNT(cud.tb_cuotadetalle_id) AS num_cuotasdetalle,SUM(cud.tb_cuotadetalle_cuo) AS mon_total_cuota, SUM(IFNULL((SELECT SUM(ing.tb_ingreso_imp) FROM tb_cuotapago cp inner join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id where cp.tb_modulo_id = 2 and ing.tb_ingreso_xac =1 and tb_cuotapago_xac=1 and cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id GROUP by cp.tb_cuotapago_modid), 0)) AS pagos_parciales FROM `tb_cliente` cl inner join tb_creditoasiveh cre on cre.tb_cliente_id = cl.tb_cliente_id inner join tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id inner join tb_cuotadetalle cud on cud.tb_cuota_id = cuo.tb_cuota_id where tb_credito_est NOT IN(1,2,4,7,8) and tb_credito_xac = 1 and cuo.tb_creditotipo_id = 2 and cud.tb_cuotadetalle_fec <= NOW() and cud.tb_cuotadetalle_est != 2 and cud.tb_cuotadetalle_estap = 0 and cre.tb_moneda_id = 1 GROUP BY 1

  SELECT cl.tb_cliente_id,cl.tb_cliente_nom, COUNT(cud.tb_cuotadetalle_id) AS num_cuotasdetalle,SUM(cud.tb_cuotadetalle_cuo) AS mon_total_cuota, SUM(IFNULL((SELECT SUM(ing.tb_ingreso_imp) FROM tb_cuotapago cp inner join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id where cp.tb_modulo_id = 2 and ing.tb_ingreso_xac =1 and tb_cuotapago_xac=1 and cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id GROUP by cp.tb_cuotapago_modid), 0)) AS pagos_parciales FROM `tb_cliente` cl inner join tb_creditoasiveh cre on cre.tb_cliente_id = cl.tb_cliente_id inner join tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id inner join tb_cuotadetalle cud on cud.tb_cuota_id = cuo.tb_cuota_id where tb_credito_est NOT IN(1,2,4,7,8) and tb_credito_xac = 1 and cuo.tb_creditotipo_id = 2 and cud.tb_cuotadetalle_fec <= NOW() and cud.tb_cuotadetalle_est != 2 and cud.tb_cuotadetalle_estap = 0 and cre.tb_moneda_id = 2 and tb_credito_tip1 in(3) GROUP BY 1 
  }*/
?>