<?php
DEFINE('HORA_INICIO', '08:00:00');
DEFINE('HORA_FIN', '12:59:00');
$apertura = new DateTime(HORA_INICIO);
$cierre = new DateTime(HORA_FIN);

$tiempo = $apertura->diff($cierre);

echo $tiempo->format('%H:%I');