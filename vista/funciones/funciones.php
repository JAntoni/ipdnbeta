<?php

function getBrowser() {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    if (strpos($user_agent, 'MSIE') !== FALSE)
        return 'Internet explorer';
    elseif (strpos($user_agent, 'Edge') !== FALSE) //Microsoft Edge
        return 'Microsoft Edge';
    elseif (strpos($user_agent, 'Trident') !== FALSE) //IE 11
        return 'Internet explorer';
    elseif (strpos($user_agent, 'Opera Mini') !== FALSE)
        return "Opera Mini";
    elseif (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
        return "Opera";
    elseif (strpos($user_agent, 'Firefox') !== FALSE)
        return 'Mozilla Firefox';
    elseif (strpos($user_agent, 'Chrome') !== FALSE)
        return 'Google Chrome';
    elseif (strpos($user_agent, 'Safari') !== FALSE)
        return "Safari";
    else
        return 'No hemos podido detectar su navegador';
}

function getLocation() {
    $ip = $_SERVER['REMOTE_ADDR'];
    /* $informacionSolicitud = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);

      // Convertir el texto JSON en un array
      $data = json_decode($informacionSolicitud, true);

      $pais = $data['geoplugin_countryName'];
      $latitud = $data['geoplugin_latitude'];
      $longitud = $data['geoplugin_longitude'];
      $ciudad = $data['geoplugin_city'];

      $localizacion = $latitud.', '.$longitud.', '.$pais;

      if(!empty($ciudad))
      $localizacion .=', '.$ciudad;

      return $localizacion; */
    return $ip;
}

function devuelve_nombre_usuario_action($usuario_action) {
    $action = '';
    if ($usuario_action == 'L')
        $action = 'leer';
    elseif ($usuario_action == 'I')
        $action = 'insertar';
    elseif ($usuario_action == 'M')
        $action = 'modificar';
    elseif ($usuario_action == 'E')
        $action = 'eliminar';
    else
        $action = 'desconocido';
    return $action;
}

function base64ToImage($imageData) {
    //$data = 'data:image/png;base64,AAAFBfj42Pj4';

    list($type, $imageData) = explode(';', $imageData);
    list(, $extension) = explode('/', $type);
    list(, $imageData) = explode(',', $imageData);

    //$fileName = uniqid().'.'.$extension;
    $imageData = base64_decode($imageData);

    $data['extension'] = $extension;
    $data['image'] = $imageData;

    //file_put_contents($fileName, $imageData);
    return $data;
}

function formato_numero($valor) {
    if($valor == '' || $valor == 0)
        $dato = 0;
    else{
        $dato = str_replace(",","", $valor);
        $dato = number_format($dato, 2, '.', '');
    }
    return $dato;
}

function moneda_mysql($monto) {
    $valor = 0;
    if (!empty($monto)){
        $valor = floatval(str_replace(",", "", $monto));
    }
    $valor = number_format($valor, 2, '.', '');
    return $valor;
}

function mostrar_moneda($monto) {
    $valor = '0.00';
    if (!empty($monto))
        $valor = str_replace(",", "", $monto);
        $valor = number_format($valor, 2, '.', ',');
    return $valor;
}

function formato_moneda($monto){
    if($monto == '' || $monto == 0)
        $dato = '0.00';
    else{
        $dato = str_replace(",","", $monto);
        $dato = number_format($dato, 2, '.', '');
    }
    return $dato;
}

function devuelve_option_nombre_meses($valor_mes) {

    $array_meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    $array_numeros = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

    $option = '<option value="">Todos..</option>';
    for ($i = 0; $i < 12; $i++) {
        if ($array_numeros[$i] == $valor_mes)
            $option .= '<option value="' . $array_numeros[$i] . '" selected>' . $array_meses[$i] . '</option>';
        else
            $option .= '<option value="' . $array_numeros[$i] . '">' . $array_meses[$i] . '</option>';
    }
    return $option;
}

function nombre_mes($mes){
    switch ($mes){ 
		 case 1: 
			 $nombre_mes="Enero"; 
			 break; 
		 case 2: 
			 $nombre_mes="Febrero"; 
			 break; 
		 case 3: 
			 $nombre_mes="Marzo"; 
			 break; 
		 case 4: 
			 $nombre_mes="Abril"; 
			 break; 
		 case 5: 
			 $nombre_mes="Mayo"; 
			 break; 
		 case 6: 
			 $nombre_mes="Junio"; 
			 break; 
		 case 7: 
			 $nombre_mes="Julio"; 
			 break; 
		 case 8: 
			 $nombre_mes="Agosto"; 
			 break; 
		 case 9: 
			 $nombre_mes="Septiembre"; 
			 break; 
		 case 10: 
			 $nombre_mes="Octubre"; 
			 break; 
		 case 11: 
			 $nombre_mes="Noviembre"; 
			 break; 
		 case 12: 
			 $nombre_mes="Diciembre"; 
			 break; 
	} 
	return $nombre_mes; 
}

function devuelve_option_anios($valor_anio, $formato) {
    $anio_inicio = 2015;
    $anio_final = intval(date('Y')) + 5;
    $option = '<option value="">Todos..</option>';

    if (intval($formato) == 2) {
        for ($i = $anio_inicio; $i < $anio_final; $i++) {

            $value = substr($i, -2);

            if (intval($value) == intval($valor_anio))
                $option .= '<option value="' . $value . '" selected>' . $i . '</option>';
            else
                $option .= '<option value="' . $value . '">' . $i . '</option>';
        }
        return $option;
    }
    if (intval($formato) == 4) {
        for ($i = $anio_inicio; $i < $anio_final; $i++) {

            if ($i == intval($valor_anio))
                $option .= '<option value="' . $i . '" selected>' . $i . '</option>';
            else
                $option .= '<option value="' . $i . '">' . $i . '</option>';
        }
        return $option;
    }
}
function devuelve_option_anios_garveh($anio_inicio1,$valor_anio, $formato) {
    $anio_inicio = $anio_inicio1;
    $anio_final = intval(date('Y')) + 5;
    $option = '<option value="">Todos..</option>';

    if (intval($formato) == 2) {
        for ($i = $anio_inicio; $i < $anio_final; $i++) {

            $value = substr($i, -2);

            if (intval($value) == intval($valor_anio))
                $option .= '<option value="' . $value . '" selected>' . $i . '</option>';
            else
                $option .= '<option value="' . $value . '">' . $i . '</option>';
        }
        return $option;
    }
    if (intval($formato) == 4) {
        for ($i = $anio_inicio; $i < $anio_final; $i++) {

            if ($i == intval($valor_anio))
                $option .= '<option value="' . $i . '" selected>' . $i . '</option>';
            else
                $option .= '<option value="' . $i . '">' . $i . '</option>';
        }
        return $option;
    }
}

function retorna_sigla_credito_nombre($credito_nom) {
    $sigla = 'SinSIGLA';
    if (trim($credito_nom) == 'creditomenor')
        $sigla = 'CM';
    if (trim($credito_nom) == 'creditoasiveh')
        $sigla = 'ASIVEH';
    if (trim($credito_nom) == 'creditogarveh')
        $sigla = 'GARVEH';
    if (trim($credito_nom) == 'creditohipo')
        $sigla = 'HIPO';
    return $sigla;
}

function retorna_nombre_tabla_creditotipo($creditotipo_id) {
    $tabla = '';
    if (intval($creditotipo_id) == 1)
        $tabla = 'tb_creditomenor';
    if (intval($creditotipo_id) == 2)
        $tabla = 'tb_creditoasiveh';
    if (intval($creditotipo_id) == 3)
        $tabla = 'tb_creditogarveh';
    if (intval($creditotipo_id) == 4)
        $tabla = 'tb_creditohipo';
    return $tabla;
}

function numtoletras($xcifra, $mon_id = 1) {

    if ($mon_id == 1)
        $moneda = "SOLES";
    if ($mon_id == 2)
        $moneda = "DOLARES AMERICANOS";
    if ($mon_id == 0)
        $moneda = '';

    $xarray = array(0 => "Cero",
        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
    );
//
    $xcifra = trim($xcifra);
    $xlength = strlen($xcifra);
    $xpos_punto = strpos($xcifra, ".");
    $xaux_int = $xcifra;
    $xdecimales = "00";
    if (!($xpos_punto === false)) {
        if ($xpos_punto == 0) {
            $xcifra = "0" . $xcifra;
            $xpos_punto = strpos($xcifra, ".");
        }
        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
    }

    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
    $xcadena = "";
    for ($xz = 0; $xz < 3; $xz++) {
        $xaux = substr($XAUX, $xz * 6, 6);
        $xi = 0;
        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
        $xexit = true; // bandera para controlar el ciclo del While 
        while ($xexit) {
            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
                break; // termina el ciclo
            }

            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
                switch ($xy) {
                    case 1: // checa las centenas
                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                        } else {
                            $xseek = $xarray[substr($xaux, 0, 3)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                            if ($xseek) {
                                $xsub = subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
                                if (substr($xaux, 0, 3) == 100)
                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                            } else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                $xseek = $xarray[substr($xaux, 0, 1) * 100]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                $xcadena = " " . $xcadena . " " . $xseek;
                            } // ENDIF ($xseek)
                        } // ENDIF (substr($xaux, 0, 3) < 100)
                        break;
                    case 2: // checa las decenas (con la misma lógica que las centenas)
                        if (substr($xaux, 1, 2) < 10) {
                            
                        } else {
                            $xseek = $xarray[substr($xaux, 1, 2)];
                            if ($xseek) {
                                $xsub = subfijo($xaux);
                                if (substr($xaux, 1, 2) == 20)
                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                $xy = 3;
                            } else {
                                $xseek = $xarray[substr($xaux, 1, 1) * 10];
                                if (substr($xaux, 1, 1) * 10 == 20)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                            } // ENDIF ($xseek)
                        } // ENDIF (substr($xaux, 1, 2) < 10)
                        break;
                    case 3: // checa las unidades
                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
                        } else {
                            $xseek = $xarray[substr($xaux, 2, 1)]; // obtengo directamente el valor de la unidad (del uno al nueve)
                            $xsub = subfijo($xaux);
                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                        } // ENDIF (substr($xaux, 2, 1) < 1)
                        break;
                } // END SWITCH
            } // END FOR
            $xi = $xi + 3;
        } // ENDDO

        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
            $xcadena .= " DE";

        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
            $xcadena .= " DE";

        // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
        if (trim($xaux) != "") {
            switch ($xz) {
                case 0:
                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                        $xcadena .= "UN BILLON ";
                    else
                        $xcadena .= " BILLONES ";
                    break;
                case 1:
                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                        $xcadena .= "UN MILLON ";
                    else
                        $xcadena .= " MILLONES ";
                    break;
                case 2:
                    if ($xcifra < 1) {
                        $xcadena = "CERO CON $xdecimales/100 $moneda ";
                    } elseif ($xcifra == 1) {
                        $xcadena = "UN CON $xdecimales/100 $moneda ";
                    } elseif ($xcifra >= 1 && $mon_id == 0) {
                        // Si no hay moneda, simplemente devuelve el número en letras sin la parte de decimales o monedas
                        $xcadena = trim($xcadena); 
                    } else {
                        $xcadena .= " CON $xdecimales/100 $moneda "; 
                    }
                    break;
            } // endswitch ($xz)
        } // ENDIF (trim($xaux) != "")
        // ------------------      en este caso, para México se usa esta leyenda     ----------------
        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles 
        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles 
        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
    } // ENDFOR ($xz)
    return trim($xcadena);
}

// END FUNCTION

function subfijo($xx) { // esta función regresa un subfijo para la cifra
    $xx = trim($xx);
    $xstrlen = strlen($xx);
    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
        $xsub = "";
    // 
    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
        $xsub = "MIL";
    //
    return $xsub;
}

// END FUNCTION

function horas_demas_base_numero_horas($hora_ini, $hora_fin, $base) {
    list($ini_hor, $ini_minu, $ini_seg) = explode(':', $hora_ini);
    list($fin_hor, $fin_minu, $fin_seg) = explode(':', $hora_fin);

    $tot_minu_base = $base * 60;
    $tot_minu_ini = intval($ini_hor) * 60 + intval($ini_minu);
    $tot_minu_fin = intval($fin_hor) * 60 + intval($fin_minu);

    $resultado = $tot_minu_fin - $tot_minu_ini;

    if ($resultado > $tot_minu_base)
        return ($resultado - $tot_minu_base);
    else
        return 0;
}

function obtener_minutos($hora_ini, $hora_fin) {
    list($ini_hor, $ini_minu, $ini_seg) = explode(':', $hora_ini);
    list($fin_hor, $fin_minu, $fin_seg) = explode(':', $hora_fin);

    $tot_minu_ini = intval($ini_hor) * 60 + intval($ini_minu);
    $tot_minu_fin = intval($fin_hor) * 60 + intval($fin_minu);

    $resultado = $tot_minu_fin - $tot_minu_ini;

    return $resultado;
}

function horas_cumplidas_turno_asignado($hor_ing, $hor_sal, $mar_ing, $mar_sal, $tip_hor, $fecha) {
    //tip_hor: tipo de horario, si es 1 está consultando la mañana, si es 2 está consultando la tarde
    //fecha está en formato: 02-08-19
    //parametros de hora de ingreso y salida que debe cumplir y horas marcadas por el colaborador, hor_ing hor_sal (turno asigando), mar_ing mar_sal (horas marcadas)
    $newDate = date_format(date_create_from_format('d-m-y', $fecha), "d-m-Y");
    $weekday = date('N', strtotime($newDate)); //número del día de la semana: (lun 1, mart 2, mier 3, jue 4, vier 5, sab 6)

    $hora_ing_valida = ''; //horas ingreso valida para calcular desde esta hora
    $hora_sal_valida = ''; //hora salida valida para calcular hasta esta hora
    $tardanza = 0;
    //verificar si las horas marcadas son NULL o 00, debido a que los sábado solo es medio día
    if (!validar_horas($mar_ing, $mar_sal)) {
        $data['estado'] = 1;

        $data['horas'] = 0;
        $data['mensaje'] = 'no valida hora';

        if (intval($tip_hor) == 1 && intval($weekday) != 7) {
            $data['tardanza'] = 240;
        }
        if (intval($tip_hor) == 2 && (intval($weekday) != 7 && intval($weekday) != 6)) {
            $data['tardanza'] = 240;
        }
        return $data;
        exit();
    }

    //la hora de ingreso marcado es menor o igual a la hora asignada
    if (strtotime($mar_ing) <= strtotime($hor_ing)) {
        $hora_ing_valida = $hor_ing;
    }
    //la hora de ingreso marcado es mayor a la hora asignada, aqui hay una tardanza
    if (strtotime($mar_ing) > strtotime($hor_ing)) {
        $hora_ing_valida = $mar_ing;
        $tardanza = obtener_minutos($hor_ing, $mar_ing); //resta hora marcada - hora asignada, ejpl: hora marcada(9:10), hora asignada (9:00) = 10 minutos
        $minutos_penalidad = 0;
        if (intval($tardanza) > 5) {
            $minutos_penalidad = intval($tardanza) - 5;
            //echo ' || AQUI HAY UNA PENALIDAD: || FECHA: '.$fecha.' hora que debe ingresar: '.$hor_ing.', hora que marcó: '.$mar_ing.'</br>';
        }
    }

    //la hora de salida marcada es mayor o igual a la hora de salida asiganada
    if ($mar_sal >= $hor_sal) {
        $hora_sal_valida = $hor_sal;
    }
    //la hora de salida marcada es es menor a la hora de salida asiganada
    if ($mar_sal < $hor_sal) {
        $hora_sal_valida = $mar_sal;
    }
    $resultado = obtener_minutos($hora_ing_valida, $hora_sal_valida); //devuelve minutos
    $resultado = $resultado / 60;

    if ($hora_ing_valida != '' && $hora_sal_valida != '' && $resultado != '') {
        $data['estado'] = 1;
        $data['tardanza'] = $tardanza;
        $data['min_penalidad'] = $minutos_penalidad;
        $data['horas'] = number_format($resultado, 2);
        if($minutos_penalidad > 0)
            //echo ' || AQUI HAY UNA PENALIDAD: || FECHA: '.$fecha.' hora que debe ingresar: '.$hor_ing.', hora que marcó: '.$mar_ing.', min penalidad: '.$minutos_penalidad.'</br>';

        return $data;
    } else {
        $data['estado'] = 0;
        $data['msj'] = 'Hay una hora vacía: Ingreso válido: ' . $hora_ing_valida . ', salida válida: ' . $hora_sal_valida . ', resultado: ' . $resultado;

        return $data;
    }
}

function validar_horas($ing, $sal) {
    if ($ing != '' && $ing != '00:00:00' && $sal != '' && $sal != '00:00:00')
        return true;
    else
        return false;
}

function penalidad_tardanza($minutos_tarde, $sueldo, $total_horas, $fecha) {

    $minutos_totales = intval($total_horas) * 60; //ejem: 240 horas mensuales representan 14 400 minutos
    $monto_penalidad = 0;

    if ($minutos_tarde > 5) {
        $min_penalidad = $minutos_tarde - 5; //debido a que se tiene 5 minutos de tolerancia
        //echo '<h4 style="color: red; font-weight: bold;">|| REVISA LA MARCACIÓN DE LA FECHA: || FECHA: '.$fecha.', Min tarde: '.$minutos_tarde.'</h4></br>';
        if (intval($sueldo) > 0 && intval($total_horas) > 0) {
            $monto_penalidad = (intval($sueldo) * $min_penalidad) / $minutos_totales;
        }
    }
    return $monto_penalidad;
    /*
      if($minutos_tarde <= 5)
      return 0;
      elseif ($minutos_tarde > 5 && $minutos_tarde <= 10) {
      return 5;
      }
      elseif ($minutos_tarde > 10 && $minutos_tarde <= 20) {
      return 10;
      }
      else
      return 20; */
}

/* GERSON (17-04-23) */
function penalidad_tardanza_final($minutos_tarde, $sueldo, $total_horas) {

    $minutos_totales = intval($total_horas) * 60; //ejem: 240 horas mensuales representan 14 400 minutos
    $monto_penalidad = 0;

    if ($minutos_tarde > 5) {
        $min_penalidad = $minutos_tarde - 5; //debido a que se tiene 5 minutos de tolerancia
        if (intval($sueldo) > 0 && intval($total_horas) > 0) {
            $monto_penalidad = (intval($sueldo) * $min_penalidad) / $minutos_totales;
        }
    }
    return $monto_penalidad;

}
/*  */

function validar_apertura_caja() {
    //vamos a validar que la caja esté aperturada para poder hacer ingresos o egresos de todo tipo


    require_once ("vista/cajaoperacion/Cajaoperacion.class.php");
    $oCajaoperacion = new Cajaoperacion();
//echo 'hastya aki estoy entrandosasss ';exit();
    $fecha_hoy = date('Y-m-d');
    $bandera_habilitado = 0;
    $empresa_id = $_SESSION['empresa_id'];
    $empresa_nombre = $_SESSION['empresa_nombre'];

    $dts = $oCajaoperacion->mostrar_fecha_hoy($fecha_hoy, $empresa_id);
    if ($dts['estado'] == 1) {
            $apertura_est = intval($dts['data']['tb_cajaoperacion_apertura_est']); // 1 apertura, no aperturado
            $cierre_est = intval($dts['data']['tb_cajaoperacion_cierre_est']); // 1 cerrado, 0 abierto

            if ($apertura_est == 1 && $cierre_est == 0)
                $bandera_habilitado = 1;

    }

    if ($bandera_habilitado == 0) {
        echo '<h2 style="font-family:cambria">No has aperturado o ya está cerrada la caja de este día: ' . date('d-m-Y') . ' ' . ' para la sede de ' . $empresa_nombre . '</h2></section>
            </div></div></body></html>';
        exit();
    }
}

function Horas_Trabajadas($hora1,$hora2){
   
$apertura = new DateTime($hora1);
$cierre = new DateTime($hora2);

$tiempo = $apertura->diff($cierre);

return $tiempo->format('%H:%I:%S');
}

function sumahoras ($hora1,$hora2){
    $hora1=explode(":",$hora1);
    $hora2=explode(":",$hora2);
    $horas=(int)$hora1[0]+(int)$hora2[0];
    $minutos=(int)$hora1[1]+(int)$hora2[1];
    $segundos=(int)$hora1[2]+(int)$hora2[2];
    $horas+=(int)($minutos/60);
    $minutos=(int)($minutos%60)+(int)($segundos/60);
    $segundos=(int)($segundos%60);
return (intval($horas)<10?'0'.intval($horas):intval($horas)).':'.($minutos<10?'0'.$minutos:$minutos).':'.($segundos<10?'0'.$segundos:$segundos);
}

function resaltar_texto($texto){
    $retorno = '<span style="background-color: rgb(255, 0, 0); color: rgb(255, 255, 255);">'.$texto.'</span>';
    return $retorno;
}

function numero_letra($value){
  	$array_letras = ['CERO', 'UNO', 'DOS', 'TRES', 'CUATRO', 'CINCO', 'SEIS', 'SIETE', 'OCHO', 'NUEVE'];
  	return $array_letras[$value];
  }

function obtener_igv_y_base($monto_total){
    $montos["base"] = $monto_total / 1.18;
    $montos["igv"] = $montos["base"] * 0.18;
    return $montos;
}

/* daniel odar 18-04-2023 */
function solo_letras($string){
    $permitidos = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    $letras = true;
    for ($contador = 0; $contador < strlen($string); $contador++) {
        if (!is_numeric(strpos($permitidos, substr($string, $contador, 1)))) {
            $letras = false;
        }
    }
    return $letras;
}

function solo_numeros($string){
    $permitidos = "0123456789";
    $numeros = true;
    for ($contador = 0; $contador < strlen($string); $contador++) {
        if (!is_numeric(strpos($permitidos, substr($string, $contador, 1)))) {
            $numeros = false;
        }
    }
    return $numeros;
}

function formato_numero_contable($string){
    $cantidad_falta = 8 - strlen($string);
    $contador_while = 0;
    $relleno = '';
    while ($contador_while < $cantidad_falta) {
        $relleno .= "0";
        $contador_while++;
    }
    return $relleno.$string;
}

function formato_serie_contable($string){
    $cantidad_falta = 4 - strlen($string);//1 o 2
    if($cantidad_falta == 2){
        $string = substr($string, 0, 1).'00'.substr($string, 1, 1);
    } elseif ($cantidad_falta == 1){
        if(solo_letras(substr($string, 1, 1))){
            $string = substr($string, 0, 2).'0'.substr($string, -1);
        } else {
            $string = substr($string, 0, 1).'0'.substr($string, -2);
        }
    }
    return $string;
}
/*  */

function completar_numero_ceros($string, $cantidad_caracteres){
    $cantidad_falta = $cantidad_caracteres - strlen($string);
    $contador_while = 0;
    $relleno = '';
    while ($contador_while < $cantidad_falta) {
        $relleno .= "0";
        $contador_while++;
    }
    return $relleno.$string;
}

function validar_apertura_caja_2(){
    //uso en formularios modales
    require_once('../cajaoperacion/Cajaoperacion.class.php');
    $oCajaoperacion = new Cajaoperacion();
    $fecha_hoy = date('Y-m-d');
    $bandera_habilitado = 0;
    $empresa_id = $_SESSION['empresa_id'];

    $dts = $oCajaoperacion->mostrar_fecha_hoy($fecha_hoy, $empresa_id);
    if ($dts['estado'] == 1) {
        $apertura_est = intval($dts['data']['tb_cajaoperacion_apertura_est']); // 1 apertura, no aperturado
        $cierre_est = intval($dts['data']['tb_cajaoperacion_cierre_est']); // 1 cerrado, 0 abierto

        if ($apertura_est == 1 && $cierre_est == 0)
            $bandera_habilitado = 1;
    }

    return $bandera_habilitado;
}

function genera_options_bancos_comunes(){
    $option = '
        <option value="">Selecciona Banco...</option>
        <option value="BANCO BCP">BANCO BCP</option>
        <option value="BANCO BBVA">BANCO BBVA</option>
        <option value="BANCO INTERBANK">BANCO INTERBANK</option>
        <option value="BANCO SCOTIABANK">BANCO SCOTIABANK</option>
        <option value="YAPE">YAPE</option>
        <option value="PLIM">PLIM</option>
        <option value="OTROS BANCOS">OTROS BANCOS</option>
    ';

    return $option;
}

function quitar_caracteres_tildes($string) {

    $string = trim($string);
    
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array('\'', '¨', 'º', '-', '~',
            '#', '@', '|', '!', '"',
            '·', '$', '%', '&', '/',
            '(', ')', '?', "'", '¡',
            '¿', '[', '^', '<code>', ']',
            '+', '}', '{', '¨', '´',
            '>', '<', ';', ',', ':',
            '.', ' '),
        '_',
        $string
    );


    return $string;
}

function obtenerOrdinales($numeros) {
    $ordinales = array(
        1 => 'primera',
        2 => 'segunda',
        3 => 'tercera',
        4 => 'cuarta',
        5 => 'quinta',
        6 => 'sexta',
        7 => 'séptima',
        8 => 'octava',
        9 => 'novena',
        10 => 'décima',
        11 => 'décimo primera',
        12 => 'décimo segunda',
        13 => 'décimo tercera',
        14 => 'décimo cuarta',
        15 => 'décimo quinta',
        16 => 'décimo sexta',
        17 => 'décimo séptima',
        18 => 'décimo octava',
        19 => 'décimo novena',
        20 => 'vigésima',
        21 => 'vigésimo primera',
        22 => 'vigésimo segunda',
        23 => 'vigésimo tercera',
        24 => 'vigésimo cuarta',
        25 => 'vigésimo quinta',
        26 => 'vigésimo sexta',
        27 => 'vigésimo séptima',
        28 => 'vigésimo octava',
        29 => 'vigésimo novena',
        30 => 'trigésima',
        31 => 'trigésimo primera',
        32 => 'trigésimo segunda',
        33 => 'trigésimo tercera',
        34 => 'trigésimo cuarta',
        35 => 'trigésimo quinta',
        36 => 'trigésimo sexta',
        37 => 'trigésimo séptima',
        38 => 'trigésimo octava',
        39 => 'trigésimo novena',
        40 => 'cuadragésima',
        41 => 'cuadragésimo primera',
        42 => 'cuadragésimo segunda',
        43 => 'cuadragésimo tercera',
        44 => 'cuadragésimo cuarta',
        45 => 'cuadragésimo quinta',
        46 => 'cuadragésimo sexta',
        47 => 'cuadragésimo séptima',
        48 => 'cuadragésimo octava',
        49 => 'cuadragésimo novena',
        50 => 'quincuagésima',
    );
    $texto_ordinales = array_map(function($numero) use ($ordinales) {
        return isset($ordinales[$numero]) ? $ordinales[$numero] : '';
    }, $numeros);
    // Eliminar elementos vacíos y obtener la cantidad de elementos
    $num_elementos = count($texto_ordinales);
    $texto_ordinales = array_filter($texto_ordinales);
    // Si solo hay un elemento, devolverlo directamente
    if ($num_elementos === 1) {
        return reset($texto_ordinales);
    }
    // Si hay más de un elemento, concatenar con "y" o "," si hay más de dos elementos
    if ($num_elementos === 2) {
        return implode(' y ', $texto_ordinales);
    } else {
        $ultimo_elemento = array_pop($texto_ordinales);
        return implode(', ', $texto_ordinales) . ' y ' . $ultimo_elemento;
    }
}

function customUcwords($str) {
    // Divide la cadena en palabras
    $words = explode(' ', $str);
    
    // Inicializa una cadena vacía para el resultado
    $result = '';
    
    foreach ($words as $word) {
        // Verifica si la palabra está entre comillas dobles o simples
        if (preg_match('/^["\'].*["\']$/', $word)) {
            // Si es así, agrega la palabra tal cual al resultado
            $result .= $word . ' ';
        } else {
            // Si no, convierte la primera letra a mayúscula y el resto a minúscula
            $result .= mb_convert_case(mb_strtolower($word, 'UTF-8'), MB_CASE_TITLE, 'UTF-8') . ' ';
        }
    }
    
    // Elimina el espacio extra al final
    return rtrim($result);
}

?>