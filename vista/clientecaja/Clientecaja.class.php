<?php
  if(defined('APP_URL')){
  require_once(APP_URL.'datos/conexion.php');}
  else{
  require_once('../../datos/conexion.php');}
  class Clientecaja extends Conexion{
    public $tb_clientecaja_id;
    public $tb_clientecaja_xac = 1;
    public $tb_cliente_id;
    public $tb_credito_id;
    public $tb_clientecaja_tip; 
    public $tb_clientecaja_fec; 
    public $tb_moneda_id; 
    public $tb_clientecaja_mon; 
    public $tb_modulo_id; 
    public $tb_clientecaja_modid; 


    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_clientecaja (
                                            tb_clientecaja_xac,
                                            tb_cliente_id,
                                            tb_credito_id,
                                            tb_clientecaja_tip,
                                            tb_clientecaja_fec,
                                            tb_moneda_id,
                                            tb_clientecaja_mon,
                                            tb_modulo_id,
                                            tb_clientecaja_modid)
                                    VALUES 
                                            (
                                            :tb_clientecaja_xac,
                                            :tb_cliente_id,
                                            :tb_credito_id,
                                            :tb_clientecaja_tip,
                                            :tb_clientecaja_fec,
                                            :tb_moneda_id,
                                            :tb_clientecaja_mon,
                                            :tb_modulo_id,
                                            :tb_clientecaja_modid)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_clientecaja_xac", $this->tb_clientecaja_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cliente_id", $this->tb_cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_id", $this->tb_credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_clientecaja_tip", $this->tb_clientecaja_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_clientecaja_fec", $this->tb_clientecaja_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_clientecaja_mon", $this->tb_clientecaja_mon, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_modulo_id", $this->tb_modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_clientecaja_modid", $this->tb_clientecaja_modid, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $tclientecaja_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['tb_clientecaja_id'] = $tclientecaja_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_clientecaja(
                :clientecaja_id,
                :clientecaja_xac, :clientecaja_id, :clientecajatipo_id,
                :clientecaja_can, :clientecaja_pro, :clientecaja_val,
                :clientecaja_valtas, :clientecaja_ser, :clientecaja_kil,
                :clientecaja_pes, :clientecaja_tas, :clientecaja_det,
                :clientecaja_web)";

        $sentencia = $this->dblink->prepare($sql);
        

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($clientecaja_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_clientecaja WHERE tb_clientecaja_id =:clientecaja_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":clientecaja_id", $clientecaja_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($clientecaja_id){
      try {
        $sql = "SELECT * FROM tb_clientecaja WHERE tb_clientecaja_id =:clientecaja_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":clientecaja_id", $clientecaja_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrar_clientecaja_modulo($modulo_id, $clientecaja_modid){
      try {
        $sql = "SELECT * FROM tb_clientecaja c INNER JOIN tb_moneda m ON c.tb_moneda_id = m.tb_moneda_id WHERE c.tb_modulo_id =:modulo_id AND c.tb_clientecaja_modid =:clientecaja_modid";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":clientecaja_modid", $clientecaja_modid, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function modificar_campo($clientecaja_id, $clientecaja_columna, $clientecaja_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($clientecaja_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_clientecaja SET ".$clientecaja_columna." =:clientecaja_valor WHERE tb_clientecaja_id =:clientecaja_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":clientecaja_id", $clientecaja_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":clientecaja_valor", $clientecaja_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":clientecaja_valor", $clientecaja_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
        
    function filtrar_por_cliente_creditomenor($cli_id,$mon_id){
      try {
        // $sql = "SELECT * FROM 
        //           tb_clientecaja cli 
        //           INNER JOIN tb_creditomenor cre on cre.tb_credito_id = cli.tb_credito_id 
        //         WHERE 
        //           tb_clientecaja_xac=1 AND cli.tb_cliente_id=$cli_id AND cre.tb_cliente_id=$cli_id AND 
        //           cli.tb_moneda_id=$mon_id and cre.tb_credito_xac = 1 and 
        //           cre.tb_credito_est in(3,4,5) ORDER BY tb_clientecaja_id";
        //? MEJORAREMOS LA CONSULTA DE CAJA CLIENTE, YA NO POR CRÉDITO SINO SOLO POR CLIENTE
        $sql = "SELECT * FROM 
                  tb_clientecaja cli 
                WHERE 
                  tb_clientecaja_xac =1 AND cli.tb_cliente_id = $cli_id  AND 
                  cli.tb_moneda_id = $mon_id ORDER BY tb_clientecaja_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Clientes de Créditos Menor";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
        
    function filtrar_por_cliente($cli_id,$mon_id, $tabla){
      try {
        $sql="SELECT * FROM tb_clientecaja cli WHERE tb_clientecaja_xac=1 AND cli.tb_cliente_id=:cliente_id AND cli.tb_moneda_id=:moneda_id ORDER BY tb_clientecaja_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
                $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Clientes de Créditos Menor";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function filtrar_por_credito($cli_id,$mon_id,$cre_id, $tabla){
      try {
        $sql = "SELECT * 
                FROM tb_clientecaja cli 
                INNER JOIN $tabla cre on cre.tb_credito_id = cli.tb_credito_id 
                WHERE tb_clientecaja_xac=1 
                AND cli.tb_cliente_id=:cliente_id 
                AND cli.tb_moneda_id=:moneda_id 
                AND cre.tb_credito_xac = 1 
                AND cli.tb_credito_id !=:credito_id 
                AND cre.tb_credito_est NOT IN (1,2) ORDER BY tb_clientecaja_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
                $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
                $sentencia->bindParam(":credito_id", $cre_id, PDO::PARAM_INT);
                $sentencia->execute();

                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay Clientes de Créditos Menor";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    
    function cambio_de_cliente($cli_id,$cli_id_nuevo){
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_clientecaja SET tb_cliente_id =:cli_id_nuevo WHERE tb_cliente_id =:cli_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cli_id_nuevo", $cli_id_nuevo, PDO::PARAM_INT);
          $sentencia->bindParam(":cli_id", $cli_id, PDO::PARAM_INT);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
  }

?>
