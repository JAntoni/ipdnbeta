<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Colocaciones extends Conexion{

    function colocaciones_vehiculares_rango($fecha_filtro1, $fecha_filtro2){
      try {
        $sql = "
          SELECT
          tb_usuario_nom,
          CONCAT(MONTH(tb_credito_fecvig),'-', YEAR(tb_credito_fecvig)) as mes_anio,
          SUM((CASE WHEN tb_moneda_id = 2 THEN ROUND(tb_credito_preaco * tb_credito_tipcam, 2) ELSE tb_credito_preaco END)) AS monto_colocado
          FROM tb_creditogarveh cre
          INNER JOIN tb_usuario usu ON usu.tb_usuario_id = cre.tb_credito_usureg
          WHERE tb_credito_xac = 1
          AND tb_credito_tip != 3 AND tb_credito_tip2 NOT IN(5,6) AND tb_credito_int > 0 AND tb_credito_preaco > 0
          and tb_credito_fecvig BETWEEN :fecha_filtro1 AND :fecha_filtro2
          GROUP BY tb_usuario_id, mes_anio ORDER BY YEAR(tb_credito_fecvig), MONTH(tb_credito_fecvig);
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE COLOCACIONES";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function colocaciones_creditomenor_rango($fecha_filtro1, $fecha_filtro2){
      try {
        $sql = "
          SELECT 
            tb_usuario_nom,
            CONCAT(MONTH(tb_credito_feccre),'-', YEAR(tb_credito_feccre)) as mes_anio,
            SUM(tb_garantia_val) AS monto_colocado
          FROM 
          tb_creditomenor tc 
            INNER JOIN tb_garantia tg on tg.tb_credito_id=tc.tb_credito_id
            INNER JOIN tb_usuario usu ON usu.tb_usuario_id = tc.tb_credito_usureg
            WHERE tb_credito_xac =1 
            AND tb_credito_est NOT IN(1,2) 
            and tb_credito_feccre BETWEEN :fecha_filtro1 AND :fecha_filtro2
            AND tb_garantiatipo_id NOT IN(5,6)
            GROUP BY tb_usuario_id, mes_anio ORDER BY YEAR(tb_credito_feccre), MONTH(tb_credito_feccre);
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE COLOCACIONES";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function venta_garantias_cmenor($fecha_filtro1, $fecha_filtro2){
      try {
        $sql = "
          SELECT 
              tb_usuario_nom,
              CONCAT(MONTH(tb_ventagarantia_fec),'-', YEAR(tb_ventagarantia_fec)) as mes_anio,
              SUM(v.tb_ventagarantia_prec1) as monto_solicitado,
              SUM(v.tb_ventagarantia_prec2) as monto_vendido
          FROM 
              (SELECT 
                  CASE 
                      WHEN tb_ventagarantia_col = 0 THEN tb_usuario_id 
                      ELSE tb_ventagarantia_col 
                  END as usuario_id,
                  tb_ventagarantia_prec1,
                  tb_ventagarantia_prec2, 
                  tb_ventagarantia_com,
                  tb_ventagarantia_fec
              FROM 
                  tb_ventagarantia
              WHERE 
                  tb_ventagarantia_xac = 1 
                  AND tb_ventagarantia_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2
                  AND tb_ventagarantia_col != 20) as v
          INNER JOIN 
              tb_usuario u ON u.tb_usuario_id = v.usuario_id
              GROUP BY tb_usuario_id, mes_anio ORDER BY YEAR(tb_ventagarantia_fec), MONTH(tb_ventagarantia_fec)
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE COLOCACIONES";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function venta_vehiculos($fecha_filtro1, $fecha_filtro2){
      try {
        $sql = "
          SELECT
            usu.tb_usuario_nom,
            CONCAT(MONTH(tb_ventavehiculo_fec),'-',YEAR(tb_ventavehiculo_fec)) as mes_anio,
            SUM((CASE WHEN tb_moneda_id = 2 THEN ROUND(tb_ventavehiculo_mon * tb_ventavehiculo_tipcam, 2) ELSE tb_ventavehiculo_mon END)) as monto_vendido
          FROM tb_ventavehiculo ven
            INNER JOIN tb_usuario usu on usu.tb_usuario_id = ven.tb_usuario_id2
            where tb_ventavehiculo_xac = 1 
            and tb_ventavehiculo_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2
            GROUP BY ven.tb_usuario_id2, mes_anio ORDER BY YEAR(tb_ventavehiculo_fec), MONTH(tb_ventavehiculo_fec);
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE COLOCACIONES";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function cobro_moras($fecha_filtro1, $fecha_filtro2){
      try {
        $sql = "
          SELECT
            emp.tb_empresa_nomcom,
            CONCAT(MONTH(tb_ingreso_fec),'-', YEAR(tb_ingreso_fec)) as mes_anio,
            SUM(tb_ingreso_imp) AS monto_moras
          FROM tb_ingreso ing
          INNER JOIN tb_empresa emp ON emp.tb_empresa_id = ing.tb_empresa_id
          WHERE tb_ingreso_xac = 1 AND tb_cuenta_id = 26 AND tb_ingreso_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2
          GROUP BY ing.tb_empresa_id, mes_anio ORDER BY YEAR(tb_ingreso_fec), MONTH(tb_ingreso_fec)
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE COLOCACIONES";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
