var datatable_global;

function colocaciones_vehiculares_rango(){

  $.ajax({
    type: "POST",
    url: VISTA_URL+"colocaciones/colocaciones_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha_filtro1: $('#txt_filtro_fec1').val(),
      fecha_filtro2: $('#txt_filtro_fec2').val(),
      vista_modulo: 'colocaciones'
    }),
    beforeSend: function() {
      
    },
    success: function(data){
      $('#tabla_vehiculares').html(data)
    },
    complete: function(data){
    },
    error: function(data){
      alerta_error('ERROR', 'Revisa la consola del error');
      console.log(data);
    }
  });
}

$(document).ready(function() {
  console.log('Colocaciones 222');

  colocaciones_vehiculares_rango();

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });


});