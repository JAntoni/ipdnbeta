<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $menu_tit; ?>
      <small><?php echo $menu_des; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
      <li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
    </ol>
  </section>
  <style type="text/css">
    div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
      font-weight: bold;
    }

    div.dataTables_filter label {
      width: 80%;
    }
  </style>
  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header">
        <?php require_once('colocaciones_filtro.php'); ?>
      </div>
      <div class="box-body">
        <!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="colocaciones_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla ASIVEH...</span></h4>
        </div>

        <div class="box-body" id="tabla_vehiculares">

        </div>

      </div>

      <div id="div_modal_colocaciones_form">
        <!-- INCLUIMOS EL MODAL PARA EL REGISthO DEL TIPO-->
      </div>
      <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
      <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
    </div>
  </section>
  <!-- /.content -->
</div>