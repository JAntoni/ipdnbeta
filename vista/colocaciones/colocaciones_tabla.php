<?php
  date_default_timezone_set("America/Lima");
  require_once('../colocaciones/Colocaciones.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oColocaciones = new Colocaciones();

  $fecha_filtro1 = fecha_mysql($_POST['fecha_filtro1']);
  $fecha_filtro2 = fecha_mysql($_POST['fecha_filtro2']);
  $fecha_hoy = date('d-m-Y');
  
  $array_meses = [];

  $start = new DateTime($_POST['fecha_filtro1']);
  $end = new DateTime($_POST['fecha_filtro2']);

  // Iterar a través de cada mes entre las fechas
  while ($start < $end) {
    // Formatear la fecha como "m-Y" y agregarla al array
    array_push($array_meses, $start->format('n-Y'));
    // Avanzar al siguiente mes
    $start->modify('+1 month');
  }

  //? REPORTE DE COLOCACIONES DE CREDITOS VEHICULARES
    $asesores_data = [];

    $result = $oColocaciones->colocaciones_vehiculares_rango($fecha_filtro1, $fecha_filtro2);
      if($result['estado'] == 1){
        //agregamos los meses únicos, como claves
        // foreach ($result['data'] as $key => $value) {
        //   if (!in_array($value['mes_anio'], $array_meses))
        //     array_push($array_meses, $value['mes_anio']);
        // }

        foreach ($result['data'] as $key => $value) {
          $asesor = $value['tb_usuario_nom'];
          $mes_anio = $value['mes_anio'];
          $monto_colocado = $value['monto_colocado'];

          if (!isset($asesores_data[$asesor])) {
            $asesores_data[$asesor] = array_fill_keys($array_meses, 0);
          }

          // Guardar el monto en el mes correspondiente
          $asesores_data[$asesor][$mes_anio] = $monto_colocado;
        }
      }
    $result = NULL;

    $tabla_vehiculares = '
    <table class="table table-hover table-striped" style="width: 100%;">
      <tbody>
        <tr>
          <th>ASESOR</th>';
          foreach ($array_meses as $mes_anio) {
            $tabla_vehiculares .= '<th>'.$mes_anio. '</th>';
          }
          $tabla_vehiculares .='
        </tr>';
        foreach ($asesores_data as $asesor => $monto_mes) {
          $tabla_vehiculares .= '<tr>';
            $tabla_vehiculares .= '<td>'.$asesor. '</td>';
            foreach ($array_meses as $mes_anio) {
              $tabla_vehiculares .= '<td>'.(isset($monto_mes[$mes_anio])? $monto_mes[$mes_anio] : 0). '</td>';
            }
          $tabla_vehiculares .= '</tr>';
        }
        $tabla_vehiculares .= '
      </tbody>
    </table>';
  //? ***************************** FIN REPOTE DE CREDITOS VEHICULARES *************************

  //* REPORTE DE COLOCACION DE CREDITOS MENORES ********************
    $asesores_data = [];

    $result = $oColocaciones->colocaciones_creditomenor_rango($fecha_filtro1, $fecha_filtro2);
      if($result['estado'] == 1){

        foreach ($result['data'] as $key => $value) {
          $asesor = $value['tb_usuario_nom'];
          $mes_anio = $value['mes_anio'];
          $monto_colocado = $value['monto_colocado'];

          if (!isset($asesores_data[$asesor])) {
            $asesores_data[$asesor] = array_fill_keys($array_meses, 0);
          }

          // Guardar el monto en el mes correspondiente
          $asesores_data[$asesor][$mes_anio] = $monto_colocado;
        }
      }
    $result = NULL;

    $tabla_menores = '
    <table class="table table-hover table-striped" style="width: 100%;">
      <tbody>
        <tr>
          <th>ASESOR</th>';
          foreach ($array_meses as $mes_anio) {
            $tabla_menores .= '<th>'.$mes_anio. '</th>';
          }
          $tabla_menores .='
        </tr>';
        foreach ($asesores_data as $asesor => $monto_mes) {
          $tabla_menores .= '<tr>';
            $tabla_menores .= '<td>'.$asesor. '</td>';
            foreach ($array_meses as $mes_anio) {
              $tabla_menores .= '<td>'.(isset($monto_mes[$mes_anio])? $monto_mes[$mes_anio] : 0). '</td>';
            }
          $tabla_menores .= '</tr>';
        }
        $tabla_menores .= '
      </tbody>
    </table>';
  //************************************* FIN REPORTE DE CREDITOS MENORES ************************

  //! REPOTE DE VENTA DE GARANTÍAS DE CREDITO MENOR **********************

    $asesores_data = [];

    //? REPORTE DE COLOCACIONES DE CREDITOS VEHICULARES
    $result = $oColocaciones->venta_garantias_cmenor($fecha_filtro1, $fecha_filtro2);
      if($result['estado'] == 1){
        //agregamos los meses únicos, como claves
        // foreach ($result['data'] as $key => $value) {
        //   if (!in_array($value['mes_anio'], $array_meses))
        //     array_push($array_meses, $value['mes_anio']);
        // }

        foreach ($result['data'] as $key => $value) {
          $asesor = $value['tb_usuario_nom'];
          $mes_anio = $value['mes_anio'];
          $monto_vendido = $value['monto_vendido'];

          if (!isset($asesores_data[$asesor])) {
            $asesores_data[$asesor] = array_fill_keys($array_meses, 0);
          }

          // Guardar el monto en el mes correspondiente
          $asesores_data[$asesor][$mes_anio] = $monto_vendido;
        }
      }
    $result = NULL;

    $tabla_remates = '
    <table class="table table-hover table-striped" style="width: 100%;">
      <tbody>
        <tr>
          <th>ASESOR</th>';
          foreach ($array_meses as $mes_anio) {
            $tabla_remates .= '<th>'.$mes_anio. '</th>';
          }
          $tabla_remates .='
        </tr>';
        foreach ($asesores_data as $asesor => $monto_mes) {
          $tabla_remates .= '<tr>';
            $tabla_remates .= '<td>'.$asesor. '</td>';
            foreach ($array_meses as $mes_anio) {
              $tabla_remates .= '<td>'.(isset($monto_mes[$mes_anio])? $monto_mes[$mes_anio] : 0). '</td>';
            }
          $tabla_remates .= '</tr>';
        }
        $tabla_remates .= '
      </tbody>
    </table>';
  //! ************************************* FIN REMATES DE CREDITO MENOR ****************************+
  
  //****************** REPORTE VENTA VEHICULAR **********************************
    $asesores_data = [];

    $result = $oColocaciones->venta_vehiculos($fecha_filtro1, $fecha_filtro2);
      if($result['estado'] == 1){      
        foreach ($result['data'] as $key => $value) {
          $asesor = $value['tb_usuario_nom'];
          $mes_anio = $value['mes_anio'];
          $monto_vendido = $value['monto_vendido'];
          
          if (!isset($asesores_data[$asesor])) {
            $asesores_data[$asesor] = array_fill_keys($array_meses, 0);
          }
          
          // Guardar el monto en el mes correspondiente
          $asesores_data[$asesor][$mes_anio] = $monto_vendido;
        }
      }
    $result = NULL;
    
    $tabla_venta_vehiculos = '
      <table class="table table-hover table-striped" style="width: 100%;">
        <tbody>
          <tr>
            <th>ASESOR</th>';
            foreach ($array_meses as $mes_anio) {
              $tabla_venta_vehiculos .= '<th>'.$mes_anio. '</th>';
            }
            $tabla_venta_vehiculos .='
          </tr>';
          foreach ($asesores_data as $asesor => $monto_mes) {
            $tabla_venta_vehiculos .= '<tr>';
              $tabla_venta_vehiculos .= '<td>'.$asesor. '</td>';
              foreach ($array_meses as $mes_anio) {
                $tabla_venta_vehiculos .= '<td>'.(isset($monto_mes[$mes_anio])? $monto_mes[$mes_anio] : 0). '</td>';
              }
            $tabla_venta_vehiculos .= '</tr>';
          }
          $tabla_venta_vehiculos .= '
        </tbody>
      </table>';
  //************************************* FIN REPORTE VENTA VEHICULOS ************************

  //? **************** REPORTE DE COBRO DE MORAS POR SEDE **********************
    $asesores_data = [];

    $result = $oColocaciones->cobro_moras($fecha_filtro1, $fecha_filtro2);
      if($result['estado'] == 1){      
        foreach ($result['data'] as $key => $value) {
          $asesor = $value['tb_empresa_nomcom'];
          $mes_anio = $value['mes_anio'];
          $monto_vendido = $value['monto_moras'];
          
          if (!isset($asesores_data[$asesor])) {
            $asesores_data[$asesor] = array_fill_keys($array_meses, 0);
          }
          
          // Guardar el monto en el mes correspondiente
          $asesores_data[$asesor][$mes_anio] = $monto_vendido;
        }
      }
    $result = NULL;
    
    $tabla_moras = '
      <table class="table table-hover table-striped" style="width: 100%;">
        <tbody>
          <tr>
            <th>SEDE</th>';
            foreach ($array_meses as $mes_anio) {
              $tabla_moras .= '<th>'.$mes_anio. '</th>';
            }
            $tabla_moras .='
          </tr>';
          foreach ($asesores_data as $asesor => $monto_mes) {
            $tabla_moras .= '<tr>';
              $tabla_moras .= '<td>'.$asesor. '</td>';
              foreach ($array_meses as $mes_anio) {
                $tabla_moras .= '<td>'.(isset($monto_mes[$mes_anio])? $monto_mes[$mes_anio] : 0). '</td>';
              }
            $tabla_moras .= '</tr>';
          }
          $tabla_moras .= '
        </tbody>
      </table>';
  //? ************************ FIN REPORTE DE COBRO DE MORAS *******************
  ?>
  
  <div class="row">
    <div class="box box-success box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Colocación de Créditos Vehiculares</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <?php echo $tabla_vehiculares;?>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="box box-success box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Colocación de Créditos Menores</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <?php echo $tabla_menores;?>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="box box-success box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Venta de Garantías Créditos Menores</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <?php echo $tabla_remates;?>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="box box-success box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Venta de Vehículos</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <?php echo $tabla_venta_vehiculos;?>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="box box-success box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Cobro de Moras por Sede</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <?php echo $tabla_moras;?>
        </div>
      </div>
    </div>
  </div>