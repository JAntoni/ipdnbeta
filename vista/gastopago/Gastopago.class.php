<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Gastopago extends Conexion{
    public $tb_gastopago_id;
    public $tb_moneda_idfk;
    public $tb_gastopago_monto;
    public $tb_empresa_idfk;
    public $tb_gasto_idfk;
    public $tb_gastopago_formapago;
    public $tb_gastopago_numope;
    public $tb_ingreso_idfk;
    public $tb_egreso_idfk;
    public $tb_gastopago_emitedocsunat;
    public $tb_gastopago_documentossunat;
    public $tb_gastopago_anexado;
    public $tb_gastopago_fecha;
    public $tb_gastopago_usureg;
    public $tb_gastopago_obs;
    public $tb_gastopago_fecreg;

    public function listar_todos($where, $join, $otros){
        //where: bidimensional con formato $column_name, $param.$i, $datatype, $conector (and o or) cuando sea mayor que cero
        //join: $iner o left, $nombretb alias, $columna_deenlace, $alias.columnaPK, $alias_columnasparaver
        //otros: arreglo para funciones diversas como el order by, limit, etc. los keys se definen de acuerdo al uso (ejm: ordenar: ["orden"]["column_name"], ["orden"]["value"])
        try {
            $sql = "SELECT gp.*";
            if (!empty($join)) {
                for ($j = 0; $j < count($join); $j++) {
                    $sql .= ",\n" . $join[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_gastopago gp\n";
            if (!empty($join)) {
                for ($k = 0; $k < count($join); $k++) {
                    $sql .= $join[$k]['tipo_union'] . " JOIN " . $join[$k]['tabla_alias'] . " ON ";
                    if (!empty($join[$k]['columna_enlace']) && !empty($join[$k]['alias_columnaPK'])){
                        $sql .= $join[$k]['columna_enlace'] . " = " . $join[$k]['alias_columnaPK'] . "\n";
                    } else {
                        $sql .= $join[$k]['on'] . "\n";
                    }
                }
            }
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i])) {
                    $sql .= ($i > 0) ? "\n{$where[$i]["conector"]} " : "WHERE ";

                    if(!empty($where[$i]["column_name"])){
                        if (stripos($where[$i]["column_name"], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$where[$i]["column_name"]}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$where[$i]["column_name"]}";
                        }

                        if(!empty($where[$i]["datatype"])){
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " ". $where[$i]["param$i"];
                        }
                    } else {
                        $sql .= $where[$i]["param$i"];
                    }
                }
            }
            if (!empty($otros["group"])) {
                $sql .= "\nGROUP BY {$otros["group"]["column_name"]}";
            }
            if (!empty($otros["orden"])) {
                $sql .= "\nORDER BY {$otros["orden"]["column_name"]} {$otros["orden"]["value"]}";
            }
            if (!empty($otros["limit"])) {
                $sql .= "\nLIMIT {$otros["limit"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i]["column_name"]) && !empty($where[$i]["datatype"])) {
                    $_PARAM = strtoupper($where[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $where[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            $retorno["sql"] = $sql;
            $retorno["row_count"] = $sentencia->rowCount();
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    public function insertar(){
        $this->dblink->beginTransaction();
        try {
            $opcional = $this->tb_gastopago_formapago == 2 ? 'tb_gastopago_numope,' : '';
            $opcional.= !empty($this->tb_ingreso_idfk) ? 'tb_ingreso_idfk,' : '';
            $opcional.= $this->tb_gastopago_emitedocsunat == 1 ? 'tb_gastopago_documentossunat,' : '';

            $param_opc = $this->tb_gastopago_formapago == 2 ? ':param_opc0,' : '';
            $param_opc.= !empty($this->tb_ingreso_idfk) == 2 ? ':param_opc2,' : '';
            $param_opc.= $this->tb_gastopago_emitedocsunat == 1 ? ':param_opc1,' : '';

            $sql = "INSERT INTO tb_gastopago (
                tb_gastopago_monto,
                tb_empresa_idfk,
                tb_gasto_idfk,
                tb_gastopago_formapago,
                tb_gastopago_emitedocsunat,
                $opcional
                tb_gastopago_usureg,
                tb_gastopago_obs,
                tb_moneda_idfk,
                tb_egreso_idfk,
                tb_gastopago_fecha)
              VALUES (
                :param0,
                :param1,
                :param2,
                :param3,
                :param4,
                $param_opc
                :param5,
                :param6,
                :param7,
                :param8,
                :param9);";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tb_gastopago_monto, PDO::PARAM_STR);
            $sentencia->bindParam(":param1", $this->tb_empresa_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param2", $this->tb_gasto_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param3", $this->tb_gastopago_formapago, PDO::PARAM_INT);
            $sentencia->bindParam(":param4", $this->tb_gastopago_emitedocsunat, PDO::PARAM_INT);
            if($this->tb_gastopago_formapago == 2){
                $sentencia->bindParam(":param_opc0", $this->tb_gastopago_numope, PDO::PARAM_STR);
            }
            if (!empty($this->tb_ingreso_idfk)) {
                $sentencia->bindParam(":param_opc2", $this->tb_ingreso_idfk, PDO::PARAM_INT);
            }
            if($this->tb_gastopago_emitedocsunat == 1){
                $sentencia->bindParam(":param_opc1", $this->tb_gastopago_documentossunat, PDO::PARAM_STR);
            }
            $sentencia->bindParam(":param5", $this->tb_gastopago_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":param6", $this->tb_gastopago_obs, PDO::PARAM_STR);
            $sentencia->bindParam(":param7", $this->tb_moneda_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param8", $this->tb_egreso_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param9", $this->tb_gastopago_fecha, PDO::PARAM_STR);

            $resultado['estado'] = $sentencia->execute();
            $result['sql'] = $sql;

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Pago de gasto registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar_campo($gasto_id, $gasto_columna, $gasto_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($gasto_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_gastopago SET " . $gasto_columna . " = :gasto_valor WHERE tb_gastopago_id = :gasto_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":gasto_id", $gasto_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":gasto_valor", $gasto_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":gasto_valor", $gasto_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_documentossunatdegastos() {
        try {
            $sql = "SELECT DISTINCT gp.tb_gastopago_documentossunat, gp.tb_gastopago_fecha,
                            g.tb_proveedor_id, p.tb_proveedor_nom, p.tb_proveedor_doc,
                            g.tb_moneda_id
                    FROM tb_gastopago gp
                    INNER JOIN tb_gasto g ON gp.tb_gasto_idfk = g.tb_gasto_id
                    INNER JOIN tb_proveedor p ON g.tb_proveedor_id = p.tb_proveedor_id
                    WHERE tb_gastopago_emitedocsunat = 1
                    AND tb_gastopago_anexado = 0
                    AND (tb_gastopago_documentossunat IS NOT NULL AND TRIM(tb_gastopago_documentossunat) <> '')
                    GROUP BY g.tb_proveedor_id, gp.tb_gastopago_documentossunat;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            $retorno['sql'] = $sql;
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["cantidad"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }
}