var datatable_global_gastopago;

$(document).ready(function () {
    setTimeout(function() {completar_tabla_gastopagos();}, 150);
});

function completar_tabla_gastopagos() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastopago/gastopago_tabla_form_vista.php",
        async: true,
        dataType: "html",
        data: ({
            gasto_id: $('#hdd_gasto_id').val()
        }),
        success: function(data) {
            $('#div_html_tablagastopago').html(data);
            estilos_datatable();
        }
    });
}

function estilos_datatable() {
    datatable_global_gastopago = $('#tbl_gastopagos').DataTable({
        "pageLength": 25,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [], orderable: false}
        ]
    });
    datatable_texto_filtrar();
}

function datatable_texto_filtrar() {
    $('input[aria-controls*="tbl_gastopagos"]')
    .attr('id', 'txt_datatable_gastopago_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_gastopago_fil').keyup(function (event) {
        $('#hdd_datatable_gastopagos_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_gastopagos_fil').val();
    if (text_fil) {
        $('#txt_datatable_gastopago_fil').val(text_fil);
        datatable_global_gastopago.search(text_fil).draw();
    }
};

function pagar(action, gasto_id, gastopago_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastopago/gastopago_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            gasto_id: gasto_id,
            gastopago_id: gastopago_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_pago_form').html(html);
            $('#modal_gastopago_form').modal('show');
            $('#modal_mensaje').modal('hide');

            modal_hidden_bs_modal('modal_gastopago_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_gastopago_form', 30);
            modal_height_auto('modal_gastopago_form'); //funcion encontrada en public/js/generales.js
        }
    });
}

function upload_form(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'gastopago', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
            //                modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function Abrir_imagen(gastopago_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastopago/gastopago_img.php",
        async: true,
        dataType: "html",
        data: ({}),
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_cajaoperacion_img').modal('show');
            modal_hidden_bs_modal('modal_cajaoperacion_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(gastopago_id);
        }
    });
}

function upload_galeria(gastopago_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'gastopago', //nombre de la tabla a relacionar
            modulo_id: gastopago_id
        }),
        success: function (data) {
            $('.galeria').html(data);
        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}