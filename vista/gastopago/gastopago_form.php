<?php
if (defined('VISTA_URL')) {
    require_once APP_URL . 'core/usuario_sesion.php';
} else {
    require_once '../../core/usuario_sesion.php';
}
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';
require_once '../monedacambio/Monedacambio.class.php';
$oMonedacambio = new Monedacambio();
require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();
require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();
require_once '../gasto/Gasto.class.php';
$oGasto = new Gasto();
require_once '../gastopago/Gastopago.class.php';
$oGastopago = new Gastopago();
require_once '../deposito/Deposito.class.php';
$oDeposito = new Deposito();

$usuario_action = $_POST['action'];
$listo = 1;
$mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
$falta_tc_calculos = false;
$vista = $_POST['vista'];
$credito_id = $_POST['credito_id'];
$ejecucion_id = $_POST['ejecucion_id'];

$caja_aperturada = validar_apertura_caja_2();
if ($caja_aperturada == 1) {
    $fec = date('Y-m-d');
    $result = $oMonedacambio->consultar($fec);
    $tccompra_hoy = mostrar_moneda(0.00);
    if ($result['estado'] == 1) {
        $tccompra_hoy = ($result['data']['tb_monedacambio_com']);
    }
    unset($result);
    if ($tccompra_hoy < 1) {
        $mensaje = "No hay tipo de cambio registrado en el día";
    } else {
        $titulo = '';
        if ($usuario_action == 'I') {
            $titulo = 'Registrar pago';
        } elseif ($usuario_action == 'M' || $usuario_action == 'M_doc') {
            $edita_solo_doc = intval($usuario_action == 'M_doc');
            $titulo = 'Editar pago';
        } elseif ($usuario_action == 'E') {
            $titulo = 'Eliminar pago';
        } elseif ($usuario_action == 'L') {
            $titulo = 'Pago registrado';
        } else {
            $titulo = 'Acción de Usuario Desconocido';
        }
        $action = $usuario_action == 'M_doc' ? 'M_doc' : devuelve_nombre_usuario_action($usuario_action);

        $gasto_id = intval($_POST['gasto_id']);
        $gastopago_id = intval($_POST['gastopago_id']);
        $tipocuenta = 2; //cuentas de egreso
        
        $cuenta_id = in_array($vista, ['legalizaciongasto', 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', 'cartanotgasto', 'ejecucionfasefile']) ? 16 : 10;//cuenta por defecto si es de ejecucion legal 16 gasto operativo, si no gastos unidades
        $subcuenta_id = in_array($vista, ['legalizaciongasto', 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', 'cartanotgasto', 'ejecucionfasefile']) ? 94 : 34; //subcuenta default si es de ejecucion legal 94 tramites notariales, si no mantenimiento unidades
        if (in_array($vista, ['legalizaciongasto', 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', 'cartanotgasto', 'ejecucionfasefile'])) {
            $gasto_prov_nom = '20600752872 - IPDN';
            $emite_doc_sunat = $vista == 'ejecucionfasefile' ? '' : 'checked';
            if ($vista == 'legalizaciongasto') {
                $comentario = 'LEGALIZACIONES DE COPIAS OFICIO INCAUTACION';
            }
            elseif ($vista == 'desce_legalizaciongasto') {
                $comentario = 'LEGALIZACIONES DE COPIAS OFICIO DESCERRAJE';
            }
            elseif ($vista == 'levordencapt_legalizaciongasto') {
                $comentario = 'LEGALIZACIONES DE COPIAS OFICIO LEVANTAMIENTO ORDEN DE CAPTURA';
            }
            elseif ($vista == 'cartanotgasto') {
                $comentario = 'CARTA NOTARIAL';
            }
            else {
                $comentario = $_POST['archivo_nom'];
            }
        }

        /* INFORMACION DEL GASTO */
        if ($gasto_id > 0) {
            $where[0]['column_name'] = 'g.tb_gasto_id';
            $where[0]['param0'] = $gasto_id; //solo creditos garveh
            $where[0]['datatype'] = 'INT';

            $inner[0]['alias_columnasparaver'] = 'p.tb_proveedor_nom, p.tb_proveedor_doc';
            $inner[0]['tipo_union'] = 'LEFT';
            $inner[0]['tabla_alias'] = 'tb_proveedor p';
            $inner[0]['columna_enlace'] = 'g.tb_proveedor_id';
            $inner[0]['alias_columnaPK'] = 'p.tb_proveedor_id';

            $inner[1]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
            $inner[1]['tipo_union'] = 'LEFT';
            $inner[1]['tabla_alias'] = 'tb_moneda mon';
            $inner[1]['columna_enlace'] = 'g.tb_moneda_id';
            $inner[1]['alias_columnaPK'] = 'mon.tb_moneda_id';

            $inner[2]['alias_columnasparaver'] = 'cgv.tb_cliente_id, cgv.tb_credito_vehpla';
            $inner[2]['tipo_union'] = 'LEFT';
            $inner[2]['tabla_alias'] = 'tb_creditogarveh cgv';
            $inner[2]['columna_enlace'] = 'g.tb_credito_id';
            $inner[2]['alias_columnaPK'] = 'cgv.tb_credito_id';

            $inner[3]['alias_columnasparaver'] = 'cli.tb_cliente_nom';
            $inner[3]['tipo_union'] = 'LEFT';
            $inner[3]['tabla_alias'] = 'tb_cliente cli';
            $inner[3]['columna_enlace'] = 'cgv.tb_cliente_id';
            $inner[3]['alias_columnaPK'] = 'cli.tb_cliente_id';

            $res = $oGasto->listar_todos($where, $inner, array());
            unset($where, $inner);
            if ($res['row_count'] > 0) {
                $gasto_reg = $res['data'][0];
                $gasto_prov_nom = $gasto_reg['tb_proveedor_doc'] . ' - ' . $gasto_reg['tb_proveedor_nom'];
                $gasto_moneda_nom = $gasto_reg['tb_moneda_nom'];
                $gasto_moneda_id = $gasto_reg['tb_moneda_id'];
                $gasto_pre_total = $gasto_reg['tb_gasto_ptl'];
                $moneda_id = $gasto_moneda_id; //necesario solo para el select de moneda para elegir moneda por default
                $fecha_gasto = fecha_mysql($gasto_reg['tb_gasto_fecreg']);

                $monto_pagado = 0;
                $falta_pagar = $gasto_pre_total;

                $result = $oEgreso->mostrar_egresos_pagogasto($gasto_id);
                if ($result['estado'] == 1) {
                    foreach ($result['data'] as $key => $egreso) {
                        if ($egreso['tb_moneda_id'] == $gasto_moneda_id) {
                            $monto_pagado += floatval($egreso['tb_egreso_imp']);
                        } else {
                            $fec_tc_compra = $egreso['tb_egreso_fec'];
                            if ($egreso['tb_gastopago_formapago'] == 2) { //egreso por caja/banco
                                $result1 = $oDeposito->listar_depositos_paragasto($egreso['tb_gastopago_numope']);
                                if ($result1['estado'] == 1) {
                                    $fec_tc_compra = $result1['data']['tb_deposito_fec'];
                                } else {
                                    $falta_tc_calculos = true;
                                    $mensaje = "ERROR, NO EXISTE DEPOSITO CON N° OPERACION '{$egreso['tb_gastopago_numope']}', PRESENTE EN EL PAGO ID {$egreso['tb_gastopago_id']}, DEL GASTO ID $gasto_id";
                                    break;
                                }
                                unset($result1);
                            }
                            $result1 = $oMonedacambio->consultar($fec_tc_compra);
                            if ($result1['estado'] == 1) {
                                $tc_compra = $result1['data']['tb_monedacambio_com'];
                                $monto_pagado += $gasto_moneda_id == 1 ? floatval($egreso['tb_egreso_imp'] * $tc_compra) : floatval($egreso['tb_egreso_imp'] / $tc_compra);
                            } else {
                                $falta_tc_calculos = true;
                                $mensaje = "HAY ERROR CON EL TIPO CAMBIO COMPRA DEL DIA " . mostrar_fecha($fec_tc_compra);
                                break;
                            }
                        }
                    }
                    $falta_pagar -= $monto_pagado;
                }
                unset($result);

                $gasto_pre_total = mostrar_moneda($gasto_pre_total);
                $monto_pagado = mostrar_moneda($monto_pagado);
                $falta_pagar = mostrar_moneda($falta_pagar);

                $result = $oMonedacambio->consultar($fecha_gasto);
                if ($result['estado'] == 1) {
                    $tc_compra_gasto = $result['data']['tb_monedacambio_com'];

                    $otra_moneda = $gasto_moneda_id < 2 ? 'US$' : 'S/.';
                    $falta_pagar_conversion = $gasto_moneda_id < 2 ? mostrar_moneda($falta_pagar / $tc_compra_gasto) : mostrar_moneda($falta_pagar * $tc_compra_gasto);
                } else {
                    $falta_tc_calculos = true;
                    $mensaje = "HAY ERROR CON EL TIPO CAMBIO COMPRA DEL DIA " . mostrar_fecha($fecha_gasto);
                }
                unset($result);
            }
            unset($res);
        }

        $empresa_pago = substr($_SESSION['empresa_nombre'], 7);
        if ($gastopago_id > 0) {
            //aqui hacer busqueda de gastopago registrado
            $where1[0]['column_name'] = 'gp.tb_gastopago_id';
            $where1[0]['param0'] = $gastopago_id; //solo creditos garveh
            $where1[0]['datatype'] = 'INT';

            $inner[0]['alias_columnasparaver'] = 'emp.tb_empresa_nomcom';
            $inner[0]['tipo_union'] = 'LEFT';
            $inner[0]['tabla_alias'] = 'tb_empresa emp';
            $inner[0]['columna_enlace'] = 'gp.tb_empresa_idfk';
            $inner[0]['alias_columnaPK'] = 'emp.tb_empresa_id';

            $inner[1]['alias_columnasparaver'] = 'mc.tb_monedacambio_com';
            $inner[1]['tipo_union'] = 'LEFT';
            $inner[1]['tabla_alias'] = 'tb_monedacambio mc';
            $inner[1]['columna_enlace'] = 'gp.tb_gastopago_fecha';
            $inner[1]['alias_columnaPK'] = 'mc.tb_monedacambio_fec';

            $inner[2]['alias_columnasparaver'] = 'eg.tb_cuenta_id, eg.tb_subcuenta_id, eg.tb_egreso_det';
            $inner[2]['tipo_union'] = 'LEFT';
            $inner[2]['tabla_alias'] = 'tb_egreso eg';
            $inner[2]['columna_enlace'] = 'gp.tb_egreso_idfk';
            $inner[2]['alias_columnaPK'] = 'eg.tb_egreso_id';

            $res = $oGastopago->listar_todos($where1, $inner, array());
            if ($res['row_count'] > 0) {
                $gastopago = $res['data'][0];
                if ($action != 'modificar') {
                    $empresa_pago = substr($gastopago['tb_empresa_nomcom'], 7);
                } else {
                    $rojo = boolval($empresa_pago != substr($gastopago['tb_empresa_nomcom'], 7));
                }
                $tccompra_hoy = $gastopago['tb_monedacambio_com'];
                $cuenta_id = $gastopago['tb_cuenta_id'];
                $subcuenta_id = $gastopago['tb_subcuenta_id'];
                $moneda_id = $gastopago['tb_moneda_idfk'];
                $monto_registrado = mostrar_moneda($gastopago['tb_gastopago_monto']);
                $fec = $gastopago['tb_gastopago_fecha'];
                $forma_pago[0] = $gastopago['tb_gastopago_formapago'] == 1 ? 'selected' : '';
                $forma_pago[1] = $gastopago['tb_gastopago_formapago'] == 2 ? 'selected' : '';
                $numope = $gastopago['tb_gastopago_numope'];
                $ingreso_id = $gastopago['tb_ingreso_idfk'];
                $emite_doc_sunat = $gastopago['tb_gastopago_emitedocsunat'] == 1 ? 'checked' : '';
                $nro_doc = $gastopago['tb_gastopago_documentossunat'];
                $comentario = $gastopago['tb_gastopago_obs'];
                if (!empty($gastopago['tb_ingreso_idfk'])){
                    $gastopago['tb_ingreso_det'] = $oIngreso->mostrarUno($gastopago['tb_ingreso_idfk'])['data']['tb_ingreso_det'];
                }
            }
            unset($res);
        }
    }
} else {
    $mensaje = 'No has aperturado o ya está cerrada la caja de este día: ' . date('d-m-Y') . ' ' . ' para la sede de ' . substr($_SESSION['empresa_nombre'], 7);
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_gastopago_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0 || $caja_aperturada == 0 || $tccompra_hoy < 1 || $falta_tc_calculos) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_pago" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
                        <input type="hidden" name="hdd_gasto_reg" id="hdd_gasto_reg" value='<?php echo json_encode($gasto_reg); ?>'>
                        <input type="hidden" name="hdd_gastopago_reg" id="hdd_gastopago_reg" value='<?php echo json_encode($gastopago); ?>'>
                        <input type="hidden" name="hdd_gasto_moneda_id" id="hdd_gasto_moneda_id" value="<?php echo $gasto_moneda_id; ?>">
                        <input type="hidden" name="hdd_edita_solodoc" id="hdd_edita_solodoc" value="<?php echo $edita_solo_doc;?>">
                        <input type="hidden" name="hdd_ingreso_idfk" id="hdd_ingreso_idfk" value="<?php echo $ingreso_id;?>">
                        <input type="hidden" name="hdd_gastopago_vista" id="hdd_gastopago_vista" value="<?php echo $vista;?>">
                        <input type="hidden" name="hdd_gastopago_credito_id" id="hdd_gastopago_credito_id" value="<?php echo $credito_id;?>">
                        <input type="hidden" name="hdd_gastopago_ejecucion_id" id="hdd_gastopago_ejecucion_id" value="<?php echo $ejecucion_id;?>">
                        <input type="hidden" name="hdd_gastopago_ejecucionfase_id" id="hdd_gastopago_ejecucionfase_id" value="<?php echo $_POST['ejecucionfase_id'];?>">
                        <input type="hidden" name="hdd_gastopago_tabla_id" id="hdd_gastopago_tabla_id" value="<?php echo $_POST['tabla_id'];?>">

                        <div class="box box-primary shadow">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_proveedor">Proveedor:</label>
                                    <input type="text" name="txt_proveedor" id="txt_proveedor" class="form-control input-sm disabled" value="<?php echo $gasto_prov_nom; ?>">
                                </div>
                            </div>

                            <?php if (!(in_array($vista, ['legalizaciongasto', 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', 'cartanotgasto', 'ejecucionfasefile']) && $usuario_action == 'I')) { ?>
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="txt_gasto_sede" style="<?php echo $rojo ? 'color: red;' : ''; ?>">Sede egreso:</label>
                                        <input type="text" name="txt_gasto_sede" id="txt_gasto_sede" class="form-control input-sm disabled" style="font-size: 11px;" value="<?php echo $empresa_pago; ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_gasto_tc">TC Compra IPDN:</label>
                                        <input type="text" name="txt_gasto_tc" id="txt_gasto_tc" class="form-control input-sm disabled" value="<?php echo $tccompra_hoy; ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_monto_total">Monto total:</label>
                                        <input type="text" name="txt_monto_total" id="txt_monto_total" class="form-control input-sm disabled" value="<?php echo "$gasto_moneda_nom $gasto_pre_total"; ?>">
                                    </div>
                                </div>
                            
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="txt_monto_pagado">Monto pagado:</label>
                                        <input type="text" name="txt_monto_pagado" id="txt_monto_pagado" class="form-control input-sm disabled" value="<?php echo "$gasto_moneda_nom $monto_pagado"; ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_saldopagar_orig">Falta pagar (<?php echo $gasto_moneda_nom; ?>):</label>
                                        <input type="text" name="txt_saldopagar_orig" id="txt_saldopagar_orig" class="form-control input-sm disabled" value="<?php echo "$gasto_moneda_nom $falta_pagar"; ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="txt_saldopagar_conv">Falta pagar (<?php echo $otra_moneda; ?>):</label>
                                        <input type="text" name="txt_saldopagar_conv" id="txt_saldopagar_conv" class="form-control input-sm disabled" value="<?php echo "$otra_moneda $falta_pagar_conversion"; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <hr>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="cbo_cuenta_id">Cuenta:</label>
                                    <select name="cbo_cuenta_id" id="cbo_cuenta_id" class="form-control input-sm">
                                        <?php require_once '../cuenta/cuenta_select.php'; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="cbo_subcuenta_id">Subcuenta:</label>
                                    <select name="cbo_subcuenta_id" id="cbo_subcuenta_id" class="form-control input-sm">
                                        <?php require_once '../subcuenta/subcuenta_select.php'; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="two-fields">
                                        <label for="txt_monto_pagar">Monto pagar:</label>
                                        <div class="input-group">
                                            <select name="cbo_moneda_pagar" id="cbo_moneda_pagar" class="form-control input-sm mayus" style="width: 38%; padding: 5px 8px;">
                                                <?php require_once '../moneda/moneda_select.php'; ?>
                                            </select>
                                            <input type="text" name="txt_monto_pagar" id="txt_monto_pagar" class="form-control input-sm moneda" style="width: 62%; padding: 5px 5px;" placeholder="0.00" value="<?php echo $gastopago_id > 0 ? $monto_registrado : '';//$falta_pagar ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="txt_fecha_gasto">Fecha de pago:</label>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control input-sm disabled" placeholder="DD-MM-AAAA" name="txt_fecha_gasto" id="txt_fecha_gasto" value="<?php echo mostrar_fecha($fec); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="cbo_forma_egreso">Forma de egreso:</label>
                                    <select name="cbo_forma_egreso" id="cbo_forma_egreso" class="form-control input-sm mayus">
                                        <option value="1" <?php echo $forma_pago[0] ?>>Egreso caja Efectivo</option>
                                        <option value="2" <?php echo $forma_pago[1] ?>>Ingreso/Egreso Banco</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="num_ope">
                                        <label for="txt_numope">N° Operación:</label>
                                        <input type="text" name="txt_numope" id="txt_numope" class="form-control input-sm mayus" placeholder="Num. operación" value="<?php echo $numope; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding: 6px 0px;">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="che_emite_docsunat" name="che_emite_docsunat" class="flat-green" value="1" <?php echo $emite_doc_sunat; ?>><b>&nbsp; Emite comprobante</b>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="emite_doc_sunat">
                                        <label for="txt_nro_documento">N° Documento:</label>
                                        <input type="text" name="txt_nro_documento" id="txt_nro_documento" class="form-control input-sm mayus" placeholder="E001-00000001" value="<?php echo $nro_doc;?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_gastopago_comen">Comentario:</label>
                                    <textarea class="form-control mayus input-sm input-shadow" name="txt_gastopago_comen" id="txt_gastopago_comen" rows="3" cols="85" placeholder="COMENTARIO"><?php echo $comentario; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="pago_mensaje" style="display: none;">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M' || $usuario_action == 'M_doc') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_pago">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_pago">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo "vista/gastopago/gastopago_form.js?ver=007558452"; ?>"></script>
