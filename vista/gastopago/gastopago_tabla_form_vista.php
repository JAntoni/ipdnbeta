<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');
require_once('../gastopago/Gastopago.class.php');
$oGastopago = new Gastopago();
$fecha_hoy = date('Y-m-d');

$gasto_id = intval($_POST['gasto_id']);

$where[0]['column_name'] = 'gp.tb_gasto_idfk';
$where[0]['param0'] = $gasto_id; //solo creditos garveh
$where[0]['datatype'] = 'INT';
$where[1]['conector'] = 'AND';
$where[1]['column_name'] = 'gp.tb_gastopago_xac';
$where[1]['param1'] = 1; //solo creditos garveh
$where[1]['datatype'] = 'INT';
$inner[0]['alias_columnasparaver'] = 'emp.tb_empresa_nomcom';
$inner[0]['tipo_union'] = 'LEFT';
$inner[0]['tabla_alias'] = 'tb_empresa emp';
$inner[0]['columna_enlace'] = 'gp.tb_empresa_idfk';
$inner[0]['alias_columnaPK'] = 'emp.tb_empresa_id';
$inner[1]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
$inner[1]['tipo_union'] = 'INNER';
$inner[1]['tabla_alias'] = 'tb_moneda mon';
$inner[1]['columna_enlace'] = 'gp.tb_moneda_idfk';
$inner[1]['alias_columnaPK'] = 'mon.tb_moneda_id';
$inner[2]['alias_columnasparaver'] = "CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as usuario_nom";
$inner[2]['tipo_union'] = 'INNER';
$inner[2]['tabla_alias'] = 'tb_usuario us';
$inner[2]['columna_enlace'] = 'gp.tb_gastopago_usureg';
$inner[2]['alias_columnaPK'] = 'us.tb_usuario_id';

$otros['orden']['column_name'] = 'gp.tb_gastopago_id';
$otros['orden']['value'] = 'ASC';

$res = $oGastopago->listar_todos($where, $inner, $otros);

$lista = '';

if ($res['row_count'] > 0) {
    foreach ($res['data'] as $key => $gasto) {
        if($gasto['tb_gastopago_formapago'] == 1){
            $forma_pago = 'CAJA EFECTIVO';
        } elseif ($gasto['tb_gastopago_formapago'] == 2) {
            $forma_pago = 'I/E POR BANCO | N° Operacion: '. $gasto['tb_gastopago_numope'];
        }
        if($gasto['tb_gastopago_emitedocsunat'] == 0){
            $factura = 'No emite documento';
        } else {
            $factura = empty($gasto['tb_gastopago_documentossunat']) ? 'Falta registrar factura' : $gasto['tb_gastopago_documentossunat'];
        }

        $lista .=
            '<tr id="tabla_cabecera_fila">
                <td id="tabla_fila">' . $gasto['tb_gastopago_id']. '</td>
                <td id="tabla_fila">' . mostrar_fecha($gasto['tb_gastopago_fecha']) . '</td>
                <td id="tabla_fila">' . $gasto['tb_moneda_nom'] . mostrar_moneda($gasto['tb_gastopago_monto']) . '</td>
                <td id="tabla_fila" align="left">' . $gasto['tb_empresa_nomcom'] . '</td>
                <td id="tabla_fila" align="left">' . $forma_pago . '</td>
                <td id="tabla_fila">' . $factura . '</td>
                <td id="tabla_fila" align="left">' . $gasto['usuario_nom'] . '</td>
                <td id="tabla_fila">'; //se abre la casilla -> condicional
        //
        $lista .=   '<button class="btn btn-info btn-xs" title="Leer" onclick="pagar(\'L\', '.$gasto_id.', '.$gasto['tb_gastopago_id'].')"><i class="fa fa-eye"></i></button>';
        //if ($fecha_hoy == $gasto['tb_gastopago_fecha'] && in_array(intval($_SESSION['usuariogrupo_id']), array(2, 6))) { //grupo de gerencia 2, y contabilidad 6
        if ($fecha_hoy == $gasto['tb_gastopago_fecha'] && in_array(intval($_SESSION['usuariogrupo_id']), array(2))) { //solo grupo gerencia 2
            if (empty($gasto['tb_gastopago_anexadolegal'])) {
                $lista .= ' <button class="btn btn-warning btn-xs" title="Editar" onclick="pagar(\'M\', '.$gasto_id.', '.$gasto['tb_gastopago_id'].')"><i class="fa fa-edit"></i></button>';
            } elseif ($factura == 'Falta registrar factura') {
                $lista .= ' <button class="btn btn-warning btn-xs" title="Editar documento" onclick="pagar(\'M_doc\', '.$gasto_id.', '.$gasto['tb_gastopago_id'].')"><i class="fa fa-edit"></i></button>';
            }
            $lista .= ' <button class="btn btn-danger btn-xs" title="Eliminar" onclick="pagar(\'E\', '.$gasto_id.', '.$gasto['tb_gastopago_id'].')"><i class="fa fa-trash"></i></button>';
        }
        elseif ($factura == 'Falta registrar factura') { //pendiente de pago entonces colocar 3 botones
            $lista .= ' <button class="btn btn-warning btn-xs" title="Editar documento" onclick="pagar(\'M_doc\', '.$gasto_id.', '.$gasto['tb_gastopago_id'].')"><i class="fa fa-edit"></i></button>';
        }
        $lista .= ' <div class="btn-group"> 
                        <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown" title="Opciones de Img"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a class="btn" href="javascript:void(0)" onclick="upload_form(\'I\', '.$gasto['tb_gastopago_id'].')"><b>Subir Img</b></a></li>
                            <li><a class="btn" href="javascript:void(0)" onclick="Abrir_imagen('.$gasto['tb_gastopago_id'].')"><b>Ver Img</b></a></li>
                        </ul>
                    </div>';
        $lista .= '</td>
            </tr>';
    }
}
?>
<table id="tbl_gastopagos" class="table table-hover" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" style="width: 2%;">Id</th>
            <th id="tabla_cabecera_fila" style="width: 6%;">Fecha</th>
            <th id="tabla_cabecera_fila" style="width: 8%;">Monto</th>
            <th id="tabla_cabecera_fila" style="width: 20%;">Sede egreso</th>
            <th id="tabla_cabecera_fila" style="width: 20%;">Forma de pago</th>
            <th id="tabla_cabecera_fila" style="width: 17%;">Fact. Proveedor</th>
            <th id="tabla_cabecera_fila" style="width: 17%;">Usuario registró</th>
            <th id="tabla_cabecera_fila" style="width: 10%;">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $lista;?>
    </tbody>
</table>