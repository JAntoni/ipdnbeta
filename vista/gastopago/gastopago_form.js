$(document).ready(function () {
    disabled($('#form_pago').find('.disabled'));
    if($('#action').val() === 'M_doc' || $('#action').val() === 'leer' || $('#action').val() === 'eliminar'){
        disabled($('#form_pago').find("button[class*='btn-sm'], select, input, textarea").not('#txt_nro_documento, #che_emite_docsunat'));
        if($('#action').val() == 'leer' || $('#action').val() === 'eliminar'){
            disabled($('#txt_nro_documento, #che_emite_docsunat'));
            if ($('#action').val() == 'eliminar') {
                $('#pago_mensaje').removeClass('callout-info').addClass('callout-warning');
                mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este pago?</h4>';
                $('#pago_mensaje').html(mens);
                $('#pago_mensaje').show();
            }
        }
    }
    $('.select2').select2({
		dropdownParent: $('#modal_gastopagar_form') //para que se pueda escribir y buscar en el select. Importante copiar css del index
	});

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: `icheckbox_flat-green`,
        radioClass: `iradio_flat-green`
    });

    $('.moneda').focus(function(){
        formato_moneda(this);
    });

    $('#txt_monto_pagar').blur(function () {
        var vista = $('#hdd_gastopago_vista').val();
        if (!($.inArray(vista, ["legalizaciongasto", 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', "cartanotgasto", "ejecucionfasefile"]) > -1)) {
            verificar_montopagar();
        }
    });

    $('#cbo_cuenta_id').change(function () {
        SubCuenta_select_form($('#cbo_cuenta_id').val());
    });

    $('#txt_nro_documento').blur(function(){
        var serie = $.trim($(this).val().split("-")[0]);
        var numer = $.trim($(this).val().split("-")[1]);
        if ($(this).val() !== '') {
            if ($(this).val().indexOf("-") <= -1) {
                swal_error("Verifique", "Formato de Factura debe incluir un guión", 10000);
                return;
            }
            else if (serie.length > 4 || serie.length < 2  || numer.length > 8 || numer.length < 1) {
                swal_error("Verifique", "Cantidad de caracteres en la serie y/o numero", 10000);
                return;
            }
            else if (!solo_letras(serie.substring(0, 1))) {
                swal_error("Verifique", "Formato de Factura, la serie debe incluir una letra al inicio", 10000);
                return;
            }
            else if (!solo_numeros(serie.substring(serie.length - 1))){
                swal_error("Verifique", "Formato de Factura, la serie debe incluir un número", 10000);
                return;
            }
            else if (!solo_numeros(numer)) {
                swal_error("Verifique", "Formato de Factura, el número de factura es incorrecto", 10000);
                return;
            }
            $('#txt_nro_documento').val(rellenar_serie_ceros(serie)+'-'+rellenar_num_ceros(numer, 8));
        }
    });

    if ($("#cbo_forma_egreso option:selected").val() == 2) {
        $("div .num_ope").show();
    } else {
        $("div .num_ope").hide();
    }

    if ($('#che_emite_docsunat').prop('checked')) {
        $("div .emite_doc_sunat").show();
    } else {
        $("div .emite_doc_sunat").hide();
    }

    $("#cbo_forma_egreso").change(function () {
        var forma_id = $("#cbo_forma_egreso option:selected").val();
        if (forma_id == "2") {
            $("div .num_ope").show(200);
            $('#txt_numope').focus();
        } else {
            $("div .num_ope").hide(200);
            $('#txt_numope').val('');
        }
    });

    $("#che_emite_docsunat").on('ifChanged', function () {
        if (this.checked) {
            $(".emite_doc_sunat").show(200);
            $('#txt_nro_documento').focus();
        }
        else {
            $(".emite_doc_sunat").hide(200);
            $("#txt_nro_documento").val(null);
        }
    });

    $('#txt_numope').blur(function () {
        if ($('#txt_numope').val() !== '') {
            verificar_numope();
        }
    });

    $('#txt_gastopago_comen').on('focus', function () {
        var vista = $('#hdd_gastopago_vista').val();
        if ($.inArray(vista, ["legalizaciongasto", 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', "cartanotgasto", "ejecucionfasefile"]) > -1 && $('#txt_nro_documento').val() != '') {
            $(this).val($(this).val() + ' SEGÚN FT '+ $('#txt_nro_documento').val());
        }
    });

    $('#form_pago').validate({
        submitHandler: function () {
            var vista = $('#hdd_gastopago_vista').val();
            if(!($.inArray(vista, ["legalizaciongasto", 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', "cartanotgasto", "ejecucionfasefile"]) > -1) && verificar_montopagar() > 0){
                return;
            }
            $.ajax({
                type: "POST",
                url: VISTA_URL + "gastopago/gastopago_controller.php",
                async: true,
                dataType: "json",
                data: serializar_form(),
                beforeSend: function () {
                    $('#pago_mensaje').show(400);
                    $('#btn_guardar_pago').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) == 1) {
                        $('#pago_mensaje').removeClass('callout-info').addClass('callout-success');
                        $('#pago_mensaje').html(`<h4>${data.mensaje}</h4>`);
                        if ($.inArray($('#action').val(), ['insertar', 'modificar', 'M_doc']) > -1) {
                            disabled($('#form_pago').find("button[class*='btn-sm'], select, input, textarea"));
                        }

                        setTimeout(function () {
                            if ($('#action').val() == 'modificar' || $('#action').val() == 'M_doc' || $('#action').val() == 'eliminar'){
                                completar_tabla_gastopagos();
                            } else if ($('#action').val() == 'insertar'){
                                if ($.inArray(vista, ["legalizaciongasto", 'desce_legalizaciongasto', 'levordencapt_legalizaciongasto', "cartanotgasto", "ejecucionfasefile"]) > -1) {
                                    actualizar_contenido_1fase($('#hdd_gastopago_ejecucionfase_id').val());
                                    if (vista == 'ejecucionfasefile') {
                                        completar_tabla_files();
                                    }
                                } else {
                                    completar_tabla_gastos();
                                }
                            }
                            $('#modal_gastopago_form').modal('hide');
                        }, 4000);
                    }
                    else {
                        $('#pago_mensaje').removeClass('callout-info').addClass('callout-warning');
                        $('#pago_mensaje').html('<h4>Alerta: ' + data.mensaje +'</h4>');
                        $('#btn_guardar_pago').prop('disabled', false);
                        console.log(data);
                    }
                },
                error: function (data) {
                    $('#pago_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#pago_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_monto_pagar: {
                required: true,
                min: '0.01'
            },
            txt_numope: {
                required: function () {
                    return $('#cbo_forma_egreso option:selected').val() == 2;
                }
            },
            txt_nro_documento: {
                required: function(){
                    return $('#action').val() === 'M_doc';
                }
            }
        },
        messages: {
            txt_monto_pagar: {
                required: 'Monto gasto*',
                min: 'Monto gasto*'
            },
            txt_numope: {
                required: "Ingrese N° Operación"
            },
            txt_nro_documento: {
                required: "Ingrese factura"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});

function SubCuenta_select_form(cuenta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "subcuenta/subcuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            cuenta_id: cuenta_id
        }),
        beforeSend: function () {
            $('#cbo_subcuenta_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cbo_subcuenta_id').html(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

function serializar_form() {
    var extra = ``;
    var elementos_disabled = $('#form_pago').find('input:disabled, select:disabled').removeAttr('disabled');
    var form_serializado = $('#form_pago').serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}

function verificar_numope() {
    const action_original = $('#action').val();
    $('#action').val('verificar_numope');
    var form_serializado = serializar_form();
    $('#action').val(action_original);
    $.ajax({
        type: "POST",
        url: VISTA_URL + "gastopago/gastopago_controller.php",
        async: true,
        dataType: "json",
        data: form_serializado,
        beforeSend: function () {
            $("#pago_mensaje").removeClass("callout-warning").addClass("callout-info");
            $("#pago_mensaje").html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
            $("#btn_guardar_pago").prop("disabled", true);
        },
        success: function (data) {
            $("#btn_guardar_pago").prop("disabled", false);
            $("#pago_mensaje").hide(400);
            if (data.estado != 1) {
                alertas_error('Verifique', data.mensaje, 'small');
            }
        }
    });
}

function verificar_montopagar(){
    if($('#action').val() == 'insertar'){
        var monedaid_selectedcbo = $('#cbo_moneda_pagar option:selected').val();
        var monedaid_gasto = $('#hdd_gasto_moneda_id').val();
        var monto_max_totype = monedaid_selectedcbo == monedaid_gasto ? $('#txt_saldopagar_orig').val() : $('#txt_saldopagar_conv').val();
        if (quitar_comas_miles($('#txt_monto_pagar').val()) > quitar_comas_miles(monto_max_totype.substring(4))) {
            alertas_error('Verifique', 'No puede pagar más del saldo restante', 'small');
            return 1;
        }
    }
}

function alertas_error(titulo, mensaje, tamaño){
    $.confirm({
        title: titulo,
        content: `<b>${mensaje}</b>`,
        type: 'orange',
        escapeKey: 'close',
        backgroundDismiss: true,
        columnClass: tamaño,
        buttons: {
            close: function () {
            }
        }
    });
}