<?php
require_once('../funciones/funciones.php');
$gasto_id = intval($_POST['gasto_id']);
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_gastopago_tabla" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Pagos del gasto</h4>
                <input type="hidden" name="hdd_gasto_id" id="hdd_gasto_id" value="<?php echo $gasto_id; ?>">
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="hdd_datatable_gastopagos_fil">

                    <div class="col-md-12">
                        <!-- tabla -->
                        <div id="div_html_tablagastopago" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="f1-buttons">
                    <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/gastopago/gastopago_tabla_form.js?ver=6184634210'; ?>"></script>