<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../gasto/Gasto.class.php');
$oGasto = new Gasto();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../deposito/Deposito.class.php');
$oDeposito = new Deposito();
require_once('../monedacambio/Monedacambio.class.php');
$oMonedaCambio = new Monedacambio();
require_once('../gastopago/Gastopago.class.php');
$oGastopago = new Gastopago();
require_once('../compracontadetalle/Compracontadetalle.class.php');
$oCCDetalle = new Compracontadetalle();
require_once '../ejecucion/Ejecucion.class.php';
$oEjecucion = new Ejecucion();
require_once '../ejecucionupload/Ejecucionfasefile.class.php';
$oEjecucionfasefile = new Ejecucionfasefile();
require_once '../demanda/Demanda.class.php';
$oDemanda = new Demanda();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$usuario_id = intval($_SESSION['usuario_id']);
$fecha_hora = date("d-m-Y h:i a");
$action = $_POST['action'];
$data['estado'] = 0;
$data['mensaje'] = "";

$gasto_reg				= (array) json_decode($_POST['hdd_gasto_reg']);
$gastopago_regorigin	= (array) json_decode($_POST['hdd_gastopago_reg']);

$deposito_registrado	= 0;
$moneda_nom				= array(1 => 'S/.', 2 => 'US$');

$valor_tc_compra_hoy	= floatval($_POST['txt_gasto_tc']);
$forma_pago				= intval($_POST['cbo_forma_egreso']);
$monedapago_id			= intval($_POST['cbo_moneda_pagar']);
$monto_pagar			= moneda_mysql($_POST['txt_monto_pagar']);

$gastopago_id			= $gastopago_regorigin['tb_gastopago_id'];
$obs					= trim(strtoupper($_POST['txt_gastopago_comen']));
$emite_doc_sunat		= intval($_POST['che_emite_docsunat']);
$ingreso_idfk			= $_POST['hdd_ingreso_idfk'];

if ($gastopago_id > 0) {
	$join[0]['tipo_union'] = 'INNER';
	$join[0]['tabla_alias'] = 'tb_gastopago gp';
	$join[0]['alias_columnasparaver'] = 'gp.*';
	$join[0]['columna_enlace'] = 'ccd.tb_gastopago_idfk';
	$join[0]['alias_columnaPK'] = 'gp.tb_gastopago_id';

	$where[0]['column_name'] = 'ccd.tb_compracontadetalle_xac';
	$where[0]['param0'] = 1;
	$where[0]['datatype'] = 'INT';

	$where[1]['conector'] = 'AND';
	$where[1]['column_name'] = 'ccd.tb_gastopago_idfk';
	$where[1]['param1'] = $gastopago_id;
	$where[1]['datatype'] = 'INT';

	$buscar_ccd = $oCCDetalle->mostrarUno($where, $join);

	if ($buscar_ccd['estado'] == 1) {
		$buscar_ccd = $buscar_ccd['data'];
		$esta_anexado_ccd = 1;
	}
}

if($forma_pago == 2){
	$num_operacion = trim($_POST['txt_numope']);
	$consulta_deposito = $oDeposito->listar_depositos_paragasto($num_operacion);
	$deposito_registrado = $consulta_deposito['estado'];
	$deposito = $consulta_deposito['data'];
	$tc_compra_deposito = $deposito['tb_deposito_fec'];
	unset($consulta_deposito);

 	$result = $oMonedaCambio->consultar($tc_compra_deposito);
	if($result['estado'] == 1){
		$tc_compra_deposito = $result['data']['tb_monedacambio_com'];
	} else {
		$deposito_registrado = 3;
		$data['mensaje'] .= "Verificar el tipo de cambio de la fecha ". mostrar_fecha($tc_compra_deposito);
	}
	unset($result);

	if($deposito_registrado == 0){
		$data['mensaje'] .=  'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor';
	} elseif ($deposito_registrado == 1) {
		$importe_ingresos_anteriores = 0;
		$dts = $oIngreso->lista_ingresos_num_operacion($num_operacion);

		if ($dts['estado'] == 1) {
			foreach ($dts['data'] as $key => $dt) {
				if ($dt['tb_ingreso_id'] != $gastopago_regorigin['tb_ingreso_idfk']) {
					$tb_ingreso_imp = moneda_mysql($dt['tb_ingreso_imp']);
					if ($monedapago_id != intval($dt['tb_moneda_id'])) {
						$importe_ingresos_anteriores += $monedapago_id == 1 ? floatval($tb_ingreso_imp * $tc_compra_deposito) : floatval($tb_ingreso_imp / $tc_compra_deposito);
					} else {
						$importe_ingresos_anteriores += $tb_ingreso_imp;
					}
				}
			}
		}
		unset($dts);

		$moneda_deposito = intval($deposito['tb_moneda_id']);
		$monto_deposito_orig = abs($deposito['tb_deposito_mon']);
		$monto_deposito = abs($deposito['tb_deposito_mon']);
		if($monedapago_id != $moneda_deposito){
			$monto_deposito = $monedapago_id == 1 ? floatval($monto_deposito * $tc_compra_deposito) : floatval($monto_deposito / $tc_compra_deposito);
		}
		$monto_puede_usar = moneda_mysql($monto_deposito - $importe_ingresos_anteriores);

		if ($monto_pagar > $monto_puede_usar) {
			$deposito_registrado = 2;
			$data['mensaje'] .= 'El monto ingresado es superior al monto disponible del depósito. No puede usar más de lo depositado. ' .
								'Monto ingresado: ' . $moneda_nom[$monedapago_id] . mostrar_moneda($monto_pagar) . ', monto disponible: ' . $moneda_nom[$monedapago_id] . mostrar_moneda($monto_puede_usar) . ', ';
			$data['mensaje'] .= 'TOTAL DEL DEPOSITO: ' . $moneda_nom[$moneda_deposito] . mostrar_moneda($monto_deposito_orig);
			$data['mensaje'] .= $monedapago_id != $moneda_deposito ? ' Ó ' . $moneda_nom[$monedapago_id] . mostrar_moneda($monto_deposito) : '';
		}
	}
}

if ($action == 'eliminar') {
	if($emite_doc_sunat == 1 && $esta_anexado_ccd == 1){
		$data['mensaje'] = "● No puede $action este pago, porque está anexado en el detalle numero {$buscar_ccd['tb_compracontadetalle_orden']} del documento id {$buscar_ccd['tb_compracontadoc_idfk']} - {$buscar_ccd['tb_gastopago_documentossunat']}";
		echo json_encode($data); exit();
	}
	if(!empty($ingreso_idfk)){
		//eliminar ingreso
		$oIngreso->ingreso_id = $gastopago_regorigin['tb_ingreso_idfk'];
		if($oIngreso->eliminar_2() > 0){
			$data['mensaje'] = "● Ingreso de caja eliminado correctamente. Ingreso id: $oIngreso->ingreso_id. <br>";
			unset($oIngreso->ingreso_id);
		} else {
			$data['mensaje'] = "● Hubo un error al $action el ingreso de caja con id $oIngreso->ingreso_id.";
			echo json_encode($data); exit();
		}
	}
	//eliminar egreso
	$oEgreso->egreso_id = $gastopago_regorigin['tb_egreso_idfk'];
	if ($oEgreso->eliminar_2()) {
		$data['mensaje'] .= "● Egreso eliminado correctamente. Egreso id: $oEgreso->egreso_id. <br>";
	} else {
		$data['mensaje'] .= "● Hubo un error al $action el egreso de caja con id $oEgreso->egreso_id.";
		echo json_encode($data); exit();
	}

	if ($oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_xac', 0, 'INT') == 1) {
		$data['estado'] = 1;
		$data['mensaje'] .= "● Se eliminó este pago";
		
		$gasto_id = intval($gasto_reg['tb_gasto_id']);
		$result = $oEgreso->mostrar_egresos_pagogasto($gasto_id);
        if ($result['estado'] != 1) {
			$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 1, 'INT');
		} else {
			$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 3, 'INT');
		}

		//buscar si el pago está en una ejecucion o ejecucionfasefile para limpiar la columna que corresponde al pago
			unset ($where, $inner);
			$where[0]['column_name'] = 'gp.tb_gastopago_id';
			$where[0]['param0'] = $gastopago_id;
			$where[0]['datatype'] = 'INT';
			
			$inner[0]['alias_columnasparaver'] = 'ej.tb_ejecucion_id';
			$inner[0]['tipo_union'] = 'LEFT';
			$inner[0]['tabla_alias'] = 'tb_ejecucion ej';
			$inner[0]['columna_enlace'] = 'gp.tb_gastopago_id';
			$inner[0]['alias_columnaPK'] = 'ej.tb_ejecucion_legalizacionpago_id';

			$inner[1]['alias_columnasparaver'] = 'ej1.tb_ejecucion_id as ejecucionid_2';
			$inner[1]['tipo_union'] = 'LEFT';
			$inner[1]['tabla_alias'] = 'tb_ejecucion ej1';
			$inner[1]['columna_enlace'] = 'gp.tb_gastopago_id';
			$inner[1]['alias_columnaPK'] = 'ej1.tb_ejecucion_cartanotpago_id';

			$inner[2]['alias_columnasparaver'] = 'ej2.tb_ejecucion_id as ejecucionid_3';
			$inner[2]['tipo_union'] = 'LEFT';
			$inner[2]['tabla_alias'] = 'tb_ejecucion ej2';
			$inner[2]['columna_enlace'] = 'gp.tb_gastopago_id';
			$inner[2]['alias_columnaPK'] = 'ej2.tb_ejecucion_oficiodescer_legalizacionpago_id';

			$inner[3]['alias_columnasparaver'] = 'ej3.tb_ejecucion_id as ejecucionid_4';
			$inner[3]['tipo_union'] = 'LEFT';
			$inner[3]['tabla_alias'] = 'tb_ejecucion ej3';
			$inner[3]['columna_enlace'] = 'gp.tb_gastopago_id';
			$inner[3]['alias_columnaPK'] = 'ej3.tb_ejecucion_oficiolevanincau_legalizacionpago_id';

			$inner[4]['alias_columnasparaver'] = 'eff.tb_ejecucionfasefile_id';
			$inner[4]['tipo_union'] = 'LEFT';
			$inner[4]['tabla_alias'] = 'tb_ejecucionfasefile eff';
			$inner[4]['columna_enlace'] = 'gp.tb_gastopago_id';
			$inner[4]['alias_columnaPK'] = 'eff.tb_gastopago_id';

			$busq_gastopago = $oGastopago->listar_todos($where, $inner, array())['data'][0];
			
			//OFICIO DE INCAUTACION
			$ejecucion_anexada_legalizacion_id = $busq_gastopago['tb_ejecucion_id'];
			if (!empty($ejecucion_anexada_legalizacion_id)) {
				$oEjecucion->modificar_campo($ejecucion_anexada_legalizacion_id, 'tb_ejecucion_legalizacionpago_id', null, 'INT');
			}

			//carta notarial
			$ejecucion_anexada_cartanot_id = $busq_gastopago['ejecucionid_2'];
			if (!empty($ejecucion_anexada_cartanot_id)) {
				$oEjecucion->modificar_campo($ejecucion_anexada_cartanot_id, 'tb_ejecucion_cartanotpago_id', null, 'INT');
			}
			
			//oficio descerraje
			$ejecucion_anexada_ofidesc_id = $busq_gastopago['ejecucionid_3'];
			if (!empty($ejecucion_anexada_ofidesc_id)) {
				$oEjecucion->modificar_campo($ejecucion_anexada_ofidesc_id, 'tb_ejecucion_oficiodescer_legalizacionpago_id', null, 'INT');
			}

			//oficio lev orden capt
			$ejecucion_anexada_ofilevordcapt_id = $busq_gastopago['ejecucionid_4'];
			if (!empty($ejecucion_anexada_ofilevordcapt_id)) {
				$oEjecucion->modificar_campo($ejecucion_anexada_ofilevordcapt_id, 'tb_ejecucion_oficiolevanincau_legalizacionpago_id', null, 'INT');
			}

			$ejecucionfasefile_anexada_id = $busq_gastopago['tb_ejecucionfasefile_id'];
			if (!empty($ejecucionfasefile_anexada_id)) {
				$oEjecucionfasefile->modificar_campo($ejecucionfasefile_anexada_id, 'tb_gastopago_id', null, 'INT');
			}
		//
	} else {
		$data['mensaje'] .= "● Hubo un error al $action este pago.";
	}
}
elseif ($action == 'verificar_numope') {
	$data['estado'] = $deposito_registrado;
}
else { // solo para modificar, modificar documento, insertar
	$vista_form = $_POST['hdd_gastopago_vista'];
	$ejecucion_id = $_POST['hdd_gastopago_ejecucion_id'];
	if ($forma_pago == 2 && $deposito_registrado != 1) {
		echo json_encode($data); exit();
	}
	
	if($emite_doc_sunat == 0){
		$documento = 'NO EMITE DOCUMENTO';
	} else {
		$documento = empty(trim($_POST['txt_nro_documento'])) ? '(PENDIENTE DE RECIBIR FACTURA)' : "El documento recibido fue: ".strtoupper(trim($_POST['txt_nro_documento']));
	}

	$monedagasto_id = intval($_POST['hdd_gasto_moneda_id']);
	$falta_pagar_orig = moneda_mysql(substr($_POST['txt_saldopagar_orig'], 4));
	$falta_pagar_conv = moneda_mysql(substr($_POST['txt_saldopagar_conv'], 4));
	$cuenta_id = intval($_POST['cbo_cuenta_id']);
	$subcuenta_id = intval($_POST['cbo_subcuenta_id']);
	$fecha_gasto = $_POST['txt_fecha_gasto'];
	$fact = strtoupper(trim($_POST['txt_nro_documento']));

	if ($action == 'insertar') {
		//espacio para registrar el gasto, previo al pagogasto
		if ($vista_form == 'legalizaciongasto' || $vista_form == 'desce_legalizaciongasto' || $vista_form == 'levordencapt_legalizaciongasto' || $vista_form == 'cartanotgasto' || $vista_form == 'ejecucionfasefile') {
			$monedagasto_id = $monedapago_id;
			$falta_pagar_orig = $monto_pagar;

			$oGasto->tb_gasto_cretip	= 3; //garveh
			$oGasto->tb_credito_id		= intval($_POST['hdd_gastopago_credito_id']);
			$oGasto->tb_gasto_tipgas	= 2; //servicio
			$oGasto->tb_proveedor_id	= 18; //ipdn
			$oGasto->tb_gasto_can		= 1; //cantidad siempre 1
			$oGasto->tb_moneda_id		= $monedagasto_id;
			$oGasto->tb_gasto_pvt		= $monto_pagar;
			$oGasto->tb_gasto_ptl		= $monto_pagar;
			$oGasto->tb_gasto_des		= $obs;
			$oGasto->tb_gasto_his		= "Datos de gasto ingresados por: <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b> | <b>$fecha_hora</b><br>";
			
			$res = $oGasto->insertar();
			if ($res['estado'] == 1) {
				$data['estado'] = 1;
				$data['mensaje'] .= '● '.$res['mensaje'] . ". ID del gasto: " . $res['nuevo'].'<br>';

				//CONSULTAR EL REGISTRO HECHO
				$where[0]['column_name'] = 'g.tb_gasto_id';
				$where[0]['param0'] = $res['nuevo']; //solo creditos garveh
				$where[0]['datatype'] = 'INT';

				$inner[0]['alias_columnasparaver'] = 'p.tb_proveedor_nom, p.tb_proveedor_doc';
				$inner[0]['tipo_union'] = 'LEFT';
				$inner[0]['tabla_alias'] = 'tb_proveedor p';
				$inner[0]['columna_enlace'] = 'g.tb_proveedor_id';
				$inner[0]['alias_columnaPK'] = 'p.tb_proveedor_id';

				$inner[1]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
				$inner[1]['tipo_union'] = 'LEFT';
				$inner[1]['tabla_alias'] = 'tb_moneda mon';
				$inner[1]['columna_enlace'] = 'g.tb_moneda_id';
				$inner[1]['alias_columnaPK'] = 'mon.tb_moneda_id';

				$inner[2]['alias_columnasparaver'] = 'cgv.tb_cliente_id, cgv.tb_credito_vehpla';
				$inner[2]['tipo_union'] = 'LEFT';
				$inner[2]['tabla_alias'] = 'tb_creditogarveh cgv';
				$inner[2]['columna_enlace'] = 'g.tb_credito_id';
				$inner[2]['alias_columnaPK'] = 'cgv.tb_credito_id';

				$inner[3]['alias_columnasparaver'] = 'cli.tb_cliente_nom';
				$inner[3]['tipo_union'] = 'LEFT';
				$inner[3]['tabla_alias'] = 'tb_cliente cli';
				$inner[3]['columna_enlace'] = 'cgv.tb_cliente_id';
				$inner[3]['alias_columnaPK'] = 'cli.tb_cliente_id';

				$res = $oGasto->listar_todos($where, $inner, array());
				unset($where, $inner);
				$gasto_reg = $res['data'][0];
				unset($res);
			} else {
				$data['estado'] = 0;
				$data['mensaje'] = "ERROR EN EL REGISTRO DEL GASTO";
				echo json_encode($data); exit();
			}
		}

		// DATOS PARA EL EGRESO
		$gasto_id = intval($gasto_reg['tb_gasto_id']);
		$prod_serv = intval($gasto_reg['tb_gasto_tipgas']) == 2 ? 'SERVICIO' : 'PRODUCTO';
		$proveedor_id = intval($gasto_reg['tb_proveedor_id']);
		$cliente = strtoupper(trim($gasto_reg['tb_cliente_nom']));
		$vehiculo = strtoupper(trim($gasto_reg['tb_credito_vehpla']));
		$gasto_desc = $gasto_reg['tb_gasto_des'] . ". Del Cliente: $cliente y vehiculo placa: $vehiculo. Comentario: [$obs]";

		$egr_det = "EGRESO CRÉDITO GARVEH para pagar el $prod_serv de $gasto_desc. $documento";
		//generar egreso
		$oEgreso->egreso_usureg	= $usuario_id;
		$oEgreso->egreso_fec	= fecha_mysql($fecha_gasto);
		$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS, unica opcion documento
		$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
		$oEgreso->egreso_det	= $egr_det;
		$oEgreso->moneda_id		= $monedapago_id;
		$oEgreso->egreso_tipcam = $monedapago_id == 2 ? $valor_tc_compra_hoy : 1;
		$oEgreso->egreso_imp	= $monto_pagar;
		$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
		$oEgreso->cuenta_id		= $cuenta_id;
		$oEgreso->subcuenta_id	= $subcuenta_id; //subcuenta de OTROS
		$oEgreso->proveedor_id = $proveedor_id;
		$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
		$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
		$oEgreso->caja_id = 1; // 1: CAJA
		$oEgreso->modulo_id = 10; //10 egreso para pago de gastos vehiculares
		$oEgreso->egreso_modide = $gasto_id; //tb_egreso_modide el id de tb_gasto
		$oEgreso->empresa_id = $_SESSION['empresa_id'];
		$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

		$result = $oEgreso->insertar();
		if ($result['estado'] > 0) {
			$egreso_id = $result['egreso_id'];
			$data['mensaje'] .= "● Egreso de CAJA registrado correctamente en sede {$_SESSION['empresa_nombre']}. Id egreso: $egreso_id<br>";
			unset($result);
			
			// en gasto actualizar historial
			$mensaje = "Se hizo un pago de {$moneda_nom[$monedapago_id]} ". mostrar_moneda($monto_pagar);
			if($monedagasto_id != $monedapago_id){
				$mensaje.= $monedapago_id == 2 ? " ó S/.".mostrar_moneda(floatval($monto_pagar * $valor_tc_compra_hoy)) : " ó US$".mostrar_moneda(floatval($monto_pagar / $valor_tc_compra_hoy));
			}
			$mensaje .= ", registrado por <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b>. ";
			$mensaje .= "Egreso id : $egreso_id | <b>$fecha_hora</b><br>";
			$mensaje .= "Se registró el egreso con el detalle: $egr_det | <b>$fecha_hora</b><br>";
			$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $gasto_reg['tb_gasto_his'].$mensaje, 'STR');
	
			//en gasto actualizar est dependiendo de si paga monto completo o paga una parte
			if (($monedapago_id == $monedagasto_id && $oEgreso->egreso_imp == $falta_pagar_orig) || ($monedapago_id != $monedagasto_id && $oEgreso->egreso_imp == $falta_pagar_conv)) {
				$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 2, 'INT');
				$data['mensaje'] .= "● Se registró el pago completo del gasto<br>";
			} else {
				$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 3, 'INT');
				$data['mensaje'] .= "● Se registró el pago parcial del gasto<br>";
			}
		} else {
			$data['mensaje'] = "● NO SE REGISTRÓ EL PAGO: error en el registro de egreso";
			echo json_encode($data); exit();
		}
		
		if ($forma_pago == 2) {
			if(is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BCP'))){
				$banco_id = 1;
				$subcuenta_ingreso_id = 147;
			} elseif (is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BBVA'))){
				$banco_id = 3;
				$subcuenta_ingreso_id = 148;
			} else {
				$banco_id = 4;
				$subcuenta_ingreso_id = 183;
			}

			//generar el ingreso
			$ing_det = "INGRESO DE CAJA POR BANCO, desde la siguente cuenta: [ {$deposito['tb_cuentadeposito_nom']} ], por EGRESO OE $egreso_id. El N° de operación fue: [ $num_operacion ]. ";
			$ing_det.= "Se usó [ {$moneda_nom[$monedapago_id]}". mostrar_moneda($monto_pagar) ." ] ";
			if ($monedapago_id != $moneda_deposito) {
				$ing_det.= $monedapago_id == 2 ? "ó [ S/.".mostrar_moneda(floatval($monto_pagar * $valor_tc_compra_hoy))." ] " : "ó [ US$".mostrar_moneda(floatval($monto_pagar / $valor_tc_compra_hoy))." ] ";
			}
			$ing_det.= "de un depósito total de [ {$deposito['tb_moneda_nom']}". mostrar_moneda($monto_deposito_orig) ." ], ";
			$ing_det.= "por concepto de $gasto_desc";
			//si falla se detiene y sale
			$oIngreso->ingreso_usureg = $usuario_id;
			$oIngreso->ingreso_fec = fecha_mysql($fecha_gasto);
			$oIngreso->documento_id = 8;//OI - OTROS INGRESOS
			$oIngreso->ingreso_numdoc = '';
			$oIngreso->ingreso_det = $ing_det;
			$oIngreso->ingreso_imp = $monto_pagar;
			$oIngreso->cuenta_id = 43;//INGRESO/EGRESO POR BANCO
			$oIngreso->subcuenta_id = $subcuenta_ingreso_id;
			$oIngreso->cliente_id = 1144;// id del cliente q hace el ingreso por banco: INVERSIONES Y PRESTAMOS DEL NORTE SAC
			$oIngreso->caja_id = 1;//CAJA
			$oIngreso->moneda_id = $monedapago_id;
			//valores que pueden ser cambiantes según requerimiento de ingreso
			$oIngreso->modulo_id = 0;
			$oIngreso->ingreso_modide = 0;
			$oIngreso->empresa_id = $_SESSION['empresa_id'];
			$oIngreso->ingreso_fecdep = $deposito['tb_deposito_fec'];
			$oIngreso->ingreso_numope = $num_operacion;
			$oIngreso->ingreso_mondep = $monto_pagar;
			$oIngreso->ingreso_comi = 0;
			$oIngreso->cuentadeposito_id = $deposito['tb_cuentadeposito_id'];
			$oIngreso->banco_id = $banco_id;
			$oIngreso->ingreso_ap = 0;//acuerdo de pago, 0
			$oIngreso->ingreso_detex = '';
			
			$result = $oIngreso->insertar();
			if ($result['estado'] != 1) {
				$data['mensaje'] .= "● Error en el registro de ingreso ficticio";
				echo json_encode($data); exit();
			} else {
				$ingreso_id = $result['ingreso_id'];
				$data['mensaje'] .= "● Ingreso ficticio de CAJA registrado correctamente en sede {$_SESSION['empresa_nombre']}. Id ingreso: $ingreso_id<br>";
			}
		}

		//ahora registrar el gastopago
		$oGastopago->tb_gastopago_monto = $monto_pagar;
		$oGastopago->tb_empresa_idfk = $_SESSION['empresa_id'];
		$oGastopago->tb_gasto_idfk = $gasto_id;
		$oGastopago->tb_gastopago_formapago = $forma_pago;
		if($forma_pago == 2){
			$oGastopago->tb_gastopago_numope = $num_operacion;
			$oGastopago->tb_ingreso_idfk = $ingreso_id;
		}
		$oGastopago->tb_gastopago_emitedocsunat = $emite_doc_sunat;
		if($emite_doc_sunat == 1){
			$oGastopago->tb_gastopago_documentossunat = strtoupper(trim($_POST['txt_nro_documento']));
		}
		$oGastopago->tb_gastopago_usureg = $usuario_id;
		$oGastopago->tb_gastopago_obs = $obs;
		$oGastopago->tb_moneda_idfk = $monedapago_id;
		$oGastopago->tb_egreso_idfk = $egreso_id;
		$oGastopago->tb_gastopago_fecha = date("Y-m-d");

		$res = $oGastopago->insertar();
		if($res['estado'] == 1){
			$data['estado'] = 1;
			$data['mensaje'] .= "● Pago de gasto registrado correctamente";

			if ($vista_form == 'legalizaciongasto') {
				$oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_legalizacionpago_id', $res['nuevo'], 'INT');
				$oGastopago->modificar_campo($res['nuevo'], 'tb_gastopago_anexadolegal', 1, 'INT');
			}
			if ($vista_form == 'desce_legalizaciongasto') {
				$oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_oficiodescer_legalizacionpago_id', $res['nuevo'], 'INT');
				$oGastopago->modificar_campo($res['nuevo'], 'tb_gastopago_anexadolegal', 3, 'INT');
			}
			if ($vista_form == 'levordencapt_legalizaciongasto') {
				$oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_oficiolevanincau_legalizacionpago_id', $res['nuevo'], 'INT');
				$oGastopago->modificar_campo($res['nuevo'], 'tb_gastopago_anexadolegal', 4, 'INT');
			}
			if ($vista_form == 'cartanotgasto') {
				$oEjecucion->modificar_campo($ejecucion_id, 'tb_ejecucion_cartanotpago_id', $res['nuevo'], 'INT');
				$oGastopago->modificar_campo($res['nuevo'], 'tb_gastopago_anexadolegal', 2, 'INT');
			}
			if ($vista_form == 'ejecucionfasefile') {
				$oEjecucionfasefile->modificar_campo($_POST['hdd_gastopago_tabla_id'], 'tb_gastopago_id', $res['nuevo'], 'INT');
				$oGastopago->modificar_campo($res['nuevo'], 'tb_gastopago_anexadolegal', 5, 'INT');
			}
		}else {
			$data['mensaje'] .= "● Error en el registro de PagoGasto";
		}
		unset($res);
	}
	elseif ($action == 'M_doc') {
		if($emite_doc_sunat == 1){
			$res = $oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_documentossunat', $fact, 'STR');
		} else {
			$res = $oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_emitedocsunat', 0, 'INT');
			$res2 = $oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_documentossunat', null, 'STR');
		}
		if ($res == 1){
			$data['estado'] = 1;
			$data['mensaje'] = "● Se actualizó el documento del pago";
			
			$egreso_id = $gastopago_regorigin['tb_egreso_idfk'];
			$egreso_det = $gastopago_regorigin['tb_egreso_det'];
			$texto_anterior = "(PENDIENTE DE RECIBIR FACTURA)";
			if($emite_doc_sunat == 1){
				$nuevo_detalle = strtoupper(str_replace($texto_anterior, "El documento recibido fue: $fact", $egreso_det));
			} else {
				$nuevo_detalle = str_replace($texto_anterior, "NO EMITE DOCUMENTO", $egreso_det);
			}
			$res1 = $oEgreso->modificar_campo($egreso_id, 'tb_egreso_det', $nuevo_detalle, 'STR');
			if ($res1 == 1) {
				$data['mensaje'] .= ".<br>● Se actualizó el detalle del egreso con id $egreso_id.";
			}
		} else {
			$data['mensaje'] = "● Error al actualizar el documento del gasto";
		}
		unset($res);
	}
	elseif ($action == 'modificar') {
		$igual[0] = boolval($_SESSION['empresa_id'] == $gastopago_regorigin['tb_empresa_idfk']);
		$igual[1] = boolval($cuenta_id == intval($gastopago_regorigin['tb_cuenta_id']));
		$igual[2] = boolval($subcuenta_id == intval($gastopago_regorigin['tb_subcuenta_id']));
		$igual[3] = boolval($monedapago_id == intval($gastopago_regorigin['tb_moneda_idfk']));
		$igual[4] = boolval($monto_pagar == $gastopago_regorigin['tb_gastopago_monto']);
		$igual[5] = boolval($forma_pago == intval($gastopago_regorigin['tb_gastopago_formapago']));
		$igual[6] = boolval($num_operacion == $gastopago_regorigin['tb_gastopago_numope']);
		$igual[7] = boolval($emite_doc_sunat == intval($gastopago_regorigin['tb_gastopago_emitedocsunat']));
		$igual[8] = boolval($fact == $gastopago_regorigin['tb_gastopago_documentossunat'] && $igual[7]);
		$igual[9] = boolval($obs == $gastopago_regorigin['tb_gastopago_obs']);

		$son_iguales = true;
		$i = 0;
		while ($i < count($igual)) {
			if (!$igual[$i]) {
				$son_iguales = false;
				$elim_ing_egre = $i < 7;
			}
			$i++;
		}

		if ($son_iguales) {
			$data['estado'] = 1;
			$data['mensaje'] = "No se realizó ninguna modificacion";
		} else {
			if ($elim_ing_egre) {
				if (!empty($gastopago_regorigin['tb_ingreso_idfk'])) {
					//eliminar el ingreso generado anteriormente
					$oIngreso->ingreso_id = $gastopago_regorigin['tb_ingreso_idfk'];
					if ($oIngreso->eliminar_2() > 0) {
						$data['mensaje'] = "● Ingreso id: $oIngreso->ingreso_id eliminado correctamente. <br>";
						unset($oIngreso->ingreso_id);
						$oGastopago->modificar_campo($gastopago_id, 'tb_ingreso_idfk', 0, 'INT');
					} else {
						$data['mensaje'] = "● Hubo un error al eliminar el ingreso de caja con id $oIngreso->ingreso_id.";
						echo json_encode($data); exit();
					}
				}
				//eliminar egreso generado anteriormente
				$oEgreso->egreso_id = $gastopago_regorigin['tb_egreso_idfk'];
				if ($oEgreso->eliminar_2()) {
					$data['mensaje'] .= "● Egreso id: $oEgreso->egreso_id eliminado correctamente.<br>";
					$oGastopago->modificar_campo($gastopago_id, 'tb_egreso_idfk', 0, 'INT');
				} else {
					$data['mensaje'] .= "● Hubo un error al eliminar el egreso de caja con id $oEgreso->egreso_id.";
					echo json_encode($data); exit();
				}

				// DATOS PARA GENERAR NUEVO EGRESO
				$gasto_id = intval($gasto_reg['tb_gasto_id']);
				$prod_serv = intval($gasto_reg['tb_gasto_tipgas']) == 2 ? 'SERVICIO' : 'PRODUCTO';
				$proveedor_id = intval($gasto_reg['tb_proveedor_id']);
				$cliente = strtoupper(trim($gasto_reg['tb_cliente_nom']));
				$vehiculo = strtoupper(trim($gasto_reg['tb_credito_vehpla']));
				$gasto_desc = $gasto_reg['tb_gasto_des'] . ". Del Cliente: $cliente y vehiculo placa: $vehiculo. Comentario: [$obs]";

				$egr_det = "CORRECION EN EGRESO CRÉDITO GARVEH para pagar el $prod_serv de $gasto_desc. $documento";
				
				$oEgreso->egreso_usureg	= $usuario_id;
				$oEgreso->egreso_fec	= fecha_mysql($fecha_gasto);
				$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS, unica opcion documento
				$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
				$oEgreso->egreso_det	= $egr_det;
				$oEgreso->moneda_id		= $monedapago_id;
				$oEgreso->egreso_tipcam = $monedapago_id == 2 ? $valor_tc_compra_hoy : 1;
				$oEgreso->egreso_imp	= $monto_pagar;
				$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
				$oEgreso->cuenta_id		= $cuenta_id;
				$oEgreso->subcuenta_id	= $subcuenta_id; //subcuenta de OTROS
				$oEgreso->proveedor_id = $proveedor_id;
				$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
				$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
				$oEgreso->caja_id = 1; // 1: CAJA
				$oEgreso->modulo_id = 10; //10 egreso para pago de gastos vehiculares
				$oEgreso->egreso_modide = $gasto_id; //tb_egreso_modide el id de tb_gasto
				$oEgreso->empresa_id = $_SESSION['empresa_id'];
				$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

				$result = $oEgreso->insertar();
				if ($result['estado'] > 0) {
					$egreso_id = $result['egreso_id'];
					$oGastopago->modificar_campo($gastopago_id, 'tb_egreso_idfk', $egreso_id, 'INT');
					$data['mensaje'] .= "● Egreso de CAJA registrado correctamente en sede {$_SESSION['empresa_nombre']}. Id egreso: $egreso_id<br>";
					unset($result);

					// en gasto actualizar historial
					$mensaje = "Se anuló el egreso id {$gastopago_regorigin['tb_egreso_idfk']} y se hizo un pago de {$moneda_nom[$monedapago_id]} " . mostrar_moneda($monto_pagar);
					if ($monedagasto_id != $monedapago_id) {
						$mensaje .= $monedapago_id == 2 ? " ó S/." . mostrar_moneda(floatval($monto_pagar * $valor_tc_compra_hoy)) : " ó US$" . mostrar_moneda(floatval($monto_pagar / $valor_tc_compra_hoy));
					}
					$mensaje .= ", registrado por <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b>. ";
					$mensaje .= "Egreso id : $egreso_id | <b>$fecha_hora</b><br>";
					$mensaje .= "Se registró el egreso con el detalle: $egr_det | <b>$fecha_hora</b><br>";
					$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $gasto_reg['tb_gasto_his'] . $mensaje, 'STR');

					//en gasto actualizar est dependiendo de si paga monto completo o paga una parte
					if (($monedapago_id == $monedagasto_id && $oEgreso->egreso_imp == $falta_pagar_orig) || ($monedapago_id != $monedagasto_id && $oEgreso->egreso_imp == $falta_pagar_conv)) {
						$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 2, 'INT');
					} else {
						$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 3, 'INT');
					}
				} else {
					$data['mensaje'] .= "● NO SE REGISTRÓ EL PAGO: error en el nuevo registro de egreso";
					echo json_encode($data);
					exit();
				}

				//generar nuevo egreso (ingreso)
				if ($forma_pago == 2) {
					if(is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BCP'))){
						$banco_id = 1;
						$subcuenta_ingreso_id = 147;
					} elseif (is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BBVA'))){
						$banco_id = 3;
						$subcuenta_ingreso_id = 148;
					} else {
						$banco_id = 4;
						$subcuenta_ingreso_id = 183;
					}
		
					//generar el ingreso
					$ing_det = "CORRECCION EN INGRESO DE CAJA POR BANCO, desde la siguente cuenta: [ {$deposito['tb_cuentadeposito_nom']} ], por EGRESO OE $egreso_id. El N° de operación fue: [ $num_operacion ]. ";
					$ing_det.= "Se usó [ {$moneda_nom[$monedapago_id]}". mostrar_moneda($monto_pagar) ." ] ";
					if ($monedapago_id != $moneda_deposito) {
						$ing_det.= $monedapago_id == 2 ? "ó [ S/.".mostrar_moneda(floatval($monto_pagar * $valor_tc_compra_hoy))." ] " : "ó [ US$".mostrar_moneda(floatval($monto_pagar / $valor_tc_compra_hoy))." ] ";
					}
					$ing_det.= "de un depósito total de [ {$deposito['tb_moneda_nom']}". mostrar_moneda($monto_deposito_orig) ." ], ";
					$ing_det.= "por concepto de $gasto_desc";
					//si falla se detiene y sale
					$oIngreso->ingreso_usureg = $usuario_id;
					$oIngreso->ingreso_fec = fecha_mysql($fecha_gasto);
					$oIngreso->documento_id = 8;//OI - OTROS INGRESOS
					$oIngreso->ingreso_numdoc = '';
					$oIngreso->ingreso_det = $ing_det;
					$oIngreso->ingreso_imp = $monto_pagar;
					$oIngreso->cuenta_id = 43;//INGRESO/EGRESO POR BANCO
					$oIngreso->subcuenta_id = $subcuenta_ingreso_id;
					$oIngreso->cliente_id = 1144;// id del cliente q hace el ingreso por banco: INVERSIONES Y PRESTAMOS DEL NORTE SAC
					$oIngreso->caja_id = 1;//CAJA
					$oIngreso->moneda_id = $monedapago_id;
					//valores que pueden ser cambiantes según requerimiento de ingreso
					$oIngreso->modulo_id = 0;
					$oIngreso->ingreso_modide = 0;
					$oIngreso->empresa_id = $_SESSION['empresa_id'];
					$oIngreso->ingreso_fecdep = $deposito['tb_deposito_fec'];
					$oIngreso->ingreso_numope = $num_operacion;
					$oIngreso->ingreso_mondep = $monto_pagar;
					$oIngreso->ingreso_comi = 0;
					$oIngreso->cuentadeposito_id = $deposito['tb_cuentadeposito_id'];
					$oIngreso->banco_id = $banco_id;
					$oIngreso->ingreso_ap = 0;//acuerdo de pago, 0
					$oIngreso->ingreso_detex = '';
					
					$result = $oIngreso->insertar();
					if ($result['estado'] != 1) {
						$data['mensaje'] .= "● Error en el registro de ingreso ficticio";
						echo json_encode($data); exit();
					} else {
						$ingreso_id = $result['ingreso_id'];
						$data['mensaje'] .= "● Ingreso ficticio de CAJA registrado correctamente en sede {$_SESSION['empresa_nombre']}. Id ingreso: $ingreso_id<br>";
						$oGastopago->modificar_campo($gastopago_id, 'tb_ingreso_idfk', $ingreso_id, 'INT');
					}
				}
			}

			if (!$igual[0]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_empresa_idfk', $_SESSION['empresa_id'], 'INT') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 0';
					echo json_encode($data); exit();
				}
			}
			if (!$igual[3]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_moneda_idfk', $monedapago_id, 'INT') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 3';
					echo json_encode($data); exit();
				}
			}
			if (!$igual[4]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_monto', $monto_pagar, 'STR') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 4';
					echo json_encode($data); exit();
				}
			}
			if (!$igual[5]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_formapago', $forma_pago, 'INT') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 5';
					echo json_encode($data); exit();
				}
			}
			if (!$igual[6]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_numope', $num_operacion, 'STR') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 6';
					echo json_encode($data); exit();
				}
			}
			if (!$igual[7]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_emitedocsunat', $emite_doc_sunat, 'INT') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 7';
					echo json_encode($data); exit();
				}
			}
			if (!$igual[8]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_documentossunat', $fact, 'STR') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 8';
					echo json_encode($data); exit();
				} else {
					$egreso_det = $gastopago_regorigin['tb_egreso_det'];
					if (!empty($gastopago_regorigin['tb_gastopago_emitedocsunat'])){
						$texto_anterior = "(PENDIENTE DE RECIBIR FACTURA)";
						if (!empty($gastopago_regorigin['tb_gastopago_documentossunat'])) {
							$texto_anterior = "El documento recibido fue: ".$gastopago_regorigin['tb_gastopago_documentossunat'];
						}
					} else {
						$texto_anterior = "NO EMITE DOCUMENTO";
					}
					$nuevo_detalle = str_replace($texto_anterior, $documento, $egreso_det);

					$oEgreso->modificar_campo($gastopago_regorigin['tb_egreso_idfk'], 'tb_egreso_det', $nuevo_detalle, 'STR');
				}
			}
			if (!$igual[9]){
				if ($oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_obs', $obs, 'STR') != 1) {
					$data['mensaje'] .= 'Error al modificar pago. 9';
					echo json_encode($data); exit();
				} elseif (!$elim_ing_egre) {
					$egreso_det = $gastopago_regorigin['tb_egreso_det'];
					$texto_anterior = $gastopago_regorigin['tb_gastopago_obs'];
					$nuevo_detalle = str_replace($texto_anterior, $obs, $egreso_det);
					$oEgreso->modificar_campo($gastopago_regorigin['tb_egreso_idfk'], 'tb_egreso_det', $nuevo_detalle, 'STR');

					if (!empty($gastopago_regorigin['tb_ingreso_idfk'])) {
						//eliminar el ingreso generado anteriormente
						$ingreso_det = $gastopago_regorigin['tb_ingreso_det'];
						$nuevo_detalle = str_replace($texto_anterior, $obs, $ingreso_det);
						$oIngreso->modificar_campo($gastopago_regorigin['tb_ingreso_idfk'], 'tb_ingreso_det', $nuevo_detalle, 'STR');
					}
				}
			}

			$data['estado'] = 1;
			$data['mensaje'] .= '● Pago modificado correctamente';
		}
	}
}

echo json_encode($data);
