<?php
require_once('../gastopago/Gastopago.class.php');
$oGastopago = new Gastopago();

$gasto_id = empty($gasto_id) ? intval($_POST['gasto_id']) : intval($gasto_id);
if(empty($_POST["gastopago_id"]) && empty($gastopago_id)){
	$gastopago_id = 0;
} elseif (!empty($gastopago_id)){
	$gastopago_id = intval($gastopago_id);
} else {
	$gastopago_id = intval($_POST["gastopago_id"]);
}

$option = '';

$parametros[0]['column_name'] = 'tb_gastopago_xac';
$parametros[0]["param0"] = 1;
$parametros[0]['datatype'] = 'INT';

$i = 1;
if(!empty($gasto_id)){
	$parametros[$i]["conector"] = 'AND';
	$parametros[$i]['column_name'] = 'tb_gasto_idfk';
	$parametros[$i]["param$i"] = $gasto_id;
	$parametros[$i]['datatype'] = 'INT';
	$i++;
}
if($_POST['vista'] == 'compracontadoc_form'){
	if ($_POST['action'] != 'L') {
		$parametros[$i]["conector"] = 'AND';
		$parametros[$i]["param$i"] = "(tb_gastopago_emitedocsunat = 0 OR (tb_gastopago_emitedocsunat = 1 AND (tb_gastopago_documentossunat <> '' AND tb_gastopago_documentossunat IS NOT NULL)))";
		$i++;
		$parametros[$i]["conector"] = 'AND';
		$parametros[$i]['column_name'] = 'tb_gastopago_anexado';
		$parametros[$i]["param$i"] = 0;
		$parametros[$i]['datatype'] = 'INT';
		$i++;
	}
}

$inner[0]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
$inner[0]['tipo_union'] = 'INNER';
$inner[0]['tabla_alias'] = 'tb_moneda mon';
$inner[0]['columna_enlace'] = 'gp.tb_moneda_idfk';
$inner[0]['alias_columnaPK'] = 'mon.tb_moneda_id';

$otros["orden"]["column_name"] = 'tb_gastopago_fecreg';
$otros["orden"]["value"] = 'DESC';

//PRIMER NIVEL
$result = $oGastopago->listar_todos($parametros, $inner, $otros);
echo $result['sql'];
if ($result['estado'] == 1) {
	$option = '<option value="0" style="font-size: 10px">..SELECCIONE..</option>';
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($gastopago_id == $value['tb_gastopago_id'])
			$selected = 'selected';
		
		if($value['tb_gastopago_formapago'] == 1){
			$forma_pago = 'CAJA EFECTIVO';
		} elseif ($value['tb_gastopago_formapago'] == 2) {
			$forma_pago = 'I/E POR BANCO | N° Operacion: '. $value['tb_gastopago_numope'];
		}
		//los valores de este option son usados en compracontadoc_elegirgasto.js, no mover
		$option .= '<option value="' . $value['tb_gastopago_id'] . '" style="font-size: 10px" ' . $selected . '>' . $value['tb_moneda_nom'].$value['tb_gastopago_monto'] ." | Pago id: {$value['tb_gastopago_id']} | Egreso por $forma_pago".'</option>';
	}
} /* else {
	$option = '<option value="0" style="font-size: 10px" class="disabled">No hay facturas anexadas a este gasto</option>';
} */
$result = NULL;

//FIN PRIMER NIVEL
echo $option;
?>