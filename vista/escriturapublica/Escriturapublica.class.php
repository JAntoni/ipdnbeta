<?php
if (defined('APP_URL'))
    require_once APP_URL . 'datos/conexion.php';
else
    require_once '../../datos/conexion.php';
//
class Escriturapublica extends Conexion {
    public $id;
    public $credito_id;
    public $cliente_ids;
    public $num;
    public $kardex;
    public $fecha;
    public $clientedir;
    public $clienteubig;
    public $clausulasejecucion;//tb_escriturapublica_clausulasejecucion
    public $ipdncargorepres;
    public $persona_id;
    public $clienterepre;
    public $fechasiguales;
    public $fechaconcfirmas;
    public $valorgravamen;
    public $tipocambio;
    public $fecreg;
    public $usureg;
    public $fecmod;
    public $usumod;
    public $xac;
    public $clausulasrepre;//tb_escriturapublica_clausulasrepre

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_escriturapublica
                    (
                        tb_credito_id,
                        tb_escriturapublica_num,
                        tb_escriturapublica_kardex,
                        tb_escriturapublica_fecha,
                        tb_escriturapublica_clientedir,
                        tb_escriturapublica_clienteubig,
                        tb_escriturapublica_clausulasejecucion,
                        tb_escriturapublica_ipdncargorepres,
                        tb_persona_id,
                        tb_escriturapublica_clienterepre,
                        tb_escriturapublica_fechasiguales,
                        tb_escriturapublica_fechaconcfirmas,
                        tb_escriturapublica_valorgravamen,
                        tb_escriturapublica_tipocambio,
                        tb_escriturapublica_usureg,
                        tb_escriturapublica_usumod,
                        tb_cliente_ids,
                        tb_escriturapublica_clausulasrepre
                    )
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5,
                        :param6,
                        :param7,
                        :param8,
                        :param9,
                        :param10,
                        :param11,
                        :param12,
                        :param13,
                        :param14,
                        :param15,
                        :param16,
                        :param17
                    )";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->num, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->kardex, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->fecha, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->clientedir, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->clienteubig, PDO::PARAM_STR);
            $sentencia->bindParam(':param6', $this->clausulasejecucion, PDO::PARAM_STR);
            $sentencia->bindParam(':param7', $this->ipdncargorepres, PDO::PARAM_INT);
            $sentencia->bindParam(':param8', $this->persona_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param9', $this->clienterepre, PDO::PARAM_INT);
            $sentencia->bindParam(':param10', $this->fechasiguales, PDO::PARAM_INT);
            $sentencia->bindParam(':param11', $this->fechaconcfirmas, PDO::PARAM_STR);
            $sentencia->bindParam(':param12', $this->valorgravamen, PDO::PARAM_STR);
            $sentencia->bindParam(':param13', $this->tipocambio, PDO::PARAM_STR);
            $sentencia->bindParam(':param14', $this->usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param15', $this->usumod, PDO::PARAM_INT);
            $sentencia->bindParam(':param16', $this->cliente_ids, PDO::PARAM_STR);
            $sentencia->bindParam(':param17', $this->clausulasrepre, PDO::PARAM_STR);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_escriturapublica SET
                        tb_escriturapublica_num = :param0,
                        tb_escriturapublica_kardex = :param1,
                        tb_escriturapublica_fecha = :param2,
                        tb_escriturapublica_clientedir = :param3,
                        tb_escriturapublica_clienteubig = :param4,
                        tb_escriturapublica_clausulasejecucion = :param5,
                        tb_escriturapublica_ipdncargorepres = :param6,
                        tb_persona_id = :param7,
                        tb_escriturapublica_clienterepre = :param8,
                        tb_escriturapublica_fechasiguales = :param9,
                        tb_escriturapublica_fechaconcfirmas = :param10,
                        tb_escriturapublica_valorgravamen = :param11,
                        tb_escriturapublica_tipocambio = :param12,
                        tb_escriturapublica_usumod = :param13,
                        tb_cliente_ids = :param14,
                        tb_escriturapublica_clausulasrepre = :param15
                    WHERE tb_escriturapublica_id =:param_x";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->num, PDO::PARAM_INT);
            $sentencia->bindParam(":param1", $this->kardex, PDO::PARAM_STR);
            $sentencia->bindParam(":param2", $this->fecha, PDO::PARAM_STR);
            $sentencia->bindParam(":param3", $this->clientedir, PDO::PARAM_STR);
            $sentencia->bindParam(":param4", $this->clienteubig, PDO::PARAM_STR);
            $sentencia->bindParam(":param5", $this->clausulasejecucion, PDO::PARAM_STR);
            $sentencia->bindParam(":param6", $this->ipdncargorepres, PDO::PARAM_INT);
            $sentencia->bindParam(":param7", $this->persona_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param8", $this->clienterepre, PDO::PARAM_INT);
            $sentencia->bindParam(":param9", $this->fechasiguales, PDO::PARAM_INT);
            $sentencia->bindParam(":param10", $this->fechaconcfirmas, PDO::PARAM_STR);
            $sentencia->bindParam(":param11", $this->valorgravamen, PDO::PARAM_STR);
            $sentencia->bindParam(":param12", $this->tipocambio, PDO::PARAM_INT);
            $sentencia->bindParam(":param13", $this->usumod, PDO::PARAM_INT);
            $sentencia->bindParam(":param14", $this->cliente_ids, PDO::PARAM_STR);
            $sentencia->bindParam(":param15", $this->clausulasrepre, PDO::PARAM_STR);
            $sentencia->bindParam(":param_x", $this->id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_todos($escriturapublica_id, $credito_id) {
        $where_opt = '';
        if (!empty($escriturapublica_id)) {
            $where_opt .= ' AND ep.tb_escriturapublica_id = :param_opc0';
        }
        if (!empty($credito_id)) {
            $where_opt .= ' AND ep.tb_credito_id = :param_opc1';
        }
        
        try {
            $sql = "SELECT ep.*,
                        CONCAT(p1.tb_persona_nom, ' ', p1.tb_persona_ape, ' - ', p1.tb_persona_doc) as repre_ipdn,
                        CONCAT(p2.tb_persona_nom, ' ', p2.tb_persona_ape, ' - ', p2.tb_persona_doc) as repre_cliente,
                        c.tb_cargo_nom,
                        u3.tb_ubigeo_coddep, u3.tb_ubigeo_nom AS Departamento_cli,
                        u2.tb_ubigeo_codpro, u2.tb_ubigeo_nom AS Provincia_cli,
                        u1.tb_ubigeo_coddis, u1.tb_ubigeo_nom AS Distrito_cli
                    FROM tb_escriturapublica ep
                    INNER JOIN tb_persona p1 ON (ep.tb_persona_id = p1.tb_persona_id)
                    INNER JOIN tb_persona p2 ON (ep.tb_escriturapublica_clienterepre = p2.tb_persona_id)
                    INNER JOIN tb_cargo c ON (ep.tb_escriturapublica_ipdncargorepres = c.tb_cargo_id)
                    INNER JOIN tb_ubigeo u1 ON (CONCAT(u1.tb_ubigeo_coddep, u1.tb_ubigeo_codpro, u1.tb_ubigeo_coddis) = ep.tb_escriturapublica_clienteubig)
                    INNER JOIN tb_ubigeo u2 ON (u1.tb_ubigeo_coddep=u2.tb_ubigeo_coddep AND u1.tb_ubigeo_codpro=u2.tb_ubigeo_codpro AND u2.tb_ubigeo_coddis='00')
                    INNER JOIN tb_ubigeo u3 ON (u2.tb_ubigeo_coddep=u3.tb_ubigeo_coddep AND u3.tb_ubigeo_codpro = '00' AND u3.tb_ubigeo_coddis = '00')
                    WHERE ep.tb_escriturapublica_xac = 1 $where_opt
                    ORDER BY ep.tb_escriturapublica_fecreg DESC;";
            //
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($escriturapublica_id)) {
                $sentencia->bindParam(':param_opc0', $escriturapublica_id, PDO::PARAM_INT);
            }
            if (!empty($credito_id)) {
                $sentencia->bindParam(':param_opc1', $credito_id, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
