
$(document).ready(function(){
	$('#datetimepicker_fec_esc_pub, #datetimepicker_fec_conc_firm').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });

	//transiciones de ubigeo
    $('select[id*="cbo_dep"]').change(function (event) {
        var codigo_dep = $(this).val();
        if (parseInt(codigo_dep) == 0) {
            $(`#cbo_pro`).html('');
            $(`#cbo_dis`).html('');
        } else {
            cargar_provincia(codigo_dep);
            $(`#cbo_dis`).html('');
        }
    });
    $('select[id*="cbo_pro"]').change(function (event) {
        var codigo_dep = $(`#cbo_dep`).val();
        var codigo_pro = $(this).val();

        if (parseInt(codigo_pro) == 0)
            $(`#cbo_dis`).html('');
        else
            cargar_distrito(codigo_dep, codigo_pro);
    });
    //

	//completar cbo de representante ipdn en la escr. publica
	$('#txt_escripub_fecha').change(function(){
		$('#cbo_escripub_repre_ipdn_cargo').val(0);
		if ($(this).val() == '') {
			$('#cbo_escripub_repre_ipdn').html('');
		} else {
			completar_cbo_representante($(this).val());
		}
	});

	$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: `icheckbox_flat-green`,
        radioClass: `iradio_flat-green`
    });

	if ($('#che_escripub_fechas_iguales').prop('checked')) {
        $("div .fechas_diferentes").hide();
    } else {
        $("div .fechas_diferentes").show();
    }
	$("#che_escripub_fechas_iguales").on('ifChanged', function () {
        if (this.checked) {
			$(".fechas_diferentes").hide(150);
            $("#txt_escripub_fechaconcfirmas").val(null);
        }
        else {
            $(".fechas_diferentes").show(150);
            $('#txt_escripub_fechaconcfirmas').focus();
        }
    });

	$('.cant').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '1',
        vMax: '10000'
    });

	$('.moneda').focus(function(){
		formato_moneda(this);
	});
	$('.tipo_cambio').focus(function() {
		formato_moneda(this, 9.999);
	});

	$('#cbo_escripub_repre_ipdn').change(function(){
		if (parseInt($(this).val()) > 0) {
			var cargo_id = $('#cbo_escripub_repre_ipdn option:selected').attr('data_cargoid');
			$('#cbo_escripub_repre_ipdn_cargo').val(cargo_id);
		} else {
			$('#cbo_escripub_repre_ipdn_cargo').val(0);
		}
	});

	//triggers
	$('select[id*="cbo_dep"]').change();
	$('#txt_escripub_fecha').change();
	buscar_asociacion_cli();
	setTimeout(function(){
		$('#obj_pdf_viewer').attr('height', $('#action_escriturapublica').parents('div.col-md-6').css('height'));
	}, 400);

	disabled($("#form_escriturapublica").find(".disabled"));

	$('#form_escriturapublica').validate({
		submitHandler: function () {
			var cliente_ids = '&cliente_ids='+$('#txt_escripub_cliente_id').val();
			$('.asociado').find('input[type="checkbox"][id*="che_cli_asociado"]:checked').each(function() {
				cliente_ids += ','+this.id.split('_')[3];
			});
			var ejecucionfase_id = $('#hdd_ejecucionfase_id').val();
			var dep_selected = '&dep_selected_nom=' + $('#cbo_dep option:selected').text();
			var pro_selected = '&pro_selected_nom=' + $('#cbo_pro option:selected').text();
			var dis_selected = '&dis_selected_nom=' + $('#cbo_dis option:selected').text();
			var repre_ipdn_selected = '&repre_ipdn_selected=' + $('#cbo_escripub_repre_ipdn option:selected').text();
			var cargo_repre_ipdn_selected = '&cargo_repreipdn_selected=' + $('#cbo_escripub_repre_ipdn_cargo option:selected').text();
			var repre_cli_selected = '&repre_cli_selected=' + $('#cbo_escripub_repre_cli option:selected').text();

			var datos_extra = dep_selected+pro_selected+dis_selected+repre_ipdn_selected+cargo_repre_ipdn_selected+repre_cli_selected+cliente_ids;
			$.ajax({
				type: "POST",
				url: VISTA_URL + "escriturapublica/escriturapublica_controller.php",
				async: true,
				dataType: "json",
				data: serialize_form('form_escriturapublica', datos_extra),
				beforeSend: function () {
					$('#escriturapublica_mensaje').show(400);
					$('#btn_guardar_escriturapublica').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						disabled($('#form_escriturapublica').find("button[class*='btn-sm'], select, input, textarea"));
						$('#escriturapublica_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#escriturapublica_mensaje').html(`<h4>${data.mensaje}</h4>`);

						setTimeout(function () {
							$('#modal_escriturapublica_form').modal('hide');
							if (data.estado == 1) {
								actualizar_contenido_1fase(ejecucionfase_id);
							}
						}, 2500);
					}
					else {
						$('#escriturapublica_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#escriturapublica_mensaje').html(`<h4>Alerta: ${data.mensaje}</h4>`);
						$('#btn_guardar_escriturapublica').prop('disabled', false);
					}
				},
				error: function (data) {
					$('#escriturapublica_mensaje').removeClass('callout-info').addClass('callout-danger');
					$('#escriturapublica_mensaje').html(`<h4>ALERTA DE ERROR: ${data.responseText}</h4>`);
				}
			});
		},
		rules: {
			txt_escripub_numero: {
				required: true
			},
			txt_escripub_kardex: {
				required: true
			},
			txt_escripub_fecha: {
				required: true
			},
			txt_escripub_clientedir: {
				required: true,
				minlength: 5
			},
			cbo_dep: {
				min: 1
			},
			cbo_pro: {
				min: 1
			},
			cbo_dis: {
				min: 1
			},
			cbo_escripub_repre_ipdn: {
				min: 1
			},
			cbo_escripub_repre_cli: {
				min: 1
			},
			txt_escripub_clausu_ejecucion: {
				required: true
			},
			txt_escripub_clausu_repre: {
				required: true
			},
			txt_escripub_valorgravamen: {
				required: true
			},
			txt_escripub_tipocambio:{
				required: true
			},
			txt_escripub_fechaconcfirmas: {
				required: true
			}
		},
		messages: {
			txt_escripub_numero: {
				required: "Obligatorio*"
			},
			txt_escripub_kardex: {
				required: "Obligatorio*"
			},
			txt_escripub_fecha: {
				required: "Obligatorio*"
			},
			txt_escripub_clientedir: {
				required: "Obligatorio*",
				minlength: "Min. 5 caracteres"
			},
			cbo_dep: {
				min: "Obligatorio*"
			},
			cbo_pro: {
				min: "Obligatorio*"
			},
			cbo_dis: {
				min: "Obligatorio*"
			},
			cbo_escripub_repre_ipdn: {
				min: "Obligatorio*"
			},
			cbo_escripub_repre_cli: {
				min: "Obligatorio*"
			},
			txt_escripub_clausu_ejecucion: {
				required: "Obligatorio*"
			},
			txt_escripub_clausu_repre: {
				required: "Obligatorio*"
			},
			txt_escripub_valorgravamen: {
				required: "Obligatorio*"
			},
			txt_escripub_tipocambio:{
				required: "Obligatorio*"
			},
			txt_escripub_fechaconcfirmas: {
				required: "Obligatorio*"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	if ($.inArray($('#action_escriturapublica').val(), ['leer']) > -1) {
		setTimeout(function(){
			disabled($('#form_escriturapublica').find("button[class*='btn-sm'], select, input, textarea, a"));
		}, 350);
    }

	$('#txt_escripub_clausu_ejecucion, #txt_escripub_clausu_repre').on('blur', function(){
		if ($(this).val().substr(-1) == ',') {
			$(this).val($(this).val().slice(0, -1));
		}
	});
});

//funciones para ubigeo
function cargar_provincia(ubigeo_coddep) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ubigeo/ubigeo_provincia_select.php",
		async: true,
		dataType: "html",
		data: ({
			ubigeo_coddep: ubigeo_coddep,
			ubigeo_codpro: $('#hdd_codpro').val()
		}),
		beforeSend: function () {
			$(`#cbo_pro`).html('<option>Cargando...</option>');
		},
		success: function (data) {
			$(`#cbo_pro`).html(data).change();
		},
		error: function (data) {
			alert(data.responseText);
		}
	});
}
function cargar_distrito(ubigeo_coddep, ubigeo_codpro) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ubigeo/ubigeo_distrito_select.php",
		async: true,
		dataType: "html",
		data: ({
			ubigeo_coddep: ubigeo_coddep,
			ubigeo_codpro: ubigeo_codpro,
			ubigeo_coddis: $('#hdd_coddis').val()
		}),
		beforeSend: function () {
			$(`#cbo_dis`).html('<option>Cargando...</option>');
		},
		success: function (data) {
			$(`#cbo_dis`).html(data);
			$(`#cbo_dis option:eq(0)`).val(0);
		},
		error: function (data) {
			alert(data.responseText);
		}
	});
}
//

function completar_cbo_representante(fecha_datetime) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "persona/cargopersona_select.php",
		async: true,
		dataType: "html",
		data: ({
			cargo_id: '4, 8', //gerencia o subgerencia
			vigente: fecha_datetime,
			persona_id: $('#hdd_escripub_ipdnrepre_id').val()
		}),
		beforeSend: function () {
			$(`#cbo_escripub_repre_ipdn`).html('<option>Cargando...</option>');
		},
		success: function (data) {
			$(`#cbo_escripub_repre_ipdn`).html(data).change();
		},
		error: function (data) {
			alert(data.responseText);
		}
	});
}

function valores_permitidos(input_txt, caracter_permitido) {
	$(input_txt).keydown(function(event){
		var codigo = event.which || event.keyCode;
		var tecleado = event.key;
		var condicional = caracter_permitido != '' ? (tecleado != ',') : true;
		if (!($.isNumeric(tecleado)) && condicional && codigo !==8 && codigo !==46 && codigo !==9 && !(codigo > 36 && codigo < 41)) {
			return false;
		}
	});
}

function buscar_asociacion_cli() {
	var cli_id = $('#txt_escripub_cliente_id').val();
	$.ajax({
		type: 'POST',
		url: VISTA_URL + 'escriturapublica/escriturapublica_controller.php',
		async: true,
		dataType: 'json',
		data: ({
			action_escriturapublica: 'buscar_asociacion',
			cliente_id: cli_id
		}),
		success: function (data) {
			if (data.estado == 1) {
				$('.che_asociados').html(data.html);
				$('.asociado').show(150);
				setTimeout(function(){
					$('.flat-green[id*="che_cli_asociado"]').iCheck({
						checkboxClass: `icheckbox_flat-green`,
						radioClass: `iradio_flat-green`
					});
					$('input[id*="che_cli_asociado"]').parents('label').css('padding-left', '8px');
				}, 200);

				//dar check a los clientes que intervienen
				var arr_clientes = JSON.parse($('#hdd_escripub_array_clientes').val());
				if (!arr_clientes !== true && arr_clientes.length > 1) {
					for (let i = 1; i < arr_clientes.length; i++) {
						$(`input[id*="che_cli_asociado_${arr_clientes[i]}"]`).iCheck('check');
					}
				}
			}
		}
	});
}