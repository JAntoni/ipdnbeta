<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';
require_once '../escriturapublica/Escriturapublica.class.php';
$oEscriturapublica = new Escriturapublica();
$listo = 1;

$usuario_action = $_POST['action'];

$titulo = '';
if ($usuario_action == 'I') {
    $titulo = 'Registrar Escritura publica';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Escritura publica';
} elseif ($usuario_action == 'L') {
    $titulo = 'Escritura publica registrado';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
$action = devuelve_nombre_usuario_action($usuario_action);

if ($listo == 1) {
    $credito_id = intval($_POST['credito_id']);
    $ejecucionfase_id = $_POST['ejecucionfase_id'];
    $datos_ejecucion = (array) json_decode($_POST['ejecucion_reg']);
    $dominio_server = 'https://ipdnsac.ipdnsac.com/';
    //$dominio_server = 'http://192.168.101.90/ipdnsac/';
    $url_EP = $_POST['url_EP'];
    $altura = intval($_POST['height']) - 20;
    $ancho = intval($_POST['width']);

    $cliente_concat_nom = "{$datos_ejecucion['tb_cliente_doc']} - {$datos_ejecucion['tb_cliente_nom']}";
    if (intval($datos_ejecucion['tb_cliente_tip']) == 2) {
        $cliente_concat_nom .= ", {$datos_ejecucion['tb_cliente_empruc']} - {$datos_ejecucion['tb_cliente_emprs']}";
    }
    $cliente_id = $datos_ejecucion['tb_cliente_id'];
    $ancho_columnas_clausula = intval($datos_ejecucion['tb_cgarvtipo_id']) == 1 ? 4 : 3;

    $res_buscar_EP = $oEscriturapublica->listar_todos('', $credito_id);
    if ($res_buscar_EP['estado'] == 1) {
        $EP = $res_buscar_EP['data'][0];

        $escriturapublica_id    = $EP['tb_escriturapublica_id'];
        $escrit_pub_num         = $EP['tb_escriturapublica_num'];
        $escrit_pub_kardex      = $EP['tb_escriturapublica_kardex'];
        $escrit_pub_fec         = mostrar_fecha($EP['tb_escriturapublica_fecha']);
        $escrit_pub_cliente_dir = $EP['tb_escriturapublica_clientedir'];
        $escrit_pub_cliente_ubi = $EP['tb_escriturapublica_clienteubig'];
        $ubigeo_coddep          = substr($escrit_pub_cliente_ubi, 0, 2);
        $ubigeo_codpro          = substr($escrit_pub_cliente_ubi, 2, 2);
        $ubigeo_coddis          = substr($escrit_pub_cliente_ubi, 4, 2);
        $escrit_pub_repreipdn   = $EP['tb_persona_id'];
        $persona_id             = $EP['tb_escriturapublica_clienterepre'];
        $escrit_pub_clausulas   = $EP['tb_escriturapublica_clausulasejecucion'];
        $escrit_pub_clausulas_rep = $EP['tb_escriturapublica_clausulasrepre'];
        $escrit_pub_valgra      = mostrar_moneda($EP['tb_escriturapublica_valorgravamen']);
        $escrit_pub_tipocambio  = $EP['tb_escriturapublica_tipocambio'];
        $escrit_pub_fechas_iguales  = $EP['tb_escriturapublica_fechasiguales'] == 1 ? 'checked' : '';
        $escrit_pub_fec_conc_firmas = mostrar_fecha($EP['tb_escriturapublica_fechaconcfirmas']);
        $cliente_ids            = explode(',',$EP['tb_cliente_ids']);
    }
    unset($res_buscar_EP);
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_escriturapublica_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_escriturapublica" method="post">
                    <div class="modal-body">
                        <div class="col-md-6">
                            <input type="hidden" name="action_escriturapublica" id="action_escriturapublica" value="<?php echo $action; ?>">
                            <input type="hidden" name="hdd_escriturapublica_id" id="hdd_escriturapublica_id" value="<?php echo $escriturapublica_id; ?>">
                            <input type="hidden" name="hdd_ejecucionfase_id" id="hdd_ejecucionfase_id" value="<?php echo $ejecucionfase_id; ?>">
                            <input type="hidden" name="hdd_escriturapublica_reg" id="hdd_escriturapublica_reg" value='<?php echo json_encode($EP); ?>'>

                            <div class="box box-primary shadow">
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="txt_escripub_creditoid">Credito:</label>
                                        <input type="text" name="txt_escripub_creditoid" id="txt_escripub_creditoid" class="input-sm form-control mayus input-shadow disabled" placeholder="Credito id" value="<?php echo $credito_id; ?>">
                                    </div>

                                    <div class="col-md-8">
                                        <input type="hidden" name="txt_escripub_cliente_id" id="txt_escripub_cliente_id" value="<?php echo $cliente_id; ?>">
                                        <label for="txt_escripub_cliente">Cliente:</label>
                                        <input type="text" name="txt_escripub_cliente" id="txt_escripub_cliente" class="input-sm form-control mayus input-shadow disabled" placeholder="Nombres del cliente" value="<?php echo $cliente_concat_nom; ?>">
                                    </div>
                                </div>

                                <div class="row form-group asociado" style="display: none;">
                                    <input type="hidden" name="hdd_escripub_array_clientes" id="hdd_escripub_array_clientes" value='<?php echo json_encode($cliente_ids);?>'>
                                    <div class="col-md-12 che_asociados">
                                        
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="txt_escripub_numero">N° Esc. Pública (en números):</label>
                                        <input type="text" name="txt_escripub_numero" id="txt_escripub_numero" class="input-sm form-control mayus input-shadow cant" placeholder="numero esc pub" value="<?php echo $escrit_pub_num; ?>">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="txt_escripub_kardex">N° Kardex:</label>
                                        <input type="text" name="txt_escripub_kardex" id="txt_escripub_kardex" onfocus="valores_permitidos(this, '')" class="form-control input-sm input-shadow mayus" placeholder="Kardex" value="<?php echo $escrit_pub_kardex; ?>">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="txt_escripub_fecha">Fecha Esc. Púb.:</label>
                                        <div class='input-group date' id='datetimepicker_fec_esc_pub'>
                                            <input type="text" name="txt_escripub_fecha" id="txt_escripub_fecha" class="form-control input-sm input-shadow mayus" placeholder="Fec. Esc. Publ." value="<?php echo $escrit_pub_fec; ?>">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label for="txt_escripub_clientedir">Cliente dirección:</label>
                                        <input type="text" name="txt_escripub_clientedir" id="txt_escripub_clientedir" class="form-control input-sm input-shadow mayus" placeholder="Direccion Cliente" value="<?php echo htmlspecialchars($escrit_pub_cliente_dir); ?>">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label for="cbo_dep">Departamento:</label>
                                        <select name="cbo_dep" id="cbo_dep" class="form-control input-sm input-shadow mayus">
                                            <?php require_once '../ubigeo/ubigeo_departamento_select.php'; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <input type="hidden" name="hdd_codpro" id="hdd_codpro" value="<?php echo $ubigeo_codpro; ?>">
                                        <label for="cbo_pro">Provincia:</label>
                                        <select name="cbo_pro" id="cbo_pro" class="form-control input-sm input-shadow mayus"></select>
                                    </div>

                                    <div class="col-md-4">
                                        <input type="hidden" name="hdd_coddis" id="hdd_coddis" value="<?php echo $ubigeo_coddis; ?>">
                                        <label for="cbo_dis">Distrito:</label>
                                        <select name="cbo_dis" id="cbo_dis" class="form-control input-sm input-shadow mayus"></select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-8">
                                        <input type="hidden" name="hdd_escripub_ipdnrepre_id" id="hdd_escripub_ipdnrepre_id" value="<?php echo $escrit_pub_repreipdn; ?>">
                                        <label for="cbo_escripub_repre_ipdn">IPDN fue representado por:</label>
                                        <select name="cbo_escripub_repre_ipdn" id="cbo_escripub_repre_ipdn" class="form-control input-sm input-shadow mayus">
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="cbo_escripub_repre_ipdn_cargo">Cargo</label>
                                        <select name="cbo_escripub_repre_ipdn_cargo" id="cbo_escripub_repre_ipdn_cargo" class="form-control input-sm input-shadow mayus disabled">
                                            <?php require_once '../cargo/cargo_select.php'; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-8">
                                        <?php $cargo_id = 9; ?>
                                        <label for="cbo_escripub_repre_cli">Representante de cliente:</label>
                                        <select name="cbo_escripub_repre_cli" id="cbo_escripub_repre_cli" class="form-control input-sm input-shadow mayus">
                                            <?php require_once '../persona/cargopersona_select.php'; ?>
                                        </select>
                                        <?php unset($cargo_id); ?>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-<?php echo $ancho_columnas_clausula;?>">
                                        <label for="txt_escripub_clausu_ejecucion">Cláusulas ejecuc.<?php echo intval($datos_ejecucion['tb_cgarvtipo_id']) != 1 ? '(cli)' : ''; ?>:</label>
                                        <input type="text" name="txt_escripub_clausu_ejecucion" id="txt_escripub_clausu_ejecucion" onfocus="valores_permitidos(this, ',')" class="form-control input-sm input-shadow" placeholder="10,12,15" value="<?php echo $escrit_pub_clausulas; ?>">
                                    </div>
                                    <?php if (intval($datos_ejecucion['tb_cgarvtipo_id']) != 1) { ?>
                                        <div class="col-md-3">
                                            <label for="txt_escripub_clausu_repre">Cláusulas ejecuc.(repre):</label>
                                            <input type="text" name="txt_escripub_clausu_repre" id="txt_escripub_clausu_repre" onfocus="valores_permitidos(this, ',')" class="form-control input-sm input-shadow" placeholder="10,12,15" value="<?php echo $escrit_pub_clausulas_rep; ?>">
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-<?php echo $ancho_columnas_clausula;?>">
                                        <label for="txt_escripub_valorgravamen">Valor gravamen:</label>
                                        <div class="input-group">
                                            <?php $moneda_id = 2; //valor del gravamen siempre es en dolares ?>
                                            <select name="cbo_escripub_moneda_gravamen" id="cbo_escripub_moneda_gravamen" class="form-control input-sm mayus input-shadow disabled" style="width: 38%; padding: 5px 5px;">
                                                <?php require_once('../moneda/moneda_select.php'); ?>
                                            </select>
                                            <?php unset($moneda_id); ?>
                                            <input type="text" name="txt_escripub_valorgravamen" id="txt_escripub_valorgravamen" class="form-control input-sm moneda input-shadow" style="width: 62%; padding: 5px 5px;" placeholder="0.00" value="<?php echo $escrit_pub_valgra; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-<?php echo $ancho_columnas_clausula;?>">
                                        <label for="txt_escripub_tipocambio">Tipo Cambio:</label>
                                        <input type="text" name="txt_escripub_tipocambio" id="txt_escripub_tipocambio" class="input-shadow input-sm form-control mayus tipo_cambio" placeholder="0.000" value="<?php echo $escrit_pub_tipocambio; ?>">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-8" style="padding: 10px 0px;">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="che_escripub_fechas_iguales" name="che_escripub_fechas_iguales" class="flat-green" value="1" <?php echo $escrit_pub_fechas_iguales; ?>><b>&nbsp; ¿Fecha conclusión de firmas es igual a fecha de E.P.?</b>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fechas_diferentes">
                                            <label for="txt_escripub_fechaconcfirmas">Fecha Conc. Firmas:</label>
                                            <div class='input-group date' id='datetimepicker_fec_conc_firm'>
                                                <input type="text" name="txt_escripub_fechaconcfirmas" id="txt_escripub_fechaconcfirmas" class="form-control input-sm input-shadow mayus" placeholder="Fec. Conc. Firmas." value="<?php echo $escrit_pub_fec_conc_firmas; ?>">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--- MESAJES DE GUARDADO -->
                            <div class="callout callout-info" id="escriturapublica_mensaje" style="display: none;">
                                <h4>Cargando...</h4>
                            </div>
                        </div>
                        <div class="col-md-6 div_pdf_viewer" style="padding: 0px;">
                            <object id="obj_pdf_viewer" data='<?php echo $dominio_server . $url_EP; ?>' type='application/pdf' width='<?php echo $ancho; ?>px'>Si no se muestra el archivo, puedes visualizarlo desde <a href='<?php echo "../../" . $url_EP; ?>' target='_blank'><b>AQUI</b></a></object>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_escriturapublica">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_escriturapublica">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/escriturapublica/escriturapublica_form.js?ver='.rand(); ?>"></script>
