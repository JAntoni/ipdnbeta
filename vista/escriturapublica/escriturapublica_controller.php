<?php
//todos los require
	require_once '../../core/usuario_sesion.php';
	require_once '../funciones/fechas.php';
	require_once '../funciones/funciones.php';
	require_once '../escriturapublica/Escriturapublica.class.php';
	$oEscritura = new Escriturapublica();
	require_once '../historial/Historial.class.php';
	$oHist = new Historial();
//

//datos estaticos
	$action				= $_POST['action_escriturapublica'];
	$usuario_id			= intval($_SESSION['usuario_id']);
	$data['estado']		= 0;
	$data['mensaje']	= "Error al $action la Escritura pública";
//

//recibir datos y asignar
	$fechas_iguales		= intval($_POST['che_escripub_fechas_iguales']);
	$ejecucionfase_id	= $_POST['hdd_ejecucionfase_id'];
	$fecha_EP			= $_POST['txt_escripub_fecha'];
	$fecha_conc_firm	= $_POST['txt_escripub_fechaconcfirmas'];
	$valor_grav			= $_POST['txt_escripub_valorgravamen'];
	$cliente_ids		= $_POST['cliente_ids'];

	$oEscritura->credito_id = $_POST['txt_escripub_creditoid'];
	$oEscritura->num = $_POST['txt_escripub_numero'];
	$oEscritura->kardex = $_POST['txt_escripub_kardex'];
	$oEscritura->fecha = fecha_mysql($fecha_EP);
	$oEscritura->clientedir = strtoupper(trim($_POST['txt_escripub_clientedir']));
	$oEscritura->clienteubig = $_POST['cbo_dep'].$_POST['cbo_pro'].$_POST['cbo_dis'];
	$oEscritura->clausulasejecucion = $_POST['txt_escripub_clausu_ejecucion'];
	$oEscritura->clausulasrepre = empty($_POST['txt_escripub_clausu_repre']) ? null : $_POST['txt_escripub_clausu_repre'];
	$oEscritura->ipdncargorepres = $_POST['cbo_escripub_repre_ipdn_cargo'];
	$oEscritura->persona_id = $_POST['cbo_escripub_repre_ipdn'];
	$oEscritura->clienterepre = $_POST['cbo_escripub_repre_cli'];
	$oEscritura->fechasiguales = $fechas_iguales;
	$oEscritura->fechaconcfirmas = $fechas_iguales == 1 ? $oEscritura->fecha : fecha_mysql($fecha_conc_firm);
	$oEscritura->valorgravamen = moneda_mysql($valor_grav);
	$oEscritura->tipocambio = $_POST['txt_escripub_tipocambio'];
	$oEscritura->usumod = $usuario_id;
	$oEscritura->cliente_ids = $cliente_ids;
//

if ($action == 'insertar') {
	$oEscritura->usureg = $usuario_id;
	
	$res_insert = $oEscritura->insertar();
	if ($res_insert['estado'] == 1) {
		$data['estado'] = 1;
		$data['mensaje'] = "Escritura pública registrada correctamente";

		//guardar historial tb_hist de la tabla tb_escriturapublica
		$oHist->setTbHistUsureg($usuario_id);
		$oHist->setTbHistNomTabla('tb_escriturapublica');
		$oHist->setTbHistRegmodid($res_insert['nuevo']);
		$oHist->setTbHistDet("Registró los datos de la Escritura pública del crédito {$oEscritura->credito_id} | <b>". date("d-m-Y h:i a").'</b>');
		$oHist->insertar();

		//guardar historial tb_hist de la tabla tb_ejecucionfase
		$oHist->setTbHistUsureg($usuario_id);
		$oHist->setTbHistNomTabla('tb_ejecucionfase');
		$oHist->setTbHistRegmodid($ejecucionfase_id);
		$oHist->setTbHistDet("Registró los datos de la Escritura pública del crédito {$oEscritura->credito_id} | <b>". date("d-m-Y h:i a").'</b>');
		$oHist->insertar();
	}
}
elseif ($action == 'modificar') {
	$EP_reg_orig = (array) json_decode($_POST['hdd_escriturapublica_reg']);

	$igual[0] = boolval($oEscritura->num == $EP_reg_orig['tb_escriturapublica_num']);
	$igual[1] = boolval($oEscritura->kardex == $EP_reg_orig['tb_escriturapublica_kardex']);
	$igual[2] = boolval($fecha_EP == mostrar_fecha($EP_reg_orig['tb_escriturapublica_fecha']));
	$igual[3] = boolval($oEscritura->clientedir == $EP_reg_orig['tb_escriturapublica_clientedir']);
	$igual[4] = boolval($oEscritura->clienteubig == $EP_reg_orig['tb_escriturapublica_clienteubig']);
	$igual[5] = boolval($oEscritura->persona_id == $EP_reg_orig['tb_persona_id']);
	$igual[6] = boolval($oEscritura->ipdncargorepres == $EP_reg_orig['tb_escriturapublica_ipdncargorepres']);
	$igual[7] = boolval($oEscritura->clienterepre == $EP_reg_orig['tb_escriturapublica_clienterepre']);
	$igual[8] = boolval($oEscritura->clausulasejecucion == $EP_reg_orig['tb_escriturapublica_clausulasejecucion']);
	$igual[9] = boolval($oEscritura->valorgravamen == $EP_reg_orig['tb_escriturapublica_valorgravamen']);
	$igual[10] = boolval($oEscritura->fechasiguales == intval($EP_reg_orig['tb_escriturapublica_fechasiguales']));
	$igual[11] = boolval($oEscritura->fechaconcfirmas == fecha_mysql($EP_reg_orig['tb_escriturapublica_fechaconcfirmas']));
	$igual[12] = boolval($oEscritura->tipocambio == $EP_reg_orig['tb_escriturapublica_tipocambio']);
	$igual[13] = boolval($oEscritura->cliente_ids == $EP_reg_orig['tb_cliente_ids']);
	$igual[14] = boolval($oEscritura->clausulasrepre == $EP_reg_orig['tb_escriturapublica_clausulasrepre']);

	$son_iguales = true;
	$i = 0;
	while ($i < count($igual)) {
		if (!$igual[$i]) {
			$son_iguales = false;
		}
		$i++;
	}
	$i = 0;

	if ($son_iguales) {
		$data['estado'] = 2;
		$data['mensaje'] = "● No se realizó ninguna modificacion";
	}
	else {
		$escriturapublica_id = $_POST['hdd_escriturapublica_id'];
		$oEscritura->id = $escriturapublica_id;

		$res_modificar = $oEscritura->modificar();
		if ($res_modificar == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = '● Datos de Escritura Pública modificados correctamente';

			//insertar historial
				$oHist->setTbHistUsureg($usuario_id);
				$oHist->setTbHistNomTabla('tb_escriturapublica');
				$oHist->setTbHistRegmodid($escriturapublica_id);
				$mensaje = "Modificó la escriturapublica con id $escriturapublica_id:";
				if (!$igual[13])
					$mensaje .= "<br> - Cambió los asociados que participan en la Esc. Pública: cliente_id {$EP_reg_orig['tb_cliente_ids']} => <b>cliente_id {$oEscritura->cliente_ids}</b>";
				if (!$igual[0])
					$mensaje .= "<br> - Cambió el N° Esc. Pública: {$EP_reg_orig['tb_escriturapublica_num']} => <b>{$oEscritura->num}</b>";
				if (!$igual[1])
					$mensaje .= "<br> - Cambió el N° Kardex: {$EP_reg_orig['tb_escriturapublica_kardex']} => <b>{$oEscritura->kardex}</b>";
				if (!$igual[2])
					$mensaje .= "<br> - Cambió la fecha de la Esc. Pública: ".mostrar_fecha($EP_reg_orig['tb_escriturapublica_fecha'])." => <b>$fecha_EP</b>";
				if (!$igual[3])
					$mensaje .= "<br> - Cambió la dirección del cliente de la Esc. Pública: {$EP_reg_orig['tb_escriturapublica_clientedir']} => <b>{$oEscritura->clientedir}</b>";
				if (!$igual[4]){
					$ubigeo_anterior_nom	= "{$EP_reg_orig['Departamento_cli']}-{$EP_reg_orig['Provincia_cli']}-{$EP_reg_orig['Distrito_cli']}";
					$ubigeo_elegido_nom		= "{$_POST['dep_selected_nom']}-{$_POST['pro_selected_nom']}-{$_POST['dis_selected_nom']}";
					$mensaje .= "<br> - Cambió el ubigeo del cliente de la Esc. Pública: {$EP_reg_orig['tb_escriturapublica_clienteubig']}, $ubigeo_anterior_nom => <b>{$oEscritura->clienteubig}, $ubigeo_elegido_nom</b>";
				}
				if (!$igual[5]){
					$repre_ipdn_elegido = $_POST['repre_ipdn_selected'];
					$mensaje .= "<br> - Cambió el representante IPDN de la Esc. Pública: {$EP_reg_orig['repre_ipdn']} => <b>$repre_ipdn_elegido</b>";
				}
				if (!$igual[6]){
					$cargo_repreipdn_elegido = $_POST['cargo_repreipdn_selected'];
					$mensaje .= "<br> - Cambió el cargo del representante IPDN de la Esc. Pública: {$EP_reg_orig['tb_cargo_nom']} => <b>$cargo_repreipdn_elegido</b>";
				}
				if (!$igual[7]){
					$repre_cli_elegido = $_POST['repre_cli_selected'];
					$mensaje .= "<br> - Cambió el cargo del representante IPDN de la Esc. Pública: {$EP_reg_orig['repre_cliente']} => <b>$repre_cli_elegido</b>";
				}
				if (!$igual[8])
					$mensaje .= "<br> - Cambió las cláusulas de ejecución de la Esc. Pública: {$EP_reg_orig['tb_escriturapublica_clausulasejecucion']} => <b>{$oEscritura->clausulasejecucion}</b>";
				if (!$igual[14])
					$mensaje .= "<br> - Cambió las cláusulas para la CN del representante: {$EP_reg_orig['tb_escriturapublica_clausulasrepre']} => <b>{$oEscritura->clausulasrepre}</b>";
				if (!$igual[9])
					$mensaje .= "<br> - Cambió el valor del gravamen de la Esc. Pública: US$ ".mostrar_moneda($EP_reg_orig['tb_escriturapublica_valorgravamen'])." => <b>US$ $valor_grav</b>";
				if (!$igual[12])
					$mensaje .= "<br> - Cambió el tipo de cambio de la Esc. Pública: {$EP_reg_orig['tb_escriturapublica_tipocambio']} => <b>{$oEscritura->tipocambio}</b>";
				if (!$igual[10]){
					$arr_igualdad = array('NO', 'SI');
					$mensaje .= "<br> - Cambió la igualdad de las fechas Esc. Pública y conc. Firmas: {$arr_igualdad[$EP_reg_orig['tb_escriturapublica_fechasiguales']]} => <b>{$arr_igualdad[$oEscritura->fechasiguales]}</b>";
				}
				if (!$igual[11])
					$mensaje .= "<br> - Cambió la fecha de conc. Firmas: ".mostrar_fecha($EP_reg_orig['tb_escriturapublica_fechaconcfirmas'])." => <b>".mostrar_fecha($oEscritura->fechaconcfirmas)."</b>";
				
				$oHist->setTbHistDet($mensaje);
				$oHist->insertar();
			//

			//guardar historial en la fase
				$oHist->setTbHistUsureg($usuario_id);
				$oHist->setTbHistNomTabla('tb_ejecucionfase');
				$oHist->setTbHistRegmodid($_POST['hdd_ejecucionfase_id']);
				$oHist->setTbHistDet("Editó los datos de la Escritura pública del crédito {$oEscritura->credito_id} | <b>". date("d-m-Y h:i a").'</b>');
				$oHist->insertar();
			//
		}
		unset($res_modificar);
	}
}
elseif ($action == 'buscar_asociacion') {
	require_once '../cliente/Cliente.class.php';
	$oCliente = new Cliente();

	$chk_box = '';

	$result = $oCliente->listar_sociedad_cliente($_POST['cliente_id']);
	$data['estado'] = $result['estado'];
	if ($result['estado'] == 1) {
		$chk_box = '<label>Elija asociación que figura en la Escritura pública:</label>';
		foreach ($result['data'] as $key => $value) {
			$chk_box .=
			'<div class="checkbox">
				<label>
					<input type="checkbox" name="che_cli_asociado_'.$value['tb_cliente_id2'].'" id="che_cli_asociado_'.$value['tb_cliente_id2'].'" class="flat-green" value="1">&nbsp; '.$value['cliente_asociado_doc'].' - '.$value['tb_cliente_nom'].'
				</label>
			</div>'
			;
		}
	}
	$data['html'] = $chk_box;

	unset($result);
}
else {
	$data['mensaje'] = "No se encontró la accion que intenta realizar";
}

echo json_encode($data);
