<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../garantia/Garantia.class.php');
  $oGarantia = new Garantia();
  require_once('../funciones/funciones.php');
  require_once ('../tareainventario/Tareainventario.class.php');
  $oTareainventario = new Tareainventario();
  require_once('../articulo/Articulo.class.php');

	$usuario_id = intval($_SESSION['usuario_id']);
	$action = $_POST['action'];

 	$oGarantia->credito_id = intval($_POST['hdd_credito_id']);
  $oGarantia->garantiatipo_id = intval($_POST['cmb_garantiatipo_id']);
	$oGarantia->garantia_estrang = intval($_POST['cmb_garantia_estrang']); //rango de estado 8 de 10, 9 de 10
  $oGarantia->garantia_can = 1; //intval($_POST['txt_garantia_can']); la cantidad será por ahora 1
  $oGarantia->garantia_pro = mb_strtoupper($_POST['txt_garantia_pro'], 'UTF-8');
  $oGarantia->garantia_val = moneda_mysql($_POST['txt_garantia_val']);
  $oGarantia->garantia_valtas = moneda_mysql($_POST['txt_garantia_valtas']);
  $oGarantia->garantia_ser = $_POST['txt_garantia_ser'];
  $oGarantia->garantia_kil = $_POST['txt_garantia_kil']; //kilates para oro
  $oGarantia->garantia_pes = $_POST['txt_garantia_pes']; //peso para oro
  $oGarantia->garantia_grab = $_POST['txt_garantia_grab']; //grabado
  $oGarantia->garantia_med = $_POST['txt_garantia_med']; //medidas
  $oGarantia->garantia_tas = $_POST['txt_garantia_tas']; //tasador de oro
  $oGarantia->garantia_det = $_POST['txt_garantia_det']; //detalle
  $oGarantia->garantia_web = $_POST['txt_garantia_web'];
	$oGarantia->garantia_calidad = intval($_POST['cmb_garantia_calidad']);
  $articulo_id = intval($_POST['cbo_articulo']);

	$oGarantia->vehiculomarca_id = 0;
	$oGarantia->vehiculomodelo_id = 0;
	$oGarantia->vehiculoclase_id = 0;
	$oGarantia->garantia_vehpla = '';
	$oGarantia->garantia_vehsermot = '';
	$oGarantia->garantia_vehsercha = '';
	$oGarantia->garantia_vehano = '';
	$oGarantia->garantia_vehkil = '';
	$oGarantia->garantia_vehcol = '';

	if($articulo_id == 75){ //? EL USUARIO HA SELECCIONADO EL ARTICULO DE TIPO VEHICULO
		$oGarantia->vehiculomarca_id = intval($_POST['cmb_vehiculomarca_id']);
		$oGarantia->vehiculomodelo_id = intval($_POST['cmb_vehiculomodelo_id']);
		$oGarantia->vehiculoclase_id = intval($_POST['cmb_vehiculoclase_id']);
		$oGarantia->garantia_vehpla = $_POST['txt_garantia_vehpla'];
		$oGarantia->garantia_vehsermot = $_POST['txt_garantia_vehsermot'];
		$oGarantia->garantia_vehsercha = $_POST['txt_garantia_vehsercha'];
		$oGarantia->garantia_vehano = $_POST['txt_garantia_vehano'];
		$oGarantia->garantia_vehkil = $_POST['txt_garantia_vehkil'];
		$oGarantia->garantia_vehcol = $_POST['txt_garantia_vehcol'];
	}

 	if($action == 'insertar'){
		/* daniel odar 06-03-23*/
		$mensaje = "";
		if($articulo_id == 0){
		  $articulo_nombre = trim(strtoupper($_POST['txt_articulo_nom']));
		  $oArticulo = new Articulo();
		  $oArticulo->tb_articulo_nombre = $articulo_nombre;
		  $oArticulo->tb_articulo_usureg = $usuario_id;
		  $oArticulo->tb_articulo_usumod = $usuario_id;
	  
		  $result = $oArticulo->insertar();
		  if($result['estado'] == 0){
			  $data['estado'] = 0;
			   $data['mensaje'] = "Error en el registro de articulo $articulo_nombre. ";
			  echo json_encode($data);
			  exit();
		  }
		  else{
			  $mensaje = "Se registró el artículo $articulo_nombre. ";
			  $oGarantia->articulo_id = $result['nuevo'];
		  }
		}
		else{
		  $oGarantia->articulo_id = $articulo_id;
		}
		/* */

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Garantía. '.$oGarantia->garantia_val;
    	$result = $oGarantia->insertar();
			if($result['estado']==1){
				$data['estado'] = 1;
				$data['mensaje'] = $mensaje . 'Garantía registrada correctamente. // '.intval($_POST['cmb_vehiculomodelo_id']);;
													
				$oTareainventario->tb_tarea_det = $_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].' registró la garantía: '.$result['garantia_id'].' que pertenece al crédito: '.$oGarantia->credito_id;
				$oTareainventario->tb_tarea_usureg = $_SESSION['usuario_id'];
				$oTareainventario->tb_garantia_id = $result['garantia_id'];

				$oTareainventario->insertar2();
			}
		$result = NULL;

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
		/* daniel odar 06-03-23*/
		$mensaje = "";
		if($articulo_id == 0){
		  $articulo_nombre = trim(strtoupper($_POST['txt_articulo_nom']));
		  $oArticulo = new Articulo();
		  $oArticulo->tb_articulo_nombre = $articulo_nombre;
		  $oArticulo->tb_articulo_usureg = $usuario_id;
		  $oArticulo->tb_articulo_usumod = $usuario_id;
	  
		  $result = $oArticulo->insertar();
		  if($result['estado'] == 0){
			  $data['estado'] = 0;
			   $data['mensaje'] = "Error en el registro de articulo $articulo_nombre. ";
			  echo json_encode($data);
			  exit();
		  }
		  else{
			  $mensaje = "Se registró el artículo $articulo_nombre. ";
			  $oGarantia->articulo_id = $result['nuevo'];
		  }
		}
		else{
		  $oGarantia->articulo_id = $articulo_id;
		}
		/* */

		/* daniel odar 07-03-2023 */
		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al modificar la Garantía.';

		$garantia_id = intval($_POST['hdd_garantia_id']);
		$cambiar_articulo = intval($_POST["hdd_mod_articulo"]);

		if($cambiar_articulo == 0){
			$oGarantia->garantia_id = $garantia_id;

			if ($oGarantia->modificar()) {
				$data['estado'] = 1;
				$data['mensaje'] = $mensaje . 'Garantia modificado correctamente.';
			}
		}
		$res = $oGarantia->modificar_campo($garantia_id, "tb_articulo_id", $oGarantia->articulo_id, "INT");

		if ($res == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = 'Garantia modificada correctamente.';
		}

		echo json_encode($data);
		/* */
 	}

 	elseif($action == 'eliminar'){
 		$garantia_id = intval($_POST['hdd_garantia_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Garantia.';

 		if($oGarantia->eliminar($garantia_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Garantia eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

	elseif($action == 'fijar_precio'){
		$garantia_id = intval($_POST['hdd_garantia_id']);
		$garantia_prefij = moneda_mysql(trim($_POST['txt_garantia_nuevo_monto']));
		$garantia_num_cuota = intval($_POST['txt_garantia_num_cuota']);


		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al asignar el precio a la Garantia. '.$garantia_id.' / '.$garantia_prefij;
		
		if($garantia_prefij <= 0){
			$data['mensaje'] = 'No puede vender un producto por el precio de: '.$garantia_prefij;
			echo json_encode($data); exit();
		}

		if($oGarantia->modificar_campo($garantia_id, 'tb_garantia_prefij', $garantia_prefij, 'STR')){
			if($oGarantia->modificar_campo($garantia_id, 'tb_garantia_num_cuota_rem', $garantia_num_cuota, 'INT')){
				$data['estado'] = 1;
				$data['mensaje'] = 'Número de cuotas fijado Correctamente';
			}
			$data['estado'] = 1;
			$data['mensaje'] = 'Precio y número de cuotas fijado Correctamente';
		}

		echo json_encode($data);
	}

	elseif($action == 'galeria_fijarprecio'){
		$data['galeria'] = '';
		require_once('../upload/Upload.class.php');
		$oUpload = new Upload();
		$res = $oUpload->listar_uploads('garantia_fijarprecio', $_POST['garantia_id']);
		if($res['estado'] == 1){
			foreach($res['data'] as $key => $upload){
				$data['galeria'] .= '
				<div class="col-md-3 col-xs-3">
					<img class="img-responsive" src="'.$upload['upload_url'].'" alt="Photo" style="width: 80%; cursor: pointer;" onclick="carousel(\'garantia_fijarprecio\', '.$_POST['garantia_id'].')">
				</div>
				';
			}
		}
		echo json_encode($data);
	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
