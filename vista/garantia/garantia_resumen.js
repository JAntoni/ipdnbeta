$(document).ready(function () {
  console.log('siii 22333')
  $('#txt_garantia_nuevo_monto').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0',
    vMax: '99999.00'
  });
  if($('#garantia_vista').val() == 'tasacionsug'){
    disabled($('#txt_garantia_nuevo_monto'));
  }
  actualizar_fotos_fijarprecio($('#hdd_garantia_id').val());

  $('#form_garantia_resumen').validate({
    submitHandler: function () {
      $.confirm({
        title: 'Asignar Precio y número de cuotas Fijo',
        icon: 'fa fa-smile-o',
        theme: 'modern',
        content: '¿Está seguro que desea asignar un precio y número de cuotas fijo?',
        type: 'blue',
        typeAnimated: true,
        buttons: {
          Aceptar: function(){
            $.ajax({
              type: "POST",
              url: VISTA_URL + "garantia/garantia_controller.php",
              async: true,
              dataType: "json",
              data: $("#form_garantia_resumen").serialize(),
              beforeSend: function () {
              },
              success: function (data) {
                if(data.estado == 1)
                  notificacion_success(data.mensaje, 6000)
                  // Gerson (17-02-23)
                  var vista = $('#garantia_vista').val();
                  if (vista == 'cobranza')
                    $('#modal_garantia_resumen').modal('hide');
                    $('#modal_cobranza_remate_garantia').modal('hide');
                    cobranza_tabla_gestion();
                  //
              },
              complete: function (data) {
                console.log(data);
              },
              error: function (data) {
              }
            });
          },
          Cerrar: function (){
          
          }
        }
      });
    },
    rules: {
      txt_garantia_nuevo_monto: {
        required: true
      }
    },
    messages: {
      txt_garantia_nuevo_monto: {
        required: "Ingrese una cantidad"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
});
})

/* GERSON (17-02-23) */
function cobranza_tabla_gestion(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_tabla_gestion.php",
    async: true,
    dataType: "html",
    data: $("#for_fil_cre_gestion").serialize(),
    beforeSend: function() {
      $('#cobranza_mensaje_tbl_gestion').show(300);
      if($('#txt_cli_gestion').val()=='' || $('#txt_cli_gestion').val()==null){
        $('#hdd_cli_id_gestion').val('');
      }
    },
    success: function(data){
      $('#div_cobranza_tabla_gestion').html(data);
      $('#cobranza_mensaje_tbl_gestion').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#cliente_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

/* GERSON (03-05-23) */
function solo_numero(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if(charCode > 31 && (charCode < 48 || charCode > 57)){
    return false;
  }
  return true;
} 
/*  */

/* daniel odar 21-06-23 */
function upload_form(usuario_act, upload_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "upload/upload_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: usuario_act, // PUEDE SER: L, I, M , E
          upload_id: upload_id,
          modulo_nom: 'garantia_fijarprecio', //nombre de la tabla a relacionar
          modulo_id: upload_id, //aun no se guarda este modulo
          upload_uniq: $('#upload_uniq').val() //ID temporal
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
          $('#modal_mensaje').modal('hide');
          if (data != 'sin_datos') {
              $('#div_modal_upload_form').html(data);
              $('#modal_registro_upload').modal('show');

              //funcion js para agregar un largo automatico al modal, al abrirlo
              modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
              modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
          }
      },
      error: function (data) {
          alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
          swal_error('ERRROR', data.responseText, 5000);
      }
  });
}

function actualizar_fotos_fijarprecio(garantia_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "garantia/garantia_controller.php",
    async: true,
    dataType: "json",
    data: {
      garantia_id: garantia_id,
      action: 'galeria_fijarprecio'
    },
    success: function (data) {
      $('#galeria_fijarprecio').html(data.galeria);
    }
  });
}