<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'garantia/Garantia.class.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'garantiafile/Garantiafile.class.php');
    require_once(VISTA_URL . 'creditomenor/Creditomenor.class.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../garantia/Garantia.class.php');
    require_once('../funciones/funciones.php');
    require_once('../garantiafile/Garantiafile.class.php');
    require_once('../creditomenor/Creditomenor.class.php');
}
$oGarantia = new Garantia();
$oGarantiafile = new Garantiafile();
$oCreditomenor = new Creditomenor();

$creditomenor_id = (isset($_POST['creditomenor_id'])) ? $_POST['creditomenor_id'] : intval($creditomenor_id); //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$result = $oCreditomenor->mostrarUno($creditomenor_id);
if ($result['estado'] == 1) {
    $credito_est = intval($result['data']['tb_credito_est']); //1 pendiente, 2 aprobado, 3 vigente
    $credito_xac = intval($result['data']['tb_credito_xac']);
}
$result = NULL;
?>
<table id="tbl_garantias" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">CANT</th>
            <?php if($creditomenor_id==0){?>
            <th id="tabla_cabecera_fila">CREDITO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <?php }?>
            <th id="tabla_cabecera_fila" width="20%">TIPO | PRODUCTO | ATRIBUTOS</th>
            <th id="tabla_cabecera_fila" width="30%">DETALLE</th>
            <th id="tabla_cabecera_fila">VALOR</th>
            <th id="tabla_cabecera_fila" width="35%">FOTOS</th>
            <th id="tabla_cabecera_fila">WEB</th>
            <th id="tabla_cabecera_fila"width="8%"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $garantia_tot = 0; //valor total de las garantías
        $result = $oGarantia->listar_garantias($creditomenor_id);
        if ($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value):
                $credito_est = '';

                $result1 = $oCreditomenor->mostrarUno($value['tb_credito_id']);
                if ($result1['estado'] == 1) {
                    $credito_est = intval($result1['data']['tb_credito_est']); //1 pendiente, 2 aprobado, 3 vigente
                    $credito_xac = intval($result1['data']['tb_credito_xac']);
                }
                $result1 = NULL;
                
                //1 pendiente,2 aprobado,3 vigente,4 liquidado,5 remate,6 vendido, estado 7 liquidado pendiente de envio
                
                
                if($credito_est==1){
                    $estado='<span class="badge bg-primary">pendiente</span>';
                }
                if($credito_est==2){
                    $estado='<span class="badge bg-yellow">aprobado</span>';
                }
                if($credito_est==3){
                    $estado='<span class="badge bg-green">Vigente</span>';
                }
                if($credito_est==4){
                    $estado='<span class="badge bg-blue">liquidado</span>';
                }
                if($credito_est==5){
                    $estado='<span class="badge bg-danger">remate</span>';
                }
                if($credito_est==6){
                    $estado='<span class="badge bg-purple">vendido</span>';
                }
                if($credito_est==7){
                    $estado='<span class="badge bg-teal">Por Entregar</span>';
                }
                
                
                ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $value['tb_garantia_can']; ?></td>
                    <?php if($creditomenor_id==0){?>
                    <td id="tabla_fila"><?php echo $value['tb_credito_id']; ?></td>
                    <td id="tabla_fila"><?php echo $estado; ?></td>
                    <?php }?>
                    <td id="tabla_fila"><?php echo $value['tb_garantiatipo_nom'] . ' | ' . $value['tb_garantia_pro']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_garantia_det']; ?></td>
                    <td id="tabla_fila">
                        <?php
                        $garantia_tot += floatval($value['tb_garantia_val']);
                        echo mostrar_moneda($value['tb_garantia_val']);
                        ?>
                    </td>
                    <td id="tabla_fila">
                        <?php
                        $result2 = $oGarantiafile->listar_garantiafiles($value['tb_garantia_id']);
                        if ($result2['estado'] == 1) {
                            foreach ($result2['data'] as $key => $value2) {
                                echo '<img style="cursor: pointer;" src="' . $value2['tb_garantiafile_url'] . '" width="60" height="40" onclick="carousel(\'garantiafile\', ' . $value['tb_garantia_id'] . ')">';
                            }
                        }
                        ?>    
                    </td>
                    <td id="tabla_fila">
                        <a class="btn btn-info btn-xs" target="_blank" href="<?php echo $value['tb_garantia_web']; ?>"><i class="fa fa-link"></i></a>
                    </td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-info btn-xs" title="Ver" onclick="garantia_form(<?php echo "'L', " . $value['tb_garantia_id']; ?>)"><i class="fa fa-eye"></i></a> 
                        <a class="btn btn-success btn-xs" title="Pagos" onclick="creditomenor_pagos(<?php echo $value['tb_credito_id'];?>)"><i class="fa fa-money"></i></a> 
                        <a class="btn btn-primary btn-xs" title="Ver Credito" onclick="creditomenor_form(<?php echo "'L', ".$value['tb_credito_id'];?>)"><i class="fa fa-eye"></i></a> 
                        <?php if (in_array($credito_est, array(1, 5))): ?>
                            <a class="btn btn-warning btn-xs btn_garantia_tabla" title="Editar" onclick="garantia_form(<?php echo "'M', " . $value['tb_garantia_id'] . ", ". $credito_est; ?>)"><i class="fa fa-edit"></i></a> 
                        <?php endif;?>
                        <?php if ($credito_xac == 0): ?>
                            <a class="btn btn-danger btn-xs btn_garantia_tabla" title="Eliminar" onclick="garantia_form(<?php echo "'E', " . $value['tb_garantia_id']; ?>)"><i class="fa fa-trash"></i></a> 
                        <?php endif; ?>
                    </td>
                </tr>
                <?php
            endforeach;
        }
        $result = NULL;
        ?>
    </tbody>
    <input type="hidden" id="hdd_garantia_tot" value="<?php echo $garantia_tot; ?>">
</table>
