function garantia_tipo_mostrar_ocultar() {
    var garantiatipo_id = parseInt($('#cmb_garantiatipo_id').val());

    /*daniel odar*/
    if(garantiatipo_id != 0 && garantiatipo_id < 5){
        $("#row_articulo").show(300);
    }else{
        $("#row_articulo").hide(300);
    }
    /**/
    if (garantiatipo_id == 2) {
        $('.garantia_oro').show(300);
        $('.garantia_serie').hide(300);
    } else if (garantiatipo_id == 1) {
        $('.garantia_oro').hide(300);
        $('.garantia_serie').show(300);
    } else {
        $('.garantia_oro').hide(300);
        $('.garantia_serie').hide(300);
    }
}
function garantiafile_form(usuario_act, garantia_id, garantiafile_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "garantiafile/garantiafile_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            garantia_id: garantia_id,
            garantiafile_id: garantiafile_id,
            vista: 'garantia'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_garantiafile_form').html(data);
                $('#modal_registro_garantiafile').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_garantiafile'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_garantiafile'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_garantiafile', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'garantiafile';
                var div = 'div_modal_garantiafile_form';
                permiso_solicitud(usuario_act, garantia_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
function garantiafile_form_referencia(usuario_act, garantia_id, garantiafilecredito_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "garantiafilecredito/garantiafilecredito_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            garantia_id: garantia_id,
            garantiafilecredito_id: garantiafilecredito_id,
            vista: 'garantia'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_garantiafilecredito_form').html(data);
                $('#modal_registro_garantiafilecredito').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_garantiafilecredito'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_garantiafilecredito'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_garantiafilecredito', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'garantiafilecredito';
                var div = 'div_modal_garantiafilecredito_form';
                permiso_solicitud(usuario_act, garantia_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {
        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}
function garantiafile_galeria() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "garantiafile/garantiafile_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            garantia_id: $('#hdd_garantia_id').val()
        }),
        beforeSend: function () {
            $('#garantiafile_galeria').html('Cargando imágenes...');
        },
        success: function (data) {
            $('#garantiafile_galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#garantiafile_galeria').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}
function garantiafilecredito_galeria() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "garantiafilecredito/garantiafilecredito_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            garantia_id: $('#hdd_garantia_id').val()
        }),
        beforeSend: function () {
            $('#garantiafilecredito_galeria').html('Cargando imágenes...');
        },
        success: function (data) {
            $('#garantiafilecredito_galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#garantiafilecredito_galeria').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}
function cargar_modelo(marca_id) {
    $.ajax({
      type: "POST",
      url: VISTA_URL + "vehiculomodelo/vehiculomodelo_select.php",
      async: true,
      dataType: "html",
      data: {
        vehiculomarca_id: marca_id,
      },
      beforeSend: function () {
        $("#cmb_vehiculomodelo_id").html('<option value="">Cargando...</option>');
      },
      success: function (html) {
        $("#cmb_vehiculomodelo_id").html(html);
      },
    });
  }
$(document).ready(function () {
    console.log('cambios form garantia 222222')
    $('#btn_add_articulo').hide();
    garantia_tipo_mostrar_ocultar();

    $('#txt_garantia_can').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0',
        vMax: '50'
    });

    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.99'
    });

    $('.moneda2').autoNumeric({
        aSep: '',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0',
        vMax: '999999999'
    });

    $("#cmb_vehiculomarca_id").change(function () {
        cargar_modelo($(this).val());
    });

    /*daniel odar 06-03-23*/
    $('#cmb_garantiatipo_id').change(function (event) {
        garantia_tipo_mostrar_ocultar();
    });
    
    $("#cbo_articulo").change(function () {
        var articulo_id = $("#cbo_articulo option:selected").val();
        if (articulo_id == "0") {
            $("#col_add_articulo").show(200);
        } else {
            $("#col_add_articulo").hide(200);
        }
    });

    /* daniel odar 07-03-2023 */
    if( $("#hdd_mod_articulo").val() == 1 ){
        disabled( $('#elementos').find($("input, select, textarea")).not(':input[class*="nodisabled"]') );
    }
    if (parseInt($('#hdd_grupousuario_id').val()) === 2) {
        disabled( $('#elementos').find($("button")).not('#btn_add_articulo, #btn_subir_fotos, #btn_garantia_file'));
    } else {
        disabled( $('#elementos').find($("button")).not('#btn_subir_fotos, #btn_garantia_file') );
    }

    if($("#action_form_garantia").val() == "leer"){
        if($("#hdd_articulo_id").val() == -2 || $("#hdd_articulo_id").val() == 0){
            $("#span_falta_asignar").show();
        }
        disabled($('#form_garantia').find($('#btn_add_articulo')));
    }
    /**/

    /* daniel odar 14-04-2023 */
    $("#cbo_articulo option[value=0]").hide();
    $('#cbo_articulo').selectpicker({
        liveSearch: true,
        maxOptions: 1,
        language: 'ES'
    });
    $('button[data-id="cbo_articulo"]').addClass('btn-sm');
    $("#cbo_articulo").change(function () {
        
        var articulo_id = $("#cbo_articulo option:selected").val();
        if (articulo_id == "0") {
            $("#col_add_articulo").show(200);
        } else {
            $("#col_add_articulo").hide(200);
            $('#txt_articulo_nom').val('');
        }

        if(articulo_id == 75)
            $('.datos_vehiculo').show(300);
        else
            $('.datos_vehiculo').hide(300);
    });
    /* if ($("#hdd_articulo_id").val()) {
        disabled($('#cbo_articulo, #btn_add_articulo'));
    } */
    /*  */


    $('#form_garantia').validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "garantia/garantia_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_garantia").serialize(),
                beforeSend: function () {
                    $('#garantia_mensaje').show(400);
                    $('#btn_guardar_garantia').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        $('#garantia_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#garantia_mensaje').html(data.mensaje);

                        var vista = $('#garantia_vista').val();

                        setTimeout(function () {
                            if (vista == 'garantia' || vista == 'creditomenor')
                                garantia_tabla();
                            $('#modal_registro_garantia').modal('hide');
                        }, 1000);
                    } else {
                        $('#garantia_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#garantia_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_garantia').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $('#garantia_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#garantia_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            cmb_garantiatipo_id: {
                min: 1
            },
            cmb_garantia_estrang: {
                min: 2
            },
            txt_garantia_pro: {
                required: true
            },
            txt_garantia_val: {
                required: true
            },
            txt_garantia_valtas: {
                required: true
            },
            txt_garantia_ser: {
                required: function () {
                    if (parseInt($('#cmb_garantiatipo_id').val()) == 1)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_kil: {
                required: function () {
                    if (parseInt($('#cmb_garantiatipo_id').val()) == 2)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_pes: {
                required: function () {
                    if (parseInt($('#cmb_garantiatipo_id').val()) == 2)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_tas: {
                required: function () {
                    if (parseInt($('#cmb_garantiatipo_id').val()) == 2)
                        return true;
                    else
                        return false;
                }
            },
            cmb_garantia_calidad: {
                required: function () {
                    if (parseInt($('#cmb_garantiatipo_id').val()) == 2)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_det: {
                required: true
            },
            txt_garantia_web: {
                required: true
            },
            cbo_articulo: {
                min: -1
            },
            txt_articulo_nom: {
                required: function(){
                    return parseInt($('#cbo_articulo').val()) == 0;
                }
            },
            cmb_vehiculomarca_id:{
                min: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return 1;
                    else
                        return 0;
                }
            },
            cmb_vehiculomodelo_id:{
                min: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return 1;
                    else
                        return 0;
                }
            },
            cmb_vehiculoclase_id:{
                min: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return 1;
                    else
                        return 0;
                }
            },
            txt_garantia_vehpla:{
                required: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_vehsermot:{
                required: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_vehsercha:{
                required: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_vehano:{
                required: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return true;
                    else
                        return false;
                }
            },
            txt_garantia_vehkil:{
                required: function () {
                    if (parseInt($("#cbo_articulo option:selected").val()) == 75)
                        return true;
                    else
                        return false;
                }
            }
        },
        messages: {
            cmb_garantiatipo_id: {
                min: "Seleccione un tipo"
            },
            cmb_garantia_estrang: {
                min: "Selecciona el estado del Producto"
            },
            txt_garantia_pro: {
                required: "Ingrese el nombre del producto"
            },
            txt_garantia_val: {
                required: "Ingrese el monto a prestar"
            },
            txt_garantia_valtas: {
                required: "Ingrese el valor de tasación"
            },
            txt_garantia_ser: {
                required: "Ingrese la serie del producto"
            },
            txt_garantia_kil: {
                required: "Ingrese los kilates"
            },
            txt_garantia_pes: {
                required: "Ingrese el peso"
            },
            txt_garantia_tas: {
                required: "Ingrese nombre de tasador"
            },
            cmb_garantia_calidad: {
                required: "Ingrese la calidad de joyas"
            },
            txt_garantia_det: {
                required: "Ingrese un detalle para el producto"
            },
            txt_garantia_web: {
                required: "Ingrese una página web de referencia"
            },
            cbo_articulo: {
                min: "Seleccione tipo artículo"
            },
            txt_articulo_nom: {
                required: "Debe ingresar nombre de artículo"
            },
            cmb_vehiculomarca_id:{
                min: "Debe seleccionar Marca"
            },
            cmb_vehiculomodelo_id:{
                min: "Debe seleccionar Modelo"
            },
            cmb_vehiculoclase_id:{
                min: "Debe seleccionar Carrocería"
            },
            txt_garantia_vehpla:{
                required: "Debe ingresar nombre de artículo"
            },
            txt_garantia_vehsermot:{
                required: "Debe ingresar nombre de artículo"
            },
            txt_garantia_vehsercha:{
                required: "Debe ingresar nombre de artículo"
            },
            txt_garantia_vehano:{
                required: "Debe ingresar nombre de artículo"
            },
            txt_garantia_vehkil:{
                required: "Debe ingresar nombre de artículo"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if( element.parents("#div_cbo_articulo").length == 1 ){
                error.insertAfter(element.parents("#div_cbo_articulo")[0]);
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

    
});

/* daniel odar 14-04-2023 */
function añadir_articulo(){
    $('ul.dropdown-menu li[data-original-index=8] > a').css('display', '');
    $('#cbo_articulo').val(0).change();
    $("#col_add_articulo").show(200);
    $('#txt_articulo_nom').focus();
}