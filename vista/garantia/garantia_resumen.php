<?php

use function PHPSTORM_META\type;

  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../garantia/Garantia.class.php');
  $oGarantia = new Garantia();
  require_once('../creditomenor/Creditomenor.class.php');
  $oCreditomenor = new Creditomenor();
  require_once('../menorgarantia/Ventagarantia.class.php');
  $oVentagarantia = new Ventagarantia();
  require_once('../menorgarantia/Ventainterna.class.php');
  $oVentainterna = new Ventainterna();
  require_once('../ingreso/Ingreso.class.php');
  $oIngreso = new Ingreso();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../upload/Upload.class.php');
  $oUpload = new Upload();

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'garantia';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuariogrupo_id = intval($_SESSION['usuariogrupo_id']); // 2 gerencia, 3 ventas
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $garantia_id = $_POST['garantia_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'I')
    $titulo = 'Resumen Garantía';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en garantia
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'garantia'; $modulo_id = $garantia_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del garantia por su ID
    $garantia_can = 1;
    $credito_int = 0;
    $credito_est = 0;

    if(intval($garantia_id) > 0){
      $result = $oGarantia->mostrarUno($garantia_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el garantia seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $array_tipo = array(1 => 'Electro', 2 =>'Oro', 3 =>'Varios', 6 =>'Sin Garantía');
          $garantia_est = $result['data']['tb_garantia_est']; // 0 disponible y 1 vendido
          $garantia_tip = $result['data']['tb_garantia_tip'];
          $credito_id = $result['data']['tb_credito_id'];
          $garantiatipo_id = $result['data']['tb_garantiatipo_id'];//1 electro, 2 oro, 3 otros, 6 sin gar
          $garantia_can = $result['data']['tb_garantia_can'];
          $garantia_pro = $result['data']['tb_garantia_pro'];
          $garantia_val = $result['data']['tb_garantia_val'];
          $garantia_prefij = $result['data']['tb_garantia_prefij']; //si es diferente de 0 fue fijado
          $garantia_valtas = $result['data']['tb_garantia_valtas'];
          $garantia_ser = $result['data']['tb_garantia_ser'];
          $garantia_kil = $result['data']['tb_garantia_kil']; //kilates para oro
          $garantia_pes = $result['data']['tb_garantia_pes']; //peso para oro
          $garantia_grab = $result['data']['tb_garantia_grab']; //peso para oro
          $garantia_med = $result['data']['tb_garantia_med']; //peso para oro
          $garantia_tas = $result['data']['tb_garantia_tas']; //tasador de oro
          $garantia_det = $result['data']['tb_garantia_det']; //detalle
          $garantia_web = $result['data']['tb_garantia_web'];
          $num_cuotas = $result['data']['tb_garantia_num_cuota_rem']; // numero de cuotas maxima para remate
          $almacen_id = intval($result['data']['tb_almacen_id']);
          $almacenes = array(1=> 'Oficina Boulevard', 2=> 'Oficina Mall', 3=> 'Almacen General');
          $almacen_nombre = $almacenes[$almacen_id];

          $result2 = $oCreditomenor->mostrarUno($credito_id);
            if($result2['estado'] == 1){
              $credito_reg = $result2['data'];
              $credito_xac = intval($result2['data']['tb_credito_xac']);
              $credito_int = $result2['data']['tb_credito_int'];
              $credito_est = $result2['data']['tb_credito_est']; // 3 vigente, 4 liquidado, 5 remate, 6 vendido
              $cliente_nombre = $result2['data']['tb_cliente_nom'];
              $usuario_reg = intval($result2['data']['tb_credito_usureg']);
              
              $empresa_id = intval($result2['data']['tb_empresa_id']);
              $empresa_nombre = 'Mall Aventura';

              if($empresa_id == 1)
                $empresa_nombre = 'Boulevard';
              if(intval($garantia_est) == 0)
                $titulo .= ' <span class="badge bg-green">Disponible para Vender</span>';
              if(intval($garantia_est) == 1)
                $titulo .= ' <span class="badge bg-purple">Garantía Vendida</span>';
            }
          $result2 = NULL;
          
          $result2 = $oUsuario->mostrarUno($usuario_reg);
            if($result2['estado'] == 1){
              $asesor_nombre = $result2['data']['tb_usuario_nom'];
            }
          $result2 = Null;

          //si la garantía fue vendida buscamos la información de venta
          $usuario_autoriza = '';
          $usuario_vendedor = '';
          $cliente_comprador = '';
          $precio_vendido = 0;
          $ingreso_id = 0;

          if($garantia_est == 1){
            $result2 = $oVentagarantia->mostrar_garantia_vendida($garantia_id);
              if($result2['estado'] == 1){
                $ventagrantia_fecreg = mostrar_fecha_hora($result2['data']['tb_ventagarantia_fecreg']);
                $usuario_autoriza = $result2['data']['autorizador'];
                $usuario_vendedor = $result2['data']['colaborador'];
                $cliente_comprador = $result2['data']['cliente'];
                $precio_vendido = $result2['data']['tb_ventagarantia_prec2'];
                $forma_venta = $result2['data']['tb_ventagarantia_col']; // si es 0 se vendió a un cliente, si es > 0 fue comprador por colaborador
                $detalle_venta = '';

                if(intval($forma_venta) == 0){ //la garantia fue vendido a aun cliente particular
                  $usuario_vendedor = $usuario_autoriza; //en venta directo a cliente no autoriza un usuario superior
                  $result3 = $oIngreso->mostrar_ingresos_modulo(90, $garantia_id); // 90 es el modulo de venta de garantia
                  if($result3['estado'] == 1){
                      foreach ($result3['data'] as $key => $value) {
                        $ingreso_id = $value['tb_ingreso_id'];
                        $detalle_venta = $value['tb_ingreso_det'].' VENDIDA EL <b>'.$ventagrantia_fecreg.'</b>';
                      }
                    }
                  $result3 = NULL;
                }
                if(intval($forma_venta) > 0){ //la garantia fue comprada por un colaborador
                  $result3 = $oVentainterna->mostrar_venta_interna_garantia($garantia_id);
                    if($result3['estado'] == 1){
                      $detalle_venta = 'Autorizado por: <b>'.$usuario_autoriza.'</b>. '.$result3['data']['tb_ventainterna_his'];
                    }
                  $result3 = NULL;
                }
              }
            $result2 = NULL;
          }
        }
      $result = NULL;

      /* daniel odar: buscar gastos de esta garantia */
      $monto_gasto_sol = 0;
      $monto_gasto_dol = 0;
      require_once('../gastogar/Gastogar.class.php');
      $oGastogar = new Gastogar();
      $parametros[0]['column_name'] = 'tb_gastogar_xac';
      $parametros[0]['param0'] = 1;
      $parametros[0]['datatype'] = 'INT';

      $parametros[1]['column_name'] = 'tb_garantia_idfk';
      $parametros[1]['param1'] = $garantia_id;
      $parametros[1]['datatype'] = 'INT';

      $res = $oGastogar->listar_todos($parametros, array());
      if($res['estado'] == 1){
        foreach($res['data'] as $key => $gasto){
          if($gasto['tb_moneda_idfk'] == 1){
            $monto_gasto_sol += $gasto['tb_gastogar_monto'];
          } else {
            $monto_gasto_dol += $gasto['tb_gastogar_monto'];
          }
        }
      }
      $res = null;
      
      /* daniel odar: mostrar capital devuelto por cliente */
      $monto_interes_pagado = 0; $cuotas_pagadas = 0;
      $capital_original = $credito_reg['tb_credito_preaco'];
      $capital_cubierto = 0;
      $capital_falta_pagar = 0;
      $monto_interes_mensual_total = $credito_reg['tb_credito_linapr'] - $credito_reg['tb_credito_preaco'];
      $monto_total_cuota_cajacliente = 0;
      require_once('../cuota/Cuota.class.php');
      $oCuota = new Cuota();
      require_once('../cuotapago/Cuotapago.class.php');
      $oCuotapago = new Cuotapago();
      #---------------------------------------------------------------------
      if($credito_reg['tb_cuotatipo_id'] == 1){ //cuota libre "CL"
        $tipo_cred_nom = 'C. Libre';
        $res = $oCuota->listar_cuotas_credito('tb_creditomenor', 1, $credito_id, '1,3');//estado 1,3 pendiente o pago parcial. devuelve siempre 1 registro con CL
        
        if($res['estado'] == 1){
          $capital_falta_pagar = $res['data'][0]['tb_cuota_cap'];
        }

        $capital_cubierto = $capital_original - $capital_falta_pagar;
        $res = null;

        $res = $oCuota->listar_cuotas_credito('tb_creditomenor', 1, $credito_id, '2,3');

        if($res['estado'] == 1){
          foreach($res['data'] as $key => $cuota){ // por cada cuota buscaremos sus cuotapago
            $cuotas_pagadas++;
            $monto_total_pagado_cuota = 0;
            if($cuota['tb_cuota_est'] == 2){
              $monto_interes_pagado += $cuota['tb_cuota_int'];
            } else { //estado 3 pago pacial
              $res1 = $oCuotapago->filtrar(1, $cuota['tb_cuota_id']);
              if ($res1['estado'] == 1) {
                foreach ($res1['data'] as $key => $cuotapago) {
                  $monto_total_pagado_cuota += $cuotapago['tb_cuotapago_mon'];
                }
                $monto_interes_pagado += $monto_total_pagado_cuota;
              }
            }
          }
        }
        $res = null;
      } elseif($credito_reg['tb_cuotatipo_id'] == 2){
        $tipo_cred_nom = 'C. Fija';
        $res = $oCuota->listar_cuotas_credito('tb_creditomenor', 1, $credito_id, '2,3');//estado 1,3 pendiente o pago parcial. devuelve siempre 1 registro con CL
        
        if($res['estado'] == 1){
          $cuotas_pagadas = $res['cantidad'];
          foreach($res['data'] as $key => $cuota){
            $monto_interes_cuota = $cuota['tb_cuota_int'];

            $res1 = $oCuotapago->filtrar(1, $cuota['tb_cuota_id']);
            $monto_total_pagado_cuota = 0;
            $monto_total_pagado_cuota_credito = 0;
            if($res1['estado'] == 1){
              foreach($res1['data'] as $key => $cuotapago){
                $monto_total_pagado_cuota += $cuotapago['tb_cuotapago_mon'];
                
                $res2 = $oIngreso->mostrar_ingresos_modulo(30, $cuotapago['tb_cuotapago_id']);
                $monto_total_pagado_cuota_credito += $res2['data'][0]['tb_ingreso_imp'];
                $res2 = null;
              }
            }
            $res1 = null;
            if($cuota['tb_cuota_est'] == 3 && $monto_total_pagado_cuota < $monto_interes_cuota){
              $monto_interes_pagado += $monto_total_pagado_cuota_credito;
            } else {
              $monto_interes_pagado += $monto_interes_cuota;
              $capital_cubierto += ($monto_total_pagado_cuota_credito - $monto_interes_cuota);
              if($monto_total_pagado_cuota > $monto_total_pagado_cuota_credito){
                $monto_total_cuota_cajacliente += $monto_total_pagado_cuota - $monto_total_pagado_cuota_credito; 
              }
            }
          }
        }
        $res = null;
      }
    }

    $precio_venta = 0;
    if($_POST['formenor_rec'] > 0 && $_POST['formenor_pro'] >= 0){
      $formenor_rec = $_POST['formenor_rec'];
      $formenor_pro = $_POST['formenor_pro'];

      $precio_venta = mostrar_moneda(($garantia_val*$formenor_rec) + ($garantia_val*$formenor_pro*($credito_int/100)));

      if($garantia_prefij > 0)
        $precio_venta = mostrar_moneda($garantia_prefij);
    }
    else{
      $bandera = 4;
      $mensaje = '<h2>NO SE HA SELECCIONADO UNA FÓRMULA PARA LA VENTA O EXISTE MÁS DE UNO EN ESTADO VIGENTE. CONSULTE CON ADMISTRADOR DE SISTEMA</h2>';
    }
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_garantia_resumen" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_garantia_resumen" method="post">
          <input type="hidden" id="garantia_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="action" value="fijar_precio">
          <input type="hidden" name="hdd_garantia_id" id="hdd_garantia_id" value="<?php echo $garantia_id;?>">
          <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id;?>">

          <div class="modal-body">
            <div class="box box-info box-solid">
              <div class="box-header">
                <h3 class="box-title">Resumen Garantía: <?php echo $array_tipo[$garantiatipo_id];?></h3>
              </div>
              <div class="box-body" style="overflow-x: auto;">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>Asesor y Sede:</th>
                      <td colspan="2"><?php echo ucwords(mb_strtolower($asesor_nombre, 'UTF-8')).' | Recibido en: <b>'.$empresa_nombre.'</b>';?></td>
                      <td><b>Ubicación:</b> <?php echo $almacen_nombre;?></td>
                    </tr>
                    <tr>
                      <th>Cliente:</th>
                      <td colspan="3"><?php echo $cliente_nombre;?></td>
                    </tr>
                    <tr>
                      <th>Producto:</th>
                      <td colspan="3"><?php echo $garantia_pro;?></td>
                    </tr>
                    <tr>
                      <th>Detalle:</th>
                      <td colspan="3"><?php echo $garantia_det;?></td>
                    </tr>
                    <!-- MOSTRAR SI LA GARANTIA ES ORO-->
                    <?php if($garantiatipo_id == 2):?>
                      <tr>
                        <th>Datos de Oro:</th>
                        <td colspan="3"><?php echo 'Kilates: '.$garantia_kil.' | Gramos: '.$garantia_pes.' | Grabado: '.$garantia_grab;?></td>
                      </tr>
                    <?php endif; ?>

                    <tr>
                      <th>Página Referencial:</th>
                      <td colspan="3"><a href="<?php echo $garantia_web;?>" target="_blank">Click aquí para ver página de referencia</a></td>
                    </tr>
                    <tr>
                      <th>Monto Prestado <?php echo "<b>($tipo_cred_nom)</b>";?>:</th>
                      <td><?php echo 'S/. '.mostrar_moneda($garantia_val);?></td>
                      <td><b>Monto interes<?php echo $credito_reg['tb_cuotatipo_id'] == 1 ? ' mensual crédito:' : ' total crédito:' ?></b> <?php echo "S/. ".mostrar_moneda($monto_interes_mensual_total);?></td>
                      <td><?php if($credito_reg['tb_cuotatipo_id'] == 2){
                        echo "<b>Caja cliente:</b> S/. ".mostrar_moneda($monto_total_cuota_cajacliente);
                      }?></td>
                    </tr>
                    <tr>
                      <th>Capital recuperado:</th>
                      <td><?php echo 'S/. '. mostrar_moneda($capital_cubierto);?></td>
                      <td><b>Intereses pagados:</b> <?php echo 'S/. '. mostrar_moneda($monto_interes_pagado);?></td>
                      <td><b>N° Cuotas pagadas:</b> <?php echo "$cuotas_pagadas";?></td>
                    </tr>
                    <tr>
                      <th>Monto total gastos:</th>
                      <td colspan="3"><?php echo "S/. ". mostrar_moneda($monto_gasto_sol) ." <b>|</b> US$ ". mostrar_moneda($monto_gasto_dol);?></td>
                    </tr>
                    <tr>
                      <th>Precio de Venta:</th>
                      <td colspan="3"><?php echo 'S/. '.mostrar_moneda($precio_venta);?></td>
                    </tr>
                    <!-- GERSON (16-01-25) -->
                    <tr>
                      <th>Fijar Nuevo Precio:</th>
                      <td colspan="2" align="center">
                        <div class="d-flex flex-wrap">
                          <div class="col-md-4 col-sm-12 d-flex flex-column">
                            <input type="text" style="max-width: 200px;" name="txt_garantia_nuevo_monto" id="txt_garantia_nuevo_monto" class="form-control input-sm" value="<?php echo mostrar_moneda($precio_venta);?>">
                          </div>
                          <div class="col-md-3 col-sm-12 badge d-flex flex-column justify-content-center align-items-center" style="background-color: #fff">
                            <div class="d-flex justify-content-center align-items-center">
                              <?php
                                if($garantia_prefij > 0)
                                    echo '<span class="badge bg-green">Precio Fijado</span>';
                                else
                                    echo '<b>Fórmula Campaña</b>';
                              ?>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12 d-flex flex-column justify-content-center align-items-center">
                            <div class="d-flex justify-content-center align-items-center mt-2 mt-md-0">
                              <button type="button" class="btn bg-black btn-sm" onclick="subasta_form(<?php echo $garantia_id;?>)">Subastar Producto</button>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <!--  -->
                    <!-- GERSON (03-05-23) -->
                    <?php if($_SESSION['usuario_id']==2 || $_SESSION['usuario_id']==11 || $_SESSION['usuario_id']==61){ ?>
                    <tr>
                      <th>Núm. Máx. Cuotas:</th>
                      <td>
                        <input type="text" style="max-width: 100px;" name="txt_garantia_num_cuota" id="txt_garantia_num_cuota" onkeypress="return solo_numero(event)" class="form-control input-sm" value="<?php echo $num_cuotas;?>">
                      </td>
                    </tr>
                    <?php } ?>
                    <!--  -->
                  </tbody>
                </table>
                <!-- PARA MOSTRAR LAS FOTOS DE LA GARANTÍA-->
                <p></p>
                <div class="box box-success shadow" style="padding: 2px;">
                  <div class="box-header with-border" style="border-bottom-color: #e5e5e5; padding-top: 9px; padding-bottom: 6px;">
                      <h1 class="box-title" style="font-size: 14px; font-weight: bold;">Fotos de la garantía</h1>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      </div>
                  </div>

                  <div class="box-body" style="padding-bottom: 1px;">
                    <div class="row" id="garantiafile_galeria">
                      <?php require('../garantiafile/garantiafile_galeria.php'); ?>
                    </div>
                  </div>
                </div>

                <!-- PARA MOSTRAR LAS FOTOS DE FIJAR PRECIO-->
                <?php if($_SESSION["usuariogrupo_id"] == 2){ ?>
                  <br>
                  <div class="box box-success shadow" style="padding: 2px;">
                    <div class="box-header with-border" style="border-bottom-color: #e5e5e5; padding-top: 9px; padding-bottom: 6px;">
                        <h1 class="box-title" style="font-size: 14px; font-weight: bold;">Fotos de Fijar precio</h1>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body" style="padding-bottom: 1px;">
                      <div class="row form-group">
                        <div class="col-md-2" style="width: 12%;">
                          <button type="button" class="btn btn-xs btn-primary" onclick="upload_form('I', <?php echo $garantia_id;?>)"><i class="fa fa-plus"></i> Subir imagen</button>
                        </div>
                        <div class="col-md-1">
                          <button type="button" class="btn btn-xs btn-info" onclick="actualizar_fotos_fijarprecio(<?php echo $garantia_id;?>)"><i class="fa fa-refresh"></i> Actualizar</button>
                        </div>
                      </div>
                      <div class="row form-group" id="galeria_fijarprecio">
                      </div>
                    </div>
                  </div>
                <?php } ?>

              </div>
            </div>
            <!-- BOX PARA LA INFO DE VENTA, SOLO SI SE VENDIO garantia_est = 1 --->
            <?php if(intval($garantia_est) == 1): ?>
              <div class="box box-warning box-solid">
                <div class="box-header">
                  <h3 class="box-title">Información de venta</h3>
                </div>
                <div class="box-body">
                <table class="table">
                    <tbody>
                      <tr>
                        <th>Vendedor</th>
                        <th>Detalle:</th>
                        <th width="100">Precio Venta</th>
                      </tr>
                      <tr>
                        <td><?php echo $usuario_vendedor;?></td>
                        <td><?php echo $detalle_venta;?></td>
                        <td><?php echo 'S/. '.mostrar_moneda($precio_vendido);?></td>
                      </tr>
                    </tbody>
                  </table>
                  <p></p>
                  <div class="row" id="ingreso_galeria">
                      <?php
                        $modulo_nom = 'ingreso';
                        $modulo_id = $ingreso_id;
                        if($forma_venta == 0){
                          $result = $oUpload->listar_uploads($modulo_nom, $modulo_id);
                            if($result['estado'] == 1){
                              foreach ($result['data'] as $key => $value) {
                                echo '
                                  <div class="col-xs-4 col-sm-4">
                                    <a href="'.$value['upload_url'].'" target="_blank"><img class="img-responsive" src="'.$value['upload_url'].'" style="height:73px; width:120px;"></a>
                                  </div>
                                ';
                              }
                            }
                            else{
                              echo '
                                <div class="col-md-12">
                                  <div class="alert alert-error alert-dismissible">
                                    <h4>
                                      <i class="icon fa fa-info"></i> Esta venta no tiene imágenes de voucher y dni del cliente
                                    </h4>
                                  </div>
                                </div>
                              ';
                            }
                          $result = NULL;
                        }
                      ?>
                  </div>
                </div>
              </div>
            <?php endif; ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="garantia_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' && $garantia_est == 0 && $usuariogrupo_id == 2): //solo gerencia puede cambiar el precio?>
                <button type="submit" class="btn btn-info" id="btn_guardar_garantia_resumen">Guardar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_garantia_resumen">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<div id="div_modal_garantiafile_form"></div>
<div id="div_modal_garantiafilecredito_form"></div>

<script type="text/javascript" src="<?php echo 'vista/garantia/garantia_resumen.js?ver=040520231';?>"></script>