<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../creditomenor/Creditomenor.class.php');
$oCreditomenor = new Creditomenor();
require_once('../funciones/funciones.php');
require_once('../funciones/operaciones.php');

$direc = 'garantia';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$garantia_id = $_POST['garantia_id'];
$credito_id = intval($_POST['credito_id']);
$creditotipo_id = 1;
$vista = $_POST['vista'];
//daniel odar
$modificar_articulo = 0;
if ($usuario_action == 'M_uno') {
  $usuario_action = 'M';
  $modificar_articulo = 1;
}
/* */

$titulo = '';
if ($usuario_action == 'L') {
  $titulo = 'Garantía Registrado';
} elseif ($usuario_action == 'I') {
  $titulo = 'Registrar Garantía';
} elseif ($usuario_action == 'M') {
  $titulo = 'Editar Garantía';
} elseif ($usuario_action == 'E') {
  $titulo = 'Eliminar Garantía';
} else
  $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en garantia
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id'];
    $modulo = 'garantia';
    $modulo_id = $garantia_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $result = NULL;
      echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
      exit();
    }
    $result = NULL;
  }

  //si la accion es modificar, mostramos los datos del garantia por su ID
  $garantia_can = 1;
  $style_vehiculo = 'style="display: none;"';

  if (intval($garantia_id) > 0) {
    $result = $oGarantia->mostrarUno($garantia_id);
    if ($result['estado'] != 1) {
      $mensaje =  'No se ha encontrado ningún registro para el garantia seleccionado, inténtelo nuevamente.';
      $bandera = 4;
    } else {
      $garantia_tip = $result['data']['tb_garantia_tip'];
      $credito_id = $result['data']['tb_credito_id'];
      $garantiatipo_id = $result['data']['tb_garantiatipo_id'];
      $garantia_estrang = $result['data']['tb_garantia_estrang'];
      $garantia_can = $result['data']['tb_garantia_can'];
      $garantia_pro = $result['data']['tb_garantia_pro'];
      $garantia_val = $result['data']['tb_garantia_val'];
      $garantia_valtas = $result['data']['tb_garantia_valtas'];
      $garantia_ser = $result['data']['tb_garantia_ser'];
      $garantia_kil = $result['data']['tb_garantia_kil']; //kilates para oro
      $garantia_pes = $result['data']['tb_garantia_pes']; //peso para oro
      $garantia_grab = $result['data']['tb_garantia_grab']; //peso para oro
      $garantia_med = $result['data']['tb_garantia_med']; //peso para oro
      $garantia_tas = $result['data']['tb_garantia_tas']; //tasador de oro
      $garantia_det = $result['data']['tb_garantia_det']; //detalle
      $garantia_web = $result['data']['tb_garantia_web'];
      $articulo_id = $result['data']['tb_articulo_id'];
      $garantia_calidad = intval($result['data']['tb_garantia_calidad']);

      $vehiculomarca_id = $result['data']['tb_vehiculomarca_id'];
      $vehiculomodelo_id = $result['data']['tb_vehiculomodelo_id'];
      $vehiculoclase_id = $result['data']['tb_vehiculoclase_id'];
      $garantia_vehpla = $result['data']['tb_garantia_vehpla'];
      $garantia_vehsermot = $result['data']['tb_garantia_vehsermot'];
      $garantia_vehsercha = $result['data']['tb_garantia_vehsercha'];
      $garantia_vehano = $result['data']['tb_garantia_vehano'];
      $garantia_vehkil = $result['data']['tb_garantia_vehkil'];
      $garantia_vehcol = $result['data']['tb_garantia_vehcol'];

      $result2 = $oCreditomenor->mostrarUno($credito_id);
        if ($result2['estado'] == 1) {
          $credito_xac = intval($result2['data']['tb_credito_xac']);
        }
      $result2 = NULL;

      if($articulo_id == 75)
        $style_vehiculo = '';
    }
    $result = NULL;
  }
} else {
  $mensaje =  $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_garantia" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo; ?></h4>
        </div>
        <form id="form_garantia" method="post">
          <input type="hidden" id="garantia_vista" value="<?php echo $vista; ?>">
          <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_garantia_id" id="hdd_garantia_id" value="<?php echo $garantia_id; ?>">
          <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id; ?>">
          <!-- daniel odar añade -->
          <input type="hidden" name="action_form_garantia" id="action_form_garantia" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_mod_articulo" id="hdd_mod_articulo" value="<?php echo $modificar_articulo; ?>">
          <input type="hidden" name="hdd_grupousuario_id" id="hdd_grupousuario_id" value="<?php echo $_SESSION['usuariogrupo_id']; ?>">

          <div class="modal-body">
            <div class="row" id="elementos">
              <!-- -->
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cmb_garantiatipo_id" class="control-label">Tipo Garantía</label>
                      <select id="cmb_garantiatipo_id" name="cmb_garantiatipo_id" class="form-control input-sm">
                        <?php require_once('../garantiatipo/garantiatipo_select.php'); ?>
                      </select>

                      <!--label for="txt_garantia_can" class="control-label">Cantidad</label por ahora en hidden la cantidad-->
                      <input type="hidden" name="txt_garantia_can" id="txt_garantia_can" class="form-control input-sm" value="<?php echo $garantia_can; ?>" readonly>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cmb_garantia_estrang" class="control-label">Estado del Producto</label>
                      <select id="cmb_garantia_estrang" name="cmb_garantia_estrang" class="form-control input-sm">
                        <?php echo listar_options_rango_estado_garantia($garantia_estrang); ?>
                      </select>
                    </div>
                  </div>
                </div>

                <!-- DANIEL ODAR 06-03-2022 -->
                <div class="row" id="row_articulo">
                  <input type="hidden" name="hdd_articulo_id" id="hdd_articulo_id" value="<?php echo $articulo_id; ?>">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cbo_articulo">Seleccione artículo:</label>
                      <select class="form-control mayus input-sm nodisabled" name="cbo_articulo" id="cbo_articulo">
                        <?php require_once('../articulo/articulo_select.php'); ?>
                      </select>
                      <!-- <div id="div_cbo_articulo" class="input-group">
                        <span class="input-group-btn">
                          <button id="btn_add_articulo" class="btn btn-primary btn-sm<?php //echo $_SESSION['usuariogrupo_id'] != 2 ? " disabled" :"";?>" type="button" onclick="añadir_articulo()" title="AÑADIR ARTICULO">
                            <span class="fa fa-plus icon"></span>
                          </button>
                        </span>
                      </div> -->
                      <span id="span_falta_asignar" class="col-md-12 mayus input-sm" style="display: none; font-weight: bold;">falta asignar articulo</span>
                    </div>
                  </div>
                  <div class="col-md-6" id="col_add_articulo" style="display: none;">
                    <div class="form-group">
                      <label for="txt_articulo_nom">Ingrese nombre de artículo:</label>
                      <input type="text" name="txt_articulo_nom" id="txt_articulo_nom" class="form-control mayus input-sm nodisabled">
                    </div>
                  </div>
                </div>

                <div class="box-body shadow datos_vehiculo" <?php echo $style_vehiculo;?> >
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cmb_vehiculomarca_id" class="control-label">Marca</label>
                      <select name="cmb_vehiculomarca_id" id="cmb_vehiculomarca_id" class="form-control input-sm">
                        <?php require_once '../vehiculomarca/vehiculomarca_select.php'; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cmb_vehiculomodelo_id" class="control-label">Modelo</label>
                      <select name="cmb_vehiculomodelo_id" id="cmb_vehiculomodelo_id" class="form-control input-sm">
                        <?php require_once '../vehiculomodelo/vehiculomodelo_select.php'; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cmb_vehiculoclase_id" class="control-label">Carrocería</label>
                      <select name="cmb_vehiculoclase_id" id="cmb_vehiculoclase_id" class="form-control input-sm">
                        <?php require_once '../vehiculoclase/vehiculoclase_select.php'; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_vehpla" class="control-label">Placa</label>
                      <input type="text" name="txt_garantia_vehpla" id="txt_garantia_vehpla" class="form-control input-sm" value="<?php echo $garantia_vehpla;?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_vehsermot" class="control-label">Serie Motor</label>
                      <input type="text" name="txt_garantia_vehsermot" id="txt_garantia_vehsermot" class="form-control input-sm" value="<?php echo $garantia_vehsermot;?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_vehsercha" class="control-label">Serie Chasis</label>
                      <input type="text" name="txt_garantia_vehsercha" id="txt_garantia_vehsercha" class="form-control input-sm" value="<?php echo $garantia_vehsercha;?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_vehano" class="control-label">Año del Vehículo</label>
                      <input type="text" name="txt_garantia_vehano" id="txt_garantia_vehano" class="form-control input-sm moneda2" value="<?php echo $garantia_vehano;?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_vehkil" class="control-label">Kilometraje</label>
                      <input type="text" name="txt_garantia_vehkil" id="txt_garantia_vehkil" class="form-control input-sm moneda2" value="<?php echo $garantia_vehkil;?>">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="txt_garantia_vehcol" class="control-label">Color</label>
                      <input type="text" name="txt_garantia_vehcol" id="txt_garantia_vehcol" class="form-control input-sm" value="<?php echo $garantia_vehcol;?>">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_garantia_pro" class="control-label">Producto</label>
                  <input type="text" name="txt_garantia_pro" id="txt_garantia_pro" class="form-control input-sm mayus" value="<?php echo $garantia_pro; ?>">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_val" class="control-label">Valor a Prestar</label>
                      <?php
                      if ($credito_xac == 1)
                        echo '<input type="text" name="txt_garantia_val" id="txt_garantia_val" class="form-control input-sm moneda" value="' . $garantia_val . '" readonly>';
                      else
                        echo '<input type="text" name="txt_garantia_val" id="txt_garantia_val" class="form-control input-sm moneda" value="' . $garantia_val . '">';
                      ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_valtas" class="control-label">Valor de Tasación</label>
                      <input type="text" name="txt_garantia_valtas" id="txt_garantia_valtas" class="form-control input-sm moneda" value="<?php echo $garantia_valtas; ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group garantia_serie">
                  <label for="txt_garantia_ser" class="control-label">Serie del Producto</label>
                  <input type="text" name="txt_garantia_ser" id="txt_garantia_ser" class="form-control input-sm mayus" value="<?php echo $garantia_ser; ?>">
                </div>
                <div class="row garantia_oro" style="display: none;">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_kil" class="control-label">Kilates</label>
                      <input type="text" name="txt_garantia_kil" id="txt_garantia_kil" class="form-control input-sm moneda" value="<?php echo $garantia_kil; ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_pes" class="control-label">Peso</label>
                      <input type="text" name="txt_garantia_pes" id="txt_garantia_pes" class="form-control input-sm moneda" value="<?php echo $garantia_pes; ?>">
                    </div>
                  </div>
                </div>
                <div class="row garantia_oro" style="display: none;">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="txt_garantia_grab" class="control-label">Grabado</label>
                      <input type="text" name="txt_garantia_grab" id="txt_garantia_grab" class="form-control input-sm mayus" value="<?php echo $garantia_grab; ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cmb_garantia_calidad" class="control-label">Calidad de Joya</label>
                      <select name="cmb_garantia_calidad" id="cmb_garantia_calidad" class="form-control input-sm">
                        <option value="">Selecciona...</option>
                        <option value="1" <?php if($garantia_calidad == 1) echo 'selected';?> >Joya como Materia</option>
                        <option value="2" <?php if($garantia_calidad == 2) echo 'selected';?> >Joya Standar</option>
                        <option value="3" <?php if($garantia_calidad == 3) echo 'selected';?> >Joya Premium</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group garantia_oro" style="display: none;">
                  <label for="txt_garantia_med" class="control-label">Medidas</label>
                  <input type="text" name="txt_garantia_med" id="txt_garantia_med" class="form-control input-sm mayus" value="<?php echo $garantia_med; ?>">
                </div>
                <div class="form-group garantia_oro" style="display: none;">
                  <label for="txt_garantia_tas" class="control-label">Tasado por</label>
                  <input type="text" name="txt_garantia_tas" id="txt_garantia_tas" class="form-control input-sm mayus" value="<?php echo $garantia_tas; ?>">
                </div>
                <div class="form-group">
                  <label for="txt_garantia_det" class="control-label">Detalle del Producto</label>
                  <textarea name="txt_garantia_det" id="txt_garantia_det" class="form-control input-sm" rows="2" style="resize: vertical;"><?php echo $garantia_det; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="txt_garantia_web" class="control-label">Página Web de Referencia</label>
                  <textarea name="txt_garantia_web" id="txt_garantia_web" class="form-control input-sm" rows="2" style="resize: vertical;"><?php echo $garantia_web; ?></textarea>
                </div>
                <div class="form-group">
                  <?php
                  if ($action == 'insertar')
                    echo '<button type="button" class="btn btn-info" disabled="true">Subir fotos</button>';
                  if ($action == 'modificar' || $action == 'leer')
                    echo '<button type="button" class="btn btn-success btn-xs" id="btn_garantia_file" onclick="garantiafile_form_referencia(\'I\', ' . $garantia_id . ', 0)" title="Subir fotos referenciales valor de producto"><i class="fa fa-camera" aria-hidden="true"></i></button>';
                  ?>
                  <?php if ($action == 'modificar' || $action == 'leer') : ?>
                    <div class="row" id="garantiafilecredito_galeria">
                      <?php
                      require_once('../garantiafilecredito/garantiafilecredito_galeria.php');
                      ?>
                    </div>
                  <?php endif; ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <?php
                  if ($action == 'insertar')
                    echo '<button type="button" class="btn btn-info" disabled="true">Subir fotos</button>';
                  if ($action == 'modificar' || $action == 'leer')
                    echo '<button type="button" class="btn btn-info" id="btn_subir_fotos" onclick="garantiafile_form(\'I\', ' . $garantia_id . ', 0)">Subir fotos</button>';
                  ?>
                </div>
                <?php if ($action == 'modificar' || $action == 'leer') : ?>
                  <div class="row" id="garantiafile_galeria">
                    <?php
                    require_once('../garantiafile/garantiafile_galeria.php');
                    ?>
                  </div>
                <?php endif; ?>
                <?php if ($action == 'insertar') : ?>
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" id="btn_close_mensajeinsertarfoto" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>
                      <i class="icon fa fa-info"></i>Mensaje
                    </h4>
                    Puedes subir tus fotos aquí, despúes de guardar la garantía.
                  </div>
                <?php endif; ?>
              </div>
            </div>
            <?php if ($credito_id == 0) : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> Estás intentando guardar una garantía sin un Crédito registrado, revisa antes de proceder por favor. ¿Seguro de proceder?</h4>
              </div>
            <?php endif; ?>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if ($action == 'eliminar') : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Garantía?</h4>
              </div>
            <?php endif; ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="garantia_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_garantia">Guardar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_garantia">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_garantia">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<div id="div_modal_garantiafile_form"></div>
<div id="div_modal_garantiafilecredito_form"></div>
<script type="text/javascript" src="<?php echo 'vista/garantia/garantia_form.js?ver=1112233'; ?>"></script>