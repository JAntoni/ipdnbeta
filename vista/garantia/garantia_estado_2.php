<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../garantia/Garantia.class.php');
  $oGarantia = new Garantia();
  require_once('../funciones/funciones.php');

  $credito_id = intval($_POST['credito_id']);

  $tr = '';
  $result = $oGarantia->listar_garantias($credito_id);
  	if($result['estado'] == 1){
  		foreach ($result['data'] as $key => $value) {
  			$estado = '';
  			if($value['tb_garantia_almest'] == 0)
  				$estado = '<span class="badge bg-aqua">En Ofcinas</span>';
  			if($value['tb_garantia_almest'] == 1)
  				$estado = '<span class="badge bg-yellow">Enviado a Almacén</span>';
  			if($value['tb_garantia_almest'] == 2)
  				$estado = '<span class="badge bg-blue">En Almacén</span>';
  			if($value['tb_garantia_almest'] == 3)
  				$estado = '<span class="badge bg-teal">Envío a Ofcinas</span>';

        $garantia_envi = $value['tb_garantia_envi']; //se guarda a todos los usuarios que envian o cancelan el envio
        $garantia_reci = $value['tb_garantia_reci']; //se concatena cada vez que un usuario recibe
        $garantia_ped = $value['tb_garantia_usuped'];//se guarda al usuario que pide y cancela

        $content = $garantia_envi.''.$garantia_reci.''.$garantia_ped;
        $arr_cont = explode('<br>', $content);
        $count = count($arr_cont);
        $array = array();

        for ($i=0; $i < $count; $i++) { 
          if(!empty(trim($arr_cont[$i]))){
            $arr_fecha = explode('|', $arr_cont[$i]);

            $fecha_limpia = str_replace('</b>', '', $arr_fecha[1]);
            $fecha_limpia = str_replace('</strong>', '', $fecha_limpia);
            $fecha_limpia = trim($fecha_limpia);

            if(array_key_exists(strtotime($fecha_limpia), $array))
              $array[strtotime($fecha_limpia).$i] = $arr_cont[$i];
            else
              $array[strtotime($fecha_limpia)] = $arr_cont[$i];
          }
        }
        ksort($array);
        $detalle = '';
        foreach ($array as $key => $value2) {
          $detalle = $value2.'<br>';
        }

  			$tr .='<tr>';
	        $tr .='
	          <td>'.$value['tb_garantia_pro'].'</td>
	          <td>'.$estado.'</td>
            <td>'.$detalle.'</td>
	        ';
	      $tr .='</tr>';
  		}
  	}
  	else{
  		$tr ='<tr><td colspan="2">NO HAY GARANTÍAS DE ESTE CRÉDITO</td></tr>';
  	}
  $result = NULL;
?>
<table id="tbl_garantias_estado" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>Producto</th>
      <th>Ubicación</th>
      <th>Detalle</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>