var datatable_global;

function garantia_form(usuario_act, garantia_id){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"garantia/garantia_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      garantia_id: garantia_id,
      vista: 'garantia'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_garantia_form').html(data);
      	$('#modal_registro_garantia').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_garantia'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_registro_garantia'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_garantia', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'garantia';
      	var div = 'div_modal_garantia_form';
      	permiso_solicitud(usuario_act, garantia_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){

		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function garantia_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"garantia/garantia_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#garantia_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_garantia_tabla').html(data);
      $('#garantia_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    complete: function(data){

    },
    error: function(data){
      $('#garantia_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function carousel(modulo_nom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_garantias').DataTable({
    "pageLength": 50,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [1,2,4,5,6], orderable: false }
    ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}
$(document).ready(function() {
  estilos_datatable();
});



function creditomenor_form(usuario_act, creditomenor_id){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"creditomenor/creditomenor_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      creditomenor_id: creditomenor_id,
      vista: 'creditomenor'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_creditomenor_form').html(data);
      	$('#modal_registro_creditomenor').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_registro_creditomenor', 95);
        modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'creditomenor';
      	var div = 'div_modal_creditomenor_form';
      	permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){

		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}

function creditomenor_pagos(creditomenor_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditomenor/creditomenor_pagos.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: creditomenor_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditomenor_pagos').html(data);
        $('#modal_creditomenor_pagos').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_creditomenor_pagos', 95);
        modal_height_auto('modal_creditomenor_pagos'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_creditomenor_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}