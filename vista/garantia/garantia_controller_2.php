<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../garantia/Garantia.class.php');
  $oGarantia = new Garantia();
  require_once('../funciones/funciones.php');

 	$action = $_POST['action'];

 	$oGarantia->credito_id = intval($_POST['hdd_credito_id']);
  $oGarantia->garantiatipo_id = intval($_POST['cmb_garantiatipo_id']);
  $oGarantia->garantia_can = intval($_POST['txt_garantia_can']);
  $oGarantia->garantia_pro = mb_strtoupper($_POST['txt_garantia_pro'], 'UTF-8');
  $oGarantia->garantia_val = moneda_mysql($_POST['txt_garantia_val']);
  $oGarantia->garantia_valtas = moneda_mysql($_POST['txt_garantia_valtas']);
  $oGarantia->garantia_ser = $_POST['txt_garantia_ser'];
  $oGarantia->garantia_kil = $_POST['txt_garantia_kil']; //kilates para oro
  $oGarantia->garantia_pes = $_POST['txt_garantia_pes']; //peso para oro
  $oGarantia->garantia_grab = $_POST['txt_garantia_grab']; //grabado
  $oGarantia->garantia_med = $_POST['txt_garantia_med']; //medidas
  $oGarantia->garantia_tas = $_POST['txt_garantia_tas']; //tasador de oro
  $oGarantia->garantia_det = $_POST['txt_garantia_det']; //detalle
  $oGarantia->garantia_web = $_POST['txt_garantia_web'];

 	if($action == 'insertar'){
 
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Garantía. '.$oGarantia->garantia_val;
 		if($oGarantia->insertar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Garantía registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
    $garantia_id = intval($_POST['hdd_garantia_id']);
    $oGarantia->garantia_id = $garantia_id;

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Garantía.';

 		if($oGarantia->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Garantia modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$garantia_id = intval($_POST['hdd_garantia_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Garantia.';

 		if($oGarantia->eliminar($garantia_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Garantia eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>