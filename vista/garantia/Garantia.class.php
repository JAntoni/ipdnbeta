<?php

if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Garantia extends Conexion
{

  public $garantia_id;
  public $garantia_xac = 1;
  public $credito_id;
  public $garantiatipo_id;
  public $garantia_estrang = 0;
  public $garantia_can;
  public $garantia_pro;
  public $garantia_val;
  public $garantia_valtas;
  public $garantia_ser;
  public $garantia_kil; //kilates para oro
  public $garantia_pes; //peso para oro
  public $garantia_grab;
  public $garantia_med;
  public $garantia_tas; //tasador de oro
  public $garantia_det; //detalle
  public $garantia_web;
  public $almacen_id;
  public $articulo_id;
  public $garantia_calidad = NULL;

  public $vehiculomarca_id = 0;
  public $vehiculomodelo_id = 0;
  public $vehiculoclase_id = 0;
  public $garantia_vehpla = '';
  public $garantia_vehsermot = '';
  public $garantia_vehsercha = '';
  public $garantia_vehano = '';
  public $garantia_vehkil = '';
  public $garantia_vehcol = '';

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $this->almacen_id = $_SESSION['empresa_id'];
      $sql = "INSERT INTO tb_garantia (
                                        tb_garantia_xac, 
                                        tb_credito_id, 
                                        tb_garantiatipo_id, 
                                        tb_garantia_estrang,
                                        tb_garantia_can, 
                                        tb_garantia_pro, 
                                        tb_garantia_val, 
                                        tb_garantia_valtas, 
                                        tb_garantia_ser, 
                                        tb_garantia_kil, 
                                        tb_garantia_pes, 
                                        tb_garantia_grab, 
                                        tb_garantia_med,
                                        tb_garantia_tas, 
                                        tb_garantia_det, 
                                        tb_garantia_web,
                                        tb_almacen_id,
                                        tb_articulo_id,
                                        tb_vehiculomarca_id,
                                        tb_vehiculomodelo_id,
                                        tb_vehiculoclase_id,
                                        tb_garantia_vehpla,
                                        tb_garantia_vehsermot,
                                        tb_garantia_vehsercha,
                                        tb_garantia_vehano,
                                        tb_garantia_vehkil,
                                        tb_garantia_vehcol,
                                        tb_garantia_calidad)
                                VALUES (
                                        :garantia_xac, 
                                        :credito_id, 
                                        :garantiatipo_id,
                                        :garantia_estrang,
                                        :garantia_can,
                                        :garantia_pro, 
                                        :garantia_val, 
                                        :garantia_valtas,
                                        :garantia_ser, 
                                        :garantia_kil, 
                                        :garantia_pes,
                                        :garantia_grab,
                                        :garantia_med,
                                        :garantia_tas, 
                                        :garantia_det, 
                                        :garantia_web,
                                        :almacen_id,
                                        :articulo_id,
                                        :vehiculomarca_id,
                                        :vehiculomodelo_id,
                                        :vehiculoclase_id,
                                        :garantia_vehpla,
                                        :garantia_vehsermot,
                                        :garantia_vehsercha,
                                        :garantia_vehano,
                                        :garantia_vehkil,
                                        :garantia_vehcol,
                                        :garantia_calidad);";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_xac", $this->garantia_xac, PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":garantiatipo_id", $this->garantiatipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_estrang", $this->garantia_estrang, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_can", $this->garantia_can, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_pro", $this->garantia_pro, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_val", $this->garantia_val, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_valtas", $this->garantia_valtas, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_ser", $this->garantia_ser, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_kil", $this->garantia_kil, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_pes", $this->garantia_pes, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_tas", $this->garantia_tas, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_det", $this->garantia_det, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_web", $this->garantia_web, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_grab", $this->garantia_grab, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_med", $this->garantia_med, PDO::PARAM_STR);
      $sentencia->bindParam(":almacen_id", $this->almacen_id, PDO::PARAM_INT);
      $sentencia->bindParam(":articulo_id", $this->articulo_id, PDO::PARAM_INT);

      $sentencia->bindParam(":vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_vehpla", $this->garantia_vehpla, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehsermot", $this->garantia_vehsermot, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehsercha", $this->garantia_vehsercha, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehano", $this->garantia_vehano, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehkil", $this->garantia_vehkil, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehcol", $this->garantia_vehcol, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_calidad", $this->garantia_calidad, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $garantia_id = $this->dblink->lastInsertId();

      $this->dblink->commit();
      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['garantia_id'] = $garantia_id;
      return $data;
      //            return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar()
  {
    $this->dblink->beginTransaction();
    try {
      // $sql = "CALL proc_modificar_garantia(
      //     :garantia_id,
      //     :garantia_xac, :credito_id, :garantiatipo_id, :garantia_estrang,
      //     :garantia_can, :garantia_pro, :garantia_val,
      //     :garantia_valtas, :garantia_ser, :garantia_kil,
      //     :garantia_pes, :garantia_tas, :garantia_det,
      //     :garantia_web, :garantia_grab, :garantia_med)";

      $sql = 'UPDATE tb_garantia SET 
                tb_garantiatipo_id =:garantiatipo_id, 
                tb_garantia_estrang =:garantia_estrang,
                tb_garantia_pro =:garantia_pro, tb_garantia_val =:garantia_val,
                tb_garantia_valtas =:garantia_valtas, tb_garantia_ser =:garantia_ser, 
                tb_garantia_kil =:garantia_kil, tb_garantia_pes =:garantia_pes, 
                tb_garantia_tas =:garantia_tas, tb_garantia_det =:garantia_det,
                tb_garantia_web =:garantia_web, tb_garantia_grab =:garantia_grab, 
                tb_garantia_med =:garantia_med, tb_articulo_id =:articulo_id,
                tb_vehiculomarca_id =:vehiculomarca_id, tb_vehiculomodelo_id =:vehiculomodelo_id,
                tb_vehiculoclase_id =:vehiculoclase_id, tb_garantia_vehpla =:garantia_vehpla,
                tb_garantia_vehsermot =:garantia_vehsermot, tb_garantia_vehsercha =:garantia_vehsercha,
                tb_garantia_vehano =:garantia_vehano, tb_garantia_vehkil =:garantia_vehkil,
                tb_garantia_vehcol =:garantia_vehcol, tb_garantia_calidad =:garantia_calidad
                WHERE tb_garantia_id = :garantia_id';

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $this->garantia_id, PDO::PARAM_INT);
      $sentencia->bindParam(":garantiatipo_id", $this->garantiatipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_estrang", $this->garantia_estrang, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_pro", $this->garantia_pro, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_val", $this->garantia_val, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_valtas", $this->garantia_valtas, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_ser", $this->garantia_ser, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_kil", $this->garantia_kil, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_pes", $this->garantia_pes, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_tas", $this->garantia_tas, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_det", $this->garantia_det, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_web", $this->garantia_web, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_grab", $this->garantia_grab, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_med", $this->garantia_med, PDO::PARAM_STR);
      $sentencia->bindParam(":articulo_id", $this->articulo_id, PDO::PARAM_INT);

      $sentencia->bindParam(":vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_vehpla", $this->garantia_vehpla, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehsermot", $this->garantia_vehsermot, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehsercha", $this->garantia_vehsercha, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehano", $this->garantia_vehano, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehkil", $this->garantia_vehkil, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_vehcol", $this->garantia_vehcol, PDO::PARAM_STR);
      $sentencia->bindParam(":garantia_calidad", $this->garantia_calidad, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function cambiar_de_almacen($gar_id, $alm_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_garantia SET tb_almacen_id =:tb_almacen_id WHERE tb_garantia_id =:tb_garantia_id;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_garantia_id", $gar_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function eliminar($garantia_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "DELETE FROM tb_garantia WHERE tb_garantia_id =:garantia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function mostrarUno($garantia_id)
  {
    try {
      $sql = "SELECT * FROM tb_garantia g
            LEFT JOIN tb_vehiculomarca vm ON g.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
            LEFT JOIN tb_vehiculomodelo vd ON g.tb_vehiculomodelo_id=vd.tb_vehiculomodelo_id
            LEFT JOIN tb_vehiculoclase cl ON g.tb_vehiculoclase_id=cl.tb_vehiculoclase_id
            WHERE tb_garantia_id =:garantia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarUnoDetalle($id)
  {
    try {
      $sql = "SELECT * FROM tb_garantia ga
                            INNER JOIN tb_creditomenor cre on cre.tb_credito_id = ga.tb_credito_id
                            INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id
                            WHERE tb_garantia_id=:tb_garantia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_garantia_id", $id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_garantias($credito_id)
  {
    try {
      $sql = "SELECT * FROM tb_garantia ga INNER JOIN tb_garantiatipo gartip on gartip.tb_garantiatipo_id = ga.tb_garantiatipo_id
                    INNER JOIN tb_almacen alm on ga.tb_almacen_id = alm.tb_almacen_id 
                    WHERE tb_garantia_xac = 1";
      if (intval($credito_id) > 0)
        $sql .= " AND ga.tb_credito_id =:credito_id";
      $sql .= " ORDER BY tb_garantia_id desc";
      $sentencia = $this->dblink->prepare($sql);
      if (intval($credito_id) > 0)
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_todos_filtro($est, $alm_id, $cli_id)
  {
    try {
      $sql = "SELECT ga.*, cli.tb_cliente_nom, al.tb_almacen_nom FROM tb_garantia ga 
                    INNER JOIN tb_creditomenor cre on ga.tb_credito_id = cre.tb_credito_id
                    INNER JOIN tb_cliente cli on cre.tb_cliente_id = cli.tb_cliente_id
                    INNER JOIN tb_garantiatipo gt on (ga.tb_garantiatipo_id = gt.tb_garantiatipo_id and gt.tb_garantiatipo_id IN(1,2,3))
                    INNER JOIN tb_almacen al on ga.tb_almacen_id = al.tb_almacen_id
                    LEFT JOIN tb_tareainventario ti on ga.tb_garantia_id = ti.tb_garantia_id
                    where ga.tb_garantia_xac =1 and cre.tb_credito_xac =1 and cre.tb_credito_est NOT IN(4,6) and tb_garantia_est =0";
      if ($est > 0) {
        $sql .= ' and ga.tb_garantia_almest = :tb_garantia_almest ';
      }
      if ($alm_id > 0) {
        if ($est == 1 || $est == 3) {
          $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
        } elseif ($est == 2) {
          $sql .= ' and ti.tb_almacen_id2 = :tb_almacen_id';
        } else {
          $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id and ga.tb_garantia_almest in (0,1)) or (ga.tb_garantia_almest=2 and ti.tb_almacen_id2= :tb_almacen_id )';
        }
      }
      if ($cli_id > 0)
        $sql .= ' and cli.tb_cliente_id =:tb_cliente_id';

      $sql .= ' GROUP BY ga.tb_garantia_id order by ga.tb_garantia_id DESC;';
      $sentencia = $this->dblink->prepare($sql);
      if ($est > 0) {
        if ($est == 3) {
          $est = 0;
        }
        $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
      }
      if (intval($alm_id) > 0)
        $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
      if (intval($cli_id) > 0)
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["cantidad"] = $sentencia->rowCount();
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["cantidad"] = 0;
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  /* GERSON (08-02-23) */
  function mostrar_todos_filtro_paginado($est, $alm_id, $cre_id, $cli_id, $inicio, $cantidad, $total = false)
  {
    try {
      $sql = "SELECT ga.*, cli.tb_cliente_nom, al.tb_almacen_nom, cre.tb_credito_est ";

      if ($total) {
        $sql = "SELECT COUNT(DISTINCT(ga.tb_garantia_id)) ";
      }

      $sql .= "FROM tb_garantia ga 
                    INNER JOIN tb_creditomenor cre on ga.tb_credito_id = cre.tb_credito_id
                    INNER JOIN tb_cliente cli on cre.tb_cliente_id = cli.tb_cliente_id
                    INNER JOIN tb_garantiatipo gt on (ga.tb_garantiatipo_id = gt.tb_garantiatipo_id and gt.tb_garantiatipo_id IN(1,2,3))
                    INNER JOIN tb_almacen al on ga.tb_almacen_id = al.tb_almacen_id
                    LEFT JOIN tb_tareainventario ti on ga.tb_garantia_id = ti.tb_garantia_id
                    where ga.tb_garantia_xac =1 and cre.tb_credito_xac =1 and tb_garantia_est IN(0,2)";
      if ($est > 0) {
        $sql .= ' and ga.tb_garantia_almest = :tb_garantia_almest ';
        // Gerson (18-04-23)
        if ($est != 2) {
          $sql .= ' and cre.tb_credito_est NOT IN(4,6) ';
        }
        //
      } else {
        $sql .= ' and cre.tb_credito_est NOT IN(4,6) ';
      }
      if ($alm_id > 0) {
        if ($est == 1 || $est == 3) {
          $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
        } elseif ($est == 2) {
          $sql .= ' and ti.tb_almacen_id2 = :tb_almacen_id';
        } else {
          $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id and ga.tb_garantia_almest in (0,1)) or (ga.tb_garantia_almest=2 and ti.tb_almacen_id2= :tb_almacen_id )';
        }
      }
      if ($cre_id > 0) {
        $sql .= ' and cre.tb_credito_id =:tb_credito_id';
      }
      if ($cli_id > 0) {
        $sql .= ' and cli.tb_cliente_id =:tb_cliente_id';
      }

      if (!$total) {
        $sql .= ' GROUP BY ga.tb_garantia_id order by ga.tb_garantia_id DESC';
        $sql .= " LIMIT " . $inicio . "," . $cantidad . ";";
      }

      $sentencia = $this->dblink->prepare($sql);
      if ($est > 0) {
        if ($est == 3) {
          $est = 0;
        }
        $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
      }
      if (intval($alm_id) > 0)
        $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
      if ($cre_id > 0)
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
      if (intval($cli_id) > 0)
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        if ($total) {
          $resultado = $sentencia->fetch(PDO::FETCH_NUM);
          $resultado = $resultado[0];
        } else {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        }
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  function garantia_credito($garantia_id)
  {
    try {
      $sql = "SELECT * FROM tb_garantia ga INNER JOIN tb_creditomenor cre on cre.tb_credito_id = ga.tb_credito_id WHERE tb_garantia_id =:garantia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function filtrar_oro($cre_id, $gar_oro)
  {
    try {
      $sql = "SELECT * FROM 
                            tb_garantia g 
                            INNER JOIN tb_garantiatipo gt ON g.tb_garantiatipo_id=gt.tb_garantiatipo_id 
                    WHERE 
                            tb_garantia_xac=1 AND gt.tb_garantiatipo_id != 5 AND tb_credito_id=:tb_credito_id";

      if (empty($gar_oro))
        $sql .= " and gt.tb_garantiatipo_id != 2 ORDER BY tb_garantia_id";
      else
        $sql .= " ORDER BY tb_garantia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function filtrar($cre_id)
  {
    try {
      $sql = "SELECT * FROM 
                            tb_garantia g 
                            INNER JOIN tb_garantiatipo gt ON g.tb_garantiatipo_id=gt.tb_garantiatipo_id
                            WHERE tb_garantia_xac=1
                            AND tb_credito_id=:cre_id
                            ORDER BY tb_garantia_id";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_campo($garantia_id, $garantia_columna, $garantia_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($garantia_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_garantia SET " . $garantia_columna . " = :garantia_valor WHERE tb_garantia_id = :garantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":garantia_valor", $garantia_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":garantia_valor", $garantia_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el egreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar_campo2($credito_id, $garantia_columna, $garantia_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($garantia_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_garantia SET " . $garantia_columna . " =:garantia_valor WHERE tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":garantia_valor", $garantia_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":garantia_valor", $garantia_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el egreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }


  function estado_usuario_pedido($gar_id, $pedido)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_garantia SET tb_garantia_usuped = CONCAT(tb_garantia_usuped,:pedido) WHERE tb_garantia_id =:gar_id;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_INT);
      $sentencia->bindParam(":pedido", $pedido, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function estado_almacen_enviado($gar_id, $est, $enviado)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_garantia 
                                    SET tb_garantia_almest =:tb_garantia_almest, tb_garantia_envi = CONCAT(tb_garantia_envi,:tb_garantia_envi) WHERE tb_garantia_id =:tb_garantia_id;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_garantia_id", $gar_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_garantia_envi", $enviado, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function estado_almacen_recibido($gar_id, $est, $recibido)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_garantia 
                                        SET tb_garantia_almest =:tb_garantia_almest, tb_garantia_reci = CONCAT(tb_garantia_reci,:tb_garantia_reci) WHERE tb_garantia_id =:tb_garantia_id;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_garantia_idtb_garantia_id", $gar_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_garantia_reci", $recibido, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  /* GERSON (28-01-23) */
  function mostrar_todos_filtro_codigobarras($est, $alm_id, $cliente_id, $cliente)
  {
    try {
      $sql = "SELECT ga.*, cli.tb_cliente_nom, al.tb_almacen_nom FROM tb_garantia ga 
                    INNER JOIN tb_creditomenor cre on ga.tb_credito_id = cre.tb_credito_id
                    INNER JOIN tb_cliente cli on cre.tb_cliente_id = cli.tb_cliente_id
                    INNER JOIN tb_garantiatipo gt on (ga.tb_garantiatipo_id = gt.tb_garantiatipo_id and gt.tb_garantiatipo_id IN(1,2,3))
                    INNER JOIN tb_almacen al on ga.tb_almacen_id = al.tb_almacen_id
                    LEFT JOIN tb_tareainventario ti on ga.tb_garantia_id = ti.tb_garantia_id
                    where ga.tb_garantia_xac =1 and cre.tb_credito_xac =1 and cre.tb_credito_est NOT IN(4,6) and tb_garantia_est =0";
      if ($est > 0) {
        $sql .= ' and ga.tb_garantia_almest = :tb_garantia_almest ';
      }
      if ($alm_id > 0) {
        if ($est == 1 || $est == 3) {
          $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
        } elseif ($est == 2) {
          $sql .= ' and ti.tb_almacen_id2 = :tb_almacen_id';
        }
        /* else{
                    $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
                } */ else {
          $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id and ga.tb_garantia_almest in (0,1)) or (ga.tb_garantia_almest=2 and ti.tb_almacen_id2= :tb_almacen_id )';
        }
      }
      if ($cliente_id > 0) {
        $sql .= ' and cli.tb_cliente_id = :tb_cliente_id';
      } else {
        if ($cliente != '' || $cliente != null)
          $sql .= ' and cli.tb_cliente_nom LIKE %:tb_cliente_nom%';
      }


      $sql .= ' GROUP BY ga.tb_garantia_id order by ga.tb_garantia_id DESC;';
      $sentencia = $this->dblink->prepare($sql);
      if ($est > 0) {
        if ($est == 3) {
          $est = 0;
        }
        $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
      }
      if (intval($alm_id) > 0)
        $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
      //if ($cliente != '' || $cliente != null)
      if ($cliente_id > 0) {
        $sentencia->bindParam(":tb_cliente_id", $cliente_id, PDO::PARAM_INT);
      } else {
        if ($cliente != '' || $cliente != null)
          $sentencia->bindParam(":tb_cliente_nom", $cliente, PDO::PARAM_STR);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  function tasacionsug($filtro_sql, $tiempo, $articulo_id)
  {
    try {
      $tiempo = 1.5 * pow(2, $tiempo);
      $sql = "SELECT
                        g.tb_garantia_id, g.tb_credito_id, g.tb_garantia_val, g.tb_garantia_valtas
                    FROM tb_garantia g
                    LEFT JOIN tb_creditomenor cm ON g.tb_credito_id = cm.tb_credito_id
                    WHERE g.tb_articulo_id = :articulo_id
                    AND g.tb_garantia_xac = 1
                    AND cm.tb_credito_xac = 1
                    $filtro_sql
                    AND cm.tb_credito_reg > now() - INTERVAL $tiempo month
                    AND cm.tb_moneda_id = 1
                    ORDER BY g.tb_garantia_id DESC;";
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(':articulo_id', $articulo_id, PDO::PARAM_INT);
      $sentencia->execute();

      $retorno["sql"] = $sql;
      $retorno["cantidad"] = $sentencia->rowCount();
      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor();
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "error";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  //Antonio 07-01-25
  function agregar_detalle_extra($garantia_id, $garantia_det)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_garantia SET tb_garantia_det = CONCAT(tb_garantia_det,' | ', :garantia_det) WHERE tb_garantia_id =:garantia_id;";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
      $sentencia->bindParam(":garantia_det", $garantia_det, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
}
