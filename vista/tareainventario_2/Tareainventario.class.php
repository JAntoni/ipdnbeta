<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Tareainventario extends Conexion{
    public $tb_tarea_id;
    public $tb_tarea_fecreg;
    public $tb_tarea_usureg;
    public $tb_tarea_det;
    public $tb_usuario_id;
    public $tb_tarea_usutip;
    public $tb_garantia_id;
    public $tb_tarea_est;
    public $tb_tarea_tipo;
    

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tareainventario(
                                tb_tarea_fecreg, 
                                tb_tarea_usureg, 
                                tb_tarea_det,
                                tb_usuario_id,
                                tb_tarea_usutip,
                                tb_garantia_id, 
                                tb_tarea_tipo) 
                        VALUES ( 
                                NOW(),
                                :tb_tarea_usureg, 
                                :tb_tarea_det,
                                :tb_usuario_id, 
                                :tb_tarea_usutip,
                                :tb_garantia_id,
                                :tb_tarea_tipo)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_tarea_usureg", $this->tb_tarea_usureg, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_tarea_det", $this->tb_tarea_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tarea_usutip", $this->tb_tarea_usutip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_garantia_id", $this->tb_garantia_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tarea_tipo", $this->tb_tarea_tipo, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    
    function eliminar_tarea($gar_id, $est, $tipo){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_tareainventario where tb_garantia_id =:gar_id and tb_tarea_est =:est and tb_tarea_tipo =:tipo;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_INT);
        $sentencia->bindParam(":est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    
    function realizar_tarea($gar_id, $est, $tipo){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_tareainventario set tb_tarea_est =1 where tb_garantia_id =:gar_id and tb_tarea_est =:est and tb_tarea_tipo =:tipo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_INT);
        $sentencia->bindParam(":est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    
      function filtrar_tarea_estado_tipo($gar_id, $est, $tipo){
      try {

        $sql = "SELECT * FROM tb_tareainventario where tb_garantia_id =:gar_id AND tb_tarea_est =:est and tb_tarea_tipo =:tipo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_STR);
        $sentencia->bindParam(":est", $est, PDO::PARAM_STR);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Tareas por Estados registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function filtrar_tareas_por_hacer($usu_id, $usu_tip){
      try {
            $sql = "SELECT * FROM tb_tareainventario where tb_tarea_est =0 and tb_tarea_usutip =:usu_tip and (tb_usuario_id =:usu_id or tb_usuario_id = 0)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":usu_id", $usu_id, PDO::PARAM_STR);
            $sentencia->bindParam(":usu_tip", $usu_tip, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay Tareas por Estados registrados";
              $retorno["data"] = "";
            }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    
}
