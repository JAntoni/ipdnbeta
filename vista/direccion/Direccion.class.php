<?php
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');
//

class Direccion extends Conexion {
    public $id;
    public $des;
    public $dir;
    public $ubigeo;
    public $tablanom;
    public $tablaid;
    public $fecini;
    public $fecfin;
    public $usureg;
    public $fecreg;
    public $xac;

    public function listar_todos($tabla_nom, $tabla_id, $registros_vigentes) {
        $where_opt = '';
        if (!empty($tabla_nom)) {
            $where_opt .= ' AND tb_direccion_tablanom = :param_opc1';
        }
        if (!empty($tabla_id)) {
            $where_opt .= ' AND tb_direccion_tablaid = :param_opc2';
        }
        if (!empty($registros_vigentes)) {
            if ($registros_vigentes == 'actualidad') {
                $where_opt .= ' AND (tb_direccion_fecfin IS NULL OR tb_direccion_fecfin = "")';
            }
            else {
                $where_opt .=" AND (
                    DATE_FORMAT(cp.tb_direccion_fecini, '%Y-%m-%d') <= '$registros_vigentes' AND
                    IF ((cp.tb_direccion_fecfin IS NULL OR cp.tb_direccion_fecfin = ''), TRUE, DATE_FORMAT(cp.tb_direccion_fecfin, '%Y-%m-%d') >= '$registros_vigentes')
                )";
            }
        }
        try {
            $sql = "SELECT dir.*
                    FROM tb_direccion dir
                    WHERE dir.tb_direccion_xac = 1 $where_opt
                    ORDER BY dir.tb_direccion_fecreg ASC;";

            $sentencia = $this->dblink->prepare($sql);
            if (!empty($tabla_nom)) {
                $sentencia->bindParam(':param_opc1', $tabla_nom, PDO::PARAM_STR);
            }
            if (!empty($tabla_id)) {
                $sentencia->bindParam(':param_opc2', $tabla_id, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $column_opc = !empty($this->des) ? ",\ntb_direccion_des" : '';
            $param_opc = !empty($this->des) ? ",\n:param_opc1" : '';
            $column_opc .= !empty($this->fecfin) ? ",\ntb_direccion_fecfin" : '';
            $param_opc .= !empty($this->fecfin) ? ",\n:param_opc2" : '';

            $sql = "INSERT INTO tb_direccion (
                        tb_direccion_dir,
                        tb_direccion_ubigeo,
                        tb_direccion_tablanom,
                        tb_direccion_tablaid,
                        tb_direccion_fecini,
                        tb_direccion_usureg$column_opc)
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5$param_opc)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->dir, PDO::PARAM_STR);
            $sentencia->bindParam(':param1', $this->ubigeo, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->tablanom, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->tablaid, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->fecini, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->usureg, PDO::PARAM_INT);
            if (!empty($this->des)) {
                $sentencia->bindParam(':param_opc1', $this->des, PDO::PARAM_STR);
            }
            if (!empty($this->fecfin)) {
                $sentencia->bindParam(':param_opc2', $this->fecfin, PDO::PARAM_STR);
            }

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar_campo($direccionid, $direccion_columna, $direccionvalor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($direccion_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_direccion SET " . $direccion_columna . " = :direccion_valor WHERE tb_direccion_id = :direccionid;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(':direccionid', $direccionid, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(':direccion_valor', $direccionvalor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(':direccion_valor', $direccionvalor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
