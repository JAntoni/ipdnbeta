<?php
require_once '../direccion/Direccion.class.php';
$oDireccion = new Direccion();

//esta variable puede ser definida en un archivo en donde se haga un require_once o un include
$direccion_id = empty($direccion_id) ? $_POST['direccion_id'] : $direccion_id;
$tabla_nom = empty($tabla_nom) ? $_POST['tabla_nom'] : $tabla_nom;
$tabla_id = empty($tabla_id) ? $_POST['tabla_id'] : $tabla_id;
$vigencia = empty($vigencia) ? $_POST['vigencia'] : $vigencia;
$tipo_cli = empty($tipo_cli) ? $_POST['tipo_cli'] : $tipo_cli;
$incluye_direccion_EP = empty($incluye_direccion_EP) ? $_POST['incluye_direccion_EP'] : $incluye_direccion_EP;

$option = '<option value="0">Seleccione</option>';

if ($tabla_nom == 'tb_cliente') {
    require_once '../cliente/Cliente.class.php';
    $oCliente = new Cliente();

    $busq_cliente = $oCliente->mostrarUno($tabla_id)['data'];
    if ($tipo_cli == 1 && !empty($busq_cliente['tb_cliente_dir'])) { //pers nat
        $selected = '';
        if($direccion_id == 'tb_cliente_dir')
            $selected = 'selected';
        //
        $option .= '<option value="tb_cliente_dir" data-ubig="'.$busq_cliente['tb_ubigeo_cod'].'" style="font-weight: bold;" '.$selected.'>DIR. CLIENTE: '.$busq_cliente['tb_cliente_dir'].'</option>';
    } elseif ($tipo_cli == 2 && !empty($busq_cliente['tb_cliente_empdir'])) { //empresa
        $selected = '';
        if($direccion_id == 'tb_cliente_empdir')
            $selected = 'selected';
        //
        $option .= '<option value="tb_cliente_empdir" data-ubig="'.$busq_cliente['tb_ubigeo_cod3'].'" style="font-weight: bold;" '.$selected.'>DIR. EMPRESA: '.$busq_cliente['tb_cliente_empdir'].'</option>';
    }

    if (!empty($busq_cliente['tb_cliente_dir2'])) {
        $selected = '';
        if($direccion_id == 'tb_cliente_dir2')
            $selected = 'selected';
        //
        $option .= '<option value="tb_cliente_dir2" data-ubig="'.$busq_cliente['tb_ubigeo_cod2'].'" style="font-weight: bold;" '.$selected.'>DIR COBRANZA: '.$busq_cliente['tb_cliente_dir2'].'</option>';
    }
    unset($busq_cliente);
}

if ($incluye_direccion_EP == 1) {
    $credito_getdocs = empty($credito_getdocs) ? $_POST['credito_getdocs'] : $credito_getdocs;

    require_once '../escriturapublica/Escriturapublica.class.php';
    $oEscritura = new Escriturapublica();

    $res_escritura = $oEscritura->listar_todos('', $credito_getdocs);
    if ($res_escritura['estado'] == 1) {
        $res_escritura = $res_escritura['data'][0];

        $selected = '';
        if($direccion_id == 'tb_escriturapublica_clientedir')
            $selected = 'selected';
        //
        $option .= '<option value="tb_escriturapublica_clientedir" data-ubig="'.$res_escritura['tb_escriturapublica_clienteubig'].'" style="font-weight: bold;" '.$selected.'>DIR. ESC.PUB: '.$res_escritura['tb_escriturapublica_clientedir'].'</option>';
    }
    unset($res_escritura);
}

//PRIMER NIVEL
$result = $oDireccion->listar_todos($tabla_nom, $tabla_id, $vigencia);
if($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $selected = '';
        if($direccion_id == $value['tb_direccion_id'])
            $selected = 'selected';
        //
        $option .= '<option value="'.$value['tb_direccion_id'].'" data-ubig="'.$value['tb_direccion_ubigeo'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_direccion_dir'].'</option>';
    }
}
$result = null;
//FIN PRIMER NIVEL
echo $option;
