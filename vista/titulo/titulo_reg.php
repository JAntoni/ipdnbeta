<?php
    require_once ("../../core/usuario_sesion.php");

    require_once ("../creditogarveh/Creditogarveh.class.php");
    $oCreditogarveh = new Creditogarveh();

    require_once("../lineatiempo/LineaTiempo.class.php");
    $oLineaTiempo = new LineaTiempo();
    require_once('../creditolinea/Creditolinea.class.php');
    $oCreditolinea = new Creditolinea();

    $creditotipo_id = 3; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
    $usuario_id = intval($_SESSION['usuario_id']);

    require_once("../funciones/funciones.php");
    require_once("../funciones/fechas.php");

    $cre_tip = intval($_POST['hdd_cre_tip']); //3 garve, 4 hipo
    $cre_id = intval($_POST['hdd_cre_id']);

    $fecha_escritura  = $_POST['txt_credito_fecescri'];
    $numero_escritura = $_POST['txt_credito_numescri'];

    $oCreditogarveh->modificar_campo($cre_id,'tb_credito_numtit', $_POST['txt_credito_numtit'], 'STR'); //VIGENTE
    $oCreditogarveh->modificar_campo($cre_id,'tb_credito_fecescri', fecha_mysql($fecha_escritura), 'STR'); //VIGENTE
    $oCreditogarveh->modificar_campo($cre_id,'tb_credito_numescri', $numero_escritura, 'STR'); //VIGENTE

    // $oCreditogarveh->modificar_campo($cre_id,'tb_credito_est',3, 'INT'); //VIGENTE
    // $oCreditogarveh->modificar_campo($cre_id,'tb_credito_fecvig', date('Y-m-d'), 'STR');

    $his = '<span style="color: blue;">Se están registrando los datos de la escritura pública. Número de Título: '.strtoupper($_POST['txt_credito_numtit']).', Fecha de escritura pública: '.$fecha_escritura.', 
        Número de Escritura: '.$numero_escritura.'. Ingresado por: '.$_SESSION['usuario_nom'].' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    // //$oCreditogarveh->registro_historial($_POST['hdd_cre_id'], $his);
    // $oCreditolinea->insertar($creditotipo_id, $_POST['hdd_cre_id'], $usuario_id, $his); // funcion anulada y mejorada con la nueva función: insertar nuevo

    // - - - - - REGISTRAR ANTERIOR EN EL HISTORIAL - - - - - //
    $oCreditolinea->tb_creditotipo_id      = $cre_tip;
    $oCreditolinea->tb_credito_id          = $cre_id;
    $oCreditolinea->tb_usuario_id          = $usuario_id;
    $oCreditolinea->tb_creditolinea_det    = $his;
    $oCreditolinea->tb_creditolinea_xac    = 1;
    $oCreditolinea->tb_creditolinea_subnom = $_POST['txt_credito_numtit'];
    $oCreditolinea->insertar_nuevo();
    // - - - - - REGISTRAR ANTERIOR EN EL HISTORIAL - - - - - //

    $obs = $_POST['txt_veri_observaciones'];
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_vertit', 1, 'INT'); //verificacion completa

    $his02 = '<span>El colaborador: <b>'.$_SESSION['usuario_nom'].'</b> indica que los datos del Crédito están correctos. '.trim($obs).' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his02);

    $fecha_verificacion = date("Y-m-d H:i:s");
    $oCreditogarveh->modificar_campo($cre_id, 'tb_credito_fecveri', $fecha_verificacion, 'STR'); //fecha de verificacion

    $data['estado'] = 1;
    $data['titulo_msj'] = 'Datos guardados correctamente y Crédito actualizado';

    echo json_encode($data);
?>