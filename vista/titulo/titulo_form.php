<?php

require_once('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

require_once ("../funciones/fechas.php");

$cre_tip = $_POST['cre_tip']; // credito tipo 3 garveh, 4 hipo
$cre_id = intval($_POST['cre_id']);

$titulo = '';
$escritura_fec = '';
$escritura_num = '';

if($cre_id>0){
  $credito = $oCreditogarveh->mostrarUno($cre_id);
  if($credito['estado'] == 1){
    $titulo = $credito['data']['tb_credito_numtit'];
    $escritura_fec = ($credito['data']['tb_credito_fecescri'] == '0000-00-00') ? '' : mostrar_fecha_hora($credito['data']['tb_credito_fecescri']);
    $escritura_num = $credito['data']['tb_credito_numescri'];
  }
}

?>
<!-- MODULO ANTHONY-->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_titulo" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
          <h4 class="modal-title"><b>Número de Título, Fecha de Escritura y Número de Escritura</b></h4>
      </div>
      <form id="form_titulo">
        <input type="hidden" name="hdd_cre_tip" value="<?php echo $cre_tip;?>">
	      <input type="hidden" name="hdd_cre_id" value="<?php echo $cre_id;?>">
        <div class="modal-body">
          <div class="panel panel-info">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <?php if($cre_id>0){ ?>
                    <div class="form-group">
                      <label for="txt_credito_numtit" class="control-label" >Número de Título:</label>
                      <input name="txt_credito_numtit" type="text" class="form-control input-sm" id="txt_credito_numtit" size="52" value="<?php echo $titulo ?>">
                    </div>
                    <div class="form-group">
                      <label for="txt_credito_fecescri" class="control-label" >Fecha de Escritura:</label>
                      <div class="input-group">
                        <div class='input-group date' id='datetimepicker5'>
                          <input type='text' maxlength="10" class="form-control input-sm" name="txt_credito_fecescri" id="txt_credito_fecescri" readonly value="<?php echo $escritura_fec ?>" />
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="txt_credito_numescri" class="control-label" >Número de Escritura:</label>
                      <input name="txt_credito_numescri" type="text" class="form-control input-sm" id="txt_credito_numescri" size="52" value="<?php echo $escritura_num ?>">
                    </div>
                  <?php }else{ ?>
                    <h3>No se encuentra crédito asociado.</h3>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="btn_guardartitulo">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_titulo" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/titulo/titulo_form.js?ver=112';?>"></script>