$(document).ready(function() {
	$('#datetimepicker5' ).datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });

  $("#form_titulo").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL + "titulo/titulo_reg.php",
				async:true,
				dataType: "json",
				data: $("#form_titulo").serialize(),
				beforeSend: function() {
					//$('#msj_titulo').html("Guardando...");
					//$('#msj_titulo').show(100);
				},
				success: function(data){						
					if(parseInt(data.estado) > 0){
						alerta_success("EXITO",data.titulo_msj);
						$('#modal_titulo').modal('hide');
						creditogarveh_tabla();
          }
					else
						console.log(data);
				},
				complete: function(data){
					console.log(data);
				}
			});
		},
		rules: {
			txt_credito_numtit: {
				required: true
			},
			txt_credito_fecescri: {
				required: true
			},
			txt_credito_numescri: {
				required: true
			}
		},
		messages: {
			txt_credito_numtit: {
				required: 'Ingresa Número de Título'
			},
			txt_credito_fecescri: {
				required: 'Ingresa Fecha de Escritura'
			},
			txt_credito_numescri: {
				required: 'Ingresa Número de Escritura'
			}
		}
	});
});

