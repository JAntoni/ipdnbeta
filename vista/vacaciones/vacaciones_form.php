<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../vacaciones/Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once('../empresa/Empresa.class.php');
$oEmpresa = new Empresa();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'vacaciones';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$vacaciones_id = $_POST['vacaciones_id'];
$personal_usuario = intval($_POST['usuario_id']);
$vista = $_POST['vista'];

$titulo = '';
if ($usuario_action == 'L') {
  $titulo = 'Vacaciones Registrado';
} elseif ($usuario_action == 'I') {
  $titulo = 'Registrar Vacaciones';
} elseif ($usuario_action == 'M') {
  $titulo = 'Editar Vacaciones';
} elseif ($usuario_action == 'E') {
  $titulo = 'Eliminar Vacaciones';
} else {
  $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en vacaciones
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id']; $modulo = 'vacaciones'; $modulo_id = $vacaciones_id; $tipo_permiso = $usuario_action; $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $bandera = 4;
      $mensaje =  'No tienes el permiso para la acción que deseas realizar: '.$action;
    }
    $result = NULL;
  }

  $vacaciones_fecini = date('d-m-Y');
  $vacaciones_fecfin = date('d-m-Y');
  $style_precio = 'style="display: none;"';
  $fecha_registrado = 'Fecha actual '.date('d-m-Y');
  //si la accion es modificar, mostramos los datos del vacaciones por su ID
  if (intval($vacaciones_id) > 0) {
    $result = $oVacaciones->mostrarUno($vacaciones_id);
      if ($result['estado'] == 1){
        $vacaciones_fecini = mostrar_fecha($result['data']['tb_vacaciones_fecini']);
        $vacaciones_fecfin = mostrar_fecha($result['data']['tb_vacaciones_fecfin']);
        $vacaciones_usureg = $result['data']['tb_vacaciones_usureg'];
        $dias_tomados = $result['data']['tb_dias_tomados'];
        $usuario_id = $result['data']['tb_usuario_id'];
        $vacaciones_forma = $result['data']['tb_vacaciones_forma'];
        $vacaciones_precio = $result['data']['tb_vacaciones_precio'];
        $vacaciones_des = $result['data']['tb_vacaciones_des'];
        $fecha_registrado = 'Registrado el '.mostrar_fecha($result['data']['tb_vacaciones_reg']);
        $vacaciones_reg = mostrar_fecha($result['data']['tb_vacaciones_reg']);
        if(intval($vacaciones_forma == 2))
          $style_precio = '';
      }
    $result = NULL;
  }

  if ($personal_usuario > 0) {
    $result = $oUsuario->mostrarUno($personal_usuario);
      if ($result['estado'] == 1) {
        $usuario_nombre = $result['data']['tb_usuario_nom'];
        $usuario_apellido = $result['data']['tb_usuario_ape'];
        $usuario_per = $result['data']['tb_usuario_per'];
      }
    $result = NULL;
  }
} 
else {
  $mensaje = $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vacaciones" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo.' <span class="badge bg-green">'.$fecha_registrado.'</span>'; ?></h4>
        </div>
        <form id="form_vacaciones" method="post">
          <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_vacaciones_id" id="hdd_vacaciones_id" value="<?php echo $vacaciones_id; ?>">
          <input type="hidden" name="hdd_vacaciones_reg" id="hdd_vacaciones_reg" value="<?php echo $vacaciones_reg; ?>">
          <input type="hidden" id="hdd_vista" value="<?php echo $vista;?>">

          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Colaborador:</label> 
                  <input type="text" class="form-control input-sm" value="<?php echo $usuario_nombre . ' ' . $usuario_apellido ?>" readonly="">
                  <input type="hidden" class="form-control input-sm" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $personal_usuario ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>INICIO DE VACACIONES :</label>
                  <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control input-sm" name="txt_fecha_inicio" id="txt_fecha_inicio" value="<?php echo $vacaciones_fecini; ?>" readonly/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>FIN DE VACACIONES :</label>
                  <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control input-sm" name="txt_fecha_fin" id="txt_fecha_fin" value="<?php echo $vacaciones_fecfin; ?>" readonly/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Forma de Vacaciones:</label>
                  <select class="form-control input-sm" name="txt_vacaciones_forma" id="txt_vacaciones_forma">
                    <option value="1" <?php if($vacaciones_forma == 1) echo 'selected';?> >Tomar días de descanso</option>
                    <option value="2" <?php if($vacaciones_forma == 2) echo 'selected';?> >Vender los días de vacaciones</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 precio_vacaciones" <?php echo $style_precio;?>>
                <div class="form-group">
                  <label>Precio de Vacaciones:</label>
                  <input type='text' class="form-control input-sm moneda" name="txt_vacaciones_precio" id="txt_vacaciones_precio" value="<?php echo $vacaciones_precio;?>"/>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>BREVE DESCRIPCIÓN:</label>
                  <textarea class="form-control" id="txt_vacaciones_des" name="txt_vacaciones_des"><?php echo $vacaciones_des;?></textarea>
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if ($action == 'eliminar') : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i>
                  <center> ¿Está seguro de que desea Finalizar el Contrato del colaborador <b><?php echo $usuario_nombre . ' ' . $usuario_apellido ?></b> ?</center>
                </h4>
              </div>
            <?php endif; ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="vacaciones_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_vacaciones">Guardar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_vacaciones">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_vacaciones">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/vacaciones/vacaciones_form.js?ver=1'; ?>"></script>