<?php
if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');

class Vacaciones  extends Conexion{
    
  public $vacaciones_id;
  public $usuario_id;
  public $vacaciones_fecini;
  public $vacaciones_fecfin;
  public $vacaciones_usureg;
  public $dias_tomados;//1 vigente,2 finalizado
  public $vacaciones_forma;
  public $vacaciones_precio;
  public $vacaciones_des;
    
  function insertar(){
    $this->dblink->beginTransaction();
    try {
      $columns = [
        'tb_usuario_id',
        'tb_vacaciones_fecini',
        'tb_vacaciones_fecfin',
        'tb_vacaciones_usureg',
        'tb_dias_tomados',
        'tb_vacaciones_forma',
        'tb_vacaciones_precio',
        'tb_vacaciones_des',
      ];

      // Lista de placeholders
      $placeholders = implode(',', array_map(function ($column) {
          return ':' . $column;
      }, $columns));

      $sql = "INSERT INTO tb_vacaciones (" . implode(',', $columns) . ") VALUES ($placeholders)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id",$this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vacaciones_fecini",$this->vacaciones_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_vacaciones_fecfin",$this->vacaciones_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_vacaciones_usureg",$this->vacaciones_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_dias_tomados",$this->dias_tomados, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vacaciones_forma",$this->vacaciones_forma, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vacaciones_precio",$this->vacaciones_precio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_vacaciones_des",$this->vacaciones_des, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $vacaciones_id = $this->dblink->lastInsertId();

      $data['estado'] = $result; //si es correcto el ingreso retorna 1
      $data['vacaciones_id'] = $vacaciones_id;

      $this->dblink->commit();

      return $data; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      echo 'Error!: '.$e->getMessage();
    }
  }
    
  function modificar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_vacaciones SET 
        tb_vacaciones_fecini =:tb_vacaciones_fecini,
        tb_vacaciones_fecfin =:tb_vacaciones_fecfin,
        tb_dias_tomados =:tb_dias_tomados,
        tb_vacaciones_forma =:tb_vacaciones_forma,
        tb_vacaciones_precio =:tb_vacaciones_precio,
        tb_vacaciones_des =:tb_vacaciones_des 
        WHERE tb_vacaciones_id =:tb_vacaciones_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_vacaciones_fecini",$this->vacaciones_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_vacaciones_fecfin",$this->vacaciones_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_dias_tomados",$this->dias_tomados, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vacaciones_forma",$this->vacaciones_forma, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_vacaciones_precio",$this->vacaciones_precio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_vacaciones_des",$this->vacaciones_des, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_vacaciones_id",$this->vacaciones_id, PDO::PARAM_INT);
      
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function mostrarTodos_usuario($usuario_id){
    try {
      $sql ="SELECT * FROM tb_vacaciones WHERE tb_vacaciones_xac = 1 AND tb_usuario_id =:usuario_id ORDER BY tb_vacaciones_id DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Registros Vacaciones seleccionados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrarTodos(){
    try {
      $sql ="SELECT * FROM tb_vacaciones WHERE tb_vacaciones_xac = 1 ORDER BY tb_vacaciones_id DESC";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Registros Vacaciones seleccionados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_dias($vacaiones_id,$dias){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_vacaciones SET 
                                      tb_dias_tomados = tb_dias_tomados +(:dias)
                                      
                                  WHERE 
                                      tb_vacaciones_id =:tb_vacaciones_id AND tb_vacaciones_est=1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_vacaciones_id", $vacaiones_id, PDO::PARAM_INT);
      $sentencia->bindParam(":dias", $dias, PDO::PARAM_INT);
      
      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
    /* GERSON (27-04-23) */
    function mostrarTodosNuevo($estado){
      try {
        $sql ="SELECT * FROM tb_vacaciones WHERE tb_vacaciones_xac=1";

        if($estado==1){
          $sql .= " AND tb_vacaciones_est = 1";
        }

        $sql .= " ORDER BY tb_vacaciones_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Registros Vacaciones seleccionados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrar_usuario_vigente($id){
      try {
        $sql = "SELECT * FROM tb_vacaciones V inner join tb_usuario U on U.tb_usuario_id=V.tb_usuario_id WHERE V.tb_vacaciones_xac=1 AND U.tb_usuario_id=:tb_vacaciones_id AND V.tb_vacaciones_est=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_vacaciones_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    /* GERSON (22-04-23) */
    function mostrarTodosPorVencer($fecha){
      try {
        $sql ="SELECT
                vac.tb_vacaciones_id,
                CONCAT(col.tb_usuario_nom,' ',col.tb_usuario_ape) AS colaborador,
                vac.tb_usuario_id,
                vac.tb_fecha_inicio_contrato,
                vac.tb_fecha_fin_contrato,
                TIMESTAMPDIFF( DAY, vac.tb_fecha_fin_contrato, :fecha ) AS dias_vencido 
              FROM
                tb_vacaciones vac
                INNER JOIN tb_usuario col ON vac.tb_usuario_id = col.tb_usuario_id 
              WHERE
                tb_vacaciones_xac = 1 
                AND tb_vacaciones_est = 1 
                AND ( TIMESTAMPDIFF( DAY, vac.tb_fecha_fin_contrato, :fecha ) < 0 AND TIMESTAMPDIFF( DAY, vac.tb_fecha_fin_contrato, :fecha ) >= - 5 ) 
                AND ( vac.tb_fecha_inicio_contrato <= :fecha AND vac.tb_fecha_fin_contrato >= :fecha)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha",$fecha, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Registros Vacaciones seleccionados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrarTodosVencidos($fecha){
      try {
        $sql ="SELECT
                vac.tb_vacaciones_id,
                CONCAT(col.tb_usuario_nom,' ',col.tb_usuario_ape) AS colaborador,
                vac.tb_usuario_id,
                vac.tb_fecha_inicio_contrato,
                vac.tb_fecha_fin_contrato,
                TIMESTAMPDIFF( DAY, vac.tb_fecha_fin_contrato, :fecha ) AS dias_vencido 
              FROM
                tb_vacaciones vac
                INNER JOIN tb_usuario col ON vac.tb_usuario_id = col.tb_usuario_id 
              WHERE
                tb_vacaciones_xac = 1 
                AND tb_vacaciones_est = 1 
                AND TIMESTAMPDIFF( DAY, tb_fecha_fin_contrato, :fecha ) >= 0 
                AND ( tb_fecha_inicio_contrato <= :fecha)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha",$fecha, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Registros Vacaciones seleccionados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    function mostrarUno($id){
      try {
        $sql = "SELECT * FROM tb_vacaciones WHERE tb_vacaciones_xac=1 AND tb_vacaciones_id=:tb_vacaciones_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_vacaciones_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarUno_usuario($id){
      try {
        $sql = "SELECT * FROM tb_vacaciones V inner join tb_usuario U on U.tb_usuario_id=V.tb_usuario_id WHERE V.tb_vacaciones_xac=1 AND U.tb_usuario_id=:tb_vacaciones_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_vacaciones_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_contratos_documentos($vacaciones_id){
      try {
        $sql = "SELECT * FROM upload docs 
          INNER JOIN tb_vacaciones vaca on (vaca.tb_vacaciones_id = docs.modulo_id AND modulo_nom = 'contratos') 
          WHERE vaca.tb_vacaciones_id =:vacaciones_id and upload_xac = 1 order by upload_id DESC";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vacaciones_id", $vacaciones_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_campo($tb_vacaciones_id, $tb_vacaciones_columna, $tb_vacaciones_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        $param_tip = strtoupper($param_tip);

        if(!empty($tb_vacaciones_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_vacaciones SET ".$tb_vacaciones_columna." =:tb_vacaciones_valor WHERE tb_vacaciones_id =:tb_vacaciones_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_vacaciones_id", $tb_vacaciones_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":tb_vacaciones_valor", $tb_vacaciones_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":tb_vacaciones_valor", $tb_vacaciones_valor, PDO::PARAM_STR);
            $result = $sentencia->execute();
            $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function eliminar($vacaciones_id){
      $this->dblink->beginTransaction();
      try {
        $sql="UPDATE tb_vacaciones SET tb_vacaciones_xac = 0 WHERE tb_vacaciones_id=:tb_vacaciones_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_vacaciones_id", $vacaciones_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    //juan 16-04-2024
    function listar_vacaciones_mes($usuario_id, $periodo_mes, $periodo_anio){
      try {
        $periodo_mes = intval($periodo_mes);

        $sql ="SELECT * FROM tb_vacaciones WHERE tb_vacaciones_xac = 1 AND tb_usuario_id =:usuario_id AND MONTH(tb_vacaciones_fecini) =:periodo_mes AND YEAR(tb_vacaciones_fecini) =:periodo_anio";
  
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":periodo_mes", $periodo_mes, PDO::PARAM_INT);
        $sentencia->bindParam(":periodo_anio", $periodo_anio, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->execute();
  
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Registros Vacaciones seleccionados";
          $retorno["data"] = "";
        }
  
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
}
