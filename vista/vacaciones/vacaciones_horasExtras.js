/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });
    $('#datetimepicker3').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fecha_inicio').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fecha_fin').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });




    $('#form_vacaciones').validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "vacaciones/vacaciones_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_vacaciones").serialize(),
                beforeSend: function () {
                    $('#vacaciones_mensaje').show(400);
//                    $('#btn_guardar_vacaciones').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        $('#vacaciones_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#vacaciones_mensaje').html(data.mensaje);
                        swal_success("SISTEMA", data.mensaje, 2500);
                        setTimeout(function () {
                            vacaciones_tabla();
                            $('#modal_registro_vacaciones_dias').modal('hide');
                        }, 1000
                                );
                    } else {
                        $('#vacaciones_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#vacaciones_mensaje').html('Alerta: ' + data.mensaje);
                        swal_warning("AVISO", data.mensaje, 7500);
//                        $('#btn_guardar_vacaciones').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    $('#vacaciones_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#vacaciones_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_vacaciones_nom: {
                required: true,
                minlength: 2
            },
            txt_vacaciones_des: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            txt_vacaciones_nom: {
                required: "Ingrese un nombre para el Area",
                minlength: "Como mínimo el nombre debe tener 2 caracteres"
            },
            txt_vacaciones_des: {
                required: "Ingrese una descripción para el Area",
                minlength: "La descripción debe tener como mínimo 5 caracteres"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });



});