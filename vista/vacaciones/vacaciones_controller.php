<?php

require_once('../../core/usuario_sesion.php');
require_once('Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once('../uploadpdf/Upload.class.php');
$oUpload = new Upload();
require_once('../contratolinea/Contratolinea.class.php');
$ocontratolinea = new Contratolinea();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../proveedor/Proveedor.class.php');
$oProveedor = new Proveedor();
require_once('../asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$action = $_POST['action'];

$oVacaciones->vacaciones_id = $_POST['hdd_vacaciones_id'];
$oVacaciones->usuario_id = $_POST['hdd_usuario_id'];
$oVacaciones->vacaciones_fecini = fecha_mysql($_POST['txt_fecha_inicio']);
$oVacaciones->vacaciones_fecfin = fecha_mysql($_POST['txt_fecha_fin']);
$oVacaciones->vacaciones_usureg = $_SESSION['usuario_id'];
$oVacaciones->vacaciones_forma = intval($_POST['txt_vacaciones_forma']); //? la forma de vacaciones: 1 descansados, 2 vendidos
$oVacaciones->vacaciones_precio = moneda_mysql($_POST['txt_vacaciones_precio']);
$oVacaciones->vacaciones_des = $_POST['txt_vacaciones_des'];

$dias_tomado = restaFechas($_POST['txt_fecha_inicio'], $_POST['txt_fecha_fin']); //? sumarle 1 porque cuando se selecciona 1 día, devuelve 0
$dias_tomado += 1; //? para poder evitar que devuelva 0 cuando se selecciona un solo día
$oVacaciones->dias_tomados = $dias_tomado;

$result = $oUsuario->mostrarUno($_POST['hdd_usuario_id']);
  if ($result['estado'] == 1) {
    $usuario_nombre = $result['data']['tb_usuario_nom'];
    $usuario_apellido = $result['data']['tb_usuario_ape'];
    $usuario_per = $result['data']['tb_usuario_per'];
    $usuario_dni = $result['data']['tb_usuario_dni'];
  }
$result = NULL;

if ($action == 'insertar') {
  $precio_vacaciones = moneda_mysql($_POST['txt_vacaciones_precio']);

  if(intval($_POST['txt_vacaciones_forma']) == 2){
    if($precio_vacaciones <= 0){
      $data['estado'] = 0;
      $data['mensaje'] = 'No puedes vender las vacaciones por un precio de: '.$precio_vacaciones;

      echo json_encode($data); exit();
    }

    $proveedor_id = 0;
    $result = $oProveedor->mostrar_por_dni($usuario_dni);
      if ($result['estado'] == 1) {
        $proveedor_id = $result['data']['tb_proveedor_id'];
      }
    $result = NULL;

    if($proveedor_id <= 0){
      $data['estado'] = 0;
      $data['mensaje'] = 'El colaborador: '.$usuario_nombre.', no está registrado como PROVEEEDOR, se debe registrar primero, DNI: '.$usuario_dni;

      echo json_encode($data); exit();
    }
  }

  $result = $oVacaciones->insertar();
    $vacaciones_id = $result['vacaciones_id'];
  $result = NULL;

  //* VAMOS JUSTIFICAR SUS ASISTENCIAS EN LOS DÍAS QUE ESTÁ SALIENDO COMO VACACIONES PARA QUE NO LOS TOME COMO FALTAS YA QUE NO MARACARÁ ASISTENCIA
  $fecha_inicio = new DateTime($_POST['txt_fecha_inicio']);
  $fecha_fin = new DateTime($_POST['txt_fecha_fin']);
  $fecha_fin->modify('+1 day'); // Esto es para incluir el día final en el periodo
  $intervalo = new DateInterval('P1D');
  $periodo = new DatePeriod($fecha_inicio, $intervalo, $fecha_fin);
  $usuario_id = intval($_POST['hdd_usuario_id']);
  $asistencia_obs = 'Se justifica la asistencia por solicitud de vacaciones del colaborador | <b>'.date('d-m-Y h:i a').'</b>';
  
  foreach ($periodo as $fecha) {
    $asistencia_fec = $fecha->format('Y-m-d');
    $oAsistencia->justificar_asistencia_por_vacaciones($usuario_id, $asistencia_fec, $asistencia_obs);
  }

  //* SI ESTÁ VENDIENDO LAS VACACIONES VAMOS A REALIZAR EL EGRESO EN CAJA POR LA VENTA
  
  if(intval($_POST['txt_vacaciones_forma']) == 2){
    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
    $oEgreso->egreso_fec = date('Y-m-d');
    $oEgreso->documento_id = 9; //OE otros egresos
    $oEgreso->egreso_numdoc = 'MOD-1-VAC-'.$vacaciones_id;
    $oEgreso->egreso_det = 'Egreso por venta de vacaciones para el colaborador, vacaciones seleccionadas desde: '.$_POST['txt_fecha_inicio'].' hasta '.$_POST['txt_fecha_fin'].', un total de '.$dias_tomado.' días.';
    $oEgreso->egreso_imp = moneda_mysql($_POST['txt_vacaciones_precio']);
    $oEgreso->egreso_tipcam = 1;
    $oEgreso->egreso_est =1;
    $oEgreso->cuenta_id = 11; //ID 11 HABERES
    $oEgreso->subcuenta_id = 36; // USB CUENTA VACACIONES;
    $oEgreso->proveedor_id = $proveedor_id; //el proveedor ID 1, egreso sin proveedor
    $oEgreso->cliente_id = 0;
    $oEgreso->usuario_id = intval($_POST['hdd_usuario_id']); //el egreso no es para ningún colaborador
    $oEgreso->caja_id = 1; //la cada ID 1 es la caja general
    $oEgreso->moneda_id = 1; //1 soles, 2 dolares
    $oEgreso->modulo_id = 1; // modulo 1 para egreso es pago de vacaciones
    $oEgreso->egreso_modide = $vacaciones_id;
    $oEgreso->empresa_id = $_SESSION['empresa_id'];
    $oEgreso->egreso_ap = 0; //aquí va el cuotapago de un acuerdo de pago

    $result = $oEgreso->insertar();
    $egreso_id = $result['egreso_id'];
    $result = NULL;
  }
  //insertar las fechas seleccionadas para las vacaciones

  // $ocontratolinea->tb_contratolinea_det = "Ha otorgado <b>" . $dias_tomado . "</b> día(s) de vacaciones al colaborador <b>" . $usuario_nombre . ' ' . $usuario_apellido . '</b> que empieza desde el dia <b>' . $_POST['txt_fecha_inicio'] . '</b> y culmina el dia <b>' . $_POST['txt_fecha_fin'] . '</b>';
  // $ocontratolinea->tb_vacaciones_id = intval($vacaciones_id);
  // $ocontratolinea->tb_usuario_id = $_SESSION['usuario_id'];
  // $ocontratolinea->tb_contratolinea_vacini = fecha_mysql($_POST['txt_fecha_inicio']);
  // $ocontratolinea->tb_contratolinea_vacfin = fecha_mysql($_POST['txt_fecha_fin']);
  // $ocontratolinea->insertarVacaciones();

  $data['estado'] = 1;
  $data['mensaje'] = 'Vacaciones Registradas correctamente';

  echo json_encode($data);
}
elseif ($action == 'modificar') {
  $vacaciones_id = intval($_POST['hdd_vacaciones_id']);
  $precio_vacaciones = moneda_mysql($_POST['txt_vacaciones_precio']);

  if(intval($_POST['txt_vacaciones_forma']) == 2){
    $vacaciones_reg = $_POST['hdd_vacaciones_reg'];
    $fecha_hoy = date('d-m-Y');

    if($fecha_hoy != $vacaciones_reg){
      $data['estado'] = 0;
      $data['mensaje'] = 'No puedes modificar unas vacaciones vendidas de fecha anterior, fecha hoy: '.$fecha_hoy.', fecha anterior: '.$vacaciones_reg;

      echo json_encode($data); exit();
    }

    if($precio_vacaciones <= 0){
      $data['estado'] = 0;
      $data['mensaje'] = 'No puedes vender las vacaciones por un precio de: '.$precio_vacaciones;

      echo json_encode($data); exit();
    }

    $proveedor_id = 0;
    $result = $oProveedor->mostrar_por_dni($usuario_dni);
      if ($result['estado'] == 1) {
        $proveedor_id = $result['data']['tb_proveedor_id'];
      }
    $result = NULL;

    if($proveedor_id <= 0){
      $data['estado'] = 0;
      $data['mensaje'] = 'El colaborador: '.$usuario_nombre.', no está registrado como PROVEEEDOR, se debe registrar primero, DNI: '.$usuario_dni;

      echo json_encode($data); exit();
    }
  }

  //insertar las fechas seleccionadas para las vacaciones
  $oVacaciones->modificar();

  if(intval($_POST['txt_vacaciones_forma']) == 2){
    //* AL MODIFICAR LAS VACACIONES VENDIDAS, VAMOS A ELIMINAR EL EGRESO ANTERIOR PARA REGISTRAR EL NUEVO

    $result = $oEgreso->mostrar_por_modulo(1, $vacaciones_id, 1, 1); // modulo 1, vacaciones id, moneda 1, xac = 1
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value) {
          $egreso_antiguo_id = $value['tb_egreso_id'];
          $oEgreso->modificar_campo($egreso_antiguo_id, 'tb_egreso_xac', '0', 'INT');
        }
      }
    $result = NULL;
    
    //* REGISTRAMOS EL NUEVO EGRESO DEBIDO A LA MODIFICACION
    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
    $oEgreso->egreso_fec = date('Y-m-d');
    $oEgreso->documento_id = 9; //OE otros egresos
    $oEgreso->egreso_numdoc = 'MOD-1-VAC-'.$vacaciones_id;
    $oEgreso->egreso_det = 'Egreso por venta de vacaciones para el colaborador, vacaciones seleccionadas desde: '.$_POST['txt_fecha_inicio'].' hasta '.$_POST['txt_fecha_fin'].', un total de '.$dias_tomado.' días.';
    $oEgreso->egreso_imp = moneda_mysql($_POST['txt_vacaciones_precio']);
    $oEgreso->egreso_tipcam = 1;
    $oEgreso->egreso_est =1;
    $oEgreso->cuenta_id = 11; //ID 11 HABERES
    $oEgreso->subcuenta_id = 36; // USB CUENTA VACACIONES;
    $oEgreso->proveedor_id = $proveedor_id; //el proveedor ID 1, egreso sin proveedor
    $oEgreso->cliente_id = 0;
    $oEgreso->usuario_id = intval($_POST['hdd_usuario_id']); //el egreso no es para ningún colaborador
    $oEgreso->caja_id = 1; //la cada ID 1 es la caja general
    $oEgreso->moneda_id = 1; //1 soles, 2 dolares
    $oEgreso->modulo_id = 1; // modulo 1 para egreso es pago de vacaciones
    $oEgreso->egreso_modide = $vacaciones_id;
    $oEgreso->empresa_id = $_SESSION['empresa_id'];
    $oEgreso->egreso_ap = 0; //aquí va el cuotapago de un acuerdo de pago

    $result = $oEgreso->insertar();
    $egreso_id = $result['egreso_id'];
    $result = NULL;
  }

  // $ocontratolinea->tb_contratolinea_det = "Ha otorgado <b>" . $dias_tomado . "</b> día(s) de vacaciones al colaborador <b>" . $usuario_nombre . ' ' . $usuario_apellido . '</b> que empieza desde el dia <b>' . $_POST['txt_fecha_inicio'] . '</b> y culmina el dia <b>' . $_POST['txt_fecha_fin'] . '</b>';
  // $ocontratolinea->tb_vacaciones_id = intval($vacaciones_id);
  // $ocontratolinea->tb_usuario_id = $_SESSION['usuario_id'];
  // $ocontratolinea->tb_contratolinea_vacini = fecha_mysql($_POST['txt_fecha_inicio']);
  // $ocontratolinea->tb_contratolinea_vacfin = fecha_mysql($_POST['txt_fecha_fin']);
  // $ocontratolinea->insertarVacaciones();

  $data['estado'] = 1;
  $data['mensaje'] = 'Vacaciones Modificado correctamente';

  echo json_encode($data);
}
elseif($action == 'eliminar'){
  $vacaciones_id = intval($_POST['hdd_vacaciones_id']);
  $precio_vacaciones = moneda_mysql($_POST['txt_vacaciones_precio']);
  $vacaciones_reg = $_POST['hdd_vacaciones_reg'];
  $fecha_hoy = date('d-m-Y');

  if($fecha_hoy != $vacaciones_reg){
    $data['estado'] = 0;
    $data['mensaje'] = 'No puedes ELIMINAR unas vacaciones de fecha anterior, fecha hoy: '.$fecha_hoy.', fecha anterior: '.$vacaciones_reg;

    echo json_encode($data); exit();
  }
  else{
    $oVacaciones->eliminar($vacaciones_id);
    $data['estado'] = 1;
    $data['mensaje'] = 'Vacaciones eliminados correctamente';

    echo json_encode($data);
  }
}
elseif ($action == 'contratos_vencidos') {


  //? ACTUALIZADO AHORA LAS ALERTAS POR CONTRATOS VENCIDOS SE HACE DESDE GESTION DEL PERSONAL


  $fecha = date('Y-m-d');
  $por_vencer = array();
  $vencidos = array();

  $dts1 = $oVacaciones->mostrarTodosPorVencer($fecha);
  if ($dts1['estado'] == 1) {
    $i = 0;
    foreach ($dts1['data'] as $key => $value1) {
      $v1['id'] = $value1['tb_vacaciones_id'];
      $v1['colaborador'] = $value1['colaborador'];
      $v1['fin_contrato'] = $value1['tb_fecha_fin_contrato'];
      $v1['vencimiento'] = $value1['dias_vencido'];
      $por_vencer[$i] = $v1;
      $i++;
    }
  }

  $dts2 = $oVacaciones->mostrarTodosVencidos($fecha);
  if ($dts2['estado'] == 1) {
    $j = 0;
    foreach ($dts2['data'] as $key => $value2) {
      $v2['id'] = $value2['tb_vacaciones_id'];
      $v2['colaborador'] = $value2['colaborador'];
      $v2['fin_contrato'] = $value2['tb_fecha_fin_contrato'];
      $v2['vencimiento'] = $value2['dias_vencido'];
      $vencidos[$j] = $v2;
      $j++;
    }
  }


  $data['estado'] = 1;
  $data['por_vencer'] = $por_vencer;
  $data['vencidos'] = $vencidos;
  //$data['mensaje'] = 'Vacaciones Registradas correctamente';

  echo json_encode($data);
}
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
