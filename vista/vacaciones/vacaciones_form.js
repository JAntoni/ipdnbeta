$(document).ready(function () {

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
  });

  $('#datetimepicker3').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
  });

  $('.moneda').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999.00'
	});

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_fecha_inicio').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_fecha_fin').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $('#txt_vacaciones_forma').change(function (e) {
    var forma = $(this).val();
    if(forma == 1){
      $('.precio_vacaciones').hide(300)
      $('#txt_vacaciones_precio').val(0);
    }
    else{
      $('.precio_vacaciones').show(300)
      $('#txt_vacaciones_precio').val('');
    }
  });

  $("#form_vacaciones").validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "vacaciones/vacaciones_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_vacaciones").serialize(),
        beforeSend: function () {
          $("#vacaciones_mensaje").show(400);
        },
        success: function (data) {
          var vista = $('#hdd_vista').val();
          if (parseInt(data.estado) > 0) {
            swal_success("SISTEMA", data.mensaje, 2500);
            if(vista == "personalcontrato")
              personalcontrato_tabla()
            
            $("#modal_registro_vacaciones").modal("hide");
          } 
          else {
            alerta_error("AVISO", data.mensaje);
          }
        },
        complete: function (data) {
          
        },
        error: function (data) {
          alerta_error("AVISO", data.responseText);
          console.log(data)
        },
      });
    },
    rules: {
      txt_fecha_inicio: {
        required: true
      },
      txt_fecha_fin: {
        required: true
      },
      txt_vacaciones_des: {
        required: true
      }
    },
    messages: {
      txt_fecha_inicio: {
        required: "Ingrese una fecha de inicio"
      },
      txt_fecha_fin: {
        required: "Ingrese una fecha final"
      },
      txt_vacaciones_des: {
        required: "Ingresa un breve detalle"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      // Add the `help-block` class to the error element
      error.addClass("help-block");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.parent("label"));
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });
});


function upload_formpdf(usuario_act, upload_id) {
  upload_uniq = $("#hdd_upload_uniq").val();
  vacaciones_id = $("#hdd_vacaciones_id").val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "uploadpdf/upload_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      upload_id: upload_id,
      upload_uniq: upload_uniq,
      modulo_nom: "contratos", //nombre de la tabla a relacionar
      modulo_id: vacaciones_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_uploadpdf_form").html(data);
        $("#modal_registro_upload").modal("show");

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_upload"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_upload", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
    },
  });
}
