
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <label for="cmb_usu_id">SEDE:</label>
                <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm mayus">

                    <?php require_once '../empresa/empresa_select.php'; ?>
                </select>
            </div>
            <div class="col-md-6">
                <label for="cmb_usu_id">COLABORADOR:</label>
                <select name="cmb_usu_id" id="cmb_usu_id" class="form-control input-sm mayus">
                    
                    <?php
                        if($action=="modificar"||$action=="eliminar"){
                            echo '<option value="'.$usuario_id.'">'.$usuario_apellido.' '.$usuario_nombre.'</option>';
                        }
                    ?>
                    
                </select>
            </div>
        </div>
        <p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>INICIO DE CONTRATO :</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fecha_inicio" id="txt_fecha_inicio" value="<?php echo mostrar_fecha($fecha_inicio_contrato) ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>FIN DE CONTRATO :</label>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fecha_fin" id="txt_fecha_fin" value="<?php echo mostrar_fecha($fecha_fin_contrato) ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <input type='hidden' class="form-control input-sm" name="txt_fecha_fin2" id="txt_fecha_fin2" value="<?php echo mostrar_fecha($fecha_fin_contrato) ?>"/>
                    <!-- /.input group -->
                </div>
            </div>
        </div>
        <?php
        if ($action == 'modificar') {

            $hoy=date('Y-m-d');
            if(strtotime($hoy)>strtotime($fechaFin)){
            $hoy= fecha_mysql($fecha_fin_contrato);
            }
            
            $dias = (strtotime($fecha_inicio_contrato) - strtotime($hoy)) / 86400;
            $dias = abs($dias);
            $dias = floor($dias);
            $dias_vacaciones = 0;
            $añoslaborando = CalcularAnios($fecha_inicio_contrato, date('Y-m-d'));
            if ($añoslaborando > 0) {
                if ($usuario_per == 1) {
                    $dias_vacaciones = 30 * $añoslaborando;
                } else {
                    $dias_vacaciones = 15 * $añoslaborando;
                }
            } else {
                $añoslaborando = 0;
            }
            $dias_tomados = $tb_dias_tomados;
            $dias_restantes = $dias_vacaciones - $dias_tomados;
            ?>
            <p>
            <div class="row">
                <div class="col-md-4">
                    <label>DIAS DE VACACIONES</label>
                    <input type="text" class="form-control input-sm" id="txt_dias_vacaciones" name="txt_dias_vacaciones" readonly="" value="<?php echo $dias . ' Dias / ' . $añoslaborando . ' Años' ?>">
                </div>
                <div class="col-md-4">
                    <label>DIAS TOMADOS</label>
                    <input type="text" class="form-control input-sm" id="txt_dias_tomados" name="txt_dias_tomados" readonly="" value="<?php echo $dias_tomados ?>">
                </div>
                <div class="col-md-4">
                    <label>DIAS RESTANTES</label>
                    <input type="text" class="form-control input-sm" id="txt_dias_restantes" name="txt_dias_restantes" readonly="" value="<?php echo $dias_restantes ?>">
                </div>
            </div>


        </div>
    </div>

    <label style="font-family: cambria;font-weight: bold;color: #660066">DETALLES DE CONTRATO(s) DEL COLABORADOR</label>
    <div class="box box-primary">
        <div class="box-body">


            <!--            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NUEVO FIN DE CONTRATO</label>
                                    <div class='input-group date' id='datetimepicker3'>
                                        <input type='text' class="form-control input-sm" name="txt_fecha_nuevo_fin" id="txt_fecha_nuevo_fin" value="<?php echo mostrar_fecha($fecha_fin_contrato) ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                     /.input group 
                                </div>
                            </div>
                        </div>-->
        <?php } ?>

        <p>
        <div class="row">
            <div class="col-xs-12">
              <button type="button" class="btn btn-success btn-sm" id="btnimagen" title="Adjuntar contrato de colaborardor" onclick="upload_formpdf('I', 0)"><i class="fa fa-address-card-o"></i> Contrato</button>
                <input type="hidden" value="<?php echo md5(uniqid('', true)).''.time(); ?>" id="hdd_upload_uniq" name="hdd_upload_uniq">
            </div>
        </div>
    </div>
</div>