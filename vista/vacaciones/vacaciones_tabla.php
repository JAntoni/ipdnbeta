<?php
//    date_default_timezone_set("America/Lima");
require_once('../../core/usuario_sesion.php');
require_once('Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../empresa/Empresa.class.php');
$oEmpresa = new Empresa();
require_once('../personalcontrato/Personalcontrato.class.php');
$oPersonal = new Personalcontrato();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$fecha_hoy = date('Y-m-d');
$dias_tomados = 0;

$result = $oPersonal->mostrar_uno_condicion($personal_usuario_id, 'primero', ''); //? obtener el primer registro de contratos sin especificar el tipo
  if($result['estado'] == 1){
    $inicio_labores = $result['data']['tb_personalcontrato_fecini'];
    $usuario_per = $result['data']['tb_usuario_per']; // tipo de personal, donde solo Engel tiene 30 días de vacaciones por año = 1
  }
$result = NULL;

$result = $oPersonal->mostrar_uno_condicion($personal_usuario_id, 'ultimo', '1,2,3');
  if($result['estado'] == 1){
    $fin_labores = $result['data']['tb_personalcontrato_fecfin'];
    $usuario_per = $result['data']['tb_usuario_per']; // tipo de personal, donde solo Engel tiene 30 días de vacaciones por año = 1
    if(strtotime($fecha_hoy) > strtotime($fin_labores))
      $fecha_hoy = $result['data']['tb_personalcontrato_fecfin'];
  }
$result = NULL;

$tr = '';
$result = $oVacaciones->mostrarTodos_usuario($personal_usuario_id); //? variable declarada en: personalcontrato_tabla.php
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      $dias_tomados += intval($value['tb_dias_tomados']);
      $forma = '<span class="badge bg-aqua">Descansados</span>';
      if(intval($value['tb_vacaciones_forma']) == 2)
        $forma = '<span class="badge bg-green">Vendidos</span>';
      $tr .= '
        <tr>
          <td>'.mostrar_fecha($value['tb_vacaciones_fecini']).'</td>
          <td>'.mostrar_fecha($value['tb_vacaciones_fecfin']).'</td>
          <td>'.$value['tb_dias_tomados'].'</td>
          <td>'.$forma.'</td>
          <td>'.mostrar_moneda($value['tb_vacaciones_precio']).'</td>
          <td>
            <a class="btn btn-info btn-xs" onclick="vacaciones_form(\'L\', '.$value['tb_vacaciones_id'].')"><i class="fa fa-eye"></i></a> 
            <a class="btn btn-warning btn-xs" onclick="vacaciones_form(\'M\', '.$value['tb_vacaciones_id'].')"><i class="fa fa-pencil"></i></a> 
            <a class="btn btn-danger btn-xs" onclick="vacaciones_form(\'E\', '.$value['tb_vacaciones_id'].')"><i class="fa fa-trash"></i></a>
          </td>
        </tr>';
    }
  }
$result = null;
$dias_vacaciones = 0;
$dias_laborando = (strtotime($fecha_hoy) - strtotime($inicio_labores)) / 86400;
$anios_laborando = CalcularAnios($inicio_labores, $fecha_hoy); //formato Y-m-d

if($anios_laborando > 0){
  if($usuario_per == 1)
    $dias_vacaciones = 30 * $anios_laborando;
  else
    $dias_vacaciones = 15 * $anios_laborando;
}
else
  $anios_laborando = 0;

$dias_restantes = $dias_vacaciones - $dias_tomados;
?>
<div class="row">
  <div class="col-md-3">
    <div class="alert alert-info alert-dismissible">
      <h5><i class="icon fa fa-info"></i> <b>T. Laborado</b></h5>
      <?php echo $dias_laborando.' días / '.$anios_laborando.' años';?>
    </div>
  </div>
  <div class="col-md-3">
    <div class="alert alert-info alert-dismissible">
      <h5><i class="icon fa fa-info"></i> <b>V. Otorgados</b></h5>
      <?php echo $dias_vacaciones.' días';?>
    </div>
  </div>
  <div class="col-md-3">
    <div class="alert alert-warning alert-dismissible">
      <h5><i class="icon fa fa-info"></i> <b>V. Tomadas</b></h5>
      <?php echo $dias_tomados.' días';?>
    </div>
  </div>
  <div class="col-md-3">
    <div class="alert alert-success alert-dismissible">
      <h5><i class="icon fa fa-info"></i> <b>V. por Tomar</b></h5>
      <?php echo $dias_restantes.' días';?>
    </div>
  </div>
</div>

<table class="table table-hover">
  <thead>
    <tr>
      <th>Inicio Vacaciones</th>
      <th>Fin Vacaciones</th>
      <th>Días Tomados</th>
      <th>Forma</th>
      <th>Precio</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr; ?>
  </tbody>
</table>