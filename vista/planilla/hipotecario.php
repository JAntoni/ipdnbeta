
<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">HIPOTECARIO</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table style="width: 100%;font-family: cambria" class="table-hover">
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">TIPO</th>
                <th id="tabla_cabecera_fila">PRECIO ACORDADO</th>
                <th id="tabla_cabecera_fila">TIPO CAMBIO</th>
                <th id="tabla_cabecera_fila">PORCENTAJE</th>
                <th id="tabla_cabecera_fila">COMISIÓN</th>
            </tr>
            <?php
            if(empty($hipo[0])){
                $perso = $tip_per; //según tipo de personal del usuario 1 jefa, 2 ventas
                $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
                $est = 1; // el estado debe ser vigente 1, 0 no vigente
                $cretip = 4; //el creditotipo_id de hipotecario es 4
                $cre_asveh = 0; //hipotecario no tiene venta nueva ni reventa
                $total_comiHipo = 0;
                $MONTO_COLOCADO_HIPO = 0;

                if(!empty($dtsHipo['data'])){
                    foreach ($dtsHipo['data']as $key=>$dt) {
                        $moneda = 'S/.';
                        if ($dt['tb_moneda_id'] == 2)
                            $moneda = 'US$';

                        $usu_reg = $dt['tb_credito_usureg']; //id del usuario que registra el credito

                        $valido = '';
                        if (moneda_mysql($dt['tb_credito_valido']) > 0) {
                            $monto = $dt['tb_credito_valido'] * $dt['tb_credito_tipcam'];
                            $valido = '| <b>' . mostrar_moneda($dt['tb_credito_valido']) . '</b>';
                        } else
                            $monto = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];

                        $MONTO_COLOCADO_HIPO += $monto;
                        
                        /* $dts4 = $oUsuario->mostrarUno($usu_reg);
                            $dt4 = mysql_fetch_array($dts4);
                            $tip_per_usureg = $dt4['tb_usuario_per']; //tipo de personal al que pertence 1 jefatura, 2 ventas
                            mysql_free_result($dts4); */

                        //solo para hipotecario si el credito es registrado por jefatura gana 1%, vendedor gana 0.5%, jefatura por vendedor gana 0.5%, ANTES ERA TIP_PER_USUREG, PERO AHORA SIGUE LA MISMA REALGA QUE GARVEH
                        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes,$empresa_id);

                        if($dtsComi['estado']==1){
                            $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                        }

                        $comi = formato_numero(($monto * $porcen) / 100);
                        $total_comiHipo += $comi;

                        echo '
                            <tr>
                            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">CRDITO HIPOTECARIO (CH-' . $dt['tb_credito_id'] . ')</td>
                            <td id="tabla_fila" align="center">' . $moneda . ' ' . mostrar_moneda($dt['tb_credito_preaco']) . ' ' . $valido . '</td>
                            <td id="tabla_fila" align="center">' . $dt['tb_credito_tipcam'] . '</td>
                            <td id="tabla_fila" align="center">' . $porcen . ' %</td>
                            <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($comi) . '</td>
                            </tr>';
                    }
                }
            }else{

                $total_comiHipo = 0.00;
                foreach ($hipo as $key => $value) {
                    echo '
                        <tr>
                            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . $value->tipo . '</td>
                            <td id="tabla_fila" align="center">' . $value->moneda . ' ' . $value->precio_acordado . '</td>
                            <td id="tabla_fila" align="center">' . $value->tipo_cambio . '</td>
                            <td id="tabla_fila" align="center">' . $value->porcentaje . ' </td>
                            <td id="tabla_fila" align="center">S/. ' . floatval($value->comision) . '</td>
                        </tr>';

                    $total_comiHipo = $total_comiHipo + floatval($value->comision);
                    
                }

            }
            ?>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="4">TOTAL COMISIONES:</td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($total_comiHipo) ?></td>
            </tr>
        </table>
    </div>
</div>