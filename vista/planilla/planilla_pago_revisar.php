<?php
require_once("../../core/usuario_sesion.php");

require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../menorgarantia/Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../proveedor/Proveedor.class.php');
$oProveedor = new Proveedor();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once('../planilla/Planilla.class.php');
$oPlanilla = new Planilla();
require_once('../formulacomision/Formulacomision.class.php');
$oComision = new Formulacomision();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../vehiculomarca/Vehiculomarca.class.php');
$oVehMarc = new Vehiculomarca();
require_once('../vehiculomodelo/Vehiculomodelo.class.php');
$oVehMod = new Vehiculomodelo();
require_once('../vehiculoclase/Vehiculoclase.class.php');
$oVehCla = new Vehiculoclase();
require_once('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$usu_id = $_POST['usu_id'];

$monto = $_POST['monto_des'];
$monto_saldo = $_POST['monto_saldo'];
$sueldo_bruto = moneda_mysql($_POST['sueldo_bruto']);
$sueldo_comision = moneda_mysql($_POST['sueldo_comision']);
$tipo = $_POST['tipo'];
$detalle =  mb_strtoupper($_POST['txt_detalle']) . ' [ ' . mb_strtoupper($_POST['txt_cuopag_obs']) . ' ]';

$mes = $_POST['mes'];
$anio = $_POST['anio'];
$fecha_hoy = date('d-m-Y');

if ($action == 'sueldo' || $action == 'comision') {
  if (!empty($usu_id) && !empty($monto)) {

    $monto_com = 0;
    $dts = $oVentainterna->listar_ventainterna($usu_id, $mes, $anio);
      $rows = count($dts['data']);
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $monto_com += $dt['tb_ventainterna_mon'];
        }
      }
    $dts = NULL;

    $dts = $oUsuario->mostrarUno($usu_id);
      if ($dts['estado'] == 1) {
        $usu_nom = $dts['data']['tb_usuario_nom'] . ' ' . $dts['data']['tb_usuario_ape'];
        $usu_dni = $dts['data']['tb_usuario_dni'];
      }
    $dts = NULL;

    $dts = $oProveedor->mostrar_por_dni($usu_dni);
      if ($dts['estado'] == 1) {
        $pro_id = $dts['data']['tb_proveedor_id'];
      }
    $dts = NULL;
    
    if (empty($pro_id)) {
      $data['est'] = 0;
      $data['msj'] = 'EL colaborador no está registrado como proveedor, revise por favor.';
      echo json_encode($data);
      exit();
    }

    $xac = 1;
    $numdoc = "";
    $compras = '';
    if ($rows > 0)
      $compras = '. Tiene ' . $rows . ' compras, monto total de: S/. ' . mostrar_moneda($monto_com);

    $array_mes = array(
      '01' => 'ENERO', '02' => 'FEBRERO', '03' => 'MARZO', '04' => 'ABRIL', '05' => 'MAYO', '06' => 'JUNIO', '07' => 'JULIO',
      '08' => 'AGOSTO', '09' => 'SEPTIEMBRE', '10' => 'OCTUBRE', '11' => 'NOVIEMBRE', '12' => 'DICIEMBRE'
    );

    $det = 'PAGO DE HABERES MENSUALES DEL MES DE ' . $array_mes[$mes] . ' DEL ' . $anio . ' AL COLABORADOR ' . $usu_nom . '. Su sueldo bruto es: ' . mostrar_moneda($sueldo_bruto) . ' y su sueldo de comision es: ' . mostrar_moneda($sueldo_comision);
    if (moneda_mysql($monto) < moneda_mysql($monto_saldo)) {
      $det = 'ADELANTO DE PAGO DE HABERES MENSUALES DEL MES DE ' . $array_mes[$mes] . ' DEL ' . $anio . ' AL COLABORADOR ' . $usu_nom . '. Su sueldo bruto es: ' . mostrar_moneda($sueldo_bruto) . ' y su cueldo de comision es: ' . mostrar_moneda($sueldo_comision);
    }
    $det .= $det . ' // <b style="color: #0000cc">' . $detalle . '</b> // Monto Desembolsado es de: <b>' . $monto . '</b>';
    $cuenta = 11;
    $subcuenta = 35;

    $modide = $anio . $mes; //el modid sera la union del anio y mes para poder consultar y no volver a pagar el mismo mes

    //* TIPO 1 SE REFIERE A FORMA DE PAGO EN EFECTIVO EN CAJA
    if ($tipo == 1) {
      $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
      $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
      $oEgreso->egreso_fec = fecha_mysql($fecha_hoy);
      $oEgreso->documento_id = 9;
      $oEgreso->egreso_numdoc = $numdoc;
      $oEgreso->egreso_det = $det;
      $oEgreso->egreso_imp = moneda_mysql($monto);
      $oEgreso->egreso_tipcam = 0;
      $oEgreso->egreso_est = 1;
      $oEgreso->cuenta_id = $cuenta;
      $oEgreso->subcuenta_id = $subcuenta;
      $oEgreso->proveedor_id = $pro_id;
      $oEgreso->cliente_id = 0;
      $oEgreso->usuario_id = 0;
      $oEgreso->caja_id = 1;
      $oEgreso->moneda_id = 1;
      $oEgreso->modulo_id = 64;
      //                $oEgreso->modulo_id = $mod_id;
      $oEgreso->egreso_modide = intval($modide);
      $oEgreso->empresa_id = $_SESSION['empresa_id'];
      $oEgreso->egreso_ap = 0; //si
      
      $result = $oEgreso->insertar();
        if (intval($result['estado']) == 1) {
          $egr_id = $result['egreso_id'];
        }
      $result = NULL;

      $oEgreso->modificar_campo($egr_id, 'tb_egreso_numdoc', $_SESSION['usuario_id'], 'STR');
      $oEgreso->modificar_campo($egr_id, 'tb_egreso_detalleplanilla', $detalle, 'STR');

      $data['est'] = 1;
      $data['msj'] = 'Se ha generado el egreso correctamente';
      echo json_encode($data);
    }

    //* TIPO 2 SE REFIERE A FORMA DE PAGO EN BANCO
    if ($tipo == 2) {

      $fec_pago = $_POST['txt_cuopag_fec'];

      $numope = trim($_POST['cuopag_numope']);
      $monto_validar = moneda_mysql($_POST['txt_cuopag_mon']); //elimina las comas y devuelve con 2 decimales
      $moneda_deposito_id = 1; //1 soles, 2 dolares
      $moneda_credito_id = 1; //1 soles, 2 dolares

      $deposito_mon = 0;
      $dts = $oDeposito->validar_num_operacion($numope);
        if ($dts['estado'] == 1) {
          foreach ($dts['data'] as $key => $dt) {
            $deposito_mon += floatval($dt['tb_deposito_mon']);
          }
        }
      $dts = NULL;

      if ($deposito_mon == 0) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
        echo json_encode($data);
        exit();
      }

      $ingreso_importe = 0;
      $dts = $oIngreso->lista_ingresos_num_operacion($numope);
        if ($dts['estado'] == 1) {
          foreach ($dts['data'] as $key => $dt) {
            $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
            $ingreso_mon_id = intval($dt['tb_moneda_id']);
            $ingreso_importe += $tb_ingreso_imp;
          }
        }
      $dts = NULL;

      $monto_usar = abs($deposito_mon) - $ingreso_importe;
      $monto_usar = moneda_mysql($monto_usar);
      //echo 'tooo esto hay een importes: '.$ingreso_importe.' // depo: '.$deposito_mon.' // validar: '.$monto_validar.' // usar: '.$monto_usar.' // moneda ing: '.$ingreso_mon_id.' // tipo cambio: '.$tipocambio_dia; exit();

      if ($monto_validar > $monto_usar) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) . ' | validar: ' . $monto_validar . ' | usar: ' . $monto_usar . ' | ingre: ' . $ingreso_importe . ' | cambio: ' . $tipocambio_dia . ' / mond ing: ' . $ingreso_mon_id;
        echo json_encode($data);
        exit();
      }

      $moneda_depo = 'S/.';

      if (!empty($_POST['txt_cuopag_mon'])) {
        $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: ' . trim($_SESSION['usuario_nombre']) . ', en la fecha de: ' . date('d-m-Y h:i a') . ', desde la computadora con nombre: ' . $nombre_pc . ' y MAC: ' . $mac_pc . ' ||&& ';

        $pedidos = '';

        //registro de ingreso
        $cue_id = 43; // INGRESO / EGRESO PORBANCO
        $subcue_id = 148; // BBVA CONTINENTAL
        $mod_id = 0; //cuota pago


        $ing_det = 'INGRESO FICTICIO DE CAJA EN BANCO = Pago abonado en el banco, en la siguente cuenta N°: [ ' . $_POST['txt_num_cuenta'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ] POR CONCEPTO DE=[' . $det . ']';


        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha;
        $oIngreso->documento_id = 8;
        $oIngreso->ingreso_numdoc = '';
        $oIngreso->ingreso_det = $ing_det;
        $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id;
        $oIngreso->cliente_id = 1144;
        $oIngreso->caja_id = 1;
        $oIngreso->moneda_id = 1;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id;
        $oIngreso->ingreso_modide = $modide;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];

        $result = $oIngreso->insertar();
        if (intval($result['estado']) == 1) {
          $ingr_id = $result['ingreso_id'];
        }
        $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
        $oIngreso->ingreso_numope = $_POST['cuopag_numope'];
        $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
        $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
        $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
        $oIngreso->banco_id = $_POST['cmb_banco_id'];
        $oIngreso->ingreso_id = $ingr_id;

        $oIngreso->registrar_datos_deposito_banco();

        $tipo_pag = 'LOS HABERES MENSUALES';
        $egr_det = ' EGRESO FICTICIO DE CAJA EN BANCO = Pago abonado en el banco, en la siguente cuenta N°: [ ' . $_POST['txt_num_cuenta'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ] ---- POR CONCEPTO DE =[' . $det . ']';

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = '';
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cuenta;
        $oEgreso->subcuenta_id = $subcuenta;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = 1;
        $oEgreso->moneda_id = 1;
        $oEgreso->modulo_id = 0;
        $oEgreso->egreso_modide = intval($modide);
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $result = $oEgreso->insertar();
        if (intval($result['estado']) == 1) {
          $egr_id = $result['egreso_id'];
        }

        $oEgreso->modificar_campo($egr_id, 'tb_egreso_numdoc', $_SESSION['usuario_id'], 'STR');
        $oEgreso->modificar_campo($egr_id, 'tb_egreso_detalleplanilla', $detalle, 'STR');

        $data['est'] = 1;
        $data['msj'] = 'Se ha generado el egreso correctamente';
        echo json_encode($data);
      }
    }
  } 
  else {
    $data['est'] = 0;
    $data['msj'] = 'El usuario o el monto están vacíos, actualice y revise.';
    echo json_encode($data);
  }
}

if ($action == 'comisiones_asesor') {
  $data['est'] = 0;
  $data['msj'] = 'Antes se guardaba en una tabla las comisiones del asesor mostradas, se ha borrado esa forma de guardado, revisar este caso en el archivo: planilla_pago_revisar.php en planilla';
  echo json_encode($data);
}
