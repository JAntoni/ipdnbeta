<!--EN LA EMPRESA-->
<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">DESCUENTOS POR COMPRA DE COLABORADOR</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table class="table-hover"  style="font-family: cambria;font-size: 12px;width: 100%">
            <tr id="tabla_cabecera" style="height:20px;">
                <th id="tabla_cabecera_fila" width="50%">TIPO DESCUENTO</th>
                <th id="tabla_cabecera_fila" width="10%">TOTAL</th>
                <th id="tabla_cabecera_fila" width="15%">MONTO</th>
                <th id="tabla_cabecera_fila" width="10%">PAGO POR</th>
                <th id="tabla_cabecera_fila" width="10%"></th>
            </tr>
            <?php
            $dts = $oVentainterna->listar_ventainterna($usu_id, $mes, $anio);

            $total_pagado_venta_interna = 0.00;

            if ($dts['estado'] == 1) {
                $tb_ventainterna_mon = 0;
                foreach ($dts['data']as $key => $dt) {
                    
                    $pagar = '<strong style="color: green;">PAGADO</strong>';
                    $modulo='';
                    if ($dt['tb_ventainterna_est'] == 0){

                        // fecha de actualizacion para pago parcial 07/06/2023
                        $procede_parcial = 0; // para distinguir que registro puede usar pago parcial
                        $fecha_corte = '2023-06-30';
                        $dts2 = $oVentainterna->lista_garantia_fuera_actua($usu_id, $fecha_corte);
                        if ($dts2['estado'] == 1) {
                            foreach ($dts2['data']as $key => $dt2) {
                                if($dt2['tb_ventainterna_id'] == $dt['tb_ventainterna_id']){
                                    $procede_parcial = 1; // procede pago parcial de venta interna
                                }
                            }
                        }
                        //

                        //JUAN 01-09-2023
                        $result = $oIngreso->consulta_ingresos_ventainterna_pago_caja($dt['tb_ventainterna_id']);
                            if($result['estado'] == 1){
                                foreach($result['data']as $key2 => $value) {
                                    $total_pagado_venta_interna += $value['tb_ingreso_imp'];
                                }
                            }
                        $result = NULL;
                        //

                        //GERSON 31-10-23
                        $result2 = $oIngreso->consulta_ingresos_ventainterna_pago_banco($dt['tb_ventainterna_id']);
                        if($result2['estado'] == 1){
                            foreach($result2['data']as $key2 => $value) {
                                $total_pagado_venta_interna += $value['tb_ingreso_imp'];
                            }
                        }
                        $result2 = NULL;
                        //

                        $pagar = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="pagar_ventainterna('.$dt['tb_ventainterna_id'].','.$procede_parcial.')" title="Pagar"><i class="fa fa-money"></i></a>';
                        $tb_ventainterna_mon+=$dt['tb_ventainterna_mon'];

                    }
                    
                    if ($dt['tb_ventainterna_est'] == 1 && $dt['tb_ventainterna_tipo'] == 1){
                        $pagar = '<strong style="color: green;">PAGADO</strong>';
                        $modulo='<strong style="color: blue;">PLANILLA</strong>';
                        $tb_ventainterna_mon+=$dt['tb_ventainterna_mon'];

                        $result = $oIngreso->consulta_ingresos_ventainterna_pago_caja($dt['tb_ventainterna_id']);
                            if($result['estado'] == 1){
                                foreach($result['data']as $key2 => $value) {
                                    $total_pagado_venta_interna += $value['tb_ingreso_imp'];
                                }
                            }
                        $result = NULL;
                        //GERSON 31-10-23
                        $result2 = $oIngreso->consulta_ingresos_ventainterna_pago_banco($dt['tb_ventainterna_id']);
                        if($result2['estado'] == 1){
                            foreach($result2['data']as $key2 => $value) {
                                $total_pagado_venta_interna += $value['tb_ingreso_imp'];
                            }
                        }
                        $result2 = NULL;
                        //
                    }
                    if ($dt['tb_ventainterna_est'] == 1 && $dt['tb_ventainterna_tipo'] ==0){
                         $modulo='<strong style="color: black;">CAJA</strong>';
                    }
                    
                    $dtsGar = $oGarantia->mostrarUno($dt['tb_garantia_id']);
                    if ($dtsGar['estado'] == 1) {
                        $garantia_cre_id = $dtsGar['data']['tb_credito_id'];
                    }

                    echo '
                        <tr style="height:30px;">  
                          <td id="tabla_fila" style="font-weight: bold">COMPRA: ' . $dt['tb_garantia_pro'] . '. Crédito CM-' . str_pad($garantia_cre_id, 4, "0", STR_PAD_LEFT) . '</td>
                          <td id="tabla_fila" style="text-align: center">1 com</td>
                          <td id="tabla_fila" style="text-align: center">S/. ' . mostrar_moneda($dt['tb_ventainterna_mon']) . ' | S/. '.mostrar_moneda($dt['tb_ventainterna_monpag']).'</td> 
                          <td id="tabla_fila" style="text-align: center">'.$modulo.'</td>
                          <td id="tabla_fila" style="text-align: center">'.$pagar.'</td>
                          <td id="tabla_fila" style="text-align: center"></td>
                        </tr>';

                    
                }

            } else {
                echo '
                        <tr style="height:20px;">  
                          <td id="tabla_fila" style="font-weight: bold">COMPRAS</td>
                          <td id="tabla_fila" style="text-align: center">0 COM</td>
                          <td id="tabla_fila" style="text-align: center">S/. 0</td>
                          <td id="tabla_fila" style="text-align: center"></td>
                          <td id="tabla_fila" style="text-align: center"></td>
                        </tr>';
            }

            /* echo '==============================';
            echo '<br>';
            echo '<strong>SUELDO:</strong> S/ '.$SUELDO_TOTAL;
            echo '<br>';
            echo '<strong>DEUDA TOTAL DEL MES:</strong> S/ '.$tb_ventainterna_mon;
            echo '<br>';
            echo '<strong>PAGOS PARCIALES:</strong> S/'.$total_pagado_venta_interna;
            echo '<br>';
            echo '==============================';
            echo '<br>'; */

            $resta1 = ($tb_ventainterna_mon - $total_pagado_venta_interna);
            //$resta2 = $SUELDO_TOTAL - ($tb_ventainterna_mon);
            $resta2 = $SUELDO_TOTAL - ($tb_ventainterna_mon - $total_pagado_venta_interna);
            if ($resta2 <= 0)
                $resta2 = 0;
            $SUELDO_TOTAL2=$resta2;
            ?>

        </table>
        <br>
         <table style="width: 100%;font-family: cambria;" class="table-hover">  
            <tr style="height: 30px">  
                <td id="tabla_fila" colspan="4">
                    <!-- <strong style="color: red;">Aplicando los descuentos por colaborador un total de <span id="">S/. <?php echo mostrar_moneda($tb_ventainterna_mon, 1);?> </span></strong> -->
                    <strong style="color: red;">Aplicando los descuentos por colaborador un total de <span id="">S/. <?php echo mostrar_moneda($resta1, 2).' | Pagos por caja: '.$total_pagado_venta_interna;?> </span></strong>
                </td>
            </tr>
            <tr style="height: 30px">  
            <td id="tabla_fila">
              <strong style="color: green;">Finalmente el sueldo a pagar es: S/: <span id="span_recibe"><?php echo number_format($SUELDO_TOTAL2, 2);?></span></strong>
            </td>
            <?php // if($tb_ventainterna_mon==0){?>
            <input type="hidden" id="hdd_total_sueldo" value="<?php echo moneda_mysql(number_format($SUELDO_TOTAL2, 2));?>">
            <td id="tabla_fila" align="center"><a class="btn_sueldo btn btn-danger btn-xs" onclick="pagar_colaborador_form('sueldo',<?php echo $usu_id;?>)" title="Pagar Sueldo"><i class="fa fa-money"></i></a></td>
            <?php // } ?>
            </tr>
        </table>
       
    </div>
</div> 
<!--FIN EN LA EMPRESA-->
<br>
<?php


?>
