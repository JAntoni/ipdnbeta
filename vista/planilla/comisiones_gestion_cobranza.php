<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">COMISIONES POR GESTIÓN DE COBRANZA <?php echo $comentario_metas; ?></label>
<div class="panel panel-primary">
  <div class="panel-body">
      <table style="width: 100%;font-family: cambria;" class="table-hover">
          <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">TIPO</th>
            <th id="tabla_cabecera_fila">COMISIÓN</th>
          </tr>
          <tr>
            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">COMISIONES POR RECAUDACIÓN</td>
            <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($pago_recaudo);?></td>
          </tr>
          <tr>
            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">COMISIONES GC</td>
            <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($pago_mejoragc);?></td>
          </tr>
          <tr>
            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">TOTAL DE COMISIONES POR GC</td>
            <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($pago_mejoragc + $pago_recaudo);?></td>
          </tr>
        </table>
  </div>
</div>
