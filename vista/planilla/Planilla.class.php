<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Planilla extends Conexion {

    function comision_asiveh($mes, $anio, $usu_id,$empresa_id) {

        try {
            $sql = "SELECT tb_credito_id,tb_credito_est,tb_credito_tip1,tb_credito_tip2,tb_credito_preaco,tb_moneda_id,tb_credito_fecvig as tb_credito_fecdes,tb_empresa_id,
                    tb_credito_tipcam FROM tb_creditoasiveh where tb_credito_xac =1 and tb_credito_est NOT IN(1,2,8) and tb_credito_tip1 != 3 and tb_credito_tip2 NOT IN(2,5,6,7) 
                    and tb_credito_int > 0 and Month(tb_credito_fecvig) =:tb_credito_fecvig and Year(tb_credito_fecvig) =:tb_credito_fecvig2";
                    if (!empty($usu_id))
                    $sql .= " and tb_credito_usureg =:tb_credito_usureg";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_fecvig", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_credito_fecvig2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
//                $resultado = $sentencia->fetch();
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (28-03-23) */
    function comision_asiveh_new($mes, $anio, $usu_id,$empresa_id) {

        try {
            $sql = "SELECT tb_credito_id,tb_credito_est,tb_credito_tip1,tb_credito_tip2,tb_credito_preaco,tb_moneda_id,tb_credito_fecvig as tb_credito_fecdes,tb_empresa_id,
                    tb_credito_tipcam FROM tb_creditoasiveh where tb_credito_xac =1 and tb_credito_tip1 != 3 and tb_credito_tip2 NOT IN(2,5,6,7) 
                    and tb_credito_int > 0 and Month(tb_credito_fecvig) =:tb_credito_fecvig and Year(tb_credito_fecvig) =:tb_credito_fecvig2";
                    if (!empty($usu_id))
                    $sql .= " and tb_credito_usureg =:tb_credito_usureg";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_fecvig", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_credito_fecvig2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }


    function comision_garveh($mes, $anio, $usu_id,$empresa_id) {

        try {
            $sql = "SELECT 
                            tb_credito_id,tb_credito_est,tb_credito_tip,tb_credito_preaco,tb_moneda_id,tb_credito_fecvig as tb_credito_fecdes,tb_empresa_id,
                            tb_credito_usureg, tb_credito_tipcam, tb_credito_valido,tb_cliente_id,tb_vehiculomarca_id,tb_vehiculomodelo_id,tb_vehiculoclase_id, tb_credito_int
                    FROM 
                            tb_creditogarveh where tb_credito_xac =1 
                            and tb_credito_est NOT IN(1,2,8) and tb_credito_tip !=3 
                            and tb_credito_tip2 NOT IN(5,6) and tb_credito_int > 0 and Month(tb_credito_fecvig) =:tb_credito_fecvig
                            and Year(tb_credito_fecvig) =:tb_credito_fecvig2";
                    if (!empty($usu_id))
                    $sql .= " and tb_credito_usureg =:tb_credito_usureg";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";
//            $sql = "SELECT 
//                            tb_credito_id,tb_credito_est,tb_credito_tip,tb_credito_preaco,tb_moneda_id,tb_credito_fecvig as tb_credito_fecdes,
//                            tb_credito_usureg, tb_credito_tipcam, tb_credito_valido 
//                    FROM 
//                            tb_creditogarveh where tb_credito_xac =1 
//                            and tb_credito_est NOT IN(1,2,8) and tb_credito_tip !=3 
//                            and tb_credito_tip2 NOT IN(5,6) and tb_credito_int > 0 and Month(tb_credito_fecvig) =:tb_credito_fecvig
//                            and Year(tb_credito_fecvig) =:tb_credito_fecvig2";
//                    if (!empty($usu_id))
//                    $sql .= " and tb_credito_usureg =:tb_credito_usureg";
//                    if ($empresa_id>0)
//                    $sql .= " and tb_empresa_id =:tb_empresa_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_fecvig", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_credito_fecvig2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
//                $resultado = $sentencia->fetch();
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (28-03-23) */
    function comision_garveh_new($mes, $anio, $usu_id,$empresa_id) {

        try {
            $sql = "SELECT 
                            tb_credito_id,tb_credito_est,tb_credito_tip,tb_credito_preaco,tb_moneda_id,tb_credito_fecvig as tb_credito_fecdes,tb_empresa_id,
                            tb_credito_usureg, tb_credito_tipcam, tb_credito_valido,tb_cliente_id,tb_vehiculomarca_id,tb_vehiculomodelo_id,tb_vehiculoclase_id, tb_credito_int
                    FROM 
                            tb_creditogarveh where tb_credito_xac =1 
                            and tb_credito_tip !=3 
                            and tb_credito_tip2 NOT IN(5,6) and tb_credito_int > 0 and Month(tb_credito_fecvig) =:tb_credito_fecvig
                            and Year(tb_credito_fecvig) =:tb_credito_fecvig2";
                    if (!empty($usu_id))
                    $sql .= " and tb_credito_usureg =:tb_credito_usureg";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_fecvig", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_credito_fecvig2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */
    
    function comision_hipo($mes, $anio, $usu_id,$empresa_id){

        try {
            $sql = "SELECT 
                        tb_credito_id,tb_credito_est,tb_credito_preaco,tb_moneda_id,tb_credito_fecvig as tb_credito_fecdes,tb_credito_usureg, tb_credito_tipcam, tb_credito_usureg, tb_credito_valido
                    FROM 
                        tb_creditohipo where tb_credito_xac =1 and tb_credito_est NOT IN(1,2,8) and tb_credito_tip !=3 and tb_credito_tip2 NOT IN(5,6) and tb_credito_int > 0 
                        and Month(tb_credito_fecvig) =:tb_credito_fecvig and Year(tb_credito_fecvig) =:tb_credito_fecvig2";
                    if(!empty($usu_id))
                      $sql .= " and tb_credito_usureg =:tb_credito_usureg";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_fecvig", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_credito_fecvig2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
//                $resultado = $sentencia->fetch();
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (28-03-23) */
    function comision_hipo_new($mes, $anio, $usu_id,$empresa_id){

        try {
            $sql = "SELECT 
                        tb_credito_id,tb_credito_est,tb_credito_preaco,tb_moneda_id,tb_credito_fecvig as tb_credito_fecdes,tb_credito_usureg, tb_credito_tipcam, tb_credito_usureg, tb_credito_valido
                    FROM 
                        tb_creditohipo where tb_credito_xac =1 and tb_credito_tip !=3 and tb_credito_tip2 NOT IN(5,6) and tb_credito_int > 0 
                        and Month(tb_credito_fecvig) =:tb_credito_fecvig and Year(tb_credito_fecvig) =:tb_credito_fecvig2";
                    if(!empty($usu_id))
                      $sql .= " and tb_credito_usureg =:tb_credito_usureg";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_fecvig", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_credito_fecvig2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */
    
    function comision_menor($mes, $anio, $usu_id,$empresa_id){

        try {
            $sql = "SELECT 
                        DISTINCT tc.*
                    FROM 
                        tb_creditomenor tc 
                        inner JOIN tb_garantia tg on tg.tb_credito_id=tc.tb_credito_id
                        where tb_credito_xac =1 and tb_credito_est NOT IN(1,2) and Month(tb_credito_feccre) =:tb_credito_fecdes and Year(tb_credito_feccre) =:tb_credito_fecdes2 AND tb_garantiatipo_id NOT IN(5,6)";
                    if(!empty($usu_id))
                      $sql .= " and tb_credito_usureg =:tb_credito_usureg";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";


            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_fecdes", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_credito_fecdes2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function comision_ventagarantia($mes, $anio, $usu_id,$empresa_id){

        try {
            $sql = "SELECT 
                            tb_ventagarantia_id,tb_usuario_id,ga.tb_garantia_id,tb_ventagarantia_prec1,tb_ventagarantia_prec2, tb_ventagarantia_com, 
                            cre.tb_credito_id, tb_ventagarantia_fec, tb_ventagarantia_col, tb_garantia_val,cre.tb_cliente_id, cre.tb_credito_int
                    FROM 
                            tb_creditomenor cre inner join tb_garantia ga on cre.tb_credito_id = ga.tb_credito_id
                            inner join tb_ventagarantia vg on vg.tb_garantia_id = ga.tb_garantia_id where tb_credito_xac =1 
                            and tb_garantia_xac =1 and tb_ventagarantia_xac = 1 and tb_garantia_est = 1 and Month(tb_ventagarantia_fecreg) =:tb_ventagarantia_fecreg 
                            and Year(tb_ventagarantia_fecreg) =:tb_ventagarantia_fecreg2";
                    if(!empty($usu_id))
                      $sql .= " and vg.tb_usuario_id =:tb_usuario_id";
                    if ($empresa_id>0)
                    $sql .= " and vg.tb_empresa_id =:tb_empresa_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_ventagarantia_fecreg", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_ventagarantia_fecreg2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function comision_ventavehiculo($mes, $anio, $usu_id, $cre_tip,$empresa_id){

        try {
            $sql = "SELECT * FROM tb_ventavehiculo ven
                INNER JOIN tb_cliente cli on cli.tb_cliente_id = ven.tb_cliente_id 
                where tb_ventavehiculo_xac =1 and Month(tb_ventavehiculo_fec) =:mes and 
                Year(tb_ventavehiculo_fec) =:anio";
                    if(!empty($usu_id))
                      $sql .= " and tb_usuario_id2 =:tb_usuario_id2";
                    if(!empty($cre_tip))
                      $sql .= " and ven.tb_creditotipo_id =:tb_creditotipo_id";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";


            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
                $sentencia->bindParam(":tb_usuario_id2", $usu_id, PDO::PARAM_INT);
            if (!empty($cre_tip))
                $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
            if ($empresa_id>0)
                $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = array();
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    //LISTA DE TODOS LOS VEHICULOS VENDIDOS POR USUARIO, EMPRESA Y FECHA
    function lista_venta_vehiculos_garveh_asiveh($mes, $anio, $usuario_id, $empresa_id){

        try {
            $sql = "SELECT IF(venta.tb_creditotipo_id = 3, 'GARVEH', 'ASVEH') AS 'tipo_credito', venta.tb_usuario_id2, venta.tb_creditotipo_id, tb_ventavehiculo_fec, 
                tb_usuario_nom, tb_cliente_nom, tb_vehiculo_pla, venta.tb_moneda_id, tb_ventavehiculo_mon, tb_ventavehiculo_tipcam  FROM tb_ventavehiculo venta 
                INNER JOIN tb_usuario usu on usu.tb_usuario_id = venta.tb_usuario_id2 
                INNER JOIN tb_cliente cli on cli.tb_cliente_id = venta.tb_cliente_id
                WHERE LEFT(tb_ventavehiculo_fec, 7) =:anio_mes AND tb_ventavehiculo_xac = 1 AND tb_ventavehiculo_est = 1
            ";

            if(!empty($usuario_id))
                $sql .= " and venta.tb_usuario_id2 =:tb_usuario_id2";
            if ($empresa_id>0)
                $sql .= " and venta.tb_empresa_id =:tb_empresa_id";

            $anio_mes = $anio.'-'.$mes;
            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindParam(":anio_mes", $anio_mes, PDO::PARAM_STR);
            if (!empty($usuario_id))
                $sentencia->bindParam(":tb_usuario_id2", $usuario_id, PDO::PARAM_INT);
            if ($empresa_id>0)
                $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function comision_ventainmueble($mes, $anio, $usu_id,$empresa_id){

        try {
            $sql = "SELECT * FROM tb_stockinmueble where tb_stockinmueble_est = 3 and Month(tb_stockinmueble_fec) =:tb_stockinmueble_fec and Year(tb_stockinmueble_fec) =:tb_stockinmueble_fec2";
                    if(!empty($usu_id))
                      $sql .= " and tb_usuario_id =:tb_usuario_id";
                    if ($empresa_id>0)
                    $sql .= " and tb_empresa_id =:tb_empresa_id";
                    

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_stockinmueble_fec", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_stockinmueble_fec2", $anio, PDO::PARAM_STR);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            if ($empresa_id>0)
            $sentencia->bindParam(":tb_empresa_id", $empresa_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function creditos_liquidados_reversion($tabla, $colum_tipo, $cre_tip, $fecha1, $fecha2, $usu_id){

        try {
            $sql = "SELECT 
                            cre.tb_credito_id, cre.tb_moneda_id, $colum_tipo,tb_credito_preaco, 
                            tb_credito_tipcam, tb_credito_fecdes, tb_credito_est, tb_cuota_fec, tb_cuota_cuo,
                            tb_cuota_est,tb_cuotadetalle_cuo, tb_cuotadetalle_est 
                    FROM 
                            tb_cuota cuo INNER JOIN tb_cuotadetalle cud ON cud.tb_cuota_id = cuo.tb_cuota_id INNER JOIN $tabla cre ON cre.tb_credito_id = cuo.tb_credito_id 
                    WHERE 
                            tb_credito_int > 0 AND tb_cuota_num = 1 AND tb_cuotadetalle_num = 1 AND $colum_tipo IN (1,2,4) AND tb_credito_tip2 NOT IN(3,5,6) AND tb_cuota_est = 1 
                            AND tb_credito_est = 7 AND tb_credito_fecdes BETWEEN :fecha1 AND :fecha2 AND cuo.tb_creditotipo_id = :tb_creditotipo_id";
                    if(!empty($usu_id))
                    $sql .= " and tb_credito_usureg =:tb_credito_usureg";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
            if (!empty($usu_id))
            $sentencia->bindParam(":tb_credito_usureg", $usu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function total_descuentos($mes, $anio, $usu_id){

        try {
            $sql = "SELECT * FROM tb_descuento where Month(tb_descuento_fec) =:mes and Year(tb_descuento_fec) =:anio";
                    if(!empty($usu_id))
                      $sql .= " and tb_usuario_id =:tb_usuario_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
            if(!empty($usu_id))
            $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function CuotasPagadas($credito_id,$opcion,$estado){

        try {
            $sql = "SELECT $opcion FROM tb_cuota cuo 
          INNER JOIN tb_creditomenor cre on cre.tb_credito_id = cuo.tb_credito_id 
          INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id 
          LEFT JOIN tb_moneda mon on mon.tb_moneda_id = cre.tb_moneda_id
          WHERE cuo.tb_credito_id =:tb_credito_id and cuo.tb_creditotipo_id =1 and tb_cuota_xac = 1 AND tb_cuota_est=:tb_cuota_est";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuota_est", $estado, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                if($opcion=='COUNT(*)')
                $resultado = $sentencia->fetch();
                else
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /* GERSON (22-03-23) */
    function obtener_sueldo_basico(){

        try {
            $sql = "SELECT * FROM tb_config
                    WHERE tb_config_codigo = 10 
                    ORDER BY tb_config_fecreg DESC";

            $sentencia = $this->dblink->prepare($sql);
            
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */
}
