<form id="planilla_filtro" name="planilla_filtro">
    <input type="hidden" class="form-control input-sm" id="usuariogrupo_id" name="usuariogrupo_id" value="<?php echo $_SESSION['usuariogrupo_id']; ?>">
    <div class="row">
        <div class="col-md-2">
            <label for="cmb_usu_id">Sede:</label>
            <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm mayus">
                
                <?php require_once 'vista/empresa/empresa_select.php';?>
            </select>
        </div>
        <div class="col-md-3">
            <label for="cmb_usu_id">Colaborador:</label>
            <select name="cmb_usu_id" id="cmb_usu_id" class="form-control input-sm mayus"></select>
        </div>
        <div class="col-md-1">

            <label for="cmb_pla_mes">Mes:</label>
            <select name="cmb_pla_mes" id="cmb_pla_mes" class="form-control input-sm mayus">
                <?php
                $hoy_mes = date('m');
                $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                $num = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                $sel = '';

                for ($i = 0; $i < 12; $i++) {
                    if ($num[$i] == $hoy_mes)
                        echo '<option value="' . $num[$i] . '" selected>' . $mes[$i] . '</option>';
                    else
                        echo '<option value="' . $num[$i] . '">' . $mes[$i] . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="col-md-1">
            <label for="cmb_pla_anio">Año:</label>
            <select name="cmb_pla_anio" id="cmb_pla_anio" class="form-control input-sm mayus">
                <?php
                $anio = intval(date('Y'));
                for ($i = 2016; $i < ($anio + 5); $i++) {
                    if ($anio == $i)
                        echo '<option value="' . $i . '" selected="true">' . $i . '</option>';
                    else
                        echo '<option value="' . $i . '">' . $i . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="col-md-1">
            <label>&nbsp;</label><br>
            <a id="btn_generar" class="btn btn-primary btn-sm" onclick="planilla_colaborador()">Generar</a>
        </div>

    </div>

</form>