<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">VENTA AL CONTADO</label>
<div class="panel panel-primary">
  <div class="panel-body">
    <table style="width: 100%;font-family: cambria;" class="table-hover">
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">TIPO</th>
        <th id="tabla_cabecera_fila">CANTIDAD</th>
        <th id="tabla_cabecera_fila">MONTO TOTAL</th>
        <th id="tabla_cabecera_fila">PORCENTAJE</th>
        <th id="tabla_cabecera_fila">COMISIÓN</th>
        <th id="tabla_cabecera_fila" width="10%"></th>
      </tr>
      <?php
        $perso = $tip_per; //según tipo de personal del usuario
        $est = 1; // el estado debe ser vigente 1, 0 no vigente
        $total_comiVenta = 0;
        $cant = 0;
        $comi = 0;
        $usuario_id_consulta = $usu_id; //esto nos permite guardar el tipo de usuario del jefe de sede
        if ($tip_per == 1){
            $usu_id = '';
            $empresa=0;
        }
        if($tip_per == 2){
            $empresa=$empresa_id;
        }
        if($tip_per == 3){
            $usu_id = '';
            $empresa=$empresa_id;
        }
        
        //? LISTA DE VENTAS DE VEHICULOS DE CRÉDITOS DE (ASISTENCIA VEHICULAR)
        $dtsVentaVeh = $oPlanilla->comision_ventavehiculo($mes, $anio, $usu_id, 2,$empresa); //venta contado creditos asiveh
          $rowsVentaVeh = count($dtsVentaVeh['data']);
          //WHILE PARA VENTA CONTADO ASIVEH
          if ($dtsVentaVeh['estado']==1) {
              $monto = 0;
              $comiVenta = 0; $comision_extra = '';
              foreach ($dtsVentaVeh['data']as $key=>$dt) {
                  $moneda_id = $dt['tb_moneda_id'];
                  $monto_venta = $dt['tb_ventavehiculo_mon'];
                  if ($moneda_id == 2){
                      $monto_venta = ($dt['tb_ventavehiculo_mon']) * floatval($dt['tb_ventavehiculo_tipcam']);
                      $monto += $monto_venta;
                  }
                  else{
                      $monto += ($dt['tb_ventavehiculo_mon']);
                  }

                  $cretip = 2; //el creditotipo_id de ASIVEH es 2
                  $cre_asveh = 4; //para venta al contado el tipo credito ASIVEH será 4 REVENTA
                  $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
                  
                  $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_venta, $mes,$empresa_id);
                  if ($dtsComi['estado']==1) {
                      
                      $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                      /*el jefe de sede tiene por defecto 0.5 de comision por venta de vehiculo, pero si el jefe de sede vende por si solo
                      entonces debe ganar el 1% de cada venta*/
                      if($dt['tb_usuario_id2'] == $usuario_id_consulta && $perso == 3){
                          $porcen = 1; //esto indica que si jefe de sede vende vehiculo deja de ganar 0.5 para ganar el 1 %
                      }
                      
                      $comi = (($monto_venta * $porcen) / 100);
                      $comiVenta += $comi;
                      $total_comiVenta += $comi;

                      $comision_extra .= $porcen.'%/';
                  } else {
                      $porcen = 'Auto';
                  }
              }
              echo '
                    <tr>
                      <td id="tabla_fila" style="font-family:cambria;font-weight: bold">VENTA CONTADO ASIVEH</td>
                      <td id="tabla_fila" align="center">' . $rowsVentaVeh . '</td>
                      <td id="tabla_fila" align="center">S/. '.mostrar_moneda($monto).'</td>
                      <td id="tabla_fila" align="center">' . $comision_extra.'</td>
                      <td id="tabla_fila" align="center"><span>S/. '.mostrar_moneda($comiVenta).'</span></td>
                      <td id="tabla_fila" align="center" width="5"></td>
                    </tr>';
          }
        $dtsVentaVeh = NULL;
        
        $CANTIDAD_VEHICULOS_VENDIDOS = 0;
        
        //? LISTA DE VENTAS DE VEHICULOS DE CRÉDITOS DE (GARANTÍA VEHICULAR)
        $dtsVentaVeh = $oPlanilla->comision_ventavehiculo($mes, $anio, $usu_id, 3,$empresa); //venta contado creditos GARVEH
          $rowsVentaVeh = count($dtsVentaVeh['data']);
          
          //WHILE PARA VENTA CONTADO GARVEH
          if ($dtsVentaVeh['estado'] == 1) {

            $monto = 0;
            $comiVenta = 0;
            $comision_extra = '';

            $CANTIDAD_VEHICULOS_VENDIDOS = $rowsVentaVeh;

            foreach ($dtsVentaVeh['data']as $key=>$dt) {
              
              /* FORMULA ANTIGUA DE CALCULAR COMISIONES PARA JEFE DE SEDE
              if($perso==2){
                $porcen=0.5;
                if($empresa==1){
                  $porcen=1;
                }
              }
              if($perso==3){
                if($dt['tb_usuario_id2']==2){
                  $porcen=1;
                }
                else{
                  $porcen=0.5;
                }
              }*/
              $moneda_id = $dt['tb_moneda_id'];
              $porcen = 0.1; //* POR DEFECTO SE LE ASIGANA UN PORCENTAJE DE 0.1 PARA INDICAR QUE AÚN NO TIENE SU FORMULA DE COMISION
              $monto = 0;
              $comi = 0;

              if($moneda_id == 2){
                $monto = ($dt['tb_ventavehiculo_mon']) * floatval($dt['tb_ventavehiculo_tipcam']);
                $montos += ($dt['tb_ventavehiculo_mon']) * floatval($dt['tb_ventavehiculo_tipcam']);                 
              }
              else{
                $monto = ($dt['tb_ventavehiculo_mon']);
                $montos += ($dt['tb_ventavehiculo_mon']);
              }

              $tipo_personal = $perso; // 1 personal tipo: JEFATURA, 2 personal tipo VENTAS, 3 personal tipo JEFE DE SEDE
              $creditotipo_id = 3; // 1 C-MENOR, 2 C-ASIVEH, 3 C-GARVEH, 4 C-HIPO, 5 ADENDAS
              $credito_asiveh = 0; // si el crédito es de tipo AIVEH, debe indicarse si es REVENTA O COMPRA VENTA
              $evento = 2; // 1 poner credito, 2 vender la garantia, 3 sobrecosto
              $estado_comision = 1; //comision estado en vigente 1, 0 eliminado
              //$monto = $dt['tb_ventavehiculo_mon'];
              $comision_mes = $mes; // FALTA LA ESTRUCTURA QUE LAS COMISIONES SEAN POR MES Y AÑO
              
              $result = $oComision->mostrar_comision_estado_monto($tipo_personal, $creditotipo_id, $credito_asiveh, $evento, $estado_comision, $monto, $comision_mes,$empresa_id);
              //$comision_extra .= $result['mensaje'];
                if($result['estado'] == 1){
                  $porcen = $result['data']['tb_comisiondetalle_porcen'];
                }
              $result = NULL;

              $comi = (($monto * $porcen) / 100);
              $comiVenta += $comi;
              $comision_extra .= $porcen.'%/';
            }

            $total_comiVenta += $comiVenta;

            echo '
                  <tr>
                      <td id="tabla_fila" style="font-family:cambria;font-weight: bold">VENTA CONTADO GARVEH</td>
                      <td id="tabla_fila" align="center">' . $rowsVentaVeh.'</td>
                      <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($montos) . '</td>
                      <td id="tabla_fila" align="center">' . $comision_extra.'</td>
                      <td id="tabla_fila" align="center"><span>S/. ' . mostrar_moneda($comiVenta) . '</span></td>
                      <td  id="tabla_fila" align="center" width="5"></td>
                  </tr>';
          }
        $dtsVentaVeh = NULL;

        //? LISTA DE VENTAS DE INMUEBLES
        $dtsIn = $oPlanilla->comision_ventainmueble($mes, $anio, $usu_id,$empresa); //venta contado creditos HIPO
        $rowsIn = count($dtsIn['data']);
        //WHILE PARA VENTA CONTADO GARVEH
        if ($dtsIn['estado']==1) {

            $monto = 0;

            foreach ($dtsIn['data']as $key=>$dt) {
                //$moneda_id = $dt['tb_moneda_id'];
                $monto += ($dt['tb_stockinmueble_pag']);
            }

            $cretip = 4; //el creditotipo_id de HIPO es 4
            $cre_asveh = 0; //para venta al contado no hay venta nueva ni reventa en GARVEH
            $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
            $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes,$empresa_id);
            if ($dtsComi['estado']==1) {
                $comiVenta = 0;
                $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                $comi = (($monto * $porcen) / 100);
                $comiVenta = $comi;
                $total_comiVenta += $comiVenta;
            } else {
                $porcen = 'Inmueble';
            }
            echo '
                  <tr>
                    <td id="tabla_fila" style="font-family:cambria;font-weight: bold">VENTA DE INMUEBLES / '.$mes.' / '.$anio.' / '.$usu_id.' / '.$empresa.'</td>
                    <td id="tabla_fila" align="center">' . $rowsIn . '</td>
                    <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($monto) . '</td>
                    <td id="tabla_fila" align="center">' . $porcen . ' %</td>
                    <td id="tabla_fila" align="center"><span>S/. ' . mostrar_moneda($comiVenta) . '</span></td>
                    <td id="tabla_fila" align="center" width="5"></td>
                  </tr>';
        }
        //FIN VENTAS INMUEBLES

        $usu_id = $_POST['usu_id'];
      ?>
      <tr>
        <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="4">TOTAL COMISIONES:</td>
        <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($total_comiVenta) ?></td>
        <td id="tabla_fila" align="center" style="height: 25px"><?php echo '<a class="btn_ver_det btn btn-success btn-xs" onclick="planilla_detalle_creditos(4, ' . $usu_id . ', ' . $perso . ')" title="ver"><i class="fa fa-eye"></i></a>'; ?></td>
      </tr>
    </table>
  </div>
</div>
