<?php
require_once('../../core/usuario_sesion.php');

require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../tareainventario/Tareainventario.class.php');
$oTarea = new TareaInventario();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$hoy = date('d-m-Y');
$fec_pago = $_POST['txt_cuopag_fec'];


$numope = trim($_POST['txt_cuopag_numope']);
$monto_validar = moneda_mysql($_POST['txt_cuopag_mon']); //elimina las comas y devuelve con 2 decimales
$moneda_deposito_id = 1; //1 soles, 2 dolares
$moneda_credito_id = 1; //1 soles, 2 dolares
$tipocambio_dia = floatval($_POST['txt_cuopag_tipcam']);

$deposito_mon = 0;
$dts = $oDeposito->validar_num_operacion($numope);
    if($dts['estado']==1){
        foreach ($dts['data'] as $key=>$dt){
		$deposito_mon += floatval($dt['tb_deposito_mon']);
	}
    }   

if($deposito_mon == 0){
	$data['cuopag_id'] = 0;
	$data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
	echo json_encode($data);
	exit();
}

$ingreso_importe = 0;
$dts = $oIngreso->lista_ingresos_num_operacion($numope);
    if($dts['estado']==1){
	foreach ($dts['data'] as $key=>$dt) {
		$tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
		$ingreso_mon_id = intval($dt['tb_moneda_id']);
		if($ingreso_mon_id == 2) //si el ingreso se hizo en dólares
                    $tb_ingreso_imp = moneda_mysql($tb_ingreso_imp * $tipocambio_dia);
                    $ingreso_importe += $tb_ingreso_imp;
	}
    }    


$monto_usar = abs($deposito_mon) - $ingreso_importe;
$monto_usar = moneda_mysql($monto_usar);
//echo 'tooo esto hay een importes: '.$ingreso_importe.' // depo: '.$deposito_mon.' // validar: '.$monto_validar.' // usar: '.$monto_usar.' // moneda ing: '.$ingreso_mon_id.' // tipo cambio: '.$tipocambio_dia; exit();

if($monto_validar > $monto_usar){
	$data['cuopag_id'] = 0;
	$data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: '.mostrar_moneda($monto_validar).', monto disponible: '.mostrar_moneda($monto_usar).', TOTAL DEL DEPOSITO: '.mostrar_moneda($deposito_mon).' | validar: '.$monto_validar.' | usar: '.$monto_usar.' | ingre: '.$ingreso_importe.' | cambio: '.$tipocambio_dia.' / mond ing: '.$ingreso_mon_id ;
	echo json_encode($data);
	exit();
}

$cre_tip = 0;


$moneda_depo = ' ? ';
if($_POST['cmb_mon_id'] != ''){
	if($_POST['cmb_mon_id'] == 1)
		$moneda_depo = 'S/.';
	else
		$moneda_depo = 'US$';
}

$moneda_cred = ' ? ';
if($_POST['hdd_mon_id'] != ''){
	if($_POST['hdd_mon_id'] == 1)
		$moneda_cred = 'S/.';
	else
		$moneda_cred = 'US$';
}


/* PAGO DE CRÉDITO MENOR EN CUOTAS LIBRES*/

	if(!empty($_POST['txt_cuopag_mon']))
	{
            $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
            $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
            $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

            $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: '.trim($_SESSION['usuario_nombre']).', en la fecha de: '.date('d-m-Y h:i a').', desde la computadora con nombre: '.$nombre_pc.' y MAC: '.$mac_pc.' ||&& ';

            $pedidos = '';

		//registro de ingreso
                $cue_id=$_POST['cmb_cue_id']; //CANCEL CUOTAS
                $subcue_id=$_POST['cmb_subcue_id']; //CUOTAS menor
                $mod_id=0; //cuota pago
                $modide=0;
                $ing_det = 'INGRESO DE CAJA EN BANCO = Pago abonado en el banco, en la siguente cuenta: [ '.$_POST['hdd_cuenta_dep'].' ], la fecha de depósito fue: [ '.$_POST['txt_cuopag_fecdep'].' ], la fecha de validación fue: [ '.$_POST['txt_cuopag_fec'].' ], el N° de operación fue: [ '.$_POST['txt_cuopag_numope'].' ], el monto depositado fue: [ '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].' ]';
                if(!empty($_POST['txt_cuopag_obs']))
                        $ing_det .='. Obs: '.$_POST['txt_cuopag_obs'];
//			$numdoc=$_POST['hdd_mod_id'].'-'.$_POST['hdd_modid'].'-'.$mod_id.'-'.$cuopag_id;

                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha; 
                $oIngreso->documento_id = 8; 
                $oIngreso->ingreso_numdoc = '';
                $oIngreso->ingreso_det = $ing_det; 
                $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
                $oIngreso->cuenta_id = $cue_id;
                $oIngreso->subcuenta_id = $subcue_id; 
                $oIngreso->cliente_id = intval($_POST['hdd_cli_id']); 
                $oIngreso->caja_id = 1; 
                $oIngreso->moneda_id = intval($_POST['cmb_mon_id']);
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = $mod_id; 
                $oIngreso->ingreso_modide = $modide; 
                $oIngreso->empresa_id = $_SESSION['empresa_id'];

                $result=$oIngreso->insertar();
                    if(intval($result['estado']) == 1){
                        $ingr_id = $result['ingreso_id'];
                    }
                $oIngreso->ingreso_fecdep =fecha_mysql($_POST['txt_cuopag_fecdep']);
                $oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
                $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
                $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
                $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
                $oIngreso->banco_id = $_POST['cmb_banco_id'];
                $oIngreso->ingreso_id=$ingr_id;

                $oIngreso->registrar_datos_deposito_banco();

                $cuopag_his .= 'En caja se generó un ingreso con el siguiente detalle: '.$ing_det.' ||&& ';


		//generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
		
                $subcue_id = 72;//72 es la subcuenta de cuenta en dolares
		if($_POST['hdd_mon_iddep'] == 1)
			$subcue_id = 71; // 71 es cuenta en soles

		$xac = 1;
                $mod_id=0;
		$cue_id=2;
		$subcue_id=9;
		$pro_id = 1;//sistema
		$modide = 0; //el modide de egreso guardará el id del CUOTAPAGO para poder anularlo mediante este id
		$egr_det ='EGRESO FICTICIO POR [ INGRESO ] DE obs : [ '.$_POST['txt_cuopag_obs'].' ]. Pago abonado en el banco, en la siguente cuenta: [ '.$_POST['hdd_cuenta_dep'].' ], la fecha de depósito fue: [ '.$_POST['txt_cuopag_fecdep'].' ], la fecha de validación fue: [ '.$_POST['txt_cuopag_fec'].' ], el N° de operación fue: [ '.$_POST['txt_cuopag_numope'].' ], el monto depositado fue: [ '.$moneda_depo.' '.$_POST['txt_cuopag_mon'].' ]';
		$caj_id = 1; //caja id 14 ya que es CAJA AP
//		$numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-';

                $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
                $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
                $oEgreso->egreso_fec = $fecha;
                $oEgreso->documento_id = 9;//otros egresos
                $oEgreso->egreso_numdoc = '';
                $oEgreso->egreso_det = $egr_det;
                $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
                $oEgreso->egreso_tipcam = 0;
                $oEgreso->egreso_est = 1;
                $oEgreso->cuenta_id = $cue_id;
                $oEgreso->subcuenta_id = $subcue_id;
                $oEgreso->proveedor_id = $pro_id;
                $oEgreso->cliente_id = 0;
                $oEgreso->usuario_id = 0;
                $oEgreso->caja_id = $caj_id;
                $oEgreso->moneda_id = intval($_POST['cmb_mon_id']);
                $oEgreso->modulo_id = $mod_id;
                $oEgreso->egreso_modide = $modide;
                $oEgreso->empresa_id = $_SESSION['empresa_id'];
                $oEgreso->egreso_ap = 0; //si
                $oEgreso->insertar();

		$cuopag_his .= 'Debido a que el pago fue con DEPÓSITO EN BANCO, en caja se generó un egreso con el siguiente detalle: '.$egr_det.' ||&& ';
		$oCuotapago->registro_historial($cuopag_id, $cuopag_his); //registramos toda la información obtenida del pago

                $data['cuopag_id']=1;
                $data['cuopag_msj']='Se registró el pago correctamente.'.$pedidos;

	}
	else
	{
                $data['cuopag_id']=0;
		$data['cuopag_msj']='Intentelo nuevamente.';
	}
	echo json_encode($data);
?>
