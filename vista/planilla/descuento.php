<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">DESCUENTOS DEL MES</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table style="width: 100%;font-family: cambria;" class="table-hover">
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">FECHA DESCUENTO</th>
                <th id="tabla_cabecera_fila">DETALLE</th>
                <th id="tabla_cabecera_fila">MONTO</th>
            </tr>
            <?php
            $total_descuentos = 0;
            $result8 = $oPlanilla->total_descuentos($mes, $anio, $usu_id);
            
            if ($result8['estado'] ==1 ) {
                foreach ($result8['data'] as $key=>$dtDes) {
                    $total_descuentos += mostrar_moneda($dtDes['tb_descuento_mon']);
                    echo '
                      <tr>
                        <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . mostrar_fecha($dtDes['tb_descuento_fec']) . '</td>
                        <td id="tabla_fila" align="center">' . $dtDes['tb_descuento_det'] . '</td>
                        <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($dtDes['tb_descuento_mon']) . '</td>
                      </tr>
                    ';
                }
            }
            ?>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="2"><b>TOTAL DESCUENTOS DEL MES</b></td>
                <td id="tabla_fila" align="center"><strong style="color: red;">- S/. <?php echo mostrar_moneda($total_descuentos); ?></strong></td>
            </tr>
        </table>
    </div>
</div>
