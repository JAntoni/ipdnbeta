
<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">ASISTENCIA VEHICULAR</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table style="width: 100%;height:20px;">
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">TIPO</th>
                <th id="tabla_cabecera_fila">PRECIO ACORDADO</th>
                <th id="tabla_cabecera_fila">TIPO CAMBIO</th>
                <th id="tabla_cabecera_fila">PORCENTAJE</th>
                <th id="tabla_cabecera_fila">COMISIÓN</th>
            </tr>
            <?php
            if(empty($asveh[0])){
                $perso = $tip_per; //según tipo de personal del usuario
                $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
                $est = 1; // el estado debe ser vigente 1, 0 no vigente
                $total_comiAsveh = 0;
                $MONTO_COLOCADO_ASIVEH = 0;

                if(!empty($dtsAsveh['data'])){
                    foreach ($dtsAsveh['data'] as $key => $dt) {
                        $tipo = '----';
                        $subTip = '';
                        $moneda = 'S/.';
                        if ($dt['tb_moneda_id'] == 2)
                            $moneda = 'US$';
                        if ($dt['tb_credito_tip1'] == 1)
                            $tipo = "VENTA NUEVA ";
                        if ($dt['tb_credito_tip1'] == 2)
                            $tipo = "ADENDA ";
                        if ($dt['tb_credito_tip1'] == 4)
                            $tipo = "RE-VENTA ";

                        if ($dt['tb_credito_tip2'] == 1)
                            $subTip = "CREDITO REGULAR";
                        if ($dt['tb_credito_tip2'] == 4)
                            $subTip = "CUOTA BALON";

                        if ($dt['tb_credito_tip1'] == 1 || $dt['tb_credito_tip1'] == 4) {
                            $cretip = 2; //credito asiveh es de tipo 2
                            $cre_asveh = $dt['tb_credito_tip1']; //obtenemos el porcentaje para venta nueva o reventa
                        }
                        if ($dt['tb_credito_tip1'] == 2) {
                            $cretip = 5; //para la formula de comision las adendas son de creditotipo_id = 5
                            $cre_asveh = 0; //ya que es adenda no tiene tipo de asveh, valor 0
                        }



                        $monto = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];
                        $MONTO_COLOCADO_ASIVEH += $monto;

                        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes,$empresa_id);
                        if ($dtsComi['estado'] == 1) {
                            $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                        }

                        $comi = moneda_mysql(($monto * $porcen) / 100);
                        $total_comiAsveh += $comi;
                        echo '
                                            <tr>
                                            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . $tipo . $subTip . ' (CAV-' . $dt['tb_credito_id'] . ')</td>
                                            <td id="tabla_fila" align="center">' . $moneda . ' ' . mostrar_moneda($dt['tb_credito_preaco']) . '</td>
                                            <td id="tabla_fila" align="center">' . $dt['tb_credito_tipcam'] . '</td>
                                            <td id="tabla_fila" align="center">' . $porcen . ' %</td>
                                            <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($comi) . '</td>
                                            </tr>';
                    }
                }
            
            }else{

                $total_comiAsveh = 0.00;
                foreach ($asiveh as $key => $value) {
                    echo '
                        <tr>
                            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . $value->tipo . '</td>
                            <td id="tabla_fila" align="center">' . $value->moneda . ' ' . $value->precio_acordado . '</td>
                            <td id="tabla_fila" align="center">' . $value->tipo_cambio . '</td>
                            <td id="tabla_fila" align="center">' . $value->porcentaje . ' </td>
                            <td id="tabla_fila" align="center">S/. ' . floatval($value->comision) . '</td>
                        </tr>';

                    $total_comiAsveh = $total_comiAsveh + floatval($value->comision);
                    
                }
                
            }
            ?>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="4">TOTAL COMISIONES:</td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($total_comiAsveh) ?></td>
            </tr>
        </table>
    </div>
</div>
