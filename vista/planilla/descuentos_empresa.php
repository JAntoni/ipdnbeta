

<!--EN LA EMPRESA-->
<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">EN LA EMPRESA</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table class="table-hover"  style="font-family: cambria;font-size: 12px;width: 100%">
            <tr id="tabla_cabecera" style="height:20px;">
                <th id="tabla_cabecera_fila" width="40%">TIPO DESCUENTO</th>
                <th id="tabla_cabecera_fila" width="30%">TOTAL</th>
                <th id="tabla_cabecera_fila" width="20%">MONTO</th>
                <th id="tabla_cabecera_fila" width="10%"></th>
            </tr>
            <tr style="height:20px;">
                <?php
                $dtsFal = $oAsistencia->numero_faltas($usu_id, $mes_anio);

                if ($dtsFal['estado'] == 1) {
                    $num_faltas = intval($dtsFal['data']['dias_falta']);
                }
                //determinar horas faltadas, si es full time entonce son 8 horas al día faltados, si con part son 4 horas
                $horas_diarias = 0;
                if ($usu_tur == 1)
                    $horas_diarias = 8;
                else
                    $horas_diarias = 4;
                $horas_faltadas = $num_faltas * $horas_diarias;
                $penalidad_falta = 0;
                if($usu_horas > 0)
                    $penalidad_falta = floatval(($usu_suel * $horas_faltadas) / $usu_horas);
                ?>
                <td id="tabla_fila" style="font-weight: bold">FALTAS <?php echo ' / '.$usu_id.' // '.$mes_anio;?></td>
                <td id="tabla_fila" style="text-align: center"><?php echo $horas_faltadas . ' hor (' . $num_faltas . ' dias)'; ?></td>
                <td id="tabla_fila" style="text-align: right"><?php echo 'S/. ' . number_format($penalidad_falta, 2); ?></td>
                <td id="tabla_fila" style="text-align: center">

                </td>
            </tr>
            <tr style="height:25px;">
                <td id="tabla_fila" style="font-weight: bold">TARDANZAS</td>
                <?php
                /* // tiempo total de tardanza
                $segundos = $suma_tardanza%60%60%60;
                $minutos = floor($suma_tardanza / 60);
                //$horas = floor($suma_tardanza / 3600);
                $hora_tarde = str_pad($minutos, 2, "0", STR_PAD_LEFT).' min '.str_pad($segundos, 2, "0", STR_PAD_LEFT).' seg';
                
                // tiempo penalizado
                $segundos_p = $suma_tardanza_pena%60%60%60;
                $minutos_p = floor($suma_tardanza_pena / 60);
                $horas_p = floor($suma_tardanza_pena / 3600);
                $hora_tarde_pena = str_pad($minutos_p, 2, "0", STR_PAD_LEFT).' min '.str_pad($segundos_p, 2, "0", STR_PAD_LEFT).' seg'; */
                ?>
                <!-- <td id="tabla_fila" style="text-align: center"><?php echo $minutos_tardanza . ' min / ' . $minutos_penalidad . ' min'; ?></td> -->
                <td id="tabla_fila" style="text-align: center"><?php echo $minutes_total . ' min / ' . $minutes_danger. ' min'; ?></td>
                <td id="tabla_fila" style="text-align: right"><?php echo 'S/. ' . number_format($penalidad_tar, 2); ?></td>
                <td id="tabla_fila" style="text-align: center">
                    <?php
                    //if ($minutos_tardanza > 0)
                    if ($minutes_total > 0)
//                                        echo '<a href="javascript:void(0)" onclick="asistencia_tardanzas('.$usu_tur.')">ver</a>';
                        echo '<a class="btn btn-facebook btn-xs" href="javascript:void(0)" onclick="asistencia_tardanzas('.$usu_id.','.$usu_tur.')"><i class="fa fa-eye"></i></a>';
                    ?>
                </td>
            </tr>
            <?php
//            $dts = $oVentainterna->listar_ventainterna($usu_id, $mes, $anio);
//            $rows_ven = mysql_num_rows($dts);

//            if ($dts['estado'] == 1) {
//                foreach ($dts['data']as $key => $dt) {
//                    $pagar = '<span style="color: green;">Pagado (<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="compra_colaborador(\'anularpago\',' . $dt['tb_ventainterna_id'] . ')">Anular</a>)</span>';
//                    if ($dt['tb_ventainterna_est'] == 0)
//                        $pagar = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="pagar_ventainterna(' . $dt['tb_ventainterna_id'] . ')" title="Pagar"><i class="fa fa-money"></i></a>';
//
//                    $dtsGar = $oGarantia->mostrarUno($dt['tb_garantia_id']);
//                    if ($dtsGar['estado'] == 1) {
//                        $garantia_cre_id = $dtsGar['data']['tb_credito_id'];
//                    }
//
//                    echo '
//                        <tr style="height:30px;">  
//                          <td id="tabla_fila" style="font-weight: bold">COMPRA: ' . $dt['tb_garantia_pro'] . '. Crédito CM-' . str_pad($garantia_cre_id, 4, "0", STR_PAD_LEFT) . '</td>
//                          <td id="tabla_fila" style="text-align: center">1 com</td>
//                          <td id="tabla_fila" style="text-align: right">S/. ' . mostrar_moneda($dt['tb_ventainterna_mon']) . '</td>
//                          <td id="tabla_fila" style="text-align: center">' . $pagar . '</td>
//                        </tr>';
//                }
//            } else {
//                echo '
//                        <tr style="height:20px;">  
//                          <td id="tabla_fila" style="font-weight: bold">COMPRAS</td>
//                          <td id="tabla_fila" style="text-align: center">0 COM</td>
//                          <td id="tabla_fila" style="text-align: right">S/. 0</td>
//                          <td id="tabla_fila" style="text-align: center"></td>
//                        </tr>';
//            }
            ?>
        </table>
    </div>
</div> 
<!--FIN EN LA EMPRESA-->
<br>
<?php
$resta = $total_sueldo_base - ($penalidad_tar + $penalidad_falta + $retencion);
if ($resta <= 0)
    $resta = 0;
$sueldo_bruto = $resta;
?>
