<?php
require_once('../../core/usuario_sesion.php');
require_once('../planilla/Planilla.class.php');
$oPlanilla = new Planilla();
require_once('../formulacomision/Formulacomision.class.php');
$oComision = new Formulacomision();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../aportaciones/Afps.class.php');
$oAfps = new Afps();
require_once('../asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();
require_once('../menorgarantia/Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
//$oVentainterna = new cVentainterna();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../bono/Bono.class.php');
$oBono = new Bono();
require_once('../recaudo/Recaudo.class.php');
$oRecaudo = new Recaudo();
require_once('../recaudo/Mejoragc.class.php');
$oMejoragc = new Mejoragc();
require_once ('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once ('../vehiculomarca/Vehiculomarca.class.php');
$oVehMarc = new Vehiculomarca();
require_once ('../vehiculomodelo/Vehiculomodelo.class.php');
$oVehMod = new Vehiculomodelo();
require_once ('../vehiculoclase/Vehiculoclase.class.php');
$oVehCla = new Vehiculoclase();
require_once ('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usu_id = $_POST['usu_id'];
$usu_id2 = $_POST['usu_id'];
$usuarioVendedor_id = intval($_POST['usu_id']);
$mes = $_POST['mes'];
$anio = $_POST['anio'];

$sueldo_bruto = 0;
$sueldo_comision = 0;

if (!empty($usu_id) && !empty($mes) && !empty($anio)) {

    $dts = $oUsuario->mostrarUno($usu_id);
    // Gerson (14-03-23)
    $fecha_fin='';
    $month = $anio.'-'.$mes;
    $aux = date('Y-m-d', strtotime("{$month} + 1 month"));
    $fecha_fin = date('Y-m-d', strtotime("{$aux} - 1 day"));
    $fecha_inicio = $anio.'-'.$mes.'-01';
    $detalle_usuario = $oUsuario->mostrarSueldoUsuario($usu_id, $fecha_inicio, $fecha_fin);
    //$detalle_usuario = $oUsuario->mostrarUltimoSueldoNew($usu_id);
    //
    $contrato_encontrado = 0;
    if ($dts['estado'] == 1) {
        //$tip_per = 0;
        //$tip_per = $dts['data']['tb_usuario_per']; //tipo de personal al que pertence 1 jefatura, 2 ventas
        $usu_nom = $dts['data']['tb_usuario_nom'] . ' ' . $dts['data']['tb_usuario_ape'];
        //$usu_suel = $dts['data']['tb_usuario_suel']; //sueldo base del colaborador
        // Gerson (14-03-23)
        if($detalle_usuario['estado'] == 1){
            $usu_suel = $detalle_usuario['data']['tb_usuario_detalle_suel']; //sueldo base del colaborador NUEVO MODO RESPETANDO HISTORIAL
            $tip_per = $detalle_usuario['data']['tb_usuario_per'];
            $contrato_encontrado = 1;
        }else{
            $detalle_usuario2 = $oUsuario->mostrarSueldoUsuarioActual($usu_id, $fecha_inicio);
            if($detalle_usuario2['estado'] == 1){
                $usu_suel = $detalle_usuario2['data']['tb_usuario_detalle_suel']; //sueldo base del colaborador NUEVO MODO RESPETANDO HISTORIAL
                $tip_per = $detalle_usuario2['data']['tb_usuario_per'];
                $contrato_encontrado = 1;
            }else{
                //$usu_suel = $dts['data']['tb_usuario_suel']; //sueldo base del colaborador
                /* GERSON (27-05-23) */
                // Si no se tiene el detalle del sueldo del colaborador es porque no era trabajador
                $usu_suel = 0.00; //sueldo base del colaborador
                /*  */
            }
        }
        //
        $usu_horas = $dts['data']['tb_usuario_hor'];
        $usu_afp = intval($dts['data']['tb_usuario_afp']); //id del afp al que pertenece el usuario
        //$usu_onp = $dts['data']['tb_usuario_onp']; //id del onp al que pertenece el usuario
        $usu_eps = intval($dts['data']['tb_usuario_eps']); //id de eps al que pertence el usuario
        $usu_tur = $dts['data']['tb_usuario_tur']; //tipo de turno del colaborador: 1 full time, 2 part time mañana, 3 part time tarde
        $usu_ing1 = $dts['data']['tb_usuario_ing1']; //hora que debe ingresar el colaborador
        $usu_sal1 = $dts['data']['tb_usuario_sal1']; //hora que debe salir el colaborador
        $usu_ing2 = $dts['data']['tb_usuario_ing2']; //hora que debe ingresar el colaborador
        $usu_sal2 = $dts['data']['tb_usuario_sal2']; //hora que debe salir el colaborador
        $empresa_id = $dts['data']['tb_empresa_id']; //sede a la que pertenece el Usuario
        $asignacion = $dts['data']['tb_usuario_asig_fam']; // asignacion familiar
    }
    $dts = NULL;

    if ($tip_per == 1){//jefatura
        $usu_id = '';
        $empresa=0;
    }
    if($tip_per == 2){//ventas
        $empresa=0;
    }
    if($tip_per == 3){//administrador de sede
        $usu_id = '';
        $empresa=$empresa_id;
    }
    
//    echo 'mes  = '.$mes.' // AÑO'.$anio.'  // Usuario '.$usu_id.'   //  empresa = '.$empresa .' // tipo usuario = '.$tip_per ;exit();

    //$dtsAsveh = $oPlanilla->comision_asiveh($mes, $anio, $usu_id,$empresa);
    $dtsAsveh = $oPlanilla->comision_asiveh_new($mes, $anio, $usu_id,$empresa); //Gerson 28-03-23
    //$dtsGarveh = $oPlanilla->comision_garveh($mes, $anio, $usu_id,$empresa);
    echo 'asas /// mes: '.$mes.' //anio '.$anio.' /// usu: '.$usu_id.' // empresa: '.$empresa;
    $dtsGarveh = $oPlanilla->comision_garveh_new($mes, $anio, $usu_id,$empresa); //Gerson 28-03-23
    //$dtsHipo = $oPlanilla->comision_hipo($mes, $anio, $usu_id,$empresa);
    $dtsHipo = $oPlanilla->comision_hipo_new($mes, $anio, $usu_id,$empresa); //Gerson 28-03-23
    $dtsMenor = $oPlanilla->comision_menor($mes, $anio, $usu_id,$empresa);
//    echo '$mes=='.$mes.'    $anio=='.$anio.'    $usu_id=='.$usu_id.'    $empresa=='.$empresa;
    $dtsVenta = $oPlanilla->comision_ventagarantia($mes, $anio, $usu_id,$empresa);

    // GERSON (29-12-23)
        $asveh = [];
        $garveh = [];
        $hipo = [];
        $cmenor = [];
        $comision_usu = $oUsuario->obtener_comision_asesor($mes, $anio, $usu_id);
        $comisiones = "";
        if($comision_usu['estado'] == 1){
            $comisiones = json_decode($comision_usu['data']['tb_comisionasesor_valor']);
        
            $asveh = $comisiones->asveh;
            $garveh = $comisiones->garveh;
            $hipo = $comisiones->hipo;
            $cmenor = $comisiones->cmenor;
    
            //var_dump($cmenor);

            /* foreach ($asiveh as $key => $value) {
                var_dump($value->tipo);
                echo '<br>';
            }
    
            foreach ($garveh as $key => $value) {
                var_dump($value->tipo);
                echo '<br>';
            } */

        }
        
    
    $usu_id = $_POST['usu_id'];
    $mes_anio = $mes . '-' . $anio;

    $arr_afp = null;
    $dts = $oAfps->aportacion_usuario_tipo_seguro($usu_afp, $mes_anio); //parametro vacio para que liste todos los tipos

    if ($dts['estado'] == 1) {
        $aporte_afp = $dts['data']['tb_afps_apor']; //aporte en soles o en % que se aplicará al sueldo para aportar
        $prima_afp = $dts['data']['tb_afps_apor2']; //aporte en soles o en % que se aplicará al sueldo para aportar
        $comision_afp = $dts['data']['tb_afps_apor3']; //aporte en soles o en % que se aplicará al sueldo para aportar
        $comision_mixta_afp = $dts['data']['tb_afps_apor4'];
        $seguro_afp_nom = $dts['data']['tb_seguro_nom'];
        /* while ($dt = mysql_fetch_array($dts)){
          $arr_afp[$dts['data']['tb_afps_id']]['tipo'] = $dts['data']['tb_afps_tip']; //tipo de entidad para aportes: 1 afp, 2 eps, 3 UIT
          $arr_afp[$dts['data']['tb_afps_id']]['nombre'] = $dts['data']['tb_afps_nom']; //nombre de la entidad
          $arr_afp[$dts['data']['tb_afps_id']]['aporte'] = $dts['data']['tb_afps_apor']; //aporte en soles o en % que se aplicará al sueldo para aportar
          $arr_afp[$dts['data']['tb_afps_id']]['prima'] = $dts['data']['tb_afps_apor2']; //aporte en soles o en % que se aplicará al sueldo para aportar
          $arr_afp[$dts['data']['tb_afps_id']]['comision'] = $dts['data']['tb_afps_apor3']; //aporte en soles o en % que se aplicará al sueldo para aportar
          $arr_afp[$dts['data']['tb_afps_id']]['mixta'] = $dts['data']['tb_afps_apor4']; //aporte en soles o en % que se aplicará al sueldo para aportar
          } */
    }
    $dts = NULL;

    $dts = $oAfps->aportacion_usuario_tipo_seguro($usu_eps, $mes_anio); //parametro vacio para que liste todos los tipos
    if ($dts['estado'] == 1) {
        $aporte_eps = $dts['data']['tb_afps_apor']; //aporte en soles o en % que se aplicará al sueldo para aportar
        $seguro_eps_nom = $dts['data']['tb_seguro_nom'];
    }
    $dts = NULL;

    //verificamos si el usuario tiene bonos
    $fecha_bono = $anio . '-' . $mes . '-' . '01';
    $bono_val = 0;
    $listBono = [];
    $dtsBo = $oBono->mostrar_lista_por_fecha_periodo($fecha_bono, $usu_id);
    //$dtsBo = $oBono->mostrar_por_fecha_periodo($fecha_bono, $usu_id);
    if ($dtsBo['estado'] == 1) {
        //$bono_tip = intval($dtsBo['data']['tb_bono_tip']); //1 % aplicado al sueldo, 2 monto fijo
        //$bono_nom = $dtsBo['data']['tb_bono_nom'];
        //$bono_val = floatval($dtsBo['data']['tb_bono_val']);
        foreach ($dtsBo['data'] as $key => $dt) {
            $valor['bono_tip'] = intval($dt['tb_bono_tip']);
            $valor['bono_nom'] = $dt['tb_bono_nom'];
            $valor['bono_val'] = floatval($dt['tb_bono_val']);

            $listBono[$key] = $valor;
        }
    }
    
    $dtsBo = NULL;

    //CONSULTAMOS LOS MONTOS PARA PAGAR DE COMISIONES POR GC

    $fec_rec = new DateTime('01-' . $mes . '-' . $anio);
    $fec_rec->modify('last day of this month');
    $dia_ult = $fec_rec->format('d');

    $fecha1 = '01-' . $mes . '-' . $anio;
    $fecha2 = $dia_ult . '-' . $mes . '-' . $anio;

    $anio_mes_metas = $anio.'-'.$mes;
    $fecha_inicio_meta = '01-' . $mes . '-' . $anio; //esto cambiará luego de obtener el inicio real
    $fecha_fin_meta = '01-' . $mes . '-' . $anio; //esto cambiará luego de obtener el fin real
    $comentario_metas = 'AUN NO HAS DEFINIDO LAS FECHAS PARA LAS METAS DE COBRANZA';

    //vamos a obtener la fecha inicio y fecha fin de la meta de RECUADO, puede ser difeernte que la meta de Mejora GC
    $result = $oRecaudo->obtener_fecha_inicio_fin_por_anio_mes($anio_mes_metas);
        if($result['estado'] == 1){
            $fecha_inicio_meta = $result['data']['tb_recaudo_fec1'];
            $fecha_fin_meta = $result['data']['tb_recaudo_fec2'];
            $comentario_metas = 'METAS DEL '.mostrar_fecha($fecha_inicio_meta).' HASTA '.mostrar_fecha($fecha_fin_meta);
        }
    $result = NULL;
    
    //COMISIONES POR RECAUDACIÓN EN CAJA SEMANAL
    $result = $oRecaudo->pagar_mes_usuario($usu_id, $fecha_inicio_meta, $fecha_fin_meta);
        if ($result['estado'] == 1) {
            $pago_recaudo = ($result['data']['pago_recaudo']);
        }
    $result = NULL;

    //COMISIONES POR DISMINUCIÓN DE GC
    $result = $oMejoragc->obtener_fecha_inicio_fin_por_anio_mes($anio_mes_metas);
        if($result['estado'] == 1){
            $fecha_inicio_meta = $result['data']['tb_mejoragc_fec1'];
            $fecha_fin_meta = $result['data']['tb_mejoragc_fec2'];
        }
    $result = NULL;

    $result2 = $oMejoragc->pagar_mes_usuario($usu_id, $fecha_inicio_meta, $fecha_fin_meta);
        if ($result2['estado'] == 1) {
            $pago_mejoragc = ($result2['data']['pago_mejoragc']);
        }
        } 
        else {
            echo '<center><h2>SELECCIONE UN COLABORADOR</h2></center>';
            exit();
        }
    $result2 = NULL;

    if($contrato_encontrado==1){
?>
        <center><h3 style="font-family: cambria;font-weight: bold;color: #ff6600"><?php echo 'PLANILLA DEL(A) COLABORADOR(A) ' . $usu_nom . ' DEL MES DE ' . strtoupper(nombre_mes(intval($mes))) . ' DEL ' . $anio. ' | Tipo Personal: '.$tip_per.' ('.$usu_id.')'; ?></h3></center>
        <div class="row">
            <div class="col-md-4">

                
                <?php include_once 'sueldo_base_horas_trabajadas.php';?>

                <br>


                <!--INICIO DEL DESCUENTO-->    
                <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">DESCUENTOS</label>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <!--EN LA EMPRESA-->
                        <?php include_once 'descuentos_empresa.php';?>
                        <!--FIN EN LA EMPRESA-->
                        
                        <!--OTROS DESCUENTOS-->
                        <?php include_once 'descuentos_otros_descuentos.php';?>
                        <!--FIN OTROS DESCUENTOS-->
                    </div>
                </div>        
                <!--FIN DEL DESCUENTO-->
                <br>
                <label style="font-family: cambria"><b>TOTAL DESCUENTOS: S/. <span id="span_descuento"><?php echo number_format($penalidad_tar + $penalidad_falta, 2); ?></span></b></label><br>
                <label style="font-family: cambria"><b>TOTAL OBLIGACIONES: S/. <span id="span_obligaciones"><?php echo number_format($retencion, 2); ?></span></b></label>
                <br>
                <table class="table-hover" style="font-family: cambria;width: 100%">
                    <tr style="height:20px;">
                        <td id="tabla_fila"><strong style="color: green;">EL COLABORADOR RECIBE: S/: </strong><span style="font-weight: bold"><?php echo number_format($resta, 2) ?></span></td>
                    </tr>
                </table>
                <br>

            </div>      

            <!--COMISIONES-->
            <div class="col-md-8">
                <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">COMISIONES</label>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <!--ASISTENCIA VEHICULAR-->
                        <?php include_once 'asistencia_vehicular.php';?>
                        <!--FIN ASISTENCIA VEHICULAR-->
                        <br>
                        <!--GARANTIA VEHICULAR-->
                        <?php include_once 'garantia_vehicular.php';?>
                        <!--FIN GARANTIA VEHICULAR-->
                        <br>
                        <!--HIPOTECARIO-->
                        <?php include_once 'hipotecario.php';?>
                        <!--HIPOTECARIO-->
                        <br>
                        <!--CRÉDITO MENOR-->
                        <?php include_once 'credito_menor.php';?>
                        <!--FIN CRÉDITO MENOR-->
                        <br>
                        <!--VENTA AL CONTADO-->
                        <?php include_once 'venta_contado.php';?>
                        <!--FIN VENTA AL CONTADO-->
                        <br>
                        <!--COMISIONES GESTION DE COBRANZA-->
                        <?php include_once 'comisiones_gestion_cobranza.php';?>
                        <!--FIN COMISIONES GESTION DE COBRANZA-->
                        <br>
                        <!--COMISIONES GESTION DE COBRANZA-->
                        <?php include_once 'reversion_comisiones.php';?>
                        <!--FIN COMISIONES GESTION DE COBRANZA-->
                        <br>
                        <!--DESCUENTO-->
                        <?php include_once 'descuento.php';?>
                        <!--FIN DESCUENTO-->
                        <br>
                        <!--DESCUENTO-->
                        <?php include_once 'total_comisiones.php';?>
                        <!--FIN DESCUENTO-->
                        
                    </div>
                </div>
            </div>
        </div>   
        <p>
        <div class="row">
            <div class="col-md-5">
                <!--DESCUENTO-->
                <?php include_once 'descuentos_obligaciones.php';?>
                <!--FIN DESCUENTO-->
            </div>
            <div class="col-md-7">
                <!--DESCUENTO-->
                <?php include_once 'descuentos_compra_colaborador.php';?>
                <!--FIN DESCUENTO-->
            </div>
        </div>

        <div id="div_planilla_detalle_asistencia"></div>

<?php }else{ ?>
        <center><h3 style="font-family: cambria;font-weight: bold;color: #ff6600"><?php echo 'COLABORADOR(A) ' . $usu_nom . ' NO TRABAJÓ EN EL MES DE ' . strtoupper(nombre_mes(intval($mes))) . ' DEL ' . $anio; ?></h3></center>
<?php } ?>