<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha Depósito :</label>
                    <div class='input-group date' id='pago_fil_picker1'>
                        <input type="text" name="txt_cuopag_fecdep" id="txt_cuopag_fecdep" class="form-control input-sm" readonly value="<?php echo date('d-m-Y'); ?>">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <!-- /.input group -->
                </div>
            </div>

            <div class="col-md-4">
                <label>Fecha Validación:</label>
                <div class="form-group">
                    <!--<div class="col-md-8">-->
                        <div class='input-group date' id='pago_fil_picker2'>
                            <input type="text" name="txt_cuopag_fec" id="txt_cuopag_fec" class="form-control input-sm" readonly value="<?php echo date('d-m-Y'); ?>">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    <!--</div>-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>N° Operación:</label>
                    <!--<div class="col-md-8">-->
                    <input type="text" class="form-control input-sm" id="txt_cuopag_numope" name="txt_cuopag_numope" required="">
                    <!--</div>-->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Monto Depósito:</label>
                    <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon" maxlength="10"  value="<?php echo mostrar_moneda($saldo); ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Comisión Banco:</label>
                    <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_comi" type="text" id="txt_cuopag_comi" maxlength="10" value="0.00">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Monto Validar:</label>
                    <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_monval" type="text" id="txt_cuopag_monval" maxlength="10" readonly   value="<?php echo mostrar_moneda($saldo); ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>N° Cuenta:</label>
                    <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">
                        <?php require_once '../cuentadeposito/cuentadeposito_select.php'; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>En Banco:</label>
                    <select name="cmb_banco_id" class="form-control input-sm">
                        <option value="">--</option>
                        <option value="1">Banco BCP</option>
                        <option value="2">Banco Interbak</option>
                        <option value="3" selected="true">Banco BBVA</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>N° de Cuenta</label>
                    <input style="text-align:right;font-weight: bold;font-size: 15px" class="form-control input-sm" name="txt_num_cuenta" type="text" id="txt_num_cuenta" value="<?php echo $nrocuenta;?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Monto Pagado:</label>
                    <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda4 form-control input-sm" name="txt_cuopag_montot" type="text" id="txt_cuopag_montot"   value="<?php echo mostrar_moneda($saldo); ?>"  maxlength="10" readonly>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label>Observación:</label>
                    <textarea type="text" name="txt_cuopag_obs" rows="2" cols="50" class="form-control input-sm mayus"></textarea>
                </div>
            </div>
        </div>
        <p>

        <div class="row">
            <div id="msj_pagobanco" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px;"></div>
        </div>
    </div>
</div>