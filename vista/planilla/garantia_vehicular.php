
<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">GARANTÍA VEHICULAR</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table style="width: 100%" class="table-hover">
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">TIPO</th>
                <th id="tabla_cabecera_fila">MARCA/MODELO/CLASE</th>
                <th id="tabla_cabecera_fila">ASESOR</th>
                <th id="tabla_cabecera_fila">MONEDA</th>
                <th id="tabla_cabecera_fila">PRECIO ACORDADO</th>
                <th id="tabla_cabecera_fila">INTERÉS CRÉDITO</th>
                <th id="tabla_cabecera_fila">TIPO CAMBIO</th>
                <th id="tabla_cabecera_fila">PORCENTAJE</th>
                <th id="tabla_cabecera_fila">COMISIÓN</th>
            </tr>
            <?php

            var_dump($garveh[0]);
            echo '<br>';
            var_dump(count($garveh));
                $perso = $tip_per; //según tipo de personal del usuario
                $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
                $est = 1; // el estado debe ser vigente 1, 0 no vigente
                $total_comiGarveh = 0;
                $sumaSoles = 0;
                $sumaDolares = 0;
                $MONTO_COLOCADO_GARVEH = 0;

                if(!empty($dtsGarveh['data'])){
                    foreach ($dtsGarveh['data'] as $key => $dt) {

                        if ($dt['tb_moneda_id'] == 1) {
                            $moneda = 'S/.';
                            $sumaSoles += $dt['tb_credito_preaco'];
                        }
                        if ($dt['tb_moneda_id'] == 2) {
                            $moneda = 'US$';
                            $sumaDolares += $dt['tb_credito_preaco'];
                        }

                        $tipo = 'GARANTÍA VEHICULAR';
                        $cretip = 3; //credito garveh es de tipo 3
                        $cre_asveh = 0; //como es garveh no tiene venta nueva o reventa, entonces tiene valor 0

                        if ($dt['tb_credito_tip'] == 2) {
                            $tipo = "ADENDA VEHICULAR";
                            $cretip = 5; //para todas las adendas de los creditos, adenda tiene como creditotipo_id = 5
                        }

                        $valido = '';
                        if (moneda_mysql($dt['tb_credito_valido']) > 0) {
                            if ($dt['tb_moneda_id'] == 2)
                                $monto = $dt['tb_credito_valido'] * $dt['tb_credito_tipcam'];
                            else
                                $monto = $dt['tb_credito_valido'];
                            $valido = '| <b>' . mostrar_moneda($dt['tb_credito_valido']) . '</b>';
                        } 
                        else{
                            if ($dt['tb_moneda_id'] == 2)
                                $monto = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];
                            else
                                $monto = $dt['tb_credito_preaco'];
                        }

                        $MONTO_COLOCADO_GARVEH += $monto;
                        
                        $porcen = 0;
                        $formula_si = '(falta formula tip_perso: '.$perso.' / cre_tip: '.$cretip.' / asveh: '.$cre_asveh.' / event: '.$evento.' / est: '.$est.' / monto: '.$monto.' / mes: '.$mes.' / emp: '.$empresa_id.')';
                        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes, $empresa_id);
                            if ($dtsComi['estado'] == 1) {
                                $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                                $formula_si = '';
                            }
                        $dtsComi = NULL;

                        if ($tip_per == 1) {
                            if ($dt['tb_empresa_id'] == 2) {
                                if ($dt['tb_credito_usureg'] == 2) {
                                    $porcen = mostrar_moneda(0.8);
                                }
                                else{
                                    $porcen = mostrar_moneda(1);
                                }
                            }
                        }
                        if ($tip_per == 3) {
                            if ($usu_id == $dt['tb_credito_usureg']) {
                                $porcen = mostrar_moneda(1.20);
                            } else {
                                $porcen = 0;
                            }
                        }
                        if ($tip_per == 2 && $empresa_id == 2) { //personal ventas que pertenezca a mall, si logra interés del 5% 0.3+
                            if (intval($dt['tb_credito_int']) == 5)
                                $porcen = $porcen + 0.3;
                        }


                        $Cliente = $oCliente->mostrarUno($dt['tb_cliente_id']);
                        if ($Cliente['estado'] == 1) {
                            $nombre = $Cliente['data']['tb_cliente_nom'];
                        }
                        $VehMarc = $oVehMarc->mostrarUno($dt['tb_vehiculomarca_id']);
                        if ($VehMarc['estado'] == 1) {
                            $marca = $VehMarc['data']['tb_vehiculomarca_nom'];
                        }
                        $VehMod = $oVehMod->mostrarUno($dt['tb_vehiculomodelo_id']);
                        if ($VehMod['estado'] == 1) {
                            $modelo = $VehMod['data']['tb_vehiculomodelo_nom'];
                        }
                        $VehCla = $oVehCla->mostrarUno($dt['tb_vehiculoclase_id']);
                        if ($VehCla['estado'] == 1) {
                            $clase = $VehCla['data']['tb_vehiculoclase_nom'];
                        }

                        $oUser = $oUsuario->mostrarUno($dt['tb_credito_usureg']);
                        if ($oUser['estado'] == 1) {
                            $asesor = $oUser['data']['tb_usuario_nom'];
                        }

                        $comi = (($monto * $porcen) / 100);
                        $total_comiGarveh += $comi;

                        echo '
                            <tr>
                            <td id="tabla_fila" style="font-family:cambria;"><b onclick="creditogarveh_form(\'L\',' . $dt['tb_credito_id'] . ') ">' . $tipo . ' (CGV-' . $dt['tb_credito_id'] . ') - [' . $nombre . ']</b></td>
                            <td id="tabla_fila" align="center" style="font-family:cambria;">' . $marca . ' / ' . $modelo . ' / ' . $clase . '</td>
                            <td id="tabla_fila" align="center" style="font-family:cambria;">' . $asesor . '</td>
                            <td id="tabla_fila" align="center" style="font-family:cambria;">' . $moneda . '</td>
                            <td id="tabla_fila" align="center"><b>' .$dt['tb_credito_preaco'] . ' ' . $valido . '</b></td>
                            <td id="tabla_fila" align="center"><b>' . $dt['tb_credito_int'] . '</b></td>
                            <td id="tabla_fila" align="center"><b>' . $dt['tb_credito_tipcam'] . '</b></td>
                            <td id="tabla_fila" align="center"><b>' . $porcen . ' %'.$formula_si.'</td>
                            <td id="tabla_fila" align="center"><b>' . formato_numero($comi) . '</b></td>
                            </tr>';
                    }
                }

            ?>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="8"> &nbsp;</td>
            </tr>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;" colspan="8"><b>SOLES : S/.</b> <?php echo mostrar_moneda($sumaSoles) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       <b>DÓLARES : $/.</b> <?php echo mostrar_moneda($sumaDolares) ?></td>

            </tr>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="7">TOTAL COMISIONES:</td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($total_comiGarveh) ?></td>
            </tr>
        </table>
    </div>
</div>
