/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    console.log('cammmbiii 99')
    var empresa_id = $("#cmb_empresa_id").val();
    cmb_usu_id(empresa_id);

});

$("#cmb_empresa_id").change(function () {
    var empresa_id = $("#cmb_empresa_id").val();
    cmb_usu_id(empresa_id);
});

function asistencia_tardanzas(usuario_id, usuario_turno) {

    $.ajax({
        type: "POST",
        url: VISTA_URL + "planilla/planilla_detalle_asistencia.php",
        async: true,
        dataType: "html",
        data: ({
            usuario_id: usuario_id,
            mes: $('#cmb_pla_mes').val(),
            anio: $('#cmb_pla_anio').val(),
            usuario_turno: usuario_turno
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_planilla_detalle_asistencia').html(html);
            $('#modal_detalle_asistencia').modal('show');
            modal_hidden_bs_modal('modal_detalle_asistencia', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            if (data.statusText != "success") {
                console.log(data);
            }
        }
    });
}

function creditogarveh_form(usuario_act, creditogarveh_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/creditogarveh_form.php",
        async: true,
        dataType: "html",
        data: ({
            action_usuario: usuario_act, // PUEDE SER: L, I, M , E
            action: usuario_act, // PUEDE SER: L, I, M , E
            cli_id: creditogarveh_id,
            vista: 'hdd_cli_id'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_creditogarveh_form').html(data);
                $('#modal_registro_creditogarveh').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_creditogarveh'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditogarveh', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_width_auto('modal_registro_creditogarveh', 80);
                modal_height_auto('modal_registro_creditogarveh');
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'creditogarveh';
                var div = 'div_modal_creditogarveh_form';
                permiso_solicitud(usuario_act, creditogarveh_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}


function cmb_usu_id(empresa_id) {
    var mostrar = 0;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/cmb_usu_id_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
            empresa_id: empresa_id
        }),
        beforeSend: function () {
            $('#cmb_usu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_usu_id').html(html);
        },
        complete: function (html) {

        }
    });
}

function planilla_colaborador() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "planilla/planilla_colaborador.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'planilla',
            usu_id: $('#cmb_usu_id').val(),
            mes: $('#cmb_pla_mes').val(),
            anio: $('#cmb_pla_anio').val()
        }),
        beforeSend: function () {
//      $('#div_planilla').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
        success: function (html) {
            $('#div_planilla_tabla').html(html);
        },
        complete: function (data) {
            if (data.status != 200)
                console.log(data);
        }
    });
}

function planilla_colaborador_comisiones() {
    $.ajax({
        type: "POST",
        url: VISTA_URL+"planilla/planilla_pago.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'comisiones_asesor',
            usu_id: $('#cmb_usu_id').val(),
            mes: $('#cmb_pla_mes').val(),
            anio: $('#cmb_pla_anio').val()
        }),
        beforeSend: function () {
//      $('#div_planilla').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
        success: function (data) {
            console.log(data);
            if (parseInt(data.est) == 1) {
                swal_success("SISTEMA",data.msj,3000);
            }else{
                if (parseInt(data.est) == 2) {
                    swal_warning("SISTEMA",data.msj,3000);
                }else{
                    swal_warning("SISTEMA",data.msj,3000);
                }
            }
        },
        complete: function (data) {
            if (data.status != 200)
                console.log(data);
        }
    });
}


function pagar_colaborador(act, usu, tipo) {
    /*var msj = '¿Está seguro de pagar el sueldo al colaborador?';
     if(act == 'comision')
     msj = '¿Está seguro de pagar las comisiones al colaborador?';
     if(!confirm(msj))
     return false;*/

    var sueldo_total = 0;
    var sueldo_bruto = 0;
    var sueldo_comision = 0;

    sueldo_total = Number($('#hdd_total_sueldo').val().replace(/[^0-9\.]+/g, ""));
    sueldo_bruto = Number($('#hdd_sueldo').val().replace(/[^0-9\.]+/g, ""));
    sueldo_comision = Number($('#hdd_comision').val().replace(/[^0-9\.]+/g, ""));

    if (sueldo_total <= 0) {
//    alert('El monto a desembolsar no puede ser cero');
        swal_warning("AVISO", "EL MONTO A DESEMBOLSAR NO PUEDE SER CERO", 2500);
        return false;
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "planilla/planilla_pago_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            usu_id: usu,
            sueldo_total: sueldo_total,
            sueldo_bruto: sueldo_bruto,
            sueldo_comision: sueldo_comision,
            mes: $('#cmb_pla_mes').val(),
            anio: $('#cmb_pla_anio').val(),
            tipo: tipo
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_planilla_form').html(html);
            $('#div_modal_planilla_form').modal('show');
            modal_hidden_bs_modal('div_modal_planilla_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('div_modal_planilla_form',60);
            modal_height_auto('div_modal_planilla_form');
        },
        complete: function (data) {
            //console.log(data);
            if (data.statusText != "success") {
//        $('#msj_aportes').html('Error: '+data.responseText);
//        console.log(data);
            }
        }
    });
}

function planilla_detalle_creditos(tip_enven_cre, usuario, tipo_personal) {
    //tip_enven_cre: 1 colocacion, 2 venta garantias, 3 valor sobre costo
    $.ajax({
        type: "POST",
        url: VISTA_URL + "planilla/planilla_detalle_creditos.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'planilla',
            tipo: tip_enven_cre,
            usu_id: usuario,
            tip_per: tipo_personal,
            mes: $('#cmb_pla_mes').val(),
            anio: $('#cmb_pla_anio').val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_planilla_det_cred').html(html);
            $('#div_planilla_det_cred_modal').modal('show');
            modal_width_auto('div_planilla_det_cred_modal', 80);
            modal_height_auto('div_planilla_det_cred_modal');
            modal_hidden_bs_modal('div_planilla_det_cred_modal', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            //if (data.status != 200)
                console.log(data);
        }
    });
}



/* SE SE SELECCIONA DONDE SE IRÁ A REALIZAR EL PAGO SI ES EN LA MISMA OFICINA O EN ALGUNA ENTIDAD BANCARIA*/
function pagar_colaborador_form(act, usu) {
    Swal.fire({
        title: 'SELECCIONE EL LUGAR DONDE REALIZARÁ EL PAGO DE PLANILLA',
        icon: 'info',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
        showDenyButton: true,
        denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
        confirmButtonColor: '#3b5998', 
        denyButtonColor: '#21ba45',
      }).then((result) => {
    
        if (result.isConfirmed) {
            pagar_colaborador(act, usu, 1);
        } else if (result.isDenied) {
            pagar_colaborador(act, usu, 2);
        }
      });
}


function pagar_ventainterna(ven,pago_parcial) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "menorgarantia/ventainterna_pago_form.php",
        async: true,
        dataType: "html",
        data: ({
            venin_id: ven,
            vista: 'planilla',
            tipo: 1,
            pago_parcial: pago_parcial
        }),
        beforeSend: function () {
//      $('#div_pagar_ventainterna').dialog('open');
        },
        success: function (html) {
            $('#div_planilla_det_cred').html(html);
            $('#modal_pagarventainterna_registro').modal('show');
//    modal_width_auto('modal_pagarventainterna_registro',60);
            modal_hidden_bs_modal('modal_pagarventainterna_registro', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_pagarventainterna_registro', 25);
        },
        complete: function (data) {
//      if(data.statusText != "success")
//        console.log(data); 
        }
    });
}