<!-- TOTAL COMISIONES-->
<?php
// $final_comisiones = $total_comiAsveh + $total_comiGarveh + $total_comiHipo + $total_comiMenor + $total_comiVenta + $pago_recaudo + $pago_mejoragc;

//? LAS METAS ESTÁN ASIGNADAS POR CADA COLABORADOR Y POR TIPO DE CRÉDITO, SI LLEGA AL 100% DE SU META ASIGANA COBRARÁ EL 100% DE SUS COMISIONES PERO SI NO LO LOGRA
//? SOLO COBRARÁ EL 50% DE LAS COMISIONES ASIGANADAS
$final_comisiones = $total_comiAsveh + $total_comiGarveh + $total_comiHipo + $comision_colocacion_cmenor + $comision_venta_cmenor + $total_comiVenta + $pago_recaudo + $pago_mejoragc;


$metacredito_creditos = 0;
$metacredito_cmenor = 0;
$metacredito_ventacm = 0;
$metacredito_ventaveh = 0;

$logrado_creditos = $total_comiAsveh + $total_comiGarveh + $total_comiHipo;
$MONTO_LOGRADO_CREDITOS = $MONTO_COLOCADO_ASIVEH + $MONTO_COLOCADO_GARVEH + $MONTO_COLOCADO_HIPO;

$tiene_metas = 'NO';
$result6 = $oMetacredito->buscarmeta_usuario($usu_id, $mes, $anio);
  if($result6['estado'] == 1){
    $metacredito_creditos = $result6['data']['tb_metacredito_creditos'];
    $metacredito_cmenor = $result6['data']['tb_metacredito_cmenor'];
    $metacredito_ventacm = $result6['data']['tb_metacredito_ventacm'];
    $metacredito_ventaveh = $result6['data']['tb_metacredito_ventaveh'];

    $tiene_metas = 'SI';
  }
$result6 = NULL;

$COBRAR_REAL_CREDITOS = $logrado_creditos;
$COBRAR_REAL_CMENOR = $comision_colocacion_cmenor;
$COBRAR_REAL_VENTACM = $comision_venta_cmenor;
$COBRAR_REAL_VENTAVEH = $total_comiVenta;
$BONO_POR_METAS = 0;

$porcentaje_creditos = 1;
$porcentaje_cmenor = 1;
$porcentaje_ventacm = 1;
$porcentaje_ventaveh = 1;

$td_logrado_creditos = '<span class="badge bg-green">'.mostrar_moneda($MONTO_LOGRADO_CREDITOS).' (ASIVEH: '.$MONTO_COLOCADO_ASIVEH.', GARVEH: '.$MONTO_COLOCADO_GARVEH.', HIPO: '.$MONTO_COLOCADO_HIPO.')</span>';
$td_logrado_cmenor = '<span class="badge bg-green">'.mostrar_moneda($MONTO_COLOCADO_CMENOR).'</span>';
$td_logrado_ventacm = '<span class="badge bg-green">'.mostrar_moneda($MONTO_VENDIDO_CMENOR).'</span>';
$td_logrado_ventaveh = '<span class="badge bg-green">'.$CANTIDAD_VEHICULOS_VENDIDOS.' unidades</span>';
$td_logrado_bono = '<span class="badge bg-red">Lamentablemente no logró cumplir sus metas</span>';

if($MONTO_LOGRADO_CREDITOS < $metacredito_creditos){
  $porcentaje_creditos = 0.5;
  $td_logrado_creditos = '<span class="badge bg-red">'.mostrar_moneda($MONTO_LOGRADO_CREDITOS).'</span>';
}
if($MONTO_COLOCADO_CMENOR < $metacredito_cmenor){
  $porcentaje_cmenor = 0.5;
  $td_logrado_cmenor = '<span class="badge bg-red">'.mostrar_moneda($MONTO_COLOCADO_CMENOR).'</span>';
}
if($MONTO_VENDIDO_CMENOR < $metacredito_ventacm){
  $porcentaje_ventacm = 0.5;
  $td_logrado_ventacm = '<span class="badge bg-red">'.mostrar_moneda($MONTO_VENDIDO_CMENOR).'</span>';
}
if($CANTIDAD_VEHICULOS_VENDIDOS < $metacredito_ventaveh){ //los vehísulos son los únicos que se miden por CANTIDAD DE UNIDADES VENDIDAS
  $porcentaje_ventaveh = 0.5;
  $td_logrado_ventaveh = '<span class="badge bg-red">'.$CANTIDAD_VEHICULOS_VENDIDOS.' unidades</span>';
}

//? revisemos si se logró cumplir LAS 4 METAS para poder pagarle el BONO ofrecido de S/. 400
if($tiene_metas == 'SI' && $MONTO_LOGRADO_CREDITOS >= $metacredito_creditos && $MONTO_COLOCADO_CMENOR >= $metacredito_cmenor && $MONTO_VENDIDO_CMENOR >= $metacredito_ventacm && $CANTIDAD_VEHICULOS_VENDIDOS >= $metacredito_ventaveh){
  //se cumplió con las 3 metas, genial se gana el bono
  $BONO_POR_METAS = 400;
  $td_logrado_bono = '<span class="badge bg-green">Felicitaciones ha logrado conseguir el Bono</span>';
}

$COBRAR_REAL_CREDITOS = $COBRAR_REAL_CREDITOS * $porcentaje_creditos;
$COBRAR_REAL_CMENOR = $COBRAR_REAL_CMENOR * $porcentaje_cmenor;
$COBRAR_REAL_VENTACM = $COBRAR_REAL_VENTACM * $porcentaje_ventacm;
$COBRAR_REAL_VENTAVEH = $COBRAR_REAL_VENTAVEH * $porcentaje_ventaveh;

$FINAL_SUMA_REAL = $COBRAR_REAL_CREDITOS + $COBRAR_REAL_CMENOR + $COBRAR_REAL_VENTACM + $COBRAR_REAL_VENTAVEH + $BONO_POR_METAS + $pago_recaudo + $pago_mejoragc;

$final_comisiones = $FINAL_SUMA_REAL - $total_reversion - $total_descuentos;
$sueldo_comision = $final_comisiones;
?>
<div class="shadow">
  <table class="table table-striped table-bordered">
    <tr>
      <th>Descripción de Meta</th>
      <th>Meta</th>
      <th>Logrado</th>
      <th>% a Pagar</th>
      <th>Pago Final</th>
    </tr>
    <tr>
      <td>Garveh + Asiveh + Hipo</td>
      <td><?php echo mostrar_moneda($metacredito_creditos);?></td>
      <td><?php echo $td_logrado_creditos;?></td>
      <td><?php echo ($porcentaje_creditos * 100).' %';?></td>
      <td><?php echo mostrar_moneda($COBRAR_REAL_CREDITOS);?></td>
    </tr>
    <tr>
      <td>Crédito Menor</td>
      <td><?php echo mostrar_moneda($metacredito_cmenor);?></td>
      <td><?php echo $td_logrado_cmenor;?></td>
      <td><?php echo ($porcentaje_cmenor * 100).' %';?></td>
      <td><?php echo mostrar_moneda($COBRAR_REAL_CMENOR);?></td>
    </tr>
    <tr>
      <td>Venta Remates</td>
      <td><?php echo mostrar_moneda($metacredito_ventacm);?></td>
      <td><?php echo $td_logrado_ventacm;?></td>
      <td><?php echo ($porcentaje_ventacm * 100).' %';?></td>
      <td><?php echo mostrar_moneda($COBRAR_REAL_VENTACM);?></td>
    </tr>
    <tr>
      <td>Venta Vehículos</td>
      <td><?php echo $metacredito_ventaveh;?></td>
      <td><?php echo $td_logrado_ventaveh;?></td>
      <td><?php echo ($porcentaje_ventaveh * 100).' %';?></td>
      <td><?php echo mostrar_moneda($COBRAR_REAL_VENTAVEH);?></td>
    </tr>
    <tr>
      <th>Bono por complimiento de Metas</th>
      <td colspan="3"><?php echo $td_logrado_bono;?></td>
      <td><?php echo mostrar_moneda($BONO_POR_METAS);?></td>
    </tr>
    <tr>
      <th colspan="4" style="font-size: 13pt;">TOTAL DE COMISIONES REALES A PAGAR</th>
      <th style="font-size: 13pt;"><?php echo mostrar_moneda($FINAL_SUMA_REAL);?></th>
    </tr>
  </table>
</div>