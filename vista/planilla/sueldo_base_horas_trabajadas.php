

<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">SUELDO BASE Y HORAS TRABAJADAS</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table class="table-hover" style="font-family: cambria;font-size: 12px;width: 100%">
            <tr id="tabla_fila" style="height:20px;">
                <td id="tabla_fila" style="font-weight: bold">HORAS A CUMPLIR</td>
                <td id="tabla_fila" style="text-align: center"><?php echo $usu_horas; ?></td>
                <td id="tabla_fila" style="font-weight: bold">SUELDO BASE</td>
                <td id="tabla_fila" style="text-align: right"><?php echo 'S/. ' . $usu_suel; ?></td>
            </tr>
            <?php //if ($bono_val > 0): ?>
                <!-- <tr id="tabla_fila" style="height:20px;">
                    <td id="tabla_fila" style="font-weight: bold" colspan="2">BONO:</td>
                    <td id="tabla_fila" style="font-weight: bold"><?php //echo $bono_nom; ?></td>
                    <td id="tabla_fila" style="text-align: right">
                        <?php
                        /* $total_bono = 0;
                        if ($bono_tip == 1)
                            $total_bono = $usu_suel * $bono_val / 100;
                        if ($bono_tip == 2)
                            $total_bono = $bono_val;

                        $total_sueldo_base += $total_bono;
                        echo 'S/. ' . mostrar_moneda($total_bono); */
                        ?>
                    </td>
                </tr> -->
            <?php //endif; ?>

            <!-- GERSON (16-03-23) -->
            <?php if ($listBono != null || $listBono!=''): ?>
                <?php $total_bono = 0; ?>
                <tr id="tabla_fila" style="height:20px;">
                    <td id="tabla_fila" style="font-weight: bold" colspan="2">BONO:</td>
                    <td align="center" id="tabla_fila" style="font-weight: bold">Detalle</td>
                    <td align="center" id="tabla_fila" style="font-weight: bold">Monto</td>
                </tr>
                <?php foreach ($listBono as $key => $v) { ?>
                    <?php
                        $monto_det = 0.00;
                        if (intval($v['bono_tip']) == 1)
                            $monto_det = $usu_suel * $v['bono_val'] / 100;
                        if (intval($v['bono_tip']) == 2)
                            $monto_det = $v['bono_val'];

                        $total_bono = $total_bono + $monto_det;
                    ?>
                    <tr id="tabla_fila" style="height:20px;">
                        <td id="tabla_fila" colspan="2"></td>
                        <td align="left" id="tabla_fila"><?php echo $v['bono_nom']; ?></td>
                        <td align="right" id="tabla_fila"><?php echo 'S/. '.mostrar_moneda($monto_det); ?></td>
                    </tr>
                <?php } ?>
                <?php $total_sueldo_base += $total_bono; ?>
                <tr id="tabla_fila" style="height:20px;">
                    <td id="tabla_fila" style="font-weight: bold" colspan="2"></td>
                    <td align="left" id="tabla_fila" style="font-weight: bold">Total Bono</td>
                    <td align="right" id="tabla_fila" style="font-weight: bold"><?php echo 'S/. '.mostrar_moneda($total_bono); ?></td>
                </tr>
            <?php endif; ?>
            <!--  -->

            <?php
            $mes_anio = $mes . '-' . substr($anio, 2, 4);
            $horas_dia = 0;
            $horas_tarde = 0;
            $minutos_tardanza = 0;
            $penalidad_tar = 0;
            $total_horas = 0;
            $minutos_penalidad = 0;
            // Gerson (24-03-23)
            $suma_tardanza = 0;
            $suma_tardanza_pena = 0;
            $monto_penalidad = 0;
            //

            //los descuentos será con faltas y tardanzas, al principio su sueldo base es el mismo
            $total_sueldo_base += $usu_suel;

            //$dts = $oAsistencia->listar_asistencia_mes_colaborador($usu_id, $mes_anio);
            $dts = $oAsistencia->listar_tardanzas($usu_id, $mes_anio);

            //tipo de turno full time, validar que ingresos y salidas no esteb vacias o 00:00:00
            $detalle_extras = '';
            if ($usu_tur == 1) {
                if (validar_horas($usu_ing1, $usu_sal1) && validar_horas($usu_ing2, $usu_sal2)) {
                    if ($dts['estado'] == 1) {
                        foreach ($dts['data']as $key => $dt) {
                            $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
                            $mar_ingAM = $dt['tb_asistencia_ing1']; //hora marcada
                            $mar_salAM = $dt['tb_asistencia_sal1']; //hora marcada
                            $mar_ingPM = $dt['tb_asistencia_ing2']; //hora marcada
                            $mar_salPM = $dt['tb_asistencia_sal2']; //hora marcada
                            $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas
                            
                            if ($just == 0) {

                                // Gerson (15-04-23)
                                $segundos_total = 0;
                                $minutos_total = 0;
                                $total_segundos_tarde = 0;
                                $total_segundos_am = 0;
                                $total_segundos_pm = 0;
                                $sabado = date("w", strtotime($dt['tb_asistencia_fec']));
                                if($sabado==6){ // es sabado
                                    if($dt['tb_empresa_id']==1){ // sede boulevard
                                        $ingreso_am = '09:00:00';
                                    }else{
                                        if($dt['tb_asistencia_horing1']!=null){
                                            $ingreso_am = $dt['tb_asistencia_horing1'];
                                        }else{
                                            $ingreso_am = $dt['tb_usuario_ing1'];
                                        }
                                    }
                                }else{
                                    if($dt['tb_asistencia_horing1']!=null){
                                        $ingreso_am = $dt['tb_asistencia_horing1'];
                                    }else{
                                        $ingreso_am = $dt['tb_usuario_ing1'];
                                    }
                                }

                                if($dt['tb_asistencia_horing2']!=null){
                                    $ingreso_pm = $dt['tb_asistencia_horing2'];
                                }else{
                                    $ingreso_pm = $dt['tb_usuario_ing2'];
                                }
                                    
                                $minutos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);
                                $minutos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);

                                if(strtotime($dt['tb_asistencia_ing1'])<0 || $dt['tb_asistencia_ing1']==null){
                                    $total_segundos_am = 0;
                                }else{
                                    if(strtotime($dt['tb_asistencia_ing1']) < strtotime($ingreso_am)){
                                        $total_segundos_am = 0;
                                    }else{
                                        $total_segundos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);
                                    }
                                }
                                if(strtotime($dt['tb_asistencia_ing2'])<0 || $dt['tb_asistencia_ing2']==null){
                                    $total_segundos_pm = 0;
                                }else{
                                    if(strtotime($dt['tb_asistencia_ing2']) < strtotime($ingreso_pm)){
                                        $total_segundos_pm = 0;
                                    }else{
                                        $total_segundos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);
                                    }
                                }
                                $total_segundos_tarde = $total_segundos_am + $total_segundos_pm; // suma de segundos entre am y pm, máximo 300seg = 5min

                                $mostrar = 0;
                                if($total_segundos_tarde > 300){
                                    $mostrar = 1;

                                    $sum_min_tardanza_pena = $sum_min_tardanza_pena + round($total_segundos_tarde / 60);
                                    $sum_min_pena = $sum_min_pena + ($total_segundos_tarde - 300);
                                }elseif($total_segundos_tarde > 0 && $total_segundos_tarde <= 300){
                                    $mostrar = 1;

                                    $sum_min_tardanza = $sum_min_tardanza + round($total_segundos_tarde / 60);
                                }

                                $marcaje_am='';
                                if($value['tb_asistencia_ing1']!=null){
                                    if($minutos_am<0){
                                        $segundos_am = 0;
                                    }else{
                                        $segundos_am = $minutos_am%60%60%60;
                                    }
                                }else{
                                    $marcaje_am='NO MARCÓ';
                                    $segundos_am = 0;
                                }
                                $marcaje_pm='';
                                if($value['tb_asistencia_ing2']!=null){
                                    if($minutos_pm<0){
                                        $segundos_pm = 0;
                                    }else{
                                        $segundos_pm = $minutos_pm%60%60%60;
                                    }
                                }else{
                                    $marcaje_pm='NO MARCÓ';
                                    $segundos_pm = 0;
                                }

                                if($value['tb_asistencia_ing1']!=null){
                                    if($minutos_am<0){
                                        $minutos_am = 0;
                                    }else{
                                        $truncar = 10**0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                                        $minutos_am = intval(($minutos_am / 60) * $truncar) / $truncar;
                                    }
                                }else{
                                    $minutos_am = 0;
                                }
                                if($value['tb_asistencia_ing2']!=null){
                                    if($minutos_pm<0){
                                        $minutos_pm = 0;
                                    }else{
                                        $truncar = 10**0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                                        $minutos_pm = intval(($minutos_pm / 60) * $truncar) / $truncar;
                                    }
                                }else{
                                    $minutos_pm = 0;
                                }

                                //



                                //if ((strtotime($mar_ingAM)<0 || $mar_ingAM==null) && strtotime($mar_ingPM)<0 || $mar_ingPM==null) {
                                    $arr_AM = horas_cumplidas_turno_asignado($usu_ing1, $usu_sal1, $mar_ingAM, $mar_salAM, 1, $fecha); //devuelve array (estado,tardanza,horas)
                                    $arr_PM = horas_cumplidas_turno_asignado($usu_ing2, $usu_sal2, $mar_ingPM, $mar_salPM, 2, $fecha); //devuelve array (estado,tardanza,horas)
                                    
                            } else {
                                $horas_dia += 4;
                                $horas_tarde += 4;
                            }
                            if($dt['tb_asistencia_ext'] != 0)
                                $detalle_extras .= $dt['tb_asistencia_fec'].' - '.$dt['tb_asistencia_ext'].' | ';
                        }
                    }
                    $total_horas = $horas_dia + $horas_tarde;

                    // GERSON (17-04-23)
                    $penalidad_tar = 0.00;
                    $minutes_total = 0;
                    $minutes_danger = 0;

                    $minutes = round($sum_min_pena / 60);
                    $penalidad_tar = penalidad_tardanza_final($minutes, $usu_suel, $usu_horas);
                    $minutes_total = $sum_min_tardanza + $sum_min_tardanza_pena;
                    $minutes_danger = round($sum_min_pena / 60);
                    //

                } else
                    echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: FULL TIME</span>';
            }
            //tipo de turno PART TIME EN LA MAÑANA, validar que ingreso 1 y salida 1 no esté vacío ni 00:00:00
            if ($usu_tur == 2) {
                if (validar_horas($usu_ing1, $usu_sal1)) {
                    if ($dts['estado'] == 1) {
                        foreach ($dts['data']as $key => $dt) {
                            $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
                            $mar_ingAM = $dt['tb_asistencia_ing1']; //hora marcada
                            $mar_salAM = $dt['tb_asistencia_sal1']; //hora marcada
                            $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas

                            if ($just == 0) {

                                // Gerson (15-04-23)
                                $segundos_total = 0;
                                $minutos_total = 0;
                                $total_segundos_tarde = 0;
                                $total_segundos_am = 0;
                                $sabado = date("w", strtotime($dt['tb_asistencia_fec']));
                                if($sabado==6){ // es sabado
                                    if($dt['tb_empresa_id']==1){ // sede boulevard
                                        $ingreso_am = '09:00:00';
                                    }else{
                                        if($dt['tb_asistencia_horing1']!=null){
                                            $ingreso_am = $dt['tb_asistencia_horing1'];
                                        }else{
                                            $ingreso_am = $dt['tb_usuario_ing1'];
                                        }
                                    }
                                }else{
                                    if($dt['tb_asistencia_horing1']!=null){
                                        $ingreso_am = $dt['tb_asistencia_horing1'];
                                    }else{
                                        $ingreso_am = $dt['tb_usuario_ing1'];
                                    }
                                }
                                    
                                $minutos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);

                                if(strtotime($dt['tb_asistencia_ing1'])<0 || $dt['tb_asistencia_ing1']==null){
                                    $total_segundos_am = 0;
                                }else{
                                    if(strtotime($dt['tb_asistencia_ing1']) < strtotime($ingreso_am)){
                                        $total_segundos_am = 0;
                                    }else{
                                        $total_segundos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);
                                    }
                                }
                                
                                $total_segundos_tarde = $total_segundos_am; // suma de segundos entre am, máximo 300seg = 5min

                                $mostrar = 0;
                                if($total_segundos_tarde > 300){
                                    $mostrar = 1;

                                    $sum_min_tardanza_pena = $sum_min_tardanza_pena + round($total_segundos_tarde / 60);
                                    $sum_min_pena = $sum_min_pena + ($total_segundos_tarde - 300);
                                }elseif($total_segundos_tarde > 0 && $total_segundos_tarde <= 300){
                                    $mostrar = 1;

                                    $sum_min_tardanza = $sum_min_tardanza + round($total_segundos_tarde / 60);
                                }

                                $marcaje_am='';
                                if($value['tb_asistencia_ing1']!=null){
                                    if($minutos_am<0){
                                        $segundos_am = 0;
                                    }else{
                                        $segundos_am = $minutos_am%60%60%60;
                                    }
                                }else{
                                    $marcaje_am='NO MARCÓ';
                                    $segundos_am = 0;
                                }

                                if($value['tb_asistencia_ing1']!=null){
                                    if($minutos_am<0){
                                        $minutos_am = 0;
                                    }else{
                                        $truncar = 10**0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                                        $minutos_am = intval(($minutos_am / 60) * $truncar) / $truncar;
                                    }
                                }else{
                                    $minutos_am = 0;
                                }

                            } else {
                                $horas_dia += 4;
                            }
                        }
                    }

                    //$total_horas = $horas_dia;

                    // GERSON (17-04-23)
                    $penalidad_tar = 0.00;
                    $minutes_total = 0;
                    $minutes_danger = 0;

                    $minutes = round($sum_min_pena / 60);
                    $penalidad_tar = penalidad_tardanza_final($minutes, $usu_suel, $usu_horas);
                    $minutes_total = $sum_min_tardanza + $sum_min_tardanza_pena;
                    $minutes_danger = round($sum_min_pena / 60);
                    //

                } else
                    echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: PART TIME EN LA MAÑANA</span>';
            }
            //tipo de turno PART TIME EN LA TARDE, validar que el ingreso 2 y salida 2 del usuario no estén vacios ni 00:00
            if ($usu_tur == 3) {
                if (validar_horas($usu_ing2, $usu_sal2)) {
                    if ($dts['estado'] == 1) {
                        foreach ($dts['data']as $key => $dt) {
                            $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
                            $mar_ingPM = $dt['tb_asistencia_ing1']; //si es turno tarde, su marcacion de ingreso se guarda en asistencia_ing1
                            $mar_salPM = $dt['tb_asistencia_sal1']; //si es turno tarde, su marcacion de salida se guarda en asistencia_sal1
                            $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas

                            if ($just == 0) {

                                // Gerson (15-04-23)
                                $segundos_total = 0;
                                $minutos_total = 0;
                                $total_segundos_tarde = 0;
                                $total_segundos_pm = 0;
                                $sabado = date("w", strtotime($dt['tb_asistencia_fec']));
                                /* if($sabado==6){ // es sabado
                                    if($dt['tb_empresa_id']==1){ // sede boulevard
                                        $ingreso_am = '09:00:00';
                                    }else{
                                        if($dt['tb_asistencia_horing1']!=null){
                                            $ingreso_am = $dt['tb_asistencia_horing1'];
                                        }else{
                                            $ingreso_am = $dt['tb_usuario_ing1'];
                                        }
                                    }
                                }else{
                                    if($dt['tb_asistencia_horing1']!=null){
                                        $ingreso_am = $dt['tb_asistencia_horing1'];
                                    }else{
                                        $ingreso_am = $dt['tb_usuario_ing1'];
                                    }
                                } */

                                if($dt['tb_asistencia_horing2']!=null){
                                    $ingreso_pm = $dt['tb_asistencia_horing2'];
                                }else{
                                    $ingreso_pm = $dt['tb_usuario_ing2'];
                                }
                                    
                                $minutos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);

                                if(strtotime($dt['tb_asistencia_ing2'])<0 || $dt['tb_asistencia_ing2']==null){
                                    $total_segundos_pm = 0;
                                }else{
                                    if(strtotime($dt['tb_asistencia_ing2']) < strtotime($ingreso_pm)){
                                        $total_segundos_pm = 0;
                                    }else{
                                        $total_segundos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);
                                    }
                                }
                                $total_segundos_tarde = $total_segundos_pm; // suma de segundos entre am y pm, máximo 300seg = 5min

                                $mostrar = 0;
                                if($total_segundos_tarde > 300){
                                    $mostrar = 1;

                                    $sum_min_tardanza_pena = $sum_min_tardanza_pena + round($total_segundos_tarde / 60);
                                    $sum_min_pena = $sum_min_pena + ($total_segundos_tarde - 300);
                                }elseif($total_segundos_tarde > 0 && $total_segundos_tarde <= 300){
                                    $mostrar = 1;

                                    $sum_min_tardanza = $sum_min_tardanza + round($total_segundos_tarde / 60);
                                }

                                $marcaje_pm='';
                                if($value['tb_asistencia_ing2']!=null){
                                    if($minutos_pm<0){
                                        $segundos_pm = 0;
                                    }else{
                                        $segundos_pm = $minutos_pm%60%60%60;
                                    }
                                }else{
                                    $marcaje_pm='NO MARCÓ';
                                    $segundos_pm = 0;
                                }

                                if($value['tb_asistencia_ing2']!=null){
                                    if($minutos_pm<0){
                                        $minutos_pm = 0;
                                    }else{
                                        $truncar = 10**0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                                        $minutos_pm = intval(($minutos_pm / 60) * $truncar) / $truncar;
                                    }
                                }else{
                                    $minutos_pm = 0;
                                }

                                //



                                $arr_PM = horas_cumplidas_turno_asignado($usu_ing2, $usu_sal2, $mar_ingPM, $mar_salPM, 2, $fecha); //devuelve array (estado,tardanza,horas)

                            } else {
                                $horas_tarde += 4;
                            }
                        }
                    }
                    //$total_horas = $horas_tarde;
                    
                    // GERSON (17-04-23)
                    $penalidad_tar = 0.00;
                    $minutes_total = 0;
                    $minutes_danger = 0;

                    $minutes = round($sum_min_pena / 60);
                    $penalidad_tar = penalidad_tardanza_final($minutes, $usu_suel, $usu_horas);
                    $minutes_total = $sum_min_tardanza + $sum_min_tardanza_pena;
                    $minutes_danger = round($sum_min_pena / 60);
                    //

                } else
                    echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: PART TIME EN LA MAÑANA</span>';
            }
            ?>

            <?php
                //* VAMOS A CALCULAR LAS HORAS EXTRAS TRABAJADAS POR EL COLABORADOR
                $extras = 0;
                $detalle_extras = '';
                if(intval($usu_horas) > 0)
                    $valor_sueldo_hora = $usu_suel / $usu_horas; // esto me calcula cuanto gana el colaborador por hora, ejem: 1,025 / 240 = 4.27 sol por hora
                else
                    $valor_sueldo_hora = $usu_suel / 240; // por defento todos estarían a 8 horas diarias trabajando
                $monto_pagar_extras = 0;

                $result = $oAsistencia->listar_horas_extras($usu_id, $mes_anio);
                    if($result['estado'] == 1){
                        foreach($result['data']as $key => $value){
                            $extras += $value['tb_asistencia_ext'];
                            $detalle_extras .= mostrar_fecha($value['tb_asistencia_fec']).' ('.$value['tb_asistencia_ext'].' min) | ';

                            $horas_extras = $value['tb_asistencia_ext'] / 60; //asistencia_ext está en minutos, los cambiamos a horas / 60
                            $calculo_extras = $horas_extras * $valor_sueldo_hora;
                            //según la ley se le debe pagar el 25% adicional por hora y por día las 2 primeras horas, luego se le paga el 35% a las siguientes horas
                            if($horas_extras < 3){
                                // ejem: 4.27(sueldo por hora) * 0.25 (25% adicional) * 2 (total horas extras trabajadas en este día)
                                $calculo_extras += $valor_sueldo_hora * 0.25 * $horas_extras;
                            }
                            $monto_pagar_extras += $calculo_extras;
                            $total_sueldo_base += $calculo_extras;
                        }
                    }
                $result = NULL;
            ?>
            <tr id="tabla_fila" style="height:20px;">
                <td id="tabla_fila" style="font-weight: bold">HORAS EXTRAS:</td>
                <td id="tabla_fila" style="text-align: center"><?php echo number_format($extras / 60, 1);?></td>
                <td id="tabla_fila" style="font-weight: bold">SUELDO EXTRAS:</td>
                <td id="tabla_fila" style="text-align: right"><?php echo 'S/. ' . number_format($monto_pagar_extras, 2); ?></td>
            </tr>
            <!-- JUAN ANTONIO: 31-07-2023-->
            <?php if($detalle_extras != ''):?>
                <tr id="tabla_fila" style="height:20px;">
                    <td colspan="4">Detalle horas extras: <?php echo $detalle_extras;?></td>
                </tr>
            <?php endif;?>

            <!-- GERSON (22-03-23) -->
            <?php
            $asignacion_monto = 0.00;
            $basico = 0.00;
            // en la tabla 'tb_config' se podrá registrar los sueldos basico minimos en caso se cambie durante el tiempo
            $config10 = $oPlanilla->obtener_sueldo_basico();
            if($config10['estado']==1){
                $basico = floatval($config10['data']['tb_config_valor']);
                if($basico>0){
                    $asignacion_monto = $basico*(0.10); // sacar el 10% del sueldo basico 
                }
            }
            if($asignacion==1){
                $total_sueldo_base += $asignacion_monto;
            ?>
            <tr id="tabla_fila" style="height:20px;">
                <td id="tabla_fila" style="font-weight: bold"></td>
                <td id="tabla_fila" style="text-align: center"></td>
                <td id="tabla_fila" style="font-weight: bold">ASIGNACIÓN FAMILIAR:</td>
                <td id="tabla_fila" style="text-align: right">S/. <?php echo mostrar_moneda($asignacion_monto); ?></td>
            </tr>
            <?php } ?>
            <!--  -->
            <tr id="tabla_fila" style="height:20px;">
                <td colspan="3" style="font-weight: bold">TOTAL SUELDO BASE: </td>
                <td style="text-align: right; font-weight: bold;"><?php echo 'S/. ' . number_format($total_sueldo_base, 2); ?></td>
            </tr>
        </table>
    </div>
</div> 

