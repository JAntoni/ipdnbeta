<?php
$fecha_cal = date('01-' . $mes . '-' . $anio);
$fecha1 = strtotime('-1 month', strtotime($fecha_cal));
$fecha2 = date('Y-m-31', $fecha1);
$fecha1 = date('Y-m-01', $fecha1);

list($dia_ant, $mes_ant, $anio_ant) = split('-', $fecha1);

$perso = $tip_per; //según tipo de personal del usuario
$evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
$est = 1; // el estado debe ser vigente 1, 0 no vigente

if ($tip_per == 1)
    $usu_pro_id = ''; //propietario del credito, jefatura es propietario de todos los creditos
else
    $usu_pro_id = intval($_POST['usu_id']);
//------------------------------credito ASIVEH ----------------------------------
$tabla = 'tb_creditoasiveh';
$colum_tipo = 'tb_credito_tip1';
$cre_tip = 2;
$asiveh_reversion = 0;
$asiveh_cred = '';

$dtsRegre = $oPlanilla->creditos_liquidados_reversion($tabla, $colum_tipo, $cre_tip, $fecha1, $fecha2, $usu_pro_id);
if ($dtsRegre['estado'] == 1) {
    foreach ($dtsRegre['data']as $key => $d) {
        $asiveh_cred .= 'CAV-' . $dt['tb_credito_id'] . ', ';
        $moneda = 'S/.';
        if ($dt['tb_moneda_id'] == 2)
            $moneda = 'US$';

        if ($dt['tb_credito_tip1'] == 1 || $dt['tb_credito_tip1'] == 4) {
            $cretip = 2; //credito asiveh es de tipo 2
            $cre_asveh = $dt['tb_credito_tip1']; //obtenemos el porcentaje para venta nueva o reventa
        }
        if ($dt['tb_credito_tip1'] == 2) {
            $cretip = 5; //para la formula de comision las adendas son de creditotipo_id = 5
            $cre_asveh = 0; //ya que es adenda no tiene tipo de asveh, valor 0
        }

        $monto = mostrar_moneda($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);

        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes_ant, $empresa_id);
        if ($dtsComi['estado'] == 1) {
            $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }

        $asiveh_reversion += mostrar_moneda(($monto * $porcen) / 100);
    }
}

//------------------------------credito GARVEHH ----------------------------------
$tabla = 'tb_creditogarveh';
$colum_tipo = 'tb_credito_tip';
$cre_tip = 3;
$garveh_reversion = 0;
$garveh_cred = '';
$dtsRegre = $oPlanilla->creditos_liquidados_reversion($tabla, $colum_tipo, $cre_tip, $fecha1, $fecha2, $usu_pro_id);
if ($dtsRegre['estado'] == 1) {
    foreach ($dtsRegre['data']as $key => $dt) {
        $garveh_cred .= 'CGV-' . $dt['tb_credito_id'] . ', ';
        $moneda = 'S/.';
        if ($dt['tb_moneda_id'] == 2)
            $moneda = 'US$';

        $cretip = 3; //credito garveh es de tipo 3
        $cre_asveh = 0; //como es garveh no tiene venta nueva o reventa, entonces tiene valor 0

        if ($dt['tb_credito_tip'] == 2) {
            $cretip = 5; //para todas las adendas de los creditos, adenda tiene como creditotipo_id = 5
        }

        $monto = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];

        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes_ant, $empresa_id);

        if ($dtsComi['estado'] == 1) {
            $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }

        $garveh_reversion += mostrar_moneda(($monto * $porcen) / 100);
    }
}

//------------------------------credito HIPOOOOOOO ----------------------------------
$tabla = 'tb_creditohipo';
$colum_tipo = 'tb_credito_tip';
$cre_tip = 4;
$hipo_reversion = 0;
$hipo_cred = '';
$dtsRegre = $oPlanilla->creditos_liquidados_reversion($tabla, $colum_tipo, $cre_tip, $fecha1, $fecha2, $usu_pro_id);
if ($dtsRegre['estado'] == 1) {
    foreach ($dtsRegre['data']as $key => $dt) {
        $hipo_cred .= 'CH-' . $dt['tb_credito_id'] . ', ';
        $moneda = 'S/.';
        if ($dt['tb_moneda_id'] == 2)
            $moneda = 'US$';

        $cretip = 4; //credito HIPO es de tipo 4
        $cre_asveh = 0; //como es garveh no tiene venta nueva o reventa, entonces tiene valor 0

        if ($dt['tb_credito_tip'] == 2) {
            $cretip = 5; //para todas las adendas de los creditos, adenda tiene como creditotipo_id = 5
        }

        $monto = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];

        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes_ant, $empresa_id);
        if ($dtsComi['estado'] == 1) {
            $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }
        $hipo_reversion += mostrar_moneda(($monto * $porcen) / 100);
    }
}

$reversion_manual = 0;
$reversion_manual_det = '';

if ($mes . '-' . $anio == '08-2020' && ($usu_id == 16 || $tip_per == 1)) {
    $reversion_manual = 605.09; //manual por el crédito de ortiz
    $reversion_manual_det = 'Descuento por modificación de comisión del crédito CGV-506';
}

$total_reversion = $asiveh_reversion + $garveh_reversion + $hipo_reversion + $reversion_manual;
?>
<!-- reversion DE COMISIONES--->

<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">REVERSIÓN DE COMSIONES</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table style="width: 100%;font-family: cambria;" class="table-hover">
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">TIPO CRÉDITO</th>
                <th id="tabla_cabecera_fila">CRÉDITOS</th>
                <th id="tabla_cabecera_fila">DEVOLVER</th>
            </tr>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold">CRÉDITO ASIVEH</td>
                <td id="tabla_fila" align="center"><?php echo $asiveh_cred; ?></td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($asiveh_reversion); ?></td>
            </tr>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold">CRÉDITO GARVEH</td>
                <td id="tabla_fila" align="center"><?php echo $garveh_cred; ?></td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($garveh_reversion); ?></td>
            </tr>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold">CRÉDITO HIPO</td>
                <td id="tabla_fila" align="center"><?php echo $hipo_cred; ?></td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($hipo_reversion); ?></td>
            </tr>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold">REGRESIÓN MANUAL</td>
                <td id="tabla_fila" align="center"><?php echo $reversion_manual_det; ?></td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($reversion_manual); ?></td>
            </tr>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="2"><b>TOTAL PARA REVERSIÓN</b></td>
                <td id="tabla_fila" align="center"><strong style="color: red;">- S/. <?php echo mostrar_moneda($total_reversion); ?></strong></td>
            </tr>
        </table>
    </div>
</div>