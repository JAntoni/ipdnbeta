

<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">SUELDO BASE Y HORAS TRABAJADAS</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table class="table-hover" style="font-family: cambria;font-size: 12px;width: 100%">
            <tr id="tabla_fila" style="height:20px;">
                <td id="tabla_fila" style="font-weight: bold">HORAS A CUMPLIR</td>
                <td id="tabla_fila" style="text-align: center"><?php echo $usu_horas; ?></td>
                <td id="tabla_fila" style="font-weight: bold">SUELDO BASE</td>
                <td id="tabla_fila" style="text-align: right"><?php echo 'S/. ' . $usu_suel; ?></td>
            </tr>
            <?php //if ($bono_val > 0): ?>
                <!-- <tr id="tabla_fila" style="height:20px;">
                    <td id="tabla_fila" style="font-weight: bold" colspan="2">BONO:</td>
                    <td id="tabla_fila" style="font-weight: bold"><?php //echo $bono_nom; ?></td>
                    <td id="tabla_fila" style="text-align: right">
                        <?php
                        /* $total_bono = 0;
                        if ($bono_tip == 1)
                            $total_bono = $usu_suel * $bono_val / 100;
                        if ($bono_tip == 2)
                            $total_bono = $bono_val;

                        $total_sueldo_base += $total_bono;
                        echo 'S/. ' . mostrar_moneda($total_bono); */
                        ?>
                    </td>
                </tr> -->
            <?php //endif; ?>

            <!-- GERSON (16-03-23) -->
            <?php if ($listBono != null || $listBono!=''): ?>
                <?php $total_bono = 0; ?>
                <tr id="tabla_fila" style="height:20px;">
                    <td id="tabla_fila" style="font-weight: bold" colspan="2">BONO:</td>
                    <td align="center" id="tabla_fila" style="font-weight: bold">Detalle</td>
                    <td align="center" id="tabla_fila" style="font-weight: bold">Monto</td>
                </tr>
                <?php foreach ($listBono as $key => $v) { ?>
                    <?php
                        $monto_det = 0.00;
                        if (intval($v['bono_tip']) == 1)
                            $monto_det = $usu_suel * $v['bono_val'] / 100;
                        if (intval($v['bono_tip']) == 2)
                            $monto_det = $v['bono_val'];

                        $total_bono = $total_bono + $monto_det;
                    ?>
                    <tr id="tabla_fila" style="height:20px;">
                        <td id="tabla_fila" colspan="2"></td>
                        <td align="left" id="tabla_fila"><?php echo $v['bono_nom']; ?></td>
                        <td align="right" id="tabla_fila"><?php echo 'S/. '.mostrar_moneda($monto_det); ?></td>
                    </tr>
                <?php } ?>
                <?php $total_sueldo_base += $total_bono; ?>
                <tr id="tabla_fila" style="height:20px;">
                    <td id="tabla_fila" style="font-weight: bold" colspan="2"></td>
                    <td align="left" id="tabla_fila" style="font-weight: bold">Total Bono</td>
                    <td align="right" id="tabla_fila" style="font-weight: bold"><?php echo 'S/. '.mostrar_moneda($total_bono); ?></td>
                </tr>
            <?php endif; ?>
            <!--  -->

            <?php
            $mes_anio = $mes . '-' . substr($anio, 2, 4);
            $horas_dia = 0;
            $horas_tarde = 0;
            $minutos_tardanza = 0;
            $penalidad_tar = 0;
            $total_horas = 0;
            $extras = 0;
            $minutos_penalidad = 0;
            // Gerson (24-03-23)
            $suma_tardanza = 0;
            $suma_tardanza_pena = 0;
            $monto_penalidad = 0;
            //

            //los descuentos será con faltas y tardanzas, al principio su sueldo base es el mismo
            $total_sueldo_base += $usu_suel;

            $dts = $oAsistencia->listar_asistencia_mes_colaborador($usu_id, $mes_anio);
//                    $rows_as = mysql_num_rows($dts);
            //tipo de turno full time, validar que ingresos y salidas no esteb vacias o 00:00:00
            if ($usu_tur == 1) {
                if (validar_horas($usu_ing1, $usu_sal1) && validar_horas($usu_ing2, $usu_sal2)) {
                    if ($dts['estado'] == 1) {
                        foreach ($dts['data']as $key => $dt) {
                            $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
                            $mar_ingAM = $dt['tb_asistencia_ing1']; //hora marcada
                            $mar_salAM = $dt['tb_asistencia_sal1']; //hora marcada
                            $mar_ingPM = $dt['tb_asistencia_ing2']; //hora marcada
                            $mar_salPM = $dt['tb_asistencia_sal2']; //hora marcada
                            $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas
                            //$del31 = ' SI/ '.$fecha.' / '.$newDate; echo $del31.'<br>';
                            if ($just == 0) {

                                

                                //if ((strtotime($mar_ingAM)<0 || $mar_ingAM==null) && strtotime($mar_ingPM)<0 || $mar_ingPM==null) {
                                    $arr_AM = horas_cumplidas_turno_asignado($usu_ing1, $usu_sal1, $mar_ingAM, $mar_salAM, 1, $fecha); //devuelve array (estado,tardanza,horas)
                                    $arr_PM = horas_cumplidas_turno_asignado($usu_ing2, $usu_sal2, $mar_ingPM, $mar_salPM, 2, $fecha); //devuelve array (estado,tardanza,horas)

                                    // Gerson (24-03-23)
                                    /* if($dt['tb_asistencia_horing1']!=null){
                                        $ingreso_am = $dt['tb_asistencia_horing1'];
                                    }else{
                                        //$ingreso_am = '00:00:00';
                                        $ingreso_am = $dt['tb_usuario_ing1'];
                                    }
                                    if($dt['tb_asistencia_horing2']!=null){
                                        $ingreso_pm = $dt['tb_asistencia_horing2'];
                                    }else{
                                        //$ingreso_pm = '00:00:00';
                                        $ingreso_pm = $dt['tb_usuario_ing2'];
                                    }
                                    
                                    //if(strtotime($mar_ingAM)<0 || $mar_ingAM==null){
                                    if($mar_ingAM==null){
                                        $total_segundos_am = 0;
                                        $minutos_am = 0;
                                    }else{
                                        //$total_segundos_am = strtotime($mar_ingAM) - strtotime($ingreso_am);
                                        //$minutos_am = strtotime($mar_ingAM) - strtotime($ingreso_am);

                                        if(strtotime($dt['tb_asistencia_ing1']) < strtotime($ingreso_am)){
                                            $total_segundos_am = 0;
                                            $minutos_am = 0;
                                        }else{
                                            $total_segundos_am = strtotime($mar_ingAM) - strtotime($ingreso_am);
                                            $minutos_am = strtotime($mar_ingAM) - strtotime($ingreso_am);
                                        }

                                    }
                                    //if(strtotime($mar_ingPM)<0 || $mar_ingPM==null){
                                    if($mar_ingPM==null){
                                        $total_segundos_pm = 0;
                                        $minutos_pm = 0;
                                    }else{
                                        //$total_segundos_pm = strtotime($mar_ingPM) - strtotime($ingreso_pm);
                                        //$minutos_pm = strtotime($mar_ingPM) - strtotime($ingreso_pm);

                                        if(strtotime($dt['tb_asistencia_ing2']) < strtotime($ingreso_pm)){
                                            $total_segundos_pm = 0;
                                            $minutos_pm = 0;
                                        }else{
                                            $total_segundos_pm = strtotime($mar_ingPM) - strtotime($ingreso_pm);
                                            $minutos_pm = strtotime($mar_ingPM) - strtotime($ingreso_pm);
                                        }
                                    }
                                    $total_segundos_tarde = $total_segundos_am + $total_segundos_pm; // suma de segundos entre am y pm, máximo 300seg = 5min

                                    $minutos_amx = 0;
                                    $minutos_pmx = 0;

                                    $horas_am = 0;
                                    $horas_pm = 0;

                                    if($dt['tb_asistencia_ing1']!=null){
                                        if($minutos_am<0){
                                            $segundos_am = 0;
                                        }else{
                                            $segundos_am = $minutos_am%60%60%60;
                                            $minutos_amx = round($minutos_am / 60);
                                            //$horas_am = floor($minutos_amx / 3600);
                                        }
                                    }
                                    if($dt['tb_asistencia_ing2']!=null){
                                        //$segundos_pm = $minutos_pm%60%60%60;
                                        if($minutos_pm<0){
                                            $segundos_pm = 0;
                                        }else{
                                            $segundos_pm = $minutos_pm%60%60%60;
                                            $minutos_pmx = round($minutos_pm / 60);
                                            //$horas_pm = floor($minutos_pmx / 3600);
                                        }
                                    }

                                    //$segundos_am = $minutos_am%60%60%60;
                                    //$segundos_pm = $minutos_pm%60%60%60;

                                    //$minutos_amx = round($minutos_am / 60);
                                    //$minutos_pmx = round($minutos_pm / 60);

                                    //$horas_am = floor($minutos_amx / 3600);
                                    //$horas_pm = floor($minutos_pmx / 3600); 

                                    //$am = str_pad($horas_am, 2, "0", STR_PAD_LEFT).':'.str_pad($minutos_amx, 2, "0", STR_PAD_LEFT).':'.str_pad($segundos_am, 2, "0", STR_PAD_LEFT);
                                    //$pm = str_pad($horas_pm, 2, "0", STR_PAD_LEFT).':'.str_pad($minutos_pmx, 2, "0", STR_PAD_LEFT).':'.str_pad($segundos_pm, 2, "0", STR_PAD_LEFT);
                                    $am = str_pad($minutos_amx, 2, "0", STR_PAD_LEFT).':'.str_pad($segundos_am, 2, "0", STR_PAD_LEFT);
                                    $pm = str_pad($minutos_pmx, 2, "0", STR_PAD_LEFT).':'.str_pad($segundos_pm, 2, "0", STR_PAD_LEFT); */
                                //

                                    if ($arr_AM['estado'] == 1 && $arr_PM['estado'] == 1) {
                                        //echo 'am: '.$arr_AM['horas'].' // pm: '.$arr_PM['horas'].'<br>';
                                        $horas_dia += $arr_AM['horas'];
                                        $horas_tarde += $arr_PM['horas'];
                                        $minutos_tardanza += $arr_AM['tardanza'];
                                        $minutos_tardanza += $arr_PM['tardanza'];

                                        $minutos_penalidad += $arr_AM['min_penalidad'];
                                        $minutos_penalidad += $arr_PM['min_penalidad'];

                                        $extras += $dt['tb_asistencia_ext']; //suma de minutos extras

                                        // Gerson (25-03-23)
                                        /* if($am=='00:00'){
                                            if($pm!='00:00'){
                                                $suma_tardanza += $minutos_am;
                                            }
                                        }elseif($pm=='00:00'){
                                            if($am!='00:00'){
                                                $suma_tardanza += $minutos_pm;
                                            }
                                        }else{
                                            $suma_tardanza += $minutos_am + $minutos_pm;
                                        }
                                        if($total_segundos_tarde > 300){ // si sobrepasa los 5 minutos de tolerancia, segundo penalizados
                                            $suma_tardanza_pena += ($total_segundos_tarde - 300);
                                        } */
                                        
                                        //$penalidad_tar += penalidad_tardanza(($suma_tardanza_pena), $usu_suel, $usu_horas);
                                        //

                                        $penalidad_tar += penalidad_tardanza($arr_AM['tardanza'], $usu_suel, $usu_horas, $fecha);
                                        $penalidad_tar += penalidad_tardanza($arr_PM['tardanza'], $usu_suel, $usu_horas, $fecha);
                                    } else {
                                        echo '<span style="color: red;">Error: Revise las horas marcadas de la fecha: ' . $fecha . '</span>';
                                        break;
                                    }
                                // } 
                            } else {
                                $extras += $dt['tb_asistencia_ext'];
                                $horas_dia += 4;
                                $horas_tarde += 4;
                            }
                        }
                    }
                    $total_horas = $horas_dia + $horas_tarde;
                    //echo $total_horas;
                } else
                    echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: FULL TIME</span>';
            }
            //tipo de turno PART TIME EN LA MAÑANA, validar que ingreso 1 y salida 1 no esté vacío ni 00:00:00
            if ($usu_tur == 2) {
                if (validar_horas($usu_ing1, $usu_sal1)) {
                    if ($dts['estado'] == 1) {
                        foreach ($dts['data']as $key => $dt) {
                            $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
                            $mar_ingAM = $dt['tb_asistencia_ing1']; //hora marcada
                            $mar_salAM = $dt['tb_asistencia_sal1']; //hora marcada
                            $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas

                            if ($just == 0) {
                                $arr_AM = horas_cumplidas_turno_asignado($usu_ing1, $usu_sal1, $mar_ingAM, $mar_salAM, 1, $fecha); //devuelve array (estado,tardanza,horas)

                                if ($arr_AM['estado'] == 1) {
                                    //echo 'am: '.$arr_AM['horas'].' // pm: '.$arr_PM['horas'].'<br>';
                                    $horas_dia += $arr_AM['horas'];
                                    $minutos_tardanza += $arr_AM['tardanza'];
                                    $minutos_penalidad += $arr_AM['min_penalidad'];
                                    $extras += $dt['tb_asistencia_ext']; //suma de minutos extras

                                    $penalidad_tar += penalidad_tardanza($arr_AM['tardanza'], $usu_suel, $usu_horas, $fecha);
                                } else {
                                    echo '<span style="color: red;">Error: Revise las horas marcadas de la fecha: ' . $fecha . '</span>';
                                    break;
                                }
                            } else {
                                $extras += $dt['tb_asistencia_ext'];
                                $horas_dia += 4;
                            }
                        }
                    }

                    $total_horas = $horas_dia;
                } else
                    echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: PART TIME EN LA MAÑANA</span>';
            }
            //tipo de turno PART TIME EN LA TARDE, validar que el ingreso 2 y salida 2 del usuario no estén vacios ni 00:00
            if ($usu_tur == 3) {
                if (validar_horas($usu_ing2, $usu_sal2)) {
                    if ($dts['estado'] == 1) {
                        foreach ($dts['data']as $key => $dt) {
                            $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
                            $mar_ingPM = $dt['tb_asistencia_ing1']; //si es turno tarde, su marcacion de ingreso se guarda en asistencia_ing1
                            $mar_salPM = $dt['tb_asistencia_sal1']; //si es turno tarde, su marcacion de salida se guarda en asistencia_sal1
                            $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas

                            if ($just == 0) {
                                $arr_PM = horas_cumplidas_turno_asignado($usu_ing2, $usu_sal2, $mar_ingPM, $mar_salPM, 2, $fecha); //devuelve array (estado,tardanza,horas)

                                if ($arr_PM['estado'] == 1) {
                                    //echo 'am: '.$arr_PM['horas'].' // pm: '.$arr_PM['horas'].'<br>';
                                    $horas_tarde += $arr_PM['horas'];
                                    $minutos_tardanza += $arr_PM['tardanza'];
                                    $minutos_penalidad += $arr_PM['min_penalidad'];
                                    $extras += $dt['tb_asistencia_ext']; //suma de minutos extras

                                    $penalidad_tar += penalidad_tardanza($arr_PM['tardanza'], $usu_suel, $usu_horas, $fecha);
                                } else {
                                    echo '<span style="color: red;">Error: Revise las horas marcadas de la fecha: ' . $fecha . '</span>';
                                    break;
                                }
                            } else {
                                $extras += $dt['tb_asistencia_ext'];
                                $horas_tarde += 4;
                            }
                        }
                    }
                    $total_horas = $horas_tarde;
                    echo $total_horas;
                } else
                    echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: PART TIME EN LA MAÑANA</span>';
            }
            ?>
            <tr id="tabla_fila" style="height:20px;">
                <td id="tabla_fila" style="font-weight: bold">HORAS EXTRAS:</td>
                <td id="tabla_fila" style="text-align: center"><?php echo number_format($extras / 60, 1); ?></td>
                <td id="tabla_fila" style="font-weight: bold">SUELDO EXTRAS:</td>
                <td id="tabla_fila" style="text-align: right">
                    <?php
                    $hor_ex = $extras / 60;
                    $res = 0;
                    if($usu_horas > 0)
                        $res = ($hor_ex * $usu_suel) / $usu_horas;
                    $res = $res * 1;
                    $total_sueldo_base += $res;
                    echo 'S/. ' . number_format($res, 2);
                    ?> 
                </td>
            </tr>
            <!-- GERSON (22-03-23) -->
            <?php
            $asignacion_monto = 0.00;
            $basico = 0.00;
            // en la tabla 'tb_config' se podrá registrar los sueldos basico minimos en caso se cambie durante el tiempo
            $config10 = $oPlanilla->obtener_sueldo_basico();
            if($config10['estado']==1){
                $basico = floatval($config10['data']['tb_config_valor']);
                if($basico>0){
                    $asignacion_monto = $basico*(0.10); // sacar el 10% del sueldo basico 
                }
            }
            if($asignacion==1){
                $total_sueldo_base += $asignacion_monto;
            ?>
            <tr id="tabla_fila" style="height:20px;">
                <td id="tabla_fila" style="font-weight: bold"></td>
                <td id="tabla_fila" style="text-align: center"></td>
                <td id="tabla_fila" style="font-weight: bold">ASIGNACIÓN FAMILIAR:</td>
                <td id="tabla_fila" style="text-align: right">S/. <?php echo mostrar_moneda($asignacion_monto); ?></td>
            </tr>
            <?php } ?>
            <!--  -->
            <tr id="tabla_fila" style="height:20px;">
                <td colspan="3" style="font-weight: bold">TOTAL SUELDO BASE: </td>
                <td style="text-align: right; font-weight: bold;"><?php echo 'S/. ' . number_format($total_sueldo_base, 2); ?></td>
            </tr>
        </table>
    </div>
</div> 

