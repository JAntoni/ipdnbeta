<?php
require_once ("../../core/usuario_sesion.php");
require_once('Planilla.class.php');
$oPlanilla = new Planilla();
require_once('../formulacomision/Formulacomision.class.php');
$oComision = new Formulacomision();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../buscaveh/Buscaveh.class.php');
$oBuscaveh = new Buscaveh();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

//tipo: 1 colocacion, 2 venta garantias, 3 valor sobre costo
$tipo = $_POST['tipo'];
$usu_id = intval($_POST['usu_id']);
$usuario_id_consulta = $usu_id;
$usuarioVendedor_id = intval($_POST['usu_id']);
$tip_per = $_POST['tip_per']; //tipo de personal 1 jefatura, 2 ventas , 3 administrador de sede
$mes = $_POST['mes'];
$anio = $_POST['anio'];

//if($tip_per == 1)
//    $usu_id = '';

$dts = $oUsuario->mostrarUno($usu_id);
if ($dts['estado'] == 1) {
    $empresa_id = $dts['data']['tb_empresa_id']; //sede a la que pertenece el Usuario
}
$dts = NULL;

if ($tip_per == 1) {//jefatura
    $usu_id = '';
    $empresa = 0;
}
if ($tip_per == 2) {//ventas
    $empresa = 0;
}
if ($tip_per == 3) {//administrador de sede
    $usu_id = '';
    $empresa = $empresa_id;
}

if ($tipo == 1) {
    $dtsMenor = $oPlanilla->comision_menor($mes, $anio, $usu_id, $empresa);

    $tabla = '';
    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
    $tabla .= '
		<table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;">
                    <tr id="tabla_cabecera">
                        <th id="tabla_cabecera_fila">TIPO</th>
                        <th id="tabla_cabecera_fila">COLABORADOR</th>
                        <th id="tabla_cabecera_fila">ID CREDITO</th>
                        <th id="tabla_cabecera_fila">CLIENTE</th>
                        <th id="tabla_cabecera_fila">FECHA</th>
                        <th id="tabla_cabecera_fila">MONTO</th>
                        <th id="tabla_cabecera_fila">INTERES</th>
                        <th id="tabla_cabecera_fila">PORCENTAJE</th>
                        <th id="tabla_cabecera_fila">COMISIÓN</th>
                    </tr>';
    $total_comiMenor = 0;
    //WHILE PARA COLOCACION
    if ($dtsMenor['estado'] == 1) {
        foreach ($dtsMenor['data']as $key => $dt) {

            $monto = ($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);

            $dtsComi = $oComision->mostrar_comision_estado_monto($tip_per, 1, 0, $evento, 1, $monto, $mes, $empresa_id);
            if ($dtsComi['data'] == 1) {
                $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                
                
                if ($tip_per == 1) {//jefatura
                    //  $porcen= 0.4;
                    $comi = (($monto * $porcen*0.4) / 100);
                }
                if ($tip_per == 2) {//jefatura
                    //  $porcen= 0.4;
                    $comi = (($monto * $porcen) / 100);
                }
                if ($tip_per == 3) {//administrador de sede
                   //  $porcen= 0.6;
                   $comi = (($monto * $porcen*0.6) / 100);
                }
                
                   //comi = (($monto * $porcen) / 100);
                $total_comiMenor += $comi;
            } else {
                $porcen = (($dt['tb_credito_int'] * $dt['tb_credito_int']) / 100);
                if ($tip_per == 1) {//jefatura
                    //  $porcen= 0.4;
                    $comi = (($monto * $porcen) / 100)*0.4;
                }
                if ($tip_per == 2) {//jefatura
                   $comi = (($monto * $porcen) / 100);
                }
                if ($tip_per == 3) {//administrador de sede
                    //$porcen= 0.6;
                    $comi = (($monto * $porcen) / 100)*0.6;
                }
                
                //$comi = (($monto * $porcen) / 100);
                $total_comiMenor += $comi;
            }
            
            $resu=$oCliente->mostrarUno($dt['tb_cliente_id']);
            if($resu['estado']==1){
                $nombre_cliente=$resu['data']['tb_cliente_nom'];
                //$apellido_cliente=$resu['data'][''];
            }
            $resu2=$oUsuario->mostrarUno($dt['tb_credito_usureg']);
            if($resu2['estado']==1){
                $nombre_colaborador=$resu2['data']['tb_usuario_nom'];
                $apellido_colaborador=$resu['data']['tb_usuario_ape'];
            }

            $tabla .= '
              <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila" align="center">COLOCACIÓN</td>
                    <td id="tabla_fila" align="right"><b>'.$nombre_colaborador.' '.$apellido_colaborador.'&nbsp;&nbsp;&nbsp;</b></td>
                    <td id="tabla_fila" align="center"><b><a  onclick="creditomenor_form(\'L\','.$dt['tb_credito_id'].')">' . $dt['tb_credito_id'] . '</a></b></td>
                    <td id="tabla_fila" align="right"><b>' .$nombre_cliente. '&nbsp;&nbsp;&nbsp;</b></td>
                    <td id="tabla_fila" align="center"><b>' . mostrar_fecha($dt['tb_credito_feccre']) . '</b></td>
                    <td id="tabla_fila" align="center">' . formato_numero($dt['tb_credito_preaco']) . '</td>
                    <td id="tabla_fila" align="center">' . formato_numero($dt['tb_credito_int']) . ' %</td>
                    <td id="tabla_fila" align="center">' . formato_numero($porcen) . ' %</td>
                    <td id="tabla_fila" align="center"><span>' . formato_numero($comi) . '</span></td>
              </tr>';
        }
    }
    $tabla .= '
    	<tr>
            <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="8">TOTAL COMISIONES:</td>
            <td id="tabla_fila" align="center">' . formato_numero($total_comiMenor) . '</td>
    	</tr>
    	</table>';
    echo $tabla;
}

if ($tipo == 2) {
    $dtsVenta = $oPlanilla->comision_ventagarantia($mes, $anio, $usu_id, $empresa);

    $tabla = '';
    $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
    $pre_venta = 0;
    $tabla .= '
		<table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;">
                    <tr id="tabla_cabecera">
                        <th id="tabla_cabecera_fila">ID GARANTIA</th>
                        <th id="tabla_cabecera_fila">TIPO</th>
                        <th id="tabla_cabecera_fila">COLABORADOR</th>
                        <th id="tabla_cabecera_fila">ID CREDITO</th>
                        <th id="tabla_cabecera_fila">CLIENTE</th>
                        <th id="tabla_cabecera_fila">FECHA</th>
                        <th id="tabla_cabecera_fila">MONTO VENTA</th>
                        <th id="tabla_cabecera_fila">MONTO GANANCIA</th>
                        <th id="tabla_cabecera_fila">N° CUOTAS PAGADAS</th>
                        <th id="tabla_cabecera_fila">POR PAGAR</th>
                        <th id="tabla_cabecera_fila">DIFERENCIA</th>
                        <th id="tabla_cabecera_fila">PORCENTAJE</th>
                        <th id="tabla_cabecera_fila">COMISIÓN</th>
                    </tr>';
    $total_comiMenor = 0;
    $colaborador = '';
    //WHILE PARA VENTA DE GARANTÍAS MENORES
    if ($dtsVenta['estado'] == 1) {
        foreach ($dtsVenta['data']as $key => $dt) {

            $results = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], 'COUNT(*)', 2);
            if ($results['estado'] == 1) {
                $cuotas_pagadas = $results['data']['COUNT(*)'];
            }
            $results = NULL;

            if ($cuotas_pagadas < 2) {
                $results2 = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], '*', 1);
                if ($results2['estado'] == 1) {
                    foreach ($results2['data']as $key => $dts) {
                        if ($cuotas_pagadas == 0) {
                            //$suma_pagadas += $dts['tb_cuota_int'] * 2; //intereses a 1 mes
                            $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100) * 2;
                        } else {
                            //$suma_pagadas += $dts['tb_cuota_int']; //intereses a 2 meses
                            $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100);
                        }
                    }
                }
                $results2 = NULL;
            }

            if ($dt['tb_ventagarantia_prec2'] > $dt['tb_ventagarantia_prec1']) {// si el precio de venta por el asesor es mayor aque pide IPDN
                $monto = floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val']); //restamos el precio que pide IPDN menos el valor de la garantia
                $pre_venta = floatval($dt['tb_ventagarantia_prec1']);
            } else {
                if ($dt['tb_ventagarantia_prec2'] <= 0 && $tip_per == 1) {// si el precio que lo vende el asesor es menor o igual a 0 y Tipo personal == 1 Administrador de sede
                    $monto = floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val']);
                    $pre_venta = floatval($dt['tb_ventagarantia_prec1']);
                } else {
                    $monto = floatval($dt['tb_ventagarantia_prec2']) - floatval($dt['tb_garantia_val']);
                    $pre_venta = floatval($dt['tb_ventagarantia_prec2']);
                }
            }

            //si la venta se hace a un colaborador
            if (intval($dt['tb_ventagarantia_col']) != 0) {
                $dts5 = $oUsuario->mostrarUno($dt['tb_ventagarantia_col']);
                if ($dts5['estado'] == 1) {
                    $colaborador_nom = $dts5['data']['tb_usuario_nom'];
                }
                $words = explode(' ', $colaborador_nom);
                $iniciales = $words[0][0] . $words[1][0];
                $colaborador = '<b>(VC) (' . $iniciales . ')</b>';
            }
            //fin de si la venta se hace a un colaborador

            $dtsComi = $oComision->mostrar_comision_estado_monto($tip_per, 1, 0, $evento, 1, $monto, $mes, $empresa_id);

            if ($dtsComi['estado'] == 1) {
                $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                if(intval($dt['tb_usuario_id']) ==$usuarioVendedor_id && intval($dt['tb_ventagarantia_col']) == 0){
                    $porcen=25;
                }
            }

            if ($monto > $suma_pagadas){
                $diferencia = $monto - $suma_pagadas;
            }
            else {
                $diferencia = 0;
            }

            if (intval($_POST['usu_id']) != intval($dt['tb_ventagarantia_col'])) {
                //$comi = number_format(($monto * $porcen) / 100, 2, '.', '');
                $comi = number_format(($diferencia * $porcen) / 100, 2, '.', '');
            } else {
                $comi = 0;
            }
            $total_comiMenor += $comi;
            
            $resu=$oCliente->mostrarUno($dt['tb_cliente_id']);
            if($resu['estado']==1){
                $nombre_cliente=$resu['data']['tb_cliente_nom'];
                //$apellido_cliente=$resu['data'][''];
            }
            $resu2=$oUsuario->mostrarUno($dt['tb_usuario_id']);
            if($resu2['estado']==1){
                $nombre_colaborador=$resu2['data']['tb_usuario_nom'];
                $apellido_colaborador=$resu['data']['tb_usuario_ape'];
            }
            

            $tabla .= '
                  <tr id="tabla_cabecera_fila">
                      <td id="tabla_fila" align="center">'.$dt['tb_garantia_id'].'</td>
                      <td id="tabla_fila" align="center">VENTA GARANTÍAS</td>
                      <td id="tabla_fila" align="right"><b>'.$nombre_colaborador.' '.$apellido_colaborador.'&nbsp;&nbsp;&nbsp;</b></td>
                      <td id="tabla_fila" align="center"><b><a  onclick="creditomenor_form(\'L\','.$dt['tb_credito_id'].')">' . $dt['tb_credito_id'] . '</a></b></td>
                      <td id="tabla_fila" align="center"><b>' .$nombre_cliente.'</b></td>
                      <td id="tabla_fila" align="center"><b>' . mostrar_fecha($dt['tb_ventagarantia_fec']) . '</b></td>
                      <td id="tabla_fila" align="center">' . formato_numero($pre_venta) . ' ' . $colaborador . '</td>
                      <td id="tabla_fila" align="center">' . formato_numero($monto) . '</td>
                      <td id="tabla_fila" align="center"><b>' . $cuotas_pagadas . '</b></td>
                      <td id="tabla_fila" align="center">' . formato_numero($suma_pagadas) . '</td>
                      <td id="tabla_fila" align="center">' . formato_numero($diferencia) . '</td>
                      <td id="tabla_fila" align="center">' . $porcen . ' %</td>
                      <td id="tabla_fila" align="center"><span>' . formato_numero($comi) . '</span></td>
                  </tr>';

            $colaborador = '';
            $suma_pagadas = 0;
        }
    }

    $tabla .= '
    	<tr>
    		<td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="12">TOTAL COMISIONES:</td>
    		<td id="tabla_fila" align="center">' . formato_numero($total_comiMenor) . '</td>
    	</tr>
    	</table>';
    echo $tabla;
}
//3 ES PARA LISTAR EL SOBRE COSTO DE LA VENTA DE GARANTÍAS
if ($tipo == 3) {
    $dtsVenta = $oPlanilla->comision_ventagarantia($mes, $anio, $usu_id, $empresa);

    $tabla = '';
    $evento = 3; //los eventos son 1 poner el credito, 2 vender garantía, 3 venta sobre costo
    $tabla .= '
		<table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;">
                    <tr id="tabla_cabecera">
                        <th id="tabla_cabecera_fila">TIPO</th>
                        <th id="tabla_cabecera_fila">ID CREDITO</th>
                        <th id="tabla_cabecera_fila">FECHA</th>
                        <th id="tabla_cabecera_fila">MONTO</th>
                        <th id="tabla_cabecera_fila">PORCENTAJE</th>
                        <th id="tabla_cabecera_fila">COMISIÓN</th>
                    </tr>';
    $total_comiMenor = 0;
    //WHILE PARA EL SOBRE PRECIO DE VENTA DE GARANTÍAS
    if ($dtsVenta['estado'] == 1) {
        foreach ($dtsVenta['data']as $key => $dt) {
            if ($dt['tb_ventagarantia_com'] > 0) {
                $monSobre = $dt['tb_ventagarantia_com'];

                $dtsComi = $oComision->mostrar_comision_estado_monto($tip_per, 1, 0, $evento, 1, $monSobre, $mes, $empresa_id);

                if ($dtsComi['estado'] == 1) {
                    $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                }

                $comi = number_format(($monSobre * $porcen) / 100, 2, '.', '');
                $total_comiMenor += $comi;

                $tabla .= '
                          <tr id="tabla_cabecera_fila">
                              <td id="tabla_fila" align="center">Monto Sobrecosto</td>
                              <td id="tabla_fila" align="center"><b>' . $dt['tb_credito_id'] . '</b></td>
                              <td id="tabla_fila" align="center"><b>' . mostrar_fecha($dt['tb_ventagarantia_fec']) . '</b></td>
                              <td id="tabla_fila" align="center">' . formato_numero($monSobre) . '</td>
                              <td id="tabla_fila" align="center">' . $porcen . ' %</td>
                              <td id="tabla_fila" align="center"><span>' . formato_numero($comi) . '</span></td>
                          </tr>';
            }
        }
    }

    $tabla .= '
    	<tr>
    		<td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="5">TOTAL COMISIONES:</td>
    		<td id="tabla_fila" align="center">' . formato_numero($total_comiMenor) . '</td>
    	</tr>
    	</table>';
    echo $tabla;
}

//DETALLE DE VENTAS AL CONTADO DE VEHICULOS ASVEH, GARVEH HE HIPOTECARIO
if ($tipo == 4) {
    $monto_total_vehiculos = 0;
    $total_comision_vehiculo = 0;
    $tabla .= '
    <table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;">
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">TIPO CREDITO</th>
            <th id="tabla_cabecera_fila">FECHA</th>
            <th id="tabla_cabecera_fila">ASESOR</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">PLACA</th>
            <th id="tabla_cabecera_fila">VEHICULO</th>
            <th id="tabla_cabecera_fila">PRECIO VENTA</th>
            <th id="tabla_cabecera_fila">TIPO CAMBIO</th>
            <th id="tabla_cabecera_fila">PRECIO FINAL</th>
            <th id="tabla_cabecera_fila">PORCENTAJE COMISION</th>
            <th id="tabla_cabecera_fila">COMISIÓN A PAGAR</th>
        </tr>';
    
    $result_vehiculo = $oPlanilla->lista_venta_vehiculos_garveh_asiveh($mes, $anio, $usu_id, $empresa); //PARA VER LAS VENTAS AL CONTADO TANTO ASIVEH Y GARVEH
        if ($result_vehiculo['estado'] == 1) {
            foreach ($result_vehiculo['data']as $key => $venta) {
                
                $simbolo_moneda = 'S/.';
                $precio_venta = formato_moneda($venta['tb_ventavehiculo_mon']);
                if($venta['tb_moneda_id'] == 2){
                    $simbolo_moneda = 'US$';
                    $precio_venta = $precio_venta * $venta['tb_ventavehiculo_tipcam'];
                }
                $monto_total_vehiculos += formato_moneda($precio_venta);

                //obtenemos los datos del vehiculo mediante su placa, consultamos a tablas de garveh y asiveh
                $marca_vehiculo = 'NaN'; $modelo_vehiculo = 'NaN'; $anio_vehiculo = 'NaN';
                $result_busca = $oBuscaveh->buscar_placa_garveh_asiveh(trim($venta['tb_vehiculo_pla']));
                    if($result_busca['estado'] == 1){
                        $marca_vehiculo = $result_busca['data']['tb_vehiculomarca_nom']; 
                        $modelo_vehiculo = $result_busca['data']['tb_vehiculomodelo_nom']; 
                        $anio_vehiculo = $result_busca['data']['tb_credito_vehano']; 
                    }
                $result_busca = NULL;
                
                //obtenemos el valor de la comisión por cada venta de vehículo
                $cretip = $venta['tb_creditotipo_id'];
                $cre_asveh = ''; //valor vacío no es relevante ahora
                $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
                //$valores = 'tip per: '.$tip_per.' / cre tip: '.$cretip.' /cre asive: '.$cre_asveh.' /evento: '.$evento.' / esta1 / pre vent: '.$precio_venta.' /mes: '.$mes.' / empre: '.$empresa_id;
                $result_comision = $oComision->mostrar_comision_estado_monto($tip_per, $cretip, $cre_asveh, $evento, 1, $precio_venta, $mes, $empresa_id);
                    if ($result_comision['estado'] == 1) {
                        
                        $porcen = $result_comision['data']['tb_comisiondetalle_porcen'];
                        /*el jefe de sede tiene por defecto 0.5 de comision por venta de vehiculo, pero si el jefe de sede vende por si solo
                        entonces debe ganar el 1% de cada venta*/
                        if($venta['tb_usuario_id2'] == $usuario_id_consulta && $tip_per == 3){
                            $porcen = 1; //esto indica que si jefe de sede vende vehiculo deja de ganar 0.5 para ganar el 1 %
                        }
                        
                        $comision = (($precio_venta * $porcen) / 100);
                        $total_comision_vehiculo += $comision;
                    } 
                    else {
                        $porcen = 'Auto';
                    }
                $result_comision = NULL;

                $tabla .= '
                <tr>
                    <td id="tabla_fila" align="center">' . $venta['tipo_credito'] . '</td>
                    <td id="tabla_fila" align="center">' . mostrar_fecha($venta['tb_ventavehiculo_fec']) . '</td>
                    <td id="tabla_fila" align="center">' . $venta['tb_usuario_nom'] . '</td>
                    <td id="tabla_fila" align="center">' . $venta['tb_cliente_nom'] . '</td>
                    <td id="tabla_fila" align="center">' . $venta['tb_vehiculo_pla'] . '</td>
                    <td id="tabla_fila" align="center">' . $marca_vehiculo.' | '.$modelo_vehiculo.' | '.$anio_vehiculo . '</td>
                    <td id="tabla_fila" align="center">' . $simbolo_moneda.' '.formato_numero($venta['tb_ventavehiculo_mon']) . '</td>
                    <td id="tabla_fila" align="center">' . $venta['tb_ventavehiculo_tipcam'] . '</td>
                    <td id="tabla_fila" align="center">' . ''.formato_numero($precio_venta) . '</td>
                    <td id="tabla_fila" align="center">' . $porcen.' % '.$valores.'</td>
                    <td id="tabla_fila" align="center">' . ''.formato_numero($comision) . '</td>
                </tr>';
            }
        }
    $result_vehiculo = NULL;
    $tabla .= '
      <tr>
        <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="10">TOTAL VENTAS:</td>
        <td id="tabla_fila" align="center">' . formato_numero($total_comision_vehiculo) . '</td>
      </tr>
      </table>';
    echo $tabla;
}
if ($tipo == 5) {
    $monto_totalInmueble = 0;
    $tabla .= '
    <table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;">
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">CRED. ID</th>
            <th id="tabla_cabecera_fila">FECHA</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">DIRECCION</th>
            <th id="tabla_cabecera_fila">MONTO</th>
            <th id="tabla_cabecera_fila">PORCENTAJE</th>
            <th id="tabla_cabecera_fila">COMISION</th>
        </tr>';

    $dtsVentaInmu = $oPlanilla->comision_ventavehiculo($mes, $anio, $usu_id, '', $empresa); //PARA VER LAS VENTAS AL CONTADO TANTO ASIVEH Y GARVEH
    if ($dtsVentaInmu['estado'] == 1) {
        foreach ($dtsVentaInmu['data']as $key => $dt) {

            $monto = floatval($dt['tb_stockinmueble_pag']);
            //if($dt['tb_moneda_id'] == 2)
            //$monto = floatval($dt['tb_ventavehiculo_mon']) * floatval($dt['tb_ventavehiculo_tipcam']);
            $comision = number_format(($monto * 1) / 100, 2, '.', '');
            $monto_totalInmueble += $comision;
            $tabla .= '
              <tr>
                <td id="tabla_fila" align="center">' . $dt['tb_credito_id'] . '</td>
                <td id="tabla_fila" align="center">' . mostrar_fecha($dt['tb_stockinmueble_fec']) . '</td>
                <td id="tabla_fila" align="center">' . $dt['tb_cliente_nom'] . '</td>
                <td id="tabla_fila" align="center">' . $dt['tb_credito_ubica'] . '</td>
                <td id="tabla_fila" align="center">' . formato_numero($monto) . '</td>
                <td id="tabla_fila" align="center">1%</td>
                <td id="tabla_fila" align="center">' . formato_numero($comision) . '</td>
              </tr>';
        }
    }
    $tabla .= '
      <tr>
        <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="6">TOTAL VENTAS:</td>
        <td id="tabla_fila" align="center">' . formato_numero($monto_totalInmueble) . '</td>
      </tr>
      </table>';
    echo $tabla;
}
?>


<div class="modal fade" tabindex="-1" role="dialog" id="div_planilla_det_cred_modal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #003eff">DETALLES DE CRÉDITOS</h4>
            </div>
            <form id="detalle_comisiones">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo $tabla; ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/planilla/planilla_detalle_credito.js?ver=11222'; ?>"></script>