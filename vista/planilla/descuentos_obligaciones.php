
 <?php

        $SUELDO_TOTAL = number_format(floatval($sueldo_bruto) + floatval($sueldo_comision), 2);
        $sueldo_bruto = floatval($sueldo_bruto);
        $sueldo_comision = floatval($sueldo_comision);

        $SUELDO_TOTAL = moneda_mysql($SUELDO_TOTAL);
      ?>

<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">OBLIGACIONES</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <strong style="font-family: cambria;font-weight: bold;color: green;">SUELDO BRUTO SIN COMISIONES: S/: <span><?php echo mostrar_moneda($sueldo_bruto); ?></span></strong>
        <br>
        <table style="width: 100%;font-family: cambria;" class="table-hover">
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">ENTIDAD</th>
                <th id="tabla_cabecera_fila">APORTE</th>
                <th id="tabla_cabecera_fila">PRIMA</th>
                <th id="tabla_cabecera_fila">COMI.</th>
                <th id="tabla_cabecera_fila">TOTAL</th>
            </tr>
            <?php
            $retencion = 0;

            //ahora hay nueva forma de calcular AFP, si el usuario eleigió por todo su sueldo o solo su base
            $tipo_sueldo = 1; // 1 el AFP solo se aplica al sueldo base, 2 se aplica a todo con comisiones
            $sueldo_para_afp = $sueldo_bruto;
            $mes_anio = $mes . '-' . $anio;
            $result = $oUsuario->sueldoforma_usuario_fecha($usu_id, $mes_anio);
            
            if ($result['estado']==1) {
                $tipo_sueldo = intval($result['data']['tb_sueldoforma_tip']);
            }
            
            if ($tipo_sueldo == 2) {
                echo '<strong style="font-family: cambria;font-weight: bold;color: blue;">El colaborador eligió pagar AFP con su Sueldo Total.</strong>';
                $sueldo_para_afp = $sueldo_bruto + $sueldo_comision;
            } else
                echo '<strong style="font-family: cambria;font-weight: bold;color: brown;">El colaborador eligió pagar AFP solo con su Sueldo Base.</strong>';

            if ($usu_afp > 0) {
                //$sueldo_neto = $total_sueldo_base - $penalidad_tar;
                //$retencion += $ret;
                $afp_aporte = floatval($sueldo_para_afp) * floatval($aporte_afp) / 100;
                $afp_prima = floatval($sueldo_para_afp) * floatval($prima_afp) / 100;
                $afp_comision = floatval($sueldo_para_afp) * floatval($comision_afp) / 100;

                if($sueldo_para_afp > 11983.66)
                  $afp_prima = 203.72;

                $ret = $afp_aporte + $afp_prima + $afp_comision;
                $retencion += $ret;

                echo '
                <tr>
                  <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . $seguro_afp_nom . '</td>
                  <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($afp_aporte) . ' (' . $aporte_afp . ')</td>
                  <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($afp_prima) . ' (' . $prima_afp . ')</td>
                  <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($afp_comision) . ' (' . $comision_afp . ')</td>
                  <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($ret) . '</td>
                </tr>';

                //para los que stan en COMISION MIXTA, se les cobra todos los diciembres una comision mixta
                if (intval($mes) == 12) {
                    $afp_comision_mixta = floatval($sueldo_para_afp) * floatval($comision_mixta_afp) / 100;
                    $ret = $afp_comision_mixta;
                    $retencion += $ret;

                    echo '
                  <tr>
                    <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . $seguro_afp_nom . '</td>
                    <td  id="tabla_fila" align="center" colspan="2">COMISION MIXTA DICIEMBRE</td>
                    <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($afp_comision_mixta) . ' (' . $comision_mixta_afp . ')</td>
                    <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($ret) . '</td>
                  </tr>';
                }
            }
            if ($usu_eps > 0) {
                $ret = floatval($aporte_eps);
                $retencion += $ret;
                echo '
                <tr>
                  <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . $seguro_eps_nom . '</td>
                  <td id="tabla_fila" style="font-family:cambria;font-weight: bold">' . $aporte_eps . '</td>
                  <td id="tabla_fila" align="center">0.00</td>
                  <td id="tabla_fila" align="center">0.00</td>
                  <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($ret) . '</td>
                </tr>';
            }

            //$SUELDO_TOTAL = floatval($SUELDO_TOTAL) - floatval($retencion);
            $SUELDO_TOTAL = floatval($sueldo_bruto) - floatval($retencion) + $sueldo_comision;
            ?>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="4"  align="center">TOTAL OBLIGACIONES:</td>
                <td id="tabla_fila" align="center"><?php echo 'S/. ' . mostrar_moneda($retencion, 2); ?></td>
            </tr>
        </table>
        <br>
        
        <table style="width: 100%;font-family: cambria;" class="table-hover">  
            <tr style="height: 30px">  
            <td id="tabla_fila">
              <input type="hidden" id="hdd_sueldo" value="<?php echo moneda_mysql(number_format($sueldo_bruto, 2));?>">
              <input type="hidden" id="hdd_comision" value="<?php echo moneda_mysql(number_format($sueldo_comision, 2));?>">
              

              <strong style="color: green;">Finalmente el sueldo a Neto a pagar es: S/: <span id="span_recibe" style="font-size: 20px"><?php echo number_format($SUELDO_TOTAL, 2);?></span></strong>
            </td>
            <!--<td id="tabla_fila" align="center"><a class="btn_sueldo btn btn-danger btn-xs" onclick="pagar_colaborador_form('sueldo',<?php echo $usu_id;?>)" title="Pagar Sueldo"><i class="fa fa-money"></i></a></td>-->
          </tr>
        </table>
        
    </div>
</div>