<?php
require_once("../../core/usuario_sesion.php");

require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../menorgarantia/Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../proveedor/Proveedor.class.php');
$oProveedor = new Proveedor();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once('../planilla/Planilla.class.php');
$oPlanilla = new Planilla();
require_once('../formulacomision/Formulacomision.class.php');
$oComision = new Formulacomision();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../vehiculomarca/Vehiculomarca.class.php');
$oVehMarc = new Vehiculomarca();
require_once('../vehiculomodelo/Vehiculomodelo.class.php');
$oVehMod = new Vehiculomodelo();
require_once('../vehiculoclase/Vehiculoclase.class.php');
$oVehCla = new Vehiculoclase();
require_once('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$usu_id = $_POST['usu_id'];

$monto = $_POST['monto_des'];
$monto_saldo = $_POST['monto_saldo'];
$sueldo_bruto = moneda_mysql($_POST['sueldo_bruto']);
$sueldo_comision = moneda_mysql($_POST['sueldo_comision']);
$tipo = $_POST['tipo'];
$detalle =  mb_strtoupper($_POST['txt_detalle']) . ' [ ' . mb_strtoupper($_POST['txt_cuopag_obs']) . ' ]';

$mes = $_POST['mes'];
$anio = $_POST['anio'];
$fecha_hoy = date('d-m-Y');

if ($action == 'sueldo' || $action == 'comision') {
  if (!empty($usu_id) && !empty($monto)) {

    $monto_com = 0;
    $dts = $oVentainterna->listar_ventainterna($usu_id, $mes, $anio);
      $rows = count($dts['data']);
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $monto_com += $dt['tb_ventainterna_mon'];
        }
      }
    $dts = NULL;

    $dts = $oUsuario->mostrarUno($usu_id);
      if ($dts['estado'] == 1) {
        $usu_nom = $dts['data']['tb_usuario_nom'] . ' ' . $dts['data']['tb_usuario_ape'];
        $usu_dni = $dts['data']['tb_usuario_dni'];
      }
    $dts = NULL;

    $dts = $oProveedor->mostrar_por_dni($usu_dni);
      if ($dts['estado'] == 1) {
        $pro_id = $dts['data']['tb_proveedor_id'];
      }
    $dts = NULL;
    
    if (empty($pro_id)) {
      $data['est'] = 0;
      $data['msj'] = 'EL colaborador no está registrado como proveedor, revise por favor.';
      echo json_encode($data);
      exit();
    }

    $xac = 1;
    $numdoc = "";
    $compras = '';
    if ($rows > 0)
      $compras = '. Tiene ' . $rows . ' compras, monto total de: S/. ' . mostrar_moneda($monto_com);

    $array_mes = array(
      '01' => 'ENERO', '02' => 'FEBRERO', '03' => 'MARZO', '04' => 'ABRIL', '05' => 'MAYO', '06' => 'JUNIO', '07' => 'JULIO',
      '08' => 'AGOSTO', '09' => 'SEPTIEMBRE', '10' => 'OCTUBRE', '11' => 'NOVIEMBRE', '12' => 'DICIEMBRE'
    );

    $det = 'PAGO DE HABERES MENSUALES DEL MES DE ' . $array_mes[$mes] . ' DEL ' . $anio . ' AL COLABORADOR ' . $usu_nom . '. Su sueldo bruto es: ' . mostrar_moneda($sueldo_bruto) . ' y su sueldo de comision es: ' . mostrar_moneda($sueldo_comision);
    if (moneda_mysql($monto) < moneda_mysql($monto_saldo)) {
      $det = 'ADELANTO DE PAGO DE HABERES MENSUALES DEL MES DE ' . $array_mes[$mes] . ' DEL ' . $anio . ' AL COLABORADOR ' . $usu_nom . '. Su sueldo bruto es: ' . mostrar_moneda($sueldo_bruto) . ' y su cueldo de comision es: ' . mostrar_moneda($sueldo_comision);
    }
    $det .= $det . ' // <b style="color: #0000cc">' . $detalle . '</b> // Monto Desembolsado es de: <b>' . $monto . '</b>';
    $cuenta = 11;
    $subcuenta = 35;

    $modide = $anio . $mes; //el modid sera la union del anio y mes para poder consultar y no volver a pagar el mismo mes

    /* GERSON (18-12-23) */
    $comision_usuario_id = $usu_id;
    $usuario = $oUsuario->mostrarUno($usu_id);
    $empresa_id = 0;
    $tip_per = 0;
    if ($usuario['estado'] == 1) {
      $empresa_id = intval($usuario['data']['tb_empresa_id']);
      $tip_per = intval($usuario['data']['tb_usuario_per']);
    }

    if ($tip_per == 1) { //jefatura
      $usu_id = '';
      $empresa = 0;
    }
    if ($tip_per == 2) { //ventas
      $empresa = 0;
    }
    if ($tip_per == 3) { //administrador de sede
      $usu_id = '';
      $empresa = $empresa_id;
    }

    $json_comision = '{';

    $dtsAsveh = $oPlanilla->comision_asiveh_new($mes, $anio, $usu_id, $empresa);
    $dtsGarveh = $oPlanilla->comision_garveh_new($mes, $anio, $usu_id, $empresa);
    $dtsHipo = $oPlanilla->comision_hipo_new($mes, $anio, $usu_id, $empresa);
    $dtsMenor = $oPlanilla->comision_menor($mes, $anio, $usu_id, $empresa);
    $dtsVenta = $oPlanilla->comision_ventagarantia($mes, $anio, $usu_id, $empresa);

    // ASISTENCIA VEHICULAR 
    $perso = $tip_per; //según tipo de personal del usuario
    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $total_comiAsveh = 0;
    $MONTO_COLOCADO_ASIVEH = 0;

    $json_comision .= '"asveh":[';

    if (!empty($dtsAsveh['data'])) {
      foreach ($dtsAsveh['data'] as $key => $dt) {
        $tipo = '----';
        $subTip = '';
        $moneda = 'S/.';
        if ($dt['tb_moneda_id'] == 2)
          $moneda = 'US$';
        if ($dt['tb_credito_tip1'] == 1)
          $tipo = "VENTA NUEVA ";
        if ($dt['tb_credito_tip1'] == 2)
          $tipo = "ADENDA ";
        if ($dt['tb_credito_tip1'] == 4)
          $tipo = "RE-VENTA ";

        if ($dt['tb_credito_tip2'] == 1)
          $subTip = "CREDITO REGULAR";
        if ($dt['tb_credito_tip2'] == 4)
          $subTip = "CUOTA BALON";

        if ($dt['tb_credito_tip1'] == 1 || $dt['tb_credito_tip1'] == 4) {
          $cretip = 2; //credito asiveh es de tipo 2
          $cre_asveh = $dt['tb_credito_tip1']; //obtenemos el porcentaje para venta nueva o reventa
        }
        if ($dt['tb_credito_tip1'] == 2) {
          $cretip = 5; //para la formula de comision las adendas son de creditotipo_id = 5
          $cre_asveh = 0; //ya que es adenda no tiene tipo de asveh, valor 0
        }

        $monto_comi = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];
        $MONTO_COLOCADO_ASIVEH += $monto_comi;

        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);
        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }

        $comi = moneda_mysql(($monto_comi * $porcen) / 100);
        $total_comiAsveh += $comi;

        $json_comision .= '{';

        $json_comision .= '"moneda": "' . $moneda . ')",';
        $json_comision .= '"tipo": ' . $tipo . $subTip . '"(CAV-' . $dt['tb_credito_id'] . ')",';
        $json_comision .= '"precio_acordado": ' . mostrar_moneda($dt['tb_credito_preaco']) . ',';
        $json_comision .= '"tipo_cambio": "' . $dt['tb_credito_tipcam'] . '",';
        $json_comision .= '"porcentaje": "' . $porcen . ' %",';
        $json_comision .= '"comision": ' . mostrar_moneda($comi);

        $json_comision .= '},';
      }
      $json_comision = substr($json_comision, 0, -1); // se leimina la coma del último valor
    }
    $json_comision .= '],';


    // GARANTIA VEHICULAR
    $perso = $tip_per; //según tipo de personal del usuario
    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $total_comiGarveh = 0;
    $sumaSoles = 0;
    $sumaDolares = 0;
    $MONTO_COLOCADO_GARVEH = 0;

    $json_comision .= '"garveh":[';

    if (!empty($dtsGarveh['data'])) {
      foreach ($dtsGarveh['data'] as $key => $dt) {

        if ($dt['tb_moneda_id'] == 1) {
          $moneda = 'S/.';
          $sumaSoles += $dt['tb_credito_preaco'];
        }
        if ($dt['tb_moneda_id'] == 2) {
          $moneda = 'US$';
          $sumaDolares += $dt['tb_credito_preaco'];
        }

        $tipo_gar = 'GARANTÍA VEHICULAR';
        $cretip = 3; //credito garveh es de tipo 3
        $cre_asveh = 0; //como es garveh no tiene venta nueva o reventa, entonces tiene valor 0

        if ($dt['tb_credito_tip'] == 2) {
          $tipo_gar = "ADENDA VEHICULAR";
          $cretip = 5; //para todas las adendas de los creditos, adenda tiene como creditotipo_id = 5
        }

        $valido = '';
        if (moneda_mysql($dt['tb_credito_valido']) > 0) {
          $monto_comi = $dt['tb_credito_valido'] * $dt['tb_credito_tipcam'];
          $valido = '| <b>' . mostrar_moneda($dt['tb_credito_valido']) . '</b>';
        } else
          $monto_comi = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];

        $MONTO_COLOCADO_GARVEH += $monto_comi;

        $porcen = 0;
        $formula_si = '(falta formula)';
        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);
        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
          $formula_si = '';
        }
        $dtsComi = NULL;

        if ($tip_per == 1) {
          if ($dt['tb_empresa_id'] == 2) {
            if ($dt['tb_credito_usureg'] == 2) {
              $porcen = mostrar_moneda(0.8);
            } else {
              $porcen = mostrar_moneda(1);
            }
          }
        }
        if ($tip_per == 3) {
          if ($usu_id == $dt['tb_credito_usureg']) {
            $porcen = mostrar_moneda(1.20);
          } else {
            $porcen = 0;
          }
        }
        if ($tip_per == 2 && $empresa_id == 2) { //personal ventas que pertenezca a mall, si logra interés del 5% 0.3+
          if (intval($dt['tb_credito_int']) == 5)
            $porcen = $porcen + 0.3;
        }


        $Cliente = $oCliente->mostrarUno($dt['tb_cliente_id']);
        if ($Cliente['estado'] == 1) {
          $nombre = $Cliente['data']['tb_cliente_nom'];
        }
        $VehMarc = $oVehMarc->mostrarUno($dt['tb_vehiculomarca_id']);
        if ($VehMarc['estado'] == 1) {
          $marca = $VehMarc['data']['tb_vehiculomarca_nom'];
        }
        $VehMod = $oVehMod->mostrarUno($dt['tb_vehiculomodelo_id']);
        if ($VehMod['estado'] == 1) {
          $modelo = $VehMod['data']['tb_vehiculomodelo_nom'];
        }
        $VehCla = $oVehCla->mostrarUno($dt['tb_vehiculoclase_id']);
        if ($VehCla['estado'] == 1) {
          $clase = $VehCla['data']['tb_vehiculoclase_nom'];
        }

        $oUser = $oUsuario->mostrarUno($dt['tb_credito_usureg']);
        if ($oUser['estado'] == 1) {
          $asesor = $oUser['data']['tb_usuario_nom'];
        }

        $comi = (($monto_comi * $porcen) / 100);
        $total_comiGarveh += $comi;

        $json_comision .= '{';

        $json_comision .= '"credito_id": "' . $dt['tb_credito_id'] . '",';
        $json_comision .= '"tipo": "' . $tipo_gar . ' (CGV-' . $dt['tb_credito_id'] . ') - [' . $nombre . ']",';
        $json_comision .= '"marca": "' . $marca . ' / ' . $modelo . ' / ' . $clase . '",';
        $json_comision .= '"asesor": "' . $asesor . '",';
        $json_comision .= '"moneda": "' . $moneda . '",';
        $json_comision .= '"valido": "' . $valido . '",';
        $json_comision .= '"precio_acordado": ' . $dt['tb_credito_preaco'] . ',';
        $json_comision .= '"interes": "' . $dt['tb_credito_int'] . '",';
        $json_comision .= '"tipo_cambio": "' . $dt['tb_credito_tipcam'] . '",';
        $json_comision .= '"porcentaje": "' . $porcen . ' %' . $formula_si . '",';
        $json_comision .= '"comision": ' . mostrar_moneda($comi);

        $json_comision .= '},';
      }
      $json_comision = substr($json_comision, 0, -1); // se leimina la coma del último valor
    }
    $json_comision .= '],';

    // HIPOTECARIO
    $perso = $tip_per; //según tipo de personal del usuario 1 jefa, 2 ventas
    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $cretip = 4; //el creditotipo_id de hipotecario es 4
    $cre_asveh = 0; //hipotecario no tiene venta nueva ni reventa
    $total_comiHipo = 0;
    $MONTO_COLOCADO_HIPO = 0;

    $json_comision .= '"hipo":[';

    if (!empty($dtsHipo['data'])) {
      foreach ($dtsHipo['data'] as $key => $dt) {
        $moneda = 'S/.';
        if ($dt['tb_moneda_id'] == 2)
          $moneda = 'US$';

        $usu_reg = $dt['tb_credito_usureg']; //id del usuario que registra el credito

        $valido = '';
        if (moneda_mysql($dt['tb_credito_valido']) > 0) {
          $monto_comi = $dt['tb_credito_valido'] * $dt['tb_credito_tipcam'];
          $valido = '| <b>' . mostrar_moneda($dt['tb_credito_valido']) . '</b>';
        } else
          $monto_comi = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];

        $MONTO_COLOCADO_HIPO += $monto_comi;

        //solo para hipotecario si el credito es registrado por jefatura gana 1%, vendedor gana 0.5%, jefatura por vendedor gana 0.5%, ANTES ERA TIP_PER_USUREG, PERO AHORA SIGUE LA MISMA REALGA QUE GARVEH
        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);

        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }

        $comi = formato_numero(($monto_comi * $porcen) / 100);
        $total_comiHipo += $comi;

        $json_comision .= '{';

        $json_comision .= '"moneda": "' . $moneda . '",';
        $json_comision .= '"tipo": "CREDITO HIPOTECARIO (CH-' . $dt['tb_credito_id'] . ')",';
        $json_comision .= '"precio_acordado": "' . mostrar_moneda($dt['tb_credito_preaco']) . ' ' . $valido . '",';
        $json_comision .= '"tipo_cambio": "' . $dt['tb_credito_tipcam'] . '",';
        $json_comision .= '"porcentaje": "' . $porcen . ' %",';
        $json_comision .= '"comision": ' . mostrar_moneda($comi);

        $json_comision .= '},';
      }
      $json_comision = substr($json_comision, 0, -1); // se leimina la coma del último valor
    }
    $json_comision .= '],';

    // CREDITO MENOR
    $perso = $tip_per; //según tipo de personal del usuario
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $cretip = 1; //el creditotipo_id de MENOR es 1
    $cre_asveh = 0; //hipotecario no tiene venta nueva ni reventa
    $total_comiMenor = 0;
    $comision_colocacion_cmenor = 0;
    $comision_venta_cmenor = 0;
    $cant = 0;
    $monto_comi = 0;
    $comi = 0;
    $rowsColoca = count($dtsMenor['data']);
    $MONTO_COLOCADO_CMENOR = 0;
    $MONTO_VENDIDO_CMENOR = 0;

    $json_comision .= '"cmenor":[{';

    if ($dtsMenor['estado'] == 1) {
      foreach ($dtsMenor['data'] as $key => $dt) {

        $monto_comi += ($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);
        $pre_aco = ($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);
        $porcenAuto = ($dt['tb_credito_int'] * $dt['tb_credito_int']) / 100;

        $MONTO_COLOCADO_CMENOR += $pre_aco;

        if ($perso == 1) {
          //$porcenAuto=0.4;
          $comi += (($pre_aco * $porcenAuto * 0.4) / 100);
        }
        if ($perso == 2) {
          //$porcenAuto=0.4;
          $comi += (($pre_aco * $porcenAuto) / 100);
        }
        if ($perso == 3) {
          //$porcenAuto=0.6;
          $comi += (($pre_aco * $porcenAuto * 0.6) / 100);
        }
      }

      $total_comiMenor += $comi;
      $comision_colocacion_cmenor = $comi;

      $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía

      $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);
      if ($dtsComi['estado'] == 1) {
        $total_comiMenor = 0;
        $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        $comi = (($monto_comi * $porcen) / 100);
        $total_comiMenor += $comi;
        $comision_colocacion_cmenor = $comi;
      } else {
        $porcen = 'Auto';
      }

      $json_comision .= '"moneda": "S/ .",';
      $json_comision .= '"tipo": "COLOCACIÓN",';
      $json_comision .= '"cantidad": "' . $rowsColoca . '",';
      $json_comision .= '"monto_total": "' . mostrar_moneda($monto_comi) . '",';
      $json_comision .= '"porcentaje": "' . $porcen . ' %",';
      $json_comision .= '"comision": ' . mostrar_moneda($comi);
    }
    $json_comision .= '},';


    $diferencia = 0;
    //WHILE PARA VENTA DE GARANTÍAS MENORES
    $rowsVenta = count($dtsVenta['data']);
    $monto_comi = 0;
    $monSobre = 0;
    $monto_total = 0;
    $cantSobre = 0;
    $resta1 = 0;

    $json_comision .= '{';

    if ($dtsVenta['estado'] == 1) {
      foreach ($dtsVenta['data'] as $key => $dt) {
        $usuarioventa = intval($dt['tb_usuario_id']);
        if ($usuarioventa == $usu_id || $tip_per == 1 || $tip_per == 3) {
          $resta1 += floatval($dt['tb_ventagarantia_prec2']);
        }

        $MONTO_VENDIDO_CMENOR += $dt['tb_ventagarantia_prec2'];

        /* consulta cuantas cuotas se han realizado de pagos del credito 
                            * si se han hecho mas de 2 cuotas no acumula saldo
                            * se se ha hecho 1 o 0 cuotas de pago acumula la cuota para proceder con el descuento                    
                            *  */
        $results = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], 'COUNT(*)', 2);
        if ($results['estado'] == 1) {
          $cuotas_pagadas = $results['data']['COUNT(*)'];
        }
        $results = NULL;

        if ($cuotas_pagadas < 2) {
          $results2 = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], '*', 1);
          if ($results2['estado'] == 1) {
            foreach ($results2['data'] as $key => $dts) {
              if ($cuotas_pagadas == 0) {
                //$suma_pagadas += $dts['tb_cuota_int'] * 2; //intereses a 2 mes
                $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100) * 2; //intereses a 2 mes
              } else {
                //$suma_pagadas += $dts['tb_cuota_int']; //intereses a 1 meses
                $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100);
              }
            }
          }
          $results2 = NULL;
        }
        /* fin de consulta de cuotas */


        if (intval($usu_id) != intval($dt['tb_ventagarantia_col'])) {
          if ($dt['tb_ventagarantia_com'] > 0) {
            $monSobre += $dt['tb_ventagarantia_com'];
            $cantSobre++;
          }

          if ($dt['tb_ventagarantia_prec2'] > $dt['tb_ventagarantia_prec1']) { // si el precio de venta por el asesor es mayor aque pide IPDN

            $resta = floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val']) - $suma_pagadas; //restamos el precio que pide IPDN menos el valor de la garantia
            if ($resta > 0) {
              $monto_comi += $resta;
              $monto_total += $resta;
            }
          } else {
            if ($dt['tb_ventagarantia_prec2'] <= 0 && $perso == 1) { //jefatura
              $resta = (floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val'])) - $suma_pagadas; //precio que pide IPDN menos valor d ela garntia
              if ($resta > 0) {
                $monto_comi += $resta;
                $monto_total += $resta;
              }
            } else {
              $resta = (floatval($dt['tb_ventagarantia_prec2']) - floatval($dt['tb_garantia_val'])) - $suma_pagadas;
              if ($resta > 0) {
                $monto_comi += $resta;
                $monto_total += $resta;
              }
            }
          }
        }



        $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);

        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }
        $dtsComi = NULL;

        if (intval($dt['tb_usuario_id']) == $usuarioVendedor_id && intval($dt['tb_ventagarantia_col']) == 0) {
          $porcen = 25;
        }

        $comi = (($monto_comi * $porcen) / 100);

        $comision_total += $comi;
        $total_comiMenor += $comi;

        $monto_comi = 0;
        $porcen = 0;
        $suma_pagadas = 0;
      }

      $comision_venta_cmenor += $comision_total;

      $json_comision .= '"moneda": "S/ .",';
      $json_comision .= '"tipo": "VENTA GARANTÍAS",';
      $json_comision .= '"cantidad": "' . $rowsVenta . '",';
      $json_comision .= '"monto_total": "' . mostrar_moneda($resta1) . '",';
      $json_comision .= '"porcentaje": "Auto %",';
      $json_comision .= '"comision": ' . mostrar_moneda($comision_total);
    }
    $json_comision .= '},';


    $json_comision .= '{';

    //WHILE PARA EL SOBRE PRECIO DE VENTA DE GARANTÍAS
    if ($cantSobre > 0) {
      $evento = 3; //los eventos son 1 poner el credito, 2 vender garantía, 3 venta sobre costo
      $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monSobre, $mes, $empresa_id);

      if ($dtsComi['estado'] == 1) {
        $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
      }

      $comi = (($monSobre * $porcen) / 100);

      $total_comiMenor += $comi;
      $comision_venta_cmenor += $comi;

      $json_comision .= '"moneda": "S/ .",';
      $json_comision .= '"tipo": "VALOR SOBRE COSTO",';
      $json_comision .= '"cantidad": "' . $cantSobre . '",';
      $json_comision .= '"monto_total": "' . mostrar_moneda($resmonSobreta1) . '",';
      $json_comision .= '"porcentaje": "' . $porcen . ' %",';
      $json_comision .= '"comision": ' . mostrar_moneda($comi);
    }
    $json_comision .= '}';


    $json_comision .= "]}";

    /*  */

    if ($tipo == 1) {

      $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
      $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
      $oEgreso->egreso_fec = fecha_mysql($fecha_hoy);
      $oEgreso->documento_id = 9;
      $oEgreso->egreso_numdoc = $numdoc;
      $oEgreso->egreso_det = $det;
      $oEgreso->egreso_imp = moneda_mysql($monto);
      $oEgreso->egreso_tipcam = 0;
      $oEgreso->egreso_est = 1;
      $oEgreso->cuenta_id = $cuenta;
      $oEgreso->subcuenta_id = $subcuenta;
      $oEgreso->proveedor_id = $pro_id;
      $oEgreso->cliente_id = 0;
      $oEgreso->usuario_id = 0;
      $oEgreso->caja_id = 1;
      $oEgreso->moneda_id = 1;
      $oEgreso->modulo_id = 64;
      //                $oEgreso->modulo_id = $mod_id;
      $oEgreso->egreso_modide = intval($modide);
      $oEgreso->empresa_id = $_SESSION['empresa_id'];
      $oEgreso->egreso_ap = 0; //si
      //                $oEgreso->insertar();
      $result = $oEgreso->insertar();
      if (intval($result['estado']) == 1) {
        $egr_id = $result['egreso_id'];
      }
      //                echo 'hasta aki esta entrabdo normal al sistema';exit();

      $oEgreso->modificar_campo($egr_id, 'tb_egreso_numdoc', $_SESSION['usuario_id'], 'STR');
      $oEgreso->modificar_campo($egr_id, 'tb_egreso_detalleplanilla', $detalle, 'STR');

      $data['est'] = 1;
      $data['msj'] = 'Se ha generado el egreso correctamente';
      echo json_encode($data);
    }
    if ($tipo == 2) {

      $fec_pago = $_POST['txt_cuopag_fec'];

      $numope = trim($_POST['cuopag_numope']);
      $monto_validar = moneda_mysql($_POST['txt_cuopag_mon']); //elimina las comas y devuelve con 2 decimales
      $moneda_deposito_id = 1; //1 soles, 2 dolares
      $moneda_credito_id = 1; //1 soles, 2 dolares

      $deposito_mon = 0;
      $dts = $oDeposito->validar_num_operacion($numope);
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $deposito_mon += floatval($dt['tb_deposito_mon']);
        }
      }

      if ($deposito_mon == 0) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
        echo json_encode($data);
        exit();
      }

      $ingreso_importe = 0;
      $dts = $oIngreso->lista_ingresos_num_operacion($numope);
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
          $ingreso_mon_id = intval($dt['tb_moneda_id']);
          $ingreso_importe += $tb_ingreso_imp;
        }
      }

      $monto_usar = abs($deposito_mon) - $ingreso_importe;
      $monto_usar = moneda_mysql($monto_usar);
      //echo 'tooo esto hay een importes: '.$ingreso_importe.' // depo: '.$deposito_mon.' // validar: '.$monto_validar.' // usar: '.$monto_usar.' // moneda ing: '.$ingreso_mon_id.' // tipo cambio: '.$tipocambio_dia; exit();

      if ($monto_validar > $monto_usar) {
        $data['cuopag_id'] = 0;
        $data['cuopag_msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) . ' | validar: ' . $monto_validar . ' | usar: ' . $monto_usar . ' | ingre: ' . $ingreso_importe . ' | cambio: ' . $tipocambio_dia . ' / mond ing: ' . $ingreso_mon_id;
        echo json_encode($data);
        exit();
      }

      $moneda_depo = 'S/.';

      if (!empty($_POST['txt_cuopag_mon'])) {
        $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        $cuopag_his .= 'Este pago fue realizado CON DEPÓSITO EN BANCO, registrado por: ' . trim($_SESSION['usuario_nombre']) . ', en la fecha de: ' . date('d-m-Y h:i a') . ', desde la computadora con nombre: ' . $nombre_pc . ' y MAC: ' . $mac_pc . ' ||&& ';

        $pedidos = '';

        //registro de ingreso
        $cue_id = 43; // INGRESO / EGRESO PORBANCO
        $subcue_id = 148; // BBVA CONTINENTAL
        $mod_id = 0; //cuota pago


        $ing_det = 'INGRESO FICTICIO DE CAJA EN BANCO = Pago abonado en el banco, en la siguente cuenta N°: [ ' . $_POST['txt_num_cuenta'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ] POR CONCEPTO DE=[' . $det . ']';


        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha;
        $oIngreso->documento_id = 8;
        $oIngreso->ingreso_numdoc = '';
        $oIngreso->ingreso_det = $ing_det;
        $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id;
        $oIngreso->cliente_id = 1144;
        $oIngreso->caja_id = 1;
        $oIngreso->moneda_id = 1;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id;
        $oIngreso->ingreso_modide = $modide;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];

        $result = $oIngreso->insertar();
        if (intval($result['estado']) == 1) {
          $ingr_id = $result['ingreso_id'];
        }
        $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
        $oIngreso->ingreso_numope = $_POST['cuopag_numope'];
        $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
        $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
        $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
        $oIngreso->banco_id = $_POST['cmb_banco_id'];
        $oIngreso->ingreso_id = $ingr_id;

        $oIngreso->registrar_datos_deposito_banco();

        $tipo_pag = 'LOS HABERES MENSUALES';
        $egr_det = ' EGRESO FICTICIO DE CAJA EN BANCO = Pago abonado en el banco, en la siguente cuenta N°: [ ' . $_POST['txt_num_cuenta'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ] ---- POR CONCEPTO DE =[' . $det . ']';


        //                $det = 'EGRESO FICTICIO POR [ INGRESO ] DE obs : [ ' . $_POST['txt_cuopag_obs'] . ' ]. Pago abonado en el banco, en la siguente cuenta: [ ' . $_POST['hdd_cuenta_dep'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fecdep'] . ' ], la fecha de validación fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['txt_cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ]';
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        //		$numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-';

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = '';
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cuenta;
        $oEgreso->subcuenta_id = $subcuenta;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = 1;
        $oEgreso->moneda_id = 1;
        $oEgreso->modulo_id = 0;
        $oEgreso->egreso_modide = intval($modide);
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $result = $oEgreso->insertar();
        if (intval($result['estado']) == 1) {
          $egr_id = $result['egreso_id'];
        }

        //                echo ' hasta aki estoy entrando al sistemaa';exit();
        $oEgreso->modificar_campo($egr_id, 'tb_egreso_numdoc', $_SESSION['usuario_id'], 'STR');
        $oEgreso->modificar_campo($egr_id, 'tb_egreso_detalleplanilla', $detalle, 'STR');

        // GERSON (29-12-23) 
        $comision_usu = $oUsuario->obtener_comision_asesor($mes, $anio, $comision_usuario_id);
        if ($comision_usu['estado'] == 1) { // existe comision ya creada, eliminar para que se vuelva a crear
          $oUsuario->eliminar_comision_asesor($mes, $anio, $comision_usuario_id);
        }
        // Guardar registro de comisión
        //$oUsuario->registrar_comision_asesor('', $json_comision, $mes, $anio, $comision_usuario_id);
        //
        //

        $data['est'] = 1;
        $data['msj'] = 'Se ha generado el egreso correctamente';
        echo json_encode($data);
      }
    }
  } else {
    $data['est'] = 0;
    $data['msj'] = 'El usuario o el monto están vacíos, actualice y revise.';
    echo json_encode($data);
  }
}

if ($action == 'comisiones_asesor') {
  if (!empty($usu_id)) {

    /* GERSON (17-01-24) */
    $usuario = $oUsuario->mostrarUno($usu_id);
    $empresa_id = 0;
    $tip_per = 0;
    if ($usuario['estado'] == 1) {
      $empresa_id = intval($usuario['data']['tb_empresa_id']);
      $tip_per = intval($usuario['data']['tb_usuario_per']);
    }

    if ($tip_per == 1) { //jefatura
      $usu_id = '';
      $empresa = 0;
    }
    if ($tip_per == 2) { //ventas
      $empresa = 0;
    }
    if ($tip_per == 3) { //administrador de sede
      $usu_id = '';
      $empresa = $empresa_id;
    }

    $json_comision = '{';

    $dtsAsveh = $oPlanilla->comision_asiveh_new($mes, $anio, $usu_id, $empresa);
    $dtsGarveh = $oPlanilla->comision_garveh_new($mes, $anio, $usu_id, $empresa);
    $dtsHipo = $oPlanilla->comision_hipo_new($mes, $anio, $usu_id, $empresa);
    $dtsMenor = $oPlanilla->comision_menor($mes, $anio, $usu_id, $empresa);
    $dtsVenta = $oPlanilla->comision_ventagarantia($mes, $anio, $usu_id, $empresa);

    // ASISTENCIA VEHICULAR 
    $perso = $tip_per; //según tipo de personal del usuario
    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $total_comiAsveh = 0;
    $MONTO_COLOCADO_ASIVEH = 0;

    $json_comision .= '"asveh":[';

    if (!empty($dtsAsveh['data'])) {
      foreach ($dtsAsveh['data'] as $key => $dt) {
        $tipo = '----';
        $subTip = '';
        $moneda = 'S/.';
        if ($dt['tb_moneda_id'] == 2)
          $moneda = 'US$';
        if ($dt['tb_credito_tip1'] == 1)
          $tipo = "VENTA NUEVA ";
        if ($dt['tb_credito_tip1'] == 2)
          $tipo = "ADENDA ";
        if ($dt['tb_credito_tip1'] == 4)
          $tipo = "RE-VENTA ";

        if ($dt['tb_credito_tip2'] == 1)
          $subTip = "CREDITO REGULAR";
        if ($dt['tb_credito_tip2'] == 4)
          $subTip = "CUOTA BALON";

        if ($dt['tb_credito_tip1'] == 1 || $dt['tb_credito_tip1'] == 4) {
          $cretip = 2; //credito asiveh es de tipo 2
          $cre_asveh = $dt['tb_credito_tip1']; //obtenemos el porcentaje para venta nueva o reventa
        }
        if ($dt['tb_credito_tip1'] == 2) {
          $cretip = 5; //para la formula de comision las adendas son de creditotipo_id = 5
          $cre_asveh = 0; //ya que es adenda no tiene tipo de asveh, valor 0
        }

        $monto_comi = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];
        $MONTO_COLOCADO_ASIVEH += $monto_comi;

        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);
        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }

        $comi = moneda_mysql(($monto_comi * $porcen) / 100);
        $total_comiAsveh += $comi;

        $json_comision .= '{';

        $json_comision .= '"moneda": "' . $moneda . ')",';
        $json_comision .= '"tipo": ' . $tipo . $subTip . '"(CAV-' . $dt['tb_credito_id'] . ')",';
        $json_comision .= '"precio_acordado": ' . mostrar_moneda($dt['tb_credito_preaco']) . ',';
        $json_comision .= '"tipo_cambio": "' . $dt['tb_credito_tipcam'] . '",';
        $json_comision .= '"porcentaje": "' . $porcen . ' %",';
        $json_comision .= '"comision": ' . mostrar_moneda($comi);

        $json_comision .= '},';

        /* echo '<br>';
                        echo ' (CAV-' . $dt['tb_credito_id'] . ')';
                        echo '<br>';
                        echo $moneda . ' ' . mostrar_moneda($dt['tb_credito_preaco']);
                        echo '<br>';
                        echo $dt['tb_credito_tipcam'];
                        echo '<br>';
                        echo $porcen . ' %';
                        echo '<br>';
                        echo 'S/. ' . mostrar_moneda($comi);
                        echo '<br>'; */
      }
      $json_comision = substr($json_comision, 0, -1); // se leimina la coma del último valor
    }
    $json_comision .= '],';


    // GARANTIA VEHICULAR
    $perso = $tip_per; //según tipo de personal del usuario
    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $total_comiGarveh = 0;
    $sumaSoles = 0;
    $sumaDolares = 0;
    $MONTO_COLOCADO_GARVEH = 0;

    $json_comision .= '"garveh":[';

    if (!empty($dtsGarveh['data'])) {
      foreach ($dtsGarveh['data'] as $key => $dt) {

        if ($dt['tb_moneda_id'] == 1) {
          $moneda = 'S/.';
          $sumaSoles += $dt['tb_credito_preaco'];
        }
        if ($dt['tb_moneda_id'] == 2) {
          $moneda = 'US$';
          $sumaDolares += $dt['tb_credito_preaco'];
        }

        $tipo_gar = 'GARANTÍA VEHICULAR';
        $cretip = 3; //credito garveh es de tipo 3
        $cre_asveh = 0; //como es garveh no tiene venta nueva o reventa, entonces tiene valor 0

        if ($dt['tb_credito_tip'] == 2) {
          $tipo_gar = "ADENDA VEHICULAR";
          $cretip = 5; //para todas las adendas de los creditos, adenda tiene como creditotipo_id = 5
        }

        $valido = '';
        if (moneda_mysql($dt['tb_credito_valido']) > 0) {
          $monto_comi = $dt['tb_credito_valido'] * $dt['tb_credito_tipcam'];
          $valido = '| <b>' . mostrar_moneda($dt['tb_credito_valido']) . '</b>';
        } else
          $monto_comi = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];

        $MONTO_COLOCADO_GARVEH += $monto_comi;

        $porcen = 0;
        $formula_si = '(falta formula)';
        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);
        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
          $formula_si = '';
        }
        $dtsComi = NULL;

        if ($tip_per == 1) {
          if ($dt['tb_empresa_id'] == 2) {
            if ($dt['tb_credito_usureg'] == 2) {
              $porcen = mostrar_moneda(0.8);
            } else {
              $porcen = mostrar_moneda(1);
            }
          }
        }
        if ($tip_per == 3) {
          if ($usu_id == $dt['tb_credito_usureg']) {
            $porcen = mostrar_moneda(1.20);
          } else {
            $porcen = 0;
          }
        }
        if ($tip_per == 2 && $empresa_id == 2) { //personal ventas que pertenezca a mall, si logra interés del 5% 0.3+
          if (intval($dt['tb_credito_int']) == 5)
            $porcen = $porcen + 0.3;
        }


        $Cliente = $oCliente->mostrarUno($dt['tb_cliente_id']);
        if ($Cliente['estado'] == 1) {
          $nombre = $Cliente['data']['tb_cliente_nom'];
        }
        $VehMarc = $oVehMarc->mostrarUno($dt['tb_vehiculomarca_id']);
        if ($VehMarc['estado'] == 1) {
          $marca = $VehMarc['data']['tb_vehiculomarca_nom'];
        }
        $VehMod = $oVehMod->mostrarUno($dt['tb_vehiculomodelo_id']);
        if ($VehMod['estado'] == 1) {
          $modelo = $VehMod['data']['tb_vehiculomodelo_nom'];
        }
        $VehCla = $oVehCla->mostrarUno($dt['tb_vehiculoclase_id']);
        if ($VehCla['estado'] == 1) {
          $clase = $VehCla['data']['tb_vehiculoclase_nom'];
        }

        $oUser = $oUsuario->mostrarUno($dt['tb_credito_usureg']);
        if ($oUser['estado'] == 1) {
          $asesor = $oUser['data']['tb_usuario_nom'];
        }

        $comi = (($monto_comi * $porcen) / 100);
        $total_comiGarveh += $comi;

        $json_comision .= '{';

        $json_comision .= '"credito_id": "' . $dt['tb_credito_id'] . '",';
        $json_comision .= '"tipo": "' . $tipo_gar . ' (CGV-' . $dt['tb_credito_id'] . ') - [' . $nombre . ']",';
        $json_comision .= '"marca": "' . $marca . ' / ' . $modelo . ' / ' . $clase . '",';
        $json_comision .= '"asesor": "' . $asesor . '",';
        $json_comision .= '"moneda": "' . $moneda . '",';
        $json_comision .= '"valido": "' . $valido . '",';
        $json_comision .= '"precio_acordado": ' . $dt['tb_credito_preaco'] . ',';
        $json_comision .= '"interes": "' . $dt['tb_credito_int'] . '",';
        $json_comision .= '"tipo_cambio": "' . $dt['tb_credito_tipcam'] . '",';
        $json_comision .= '"porcentaje": "' . $porcen . ' %' . $formula_si . '",';
        $json_comision .= '"comision": ' . mostrar_moneda($comi);

        $json_comision .= '},';
      }
      $json_comision = substr($json_comision, 0, -1); // se leimina la coma del último valor
    }
    $json_comision .= '],';

    // HIPOTECARIO
    $perso = $tip_per; //según tipo de personal del usuario 1 jefa, 2 ventas
    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $cretip = 4; //el creditotipo_id de hipotecario es 4
    $cre_asveh = 0; //hipotecario no tiene venta nueva ni reventa
    $total_comiHipo = 0;
    $MONTO_COLOCADO_HIPO = 0;

    $json_comision .= '"hipo":[';

    if (!empty($dtsHipo['data'])) {
      foreach ($dtsHipo['data'] as $key => $dt) {
        $moneda = 'S/.';
        if ($dt['tb_moneda_id'] == 2)
          $moneda = 'US$';

        $usu_reg = $dt['tb_credito_usureg']; //id del usuario que registra el credito

        $valido = '';
        if (moneda_mysql($dt['tb_credito_valido']) > 0) {
          $monto_comi = $dt['tb_credito_valido'] * $dt['tb_credito_tipcam'];
          $valido = '| <b>' . mostrar_moneda($dt['tb_credito_valido']) . '</b>';
        } else
          $monto_comi = $dt['tb_credito_preaco'] * $dt['tb_credito_tipcam'];

        $MONTO_COLOCADO_HIPO += $monto_comi;

        //solo para hipotecario si el credito es registrado por jefatura gana 1%, vendedor gana 0.5%, jefatura por vendedor gana 0.5%, ANTES ERA TIP_PER_USUREG, PERO AHORA SIGUE LA MISMA REALGA QUE GARVEH
        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);

        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }

        $comi = formato_numero(($monto_comi * $porcen) / 100);
        $total_comiHipo += $comi;

        $json_comision .= '{';

        $json_comision .= '"moneda": "' . $moneda . '",';
        $json_comision .= '"tipo": "CREDITO HIPOTECARIO (CH-' . $dt['tb_credito_id'] . ')",';
        $json_comision .= '"precio_acordado": "' . mostrar_moneda($dt['tb_credito_preaco']) . ' ' . $valido . '",';
        $json_comision .= '"tipo_cambio": "' . $dt['tb_credito_tipcam'] . '",';
        $json_comision .= '"porcentaje": "' . $porcen . ' %",';
        $json_comision .= '"comision": ' . mostrar_moneda($comi);

        $json_comision .= '},';
      }
      $json_comision = substr($json_comision, 0, -1); // se leimina la coma del último valor
    }
    $json_comision .= '],';

    // CREDITO MENOR
    $perso = $tip_per; //según tipo de personal del usuario
    $est = 1; // el estado debe ser vigente 1, 0 no vigente
    $cretip = 1; //el creditotipo_id de MENOR es 1
    $cre_asveh = 0; //hipotecario no tiene venta nueva ni reventa
    $total_comiMenor = 0;
    $comision_colocacion_cmenor = 0;
    $comision_venta_cmenor = 0;
    $cant = 0;
    $monto_comi = 0;
    $comi = 0;
    $rowsColoca = count($dtsMenor['data']);
    $MONTO_COLOCADO_CMENOR = 0;
    $MONTO_VENDIDO_CMENOR = 0;

    $json_comision .= '"cmenor":[{';

    if ($dtsMenor['estado'] == 1) {
      foreach ($dtsMenor['data'] as $key => $dt) {

        $monto_comi += ($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);
        $pre_aco = ($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);
        $porcenAuto = ($dt['tb_credito_int'] * $dt['tb_credito_int']) / 100;

        $MONTO_COLOCADO_CMENOR += $pre_aco;

        if ($perso == 1) {
          //$porcenAuto=0.4;
          $comi += (($pre_aco * $porcenAuto * 0.4) / 100);
        }
        if ($perso == 2) {
          //$porcenAuto=0.4;
          $comi += (($pre_aco * $porcenAuto) / 100);
        }
        if ($perso == 3) {
          //$porcenAuto=0.6;
          $comi += (($pre_aco * $porcenAuto * 0.6) / 100);
        }
      }

      $total_comiMenor += $comi;
      $comision_colocacion_cmenor = $comi;

      $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía

      $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);
      if ($dtsComi['estado'] == 1) {
        $total_comiMenor = 0;
        $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        $comi = (($monto_comi * $porcen) / 100);
        $total_comiMenor += $comi;
        $comision_colocacion_cmenor = $comi;
      } else {
        $porcen = 'Auto';
      }

      $json_comision .= '"moneda": "S/ .",';
      $json_comision .= '"tipo": "COLOCACIÓN",';
      $json_comision .= '"cantidad": "' . $rowsColoca . '",';
      $json_comision .= '"monto_total": "' . mostrar_moneda($monto_comi) . '",';
      $json_comision .= '"porcentaje": "' . $porcen . ' %",';
      $json_comision .= '"comision": ' . mostrar_moneda($comi);
    }
    $json_comision .= '},';


    $diferencia = 0;
    //WHILE PARA VENTA DE GARANTÍAS MENORES
    $rowsVenta = count($dtsVenta['data']);
    $monto_comi = 0;
    $monSobre = 0;
    $monto_total = 0;
    $cantSobre = 0;
    $resta1 = 0;

    $json_comision .= '{';

    if ($dtsVenta['estado'] == 1) {
      foreach ($dtsVenta['data'] as $key => $dt) {
        $usuarioventa = intval($dt['tb_usuario_id']);
        if ($usuarioventa == $usu_id || $tip_per == 1 || $tip_per == 3) {
          $resta1 += floatval($dt['tb_ventagarantia_prec2']);
        }

        $MONTO_VENDIDO_CMENOR += $dt['tb_ventagarantia_prec2'];

        /* consulta cuantas cuotas se han realizado de pagos del credito 
                            * si se han hecho mas de 2 cuotas no acumula saldo
                            * se se ha hecho 1 o 0 cuotas de pago acumula la cuota para proceder con el descuento                    
                            *  */
        $results = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], 'COUNT(*)', 2);
        if ($results['estado'] == 1) {
          $cuotas_pagadas = $results['data']['COUNT(*)'];
        }
        $results = NULL;

        if ($cuotas_pagadas < 2) {
          $results2 = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], '*', 1);
          if ($results2['estado'] == 1) {
            foreach ($results2['data'] as $key => $dts) {
              if ($cuotas_pagadas == 0) {
                //$suma_pagadas += $dts['tb_cuota_int'] * 2; //intereses a 2 mes
                $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100) * 2; //intereses a 2 mes
              } else {
                //$suma_pagadas += $dts['tb_cuota_int']; //intereses a 1 meses
                $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100);
              }
            }
          }
          $results2 = NULL;
        }
        /* fin de consulta de cuotas */


        if (intval($usu_id) != intval($dt['tb_ventagarantia_col'])) {
          if ($dt['tb_ventagarantia_com'] > 0) {
            $monSobre += $dt['tb_ventagarantia_com'];
            $cantSobre++;
          }

          if ($dt['tb_ventagarantia_prec2'] > $dt['tb_ventagarantia_prec1']) { // si el precio de venta por el asesor es mayor aque pide IPDN

            $resta = floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val']) - $suma_pagadas; //restamos el precio que pide IPDN menos el valor de la garantia
            if ($resta > 0) {
              $monto_comi += $resta;
              $monto_total += $resta;
            }
          } else {
            if ($dt['tb_ventagarantia_prec2'] <= 0 && $perso == 1) { //jefatura
              $resta = (floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val'])) - $suma_pagadas; //precio que pide IPDN menos valor d ela garntia
              if ($resta > 0) {
                $monto_comi += $resta;
                $monto_total += $resta;
              }
            } else {
              $resta = (floatval($dt['tb_ventagarantia_prec2']) - floatval($dt['tb_garantia_val'])) - $suma_pagadas;
              if ($resta > 0) {
                $monto_comi += $resta;
                $monto_total += $resta;
              }
            }
          }
        }



        $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto_comi, $mes, $empresa_id);

        if ($dtsComi['estado'] == 1) {
          $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
        }
        $dtsComi = NULL;

        if (intval($dt['tb_usuario_id']) == $usuarioVendedor_id && intval($dt['tb_ventagarantia_col']) == 0) {
          $porcen = 25;
        }

        $comi = (($monto_comi * $porcen) / 100);

        $comision_total += $comi;
        $total_comiMenor += $comi;

        $monto_comi = 0;
        $porcen = 0;
        $suma_pagadas = 0;
      }

      $comision_venta_cmenor += $comision_total;

      $json_comision .= '"moneda": "S/ .",';
      $json_comision .= '"tipo": "VENTA GARANTÍAS",';
      $json_comision .= '"cantidad": "' . $rowsVenta . '",';
      $json_comision .= '"monto_total": "' . mostrar_moneda($resta1) . '",';
      $json_comision .= '"porcentaje": "Auto %",';
      $json_comision .= '"comision": ' . mostrar_moneda($comision_total);
    }
    $json_comision .= '},';


    $json_comision .= '{';

    //WHILE PARA EL SOBRE PRECIO DE VENTA DE GARANTÍAS
    if ($cantSobre > 0) {
      $evento = 3; //los eventos son 1 poner el credito, 2 vender garantía, 3 venta sobre costo
      $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monSobre, $mes, $empresa_id);

      if ($dtsComi['estado'] == 1) {
        $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
      }

      $comi = (($monSobre * $porcen) / 100);

      $total_comiMenor += $comi;
      $comision_venta_cmenor += $comi;

      $json_comision .= '"moneda": "S/ .",';
      $json_comision .= '"tipo": "VALOR SOBRE COSTO",';
      $json_comision .= '"cantidad": "' . $cantSobre . '",';
      $json_comision .= '"monto_total": "' . mostrar_moneda($resmonSobreta1) . '",';
      $json_comision .= '"porcentaje": "' . $porcen . ' %",';
      $json_comision .= '"comision": ' . mostrar_moneda($comi);
    }
    $json_comision .= '}';


    $json_comision .= "]}";

    $comision = $oUsuario->obtener_comision_asesor($mes, $anio, $usu_id);

    if ($comision['estado'] == 1) { // ya hay comisión generada
      $data['est'] = 2;
      $data['msj'] = 'Ya se ha generado comisión este mes.';
      echo json_encode($data);
    } else {
      //if($oUsuario->registrar_comision_asesor('', $json_comision, $mes, $anio, $usu_id)){
      if (1 == 1) {
        $data['est'] = 1;
        $data['msj'] = 'Se ha generado la comisión correctamente';
        echo json_encode($data);
      }
    }
    /*  */
  } else {
    $data['est'] = 0;
    $data['msj'] = 'El usuario está vacío, actualice y revise.';
    echo json_encode($data);
  }
}