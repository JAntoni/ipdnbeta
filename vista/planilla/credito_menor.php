<label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #003eff">CRÉDITO MENOR</label>
<div class="panel panel-primary">
    <div class="panel-body">
        <table style="width: 100%;font-family: cambria;" class="table-hover">
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">TIPO</th>
                <th id="tabla_cabecera_fila">CANTIDAD</th>
                <th id="tabla_cabecera_fila">MONTO TOTAL</th>
                <th id="tabla_cabecera_fila">PORCENTAJE</th>
                <th id="tabla_cabecera_fila">COMISIÓN</th>
                <th id="tabla_cabecera_fila" width="10%"></th>
            </tr>
            <?php
            if(empty($cmenor[0])){
                $perso = $tip_per; //según tipo de personal del usuario
                $est = 1; // el estado debe ser vigente 1, 0 no vigente
                $cretip = 1; //el creditotipo_id de MENOR es 1
                $cre_asveh = 0; //hipotecario no tiene venta nueva ni reventa
                $total_comiMenor = 0;
                $comision_colocacion_cmenor = 0;
                $comision_venta_cmenor = 0;
                $cant = 0;
                $monto = 0;
                $comi = 0;
                $rowsColoca = count($dtsMenor['data']);
                $MONTO_COLOCADO_CMENOR = 0;
                $MONTO_VENDIDO_CMENOR = 0;

                if ($dtsMenor['estado'] == 1) {
                    foreach ($dtsMenor['data']as $key => $dt) {

                        $monto += ($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);
                        $pre_aco = ($dt['tb_credito_preaco'] * $dt['tb_credito_tipcam']);
                        $porcenAuto = ($dt['tb_credito_int'] * $dt['tb_credito_int']) / 100;
                        
                        $MONTO_COLOCADO_CMENOR += $pre_aco;
                        
                        if($perso==1){
                            //$porcenAuto=0.4;
                            $comi += (($pre_aco * $porcenAuto*0.4) / 100);
                        }
                        if($perso==2){
    //                        $porcenAuto=0.4;
                            $comi += (($pre_aco * $porcenAuto) / 100);
                        }
                        if($perso==3){
    //                        $porcenAuto=0.6;
                            $comi += (($pre_aco * $porcenAuto*0.6) / 100);
                        }

    //                    $comi += (($pre_aco * $porcenAuto) / 100);
    //                    $total_comiMenor += (($pre_aco * $porcenAuto) / 100);
                    }
                    $total_comiMenor += $comi;
                    $comision_colocacion_cmenor = $comi;

                    $evento = 1; //los eventos son 1 poner el credito, 2 vender garantía
                    $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes, $empresa_id);
                    if ($dtsComi['estado'] == 1) {
                        $total_comiMenor = 0;
                        $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                        $comi = (($monto * $porcen) / 100);
                        $total_comiMenor += $comi;
                        $comision_colocacion_cmenor = $comi;
                    } else {
                        $porcen = 'Auto';
                    }

                    echo '
                        <tr style="height: 25px">
                        <td id="tabla_fila" style="font-family:cambria;font-weight: bold">COLOCACIÓN</td>
                        <td id="tabla_fila" align="center">' . $rowsColoca . '</td>
                        <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($monto) . '</td>
                        <td id="tabla_fila" align="center">' . $porcen . ' %</td>
                        <td id="tabla_fila" align="center"><span>S/. ' . mostrar_moneda($comi) . '</span></td>
                        <td id="tabla_fila" align="center"><a class="btn_ver_det btn btn-primary btn-xs" onclick="planilla_detalle_creditos(1, ' . $usu_id . ', ' . $perso . ')" title="ver créditos menores"><i class="fa fa-eye"></i></a></td>
                        </tr>';
                }
            }else{
                if(!empty($cmenor[0]->comision)){
                    echo '
                        <tr style="height: 25px">
                            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">COLOCACIÓN</td>
                            <td id="tabla_fila" align="center">' . $cmenor[0]->cantidad . '</td>
                            <td id="tabla_fila" align="center">S/. ' . $cmenor[0]->monto_total . '</td>
                            <td id="tabla_fila" align="center">' . $cmenor[0]->porcentaje . ' </td>
                            <td id="tabla_fila" align="center"><span>S/. ' . mostrar_moneda($cmenor[0]->comision) . '</span></td>
                            <td id="tabla_fila" align="center"><a class="btn_ver_det btn btn-primary btn-xs" onclick="planilla_detalle_creditos(1, ' . $usu_id . ', ' . $perso . ')" title="ver créditos menores"><i class="fa fa-eye"></i></a></td>
                        </tr>';

                    $total_comiMenor = $total_comiMenor + $cmenor[0]->comision;
                }
                
            }


            $diferencia = 0;
            //WHILE PARA VENTA DE GARANTÍAS MENORES
            $rowsVenta = count($dtsVenta['data']);
            $monto = 0;
            $monSobre = 0;
            $monto_total=0;
            $cantSobre = 0;
            $resta1 = 0;
            if(empty($cmenor[1])){
                if ($dtsVenta['estado'] == 1) {
                    foreach ($dtsVenta['data']as $key => $dt) {
                        $usuarioventa= intval($dt['tb_usuario_id']);
                        if($usuarioventa==$usu_id || $tip_per == 1 || $tip_per == 3){
                            $resta1 += floatval($dt['tb_ventagarantia_prec2']);
                        }
                        
                        $MONTO_VENDIDO_CMENOR += $dt['tb_ventagarantia_prec2'];

                        /* consulta cuantas cuotas se han realizado de pagos del credito 
                        * si se han hecho mas de 2 cuotas no acumula saldo
                        * se se ha hecho 1 o 0 cuotas de pago acumula la cuota para proceder con el descuento                    
                        *  */
                        $results = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], 'COUNT(*)', 2);
                        if ($results['estado'] == 1) {
                            $cuotas_pagadas = $results['data']['COUNT(*)'];
                        }
                        $results = NULL;

                        if ($cuotas_pagadas < 2) {
                            $results2 = $oPlanilla->CuotasPagadas($dt['tb_credito_id'], '*', 1);
                            if ($results2['estado'] == 1) {
                                foreach ($results2['data']as $key => $dts) {
                                    if ($cuotas_pagadas == 0) {
                                        //$suma_pagadas += $dts['tb_cuota_int'] * 2; //intereses a 2 mes
                                        $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100) * 2; //intereses a 2 mes
                                    } else {
                                        //$suma_pagadas += $dts['tb_cuota_int']; //intereses a 1 meses
                                        $suma_pagadas += (floatval($dt['tb_garantia_val']) * floatval($dt['tb_credito_int']) / 100);
                                    }
                                }
                            }
                            $results2 = NULL;
                        }
                        /* fin de consulta de cuotas */


                        if (intval($usu_id) != intval($dt['tb_ventagarantia_col'])) {
                            if ($dt['tb_ventagarantia_com'] > 0) {
                                $monSobre += $dt['tb_ventagarantia_com'];
                                $cantSobre++;
                            }

                            if ($dt['tb_ventagarantia_prec2'] > $dt['tb_ventagarantia_prec1']) {// si el precio de venta por el asesor es mayor aque pide IPDN
                                
                                $resta = floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val']) - $suma_pagadas; //restamos el precio que pide IPDN menos el valor de la garantia
                                if ($resta > 0) {
                                    $monto += $resta;
                                    $monto_total += $resta;
                                }
                            } else {
                                if ($dt['tb_ventagarantia_prec2'] <= 0 && $perso == 1) {//jefatura
                                    $resta = (floatval($dt['tb_ventagarantia_prec1']) - floatval($dt['tb_garantia_val'])) - $suma_pagadas; //precio que pide IPDN menos valor d ela garntia
                                    if ($resta > 0) {
                                        $monto += $resta;
                                        $monto_total += $resta;
                                    }
                                } else {
                                    $resta = (floatval($dt['tb_ventagarantia_prec2']) - floatval($dt['tb_garantia_val'])) - $suma_pagadas;
                                    if ($resta > 0) {
                                        $monto += $resta;
                                        $monto_total += $resta;
                                    }
                                }
                            }
                        }



                        $evento = 2; //los eventos son 1 poner el credito, 2 vender garantía
                        $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monto, $mes, $empresa_id);

                        if ($dtsComi['estado'] == 1) {
                            $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                        }
                        $dtsComi = NULL;

                        if (intval($dt['tb_usuario_id']) == $usuarioVendedor_id && intval($dt['tb_ventagarantia_col']) == 0) {
                            $porcen = 25;
                        }

                        $comi = (($monto * $porcen) / 100);
    //                    $comi = (($diferencia * $porcen) / 100);
                        $comision_total += $comi;
                        $total_comiMenor += $comi;

                        $monto = 0;
                        $porcen = 0;
                        $suma_pagadas = 0;
                    }
                    $comision_venta_cmenor += $comision_total;
                    echo '
                        <tr style="height: 25px">
                        <td id="tabla_fila" style="font-family:cambria;font-weight: bold">VENTA GARANTÍAS</td>
                        <td id="tabla_fila" align="center">' . $rowsVenta . '</td>
                        <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($resta1) . '</td>
                        <td id="tabla_fila" align="center"> Auto %</td>
                        <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($comision_total) . '</td>
                        <td id="tabla_fila" align="center"><a class="btn_ver_det btn btn-info btn-xs" onclick="planilla_detalle_creditos(2, ' . $usu_id . ', ' . $perso . ')" title="ver venta de garantías"><i class="fa fa-eye"></i></a></td>
                        </tr>';
                }
            }else{
                if(!empty($cmenor[1]->comision)){
                    echo '
                        <tr style="height: 25px">
                            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">VENTA GARANTÍAS</td>
                            <td id="tabla_fila" align="center">' . $cmenor[1]->cantidad . '</td>
                            <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($cmenor[1]->monto_total) . '</td>
                            <td id="tabla_fila" align="center"> Auto %</td>
                            <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($cmenor[1]->comision) . '</td>
                            <td id="tabla_fila" align="center"><a class="btn_ver_det btn btn-info btn-xs" onclick="planilla_detalle_creditos(2, ' . $usu_id . ', ' . $perso . ')" title="ver venta de garantías"><i class="fa fa-eye"></i></a></td>
                        </tr>';

                    $total_comiMenor = $total_comiMenor + $cmenor[1]->comision;
                }
                

            }
            //WHILE PARA EL SOBRE PRECIO DE VENTA DE GARANTÍAS
            if(empty($cmenor[2])){
                if ($cantSobre > 0) {
                    $evento = 3; //los eventos son 1 poner el credito, 2 vender garantía, 3 venta sobre costo
                    $dtsComi = $oComision->mostrar_comision_estado_monto($perso, $cretip, $cre_asveh, $evento, $est, $monSobre, $mes, $empresa_id);

                    if ($dtsComi['estado'] == 1) {
                        $porcen = $dtsComi['data']['tb_comisiondetalle_porcen'];
                    }

                    $comi = (($monSobre * $porcen) / 100);

                    $total_comiMenor += $comi;
                    $comision_venta_cmenor += $comi;
                    echo '
                        <tr style="height: 25px">
                        <td id="tabla_fila" style="font-family:cambria;font-weight: bold">VALOR SOBRE COSTO</td>
                        <td id="tabla_fila" align="center">' . $cantSobre . '</td>
                        <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($monSobre) . '</td>
                        <td id="tabla_fila" align="center">' . $porcen . ' %</td>
                        <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($comi) . '</td>
                        <td  id="tabla_fila" align="center" width="5"><a class="btn_ver_det btn btn-facebook btn-xs" onclick="planilla_detalle_creditos(3, ' . $usu_id . ', ' . $perso . ')" title="ver Créditos"><i class="fa fa-eye"></i></a></td>
                        </tr>';
                }
            }else{

                if(!empty($cmenor[2]->comision)){
                    echo '
                        <tr style="height: 25px">
                            <td id="tabla_fila" style="font-family:cambria;font-weight: bold">VALOR SOBRE COSTO</td>
                            <td id="tabla_fila" align="center">' . $cmenor[2]->cantidad . '</td>
                            <td id="tabla_fila" align="center">S/. ' . $cmenor[2]->monto_total . '</td>
                            <td id="tabla_fila" align="center">' . $cmenor[2]->porcentaje . ' %</td>
                            <td id="tabla_fila" align="center">S/. ' . mostrar_moneda($cmenor[2]->comision) . '</td>
                            <td  id="tabla_fila" align="center" width="5"><a class="btn_ver_det btn btn-facebook btn-xs" onclick="planilla_detalle_creditos(3, ' . $usu_id . ', ' . $perso . ')" title="ver Créditos"><i class="fa fa-eye"></i></a></td>
                        </tr>';

                    $total_comiMenor = $total_comiMenor + $cmenor[2]->comision;
                }

            }
            ?>
            <tr>
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="4">TOTAL COMISIONES:</td>
                <td id="tabla_fila" align="center">S/. <?php echo mostrar_moneda($total_comiMenor) ?></td>
            </tr>
        </table>
    </div>
</div>