<?php
require_once("../../core/usuario_sesion.php");
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once('../proveedor/Proveedor.class.php');
$oProveedor = new Proveedor();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../menorgarantia/Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$usu_id = $_POST['usu_id'];
$tipo = $_POST['tipo'];

$sueldo_total = moneda_mysql($_POST['sueldo_total']);
$sueldo_bruto = moneda_mysql($_POST['sueldo_bruto']);
$sueldo_comision = moneda_mysql($_POST['sueldo_comision']);

$mes = $_POST['mes'];
$anio = $_POST['anio'];
$fecha_hoy = date('d-m-Y');

$array_mes = array(
  '01' => 'ENERO', '02' => 'FEBRERO', '03' => 'MARZO', '04' => 'ABRIL', '05' => 'MAYO', '06' => 'JUNIO', '07' => 'JULIO',
  '08' => 'AGOSTO', '09' => 'SEPTIEMBRE', '10' => 'OCTUBRE', '11' => 'NOVIEMBRE', '12' => 'DICIEMBRE'
);

$dts = $oUsuario->mostrarUno($usu_id);
if ($dts['estado'] == 1) {
  $usu_nom = $dts['data']['tb_usuario_nom'] . ' ' . $dts['data']['tb_usuario_ape'];
  $usu_dni = $dts['data']['tb_usuario_dni'];
  $tipoban = $dts['data']['tb_usuario_tipoban'];
  $nrocuenta = $dts['data']['tb_usuario_nrocuenta'];
}
$dts = NULL;

$dts = $oProveedor->mostrar_por_dni($usu_dni);
if ($dts['estado'] == 1) {
  $pro_id = $dts['data']['tb_proveedor_id'];
}
$dts = NULL;
////////////echo $monto_com.' // dni: '.$usu_dni.' // pro id: '.$pro_id; exit();
if (empty($pro_id)) {
  echo '<center><h2>EL colaborador no está registrado como proveedor, revise por favor.</h2></center>';
  exit();
}

$cuenta = 11;
$subcuenta = 35;
$tipo_pag = 'haberes';

if ($action == 'comision') {
  $cuenta = 9;
  $subcuenta = 0;
  $tipo_pag = 'comisiones';
}

$modide = $anio . $mes; //el modid sera la union del anio y mes para poder consultar y no volver a pagar el mismo mes




$dts = $oVentainterna->listar_ventainterna($usu_id, $mes, $anio);
  if ($dts['estado'] == 1) {
    $tb_ventainterna_mon = 0;
    $pagos_parciales_caja = 0;
    foreach ($dts['data'] as $key => $dt) {
      $pagar = '<strong style="color: green;">PAGADO</strong>';
      if ($dt['tb_ventainterna_est'] == 0) {
        $pagar = '<strong style="color: red;">POR PAGAR</strong>';
        $tb_ventainterna_mon += $dt['tb_ventainterna_mon'];
      }

      if ($dt['tb_ventainterna_est'] == 1 && $dt['tb_ventainterna_tipo'] == 1) {
        $tb_ventainterna_mon += $dt['tb_ventainterna_mon'];
        //? BUSCAMOS LOS PAGOS PREVIOS QUE SE HAYAN HECHO A LA VENTATAINTERNA PARA RESTAR LOS PAGOS POR PLANILLA
        $result = $oIngreso->consulta_ingresos_ventainterna_pago_caja($dt['tb_ventainterna_id']);
          if ($result['estado'] == 1) {
            foreach ($result['data'] as $key2 => $value) {
              $pagos_parciales_caja += $value['tb_ingreso_imp'];
              $tb_ventainterna_mon = $tb_ventainterna_mon - $value['tb_ingreso_imp'];
            }
          }
        $result = NULL;
        //GERSON 31-10-23
        $result2 = $oIngreso->consulta_ingresos_ventainterna_pago_banco($dt['tb_ventainterna_id']);
          if ($result2['estado'] == 1) {
            foreach ($result2['data'] as $key2 => $value) {
              $tb_ventainterna_mon = $tb_ventainterna_mon -  $value['tb_ingreso_imp'];
            }
          }
        $result2 = NULL;
        //
      }

      $dtsGar = $oGarantia->mostrarUno($dt['tb_garantia_id']);
      if ($dtsGar['estado'] == 1) {
        $garantia_cre_id = $dtsGar['data']['tb_credito_id'];
      }
    }
  }
$dts = NULL;

$pago_previo = 0;
$tabla_adelanto = '';
$datos = $cuenta . ' // ' . $subcuenta . ' // ' . $pro_id . ' // ' . intval($modide) . ' // ' . $pagos_parciales_caja.' / tipo: '.$tipo;
//antes de proceder hacer el desembolso consultemos si ya se hizo el pago al colaborador, para ello ingresamos el tipo de cuenta y en modide uniomos el año con el mes, ya que ese indica el mes y amio de pago
$dts = $oEgreso->revisar_pago_colaborar_cuenta($cuenta, $subcuenta, $pro_id, intval($modide));

if ($dts['estado'] == 1) {
  foreach ($dts['data'] as $key => $dt) {
    $tabla_adelanto .= '
            <tr>
                <td style="border: 1px solid black;" align="center">' . mostrar_fecha($dt['tb_egreso_fec']) . '</td>
                <td style="border: 1px solid black;" align="center">S/. ' . mostrar_moneda($dt['tb_egreso_imp']) . '</td>
                <td style="border: 1px solid black;" align="center">' . $dt['tb_egreso_detalleplanilla'] . '</td>
            </tr>';
    $pago_previo += floatval($dt['tb_egreso_imp']);
  }
  $tabla_adelanto .= '
            <tr>
                <td style="border: 1px solid black;" align="center"><b>TOTAL</b></td>
                <td style="border: 1px solid black;" align="center"><b>S/. ' . mostrar_moneda($pago_previo) . '</b></td>
                <td style="border: 1px solid black;" align="center;font-family:cambria""></td>
            </tr>';
}

if ($pago_previo <= 0)
  $tabla_adelanto = '<td colspan ="3" style="border: 1px solid black;font-family:cambria" align="center">SIN ADELANTOS</td>';
$saldo = ($sueldo_total - $pago_previo);

if ($saldo <= 0.09) {
  echo ' 
        <div class="modal fade" tabindex="-1" role="dialog" id="div_modal_planilla_form" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #003eff">PAGO DE SUELDO O COMISIONES AL COLABORADOR ' . $datos . '</h4>
                    </div>
                    <form id="form_pago_cola">
                       



                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <table id="tbl_pagos" style="width: 100%;border: 1px solid black; border-collapse: collapse;font-family: cambria">
                                        <tr id="tabla_cabecera">
                                            <th id="tabla_cabecera_fila" style="border: 1px solid black;" colspan="3" >ADELANTOS DE SUELDO</th>
                                        </tr>
                                        <tr id="tabla_cabecera">
                                            <th id="tabla_cabecera_fila" style="border: 1px solid black;">FECHA</th>
                                            <th id="tabla_cabecera_fila" style="border: 1px solid black;">MONTO</th>
                                            <th id="tabla_cabecera_fila" style="border: 1px solid black;">DETALLE</th>
                                        </tr>' .
    $tabla_adelanto .
    '</table>
                                </div>
                                <div class="col-md-4">
                                    <center><h2 style="font-family:cambria">Ya se le pagó el total de ' . $tipo_pag . ' al colaborador ' . $usu_nom . ' del mes de ' . $array_mes[$mes] . ' del ' . $anio . '</h2></center>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <div class="f1-buttons">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>         
                        </div>
                    </form>
                </div>
            </div>
        </div>';


  exit();
}


?>


<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_planilla_form" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" id="modalSizePagoForm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #003eff">BETA: PAGO DE SUELDO O COMISIONES AL COLABORADOR <?php echo $datos; ?></h4>
      </div>
      <form id="form_pago_cola">
        <input type="hidden" id="hdd_action" value="<?php echo $action; ?>">
        <input type="hidden" id="hdd_usu_id" value="<?php echo $usu_id; ?>">
        <input type="hidden" id="hdd_mes" value="<?php echo $mes; ?>">
        <input type="hidden" id="hdd_anio" value="<?php echo $anio; ?>">
        <input type="hidden" id="hdd_tipo" name="hdd_tipo" value="<?php echo $tipo; ?>">
        <input type="hidden" id="hdd_saldo" value="<?php echo formato_numero($saldo); ?>">
        <input type="hidden" id="hdd_total_sueldo" value="<?php echo formato_numero($sueldo_total) ?>">
        <input type="hidden" id="hdd_sueldo" value="<?php echo formato_numero($sueldo_bruto) ?>">
        <input type="hidden" id="hdd_comision" value="<?php echo formato_numero($sueldo_comision); ?>">



        <div class="modal-body">
          <div class="row">
            <div class="col-lg-12">
              <center><strong style="font-family: cambria;text-align: center">Pago de <?php echo $tipo_pag . ' al colaborador ' . $usu_nom . ' del mes de ' . $array_mes[$mes] . ' del ' . $anio; ?></strong></center>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
              <div class="col-lg-12">
                <?php
                if ($sueldo_total >= $tb_ventainterna_mon) {
                  $sueldo_total2 = $sueldo_total + $tb_ventainterna_mon;
                } else {
                  $sueldo_total2 = 0.00;
                }

                ?>
                <label>Pago total de <?php echo $tipo_pag; ?></label>
                <input type="text" class="form-control input-sm" name="txt_pla_pag_tot" value="<?php echo mostrar_moneda($sueldo_total2); ?>" readonly>
              </div>
              <p>
              <div class="col-lg-12">
                <label>Descuento por Compras Colaboradores</label>
                <input type="text" class="form-control input-sm" name="txt_pla_des_tot" value="<?php echo mostrar_moneda($tb_ventainterna_mon); ?>" readonly>
              </div>
              <p>
              <div class="col-lg-12">
                <label>Monto adelantado de <?php echo $tipo_pag; ?></label>
                <input type="text" class="form-control input-sm" name="txt_pla_pag_par" value="<?php echo mostrar_moneda($pago_previo); ?>" readonly>
              </div>
              <p>
              <div class="col-lg-12">
                <label>Monto a Pagar</label>
                <input type="text" id="txt_pla_pag_pag" class="form-control input-sm moneda" value="<?php echo mostrar_moneda($saldo); ?>">
              </div>
              <p>
              <div class="col-lg-12">
                <label>Detalle</label>
                <textarea class="form-control input-sm mayus" id="txt_detalle" name="txt_detalle"></textarea>
              </div>
            </div>
            <div class="col-md-8">
              <table id="tbl_pagos" style="width: 100%;border: 1px solid black; border-collapse: collapse;font-family: cambria">
                <tr id="tabla_cabecera">
                  <th id="tabla_cabecera_fila" style="border: 1px solid black;" colspan="3">ADELANTOS DE SUELDO</th>
                </tr>
                <tr id="tabla_cabecera">
                  <th id="tabla_cabecera_fila" style="border: 1px solid black;">FECHA</th>
                  <th id="tabla_cabecera_fila" style="border: 1px solid black;">MONTO</th>
                  <th id="tabla_cabecera_fila" style="border: 1px solid black;">DETALLE</th>
                </tr>
                <?php echo $tabla_adelanto; ?>
              </table>
            </div>
          </div>
          <br>
          <?php if ($tipo == 2) {?>
            <div id="pago_banco">
              <?php require_once('../planillabeta/planilla_pago_banco_form.php'); ?>
            </div>
          <?php }?>
          <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
          <?php if ($action == 'eliminar') : ?>
            <div class="callout callout-warning">
              <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Area?</h4>
            </div>
          <?php endif; ?>

          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="area_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_area">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/planillabeta/planilla_pago_form.js?ver=1'; ?>"></script>
<script>
    function adjustModalSizePagoForm() {
        var modalDialogForm = document.getElementById('modalSizePagoForm');
        if (window.innerWidth < 768) { // Tamaño 'sm' o menos
            modalDialogForm.classList.remove('modal-lg');
            modalDialogForm.classList.add('modal-xl');
        } else { // Tamaño 'md' o mayor
            modalDialogForm.classList.remove('modal-xl');
            modalDialogForm.classList.add('modal-lg');
        }
    }

    adjustModalSizePagoForm();

    // Ajustar el tamaño del modal al cambiar el tamaño de la ventana
    window.addEventListener('resize', adjustModalSizePagoForm);
</script>