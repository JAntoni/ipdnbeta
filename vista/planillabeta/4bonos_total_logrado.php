<!-- TOTAL COMISIONES-->
<?php
require_once ('../metacredito/Metacredito.class.php');
$oMetacredito = new Metacredito();

//? LAS METAS ESTÁN ASIGNADAS POR CADA COLABORADOR Y POR TIPO DE CRÉDITO, SI LLEGA AL 100% DE SU META ASIGANA COBRARÁ EL 100% DE SUS COMISIONES PERO SI NO LO LOGRA
//? SOLO COBRARÁ EL 50% DE LAS COMISIONES ASIGANADAS
// $final_comisiones = $total_comiAsveh + $total_comiGarveh + $total_comiHipo + $comision_colocacion_cmenor + $comision_venta_cmenor + $total_comiVenta + $pago_recaudo + $pago_mejoragc;

$metacredito_creditos = 0;
$metacredito_cmenor = 0;
$metacredito_ventacm = 0;
$metacredito_ventaveh = 0;

$comisiones_creditos = $ASIVEH_TOTAL_COMISIONES + $GARVEH_TOTAL_COMISIONES + $HIPO_TOTAL_COMISIONES;
$MONTO_LOGRADO_CREDITOS = $ASIVEH_TOTAL_LOGRADO + $GARVEH_TOTAL_LOGRADO + $HIPO_TOTAL_LOGRADO;

$tiene_metas = 'NO';
$result6 = $oMetacredito->buscarmeta_usuario($GLOBAL_USUARIO_ID, $GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA);
  if($result6['estado'] == 1){
    $metacredito_creditos = $result6['data']['tb_metacredito_creditos'];
    $metacredito_cmenor = $result6['data']['tb_metacredito_cmenor'];
    $metacredito_ventacm = $result6['data']['tb_metacredito_ventacm'];
    $metacredito_ventaveh = $result6['data']['tb_metacredito_ventaveh'];

    $tiene_metas = 'SI';
  }
$result6 = NULL;

$COBRAR_REAL_CREDITOS = $comisiones_creditos;
$COBRAR_REAL_CMENOR = $MENOR_TOTAL_COMISIONES;
$COBRAR_REAL_VENTACM = ($REMATES_TOTAL_COMISIONES + $SOBRECOTO_TOTAL_COMISIONES);
$COBRAR_REAL_VENTAVEH = $VENTAVEHICULO_TOTAL_COMISIONES;
$BONO_POR_METAS = 0;

$porcentaje_creditos = 1;
$porcentaje_cmenor = 1;
$porcentaje_ventacm = 1;
$porcentaje_ventaveh = 1;
$CANTIDAD_VENTAVEH = intval($CANTIDAD_VENTAVEH);

$td_logrado_creditos = '<span class="badge bg-green">'.mostrar_moneda($MONTO_LOGRADO_CREDITOS).' (ASIVEH: '.$ASIVEH_TOTAL_LOGRADO.', GARVEH: '.mostrar_moneda($GARVEH_TOTAL_LOGRADO).', HIPO: '.$HIPO_TOTAL_LOGRADO.')</span>';
$td_logrado_cmenor = '<span class="badge bg-green">'.mostrar_moneda($MENOR_TOTAL_LOGRADO).'</span>';
$td_logrado_ventacm = '<span class="badge bg-green">'.mostrar_moneda($REMATES_TOTAL_LOGRADO).'</span>';
$td_logrado_ventaveh = '<span class="badge bg-green">'.$CANTIDAD_VENTAVEH.' unidades</span>';
$td_logrado_bono = '<span class="badge bg-red">Lamentablemente no logró cumplir sus metas</span>';

if($MONTO_LOGRADO_CREDITOS < $metacredito_creditos){
  $porcentaje_creditos = 0.5;
  $td_logrado_creditos = '<span class="badge bg-red">'.mostrar_moneda($MONTO_LOGRADO_CREDITOS).'</span>';
}
if($MENOR_TOTAL_LOGRADO < $metacredito_cmenor){
  $porcentaje_cmenor = 0.5;
  $td_logrado_cmenor = '<span class="badge bg-red">'.mostrar_moneda($MENOR_TOTAL_LOGRADO).'</span>';
}
if($REMATES_TOTAL_LOGRADO < $metacredito_ventacm){
  $porcentaje_ventacm = 0.5;
  $td_logrado_ventacm = '<span class="badge bg-red">'.mostrar_moneda($REMATES_TOTAL_LOGRADO).'</span>';
}
if($CANTIDAD_VENTAVEH < $metacredito_ventaveh){ //los vehísulos son los únicos que se miden por CANTIDAD DE UNIDADES VENDIDAS
  $porcentaje_ventaveh = 0.5;
  $td_logrado_ventaveh = '<span class="badge bg-red">'.$CANTIDAD_VENTAVEH.' unidades</span>';
}

//? revisemos si se logró cumplir LAS 4 METAS para poder pagarle el BONO ofrecido de S/. 400
if($tiene_metas == 'SI' && $MONTO_LOGRADO_CREDITOS >= $metacredito_creditos && $MENOR_TOTAL_LOGRADO >= $metacredito_cmenor && $REMATES_TOTAL_LOGRADO >= $metacredito_ventacm && $CANTIDAD_VENTAVEH >= $metacredito_ventaveh){
  //se cumplió con las 3 metas, genial se gana el bono
  $BONO_POR_METAS = 400;
  if($GLOBAL_USUARIO_ID == 11) //? solo engel puede llevar un bono de 5,500 ya que no comisiona por GARVEH
    $BONO_POR_METAS = 5500;
  $td_logrado_bono = '<span class="badge bg-green">Felicitaciones ha logrado conseguir el Bono</span>';
}

$COBRAR_REAL_CREDITOS = $COBRAR_REAL_CREDITOS * $porcentaje_creditos;
$COBRAR_REAL_CMENOR = $COBRAR_REAL_CMENOR * $porcentaje_cmenor;
$COBRAR_REAL_VENTACM = $COBRAR_REAL_VENTACM * $porcentaje_ventacm;
$COBRAR_REAL_VENTAVEH = $COBRAR_REAL_VENTAVEH * $porcentaje_ventaveh;

$TOTAL_COMISIONES_SUMA_REAL = $COBRAR_REAL_CREDITOS + $COBRAR_REAL_CMENOR + $COBRAR_REAL_VENTACM + $COBRAR_REAL_VENTAVEH + $BONO_POR_METAS + $RECAUDO_TOTAL_COMISIONES + $MEJORAGC_TOTAL_COMISIONES;

$final_comisiones = $TOTAL_COMISIONES_SUMA_REAL; // - $total_reversion - $total_descuentos;
$sueldo_comision = $final_comisiones;
?>
<div class="col-md-6">
  <div class="box box-info shadow-simple">
    <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">RESUMEN LOGRADO Y BONO</h3></div>
    <div class="box-body table-responsive" style="">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Descripción de Meta</th>
          <th>Meta</th>
          <th>Logrado</th>
          <th>% a Pagar</th>
          <th>Pago Final</th>
        </tr>
        <tr>
          <td>Garveh + Asiveh + Hipo</td>
          <td><?php echo mostrar_moneda($metacredito_creditos);?></td>
          <td><?php echo $td_logrado_creditos;?></td>
          <td><?php echo ($porcentaje_creditos * 100).' %';?></td>
          <td><?php echo mostrar_moneda($COBRAR_REAL_CREDITOS);?></td>
        </tr>
        <tr>
          <td>Crédito Menor</td>
          <td><?php echo mostrar_moneda($metacredito_cmenor);?></td>
          <td><?php echo $td_logrado_cmenor;?></td>
          <td><?php echo ($porcentaje_cmenor * 100).' %';?></td>
          <td><?php echo mostrar_moneda($COBRAR_REAL_CMENOR);?></td>
        </tr>
        <tr>
          <td>Venta Remates</td>
          <td><?php echo mostrar_moneda($metacredito_ventacm);?></td>
          <td><?php echo $td_logrado_ventacm;?></td>
          <td><?php echo ($porcentaje_ventacm * 100).' %';?></td>
          <td><?php echo mostrar_moneda($COBRAR_REAL_VENTACM);?></td>
        </tr>
        <tr>
          <td>Venta Vehículos e Inmuebles</td>
          <td><?php echo $metacredito_ventaveh;?></td>
          <td><?php echo $td_logrado_ventaveh;?></td>
          <td><?php echo ($porcentaje_ventaveh * 100).' %';?></td>
          <td><?php echo mostrar_moneda($COBRAR_REAL_VENTAVEH);?></td>
        </tr>
        <tr>
          <th>Bono por complimiento de Metas</th>
          <td colspan="3"><?php echo $td_logrado_bono;?></td>
          <td><?php echo mostrar_moneda($BONO_POR_METAS);?></td>
        </tr>
        <tr>
          <th colspan="4" style="font-size: 13pt;">TOTAL DE COMISIONES REALES A PAGAR</th>
          <th style="font-size: 13pt;"><?php echo mostrar_moneda($TOTAL_COMISIONES_SUMA_REAL);?></th>
        </tr>
      </table>
    </div>
  </div>
</div>