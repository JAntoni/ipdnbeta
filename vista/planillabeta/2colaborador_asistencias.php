<?php
//? 1. CALCULAMOS LA CANTIDAD DE DÍAS FALTADOS PARA PODER OBTENER LA CANTIDAD DE HORAS FALTADOS.
$cantidad_dias_faltados = 0;
$result = $oAsistencia->numero_faltas($GLOBAL_USUARIO_ID, $GLOBAL_MES_ANIO_CODIGO);
if ($result['estado'] == 1) {
  $cantidad_dias_faltados = intval($result['data']['dias_falta']);
}
$result = NULL;

//determinar horas faltadas, si es full time entonce son 8 horas al día faltados, si con part son 4 horas
$horas_diarias_para_descontar = 0;
if ($GLOBAL_PERSONAL_TIPOTURNO == 1)
  $horas_diarias_para_descontar = 8;
else
  $horas_diarias_para_descontar = 4;

$total_horas_faltadas = $cantidad_dias_faltados * $horas_diarias_para_descontar;

$penalidad_falta = 0;
if ($total_horas_faltadas > 0)
  $penalidad_falta = formato_numero(($GLOBAL_COLABORADOR_SUELDO * $total_horas_faltadas) / $GLOBAL_COLABORADOR_HORAS);

//* 2. OBTEMOS INFORMACIÓN DE LAS TARDANZAS PARA PODER CALCULAR LA PENALIDAD POR TARDANZAS
$dts = $oAsistencia->listar_tardanzas_actualizado($GLOBAL_USUARIO_ID, $GLOBAL_MES_ANIO_CODIGO, $GLOBAL_PERSONALCONTRATO_ID);
  //tipo de turno full time, validar que ingresos y salidas no esteb vacias o 00:00:00
  $detalle_extras = '';
  if ($GLOBAL_PERSONAL_TIPOTURNO == 1) {
    if (validar_horas($GLOBAL_COLABORADOR_ING1, $GLOBAL_COLABORADOR_SAL1) && validar_horas($GLOBAL_COLABORADOR_ING2, $GLOBAL_COLABORADOR_SAL2)) {
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
          $mar_ingAM = $dt['tb_asistencia_ing1']; //hora marcada
          $mar_salAM = $dt['tb_asistencia_sal1']; //hora marcada
          $mar_ingPM = $dt['tb_asistencia_ing2']; //hora marcada
          $mar_salPM = $dt['tb_asistencia_sal2']; //hora marcada
          $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas

          if ($just == 0) {

            // Gerson (15-04-23)
            $segundos_total = 0;
            $minutos_total = 0;
            $total_segundos_tarde = 0;
            $total_segundos_am = 0;
            $total_segundos_pm = 0;
            $sabado = date("w", strtotime($dt['tb_asistencia_fec']));
            if ($sabado == 6) { // es sabado
              if ($dt['tb_empresa_id'] == 1) { // sede boulevard
                $ingreso_am = '09:00:00';
              } else {
                if ($dt['tb_asistencia_horing1'] != null) {
                  $ingreso_am = $dt['tb_asistencia_horing1'];
                } else {
                  $ingreso_am = $dt['tb_personalcontrato_ing1'];
                }
              }
            } else {
              if ($dt['tb_asistencia_horing1'] != null) {
                $ingreso_am = $dt['tb_asistencia_horing1'];
              } else {
                $ingreso_am = $dt['tb_personalcontrato_ing1'];
              }
            }

            if ($dt['tb_asistencia_horing2'] != null) {
              $ingreso_pm = $dt['tb_asistencia_horing2'];
            } else {
              $ingreso_pm = $dt['tb_personalcontrato_ing2'];
            }

            $minutos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);
            $minutos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);

            if (strtotime($dt['tb_asistencia_ing1']) < 0 || $dt['tb_asistencia_ing1'] == null) {
              $total_segundos_am = 0;
            } else {
              if (strtotime($dt['tb_asistencia_ing1']) < strtotime($ingreso_am)) {
                $total_segundos_am = 0;
              } else {
                $total_segundos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);
              }
            }
            if (strtotime($dt['tb_asistencia_ing2']) < 0 || $dt['tb_asistencia_ing2'] == null) {
              $total_segundos_pm = 0;
            } else {
              if (strtotime($dt['tb_asistencia_ing2']) < strtotime($ingreso_pm)) {
                $total_segundos_pm = 0;
              } else {
                $total_segundos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);
              }
            }
            $total_segundos_tarde = $total_segundos_am + $total_segundos_pm; // suma de segundos entre am y pm, máximo 300seg = 5min

            $mostrar = 0;
            if ($total_segundos_tarde > 300) {
              $mostrar = 1;

              $sum_min_tardanza_pena = $sum_min_tardanza_pena + round($total_segundos_tarde / 60);
              $sum_min_pena = $sum_min_pena + ($total_segundos_tarde - 300);
            } elseif ($total_segundos_tarde > 0 && $total_segundos_tarde <= 300) {
              $mostrar = 1;

              $sum_min_tardanza = $sum_min_tardanza + round($total_segundos_tarde / 60);
            }

            $marcaje_am = '';
            if ($value['tb_asistencia_ing1'] != null) {
              if ($minutos_am < 0) {
                $segundos_am = 0;
              } else {
                $segundos_am = $minutos_am % 60 % 60 % 60;
              }
            } else {
              $marcaje_am = 'NO MARCÓ';
              $segundos_am = 0;
            }
            $marcaje_pm = '';
            if ($value['tb_asistencia_ing2'] != null) {
              if ($minutos_pm < 0) {
                $segundos_pm = 0;
              } else {
                $segundos_pm = $minutos_pm % 60 % 60 % 60;
              }
            } else {
              $marcaje_pm = 'NO MARCÓ';
              $segundos_pm = 0;
            }

            if ($value['tb_asistencia_ing1'] != null) {
              if ($minutos_am < 0) {
                $minutos_am = 0;
              } else {
                $truncar = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                $minutos_am = intval(($minutos_am / 60) * $truncar) / $truncar;
              }
            } else {
              $minutos_am = 0;
            }
            if ($value['tb_asistencia_ing2'] != null) {
              if ($minutos_pm < 0) {
                $minutos_pm = 0;
              } else {
                $truncar = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                $minutos_pm = intval(($minutos_pm / 60) * $truncar) / $truncar;
              }
            } else {
              $minutos_pm = 0;
            }

            $arr_AM = horas_cumplidas_turno_asignado($GLOBAL_COLABORADOR_ING1, $GLOBAL_COLABORADOR_SAL1, $mar_ingAM, $mar_salAM, 1, $fecha); //devuelve array (estado,tardanza,horas)
            $arr_PM = horas_cumplidas_turno_asignado($GLOBAL_COLABORADOR_ING2, $GLOBAL_COLABORADOR_SAL2, $mar_ingPM, $mar_salPM, 2, $fecha); //devuelve array (estado,tardanza,horas)

          } else {
            $horas_dia += 4;
            $horas_tarde += 4;
          }
          if ($dt['tb_asistencia_ext'] != 0)
            $detalle_extras .= $dt['tb_asistencia_fec'] . ' - ' . $dt['tb_asistencia_ext'] . ' | ';
        }
      }
      $total_horas = $horas_dia + $horas_tarde;

      // GERSON (17-04-23)
      $penalidad_tar = 0.00;
      $minutes_total = 0;
      $minutes_danger = 0;

      $minutes = round($sum_min_pena / 60);
      $penalidad_tar = penalidad_tardanza_final($minutes, $GLOBAL_COLABORADOR_SUELDO, $GLOBAL_COLABORADOR_HORAS);
      $minutes_total = $sum_min_tardanza + $sum_min_tardanza_pena;
      $minutes_danger = round($sum_min_pena / 60);
      //

    } else
      echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: FULL TIME | ing1: '.$GLOBAL_COLABORADOR_ING1.' | sal1: '.$GLOBAL_COLABORADOR_SAL1.' | ing2: '.$GLOBAL_COLABORADOR_ING2.' | sal2: '.$GLOBAL_COLABORADOR_SAL2.'</span>';
  }
  //tipo de turno PART TIME EN LA MAÑANA, validar que ingreso 1 y salida 1 no esté vacío ni 00:00:00
  if ($GLOBAL_PERSONAL_TIPOTURNO == 2) {
    if (validar_horas($GLOBAL_COLABORADOR_ING1, $GLOBAL_COLABORADOR_SAL1)) {
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
          $mar_ingAM = $dt['tb_asistencia_ing1']; //hora marcada
          $mar_salAM = $dt['tb_asistencia_sal1']; //hora marcada
          $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas

          if ($just == 0) {

            // Gerson (15-04-23)
            $segundos_total = 0;
            $minutos_total = 0;
            $total_segundos_tarde = 0;
            $total_segundos_am = 0;
            $sabado = date("w", strtotime($dt['tb_asistencia_fec']));
            if ($sabado == 6) { // es sabado
              if ($dt['tb_empresa_id'] == 1) { // sede boulevard
                $ingreso_am = '09:00:00';
              } else {
                if ($dt['tb_asistencia_horing1'] != null) {
                  $ingreso_am = $dt['tb_asistencia_horing1'];
                } else {
                  $ingreso_am = $dt['tb_personalcontrato_ing1'];
                }
              }
            } else {
              if ($dt['tb_asistencia_horing1'] != null) {
                $ingreso_am = $dt['tb_asistencia_horing1'];
              } else {
                $ingreso_am = $dt['tb_personalcontrato_ing1'];
              }
            }

            $minutos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);

            if (strtotime($dt['tb_asistencia_ing1']) < 0 || $dt['tb_asistencia_ing1'] == null) {
              $total_segundos_am = 0;
            } else {
              if (strtotime($dt['tb_asistencia_ing1']) < strtotime($ingreso_am)) {
                $total_segundos_am = 0;
              } else {
                $total_segundos_am = strtotime($dt['tb_asistencia_ing1']) - strtotime($ingreso_am);
              }
            }

            $total_segundos_tarde = $total_segundos_am; // suma de segundos entre am, máximo 300seg = 5min

            $mostrar = 0;
            if ($total_segundos_tarde > 300) {
              $mostrar = 1;

              $sum_min_tardanza_pena = $sum_min_tardanza_pena + round($total_segundos_tarde / 60);
              $sum_min_pena = $sum_min_pena + ($total_segundos_tarde - 300);
            } elseif ($total_segundos_tarde > 0 && $total_segundos_tarde <= 300) {
              $mostrar = 1;

              $sum_min_tardanza = $sum_min_tardanza + round($total_segundos_tarde / 60);
            }

            $marcaje_am = '';
            if ($value['tb_asistencia_ing1'] != null) {
              if ($minutos_am < 0) {
                $segundos_am = 0;
              } else {
                $segundos_am = $minutos_am % 60 % 60 % 60;
              }
            } else {
              $marcaje_am = 'NO MARCÓ';
              $segundos_am = 0;
            }

            if ($value['tb_asistencia_ing1'] != null) {
              if ($minutos_am < 0) {
                $minutos_am = 0;
              } else {
                $truncar = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                $minutos_am = intval(($minutos_am / 60) * $truncar) / $truncar;
              }
            } else {
              $minutos_am = 0;
            }
          } else {
            $horas_dia += 4;
          }
        }
      }

      //$total_horas = $horas_dia;

      // GERSON (17-04-23)
      $penalidad_tar = 0.00;
      $minutes_total = 0;
      $minutes_danger = 0;

      $minutes = round($sum_min_pena / 60);
      $penalidad_tar = penalidad_tardanza_final($minutes, $GLOBAL_COLABORADOR_SUELDO, $GLOBAL_COLABORADOR_HORAS);
      $minutes_total = $sum_min_tardanza + $sum_min_tardanza_pena;
      $minutes_danger = round($sum_min_pena / 60);
      //

    } else
      echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: PART TIME EN LA MAÑANA</span>';
  }
  //tipo de turno PART TIME EN LA TARDE, validar que el ingreso 2 y salida 2 del usuario no estén vacios ni 00:00
  if ($GLOBAL_PERSONAL_TIPOTURNO == 3) {
    if (validar_horas($GLOBAL_COLABORADOR_ING2, $GLOBAL_COLABORADOR_SAL2)) {
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $fecha = $dt['tb_asistencia_cod']; //es un codigo único que viena a hacer la fecha
          $mar_ingPM = $dt['tb_asistencia_ing1']; //si es turno tarde, su marcacion de ingreso se guarda en asistencia_ing1
          $mar_salPM = $dt['tb_asistencia_sal1']; //si es turno tarde, su marcacion de salida se guarda en asistencia_sal1
          $just = $dt['tb_asistencia_jus']; //justificacion, si es 1 se toma las 8 horas completas

          if ($just == 0) {

            // Gerson (15-04-23)
            $segundos_total = 0;
            $minutos_total = 0;
            $total_segundos_tarde = 0;
            $total_segundos_pm = 0;
            $sabado = date("w", strtotime($dt['tb_asistencia_fec']));
            /* if($sabado==6){ // es sabado
                              if($dt['tb_empresa_id']==1){ // sede boulevard
                                  $ingreso_am = '09:00:00';
                              }else{
                                  if($dt['tb_asistencia_horing1']!=null){
                                      $ingreso_am = $dt['tb_asistencia_horing1'];
                                  }else{
                                      $ingreso_am = $dt['tb_personalcontrato_ing1'];
                                  }
                              }
                          }else{
                              if($dt['tb_asistencia_horing1']!=null){
                                  $ingreso_am = $dt['tb_asistencia_horing1'];
                              }else{
                                  $ingreso_am = $dt['tb_personalcontrato_ing1'];
                              }
                          } */

            if ($dt['tb_asistencia_horing2'] != null) {
              $ingreso_pm = $dt['tb_asistencia_horing2'];
            } else {
              $ingreso_pm = $dt['tb_personalcontrato_ing2'];
            }

            $minutos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);

            if (strtotime($dt['tb_asistencia_ing2']) < 0 || $dt['tb_asistencia_ing2'] == null) {
              $total_segundos_pm = 0;
            } else {
              if (strtotime($dt['tb_asistencia_ing2']) < strtotime($ingreso_pm)) {
                $total_segundos_pm = 0;
              } else {
                $total_segundos_pm = strtotime($dt['tb_asistencia_ing2']) - strtotime($ingreso_pm);
              }
            }
            $total_segundos_tarde = $total_segundos_pm; // suma de segundos entre am y pm, máximo 300seg = 5min

            $mostrar = 0;
            if ($total_segundos_tarde > 300) {
              $mostrar = 1;

              $sum_min_tardanza_pena = $sum_min_tardanza_pena + round($total_segundos_tarde / 60);
              $sum_min_pena = $sum_min_pena + ($total_segundos_tarde - 300);
            } elseif ($total_segundos_tarde > 0 && $total_segundos_tarde <= 300) {
              $mostrar = 1;

              $sum_min_tardanza = $sum_min_tardanza + round($total_segundos_tarde / 60);
            }

            $marcaje_pm = '';
            if ($value['tb_asistencia_ing2'] != null) {
              if ($minutos_pm < 0) {
                $segundos_pm = 0;
              } else {
                $segundos_pm = $minutos_pm % 60 % 60 % 60;
              }
            } else {
              $marcaje_pm = 'NO MARCÓ';
              $segundos_pm = 0;
            }

            if ($value['tb_asistencia_ing2'] != null) {
              if ($minutos_pm < 0) {
                $minutos_pm = 0;
              } else {
                $truncar = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
                $minutos_pm = intval(($minutos_pm / 60) * $truncar) / $truncar;
              }
            } else {
              $minutos_pm = 0;
            }

            //



            $arr_PM = horas_cumplidas_turno_asignado($GLOBAL_COLABORADOR_ING2, $GLOBAL_COLABORADOR_SAL2, $mar_ingPM, $mar_salPM, 2, $fecha); //devuelve array (estado,tardanza,horas)

          } else {
            $horas_tarde += 4;
          }
        }
      }
      //$total_horas = $horas_tarde;

      // GERSON (17-04-23)
      $penalidad_tar = 0.00;
      $minutes_total = 0;
      $minutes_danger = 0;

      $minutes = round($sum_min_pena / 60);
      $penalidad_tar = penalidad_tardanza_final($minutes, $GLOBAL_COLABORADOR_SUELDO, $GLOBAL_COLABORADOR_HORAS);
      $minutes_total = $sum_min_tardanza + $sum_min_tardanza_pena;
      $minutes_danger = round($sum_min_pena / 60);
      //

    } else
      echo '<span style="color: red;">Error: Debe asignar un horario a este colaborador: PART TIME EN LA TARDE</span>';
  }
$dts = NULL;

//? Suma total de penalidades

$TOTAL_PENALIDADES = formato_numero($penalidad_falta + $penalidad_tar);
?>

<table class="table table-hover table-responsive">
  <tr>
    <th>TIPO DESCUENTO</th>
    <th style="text-align: center">TOTAL</th>
    <th style="text-align: center">MONTO</th>
    <th></th>
  </tr>
  <tr>
    <td style="font-weight: bold">FALTAS <?php echo ' / ' . $GLOBAL_USUARIO_ID . ' // ' . $GLOBAL_MES_ANIO_CODIGO; ?></td>
    <td style="text-align: center"><?php echo $total_horas_faltadas . ' hor (' . $cantidad_dias_faltados . ' dias)'; ?></td>
    <td style="text-align: center"><?php echo 'S/. ' . number_format($penalidad_falta, 2); ?></td>
    <td></td>
  </tr>
  <tr>
    <td style="font-weight: bold">TARDANZAS</td>
    <td style="text-align: center"><?php echo $minutes_total . ' min / ' . $minutes_danger . ' min'; ?></td>
    <td style="text-align: center"><?php echo 'S/. ' . number_format($penalidad_tar, 2); ?></td>
    <td style="text-align: center">
      <?php
      if ($minutes_total > 0)
        echo '<a class="btn btn-facebook btn-xs" onclick="asistencia_tardanzas(' . $GLOBAL_USUARIO_ID . ',' . $GLOBAL_PERSONAL_TIPOTURNO . ')"><i class="fa fa-eye"></i></a>';
      ?>
    </td>
  </tr>
  <tr>
    <td style="font-weight: bold" colspan="2">TOTAL DESCUENTOS POR PENALIDAD:</td>
    <td style="text-align: center;"><?php echo 'S/. ' . number_format($TOTAL_PENALIDADES, 2); ?></td>
    <td></td>
  </tr>
</table>

<input type="hidden" id="pg_planilla_faltas_tardanzas" value="<?php echo formato_numero($TOTAL_PENALIDADES);?>"/>