<?php
  require_once('../bono/Bono.class.php');
  $oBono = new Bono();
  require_once('../vacaciones/Vacaciones.class.php');
  $oVacaciones = new Vacaciones();
  require_once('../pagoplanilla/Pagoplanilla.class.php');
  $oPagoplanilla = new Pagoplanilla();

  $TOTAL_SUELDO_BASE = 0;
  $suma_total_bono = 0;
  $suma_total_vacaciones = 0;
  $sueldo_menos_vacaciones_mas_familiar = 0;

  $tr_bono = '';
  //? 1. BONOS OTORGADOS AL COLABORADOR
  $result = $oBono->mostrar_lista_por_fecha_periodo($GLOBAL_FECHA_CONSULTA, $GLOBAL_USUARIO_ID);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $tr_bono .= '
          <tr>
            <td><span class="badge bg-aqua">'.$value['tb_bono_nom'].'<span></td>
            <td>'.$value['tb_bono_det'].'</td>
            <td>'.$value['tb_bono_val'].'</td>
          </tr>
        ';

        $suma_total_bono += formato_numero($value['tb_bono_val']);
      }
    }
  $result = NULL;

  //? 2. VACACIONES OTORGADAS AL COLABORADOR
  $dias_vacaciones = 0; 
  $tr_vacaciones = ''; 
  $detalle_sueldo = 'Sueldo mínimo por ley'; 
  $sueldo_menos_vacaciones = $GLOBAL_COLABORADOR_SUELDO;

  $result = $oVacaciones->listar_vacaciones_mes($GLOBAL_USUARIO_ID, $GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $dias_vacaciones += $value['tb_dias_tomados'];
      }
    }
  $result = NULL;
  
  if($dias_vacaciones > 0){
    $resta_sueldo = 0;
    $sueldo_equivalente_dia = formato_numero($GLOBAL_COLABORADOR_SUELDO / 30);
    $valor_vacaciones_dias = formato_numero($dias_vacaciones * $sueldo_equivalente_dia);
    $sueldo_menos_vacaciones = formato_numero($GLOBAL_COLABORADOR_SUELDO - $valor_vacaciones_dias);
    $detalle_sueldo = 'Al sueldo base: '.$GLOBAL_COLABORADOR_SUELDO.' se le resta ('.$dias_vacaciones.' días de vacaciones) equivalente a '.$sueldo_equivalente_dia.' por día.';
    $suma_total_vacaciones = $valor_vacaciones_dias; //empieza por el valor de días de vacaciones
    $texto_vacaciones = 'Calcula '.$sueldo_equivalente_dia.' (sueldo por día) x '.$dias_vacaciones.' días tomados.';

    //? CONSULTAMOS LOS 6 ULTIMOS MESES PARA CALCULAR EL PROMEDIO DE COMISIONES
    $result = $oPagoplanilla->promedio_sueldo_comisiones_usuario($GLOBAL_USUARIO_ID, $GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA);
      if($result['estado'] == 1){
        $promedio_comisiones = $result['data']['promedio_comisiones'];
        if($promedio_comisiones > 0){
          $comisiones_x_dia = formato_numero($promedio_comisiones / 30);
          $comisiones_para_sumar = $comisiones_x_dia * $dias_vacaciones;
          $texto_vacaciones .= ' Suma el promedio de comisiones: '.$promedio_comisiones.' (6 últimos meses), donde '.$comisiones_x_dia.' (equivalente por día) x '.$dias_vacaciones.' días tomados.';
          $suma_total_vacaciones += $comisiones_para_sumar;
        }
        else
        $texto_vacaciones .= ' Este colaborador no tiene comisiones para calcular';
      }
    $result = NULL;

    $tr_vacaciones .= '
      <tr>
        <td><span class="badge bg-blue">Vacaciones<span></td>
        <td>'.$texto_vacaciones.'</td>
        <td>'.formato_numero($suma_total_vacaciones).'</td>
      </tr>
    ';
  }

  //? 3. VAMOS A CONSULTAR SI EL COLABORADOR TUVO HORAS EXTRAS DE TRABAJO
  $valor_sueldo_hora = formato_numero($GLOBAL_COLABORADOR_SUELDO / $GLOBAL_COLABORADOR_HORAS); // ejemplo: 2500 / 240 horas
  $tr_horas_extras = '';

  $result = $oAsistencia->listar_horas_extras($GLOBAL_USUARIO_ID, $GLOBAL_MES_ANIO_CODIGO);
    if($result['estado'] == 1){
      $detalle_extras = '';
      $monto_pagar_extras = 0;
      foreach($result['data']as $key => $value){
        $extras += $value['tb_asistencia_ext'];
        $detalle_extras .= mostrar_fecha($value['tb_asistencia_fec']).' ('.$value['tb_asistencia_ext'].' min) | ';

        $horas_extras = $value['tb_asistencia_ext'] / 60; //asistencia_ext está en minutos, los cambiamos a horas / 60
        $calculo_extras = $horas_extras * $valor_sueldo_hora;
        //según la ley se le debe pagar el 25% adicional por hora y por día las 2 primeras horas, luego se le paga el 35% a las siguientes horas
        if($horas_extras < 3){
          // ejem: 4.27(sueldo por hora) * 0.25 (25% adicional) * 2 (total horas extras trabajadas en este día)
          $calculo_extras += $valor_sueldo_hora * 0.25 * $horas_extras;
        }
        $monto_pagar_extras += formato_numero($calculo_extras);
      }

      $tr_horas_extras = '
        <tr>
          <td><span class="badge bg-yellow">Horas Extras</span></td>
          <td>Horas extras trabajadas en las fechas: '.$detalle_extras.'</td>
          <td>'.$monto_pagar_extras.'</td>
        </tr>
      ';

      $suma_total_bono += $monto_pagar_extras;
    }
  $result = NULL;

  //? 4. EVALUAMOS SI EL COLABORADOR TIENE ASIGNACION FAMILIAR
  $tr_familiar = '';
  
  if($GLOBAL_COLABORADOR_ASIGFAMILIA > 0){
    $tr_familiar = '
    <tr>
      <td width="20%"><span class="badge bg-purple">Asignación familiar</span></td>
      <td>A todo colaborador que tenga hijos</td>
      <td>'.$GLOBAL_COLABORADOR_ASIGFAMILIA.'</td>
    </tr>';
  }

  $tr_sueldos = '
    <tr>
      <td class=""><span class="badge bg-green detalle-pago">Sueldo Básico</span></td>
      <td>'.$detalle_sueldo.'</td>
      <td>'.$sueldo_menos_vacaciones.'</td>
    </tr>
    '.$tr_familiar.'
    '.$tr_bono.'
    '.$tr_vacaciones.'
    '.$tr_horas_extras;

  $sueldo_menos_vacaciones_mas_familiar = $sueldo_menos_vacaciones + $GLOBAL_COLABORADOR_ASIGFAMILIA;
  $TOTAL_SUELDO_BASE = $sueldo_menos_vacaciones_mas_familiar + $suma_total_bono + $suma_total_vacaciones;

  $tr_sueldos .= '
    <tr>
      <td class="" colspan="2"><span class="badge bg-black detalle-pago">Sueldo Base Total:</span></td>
      <td>'.$TOTAL_SUELDO_BASE.'</td>
    </tr>';
?>
<table id="tbl_bonos" class="table table-striped table-responsive">
  <thead>
    <tr>
      <th>Tipo Pago</th>
      <th>Detalle</th>
      <th>Monto</th>
    </tr>
  </thead>
  <tbody>
    <!-- <div id="desktop" class="">
      <?php echo $tr_sueldos;?>
    </div>
    <div id="mobile" class="">
      <tr>
        <td class=""><span class="badge bg-green detalle-pago">Sueldo Básico</span></td>
      </tr>
      <tr>
        <td><?php echo $detalle_sueldo; ?></td>
      </tr>
      <tr>
        <td><?php echo $sueldo_menos_vacaciones; ?></td>
      </tr>
    </div> -->
    <?php echo $tr_sueldos;?>
  </tbody>
</table>
<!-- SUELDO BRUTO SE REFIERE AL SUELDO FIJO PACTADO EN EL CONTRATO SIN MODIFICACIONES-->
<input type="hidden" id="pg_planilla_sueldo_bruto" value="<?php echo $GLOBAL_COLABORADOR_SUELDO;?>">
<!-- SUELDO NETO ES EL RESULTANTE DE LA RESTA DEL SUELDO BRUTO - DÍAS DE VACACIONES + LA ASIGNACION FAMILIAR-->
<input type="hidden" id="pg_planilla_sueldo_asignacion" value="<?php echo $sueldo_menos_vacaciones_mas_familiar;?>">
<input type="hidden" id="pg_planilla_bono" value="<?php echo $suma_total_bono;?>">
<input type="hidden" id="pg_planilla_vacaciones" value="<?php echo $suma_total_vacaciones;?>">
<style>
  @media (max-width: 576px) {
    #desktop {
      display: none !important;
    }
    #mobile {
      display: inline !important;
    }
  }

  @media (min-width: 577px) {
    #desktop {
      display: inline;
    }
    #mobile {
      display: none;
    }
  }

  /* @media (max-width: 400px) {
    .my-fixed-item {
      top: 145px !important;
      z-index: 10;
    }
    .box-body {
      padding-left: 0px !important;
      padding-right: 0px !important;
    }
    .botones-fase {
      padding-left: 0px !important;
      padding-right: 0px !important;
    }
  }

  @media (min-width: 576px) {
    .my-fixed-item {
      top: 100px !important;
      z-index: 10;
    }
  }

  @media (min-width: 776px) {
    .my-fixed-item {
      top: 1200px !important;
      z-index: 10;
    }
  }

  @media (min-width: 986px) {
    .my-fixed-item {
      top: 80px !important;
      z-index: 10;
    }
  } */
</style>