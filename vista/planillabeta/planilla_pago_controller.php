<?php

//date_default_timezone_set('America/Lima');
require_once("../../core/usuario_sesion.php");

require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../menorgarantia/Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../proveedor/Proveedor.class.php');
$oProveedor = new Proveedor();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once('../planilla/Planilla.class.php');
$oPlanilla = new Planilla();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../pagoplanilla/Pagoplanilla.class.php');
$oPagoplanilla = new Pagoplanilla();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$usuario_id = $_POST['usu_id'];

$monto = $_POST['monto_des']; //monto a pagar si en caso es oficina
$monto_saldo = formato_numero($_POST['monto_saldo']); //saldo que resta por pagar al colaborador
$sueldo_bruto = moneda_mysql($_POST['sueldo_bruto']);
$sueldo_comision = moneda_mysql($_POST['sueldo_comision']);
$tipo = $_POST['tipo']; //1 si es pago por oficina, 2. si es pago por banco
$detalle =  mb_strtoupper($_POST['txt_detalle']) . ' [ ' . mb_strtoupper($_POST['txt_cuopag_obs']) . ' ]';

$sueldo_pagar = $_POST['monto_des']; //? por defecto sería el pago que se haría en efectivo
if(intval($tipo) == 2)
  $sueldo_pagar = formato_numero($_POST['txt_cuopag_montot']);

$mes = $_POST['mes'];
$anio = $_POST['anio'];
$fecha_hoy = date('d-m-Y');

if ($action == 'sueldo' || $action == 'comision') {
  if (!empty($usuario_id) && !empty($monto)) {

    $dts = $oUsuario->mostrarUno($usuario_id);
      if ($dts['estado'] == 1) {
        $usuario_nom = $dts['data']['tb_usuario_nom'] . ' ' . $dts['data']['tb_usuario_ape'];
        $usuario_dni = $dts['data']['tb_usuario_dni'];
      }
    $dts = NULL;

    $dts = $oProveedor->mostrar_por_dni($usuario_dni);
      if ($dts['estado'] == 1) {
        $proveedor_id = $dts['data']['tb_proveedor_id'];
      }
    $dts = NULL;
    
    if (empty($proveedor_id)) {
      $data['estado'] = 0;
      $data['mensaje'] = 'EL colaborador no está registrado como proveedor, revise por favor.';
      echo json_encode($data);
      exit();
    }

    $array_mes = array(
      '01' => 'ENERO', '02' => 'FEBRERO', '03' => 'MARZO', '04' => 'ABRIL', '05' => 'MAYO', '06' => 'JUNIO', '07' => 'JULIO',
      '08' => 'AGOSTO', '09' => 'SEPTIEMBRE', '10' => 'OCTUBRE', '11' => 'NOVIEMBRE', '12' => 'DICIEMBRE'
    );

    
    $pre_titulo = 'PAGO DE HABERES';
    if (moneda_mysql($monto) < moneda_mysql($monto_saldo))
      $pre_titulo = 'ADELANTO DE PAGO DE HABERES';
    
    $det = $pre_titulo.' MENSUALES DEL MES DE ' . $array_mes[$mes] . ' DEL ' . $anio . ' AL COLABORADOR ' . $usuario_nom . '. Su sueldo bruto es: ' . mostrar_moneda($sueldo_bruto) . ' y su sueldo de comision es: ' . mostrar_moneda($sueldo_comision);

    $cuenta = 11;
    $subcuenta = 35;
    $modide = $anio . $mes; //el modid sera la union del anio y mes para poder consultar y no volver a pagar el mismo mes
    $numdoc = $cuenta.'-'.$subcuenta.'-'.$proveedor_id;

    if ($tipo == 1) {

      $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
      $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
      $oEgreso->egreso_fec = fecha_mysql($fecha_hoy);
      $oEgreso->documento_id = 9;
      $oEgreso->egreso_numdoc = $numdoc;
      $oEgreso->egreso_det = $det;
      $oEgreso->egreso_imp = moneda_mysql($monto);
      $oEgreso->egreso_tipcam = 0;
      $oEgreso->egreso_est = 1;
      $oEgreso->cuenta_id = $cuenta;
      $oEgreso->subcuenta_id = $subcuenta;
      $oEgreso->proveedor_id = $proveedor_id;
      $oEgreso->cliente_id = 0;
      $oEgreso->usuario_id = 0;
      $oEgreso->caja_id = 1;
      $oEgreso->moneda_id = 1;
      $oEgreso->modulo_id = 64;
      $oEgreso->egreso_modide = intval($modide);
      $oEgreso->empresa_id = $_SESSION['empresa_id'];
      $oEgreso->egreso_ap = 0;

      $result = $oEgreso->insertar();
      if (intval($result['estado']) == 1) {
        $egreso_id = $result['egreso_id'];
      }

      $oEgreso->modificar_campo($egreso_id, 'tb_egreso_numdoc', $numdoc, 'STR');
      $oEgreso->modificar_campo($egreso_id, 'tb_egreso_detalleplanilla', $detalle, 'STR');

      $data['estado'] = 1;
      $data['mensaje'] = 'Se ha generado el egreso correctamente';
    }
    //? ------ ---------- -------------------------------- AQUI ME QUEDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    if ($tipo == 2) {

      $fec_pago = $_POST['txt_cuopag_fec'];

      $numope = trim($_POST['cuopag_numope']);
      $monto_validar = moneda_mysql($_POST['txt_cuopag_mon']); //elimina las comas y devuelve con 2 decimales
      $moneda_deposito_id = 1; //1 soles, 2 dolares
      $moneda_credito_id = 1; //1 soles, 2 dolares

      $deposito_mon = 0;
      $dts = $oDeposito->validar_num_operacion($numope);
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $deposito_mon += floatval($dt['tb_deposito_mon']);
        }
      }

      if ($deposito_mon == 0) {
        $data['estado'] = 0;
        $data['mensaje'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
        echo json_encode($data);
        exit();
      }

      $ingreso_importe = 0;
      $dts = $oIngreso->lista_ingresos_num_operacion($numope);
      if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
          $tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
          $ingreso_mon_id = intval($dt['tb_moneda_id']);
          $ingreso_importe += $tb_ingreso_imp;
        }
      }

      $monto_usar = abs($deposito_mon) - $ingreso_importe;
      $monto_usar = moneda_mysql($monto_usar);
      //echo 'tooo esto hay een importes: '.$ingreso_importe.' // depo: '.$deposito_mon.' // validar: '.$monto_validar.' // usar: '.$monto_usar.' // moneda ing: '.$ingreso_mon_id.' // tipo cambio: '.$tipocambio_dia; exit();

      if ($monto_validar > $monto_usar) {
        $data['estado'] = 0;
        $data['mensaje'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: ' . mostrar_moneda($monto_validar) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) . ' | validar: ' . $monto_validar . ' | usar: ' . $monto_usar . ' | ingre: ' . $ingreso_importe . ' | cambio: ' . $tipocambio_dia . ' / mond ing: ' . $ingreso_mon_id;
        echo json_encode($data);
        exit();
      }

      $moneda_depo = 'S/.';

      if (!empty($_POST['txt_cuopag_mon'])) {
        $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
        $mon_pagado = $_POST['txt_cuopag_montot']; //monto saldo que paga el cliente despues de aplicar la comision del banco
        $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

        //registro de ingreso
        $cue_id = 43; // INGRESO / EGRESO PORBANCO
        $subcue_id = 148; // BBVA CONTINENTAL
        $mod_id = 0; //cuota pago


        $ing_det = 'INGRESO FICTICIO DE CAJA EN BANCO = Pago abonado en el banco, en la siguente cuenta N°: [ ' . $_POST['txt_num_cuenta'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ] POR CONCEPTO DE=[' . $det . ']';


        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha;
        $oIngreso->documento_id = 8;
        $oIngreso->ingreso_numdoc = '';
        $oIngreso->ingreso_det = $ing_det;
        $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id;
        $oIngreso->cliente_id = 1144;
        $oIngreso->caja_id = 1;
        $oIngreso->moneda_id = 1;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id;
        $oIngreso->ingreso_modide = $modide;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];

        $result = $oIngreso->insertar();
        if (intval($result['estado']) == 1) {
          $ingr_id = $result['ingreso_id'];
        }
        $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
        $oIngreso->ingreso_numope = $_POST['cuopag_numope'];
        $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
        $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
        $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
        $oIngreso->banco_id = $_POST['cmb_banco_id'];
        $oIngreso->ingreso_id = $ingr_id;

        $oIngreso->registrar_datos_deposito_banco();

        $tipo_pag = 'LOS HABERES MENSUALES';
        $egr_det = ' EGRESO FICTICIO DE CAJA EN BANCO = Pago abonado en el banco, en la siguente cuenta N°: [ ' . $_POST['txt_num_cuenta'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ] -- POR CONCEPTO DE =[' . $det . ']';


        //                $det = 'EGRESO FICTICIO POR [ INGRESO ] DE obs : [ ' . $_POST['txt_cuopag_obs'] . ' ]. Pago abonado en el banco, en la siguente cuenta: [ ' . $_POST['hdd_cuenta_dep'] . ' ], la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fecdep'] . ' ], la fecha de validación fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['txt_cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ]';
        $caj_id = 1; //caja id 14 ya que es CAJA AP
        //		$numdoc = $mod_id.'-'.$_POST['hdd_modid'].'-';

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = '';
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($_POST['txt_cuopag_montot']);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cuenta;
        $oEgreso->subcuenta_id = $subcuenta;
        $oEgreso->proveedor_id = $proveedor_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = 1;
        $oEgreso->moneda_id = 1;
        $oEgreso->modulo_id = 0;
        $oEgreso->egreso_modide = intval($modide);
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si
        $result = $oEgreso->insertar();
        if (intval($result['estado']) == 1) {
          $egreso_id = $result['egreso_id'];
        }

        //                echo ' hasta aki estoy entrando al sistemaa';exit();
        $oEgreso->modificar_campo($egreso_id, 'tb_egreso_numdoc', $numdoc, 'STR');
        $oEgreso->modificar_campo($egreso_id, 'tb_egreso_detalleplanilla', $detalle, 'STR');

        $data['estado'] = 1;
        $data['mensaje'] = 'Se ha generado el egreso correctamente';
      }
    }

    //* AQUI GUARDAMOS LOS DATOS DE PLANILLA NECESARIOS PARA EL HISTORIAL
    $tardanzas = moneda_mysql($_POST['pg_planilla_faltas_tardanzas']);
    $compras_colaborador = moneda_mysql($_POST['pg_planilla_compras']);
    $pagoplanilla_descuentos = moneda_mysql($tardanzas + $compras_colaborador);

    $oPagoplanilla->usuario_id = intval($usuario_id);
    $oPagoplanilla->pagoplanilla_suelbruto = moneda_mysql($_POST['pg_planilla_sueldo_bruto']);
    $oPagoplanilla->pagoplanilla_suelneto = moneda_mysql($_POST['pg_planilla_sueldo_asignacion']);
    $oPagoplanilla->pagoplanilla_bono = moneda_mysql($_POST['pg_planilla_bono']);
    $oPagoplanilla->pagoplanilla_vacaciones = moneda_mysql($_POST['pg_planilla_vacaciones']);
    $oPagoplanilla->pagoplanilla_comisiones = moneda_mysql($_POST['pg_planilla_comisiones']);
    $oPagoplanilla->pagoplanilla_descuentos = $pagoplanilla_descuentos;
    $oPagoplanilla->pagoplanilla_quinta = moneda_mysql($_POST['pg_planilla_renta_quinta']);
    $oPagoplanilla->pagoplanilla_aportacion = moneda_mysql($_POST['pg_planilla_aportaciones']);
    $oPagoplanilla->pagoplanilla_montopagado = moneda_mysql($_POST['pg_planilla_sueldo_neto']);
    $oPagoplanilla->pagoplanilla_usureg = intval($_SESSION['usuario_id']);
    $oPagoplanilla->pagoplanilla_mes = $mes;
    $oPagoplanilla->pagoplanilla_anio = $anio;

    //* ANTES DE REGISTRAR EL PAGO PLANILLA, VERIFICAMOS QUE NO HAYA SIDO GUARDADO ANTES, SI EN CASO YA EXISTE PROCEDEMOS ACTUALIZAR
    $result = $oPagoplanilla->listar_pagoplanilla_periodo(intval($usuario_id), $mes, $anio);
      if($result['estado'] == 1){
        $pagoplanilla_id = $result['data']['tb_pagoplanilla_id'];
        $oPagoplanilla->pagoplanilla_id = $pagoplanilla_id;//esto es para que cuando se actualice

        $oPagoplanilla->modificar();

        $data['estado'] = 1;
        $data['mensaje'] = ' / El Pago planilla ha sido ACTUALIZADO correctamente';
      }
      else{
        $oPagoplanilla->insertar(); //quiere decir que no existe un registro en este periodo, procedemos a registrarlo

        $data['estado'] = 1;
        $data['mensaje'] .= ' / El Pago planilla ha sido REGISTRADO correctamente';
      }
    $result = NULL;

    echo json_encode($data);
  } 
  else {
    $data['estado'] = 0;
    $data['mensaje'] = 'El usuario o el monto están vacíos, actualice y revise.';
    echo json_encode($data);
  }
}
?>