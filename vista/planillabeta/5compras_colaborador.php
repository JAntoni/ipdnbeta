<?php
require_once('../menorgarantia/Ventainterna.class.php');
$oVentainterna = new Ventainterna();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
?>
<div class="col-md-6">
  <div class="box box-info shadow-simple">
    <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">COMPRAS COLABORADOR</h3></div>
    <div class="box-body" style="height: 400px; overflow-y: auto;">
      <table class="table table-striped table-bordered">
        <tr>
          <th width="50%">TIPO DESCUENTO</th>
          <th width="15%">MONTO</th>
          <th width="10%">PAGO POR</th>
          <th width="10%"></th>
        </tr>
        <?php
        $COMPRACOLABORADOR_TOTAL_PAGADO = 0.00;
        $COMPRACOLABORADOR_TOTAL_POR_PAGAR = 0;

        $result = $oVentainterna->listar_ventainterna($GLOBAL_USUARIO_ID, $GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA);
          if ($result['estado'] == 1) {
            $tb_ventainterna_mon = 0;
            foreach ($result['data'] as $key => $dt) {

              $pagar = '<span class="badge bg-green">PAGADO</span>';
              $modulo = '';
              if ($dt['tb_ventainterna_est'] == 0) {

                // fecha de actualizacion para pago parcial 07/06/2023, se hizo correciones para poder tener control sobre los pagos parciales
                // a las compra colaborador, antes el ingreso no guardaba el ID de la compra colaborador, por ello no se sabía a quien pertenece los pagos parciales
                // a partir de la fecha 30-06-23 se hizo esta correción
                $procede_parcial = 0; // para distinguir que registro puede usar pago parcial
                $fecha_corte = '2023-06-30';
                $result2 = $oVentainterna->lista_garantia_fuera_actua($GLOBAL_USUARIO_ID, $fecha_corte);
                  if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key => $dt2) {
                      if ($dt2['tb_ventainterna_id'] == $dt['tb_ventainterna_id']) {
                        $procede_parcial = 1; // procede pago parcial de venta interna
                      }
                    }
                  }
                $result2 = NULL;

                //JUAN 01-09-2023
                $result2 = $oIngreso->consulta_ingresos_ventainterna_pago_caja($dt['tb_ventainterna_id']);
                  if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key2 => $value) {
                      $COMPRACOLABORADOR_TOTAL_PAGADO += $value['tb_ingreso_imp'];
                    }
                  }
                $result2 = NULL;

                //GERSON 31-10-23
                $result2 = $oIngreso->consulta_ingresos_ventainterna_pago_banco($dt['tb_ventainterna_id']);
                  if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key2 => $value) {
                      $COMPRACOLABORADOR_TOTAL_PAGADO += $value['tb_ingreso_imp'];
                    }
                  }
                $result2 = NULL;

                $pagar = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="pagar_ventainterna(' . $dt['tb_ventainterna_id'] . ',' . $procede_parcial . ')" title="Pagar"><i class="fa fa-money"></i></a>';
                $tb_ventainterna_mon += $dt['tb_ventainterna_mon'];
              }

              if ($dt['tb_ventainterna_est'] == 1 && $dt['tb_ventainterna_tipo'] == 1) {
                $pagar = '<span class="badge bg-green">PAGADO</span>';
                $modulo = '<span class="badge bg-aqua">PLANILLA</span>';
                $tb_ventainterna_mon += $dt['tb_ventainterna_mon'];

                $result2 = $oIngreso->consulta_ingresos_ventainterna_pago_caja($dt['tb_ventainterna_id']);
                  if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key2 => $value) {
                      $COMPRACOLABORADOR_TOTAL_PAGADO += $value['tb_ingreso_imp'];
                    }
                  }
                $result2 = NULL;
                //GERSON 31-10-23
                $result2 = $oIngreso->consulta_ingresos_ventainterna_pago_banco($dt['tb_ventainterna_id']);
                  if ($result2['estado'] == 1) {
                    foreach ($result2['data'] as $key2 => $value) {
                      $COMPRACOLABORADOR_TOTAL_PAGADO += $value['tb_ingreso_imp'];
                    }
                  }
                $result2 = NULL;
              }

              if ($dt['tb_ventainterna_est'] == 1 && $dt['tb_ventainterna_tipo'] == 0) {
                $modulo = '<span class="badge bg-black">CAJA</span>';
              }

              $resultGar = $oGarantia->mostrarUno($dt['tb_garantia_id']);
                if ($resultGar['estado'] == 1) {
                  $garantia_cre_id = $resultGar['data']['tb_credito_id'];
                }
              $resultGar = NULL;
              
              echo '
                <tr>  
                  <td style="font-weight: bold">COMPRA: ' . substr($dt['tb_garantia_pro'], 0, 50) . '... | Crédito CM-' . str_pad($garantia_cre_id, 4, "0", STR_PAD_LEFT) . '</td>
                  <td style="text-align: center">S/. ' . mostrar_moneda($dt['tb_ventainterna_mon']) . ' | S/. ' . mostrar_moneda($dt['tb_ventainterna_monpag']) . '</td> 
                  <td style="text-align: center">' . $modulo . '</td>
                  <td style="text-align: center">' . $pagar . '</td>
                </tr>
                ';
            }

            $COMPRACOLABORADOR_TOTAL_POR_PAGAR = ($tb_ventainterna_mon - $COMPRACOLABORADOR_TOTAL_PAGADO);

            echo '
              <tr>
                <td>
                  <span class="badge bg-yellow">Aplicando los descuentos por colaborador un total de: </span>
                </td>
                <td colspan="3">
                  <span class="badge bg-yellow">S/. '.mostrar_moneda($COMPRACOLABORADOR_TOTAL_POR_PAGAR, 2) . ' | Pagos por caja: ' . $COMPRACOLABORADOR_TOTAL_PAGADO.'</span>
                </td>
              </tr>
            ';
          }
          else {
            echo '
              <tr>  
                <td style="font-weight: bold">COMPRAS</td>
                <td style="text-align: center">S/. 0</td>
                <td style="text-align: center"></td>
                <td style="text-align: center"></td>
              </tr>';
          }
        $result = NULL;
        ?>
      </table>
    </div>
  </div>
</div>