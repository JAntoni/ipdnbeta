<?php
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../aportaciones/Afps.class.php');
  $oAfps = new Afps();
  require_once('../descuento/Descuento.class.php');
  $oDescuento = new Descuento();

  //? esta suma total comisoines está en 4bonos_total_logrado.php
  $TOTAL_COMISIONES_SUMA_REAL = formato_numero($TOTAL_COMISIONES_SUMA_REAL);
  //? TOTAL_SUELDO_BASE está en 1colaborador_sueldobase y TOTAL_PENALIDADES esta´en 2colaborador_asistencias
  $TOTAL_SUELDO_BRUTO = formato_numero($TOTAL_SUELDO_BASE) - formato_numero($TOTAL_PENALIDADES);

  $mostrar_comisiones = mostrar_moneda($TOTAL_COMISIONES_SUMA_REAL);
  $mostrar_sueldo_bruto = mostrar_moneda($TOTAL_SUELDO_BRUTO);
  $mostrar_suma = mostrar_moneda($TOTAL_COMISIONES_SUMA_REAL + $TOTAL_SUELDO_BRUTO);

  //? 1. ahora hay nueva forma de calcular AFP, si el usuario eleigió por todo su sueldo o solo su base
  //? 1 el AFP solo se aplica al sueldo base, 2 se aplica a todo con comisiones
  $tipo_sueldo = 1;
  $SUELDO_PARA_AFP = $TOTAL_SUELDO_BRUTO ;
  $eleccion = '<span class="badge bg-yellow">El colaborador eligió pagar AFP solo con su Sueldo Base.</span>';

  $result = $oUsuario->sueldoforma_usuario_fecha($GLOBAL_USUARIO_ID, $GLOBAL_MES_CONSULTA.'-'.$GLOBAL_ANIO_CONSULTA);
    if ($result['estado'] == 1) {
      $tipo_sueldo = intval($result['data']['tb_sueldoforma_tip']);
    }
  $result = NULL;
            
  if ($tipo_sueldo == 2) {
    $eleccion = '<span class="badge bg-yellow">El colaborador eligió pagar AFP con su Sueldo Total.</span>';
    $SUELDO_PARA_AFP = $TOTAL_SUELDO_BRUTO + $TOTAL_COMISIONES_SUMA_REAL;
  }

  //? 2. VAMOS A OBTENER LOS VALORES % DE LAS APORTACIONES AFP U ONP DEL COLABORADOR
  $tabla_afp = '';
  $TOTAL_APORTACIONES = 0;

  if ($GLOBAL_COLABORADOR_AFP_ID > 0) {
    $result = $oAfps->aportacion_usuario_tipo_seguro($GLOBAL_COLABORADOR_AFP_ID, $GLOBAL_MES_CONSULTA.'-'.$GLOBAL_ANIO_CONSULTA);
      if ($result['estado'] == 1) {
        $aporte_afp = $result['data']['tb_afps_apor']; //aporte en soles o en % que se aplicará al sueldo para aportar
        $prima_afp = $result['data']['tb_afps_apor2']; //aporte en soles o en % que se aplicará al sueldo para aportar
        $comision_afp = $result['data']['tb_afps_apor3']; //aporte en soles o en % que se aplicará al sueldo para aportar
        $comision_mixta_afp = $result['data']['tb_afps_apor4'];
        $seguro_afp_nom = $result['data']['tb_seguro_nom'];

        $afp_aporte = floatval($SUELDO_PARA_AFP) * floatval($aporte_afp) / 100;
        $afp_prima = floatval($SUELDO_PARA_AFP) * floatval($prima_afp) / 100;
        $afp_comision = floatval($SUELDO_PARA_AFP) * floatval($comision_afp) / 100;

        if($SUELDO_PARA_AFP > 11983.66)
          $afp_prima = 203.72;

        $retenciones = $afp_aporte + $afp_prima + $afp_comision;
        $TOTAL_APORTACIONES += $retenciones;

        $tabla_afp .= '
          <tr>
            <td>' . $seguro_afp_nom . '</td>
            <td align="center">S/. ' . mostrar_moneda($afp_aporte) . ' (' . $aporte_afp . ')</td>
            <td align="center">S/. ' . mostrar_moneda($afp_prima) . ' (' . $prima_afp . ')</td>
            <td align="center">S/. ' . mostrar_moneda($afp_comision) . ' (' . $comision_afp . ')</td>
            <td align="center">S/. ' . mostrar_moneda($retenciones) . '</td>
          </tr>';

        //? para los que stan en COMISION MIXTA, se les cobra todos los diciembres una comision mixta
        if (intval($mes) == 12) {
          $afp_comision_mixta = floatval($SUELDO_PARA_AFP) * floatval($comision_mixta_afp) / 100;
          $retenciones = $afp_comision_mixta;
          $TOTAL_APORTACIONES += $retenciones;

          $tabla_afp .= '
            <tr>
              <td style="font-family:cambria;font-weight: bold">' . $seguro_afp_nom . '</td>
              <td align="center" colspan="2">COMISION MIXTA DICIEMBRE</td>
              <td align="center">S/. ' . mostrar_moneda($afp_comision_mixta) . ' (' . $comision_mixta_afp . ')</td>
              <td align="center">S/. ' . mostrar_moneda($retenciones) . '</td>
            </tr>';
        }
      }
    $result = NULL;
  }

  //? COMO PARTE FINAL VAMOS A OBTENER LA LISTA DE DESCUENTOS
  $tabla_descuento = '';
  $TOTAL_DESCUENTOS = 0;
  $RENTA_QUINTA = 0;

  $result = $oDescuento->listar_descuentos_peridio($GLOBAL_USUARIO_ID, $GLOBAL_FECHA_CONSULTA);
    if($result['estado'] == 1){
      foreach($result['data'] as $value){
        $tipo_descuento = 'SIN TIPO';
        if(intval($value['tb_descuento_tip']) == 1){
          $tipo_descuento = 'Renta Quinta';
          $RENTA_QUINTA += formato_numero($value['tb_descuento_mon']);
        }
        if(intval($value['tb_descuento_tip']) == 2)
          $tipo_descuento = 'Regularizar Créditos';
        if(intval($value['tb_descuento_tip']) == 3)
          $tipo_descuento = 'Otro tipo';

        $tabla_descuento .= '
          <tr>
            <td>'.$tipo_descuento.'</td>
            <td>'.$value['tb_descuento_mon'].'</td>
          </tr>';
        $TOTAL_DESCUENTOS += formato_numero($value['tb_descuento_mon']);
      }
    }
  $result = NULL;

  $FINAL_SUELDO_NETO = formato_numero($TOTAL_SUELDO_BRUTO + $TOTAL_COMISIONES_SUMA_REAL - $TOTAL_APORTACIONES - $COMPRACOLABORADOR_TOTAL_POR_PAGAR - $TOTAL_DESCUENTOS);
?>

<input type="hidden" id="hdd_sueldo_bruto" value="<?php echo formato_numero($mostrar_sueldo_bruto);?>">
<input type="hidden" id="hdd_total_comisiones" value="<?php echo formato_numero($mostrar_comisiones);?>">

<div class="col-md-6">
  <div class="box box-info shadow-simple">
    <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">APORTACIONES DE AFP Y ONP</h3></div>
    <div class="box-body table-responsive">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Elección del Colaborador</th>
          <th>Sueldo Bruto</th>
          <th>Comisiones</th>
          <th>Sueldo B + Comsiones</th>
        </tr>
        <tr>
          <td><?php echo $eleccion;?></td>
          <td><?php echo $mostrar_sueldo_bruto;?></td>
          <td><?php echo $mostrar_comisiones;?></td>
          <td><?php echo $mostrar_suma;?></td>
        </tr>
      </table>
      <br>
      <table class="table table-striped table-bordered">
        <tr>
          <th>ENTIDAD</th>
          <th>APORTE</th>
          <th>PRIMA</th>
          <th>COMI.</th>
          <th>TOTAL</th>
        </tr>
        <?php echo $tabla_afp;?>
      </table>
    </div>
  </div>
</div>

<div class="col-md-6">
  <div class="box box-info shadow-simple">
    <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">FINAL A PAGAR</h3></div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-4">
          <table class="table table-striped table-bordered">
            <tr>
              <th>Tipo Descuento</th>
              <th>Monto Descuento</th>
            </tr>
            <?php echo $tabla_descuento;?>
          </table>
        </div>
        <div class="col-md-8">
          <table class="table table-striped table-bordered">
            <tr>
              <th>Sueldo Bruto + Comisiones</th>
              <th>Aportaciones Obligatorias</th>
              <th>Compras Colaborador</th>
              <th>Sueldo Total Neto</th>
            </tr>
            <tr>
              <td><?php echo $mostrar_suma;?></td>
              <td><?php echo mostrar_moneda($TOTAL_APORTACIONES);?></td>
              <td><?php echo mostrar_moneda($COMPRACOLABORADOR_TOTAL_POR_PAGAR);?></td>
              <td><?php echo mostrar_moneda($FINAL_SUELDO_NETO);?></td>
            </tr>
          </table>
        </div>
      </div>

      <input type="hidden" id="hdd_total_sueldo_neto" value="<?php echo moneda_mysql($FINAL_SUELDO_NETO); ?>">

      <input type="hidden" id="pg_planilla_renta_quinta" value="<?php echo moneda_mysql($RENTA_QUINTA); ?>">
      <input type="hidden" id="pg_planilla_aportaciones" value="<?php echo moneda_mysql($TOTAL_APORTACIONES); ?>">
      <input type="hidden" id="pg_planilla_compras" value="<?php echo moneda_mysql($COMPRACOLABORADOR_TOTAL_POR_PAGAR); ?>">
      <input type="hidden" id="pg_planilla_comisiones" value="<?php echo moneda_mysql($mostrar_comisiones); ?>">
      <input type="hidden" id="pg_planilla_sueldo_neto" value="<?php echo moneda_mysql($FINAL_SUELDO_NETO); ?>">

      <table class="table table-striped table-bordered">
        <tr>
          <th align="center"></th>
        </tr>
        <tr>
          <td align="center">
            <a class="btn btn-success" onclick="pagar_colaborador_form('sueldo',<?php echo $GLOBAL_USUARIO_ID; ?>)"><i class="fa fa-money"></i> PAGAR PLANILLA</a>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>