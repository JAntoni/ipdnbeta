/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
  console.log("================================ cambio 777");
  $("#pago_fil_picker1, #pago_fil_picker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });

  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "9999999.99",
    //        vMax: $("#hdd_saldo").val()
  });

  $(".moneda3").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "999999.99",
  });
  $(".moneda4").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "99999.00",
  });

  var monto = $("#txt_pla_pag_pag").val();

  $("#txt_pla_pag_pag").change(function () {
    var tipo_pago = parseInt($("#hdd_tipo").val());

    if (tipo_pago == 2) {
      valor = $(this).val();
      $("#txt_cuopag_mon").val(valor);

      var monto_deposito = Number($("#txt_cuopag_mon").autoNumeric("get"));
      var monto_comision = Number($("#txt_cuopag_comi").autoNumeric("get"));

      var saldo = 0;
      console.log("aquiii continuaaaaa 1111");
      saldo = monto_deposito - monto_comision;

      if (monto_deposito > 0 && monto_comision >= 0 && saldo > 0) {
        $("#txt_cuopag_montot").autoNumeric("set", saldo.toFixed(2));
        $("#txt_cuopag_monval").autoNumeric("set", saldo.toFixed(2));

        console.log("hasta aki");
        return false;
      } else {
        $("#txt_cuopag_montot").autoNumeric("set", 0);
        $("#txt_cuopag_monval").autoNumeric("set", 0);
      }
    }
  });

  $("#txt_cuopag_mon, #txt_cuopag_comi").change(function (event) {
    var monto_deposito = Number($("#txt_cuopag_mon").autoNumeric("get"));
    var monto_comision = Number($("#txt_cuopag_comi").autoNumeric("get"));
    //            var cuenta_deposito = $('#cmb_cuedep_id').val();
    var saldo = 0;

    saldo = monto_deposito - monto_comision;

    if (monto_deposito > 0 && monto_comision >= 0 && saldo > 0) {
      $("#txt_cuopag_montot").autoNumeric("set", saldo.toFixed(2));
      $("#txt_cuopag_monval").autoNumeric("set", saldo.toFixed(2));

      console.log("hasta aki");
      return false;
    } else {
      $("#txt_cuopag_montot").autoNumeric("set", 0);
      $("#txt_cuopag_monval").autoNumeric("set", 0);
    }
  });
});

$("#form_pago_cola").submit(function (event) {
  event.preventDefault();

  var saldo = $("#hdd_saldo").val();
  var sueldo_total = 0;
  var sueldo_bruto = 0;
  var sueldo_comision = 0;

  sueldo_total = Number(
    $("#txt_pla_pag_pag")
      .val()
      .replace(/[^0-9\.]+/g, "")
  );
  sueldo_bruto = Number(
    $("#hdd_sueldo")
      .val()
      .replace(/[^0-9\.]+/g, "")
  );
  sueldo_comision = Number(
    $("#hdd_comision")
      .val()
      .replace(/[^0-9\.]+/g, "")
  );

  //* VAMOS A DEFINIR AQUI TODOS LOS VALORES IMPORTANTES DEL PAGO DE PLANILLA, ESTOS VALORES ESTÁN FIJADOS EN EL DOM DE: planilla_colaborador.php
  var pg_planilla_sueldo_bruto = $('#pg_planilla_sueldo_bruto').val();
  var pg_planilla_sueldo_asignacion = $('#pg_planilla_sueldo_asignacion').val();
  var pg_planilla_bono = $('#pg_planilla_bono').val();
  var pg_planilla_vacaciones = $('#pg_planilla_vacaciones').val();
  var pg_planilla_faltas_tardanzas = $('#pg_planilla_faltas_tardanzas').val();
  var pg_planilla_renta_quinta = $('#pg_planilla_renta_quinta').val();
  var pg_planilla_aportaciones = $('#pg_planilla_aportaciones').val();
  var pg_planilla_compras = $('#pg_planilla_compras').val();
  var pg_planilla_comisiones = $('#pg_planilla_comisiones').val();
  var pg_planilla_sueldo_neto = $('#pg_planilla_sueldo_neto').val();

  console.log(' // saldo: ' + saldo);

  $.ajax({
    type: "POST",
    url: VISTA_URL + "planillabeta/planilla_pago_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: $("#hdd_action").val(),
      usu_id: $("#hdd_usu_id").val(),
      monto_saldo: saldo,
      monto_des: sueldo_total,
      sueldo_bruto: sueldo_bruto,
      sueldo_comision: sueldo_comision,
      mes: $("#hdd_mes").val(),
      anio: $("#hdd_anio").val(),
      tipo: $("#hdd_tipo").val(), //1 si es pago por oficina, 2. si es pago por banco
      cuopag_numope: $("#txt_cuopag_numope").val(),
      txt_cuopag_mon: $("#txt_cuopag_mon").val(),
      txt_cuopag_tot: $("#txt_cuopag_tot").val(),
      txt_cuopag_montot: $("#txt_cuopag_montot").val(),
      txt_cuopag_fec: $("#txt_cuopag_fec").val(),
      txt_cuopag_obs: $("#txt_cuopag_obs").val(),
      txt_cuopag_comi: $("#txt_cuopag_comi").val(),
      cmb_cuedep_id: $("#cmb_cuedep_id").val(),
      cmb_banco_id: $("#cmb_banco_id").val(),
      txt_num_cuenta: $("#txt_num_cuenta").val(),
      txt_detalle: $("#txt_detalle").val(),
      pg_planilla_sueldo_bruto: pg_planilla_sueldo_bruto,
      pg_planilla_sueldo_asignacion: pg_planilla_sueldo_asignacion,
      pg_planilla_bono: pg_planilla_bono,
      pg_planilla_vacaciones: pg_planilla_vacaciones,
      pg_planilla_faltas_tardanzas: pg_planilla_faltas_tardanzas,
      pg_planilla_renta_quinta: pg_planilla_renta_quinta,
      pg_planilla_aportaciones: pg_planilla_aportaciones,
      pg_planilla_compras: pg_planilla_compras,
      pg_planilla_comisiones: pg_planilla_comisiones,
      pg_planilla_sueldo_neto: pg_planilla_sueldo_neto
    },
    beforeSend: function () {
      //            $('#msj_aportes').show(100);
      //            $('#msj_aportes').html('Ejecutando evento seleccionado...');
    },
    success: function (data) {
      console.log(" desde successss : ");
      console.log(data);
      if (parseInt(data.estado) == 1) {
        planilla_colaborador();
        $("#div_modal_planilla_form").modal("hide");
      }

      //            $('#msj_aportes').html(data.msj);
      swal_success("SISTEMA", data.mensaje, 3000);
    },
    complete: function (data) {
      //console.log(data);
      if (data.statusText != "success") {
        $("#msj_aportes").html("Error: " + data.responseText);
        console.log(data);
      }
    },
  });
});
