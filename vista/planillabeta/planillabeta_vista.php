<?php include(VISTA_URL . 'funciones/operaciones.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $menu_tit; ?>
      <small><?php echo $menu_des; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Planilla</a></li>
      <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
    </ol>
  </section>
  <style type="text/css">
    .c_cursor{
      cursor: pointer;
    }
  </style>
  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header">
        <div class="panel panel-info shadow-simple">
          <div class="panel-body">
            <form class="form-inline">
              <div class="form-group">
                <label for="cmb_planilla_usuario_id" class="">Selecciona un colaborador:</label>
                <select name="cmb_planilla_usuario_id" id="cmb_planilla_usuario_id" class="form-control selectpicker" data-live-search="true" data-max-options="1" data-size="12">
                  <?php require_once 'vista/usuario/usuario_select.php';?>
                </select>
              </div>

              <div class="form-group">
                <label for="cmb_planilla_mes" class="">Selecciona un colaborador:</label>
                <select name="cmb_planilla_mes" id="cmb_planilla_mes" class="form-control selectpicker" data-live-search="true" data-max-options="1" data-size="12">
                  <?php planilla_option_meses(date('m')) //funcion en el archivo operaciones.php;?>
                </select>
              </div>
              <div class="form-group">
                <label for="cmb_planilla_anio" class="">Selecciona un colaborador:</label>
                <select name="cmb_planilla_anio" id="cmb_planilla_anio" class="form-control selectpicker" data-live-search="true" data-max-options="1" data-size="12">
                  <?php planilla_option_anios(date('Y'));?>
                </select>
              </div>
              <div class="form-group">
                <br>
                <button type="button" class="btn btn-success" onclick="planilla_colaborador()">Ver Planilla</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="box-body planilla_colaborador">

      </div>

      <div id="div_planilla_form">
        
      </div>

      <div id="div_detalle_colocaciones">
        
      </div>

      <div id="div_modal_creditomenor_form">
        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
      </div>

      <div id="div_planilla_det_cred">
        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
      </div>
      <div id="div_planilla_marcacion_asistencia">
        <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
      </div>
      <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
      <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
    </div>
  </section>
  <!-- /.content -->
</div>