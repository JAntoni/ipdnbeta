<?php
  require_once('../personalcontrato/Personalcontrato.class.php');
  $oPersonal = new Personalcontrato();
  require_once('../asistencia/Asistencia.class.php');
  $oAsistencia = new Asistencia();

  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $personal_usuario_id = $_POST['usuario_id'];
  $mes_seleccionado = $_POST['mes']; //en formato 01, 02
  $anio_seleccionado = $_POST['anio'];

  $GLOBAL_COLABORADOR_SUELDO = 0;
  $GLOBAL_COLABORADOR_ASIGFAMILIA = 0;
  $GLOBAL_USUARIO_ID = intval($_POST['usuario_id']);
  $GLOBAL_FECHA_CONSULTA = $anio_seleccionado.'-'.$mes_seleccionado.'-01';
  $GLOBAL_MES_CONSULTA = $mes_seleccionado;
  $GLOBAL_ANIO_CONSULTA = $anio_seleccionado;
  $GLOBAL_COLABORADOR_HORAS = 0;
  $FINAL_SUELDO_DEPOSITAR = 0;
  $GLOBAL_PERSONAL_TIPOTURNO = 0;
  $GLOBAL_MES_ANIO_CODIGO = $GLOBAL_MES_CONSULTA.'-'.($GLOBAL_ANIO_CONSULTA - 2000);
  $GLOBAL_COLABORADOR_ING1 = '';
  $GLOBAL_COLABORADOR_SAL1 = '';
  $GLOBAL_COLABORADOR_ING2 = '';
  $GLOBAL_COLABORADOR_SAL2 = '';

  $tabla_contratos = '';
  //? tipos: 1->Inicio Labores, 2->Extensión de Contrato, 3->Ascenso del Colaborador, 4->Finalización de Contrato, 5->Renuncia Voluntaria, 6->Despido por la Empresa
  $usuario_nom = 'Sin Nombre Aun';
  $cargo_nom = 'Sin Cargo Aun';
  $usuario_foto = 'public/images/avatar.png';
  $fecha_actual = $anio_seleccionado.'-'.$mes_seleccionado.'-01'; // le damos el día 1 al mes seleccionado para convertirlo en fecha
  $fecha_finaliza = 'Ya finalizó sus labores';
  $estado_personal = 'INACTIVO';
  
  $result = $oPersonal->mostrar_uno_condicion($personal_usuario_id, 'primero', ''); //? obtener el primer registro de contratos sin especificar el tipo
    if($result['estado'] == 1){
      $inicio_labores = fechaActual($result['data']['tb_personalcontrato_fecini']);
    }
  $result = NULL;

  $result = $oPersonal->mostrar_vigencia_personal($personal_usuario_id, $fecha_actual);
    if($result['estado'] == 1){
      $GLOBAL_PERSONALCONTRATO_ID = intval($result['data']['tb_personalcontrato_id']);
      $empresa_nom = $result['data']['tb_empresa_nomcom'];
      $GLOBAL_PERSONAL_TIPOTURNO = intval($result['data']['tb_personalcontrato_tiptur']);
      $personalcontrato_horas = intval($result['data']['tb_personalcontrato_horas']);
      $usuario_nom = $result['data']['tb_usuario_nom'];

      $GLOBAL_COLABORADOR_HORAS = $personalcontrato_horas;
      $GLOBAL_COLABORADOR_ING1 = $result['data']['tb_personalcontrato_ing1'];
      $GLOBAL_COLABORADOR_SAL1 = $result['data']['tb_personalcontrato_sal1'];
      $GLOBAL_COLABORADOR_ING2 = $result['data']['tb_personalcontrato_ing2'];
      $GLOBAL_COLABORADOR_SAL2 = $result['data']['tb_personalcontrato_sal2'];

      $GLOBAL_COLABORADOR_AFP_ID = intval($result['data']['tb_seguro_id']); //? ID de la aprotacion AFP
      $seguro_nom = $result['data']['tb_seguro_nom'];
      $tipo_turno = 'REVISAR SIN TURNO';
      $cargo_nom = $result['data']['tb_cargo_nom'];
      $GLOBAL_COLABORADOR_CARGO_ID = $result['data']['tb_cargo_id'];
      $GLOBAL_COLABORADOR_CARGO_NOMBRE = $result['data']['tb_cargo_nom'];

      if($GLOBAL_PERSONAL_TIPOTURNO == 1)
        $tipo_turno = 'Full Time';
      if($GLOBAL_PERSONAL_TIPOTURNO == 2)
        $tipo_turno = 'Part Time - Mañana';
      if($GLOBAL_PERSONAL_TIPOTURNO == 3)
        $tipo_turno = 'Part Time - Tarde';

      $fecha_finaliza = fechaActual($result['data']['tb_personalcontrato_fecfin']);
      $estado_personal = 'VIGENTE';

      $GLOBAL_COLABORADOR_SUELDO = $result['data']['tb_personalcontrato_sueldo'];
      $GLOBAL_COLABORADOR_ASIGFAMILIA = $result['data']['tb_personalcontrato_asigfamilia'];
    }
  $result = NULL;
?>
<input type="hidden" id="hhd_personalcontrato_id" value="<?php echo $GLOBAL_PERSONALCONTRATO_ID;?>" />

<div class="panel-body shadow">
  <div class="row">
    <div class="col-md-3"><h5 class=""><b>Cargo:</b> <?php echo $cargo_nom;?></h5></div>
    <div class="col-md-3"><h5 class=""><b>Inicio Labores:</b> <?php echo $inicio_labores;?></h5></div>
    <div class="col-md-3"><h5 class=""><b>Tipo Jornada:</b> <?php echo $tipo_turno;?></h5></div>
    <div class="col-md-3"><h5 class=""><b>Aportación Obligatoria:</b> <?php echo $seguro_nom;?></h5></div>
  </div>
  <div class="row">
    <div class="col-md-3"><h5 class=""><b>Sede de Labores:</b> <?php echo $empresa_nom;?></h5></div>
    <div class="col-md-3"><h5 class=""><b>Horas a Cumplir:</b> <?php echo $personalcontrato_horas.' horas';?></h5></div>
    <div class="col-md-3"><h5 class=""><b>Finaliza Contrato:</b> <?php echo $fecha_finaliza;?></h5></div>
    <div class="col-md-3"><h5 class=""><b>Estado Personal:</b> <?php echo $estado_personal;?></h5></div>
  </div>
</div>

<?php if($estado_personal == 'VIGENTE'):?>
  <div class="alert alert-success alert-dismissible" style="margin-top: 20px;">
    <center><h4><b>INFORMACIÓN DE SUELDO, VACIONES, BONOS Y ASISTENCIAS DEL COLABORADOR: <span style="color: black;"><?php echo $usuario_nom;?></span></b></h4></center>
  </div>

  <div class="row" style="margin-top: 20px;">
    <!--  -->
    <div class="col-md-6">
      <div class="box box-success shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00a65a;">Sueldo, Bonos, Vacaciones</h3></div>
        <div class="box-body max-contenedor">
          <?php require_once('../planillabeta/1colaborador_sueldobase.php');?>
        </div>
      </div>
    </div>

    <!--  -->
    <div class="col-md-6">
      <div class="box box-success shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00a65a;">Asistencias y Faltas</h3></div>
        <div class="box-body max-contenedor">
          <?php require_once('../planillabeta/2colaborador_asistencias.php');?>
        </div>
      </div>
    </div>
  </div>

  <div class="alert alert-info alert-dismissible" style="margin-top: 20px;">
    <center><h4><b>COMISIONES POR TIPO DE SERVICIO DEL COLABORADOR: <span style="color: black;"><?php echo $usuario_nom.' | PERIODO: '.$GLOBAL_MES_CONSULTA.'/'.$GLOBAL_ANIO_CONSULTA;?></span></b></h4></center>
  </div>

  <div class="row" style="margin-top: 20px;">
    <?php require_once('../planillabeta/3planilla_servicios.php');?>
  </div>

  <div class="row" style="margin-top: 10px;">
    <?php require_once('../planillabeta/4bonos_total_logrado.php');?>

    <?php require_once('../planillabeta/5compras_colaborador.php');?>

    <?php require_once('../planillabeta/6aportaciones_pago_final.php');?>
  </div>

<?php endif;?>