function planilla_colaborador() {
  var usuario_id = $("#cmb_planilla_usuario_id").val();
  var mes = $("#cmb_planilla_mes").val();
  var anio = $("#cmb_planilla_anio").val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "planillabeta/planilla_colaborador.php",
    async: true,
    dataType: "html",
    data: {
      usuario_id: usuario_id,
      mes: mes,
      anio: anio
    },
    beforeSend: function () {
    },
    success: function (html) {
      $(".planilla_colaborador").html(html);
    },
    complete: function (data) {
      console.log(data);
    },
  });
}

function pagar_colaborador_form(tipo_accion, usuario_id) {
  Swal.fire({
    title: "SELECCIONE EL LUGAR DONDE REALIZARÁ EL PAGO DE PLANILLA",
    icon: "info",
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: "#3b5998",
    denyButtonColor: "#21ba45",
  }).then((result) => {
    if (result.isConfirmed) {
      pagar_colaborador(tipo_accion, usuario_id, 1);
    } else if (result.isDenied) {
      pagar_colaborador(tipo_accion, usuario_id, 2);
    }
  });
}

function pagar_colaborador(tipo_accion, usuario_id, tipo) {
  var sueldo_total = Number($('#hdd_total_sueldo_neto').val().replace(/[^0-9\.]+/g, ""));
  var sueldo_bruto = Number($('#hdd_sueldo_bruto').val().replace(/[^0-9\.]+/g, ""));
  var sueldo_comision = Number($('#hdd_total_comisiones').val().replace(/[^0-9\.]+/g, ""));
  var mes_seleccionado = $('#cmb_planilla_mes').val();
  var anio_seleccionado = $('#cmb_planilla_anio').val();

  if (sueldo_total <= 0) {
    swal_warning("AVISO", "EL MONTO A DESEMBOLSAR NO PUEDE SER CERO", 2500);
    return false;
  }

  $.ajax({
      type: "POST",
      url: VISTA_URL + "planillabeta/planilla_pago_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: tipo_accion,
          usu_id: usuario_id,
          sueldo_total: sueldo_total,
          sueldo_bruto: sueldo_bruto,
          sueldo_comision: sueldo_comision,
          mes: mes_seleccionado,
          anio: anio_seleccionado,
          tipo: tipo
      }),
      beforeSend: function () {

      },
      success: function (html) {
        $('#div_planilla_form').html(html);
        $('#div_modal_planilla_form').modal('show');
        modal_hidden_bs_modal('div_modal_planilla_form', 'limpiar'); //funcion encontrada en public/js/generales.js
        modal_width_auto('div_modal_planilla_form',60);
        modal_height_auto('div_modal_planilla_form');
      },
      complete: function (data) {
        if (data.statusText != "success") {
          console.log(data)
        }
      }
  });
}

function planilla_detalle_colocaciones(tipo_colocacion, usuario_id, comision_id){
  var mes_seleccionado = $('#cmb_planilla_mes').val();
  var anio_seleccionado = $('#cmb_planilla_anio').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "planillabeta/planilla_detalle_colocaciones.php",
    async: true,
    dataType: "html",
    data: {
      tipo_colocacion: tipo_colocacion,
      usuario_id: usuario_id,
      comision_id: comision_id,
      mes: mes_seleccionado,
      anio: anio_seleccionado
    },
    beforeSend: function () {
    },
    success: function (html) {
      $('#div_detalle_colocaciones').html(html);
      $('#modal_detalle_colocaciones').modal('show');
      modal_hidden_bs_modal('modal_detalle_colocaciones', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_width_auto('modal_detalle_colocaciones', 80);
    },
    complete: function (data) {
      console.log(data);
    }
  });
}

function creditomenor_form(usuario_act, creditomenor_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "creditomenor/creditomenor_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: usuario_act, // PUEDE SER: L, I, M , E
          creditomenor_id: creditomenor_id,
          vista: 'creditomenor'
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
          $('#modal_mensaje').modal('hide');
          if (data != 'sin_datos') {
              $('#div_modal_creditomenor_form').html(data);
              $('#modal_registro_creditomenor').modal('show');

              //desabilitar elementos del form si es L (LEER)
              if (usuario_act == 'L' || usuario_act == 'E')
                  form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

              //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
              modal_width_auto('modal_registro_creditomenor', 95);
              modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
              modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
          } else {
              //llamar al formulario de solicitar permiso
              var modulo = 'creditomenor';
              var div = 'div_modal_creditomenor_form';
              permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
          }
      },
      complete: function (data) {

      },
      error: function (data) {
          $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
          $('#overlay_modal_mensaje').removeClass('overlay').empty();
          $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
          console.log(data.responseText);
      }
  });
}

function pagar_ventainterna(ven,pago_parcial) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "menorgarantia/ventainterna_pago_form.php",
    async: true,
    dataType: "html",
    data: ({
      venin_id: ven,
      vista: 'planilla',
      tipo: 1,
      pago_parcial: pago_parcial
    }),
    beforeSend: function () {
    },
    success: function (html) {
      $('#div_planilla_det_cred').html(html);
      $('#modal_pagarventainterna_registro').modal('show');
      modal_hidden_bs_modal('modal_pagarventainterna_registro', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_width_auto('modal_pagarventainterna_registro', 25);
    },
    complete: function (data) {
    }
  });
}

function asistencia_tardanzas(usuario_id, usuario_turno) {
  var mes_seleccionado = $('#cmb_planilla_mes').val();
  var anio_seleccionado = $('#cmb_planilla_anio').val();
  var personalcontrato_id = $('#hhd_personalcontrato_id').val();
  $.ajax({
      type: "POST",
      url: VISTA_URL + "planillabeta/planilla_marcacion_asistencia.php",
      async: true,
      dataType: "html",
      data: ({
        usuario_id: usuario_id,
        mes: mes_seleccionado,
        anio: anio_seleccionado,
        usuario_turno: usuario_turno,
        personalcontrato_id: personalcontrato_id
      }),
      beforeSend: function () {

      },
      success: function (html) {
        $('#div_planilla_marcacion_asistencia').html(html);
        $('#modal_marcacion_asistencia').modal('show');
        modal_hidden_bs_modal('modal_marcacion_asistencia', 'limpiar'); //funcion encontrada en public/js/generales.js
      },
      complete: function (data) {
        if (data.statusText != "success") {
          console.log(data);
        }
      }
  });
}
$(document).ready(function () {
  console.log("planillaaa al 29-08-2024");

  $("#cmb_planilla_usuario_id, #cmb_planilla_mes, #cmb_planilla_anio").change(function () {
    var usuario_id = $("#cmb_planilla_usuario_id").val();
    if(parseInt(usuario_id) > 0)
      planilla_colaborador();
    else
      alerta_warning('Importante', 'Selecciona un colaborador para ver su planilla');
  });
});