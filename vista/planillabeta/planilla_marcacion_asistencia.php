<?php
require_once("../../core/usuario_sesion.php");
require_once("../asistencia/Asistencia.class.php");
$oAsistencia = new Asistencia();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuario_id = $_POST['usuario_id'];
$mes_anio = $_POST['mes'] . '-' . $_POST['anio'];
$usuario_turno = $_POST['usuario_turno'];
$personalcontrato_id = $_POST['personalcontrato_id'];

// Gerson (15-03-23)
$anio_short = substr($_POST['anio'], 2); // ejemplo 2023 -> 23
$fecha = $_POST['mes'] . '-' . $anio_short;
// Gerson (29-03-23)
$sum_min_tardanza = 0;
$sum_min_tardanza_pena = 0;
$sum_min_pena = 0;
//
$tabla = '';
$result = $oAsistencia->listar_tardanzas_actualizado($usuario_id, $fecha, $personalcontrato_id);
if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $segundos_total = 0;
    $minutos_total = 0;
    $total_segundos_tarde = 0;
    $total_segundos_am = 0;
    $total_segundos_pm = 0;
    //$minutos_am = 0;
    //$minutos_pm = 0;
    // Gerson (15-03-23)
    $sabado = date("w", strtotime($value['tb_asistencia_fec']));
    if ($sabado == 6) { // es sabado
      if ($value['tb_empresa_id'] == 1) { // sede boulevard
        $ingreso_am = '09:00:00';
      } else {
        if ($value['tb_asistencia_horing1'] != null) {
          $ingreso_am = $value['tb_asistencia_horing1'];
        } else {
          $ingreso_am = $value['tb_personalcontrato_ing1'];
        }
      }
    } else {
      if ($value['tb_asistencia_horing1'] != null) {
        $ingreso_am = $value['tb_asistencia_horing1'];
      } else {
        $ingreso_am = $value['tb_personalcontrato_ing1'];
      }
    }

    if ($value['tb_asistencia_horing2'] != null) {
      $ingreso_pm = $value['tb_asistencia_horing2'];
    } else {
      $ingreso_pm = $value['tb_personalcontrato_ing2'];
    }
    //$ingreso_am = '09:00:00';
    //$ingreso_pm = '15:00:00';
    //
    $minutos_am = strtotime($value['tb_asistencia_ing1']) - strtotime($ingreso_am);
    $minutos_pm = strtotime($value['tb_asistencia_ing2']) - strtotime($ingreso_pm);

    // Gerson (24-03-23)
    if (strtotime($value['tb_asistencia_ing1']) < 0 || $value['tb_asistencia_ing1'] == null) {
      $total_segundos_am = 0;
    } else {
      //$total_segundos_am = strtotime($value['tb_asistencia_ing1']) - strtotime($ingreso_am);
      if (strtotime($value['tb_asistencia_ing1']) < strtotime($ingreso_am)) {
        $total_segundos_am = 0;
      } else {
        $total_segundos_am = strtotime($value['tb_asistencia_ing1']) - strtotime($ingreso_am);
      }
    }
    if (strtotime($value['tb_asistencia_ing2']) < 0 || $value['tb_asistencia_ing2'] == null) {
      $total_segundos_pm = 0;
    } else {
      //$total_segundos_pm = strtotime($value['tb_asistencia_ing2']) - strtotime($ingreso_pm);
      if (strtotime($value['tb_asistencia_ing2']) < strtotime($ingreso_pm)) {
        $total_segundos_pm = 0;
      } else {
        $total_segundos_pm = strtotime($value['tb_asistencia_ing2']) - strtotime($ingreso_pm);
      }
    }
    $total_segundos_tarde = $total_segundos_am + $total_segundos_pm; // suma de segundos entre am y pm, máximo 300seg = 5min

    $mostrar = 0;
    $style = '';
    if ($total_segundos_tarde > 300) {
      $mostrar = 1;
      $style = 'style="background-color: rgb(255,0,0,0.18);"';

      $sum_min_tardanza_pena = $sum_min_tardanza_pena + round($total_segundos_tarde / 60);
      $sum_min_pena = $sum_min_pena + ($total_segundos_tarde - 300);
    } elseif ($total_segundos_tarde > 0 && $total_segundos_tarde <= 300) {
      $mostrar = 1;
      $style = 'style="background-color: rgb(225,255,0,0.18);"';

      $sum_min_tardanza = $sum_min_tardanza + round($total_segundos_tarde / 60);
    }

    $marcaje_am = '';
    if ($value['tb_asistencia_ing1'] != null) {
      if ($minutos_am < 0) {
        $segundos_am = 0;
      } else {
        $segundos_am = $minutos_am % 60 % 60 % 60;
      }
    } else {
      $marcaje_am = 'NO MARCÓ';
      $segundos_am = 0;
    }
    $marcaje_pm = '';
    if ($value['tb_asistencia_ing2'] != null) {
      //$segundos_pm = $minutos_pm%60%60%60;
      if ($minutos_pm < 0) {
        $segundos_pm = 0;
      } else {
        $segundos_pm = $minutos_pm % 60 % 60 % 60;
      }
    } else {
      $marcaje_pm = 'NO MARCÓ';
      $segundos_pm = 0;
    }

    //

    //$minutos_am = round($minutos_am / 60);
    //$minutos_pm = round($minutos_pm / 60);

    if ($value['tb_asistencia_ing1'] != null) {
      if ($minutos_am < 0) {
        $minutos_am = 0;
      } else {
        //$minutos_am = round($minutos_am / 60);
        $truncar = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
        $minutos_am = intval(($minutos_am / 60) * $truncar) / $truncar;
      }
    } else {
      $minutos_am = 0;
    }
    if ($value['tb_asistencia_ing2'] != null) {
      if ($minutos_pm < 0) {
        $minutos_pm = 0;
      } else {
        //$minutos_pm = round($minutos_pm / 60);
        $truncar = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
        $minutos_pm = intval(($minutos_pm / 60) * $truncar) / $truncar;
      }
    } else {
      $minutos_pm = 0;
    }

    $clase_am = '';
    $clase_pm = '';

    /* if(intval($minutos_am) > 5)
        $clase_am = 'class="danger"';
      if(intval($minutos_pm) > 5)
        $clase_pm = 'class="danger"';
      
      if(intval($minutos_am) < 0){
        $minutos_am = '<b>NO MARCÓ</b>';
        $clase_am = 'class="danger"';
      }
      if(intval($minutos_pm) < 0){
        $minutos_pm = '<b>NO MARCÓ</b>';
        $clase_pm = 'class="danger"';
      } */

    if ($mostrar == 1) {

      $tabla .= '
          <tr>
            <td style="width: 85px !important;">' . $value['tb_asistencia_cod'] . '</td>
            <td>' . date("g:i:s a", strtotime($ingreso_am)) . '</td>';
      if ($value['tb_asistencia_ing1'] != null) {
        $tabla .= '<td>' . date("g:i:s a", strtotime($value['tb_asistencia_ing1'])) . '</td>';
      } else {
        $tabla .= '<td>' . $value['tb_asistencia_ing1'] . '</td>';
      }
      /* $tabla .= '
            <td '.$style.'><strong>'.$minutos_am.':'.str_pad($segundos_am, 2, "0", STR_PAD_LEFT).' min</strong></td>
            <td>'.date("g:i:s a", strtotime($ingreso_pm)).'</td>'; */
      if ($marcaje_am == '') {
        $tabla .= '<td ' . $style . '><strong>' . $minutos_am . ' min ' . str_pad($segundos_am, 2, "0", STR_PAD_LEFT) . ' seg</strong></td>';
      } else {
        $tabla .= '<td><strong>' . $marcaje_am . '</strong></td>';
      }
      $tabla .= '<td>' . date("g:i:s a", strtotime($ingreso_pm)) . '</td>';
      if ($value['tb_asistencia_ing2'] != null) {
        $tabla .= '<td>' . date("g:i:s a", strtotime($value['tb_asistencia_ing2'])) . '</td>';
      } else {
        $tabla .= '<td>' . $value['tb_asistencia_ing2'] . '</td>';
      }
      // TOTALIZAR MINUTOS TARDE
      $segundos_total = $total_segundos_tarde % 60 % 60 % 60;
      //$minutos_total = round($total_segundos_tarde / 60);
      $truncar_total = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
      $minutos_total = intval(($total_segundos_tarde / 60) * $truncar_total) / $truncar_total;
      //
      if ($marcaje_pm == '') {
        $tabla .= '<td ' . $style . '><strong>' . $minutos_pm . ' min ' . str_pad($segundos_pm, 2, "0", STR_PAD_LEFT) . ' seg</strong></td>';
      } else {
        $tabla .= '<td><strong>' . $marcaje_pm . '</strong></td>';
      }

      $tabla .= '
            <td ' . $style . '><strong>' . $minutos_total . ' min ' . str_pad($segundos_total, 2, "0", STR_PAD_LEFT) . ' seg</strong></td>';


      if ($total_segundos_tarde > 300) {
        // TOTALIZAR MINUTOS TARDE PENALIZADOS
        $segundos_total_pena = ($total_segundos_tarde - 300) % 60 % 60 % 60;
        //$minutos_total_pena = round(($total_segundos_tarde - 300) / 60);
        $truncar_total_pena = 10 ** 0; // Al redondear creaba el problema de al pasar 30seg lo tomaba como 1 min... Ahora solo se cortan los decimales = 0
        $minutos_total_pena = intval((($total_segundos_tarde - 300) / 60) * $truncar_total_pena) / $truncar_total_pena;
        //
        $tabla .= '<td ' . $style . '><strong>' . $minutos_total_pena . ' min ' . str_pad($segundos_total_pena, 2, "0", STR_PAD_LEFT) . ' seg </strong></td></tr>';
      } else {
        $tabla .= '<td><strong></strong></td></tr>';
      }
    }
  }
} else
  $tabla = 'SIN DATOS: ' . $usuario_id . ' / ' . $mes_anio;
$result = NULL;
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_marcacion_asistencia" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #003eff">LISTA DE ASISTENCIAS CON MINUTOS DE TARDANZA</h4>
      </div>

      <div class="modal-body table-responsive">
        <table class="table table-hower table-responsive">
          <thead>
            <tr>
              <th>DIA</th>
              <th>INGRESO AM ASIGNADO</th>
              <th>INGRESO MARCADO</th>
              <th>MINUTOS TARDE</th>
              <th>INGRESO PM ASIGNADO</th>
              <th>INGRESO PM MARCADO</th>
              <th>MINUTOS TARDE</th>
              <th>MINUTOS TARDE TOTAL</th>
              <th>MINUTOS PENALIZADOS</th>
            </tr>
          </thead>
          <tbody>
            <?php echo $tabla; ?>
          </tbody>
        </table>
        <div style="margin-left: auto; width: 40%">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>MINUTOS TARDANZA</th>
                <!-- <th>MINUTOS TARDANZA PENALIZADOS</th> -->
                <th>MINUTOS PENALIZADOS</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="background-color: rgb(225,255,0,0.18);"><strong><?php echo $sum_min_tardanza + $sum_min_tardanza_pena; ?> min</strong></td>
                <!-- <td style="background-color: rgb(255,163,2,0.18);"><strong><?php echo $sum_min_tardanza_pena; ?> min</strong></td> -->
                <td style="background-color: rgb(255,0,0,0.18);"><strong><?php echo round($sum_min_pena / 60); ?> min</strong></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      
      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>