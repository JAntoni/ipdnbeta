<?php
require_once("../../core/usuario_sesion.php");
require_once('../planillabeta/Planilla.class.php');
$oPlanilla = new Planilla();
require_once('../cargocomision/Cargocomision.class.php');
$oComision = new Cargocomision();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../creditogarveh/Creditogarveh.class.php');
$oGarveh = new Creditogarveh();
require_once('../creditoasiveh/Creditoasiveh.class.php');
$oAsiveh = new Creditoasiveh();
require_once('../personalcontrato/Personalcontrato.class.php');
$oPersonal = new Personalcontrato();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

//? tipo: 1 colocacion credito menor, 2 venta garantias, 3 valor sobre costo, venta vehiculos
$tipo_colocacion = $_POST['tipo_colocacion'];
$personal_usuario_id = intval($_POST['usuario_id']);
$comision_id = intval($_POST['comision_id']);
$mes_seleccionado = $_POST['mes'];
$anio_seleccionado = $_POST['anio'];
$fecha_actual = $anio_seleccionado . '-' . $mes_seleccionado . '-01';

$result = $oPersonal->mostrar_vigencia_personal($personal_usuario_id, $fecha_actual);
if ($result['estado'] == 1) {
  $GLOBAL_COLABORADOR_CARGO_ID = $result['data']['tb_cargo_id'];
  $GLOBAL_COLABORADOR_CARGO_NOMBRE = $result['data']['tb_cargo_nom'];
}
$result = NULL;

$usuario_consulta_id = $personal_usuario_id;
//? SI EL CARGO EMPIEZA CON LAS PALABRAS JEFE DE SEDE YA SEA MALL,BOULEVARD
if ($GLOBAL_COLABORADOR_CARGO_ID == 8 || strpos($GLOBAL_COLABORADOR_CARGO_NOMBRE, 'JEFE DE SEDE') !== false)
  $usuario_consulta_id = 0;

//* ------------------------ TIPO 1 - COLOCACIONES DE CREDITO MENOR ---------------------------------------------------
if ($tipo_colocacion == 1) {
  $empresas_id = '';
  $MENOR_TOTAL_COMISIONES = 0;
  $MENOR_TOTAL_LOGRADO = 0;

  $result = $oComision->mostrarUno($comision_id);
  if ($result['estado'] == 1) {
    $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
  }
  $result = NULL;

  $tabla = '
    <table class="table table-striped table-bordered table-responsive">
      <tr>
          <th>TIPO</th>
          <th>COLABORADOR</th>
          <th>ID CREDITO</th>
          <th>CLIENTE</th>
          <th>FECHA</th>
          <th>MONTO</th>
          <th>INTERES</th>
          <th>PORCENTAJE</th>
          <th>COMISIÓN</th>
      </tr>';

  $dtsMenor = $oPlanilla->comision_credito_menor($mes_seleccionado, $anio_seleccionado, $usuario_consulta_id, $empresas_id);
    if ($dtsMenor['estado'] == 1) {
      foreach ($dtsMenor['data'] as $key => $value) {
        $menor_monto_prestado = formato_numero($value['tb_credito_preaco']);
        $MENOR_TOTAL_LOGRADO += formato_numero($value['tb_credito_preaco']);
        //? si el crédito fue colocado al 15% entonces el interés para comisionar será: 15 * 15 / 100 = 2.25, esto se aplica al monto prestado
        $interes_para_comisionar = ($value['tb_credito_int'] * $value['tb_credito_int']) / 100;

        $porcentaje_comision = 0;
        //? en crédito menor el porcentaje de comisión se basa a crobrar el 100% del interés Ejemplo: asesor gana el 100%, jefe de sede gana el 60%                  
        $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $menor_monto_prestado);
          if ($result['estado'] == 1) {
            $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 100%, 60%, 40%
          }
        $result = NULL;

        $comision_a_ganar = $menor_monto_prestado * $interes_para_comisionar / 100; //? ejemplo: 100 * 2.25 / 100
        $comision_valido_colaborador = $comision_a_ganar * $porcentaje_comision / 100;
        $MENOR_TOTAL_COMISIONES += $comision_valido_colaborador;

        $result = $oCliente->mostrarUno($value['tb_cliente_id']);
          if ($result['estado'] == 1) {
            $nombre_cliente = $result['data']['tb_cliente_nom'];
          }
        $result = NULL;

        $result = $oUsuario->mostrarUno($value['tb_credito_usureg']);
          if ($result['estado'] == 1) {
            $nombre_colaborador = $result['data']['tb_usuario_nom'];
            $apellido_colaborador = $result['data']['tb_usuario_ape'];
          }
        $result = NULL;

        $tabla .= '
          <tr>
              <td align="center">COLOCACIÓN</td>
              <td align="right"><b>' . $nombre_colaborador .'</b></td>
              <td align="center"><b><a  onclick="creditomenor_form(\'L\',' . $value['tb_credito_id'] . ')">' . $value['tb_credito_id'] . '</a></b></td>
              <td align="left"><b>' . $nombre_cliente . '</b></td>
              <td align="center"><b>' . mostrar_fecha($value['tb_credito_feccre']) . '</b></td>
              <td align="center">' . formato_numero($menor_monto_prestado) . '</td>
              <td align="center">' . formato_numero($value['tb_credito_int']) . ' %</td>
              <td align="center">' . formato_numero($porcentaje_comision) . ' %</td>
              <td align="center"><span>' . formato_numero($comision_valido_colaborador) . '</span></td>
            </tr>';
      }
    }
  $dtsMenor = NULL;

  $tabla .= '
    <tr>
      <td colspan="8"><b>TOTAL COMISIONES:</b></td>
      <td align="center">' . formato_numero($MENOR_TOTAL_COMISIONES) . '</td>
    </tr>
  </table>';
}

//* ------------------------ TIPO 2 - VENTA DE GARANTÍAS DE CRÉDITOS MENORES ----------------------------------------------------
if ($tipo_colocacion == 2) {
  $empresas_id = '';
  $total_comiMenor = 0;
  $MENOR_TOTAL_COMISIONES = 0;

  $result = $oComision->mostrarUno($comision_id);
    if($result['estado'] == 1){
      $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
    }
  $result = NULL;

  $tabla .= '
		<table class="table table-striped table-bordered">
      <tr>
        <th>ID GARANTIA</th>
        <th>TIPO</th>
        <th>VENDEDOR</th>
        <th>ID CREDITO</th>
        <th>CLIENTE</th>
        <th>FECHA</th>
        <th>MONTO VENTA</th>
        <th>MONTO GANANCIA</th>
        <th>N° CUOTAS PAGADAS</th>
        <th>POR PAGAR</th>
        <th>DIFERENCIA</th>
        <th>PORCENTAJE</th>
        <th>COMISIÓN</th>
    </tr>';

  $result_remate = $oPlanilla->comision_venta_garantia($mes_seleccionado, $anio_seleccionado, $usuario_consulta_id, $empresas_id);
    if($result_remate['estado'] == 1){
      $cantidad_remates = count($result_remate['data']);

      foreach ($result_remate['data'] as $key => $value) {
        $monto_prestado = $value['tb_garantia_val'];
        $monto_solicita_ipdn = $value['tb_ventagarantia_prec1'];
        $monto_vendido_asesor = $value['tb_ventagarantia_prec2'];
        $monto_sobre_costo = $value['tb_ventagarantia_com'];
        $usuario_vendedor_id = $value['tb_usuario_id']; //este es el usuario que vende la ganratía, puede venderse a sí mismo
        $colaborador_id = intval($value['tb_ventagarantia_col']); //este es el colaborador que compra el producto
        $REMATES_TOTAL_LOGRADO += formato_numero($monto_vendido_asesor);
        $total_cuotas_para_descontar = 2; // por defecto debemos descontar 2 meses de interés, para luego calcular la comisión
        $cliente_id = $value['tb_cliente_id'];
        
        //calculamos la ganancia de la venta, que es la resta de lo vendido - lo prestado, pero restamos 2 meses de interés del préstamo
        //esto en caso que el cliente no haya pagado ninguna cuota, si pagó 1 cuota entonces descontamos 1 mes de interés
        if($monto_vendido_asesor >= $monto_solicita_ipdn)
          $ganancia_dela_venta_bruta = $monto_solicita_ipdn - $monto_prestado; // ejemplo: se vendió en 300 y lo prestado fue 150, ganancia sería 150
        else
          $ganancia_dela_venta_bruta = $monto_vendido_asesor - $monto_prestado;

        //vamos a consultar cuántas cuotas se pagó de este crédito y garantía
        $result = $oPlanilla->CuotasPagadas($value['tb_credito_id'], 'COUNT(*)', 2); //esto realiza un SELECT COUNT(*) y tb_cuota_est = 2 pagadas
          if($result['estado'] == 1){
            $cuotas_pagadas = intval($result['data']['COUNT(*)']);
            if($cuotas_pagadas >= 2)
              $total_cuotas_para_descontar = 0;
            if($cuotas_pagadas == 1)
              $total_cuotas_para_descontar = 1;
            if($cuotas_pagadas <= 0)
              $total_cuotas_para_descontar = 2;
          }
        $result = NULL;
        
        $interes_prestamo_descontar = 0;
        $ganancia_dela_venta = $ganancia_dela_venta_bruta; // en caso que el cliente si haya pagado más de 2 cuotas, la ganancia de la venta no se descuenta nada

        if($total_cuotas_para_descontar > 0){
          $interes_prestamo_descontar = formato_numero(($monto_prestado * $value['tb_credito_int'] / 100) * $total_cuotas_para_descontar);
          $ganancia_dela_venta = max(0, $ganancia_dela_venta_bruta - $interes_prestamo_descontar); //? asegura si la resta es negativa lo iguala a 0
        }

        //? VAMOS A OBTENER EL VALOR DEL % A COMISIONAR PARA ESTE CASO                 
        $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $ganancia_dela_venta);
          if ($result['estado'] == 1) {
            $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 25% asesor, 7% jefes
          }
        $result = NULL;
        
        $comision_a_ganar = 0;
        if($ganancia_dela_venta > 0 && $usuario_consulta_id != $colaborador_id  && $monto_vendido_asesor > $monto_prestado){
          $comision_a_ganar = $ganancia_dela_venta * $porcentaje_comision / 100;
          $REMATES_TOTAL_COMISIONES += $comision_a_ganar;
        }
        
        $result = $oCliente->mostrarUno($cliente_id);
          if ($result['estado'] == 1) {
            $nombre_cliente = $result['data']['tb_cliente_nom'];
          }
        $result = NULL;

        $result = $oUsuario->mostrarUno($usuario_vendedor_id);
          if ($result['estado'] == 1) {
            $nombre_colaborador = $result['data']['tb_usuario_nom'];
            $apellido_colaborador = $result['data']['tb_usuario_ape'];
          }
        $result = NULL;
        
        $v_colaborador = '';
        if ($colaborador_id != 0) {
          $v_colaborador = '<span class="badge bg-green">V. Colaborador</span>';
        }

        $tabla .= '
          <tr>
              <td align="center">' . $value['tb_garantia_id'] . '</td>
              <td align="center">VENTA GARANTÍAS</td>
              <td align="right"><b>' . $nombre_colaborador . '</b></td>
              <td align="center"><b><a  onclick="creditomenor_form(\'L\',' . $value['tb_credito_id'] . ')">' . $value['tb_credito_id'] . '</a></b></td>
              <td align="center"><b>' . $nombre_cliente . '</b></td>
              <td align="center"><b>' . mostrar_fecha($value['tb_ventagarantia_fec']) . '</b></td>
              <td align="center">' . formato_numero($monto_vendido_asesor) . ' '.$v_colaborador.'</td>
              <td align="center">' . formato_numero($ganancia_dela_venta_bruta) . '</td>
              <td align="center"><b>' . $cuotas_pagadas . '</b></td>
              <td align="center">' . formato_numero($interes_prestamo_descontar) . '</td>
              <td align="center">' . formato_numero($ganancia_dela_venta) . '</td>
              <td align="center">' . $porcentaje_comision . ' %</td>
              <td align="center"><span>' . formato_numero($comision_a_ganar) . '</span></td>
          </tr>';
      }

      $REMATES_TOTAL_COMISIONES = formato_numero($REMATES_TOTAL_COMISIONES);
      
    }
  $result_remate = NULL;

  $tabla .= '
      <tr>
        <td colspan="12"><b>TOTAL COMISIONES:</b></td>
        <td align="center">' . formato_numero($REMATES_TOTAL_COMISIONES) . '</td>
      </tr>
    </table>';
}

//* ------------------------ TIPO 3 - SOBRE COSTO VENTA DE GARANTÍAS DE CRÉDITOS MENORES ----------------------------------------------------

if ($tipo_colocacion == 3) {
  $empresas_id = '';
  $total_comiMenor = 0;
  $MENOR_TOTAL_SOBRECOSTO = 0;

  $result = $oComision->mostrarUno($comision_id);
    if($result['estado'] == 1){
      $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
    }
  $result = NULL;

  $tabla = '
		<table class="table table-striped table-bordered">
      <tr>
        <th>TIPO</th>
        <th>ID CREDITO</th>
        <th>FECHA</th>
        <th>MONTO</th>
        <th>PORCENTAJE</th>
        <th>COMISIÓN</th>
      </tr>';

  $result_sobrecosto = $oPlanilla->comision_venta_garantia($mes_seleccionado, $anio_seleccionado, $usuario_consulta_id, $empresas_id);
    if ($result_sobrecosto['estado'] == 1) {
      foreach ($result_sobrecosto['data'] as $key => $value) {
        if ($value['tb_ventagarantia_com'] > 0) {
          $monto_sobrecosto = $value['tb_ventagarantia_com'];

          //? VAMOS A OBTENER EL VALOR DEL % A COMISIONAR PARA ESTE CASO
          $porcentaje_comision = 30; // por defecto 30% en el sobrecosto                
          $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $monto_sobrecosto);
            if ($result['estado'] == 1) {
              $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 25% asesor, 7% jefes
            }
          $result = NULL;

          $comision = formato_numero(($monto_sobrecosto * $porcentaje_comision) / 100);
          $MENOR_TOTAL_SOBRECOSTO += $comision;

          $tabla .= '
            <tr>
                <td align="center">Monto Sobrecosto</td>
                <td align="center"><b>' . $value['tb_credito_id'] . '</b></td>
                <td align="center"><b>' . mostrar_fecha($value['tb_ventagarantia_fec']) . '</b></td>
                <td align="center">' . formato_numero($monto_sobrecosto) . '</td>
                <td align="center">' . $porcentaje_comision . ' %</td>
                <td align="center"><span>' . formato_numero($comision) . '</span></td>
            </tr>';
        }
      }
    }
  $result_sobrecosto = NULL;

  $tabla .= '
      <tr>
        <td style="font-family:cambria;font-weight: bold" colspan="5">TOTAL COMISIONES:</td>
        <td align="center">' . formato_numero($MENOR_TOTAL_SOBRECOSTO) . '</td>
      </tr>
    </table>';

  echo $tabla;
}

//* ------------------------ TIPO 4 - VENTA DE VEHÍCULOS  ----------------------------------------------------
if ($tipo_colocacion == 4) {
  $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
  $VENTAVEHICULO_TOTAL_COMISIONES = 0;

  $result = $oComision->mostrarUno($comision_id);
    if($result['estado'] == 1){
      $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
    }
  $result = NULL;

  $tabla = '
    <table class="table table-striped table-bordered">
      <tr>
        <th>TIPO CREDITO</th>
        <th>FECHA</th>
        <th>ASESOR</th>
        <th>CLIENTE</th>
        <th>PLACA</th>
        <th>VEHICULO</th>
        <th>PRECIO VENTA</th>
        <th>TIPO CAMBIO</th>
        <th>PRECIO FINAL</th>
        <th>PORCENTAJE COMISION</th>
        <th>COMISIÓN A PAGAR</th>
      </tr>';

  $result_ventaveh = $oPlanilla->comision_venta_vehiculo($mes_seleccionado, $anio_seleccionado, $usuario_consulta_id, $empresas_id);
    if($result_ventaveh['estado'] == 1){
      foreach ($result_ventaveh['data'] as $key => $value) {
        $moneda_id = $value['tb_moneda_id'];
        $simbolo_moneda = 'S/.';

        $venta_valida = $value['tb_ventavehiculo_mon'];

        //? SOLO EL CARGO 8 : SUB GERENTE COMISIONA POR EL TODO DE LA VENTA VEHÍCULO, PERO LOS ASESORES SOLO COMISIONES POR LA INICIAL TOTAL
        //? YA QUE LUEGO COMISIONAN POR EL CRÉDITO CREADO, PARA CASOS DE VENTA AL CRÉDITO
        if($GLOBAL_COLABORADOR_CARGO_ID != 8){
          //* TODOS LOS ASESORES QUE NO SON SUB GERENTE, VAMOS A OBTENER SU VALOR VÁLIDO PARA LA VENTA VEHÍCULO
          if(formato_numero($value['tb_ventavehiculo_moncre']) != 0) //? quiere decir que fue vendido al crédito
            $venta_valida = $value['tb_ventavehiculo_monini']; //? el asesor solo comisionará del total de inicial, sin el monto de crédito
        }

        $monto_venta = $venta_valida;

        if ($moneda_id == 2){
          $monto_venta = ($venta_valida) * floatval($value['tb_ventavehiculo_tipcam']);
          $simbolo_moneda = 'USD$';
        }

        $porcentaje_comision = 0;              
        $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $monto_venta);
          if ($result['estado'] == 1) {
            $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 100%, 60%, 40%
          }
        $result = NULL;
        
        //* esto indica que si jefe de sede vende vehiculo deja de ganar 0.5 para ganar el 1 %
        if ($value['tb_usuario_id2'] == $personal_usuario_id && strpos($GLOBAL_COLABORADOR_CARGO_NOMBRE, 'JEFE DE SEDE')) {
          $porcentaje_comision = 1;
        }

        $monto_comisionar = formato_numero(($monto_venta * $porcentaje_comision) / 100);
        $VENTAVEHICULO_TOTAL_COMISIONES += $monto_comisionar;

        //obtenemos los datos del vehiculo mediante su placa, consultamos a tablas de garveh y asiveh
        $creditotipo_id = intval($value['tb_creditotipo_id']); // de la tabla tb_ventavehiculo
        $credito_id = intval($value['tb_credito_id']); // de la tabla tb_ventavehiculo
        $marca_vehiculo = 'NaN: CRE ID: '.$credito_id.' / tip: '.$creditotipo_id;
        $modelo_vehiculo = 'NaN';
        $anio_vehiculo = 'NaN';
        $tipo_credito = 'GARVEH';
        
        $result_busca = NULL;
          if($creditotipo_id == 2){
            $result_busca = $oAsiveh->mostrarUno($credito_id);
            $tipo_credito = 'ASIVEH';
          }
          if($creditotipo_id == 3)
            $result_busca = $oGarveh->mostrarUno($credito_id);

          if ($result_busca['estado'] == 1) {
            $marca_vehiculo = $result_busca['data']['tb_vehiculomarca_nom'];
            $modelo_vehiculo = $result_busca['data']['tb_vehiculomodelo_nom'];
            $anio_vehiculo = $result_busca['data']['tb_credito_vehano'];
          }
        $result_busca = NULL;

        $tabla .= '
          <tr>
              <td align="center">' . $tipo_credito . '</td>
              <td align="center">' . mostrar_fecha($value['tb_ventavehiculo_fec']) . '</td>
              <td align="center">' . $value['tb_usuario_nom'] . '</td>
              <td align="center">' . $value['tb_cliente_nom'] . '</td>
              <td align="center">' . $value['tb_vehiculo_pla'] . '</td>
              <td align="center">' . $marca_vehiculo . ' | ' . $modelo_vehiculo . ' | ' . $anio_vehiculo . '</td>
              <td align="center">' . $simbolo_moneda . ' ' . formato_numero($venta_valida) . '</td>
              <td align="center">' . $value['tb_ventavehiculo_tipcam'] . '</td>
              <td align="center">' . '' . formato_numero($monto_venta) . '</td>
              <td align="center">' . $porcentaje_comision . ' % </td>
              <td align="center">' . '' . formato_numero($monto_comisionar) . '</td>
          </tr>';
      }
    }
  $result_vehiculo = NULL;

  $tabla .= '
      <tr>
        <td style="font-family:cambria;font-weight: bold" colspan="10">TOTAL VENTAS:</td>
        <td align="center">' . formato_numero($VENTAVEHICULO_TOTAL_COMISIONES) . '</td>
      </tr>
      </table>';
  echo $tabla;
}

if ($tipo_colocacion == 5) {
  $monto_totalInmueble = 0;
  $tabla .= '
    <table class="tbl_detalle table-hover" style="width: 100%;font-family: cambria;">
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">CRED. ID</th>
            <th id="tabla_cabecera_fila">FECHA</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">DIRECCION</th>
            <th id="tabla_cabecera_fila">MONTO</th>
            <th id="tabla_cabecera_fila">PORCENTAJE</th>
            <th id="tabla_cabecera_fila">COMISION</th>
        </tr>';

  $dtsVentaInmu = $oPlanilla->comision_ventavehiculo($mes, $anio, $usu_id, '', $empresa); //PARA VER LAS VENTAS AL CONTADO TANTO ASIVEH Y GARVEH
  if ($dtsVentaInmu['estado'] == 1) {
    foreach ($dtsVentaInmu['data'] as $key => $dt) {

      $monto = floatval($dt['tb_stockinmueble_pag']);
      //if($dt['tb_moneda_id'] == 2)
      //$monto = floatval($dt['tb_ventavehiculo_mon']) * floatval($dt['tb_ventavehiculo_tipcam']);
      $comision = number_format(($monto * 1) / 100, 2, '.', '');
      $monto_totalInmueble += $comision;
      $tabla .= '
              <tr>
                <td id="tabla_fila" align="center">' . $dt['tb_credito_id'] . '</td>
                <td id="tabla_fila" align="center">' . mostrar_fecha($dt['tb_stockinmueble_fec']) . '</td>
                <td id="tabla_fila" align="center">' . $dt['tb_cliente_nom'] . '</td>
                <td id="tabla_fila" align="center">' . $dt['tb_credito_ubica'] . '</td>
                <td id="tabla_fila" align="center">' . formato_numero($monto) . '</td>
                <td id="tabla_fila" align="center">1%</td>
                <td id="tabla_fila" align="center">' . formato_numero($comision) . '</td>
              </tr>';
    }
  }
  $tabla .= '
      <tr>
        <td id="tabla_fila" style="font-family:cambria;font-weight: bold" colspan="6">TOTAL VENTAS:</td>
        <td id="tabla_fila" align="center">' . formato_numero($monto_totalInmueble) . '</td>
      </tr>
      </table>';
  echo $tabla;
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_detalle_colocaciones" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" id="modalSizeColo" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #003eff">DETALLES DE COLOCACIONES POR COLABORADOR</h4>
      </div>
      <form id="detalle_comisiones">
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-12 table-responsive">
              <?php echo $tabla;?>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
    function adjustModalSizeColo() {
        var modalDialogForm = document.getElementById('modalSizeColo');
        if (window.innerWidth < 768) { // Tamaño 'sm' o menos
            modalDialogForm.classList.remove('modal-lg');
            modalDialogForm.classList.add('modal-xl');
        } else { // Tamaño 'md' o mayor
            modalDialogForm.classList.remove('modal-xl');
            modalDialogForm.classList.add('modal-lg');
        }
    }

    adjustModalSizeColo();

    // Ajustar el tamaño del modal al cambiar el tamaño de la ventana
    window.addEventListener('resize', adjustModalSizeColo);
</script>