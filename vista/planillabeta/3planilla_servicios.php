<?php
  require_once('../cargocomision/Cargocomision.class.php');
  $oComision = new Cargocomision();
  require_once('../planillabeta/Planilla.class.php');
  $oPlanilla = new Planilla();
  require_once('../recaudo/Recaudo.class.php');
  $oRecaudo = new Recaudo();
  require_once('../recaudo/Mejoragc.class.php');
  $oMejoragc = new Mejoragc();

  //? el cargo 8 = SUB GERENTE, 11 JEFE DE SEDE, ambos comisionan todos los créditos de los asesores de venta, por ende se debe listar todos los créditos
  //? y para listar todos los créditos de los asesores, su usuario id a consultar debe ser 0
  $usuario_consulta_id = $GLOBAL_USUARIO_ID;
  if($GLOBAL_COLABORADOR_CARGO_ID == 8 || strpos($GLOBAL_COLABORADOR_CARGO_NOMBRE, 'JEFE DE') !== false) //? SI EL CARGO EMPIEZA CON LAS PALABRAS JEFE DE SEDE YA SEA MALL,BOULE
    $usuario_consulta_id = 0;

  $array_servicios = array();
  $result = $oComision->listar_comisiones_vigentes($GLOBAL_FECHA_CONSULTA, $GLOBAL_COLABORADOR_CARGO_ID);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $array_servicios[$value['tb_serviciocomision_id']] = $value['tb_comision_id'];
      }
    }
  $result = NULL;
?>

<!-- VERIFICAMOS QUÉ SERVICIOS SE PODRÁ COMISIONAR-->

<?php 
  //? SERVICIO 2. SE REFIERE A COMISIONAR SOBRE GARANTIA VEHICULAR
  if(array_key_exists(2, $array_servicios)): ?>
    <div class="col-md-6">
      <div class="box box-info shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">COMISIONES DE: GARANTÍA VEHICULAR</h3></div>
        <div class="box-body max-servicios">
          <?php 
            $comision_id = intval($array_servicios[2]);
            $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
            $tabla_garveh = '';
            $GARVEH_TOTAL_COLOCADO_SOLES = 0;
            $GARVEH_TOTAL_COLOCADO_DOLARES = 0;
            $GARVEH_TOTAL_LOGRADO = 0;
            $GARVEH_TOTAL_COMISIONES = 0;

            $result = $oComision->mostrarUno($comision_id);
              if($result['estado'] == 1){
                $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
              }
            $result = NULL;

            $result_garveh = $oPlanilla->comision_credito_garveh($GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA, $usuario_consulta_id, $empresas_id);
              if($result_garveh['estado'] == 1){
                foreach ($result_garveh['data'] as $key => $value) {

                  if ($value['tb_moneda_id'] == 1) {
                    $moneda = 'S/.';
                    $GARVEH_TOTAL_COLOCADO_SOLES += $value['tb_credito_preaco'];
                  }
                  if ($value['tb_moneda_id'] == 2) {
                    $moneda = 'US$';
                    $GARVEH_TOTAL_COLOCADO_DOLARES += $value['tb_credito_preaco'];
                  }

                  $garveh_tipo = 'GARANTÍA VEHICULAR';

                  if ($value['tb_credito_tip'] == 2) 
                    $garveh_tipo = "ADENDA VEHICULAR";

                  $garveh_monto_prestado = 0;
                  $valido = '';

                  //? ESTO DE MONTO VALIDO, SE REFIERE A QUE HAY CASOS EN DONDE EL MONTO PRESTADO NO ES EL REAL PARA LA COMISIÓN, HAY OCACIONES EN DONDE SE REDUCE EL MONTO PRESTADO PARA COMISIONAR, ESE MONTO VALIDO SE GUARDA EN LA COLUMNA DE _valido
                  if (formato_numero($value['tb_credito_valido']) > 0) {
                    if ($value['tb_moneda_id'] == 2)
                      $garveh_monto_prestado = $value['tb_credito_valido'] * $value['tb_credito_tipcam'];
                    else
                      $garveh_monto_prestado = $value['tb_credito_valido'];
                    $valido = '| <b>' . mostrar_moneda($value['tb_credito_valido']) . '</b>';
                  } 
                  else{
                    if ($value['tb_moneda_id'] == 2)
                      $garveh_monto_prestado = $value['tb_credito_preaco'] * $value['tb_credito_tipcam'];
                    else
                      $garveh_monto_prestado = $value['tb_credito_preaco'];
                  }

                  $GARVEH_TOTAL_LOGRADO += $garveh_monto_prestado;

                  $porcentaje_comision = 0;                  
                  $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $garveh_monto_prestado);
                    if ($result['estado'] == 1) {
                      $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen'];
                    }
                  $result = NULL;
                  
                  //? PERSONAL JUNIOR = 1, TIENE LA CHANCE DE GANAR UN EXTRA DEL 0.3 SI COLOCA CRÉDITOS AL 5%
                  if ($GLOBAL_COLABORADOR_CARGO_ID == 1 && intval($value['tb_credito_int']) >= 5) { 
                    $porcentaje_comision = $porcentaje_comision + 0.3;
                  }

                  $monto_comisionar = formato_numero(($garveh_monto_prestado * $porcentaje_comision) / 100);
                  $GARVEH_TOTAL_COMISIONES += ($garveh_monto_prestado * $porcentaje_comision) / 100;

                  $cliente_nombre = $value['tb_cliente_nom'];
                  if(!empty($value['tb_cliente_emprs']))
                    $cliente_nombre = $value['tb_cliente_emprs'].' - '.$value['tb_cliente_nom'];
                  $vehiculo_marca = $value['tb_vehiculomarca_nom'];
                  $vehiculo_modelo = $value['tb_vehiculomodelo_nom'];
                  $vehiculo_clase = $value['tb_vehiculoclase_nom'];
                  $asesor_nombre = $value['tb_usuario_nom'];

                  $tabla_garveh .='
                    <tr>
                      <td><b onclick="creditogarveh_form(\'L\',' . $value['tb_credito_id'] . ') ">'.$garveh_tipo. ' (CGV-'.$value['tb_credito_id'].') - ['.$cliente_nombre.']</b></td>
                      <td align="center">' . $vehiculo_marca . ' / ' . $vehiculo_modelo . ' / ' . $vehiculo_clase . '</td>
                      <td align="center">' . $asesor_nombre . '</td>
                      <td align="center">' . $moneda . '</td>
                      <td align="center"><b>' .$value['tb_credito_preaco'] . ' ' . $valido . '</b></td>
                      <td align="center"><b>' . $value['tb_credito_int'] . '</b></td>
                      <td align="center"><b>' . $value['tb_credito_tipcam'] . '</b></td>
                      <td align="center"><b>' . $porcentaje_comision . ' %</td>
                      <td align="center"><b>' . formato_numero($monto_comisionar) . '</b></td>
                    </tr>';
                }

                $GARVEH_TOTAL_COMISIONES = formato_numero($GARVEH_TOTAL_COMISIONES);
              }
            $result_garveh = NULL;
          ?>

          <table class="table table-striped table-bordered">
            <tbody>
              <tr>
                <th>Detalle del Crédito</th>
                <th>Marca/ Modelo/ Clase</th>
                <th>Asesor</th>
                <th>Moneda</th>
                <th>Préstamo Acordado</th>
                <th>Interés Crédito</th>
                <th>Tipo Cambio</th>
                <th>Porcentaje</th>
                <th>Comisión</th>
              </tr>
              <?php echo $tabla_garveh;?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="9" style="font-size: 12pt;"><b><?php echo 'Total Colocados en Soles: S/. '.mostrar_moneda($GARVEH_TOTAL_COLOCADO_SOLES).' | Total Colocado en Dólares: US$ '.mostrar_moneda($GARVEH_TOTAL_COLOCADO_DOLARES);?></b></td>
              </tr>
              <tr>
                <td colspan="8" style="font-size: 12pt;"><b>Total Comisiones de Garantía Vehicular</b></td>
                <td><b><?php echo $GARVEH_TOTAL_COMISIONES;?></b></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
<?php endif; ?>

<?php 
  //? SERVICIO 1. SE REFIERE A COMISIONAR SOBRE ASISTENCIA VEHICULAR
  if(array_key_exists(1, $array_servicios)): ?>
    <div class="col-md-6">
      <div class="box box-info shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">COMISIONES DE: ASISTENCIA VEHICULAR</h3></div>
        <div class="box-body max-agrupados">
          <?php 
            $comision_id = intval($array_servicios[1]);
            $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
            $tabla_asiveh = '';
            $ASIVEH_TOTAL_LOGRADO = 0;
            $ASIVEH_TOTAL_COMISIONES = 0;

            $result = $oComision->mostrarUno($comision_id);
              if($result['estado'] == 1){
                $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
              }
            $result = NULL;
            
            $result_asiveh = $oPlanilla->comision_credito_asiveh($GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA, $usuario_consulta_id, $empresas_id);
              if($result_asiveh['estado'] == 1){
                foreach ($result_asiveh['data'] as $key => $value) {
                  $asiveh_tipo = '----';
                  $asiveh_subtipo = '';
                  $moneda = 'S/.';

                  if ($value['tb_moneda_id'] == 2)
                    $moneda = 'US$';
                  if ($value['tb_credito_tip1'] == 1)
                    $asiveh_tipo = "VENTA NUEVA ";
                  if ($value['tb_credito_tip1'] == 2)
                    $asiveh_tipo = "ADENDA ";
                  if ($value['tb_credito_tip1'] == 4)
                    $asiveh_tipo = "RE-VENTA ";

                  if ($value['tb_credito_tip2'] == 1)
                    $asiveh_subtipo = "CREDITO REGULAR";
                  if ($value['tb_credito_tip2'] == 4)
                    $asiveh_subtipo = "CUOTA BALON";

                  $asiveh_monto_prestado = $value['tb_credito_preaco'];
                  if(intval($value['tb_moneda_id']) == 2)
                    $asiveh_monto_prestado = $value['tb_credito_preaco'] * $value['tb_credito_tipcam'];

                  $ASIVEH_TOTAL_LOGRADO += formato_numero($asiveh_monto_prestado);

                  $porcentaje_comision = 0;
                  $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $asiveh_monto_prestado);
                    if ($result['estado'] == 1) {
                      $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen'];
                    }
                  $result = NULL;

                  $monto_comisionar = formato_numero(($asiveh_monto_prestado * $porcentaje_comision) / 100);
                  $ASIVEH_TOTAL_COMISIONES += $monto_comisionar;

                  $tabla_asiveh .= '
                    <tr>
                      <td style="font-family:cambria;font-weight: bold">' . $asiveh_tipo . $asiveh_subtipo . ' (CAV-' . $value['tb_credito_id'] . ')</td>
                      <td>'.$moneda.'</td>
                      <td>'.$value['tb_credito_preaco'].'</td>
                      <td>'.$value['tb_credito_tipcam'].'</td>
                      <td>'.$porcentaje_comision.'%</td>
                      <td>'.$monto_comisionar.'</td>
                    </tr>
                  ';
                }
              }
            $result_asiveh = NULL;
          ?>

          <table class="table table-striped table-bordered">
            <tr>
              <th>Tipo</th>
              <th>Moneda</th>
              <th>Monto Acordado</th>
              <th>Tipo de Cambio</th>
              <th>Porcentaje</th>
              <th>Comisión</th>
            </tr>
            <?php echo $tabla_asiveh;?>
            <tr>
              <td colspan="5" style="font-size: 12pt;"><b>Total Comisiones</b></td>
              <td><?php echo $ASIVEH_TOTAL_COMISIONES;?></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
<?php endif; ?>

<?php 
  //? SERVICIO 3. SE REFIERE A COMISIONAR SOBRE CREDITO HIPOTECARIO
  if(array_key_exists(3, $array_servicios)): ?>
    <div class="col-md-6">
      <div class="box box-info shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">COMISIONES DE: CRÉDITO HIPOTECARIO</h3></div>
        <div class="box-body max-agrupados">
          <?php 
            $comision_id = intval($array_servicios[3]);
            $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
            $tabla_hipo = '';
            $HIPO_TOTAL_LOGRADO = 0;
            $HIPO_TOTAL_COMISIONES = 0;

            $result = $oComision->mostrarUno($comision_id);
              if($result['estado'] == 1){
                $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
              }
            $result = NULL;

            $result_hipo = $oPlanilla->comision_credito_hipotecario($GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA, $usuario_consulta_id, $empresas_id);
              if($result_hipo['estado'] == 1){
                foreach ($result_hipo['data'] as $key => $value) {

                  $moneda = 'S/.';
                  if ($value['tb_moneda_id'] == 2)
                    $moneda = 'US$';

                  $hipo_tipo = 'GARANTÍA VEHICULAR';

                  if ($value['tb_credito_tip'] == 2) 
                    $hipo_tipo = "ADENDA VEHICULAR";

                  $hipo_monto_prestado = 0;
                  $valido = '';

                  //? ESTO DE MONTO VALIDO, SE REFIERE A QUE HAY CASOS EN DONDE EL MONTO PRESTADO NO ES EL REAL PARA LA COMISIÓN, HAY OCACIONES EN DONDE SE REDUCE EL MONTO PRESTADO PARA COMISIONAR, ESE MONTO VALIDO SE GUARDA EN LA COLUMNA DE _valido
                  if (formato_numero($value['tb_credito_valido']) > 0) {
                    if ($value['tb_moneda_id'] == 2)
                      $hipo_monto_prestado = $value['tb_credito_valido'] * $value['tb_credito_tipcam'];
                    else
                      $hipo_monto_prestado = $value['tb_credito_valido'];
                    $valido = '| <b>' . mostrar_moneda($value['tb_credito_valido']) . '</b>';
                  } 
                  else{
                    if ($value['tb_moneda_id'] == 2)
                      $hipo_monto_prestado = $value['tb_credito_preaco'] * $value['tb_credito_tipcam'];
                    else
                      $hipo_monto_prestado = $value['tb_credito_preaco'];
                  }

                  $HIPO_TOTAL_LOGRADO += $hipo_monto_prestado;

                  $porcentaje_comision = 0;                  
                  $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $hipo_monto_prestado);
                    if ($result['estado'] == 1) {
                      $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen'];
                    }
                  $result = NULL;
                  
                  //? PERSONAL JUNIOR = 1, TIENE LA CHANCE DE GANAR UN EXTRA DEL 0.3 SI COLOCA CRÉDITOS AL 5%
                  if ($GLOBAL_COLABORADOR_CARGO_ID == 1 && intval($value['tb_credito_int']) >= 5) { 
                    $porcentaje_comision = $porcentaje_comision + 0.3;
                  }

                  $monto_comisionar = formato_numero(($hipo_monto_prestado * $porcentaje_comision) / 100);
                  $HIPO_TOTAL_COMISIONES += $monto_comisionar;

                  $tabla_hipo .='
                    <tr>
                      <td style="font-family:cambria;font-weight: bold">CRÉDITO HIPOTECARIO (CH-'.$value['tb_credito_id'].') - '.$value['tb_cliente_nom'].'</td>
                      <td align="center">' . $moneda .'</td>
                      <td align="center">' . mostrar_moneda($value['tb_credito_preaco']) . ' ' . $valido . '</td>
                      <td align="center">' . $value['tb_credito_int'] . '</td>
                      <td align="center">' . $porcentaje_comision . ' %</td>
                      <td align="center">S/. ' . mostrar_moneda($monto_comisionar) . '</td>
                    </tr>';
                }
              }
            $result_hipo = NULL;
          ?>

          <table class="table table-striped table-bordered">
            <tbody>
              <tr>
                <th>Detalle del Crédito</th>
                <th>Moneda</th>
                <th>Préstamo Acordado</th>
                <th>Interés Crédito</th>
                <th>Porcentaje</th>
                <th>Comisión</th>
              </tr>
              <?php echo $tabla_hipo;?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="6" style="font-size: 12pt;"><b><?php echo 'Total Colocados en Soles: S/. '.mostrar_moneda($HIPO_TOTAL_LOGRADO);?></b></td>
              </tr>
              <tr>
                <td colspan="5" style="font-size: 12pt;"><b>Total Comisiones de Garantía Hipotecaria</b></td>
                <td><b><?php echo $HIPO_TOTAL_COMISIONES;?></b></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
<?php endif; ?>

<?php 
  //? SERVICIO 4. SE REFIERE A COMISIONAR SOBRE CREDITO MENOR
  if(array_key_exists(4, $array_servicios)): ?>
    <div class="col-md-6">
      <div class="box box-info shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">COMISIONES DE: CRÉDITOS MENORES <?php echo 'car: '.$GLOBAL_COLABORADOR_CARGO_NOMBRE.' / id: '.$usuario_consulta_id.' / '.strpos($GLOBAL_COLABORADOR_CARGO_NOMBRE, 'JEFE DE SEDE');?></h3></div>
        <div class="box-body max-agrupados">
          <?php 
            $comision_id = intval($array_servicios[4]);
            $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
            $tabla_menor = '';
            $MENOR_TOTAL_LOGRADO = 0;
            $MENOR_TOTAL_COMISIONES = 0;
            $MENOR_REMATES_SOBRECOSTO_TOTAL_COMISIONES = 0;

            $result = $oComision->mostrarUno($comision_id);
              if($result['estado'] == 1){
                $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
              }
            $result = NULL;

            $result_menor = $oPlanilla->comision_credito_menor($GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA, $usuario_consulta_id, $empresas_id);
              if($result_menor['estado'] == 1){
                $cantidad_colocacion = count($result_menor['data']);

                foreach ($result_menor['data'] as $key => $value) {
                  $menor_monto_prestado = formato_numero($value['tb_credito_preaco']); 
                  $MENOR_TOTAL_LOGRADO += formato_numero($value['tb_credito_preaco']);
                  //? si el crédito fue colocado al 15% entonces el interés para comisionar será: 15 * 15 / 100 = 2.25, esto se aplica al monto prestado
                  $interes_para_comisionar = ($value['tb_credito_int'] * $value['tb_credito_int']) / 100;

                  $porcentaje_comision = 0;
                  //? en crédito menor el porcentaje de comisión se basa a crobrar el 100% del interés Ejemplo: asesor gana el 100%, jefe de sede gana el 60%                  
                  $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $menor_monto_prestado);
                    if ($result['estado'] == 1) {
                      $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 100%, 60%, 40%
                    }
                  $result = NULL;
                  
                  $comision_a_ganar = $menor_monto_prestado * $interes_para_comisionar / 100; //? ejemplo: 100 * 2.25 / 100
                  $MENOR_TOTAL_COMISIONES += $comision_a_ganar * $porcentaje_comision / 100;

                }
                $MENOR_TOTAL_COMISIONES = formato_numero($MENOR_TOTAL_COMISIONES);
                $MENOR_REMATES_SOBRECOSTO_TOTAL_COMISIONES += $MENOR_TOTAL_COMISIONES;

                $tabla_menor .='
                  <tr>
                    <td style="font-weight: bold">COLOCACIÓN</td>
                    <td align="center">' . $cantidad_colocacion . '</td>
                    <td align="center">S/. ' . mostrar_moneda($MENOR_TOTAL_LOGRADO) . '</td>
                    <td align="center">' . $porcentaje_comision . ' %</td>
                    <td align="center"><span>S/. ' . mostrar_moneda($MENOR_TOTAL_COMISIONES) . '</span></td>
                    <td align="center">
                      <a class="btn_ver_det btn btn-primary btn-xs" onclick="planilla_detalle_colocaciones(1, '.$GLOBAL_USUARIO_ID.', '.$comision_id.')" title="ver créditos menores"><i class="fa fa-eye"></i></a>
                    </td>
                  </tr>';
              }
            $result_menor = NULL;
            //* --------------------------------------- FIN COLOCACION DE CREDITO MENOR ----------------------------------------------------------------

            //* ----------------------------------------------------------------------------------------------------------------------------------------------------------

            //* AQUI MISMO HAREMOS LOS CALCULOS PARA LA VENTA DE REMATES, SI ESTÁ HABILITADO LA OPCION DE COMISIONAR POR REMATES SE HARÁ AQUÍ
            $SOBRECOSTO_CANTIDAD = 0;
            $SOBRECOSTO_TOTAL_LOGRADO = 0;
            
            //? SERVICIO 5. SE REFIERE A COMISIONAR SOBRE VENTA DE PRODUCTOS DE REMATE
            if(array_key_exists(5, $array_servicios)){
              $comision_id = intval($array_servicios[5]);
              $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
              $tabla_remates = '';
              $cantidad_remates = 0;
              $porcentaje_comision = 0;
              $REMATES_TOTAL_LOGRADO = 0;
              $REMATES_TOTAL_COMISIONES = 0;
              $veamos = '';
              
              $result = $oComision->mostrarUno($comision_id);
                if($result['estado'] == 1){
                  $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
                }
              $result = NULL;

              $result_remate = $oPlanilla->comision_venta_garantia($GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA, $usuario_consulta_id, $empresas_id);
                if($result_remate['estado'] == 1){
                  $cantidad_remates = count($result_remate['data']);

                  foreach ($result_remate['data'] as $key => $value) {
                    $monto_prestado = $value['tb_garantia_val'];
                    $monto_solicita_ipdn = $value['tb_ventagarantia_prec1'];
                    $monto_vendido_asesor = $value['tb_ventagarantia_prec2'];
                    $monto_sobre_costo = $value['tb_ventagarantia_com'];
                    $usuario_vendedor_id = $value['tb_usuario_id']; //este es el usuario que vende la ganratía, puede venderse a sí mismo
                    $colaborador_id = $value['tb_ventagarantia_col']; //este es el colaborador que compra el producto
                    $REMATES_TOTAL_LOGRADO += formato_numero($monto_vendido_asesor);
                    $total_cuotas_para_descontar = 2; // por defecto debemos descontar 2 meses de interés, para luego calcular la comisión
                    
                    //$veamos .= ' /// '.$usuario_vendedor_id .' != '.$colaborador_id .' && '.$monto_vendido_asesor .' >= '.$monto_prestado;
                    //* SOLO SE PODRÁ COMISIONAR TODAS LAS VENTAS A CLIENTES O A OTROS COLABORADORES, NO SE PODRÁ COMISIONAR SI LA VENTA SE HIZO A SÍ MISMO
                    //* TAMBIEN VALIDAMOS QUE EL MONTO FINAL VENDIDO SEA IGUAL O MAYOR A LO QUE PEDÍA IPDN, PORQUE PUEDE SER MENOR Y SI ES MENOR, NO SE DEBE COMISIONAR
                    if($GLOBAL_USUARIO_ID != $colaborador_id  && $monto_vendido_asesor > $monto_prestado){
                      //obtenos la información del sobrecosto
                      if($monto_sobre_costo > 0){
                        $SOBRECOSTO_CANTIDAD ++;
                        $SOBRECOSTO_TOTAL_LOGRADO += formato_numero($monto_sobre_costo);
                      }
                      //calculamos la ganancia de la venta, que es la resta de lo vendido - lo prestado, pero restamos 2 meses de interés del préstamo
                      //esto en caso que el cliente no haya pagado ninguna cuota, si pagó 1 cuota entonces descontamos 1 mes de interés
                      if($monto_vendido_asesor >= $monto_solicita_ipdn)
                        $ganancia_dela_venta = $monto_solicita_ipdn - $monto_prestado; // ejemplo: se vendió en 300 y lo prestado fue 150, ganancia sería 150
                      else
                        $ganancia_dela_venta = $monto_vendido_asesor - $monto_prestado;

                      //vamos a consultar cuántas cuotas se pagó de este crédito y garantía
                      $result = $oPlanilla->CuotasPagadas($value['tb_credito_id'], 'COUNT(*)', 2); //esto realiza un SELECT COUNT(*) y tb_cuota_est = 2 pagadas
                        if($result['estado'] == 1){
                          $cuotas_pagadas = intval($result['data']['COUNT(*)']);
                          if($cuotas_pagadas >= 2)
                            $total_cuotas_para_descontar = 0;
                          if($cuotas_pagadas == 1)
                            $total_cuotas_para_descontar = 1;
                          if($cuotas_pagadas <= 0)
                            $total_cuotas_para_descontar = 2;
                        }
                      $result = NULL;

                      if($total_cuotas_para_descontar > 0){
                        $interes_prestamo_descontar = formato_numero(($monto_prestado * $value['tb_credito_int'] / 100) * $total_cuotas_para_descontar);
                        $ganancia_dela_venta = max(0, $ganancia_dela_venta - $interes_prestamo_descontar); //? asegura si la resta es negativa lo iguala a 0
                      }

                      //? VAMOS A OBTENER EL VALOR DEL % A COMISIONAR PARA ESTE CASO                 
                      $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $ganancia_dela_venta);
                        if ($result['estado'] == 1) {
                          $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 25% asesor, 7% jefes
                        }
                      $result = NULL;
                      
                      if($ganancia_dela_venta > 0){
                        $comision_a_ganar = $ganancia_dela_venta * $porcentaje_comision / 100;
                        $REMATES_TOTAL_COMISIONES += $comision_a_ganar;
                      }
                    }
                    //else
                      //$veamos .= '(||) vendedor: '.$usuario_vendedor_id.' / cola: '.$colaborador_id.' // mon vendid: '.$monto_vendido_asesor.' / prest: '.$monto_prestado;
                  }

                  $REMATES_TOTAL_COMISIONES = formato_numero($REMATES_TOTAL_COMISIONES);
                  $MENOR_REMATES_SOBRECOSTO_TOTAL_COMISIONES += $REMATES_TOTAL_COMISIONES;

                  $tabla_remates .='
                    <tr style="height: 25px">
                      <td style="font-family:cambria;font-weight: bold">VENTA GARANTÍAS</td>
                      <td align="center">' . $cantidad_remates . '</td>
                      <td align="center">S/. ' . mostrar_moneda($REMATES_TOTAL_LOGRADO) . '</td>
                      <td align="center"> '.$porcentaje_comision.' %</td>
                      <td align="center">S/. ' . mostrar_moneda($REMATES_TOTAL_COMISIONES) . '</td>
                      <td align="center">
                        <a class="btn_ver_det btn btn-info btn-xs" onclick="planilla_detalle_colocaciones(2, '.$GLOBAL_USUARIO_ID.', '.$comision_id.')" title="ver venta de garantías"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>';
                }
              $result_remate = NULL;

            }
            //* --------------------------------------- FIN LISTA DE REMATES DE PRODUCTOS MENORES ----------------------------------------------------------------

            //* AQUI MISMO HAREMOS LOS CALCULOS PARA LOS SOBRE COSTOS DE REMATES, QUE YA ESTÁN SUMADOS ANTERIORMENTE SOLO SERÍA HACERLOS USO AQUÍ
            //? SERVICIO 6. SE REFIERE A COMISIONAR SOBRE COSTO DE VENTAS DE REMATES
            if(array_key_exists(6, $array_servicios)){
              $comision_id = intval($array_servicios[6]);
              $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
              $tabla_sobrecosto = '';
              $SOBRECOTO_TOTAL_COMISIONES = 0;

              $result = $oComision->mostrarUno($comision_id);
                if($result['estado'] == 1){
                  $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
                }
              $result = NULL;

              if($SOBRECOSTO_CANTIDAD > 0){
                $porcentaje_comision = 0;
                //? VAMOS A OBTENER EL VALOR DEL % A COMISIONAR PARA ESTE CASO                 
                $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $SOBRECOSTO_TOTAL_LOGRADO);
                  if ($result['estado'] == 1) {
                    $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 25% asesor, 7% jefes
                  }
                $result = NULL;
                
                $SOBRECOTO_TOTAL_COMISIONES = formato_numero(($SOBRECOSTO_TOTAL_LOGRADO * $porcentaje_comision) / 100);
                $MENOR_REMATES_SOBRECOSTO_TOTAL_COMISIONES += $SOBRECOTO_TOTAL_COMISIONES;

                $tabla_sobrecosto .='
                  <tr style="height: 25px">
                    <td style="font-family:cambria;font-weight: bold">VALOR SOBRE COSTO</td>
                    <td align="center">' . $SOBRECOSTO_CANTIDAD . '</td>
                    <td align="center">S/. ' . mostrar_moneda($SOBRECOSTO_TOTAL_LOGRADO) . '</td>
                    <td align="center">' . $porcentaje_comision . ' %</td>
                    <td align="center">S/. ' . mostrar_moneda($SOBRECOTO_TOTAL_COMISIONES) . '</td>
                    <td align="center" width="5">
                      <a class="btn_ver_det btn btn-facebook btn-xs" onclick="planilla_detalle_colocaciones(3, '.$GLOBAL_USUARIO_ID.', '.$comision_id.')" title="ver Créditos"><i class="fa fa-eye"></i></a>
                    </td>
                  </tr>';
              }
            }
          ?>
          <table class="table table-striped table-bordered">
            <tbody>
              <tr>
                <th>Tipo</th>
                <th>Cantidad</th>
                <th>Monto Total</th>
                <th>Porcentaje Comisión</th>
                <th>Comisión</th>
                <th>Ver</th>
              </tr>
              <?php echo $tabla_menor . $tabla_remates . $tabla_sobrecosto;?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="4" style="font-size: 12pt;"><b>Total Comisiones Créditos Menores</b></td>
                <td align="center"><b><?php echo $MENOR_REMATES_SOBRECOSTO_TOTAL_COMISIONES;?></b></td>
                <td></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
<?php endif; ?>

<?php 
  //? SERVICIO 7. SE REFIERE A COMISIONAR SOBRE VENTA DE VEHICULOS
  if(array_key_exists(7, $array_servicios)): ?>
    <div class="col-md-6">
      <div class="box box-info shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">COMISIONES DE: VENTAS VEHICULARES E INMUEBLES</h3></div>
        <div class="box-body max-agrupados">
          <?php 
            $comision_id = intval($array_servicios[7]);
            $empresas_id = ''; //? puede ser que solo comisione de 1 sede, o puede ser de varias sedes, ejem: 1,2,3 sedes
            $tabla_ventavehiculo = '';
            $CANTIDAD_VENTAVEH = 0;
            $VENTAVEHICULO_TOTAL_LOGRADO = 0;
            $VENTAVEHICULO_TOTAL_COMISIONES = 0;

            $result = $oComision->mostrarUno($comision_id);
              if($result['estado'] == 1){
                $empresas_id = $result['data']['tb_comision_sedes']; // las sedes asignadas a comisionar, puede ser que comisione en todas las sedes o solo en 1
              }
            $result = NULL;
            
            $result_ventaveh = $oPlanilla->comision_venta_vehiculo($GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA, $usuario_consulta_id, $empresas_id);
              if($result_ventaveh['estado'] == 1){
                $CANTIDAD_VENTAVEH = count($result_ventaveh['data']);

                foreach ($result_ventaveh['data'] as $key => $value) {
                  $moneda_id = $value['tb_moneda_id'];
                  $venta_valida = $value['tb_ventavehiculo_mon'];

                  //? SOLO EL CARGO 8 : SUB GERENTE COMISIONA POR EL TODO DE LA VENTA VEHÍCULO, PERO LOS ASESORES SOLO COMISIONES POR LA INICIAL TOTAL
                  //? YA QUE LUEGO COMISIONAN POR EL CRÉDITO CREADO, PARA CASOS DE VENTA AL CRÉDITO
                  if($GLOBAL_COLABORADOR_CARGO_ID != 8){
                    //* TODOS LOS ASESORES QUE NO SON SUB GERENTE, VAMOS A OBTENER SU VALOR VÁLIDO PARA LA VENTA VEHÍCULO
                    if(formato_numero($value['tb_ventavehiculo_moncre']) != 0) //? quiere decir que fue vendido al crédito
                      $venta_valida = $value['tb_ventavehiculo_monini']; //? el asesor solo comisionará del total de inicial, sin el monto de crédito
                  }
                    
                  $monto_venta = $venta_valida;

                  if ($moneda_id == 2){
                    $monto_venta = ($venta_valida) * floatval($value['tb_ventavehiculo_tipcam']);
                    $VENTAVEHICULO_TOTAL_LOGRADO += $monto_venta;
                  }
                  else{
                    $VENTAVEHICULO_TOTAL_LOGRADO += ($venta_valida);
                  }

                  $porcentaje_comision = 0;              
                  $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $monto_venta);
                    if ($result['estado'] == 1) {
                      $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 100%, 60%, 40%
                    }
                  $result = NULL;
                  
                  $monto_comisionar = formato_numero(($monto_venta * $porcentaje_comision) / 100);
                  $VENTAVEHICULO_TOTAL_COMISIONES += $monto_comisionar;

                  $tabla_ventavehiculo .= '
                    <tr>
                      <td style="font-family:cambria;font-weight: bold">VENTA VEHICULAR</td>
                      <td align="center">' . $value['tb_vehiculo_pla'] . '</td>
                      <td align="center">S/. '.mostrar_moneda($monto_venta).'</td>
                      <td align="center">' . $porcentaje_comision.' %</td>
                      <td align="center"><span>S/. '.mostrar_moneda($monto_comisionar).'</span></td>
                      <td align="center" width="5"></td>
                    </tr>';
                }
              }
            $result_ventaveh = NULL;

            //* DE MANERA ADICIONAL, AGREGAREMOS LA VENTA DE INMUEBLES EN LA ZONA VEHÍCULOS, YA QUE NO SUCEDE A MENUDO
            $empresas_id .= ',0'; //? AGREGAMOS EMPRESA 0 A LA BÚSQUEDA, YA QUE EN INMUEBLES PUEDE TENER ESTE INCONVENIENTE, EVITAMOS EL PROBLEMA
            $result_inmueble = $oPlanilla->comision_venta_inmueble($GLOBAL_MES_CONSULTA, $GLOBAL_ANIO_CONSULTA, $usuario_consulta_id, $empresas_id);
              if($result_inmueble['estado'] == 1){
                $CANTIDAD_VENTAVEH = count($result_inmueble['data']);

                foreach ($result_inmueble['data'] as $key => $value) {
                  $moneda_id = 1;
                  $monto_venta = $value['tb_stockinmueble_pag'];

                  $VENTAVEHICULO_TOTAL_LOGRADO += ($value['tb_stockinmueble_pag']);
                  
                  $porcentaje_comision = 0;              
                  $result = $oComision->mostrar_porcentaje_comision_por_monto($comision_id, $monto_venta);
                    if ($result['estado'] == 1) {
                      $porcentaje_comision = $result['data']['tb_comisiondetalle_porcen']; //? puede ser 100%, 60%, 40%
                    }
                  $result = NULL;
                  
                  $monto_comisionar = formato_numero(($monto_venta * $porcentaje_comision) / 100);
                  $VENTAVEHICULO_TOTAL_COMISIONES += $monto_comisionar;

                  $tabla_ventavehiculo .= '
                    <tr>
                      <td style="font-family:cambria;font-weight: bold; color: green;">VENTA INMUEBLE</td>
                      <td align="center">Propiedad</td>
                      <td align="center">S/. '.mostrar_moneda($monto_venta).'</td>
                      <td align="center">' . $porcentaje_comision.' %</td>
                      <td align="center"><span>S/. '.mostrar_moneda($monto_comisionar).'</span></td>
                      <td align="center" width="5"></td>
                    </tr>';
                }
              }
            $result_inmueble = NULL;
            //* ------------------------------------------------------------------------------------------------------

          ?>

          <table class="table table-striped table-bordered">
            <tbody>
              <tr>
                <th>Tipo</th>
                <th>Placa/Descripción</th>
                <th>Monto Total</th>
                <th>Porcentaje Comisión</th>
                <th>Comisión</th>
                <th>Ver</th>
              </tr>
              <?php echo $tabla_ventavehiculo;?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="4" style="font-size: 12pt;"><b>Total Comisiones Venta Vehículos <?php echo '(EMP: '.$empresas_id.')';?></b></td>
                <td align="center"><b><?php echo formato_numero($VENTAVEHICULO_TOTAL_COMISIONES);?></b></td>
                <td>
                  <?php 
                    echo '<a class="btn_ver_det btn btn-success btn-xs" onclick="planilla_detalle_colocaciones(4,'.$GLOBAL_USUARIO_ID.', '.$comision_id.')" title="ver"><i class="fa fa-eye"></i></a>'; 
                  ?>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
<?php endif; ?>

<?php 
  //? SERVICIO 8. SE REFIERE A COMISIONAR SOBRE GESTION DE COBRANZA
  if(array_key_exists(8, $array_servicios)): ?>
    <div class="col-md-6">
      <div class="box box-info shadow-simple">
        <div class="box-header"><h3 class="box-title" style="font-weight: bold; color: #00c0ef;">COMISIONES DE: GESTIÓN DE COBRANZA</h3></div>
        <div class="box-body">
          <?php 
            $anio_mes_metas = $GLOBAL_ANIO_CONSULTA.'-'.$GLOBAL_MES_CONSULTA; 
            $fecha_inicio_meta = "";
            $fecha_fin_meta = "";
            $RECAUDO_TOTAL_COMISIONES = 0;
            $MEJORAGC_TOTAL_COMISIONES = 0;
            $comentario_recaudo = 'AUN NO HAS DEFINIDO LAS FECHAS PARA LAS METAS DE COBRANZA';
            $comentario_mejoragc = 'AUN NO HAS DEFINIDO LAS FECHAS PARA LAS METAS DE COBRANZA';
            $tabla_gc = '';
            $TOTAL_COMISIONES_GC = 0;
        
            //vamos a obtener la fecha inicio y fecha fin de la meta de RECUADO, puede ser difeernte que la meta de Mejora GC
            //* LAS METAS NO EMPIESAN EXACTAMENTE UN 01 Y TERMINA UN 30 DEL MIMSMO MES, SINO PUEDE EMPEZAR UN 03 DEL MES Y TERMINAR UN 04 DEL OTRO MES, ES POR ELLO
            //* QUE SE OBTIENE LAS FECHAS DE INICIO Y FIN DE LAS METAS
            $result = $oRecaudo->obtener_fecha_inicio_fin_por_anio_mes($anio_mes_metas);
              if($result['estado'] == 1){
                $fecha_inicio_meta = $result['data']['tb_recaudo_fec1'];
                $fecha_fin_meta = $result['data']['tb_recaudo_fec2'];
                $comentario_recaudo = 'METAS DEL '.mostrar_fecha($fecha_inicio_meta).' HASTA '.mostrar_fecha($fecha_fin_meta);
              }
            $result = NULL;
            
            //COMISIONES POR RECAUDACIÓN EN CAJA SEMANAL
            $result = $oRecaudo->pagar_mes_usuario($GLOBAL_USUARIO_ID, $fecha_inicio_meta, $fecha_fin_meta);
              if ($result['estado'] == 1) {
                $RECAUDO_TOTAL_COMISIONES = formato_numero($result['data']['pago_recaudo']);
              }
            $result = NULL;
        
            //COMISIONES POR DISMINUCIÓN DE GC
            $result = $oMejoragc->obtener_fecha_inicio_fin_por_anio_mes($anio_mes_metas);
              if($result['estado'] == 1){
                $fecha_inicio_meta = $result['data']['tb_mejoragc_fec1'];
                $fecha_fin_meta = $result['data']['tb_mejoragc_fec2'];
                $comentario_mejoragc = 'METAS DEL '.mostrar_fecha($fecha_inicio_meta).' HASTA '.mostrar_fecha($fecha_fin_meta);
              }
            $result = NULL;
        
            $result2 = $oMejoragc->pagar_mes_usuario($GLOBAL_USUARIO_ID, $fecha_inicio_meta, $fecha_fin_meta);
              if ($result2['estado'] == 1) {
                $MEJORAGC_TOTAL_COMISIONES = formato_numero($result2['data']['pago_mejoragc']);
              }
            $result2 = NULL;

            $tabla_gc = '
              <tr>
                <td style="font-weight: bold">COMISIONES POR RECAUDACIÓN</td>
                <td style="font-weight: bold">'.$comentario_recaudo.'</td>
                <td align="center">S/. '.mostrar_moneda($RECAUDO_TOTAL_COMISIONES).'</td>
              </tr>
              <tr>
                <td style="font-weight: bold">COMISIONES GC</td>
                <td style="font-weight: bold">'.$comentario_mejoragc.'</td>
                <td align="center">S/. '.mostrar_moneda($MEJORAGC_TOTAL_COMISIONES).'</td>
              </tr>
            ';

            $TOTAL_COMISIONES_GC = formato_numero($RECAUDO_TOTAL_COMISIONES + $MEJORAGC_TOTAL_COMISIONES);
          ?>

          <table class="table table-striped table-bordered">
            <tbody>
              <tr>
                <th>Tipo</th>
                <th>Detalle</th>
                <th>Comisión</th>
              </tr>
              <?php echo $tabla_gc;?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2" style="font-size: 12pt;"><b>Total Comisiones de Gestión de Cobranza</b></td>
                <td align="center"><b><?php echo formato_numero($TOTAL_COMISIONES_GC);?></b></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
<?php endif; ?>