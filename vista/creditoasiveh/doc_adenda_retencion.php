<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  
  require_once('../../public/librerias/html2pdf/_tcpdf_5.9.206/tcpdf.php');
  require_once("../funciones/funciones.php");
  require_once("../funciones/fechas.php");
  require_once("../funciones/operaciones.php");

  require_once ("../creditoasiveh/Creditoasiveh.class.php");
  $oCreditoasiveh = new Creditoasiveh();
  require_once("../cuota/Cuotadetalle.class.php");
  $oCuotadetalle = new Cuotadetalle();
  require_once ("../usuario/Usuario.class.php");
  $oUsuario = new Usuario();
  require_once ("../ubigeo/Ubigeo.class.php");
  $oUbigeo = new Ubigeo();

  $title='ADENDA DE PAGO DE COCHERA';
  $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);

  $dts = $oCreditoasiveh->mostrarUno($_GET['d1']);
  
    $cre_id     = $dts['data']['tb_credito_id'];
    $reg        = mostrar_fecha_hora($dts['data']['tb_credito_reg']);
    $mod        = mostrar_fecha_hora($dts['data']['tb_credito_mod']);
    $apr        = mostrar_fecha_hora($dts['data']['tb_credito_apr']);

    $usureg     = $dts['data']['tb_credito_usureg'];
    $usumod     = $dts['data']['tb_credito_usumod'];
    $usuapr     = $dts['data']['tb_credito_usuapr'];

    $cuotip_id  = $dts['data']['tb_cuotatipo_id'];
    $mon_id     = $dts['data']['tb_moneda_id'];

    if($mon_id == 1){ $moneda = 'S/.'; }else{ $moneda = 'US$'; }
      
    // $cre_tipcap = $dts['data']['tb_credito_tipcap'];
    $cre_id_ori = $dts['data']['tb_credito_adreg']; //aqui se guara el id del credito original

    //fechas
    $cre_feccre=mostrar_fecha($dts['data']['tb_credito_feccre']);
    $cre_fecdes=mostrar_fecha($dts['data']['tb_credito_fecdes']);
    $cre_fecfac=mostrar_fecha($dts['data']['tb_credito_fecfac']);
    $cre_fecpro=mostrar_fecha($dts['data']['tb_credito_fecpro']);
    $cre_fecent=mostrar_fecha($dts['data']['tb_credito_fecent']);

    $cuota_subper   = $dts['data']['tb_cuotasubperiodo_id']; //1 mensual,2 quincenal, 3 semanal
    $cre_preaco     = $dts['data']['tb_credito_preaco'];
    $cre_int        = $dts['data']['tb_credito_int'];
    $cre_numcuo     = $dts['data']['tb_credito_numcuo'];
    $cre_linapr     = $dts['data']['tb_credito_linapr'];
    $cre_numcuomax  = $dts['data']['tb_credito_numcuomax'];

    $transferencia_bien = $cre_linapr;

    //cuenta deposito
    $cuedep_id      = $dts['data']['tb_cuentadeposito_id'];

    $cre_pla        = $dts['data']['tb_credito_pla'];
    $cre_comdes     = $dts['data']['tb_credito_comdes'];
    $cre_obs        = $dts['data']['tb_credito_obs'];
    $cre_webref     = $dts['data']['tb_credito_webref'];

    $cre_est        = $dts['data']['tb_credito_est'];

    //cliente
    $cli_id         = $dts['data']['tb_cliente_id'];
    $cli_doc        = $dts['data']['tb_cliente_doc'];
    $cli_nom        = $dts['data']['tb_cliente_nom'];
    $cli_dir        = $dts['data']['tb_cliente_dir'];
    $cli_ema        = $dts['data']['tb_cliente_ema'];
    $cli_fecnac     = $dts['data']['tb_cliente_fecnac'];
    $cli_tel        = $dts['data']['tb_cliente_tel'];
    $cli_cel        = $dts['data']['tb_cliente_cel'];
    $cli_telref     = $dts['data']['tb_cliente_telref'];
    $cli_ubigeo     = $dts['data']['tb_ubigeo_cod'];

    $cli_estciv   = $dts['data']['tb_cliente_estciv']; // 1 soltero, 2 casado
    $cli_bien     = $dts['data']['tb_cliente_bien']; // 1 separacion de bienes, 2 sin separacion
    $cli_firm     = $dts['data']['tb_cliente_firm']; // 1 firma solo, 2 firma con conyuge
    // $cli_zon_id   = $dts['data']['tb_zonaregistral_id']; // id de la zona registral donde está el N° de partida
    $cli_numpar   = $dts['data']['tb_cliente_numpar']; //numero de partida del cliente

    //vehiculo
    $veh_pre        = $dts['data']['tb_credito_vehpre'];
    $vehmar_id      = $dts['data']['tb_vehiculomarca_id'];
    $vehmar_nom     = $dts['data']['tb_vehiculomarca_nom'];
    $vehmod_id      = $dts['data']['tb_vehiculomodelo_id'];
    $vehmod_nom     = $dts['data']['tb_vehiculomodelo_nom'];
    $vehcla_id      = $dts['data']['tb_vehiculoclase_id'];
    $vehcla_nom     = $dts['data']['tb_vehiculoclase_nom'];
    $vehtip_id      = $dts['data']['tb_vehiculotipo_id'];
    $vehtip_nom     = $dts['data']['tb_vehiculotipo_nom'];
    $veh_pla        = $dts['data']['tb_credito_vehpla'];
    $veh_mot        = $dts['data']['tb_credito_vehsermot'];
    $veh_cha        = $dts['data']['tb_credito_vehsercha'];
    $veh_ano        = $dts['data']['tb_credito_vehano'];
    $veh_col        = $dts['data']['tb_credito_vehcol'];
    $veh_numpas     = $dts['data']['tb_credito_vehnumpas'];
    $veh_numasi     = $dts['data']['tb_credito_vehnumasi'];
    $veh_kil        = $dts['data']['tb_credito_vehkil'];
    $gps_id         = $dts['data']['tb_credito_gps'];
    $gps_pre        = $dts['data']['tb_credito_gpspre'];
    $str_id         = $dts['data']['tb_credito_str'];
    $str_pre        = $dts['data']['tb_credito_strpre'];
    $soa_id         = $dts['data']['tb_credito_soa'];
    $soa_pre        = $dts['data']['tb_credito_soapre'];
    $gas_id         = $dts['data']['tb_credito_gas'];
    $gas_pre        = $dts['data']['tb_credito_gaspre'];
    $otr_pre        = $dts['data']['tb_credito_otrpre'];
    $veh_est        = $dts['data']['tb_credito_vehest'];

  $dts == null;

  //fecha del contrato
  list($day, $month, $year) = explode('-', $cre_feccre);  
  $feccre_dia = $day;
  $feccre_mes = $month;
  $feccre_ano = $year;

  if($cli_ubigeo>0)
  {
    $dts = $oUbigeo->mostrarUbigeo($cli_ubigeo);
      $ubi_dep = $dts['data']['Departamento'];
      $ubi_pro = $dts['data']['Provincia'];
      $ubi_dis = $dts['data']['Distrito'];
    $dts = null;
  }

  if($usureg>0)
  {
    $rws = $oUsuario->mostrarUno($usureg);
      $usureg = $rws['data']['tb_usuario_nom']." ".$rws['data']['tb_usuario_ape'];
      $rws = null;
  }

  if($usumod>0)
  {
    $rws=$oUsuario->mostrarUno($usumod);
      $usumod = $rws['data']['tb_usuario_nom']." ".$rws['data']['tb_usuario_ape'];
    $rws = null;
  }

  if($usuapr>0)
  {
    $mos_usuapr = 1;
    $rws=$oUsuario->mostrarUno($usuapr);
      $usuapr = $rws['data']['tb_usuario_nom']." ".$rws['data']['tb_usuario_ape'];
    $rws = null;
  }

  $cli_domici = $cli_dir.', Distrito de '.$ubi_dis.', Provincia de '.$ubi_pro.', Departamento de '.$ubi_dep;

  //detalle de las cuotas del credito original, 2 ya que es un credito ASIVEH
  $dts = $oCuotadetalle->mostrar_una_cuotadetalle_por_creditoid($cre_id_ori,2);
    $cuodet_cuo_ori = $dts['data']['tb_cuotadetalle_cuo'];
    $cuodet_fec = mostrar_fecha($dts['data']['tb_cuotadetalle_fec']);
  $dts = null;

  //tabla de cuotas de pagos
  $periodo_nom = 'MENSUAL';
  if($cuota_subper == 2)
    $periodo_nom = 'QUINCENAL';
  if($cuota_subper == 3)
    $periodo_nom = 'SEMANAL';

  $registros = $oCuotadetalle->mostrar_cuotasdetalle_por_creditoid($cre_id,2); // 2 ya que es un credito ASIVEH
    $num = 1;
    $cuota_detalle = '';
    for ($i=0; $i<count($registros);$i++)
    {
      $cuota_detalle .='
      <tr>
        <td align="center">'.$num.'</td>
        <td align="center">'.mostrar_fecha($registros['data'][$i]['tb_cuotadetalle_fec']).'</td>
        <td align="right">'.$moneda.' '.formato_moneda($registros['data'][$i]['tb_cuotadetalle_cuo']).'</td>
      </tr>';
      $num++;
    }
  $registros = null;

  //obtenemos información del cliente, sus nombres, su esposa(o), copropietario o apoderado
  $esposa = ''; $apoderado = '';
  $clientes = '<strong>'.$cli_nom.'</strong>, identificado con DNI N° <strong>'.$cli_doc.'</strong> con domicilio en <strong>'.trim($cli_dir).'</strong>, del Distrito de <strong>'.$ubi_dis.'</strong>, Provincia de <strong>'.$ubi_pro.'</strong>, del Departamento de <strong>'.$ubi_dep.'</strong>';

  //si el cliente es casado
  if($cli_estciv == 2){
    $esposa = devolver_datos_esposa_apoderado($cli_id, $cli_bien, $cli_firm, 1); //parametro 1 CONYUGAL
    $clientes = $clientes.' '.$esposa;
  }

  $apoderado = devolver_datos_esposa_apoderado($cli_id, $cli_bien, $cli_firm, 3); //parametro 3 apoderado

  if($apoderado != '')
    $clientes = $clientes.', '.$apoderado;

  //verificar si el cliente tiene co-propietarios
  $copro = devolver_datos_copropietarios($cli_id);

  if($copro != '')
    $clientes .= ' '.$copro;

  $clientes = str_replace(' ,', ',', $clientes);

  // create new PDF document
  class MYPDF extends TCPDF {

    public function Header() {
      $image_file = K_PATH_IMAGES.'logo.jpg';
      $this->Image($image_file, 20, 10, 71, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
      // Set font
      //$this->SetFont('helvetica', 'B', 20);
      // Title
      //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    public function Footer() {

      $style = array(
        'position' => 'L',
        'align' => 'L',
        'stretch' => false,
        'fitwidth' => true,
        'cellfitalign' => '',
        'border' => false,
        'padding' => 0,
        'fgcolor' => array(0,0,0),
        'bgcolor' => false,
        'text' => false
      //     'font' => 'helvetica',
      //     'fontsize' => 8,
      //     'stretchtext' => 4
      );

      $this -> SetY(-24);
      // Page number
      $this->SetFont('helvetica', '', 9);
      //$this->SetTextColor(0,0,0);
      $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
      
      $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
      
      $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
      $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
    }
  }
  // create new PDF document
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.inticap.com');
    $pdf->SetTitle($title);
    $pdf->SetSubject('www.inticap.com');
    $pdf->SetKeywords('www.inticap.com');

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(30, 40, 30);// left top right
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, 30);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //set some language-dependent strings
    $pdf->setLanguageArray($l);
    // ---------------------------------------------------------

    // add a page
  $pdf->AddPage('P', 'A4');
  //Sistem variable
      list($dia_ori,$mes_ori,$anio_ori) = str_split('-', $cre_feccre);
      list($dia_des,$mes_des,$anio_des) = str_split('-', $cre_fecdes);
      list($dia_cud,$mes_cud,$anio_cud) = str_split('-', $cuodet_fec);
      list($dia_hoy,$mes_hoy,$anio_hoy) = str_split('-', date('d-m-Y'));
      
      $html= '
      <p align="center"><strong><u>ADDENDUM  AL CONTRATO DE ALQUILER VENTA</u></strong></p>
      
      <p align="justify"> Conste por el presente documento el Addendum al Contrato de Alquiler Venta, que celebran de una parte, <strong>INVERSIONES Y PRESTAMOS DEL NORTE SAC</strong>, identificada con R.U.C <strong>20600752872</strong>, con domicilio en Centro Comercial Boulevard Plaza Interior J-7, debidamente representada por su Gerente General el Sr. <strong>Victor Manuel Vargas Torres</strong>, identificado con D.N.I. Nº <strong>44760801</strong>, según facultades inscritas en la Partida N° 11218951 del Registro de Personas Jurídicas de la Oficina Registral de Chiclayo, a quien en adelante se les denominará <strong>EL VENDEDOR</strong>, y de otra parte '.$clientes.', a quien en adelante se denominará como <strong>EL COMPRADOR</strong>, en los términos y condiciones siguientes:</p>
      <p></p>
      <p align="justify"><b><u>CLAUSULA PRIMERA:</u></b> Con fecha <b>'.$dia_ori.' de '.strtolower(nombre_mes($mes_ori)).' del '.$anio_ori.'</b>, <b>EL VENDEDOR y EL COMPRADOR</b> celebraron un <b>CONTRATO DE ALQUILER VENTA DE VEHÍCULO</b>, en virtud del cual <b>EL VENDEDOR</b> da en Alquiler Venta Real y Enajenación el vehículo de marca: <b>'.$vehmar_nom.'</b> modelo: <b>'.$vehmod_nom.'</b>, carrocería: <b>'.$vehtip_nom.'</b>, color: <b>'.$veh_col.'</b>, con numero de motor: <b>'.$veh_mot.'</b>, Nro. De CHASIS: <b>'.$veh_cha.'</b> y placa de rodaje: <b>'.$veh_pla.'</b> a <b>EL COMPRADOR</b>, en la suma de <strong>'.$moneda_ori.' '.formato_moneda($transferencia_bien).' ('.numtoletras($transferencia_bien,$mon_id).')</strong>, a <b>EL COMPRADOR</b>, y conforme a la cláusula 1.1 del contrato de la referencia en adelante, “El Contrato”, a cambio de un pago que será cancelado por <b>'.$num_cuo_ori.'</b> cuotas semanales de <b>'.$moneda_ori.' '.formato_moneda($cuodet_cuo_ori).' ('.numtoletras($cuodet_cuo_ori,$mon_id).')</b>.</p>
      <p></p>
      <p align="justify"><b><u>CLAUSULA SEGUNDA:</u></b> Las partes por mutuo acuerdo, establecen que <b>EL COMPRADOR</b> deberá cancelar la suma de S/. 5.00 soles (CINCO CON 00/100 SOLES) diarios contados a partir de la fecha de la RETENCIÓN DE LA UNIDAD VEHICULAR POR FALTA DE PAGO DE CUOTA SEGÚN CRONOGRAMA DE PAGOS, por concepto de <b>COCHERA</b>.<br>En el mismo sentido, el monto total por el pago de cochera (la cochera es elegida por <b>EL VENDEDOR</b>), será calculado en base a los días que el vehículo permanezca en ésta, hasta el momento de la cancelación de la deuda por parte <b>EL COMPRADOR</b>. Dicho pago será en efectivo y será cancelado en la fecha de entrega de la unidad por parte de <b>EL VENDEDOR a EL COMPRADOR</b>.<br>Así también ambas partes acuerdan que <b>EL COMPRADOR</b> pagará la suma de S/. 10.00 soles (DIEZ CON 00/100 SOLES) por concepto de <b>MOVILIDAD</b> de personal de Inversiones y Préstamos del Norte SAC invertidos en la retención del vehículo.<br>Cabe resaltar que estos costos de retención podrán variar en función de la distancia y otros. En cualquier caso queda establecido por las partes que será <b>EL COMPRADOR</b> quién asumirá el íntegro de los costos de retención vehicular por incumplimiento de pago, y que deberá cancelar la totalidad de dichos costos para poder solicitar la devolución de la unidad vehicular, previa cancelación de la deuda que motiva dicha retención.
      </p>

      <p></p>

      <p align="justify"><b><u>CLAUSULA TERCERA:</u></b> Quedan inalterables las demás cláusulas del contrato primigenio.</p>
      <p></p>

      <p align="justify">   En señal de aceptación y aprobación, ambas partes suscriben el presente Addendum, en dos ejemplares, en la ciudad de  Chiclayo, a  los '.$dia_des.' días del mes de '.strtolower(nombre_mes($mes_des)).' de '.$anio_des.'.</p>

        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
      <table style="width:100%;" border="0">
        <tr>
          <td style="width:50%;">
            <table style="padding-top:20px;">
              <tr>
                <td align="center">___________________________
                </td>
              </tr>
              <tr>
                <td align="center"><strong>EL COMPRADOR</strong></td>
              </tr>
            </table>
          </td>
          <td style="width:50%;">
            <table style="padding-top:20px;">
              <tr>
                <td align="center">___________________________
                </td>
              </tr>
              <tr>
                <td align="center"><strong>EL VENDEDOR</strong></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
  ';

  // set core font
  $pdf->SetFont('arial', '', 11);

  // output the HTML content
  $pdf->writeHTML($html, true, 0, true, true);
  $pdf->Ln();

  // reset pointer to the last page
  $pdf->lastPage();

  // ---------------------------------------------------------

  //Close and output PDF document
  $nombre_archivo=$codigo."_".$title.".pdf";
  $pdf->Output($nombre_archivo, 'I');
  //============================================================+
  // END OF FILE                                                
  //============================================================+
?>
