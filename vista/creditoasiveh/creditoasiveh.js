$(document).ready(function() {
  console.log('cambios 20-09-2024');
  $(".moneda").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "999999999.00",
  });

  $(".moneda2").autoNumeric({
    aSep: ",",
    aDec: ".",
    vMin: "0.00",
    vMax: "999999.00",
  });

  usuario_select(0);
  $('#datetimepicker1, #datetimepicker2' ).datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });

  creditoasiveh_tabla();

  $("#txt_creditoasiveh_fil_cli_nom").autocomplete({
    minLength: 1,
    source: VISTA_URL + "cliente/cliente_autocomplete.php",
    select: function (event, ui) {
      var valor = ui.item.cliente_id;
      $("#hdd_creditoasiveh_fil_cli_id").val(ui.item.cliente_id);
      //$("#txt_creditoasiveh_fil_cli_nom").val(ui.item.nombre);
      //$("#txt_cliente_doc").val(ui.item.documento);
      creditoasiveh_tabla();
    }
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_fil_ing_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });

  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_fil_ing_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  
  $("#txt_fil_ing_fec1, #txt_fil_ing_fec2, #cmb_estado, #chkcredasiveh01, #chkcredasiveh02, #chkcredasiveh03, #chkcredasiveh04").change(function (){
    creditoasiveh_tabla();
  });

  $("#btnfiltrar").click(function() {
    creditoasiveh_tabla();
  });

  $("#btnreset").click(function() {
    $("#txt_creditoasiveh_fil_cli_nom").val('');
    $("#hdd_creditoasiveh_fil_cli_id").val('');
    $("#cmb_estado").val('');
    $("#cmb_fil_opc_cre").val('');
    $("#cmb_usuario_id").val('0');
    var checkbox1 = document.getElementById("chkcredasiveh01");
    checkbox1.checked = false;
    var checkbox2 = document.getElementById("chkcredasiveh02");
    checkbox2.checked = false;
    var checkbox3 = document.getElementById("chkcredasiveh03");
    checkbox3.checked = false;
    var checkbox4 = document.getElementById("chkcredasiveh04");
    checkbox4.checked = false;
    creditoasiveh_tabla();
  });

  $("#cmb_usuario_id").change(function() {
    var cre_radio = $("input[name=ra_estado]:checked").val();
    var asesor = $("#cmb_usuario_id").val();

    if(asesor==undefined || cre_radio==undefined) {
      $("#cmb_usuario_id").val('0');
      alerta_warning("ERROR","Seleccione asesor o credito");
    }
    else{
      cambiar_asesor(asesor,cre_radio);
    }
  });
  
  //   $('#cmb_fil_opc_cre').change(function(event) {
        
  //       var opc = $(this).val();
  //       var radio = $('input[name=ra_estado]:checked').val();
  //       var cre_est = $('input[name=ra_estado]:checked').data('est');
  //       var cre_tip = $('input[name=ra_estado]:checked').data('tip');
  //       var cuo_tip = $('input[name=ra_estado]:checked').data('cuo_tip'); //tipo de cuota, 3 LIBRE, 4 FIJA
  //       console.log("opc ="+opc+" cre_est ="+cre_est+" cuo_tip ="+cuo_tip+" cre_tip ="+cre_tip);
  //       if(!opc)
  //               return false;

  //       var bandera = false;
  //       var act = '';
  //       if(parseInt(opc) == 3)
  //               act = 'vigente';
  //       if(parseInt(opc) == 4)
  //               act = 'paralizar';
  //       if(parseInt(opc) == 8)
  //               act = 'resolver';

  //       if(parseInt(opc) == 10){
  //         refinanciar_form(radio, opc, 'multiple'); //refinanciar multiple adendas o créditos de un mismo cliente
  //         //alerta_warning("ALERTA","ESTÁ EN CONSTRUCCION")
  //         return false;
  //       }

  //       if(parseInt(opc) == 9 && parseInt(cre_est) == 3 && parseInt(cuo_tip) == 3){
  //               //renovación de cuotas para los creditos vigentes de tipo cuotas LIBRES
  //               $('#cmb_fil_opc_cre').val('');
  //               renovar_cuotas(radio);
  //               return false;
  //       }

  //       if(parseInt(opc) == 3 && (parseInt(cre_est) == 4) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
  //         bandera = true;
  //       if(parseInt(opc) == 4 && parseInt(cre_est) == 3 && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
  //         bandera = true;
  //       if(parseInt(opc) == 8 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
  //         bandera = true;
  //       if(parseInt(opc) == 5 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 2 || parseInt(cre_tip) == 4))
  //         bandera = true;
  //       if(parseInt(opc) == 6 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4))
  //         bandera = true;
  //       if(parseInt(opc) == 11 && (parseInt(cre_est) == 4 || parseInt(cre_est) == 3) && (parseInt(cre_tip) == 1 || parseInt(cre_tip) == 4 || parseInt(cre_tip) == 2))
  //         bandera = true;

  //       if(!bandera){
  //         alerta_warning('!IMPORTANTE','Debes seleccionar un crédito o quizás el estado del crédito no se puede modifcar, revisa nuevamente o consulta con Sistemas.')
  //         $('#cmb_fil_opc_cre').val('');
  //         return false;
  //       }

  //       if(radio != undefined){
          
  //         if(opc == 5 || opc == 6){
  //           //console.log(' opc = '+opc+' radio = '+radio+' cre_est = '+cre_est+' cre_tip = '+cre_tip+' cuo_tip = '+cuo_tip);
  //           $('#cmb_fil_opc_cre').val('');
  //           refinanciar_form(radio, opc, 'simple');  //enviamos el id del crédito seleccionado, id del credito es radio, refinanciar simple 1 solo credito
                  
  //           return false;
  //         }
  //         if(parseInt(opc) == 11){
            
  //           liquidacion_formato_form(radio);
  //           return false;
  //         }

  //         //eset form solo será para PONER EN VIGENCIA, PARALIZAR Y RESOLVER
  //         $('#cmb_fil_opc_cre').val('');
  //           credito_resolver_vigente(act, cre_tip, cre_est, radio); 
  //       }
  //       else{
  //         alerta_warning('!IMPORTANTE','Debes seleccionar un crédito o quizás el estado del crédito no se puede modifcar, revisa nuevamente o consulta con Sistemas.')
  //         $('#cmb_fil_opc_cre').val('');
  //       }
  //   });
  
});

function usuario_select(id) {

  var sede_id = $("#hdd_empresa_id").val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "/usuario/usuario_select.php",
    async: true,
    dataType: "html",
    data: {
      usuario_columna: 'tb_empresa_id',
      param_tip:'INT',
      usuario_valor: sede_id
    },
    beforeSend: function () {
      $("#cmb_usuario_id").val("<option>' cargando '</option>");
    },
    success: function (data) {
      $("#cmb_usuario_id").html(data);
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      $("#creditoasiveh_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CANCHA: " + data.responseText
      );
    },
  });
}

function clearClient() {
  if( ($("#txt_creditoasiveh_fil_cli_nom").val().length) == 0 )
  {
    $("#hdd_creditoasiveh_fil_cli_id").val('');
    creditoasiveh_tabla();
  }
}

function creditoasiveh_tabla() {
  var fecha1           = $('#txt_fil_ing_fec1').val();
  var fecha2           = $('#txt_fil_ing_fec2').val();
  var cliente_fil_id   = $('#hdd_creditoasiveh_fil_cli_id').val();
  var estado           = $('#cmb_estado').val();
  var cmb_sede_id      = $('#hdd_empresa_id').val();
  var cmb_usuario_id   = $('#cmb_usuario_id').val();
  var cmb_fil_opc_cre  = $('#cmb_fil_opc_cre').val();
  var chckcredasiveh01 = document.getElementById("chkcredasiveh01").checked;
  var chckcredasiveh02 = document.getElementById("chkcredasiveh02").checked;
  var chckcredasiveh03 = document.getElementById("chkcredasiveh03").checked;
  var chckcredasiveh04 = document.getElementById("chkcredasiveh04").checked;
  
  if( cliente_fil_id == null || $.trim(cliente_fil_id) == ''){ cliente_fil_id = 0;}
  if( estado == null || $.trim(estado) == '' ){ estado = 0;}
  if( cmb_usuario_id == null || $.trim(cmb_usuario_id) == '' ){ cmb_usuario_id = 0;}
  if( cmb_fil_opc_cre == null || $.trim(cmb_fil_opc_cre) == '' ){ cmb_fil_opc_cre = 0;}
  
  if( chckcredasiveh01 == false || chckcredasiveh01 == null ){ chckcredasiveh01 = 0; } else { chckcredasiveh01 = $('#chkcredasiveh01').val(); }
  if( chckcredasiveh02 == false || chckcredasiveh02 == null ){ chckcredasiveh02 = 0; } else { chckcredasiveh02 = $('#chkcredasiveh02').val(); }
  if( chckcredasiveh03 == false || chckcredasiveh03 == null ){ chckcredasiveh03 = 0; } else { chckcredasiveh03 = $('#chkcredasiveh03').val(); }
  if( chckcredasiveh04 == false || chckcredasiveh04 == null ){ chckcredasiveh04 = 0; } else { chckcredasiveh04 = $('#chkcredasiveh04').val(); }

  // console.log(fecha1+' / '+fecha2+' / cliente: '+cliente_fil_id+' / estado: '+estado+' / asesor: '+cmb_usuario_id+' / op. cred: '+cmb_fil_opc_cre+' / checks / '+chckcredasiveh01+' / '+chckcredasiveh02+' / '+chckcredasiveh03+' / '+chckcredasiveh04);

  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditoasiveh/creditoasiveh_tabla.php",
    async: true,
    dataType: "html",
    data: ({
        fecha1: fecha1,
        fecha2: fecha2,
        cliente_id: cliente_fil_id,
        p_estado: estado,
        sede_id: cmb_sede_id,
        cmb_usuario_id: cmb_usuario_id,
        cmb_fil_opc_cre: cmb_fil_opc_cre,
        chckcredasiveh01: chckcredasiveh01,
        chckcredasiveh02: chckcredasiveh02,
        chckcredasiveh03: chckcredasiveh03,
        chckcredasiveh04: chckcredasiveh04
    }), 
    beforeSend: function () {
      $("#creditoasiveh_mensaje_tbl").show(300);
    },
    success: function (data) {
      //INSERTA MEDIANTE HTML LA DATA QUE REGRESA DE CLIENTE_TABLA.PHP
      $("#div_creditogarveh_tabla").html(data);
      $("#creditoasiveh_mensaje_tbl").hide(300);
    },
    complete: function (data) {
        estilos_datatable();
      // console.log(data); MUESTRA LA DATA QUE DEVUELVE ACA PUEDO VER ERROR
    },
    error: function (data) {
      $("#creditoasiveh_mensaje_tbl").html(
        "ERROR: " + data.responseText
      );
    },
  });
}

function estilos_datatable() {
  datatable_global = $('#tbl_creditoasiveh').DataTable({
    "pageLength": 50,
    "responsive": true,
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsqueda",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [[1, 'asc']],
    columnDefs: [
        {targets: [11,12], orderable: false}
    ]
  });

  datatable_texto_filtrar();
}

function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}

function cambiar_asesor(asesor,cre_radio){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "creditoasiveh/creditoasiveh_controller.php",
    async:false,
    dataType: "json",
    data: ({
      action: 'asesor',
      asesor_id: asesor,
      cre_id: cre_radio
    }),
    beforeSend: function() {
      //$('#msj_credito').html('Cambiando de Asesor...');
      //$('#msj_credito').show(400)
    },
    success: function(data){
      // console.log(data)
      if(parseInt(data.estado) == 1){
        alerta_success("Exito",data.mensaje);
        creditoasiveh_tabla();
      }
      else{
        alerta_warning("Error",data.mensaje);
      }
    },
    complete: function(data){
      // console.log(data);
    }
  });
}

//pendienteee el boton ver
function creditoasiveh_form(usuario_act, creditoasiveh_id, proceso_id=0) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "creditoasiveh/creditoasiveh_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: usuario_act, // PUEDE SER: L, I, M , E
          creditoasiveh_id: creditoasiveh_id,
          proceso_id: proceso_id,
          vista: 'creditoasiveh'
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
        // console.log(data);
        $('#modal_mensaje').modal('hide');
        if (data != 'sin_datos') {
            $('#div_modal_creditoasiveh_form').html(data);
            $('#modal_registro_creditoasiveh').modal('show');

            //desabilitar elementos del form si es L (LEER)
            if (usuario_act == 'L'){
                form_desabilitar_elementos('for_cre');
            } //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_registro_creditoasiveh', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_registro_creditoasiveh',85);
            modal_height_auto('modal_registro_creditoasiveh');
        } else {
            //llamar al formulario de solicitar permiso
            var modulo = 'creditoasiveh';
            var div = 'div_modal_creditoasiveh_form';
            permiso_solicitud(usuario_act, creditoasiveh_id, modulo, div); //funcion ubicada en public/js/permiso.js
        }
      },
      complete: function (data) {
        //console.log(data);
        credito_cronograma();
        // cuota_tabla();
        credito_file_tabla();
      },
      error: function (data) {
          $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
          $('#overlay_modal_mensaje').removeClass('overlay').empty();
          $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
          console.log(data.responseText);
      }
  });
}

function credito_cronograma()
{	
	var v_pre = $('#txt_veh_pre').val();
  var v_preaco = $('#txt_cre_preaco').val();
  var act = $('#action_credito').val();
  if(act == 'ad_regular'){
    v_pre = 0;
    v_preaco = $('#txt_cre_preaco_ad').val();
  }
  if($('#txt_cre_fecfac').val() == '' || $('#txt_cre_fecpro').val() == ''){
    alert('Ingrese fecha de desembolso y prorrateo');
    return false;
  }
  $.ajax({
		type: "POST",
		url: "./vista/creditoasiveh/credito_cronograma.php",
		async:true,
		dataType: "html",
		data: ({
			veh_pre: 	v_pre,
			cre_ini: 	$('#txt_cre_ini').val(),          
      cre_id: $('#hdd_cre_id').val(),             
      fec_desem: $('#txt_cre_fecdes').val(),      
      fec_prorra: $('#txt_cre_fecpro').val(),     
			cre_preaco: v_preaco,                       
			cre_int: $('#txt_cre_int').val(),           
			cre_numcuo: $('#txt_cre_numcuo').val(),     
			cre_numcuo_res: $('#txt_cre_cuo_res').val(), //son la cantidad de cuotas restantes que faltan por pagar, sirve para calcular la CUOTA BALON
			cre_linapr: $('#txt_cre_linapr').val(),     
			mon_id: $('#cmb_mon_id').val(),             
			cre_fecfac: $('#txt_cre_fecfac').val(),     
      cre_per: $('#cmb_cre_subper').val(),        
      tip_ade: $('#cmb_cre_tipade').val()         
		}),
		beforeSend: function(){
			$('#div_credito_cronograma').html('<span>Cargando...</span>');
    },
		success: function(html){
			$('#div_credito_cronograma').html(html);

			monto_pro = Number($('#hdd_mon_pro_1').val());
			/*var linapr = Number($('#txt_cre_linapr').val().replace(/[^0-9\.]+/g,""));
			
			suma_lin_pro = parseFloat(mon_pro + linapr).toFixed(2);
			console.log('prorra: '+mon_pro+' / linapr: '+linapr+' / suma: '+suma_lin_pro);*/
		},
		complete: function(){
			credito_calculo();
      // cuota_tabla();
      // acuerdo_tabla();
		}
	});
}

function credito_calculo(){	
	var v_pre = $('#txt_veh_pre').val();
  var v_preaco = $('#txt_cre_preaco').val();
  var act = $('#action_credito').val();
  if(act == 'ad_regular'){
    v_pre = 0;
    v_preaco = $('#txt_cre_preaco_ad').val();
  }
  console.log('cre-cal '+v_pre +' '+v_preaco);
  $.ajax({
		type: "POST",
		url: "./vista/creditoasiveh/credito_calculo.php",
		async:true,
		dataType: "json",
		data: ({
			action: "cuotas",
			veh_pre: 	v_pre,
			cre_ini: 	$('#txt_cre_ini').val(),
			cre_preaco: v_preaco,
			cre_int: 	$('#txt_cre_int').val(),
			cre_numcuo: $('#txt_cre_numcuo').val(),
			cre_numcuo_res: $('#txt_cre_cuo_res').val(),
			tip_ade: $('#cmb_cre_tipade').val()
		}),
		beforeSend: function() {
			$('#txt_cre_linapr').html('Cargando...');
    },
		success: function(data){
			$('#txt_cre_linapr').val(data.credito_resultado);
      var valorFloat = 0;
			var linapr = Number($('#txt_cre_linapr').val().replace(/[^0-9\.]+/g,""));
			var suma = parseFloat(monto_pro + linapr).toFixed(2);
      valorFloat = valorFloat + parseFloat(suma);
			$('#txt_cre_linapr').val(valorFloat.toFixed(2));//autoNumeric('set', valorFloat.toFixed(2)); // autoNumericSet();
      $('#txt_cre_linapr').closest('.form-group').removeClass('has-error').addClass('has-success');
			console.log('prorra: '+monto_pro+' / linapr: '+linapr+' / suma: '+suma);
		},
		complete: function(data){
      // console.log(data)
		}
	});
}


function cuota_tabla(){
	$.ajax({
		type: "POST",
		url: "./vista/cuota/cuota_tabla_asiveh.php",
		async:true,
		dataType: "html",                      
		data: ({
			cre_id:	$('#hdd_cre_id').val()
		}),
		beforeSend: function() {
			$('#div_cuota_tabla').addClass("ui-state-disabled");
        },
		success: function(html){
			$('#div_cuota_tabla').html(html);
		},
		complete: function(){
			$('#div_cuota_tabla').removeClass("ui-state-disabled");
		}
	});     
}

function acuerdo_tabla(){
	var cre_tip = $('#hdd_cre_tip').val();
	// if(cre_tip != 3){ return false; }
  $.ajax({
    type: "POST",
    url: "./vista/acuerdopago/acuerdopago_form_tabla.php",
    async:true,
    dataType: "html",
    data: ({
      cre_id:	cre_tip,
      acu_id: 1
    }),
    beforeSend: function() {
      $('#div_acuerdo_tabla').addClass("ui-state-disabled");
    },
    success: function(html){
      console.log('se cargo la tabla de acuerdo - tabla :: ' +html)
      $('#div_acuerdo_tabla').html(html);
    },
    complete: function(){			
      $('#div_acuerdo_tabla').removeClass("ui-state-disabled");
    }
  });
}

function credito_file_form()
{
  $.ajax({
		type: "POST",
    url: "./vista/creditoasivehfile/credito_file_form.php",
    async: false,
    dataType: "html",
    data: ({
      cre_id:	$('#hdd_cre_id').val(),
      action: 'insertar'
    }),
    beforeSend: function() {
      $('#msj_crefil').hide();
      $('#div_credito_file_section').html('cargando ...');
      // $('#div_credito_file_section').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
    success: function(html){
      console.log(html)
      $('#div_credito_file_section').html(html);
      $('#modal_creditoasivehfile_form').modal('show');
    },
    complete: function(data){

    }
  });
}

function credito_file_tabla()
{	
	$.ajax({
		type: "POST",
		url: "./vista/creditoasivehfile/credito_file_tabla.php",
		async:false,
		dataType: "html",                      
		data: ({
			 cre_id:	$('#hdd_cre_id').val() 	
		}),
		beforeSend: function() {
			//alert($("#hdd_catimg_id").val());
			$('#div_credito_file_tabla').addClass("ui-state-disabled");
        },
		success: function(html){
      console.log(html)
			$('#div_credito_file_tabla').html(html);
		},
		complete: function(){			
			$('#div_credito_file_tabla').removeClass("ui-state-disabled");
		}
	});     
}





//Juan Antonio 29-08-2023
function refinanciar_multiple(){

}


function credito_his(idf){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_timeline.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: 'historial',
			cre_id:	idf
		}),
		beforeSend: function() {
			//$('#div_credito_his').dialog("open");
			//$('#div_credito_his').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
      console.log(html);
			$('#div_creditogarveh_his').html(html);
      $('#modal_creditogarveh_timeline').modal('show');
      modal_hidden_bs_modal('modal_creditogarveh_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_creditogarveh_timeline'); //funcion encontrada en public/js/generales.js
		}
	});
}

function cliente_nota(idf)
{
	$.ajax({
		type: "POST",
		url: VISTA_URL + "cliente/clientenota_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			cre_id:	idf,
			cre_tip: 3 //tipo de credito 3: garveh
		}),
		beforeSend: function() {
			//$('#msj_credito').hide();
			//$('#div_clientenota_form').dialog("open");
			//$('#div_clientenota_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
			$('#div_clientenota_form').html(html);	
      $('#modal_cliente_nota').modal('show');
      modal_hidden_bs_modal('modal_cliente_nota', 'limpiar'); //funcion encontrada en public/js/generales.js
		}
	});
}

function creditogarveh_pagos(creditogarveh_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditogarveh/creditogarveh_pagos.php",
    async: true,
    dataType: "html",
    data: ({
      credito_id: creditogarveh_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_creditogarveh_pagos').html(data);
        $('#modal_creditogarveh_pagos').modal('show');

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_width_auto('modal_creditogarveh_pagos', 95);
        modal_height_auto('modal_creditogarveh_pagos'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_creditogarveh_pagos', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function eliminar_credito(id,tipo)
{   
	if(confirm("Realmente desea eliminar?")){
            if(tipo < 3){
		$.ajax({
			type: "POST",
			url: VISTA_URL + "creditogarveh/credito_reg.php",
			async:true,
			dataType: "html",
			data: ({
				action: "eliminar",
				cre_id:	id
			}),
			beforeSend: function() {
				//$('#msj_credito').html("Cargando...");
				//$('#msj_credito').show(100);
			},
			success: function(html){
                           if(html==1){
                                alerta_success("Exito","Credito eliminado");
                                creditoasiveh_tabla();
                            }
                            else{
                                alerta_warning("Hubo un error","No se pudo eliminar crédito");
                            }
			},
			complete: function(){
				creditoasiveh_tabla();
			}
		});
            }
            else{
                alert("NO PUEDE ELIMINAR ESTE CREDITO");
            }
        }
}

function placa_form(cre_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_placa.php",
		async:true,
		dataType: "html",                      
		data: ({
			credito_id:	cre_id
		}),
		beforeSend: function() {
			//$('#div_placa_form').dialog("open");
			//$('#div_placa_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(data){
      if(data != 'sin_datos'){
        $('#div_placa_form').html(data);
        modal_width_auto('modal_creditogarveh_placa',80);
        modal_height_auto('modal_creditogarveh_placa');
        $('#modal_creditogarveh_placa').modal('show');
        modal_hidden_bs_modal('modal_creditogarveh_placa', 'limpiar'); //funcion encontrada en public/js/generales.js
        creditoasiveh_tabla();
        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      
      }		
		}
	});
}

function verificacion_titulos_form(cre_id){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/creditogarveh_verificacion.php",
        async:true,
        dataType: "html",                      
        data: ({
            credito_id:	cre_id
        }),
        beforeSend: function() {
                //$('#div_verificacion_titulo_form').dialog("open");
                //$('#div_verificacion_titulo_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
        success: function(data){
            if(data != 'sin_datos'){
              $('#div_verificacion_titulo_form').html(data);
              modal_width_auto('modal_creditogarveh_verificacion',80);
              modal_height_auto('modal_creditogarveh_verificacion');
              $('#modal_creditogarveh_verificacion').modal('show');
              modal_hidden_bs_modal('modal_creditogarveh_verificacion', 'limpiar'); //funcion encontrada en public/js/generales.js

              //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo

            }	
        }
    });
}

function credito_documentos(cre_id){
	if(!confirm('¿Está seguro que los documentos están completos?'))
		return false;
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/credito_reg.php",
		async:true,
		dataType: "text",                      
		data: ({
			credito_id:	cre_id,
			action: 'documentos'
		}),
		beforeSend: function() {
			//$('#msj_credito').html('Guardando documentos completos...');
			//$('#msj_credito').show();
    },
		success: function(data){
                    if(data == 'exito'){
                        alerta_success("EXITO","Estado cambiado");
                        creditoasiveh_tabla();
                    }
                }
	});
}

function titulo_form(cre_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "titulo/titulo_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			cre_tip:	3, //3 garveh
			cre_id:	cre_id
		}),
		beforeSend: function() {
			//$('#div_titulo_form').dialog("open");
			//$('#div_titulo_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(data){
      if(data != 'sin_datos'){
        $('#div_titulo_form').html(data);
        modal_width_auto('modal_titulo',80);
        modal_height_auto('modal_titulo');
        $('#modal_titulo').modal('show');
        modal_hidden_bs_modal('modal_titulo', 'limpiar'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo

      }	
		}
	});
}
//SE AGREGÓ UN NUEVO PARAMETRO A LA FUNCION
function credito_aprobar(idf,custodia){    
	if(confirm("Realmente desea aprobar?")){
		$.ajax({
			type: "POST",
			url: VISTA_URL + "creditogarveh/credito_reg.php",
			async:true,
			dataType: "json",
			data: ({
				action: "aprobar",
				cre_id:		idf,
				custodia:	custodia
			}),
			beforeSend: function() {
				//$('#msj_credito').html("Cargando...");
				//$('#msj_credito').show(100);
			},
			success: function(data){ //REVISAR PORQUE NO DEBERIA SER Q ABRA MODALES}
                            console.log(data);
                            //SE AGREGO UNA CONDICIONAL PARA VERIFICAR EL ID DE LA CUSTODIA 
				if(parseInt(data.estado) > 0){
                                    alerta_success("EXITO", "APROBADO");
                                    //if(data.custodia==2){
                                    //registrar_polizas(data.credito_id);}
                                    creditoasiveh_tabla();
                                  }
                                  
			},
			complete: function(){
				//credito_tabla();
			}
		});
	}
}

function credito_desembolso_form(act,idf){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditogarveh/creditogarveh_desembolso_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: act,
			cre_id:	idf,
			vista:	'credito_tabla'
		}),
		beforeSend: function() {
			//$('#msj_credito').hide();
			//$('#div_credito_desembolso_form').dialog("open");
			//$('#div_credito_desembolso_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
		success: function(data){
                        //console.log(data);
			if(data != 'sin_datos'){
        $('#div_credito_desembolso_form').html(data);
        modal_width_auto('modal_creditogarveh_desembolso',80);
        modal_height_auto('modal_creditogarveh_desembolso');
        $('#modal_creditogarveh_desembolso').modal('show');
        modal_hidden_bs_modal('modal_creditogarveh_desembolso', 'limpiar'); //funcion encontrada en public/js/generales.js


        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      }	
		},
                complete: function(data){
                
                }
	});
}

//COMBO ACCIONES CREDITOS
function renovar_cuotas(idf){
	$.ajax({
		type: "POST",
		url: VISTA_URL + "resolver/renovar_cuotas_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			action: 'renovar',
			cre_id:	idf,
			cre_tip: 3 //tipo de credito 3: garveh
		}),
		beforeSend: function() {
			//$('#msj_credito').hide();
			//$('#div_renovar_cuotas').dialog("open");
			//$('#div_renovar_cuotas').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
    },
		success: function(html){
      if(html != 'sin_datos'){
        $('#div_renovar_cuotas').html(html);
        //modal_width_auto('modal_creditogarveh_desembolso',80);
        //modal_height_auto('modal_creditogarveh_desembolso');
        $('#modal_renovar_cuotas').modal('show');
        modal_hidden_bs_modal('modal_renovar_cuotas', 'limpiar'); //funcion encontrada en public/js/generales.js

      }	
					
		}
	});
}

function refinanciar_form(idf, est, tipo){
	var fecha = prompt('Ingrese una Fecha para el cálculo, formato: '+$('#txt_fecha_ref').val(), $('#txt_fecha_ref').val());
  var array_multiples = [];
  var id_multiples = '';

  if(tipo == 'multiple'){
    $('.che_multiple:checked').each(function() {
      var credito = $(this).data('credito');
      
      array_multiples.push(credito); // Agregar el valor de credito al array
    });

    if (array_multiples.length > 1) {
      id_multiples = array_multiples.join(','); // Unir los valores en una cadena separada por comas
      
    } else {
      alerta_warning('Importante', 'Para refinanciar múltiple debes seleccionar más de 1 crédito en estado Vigente, si solo es uno selecciona el Radio');
      return false;
    }

    console.log('los id selecc: ' + id_multiples);
  }

	if(fecha != undefined){
    if(fecha == ""){
      return false;
    }  
        
		$.ajax({
			type: "POST",
			url: VISTA_URL + "refinanciar/refinanciar_form.php",
			async:true,
			dataType: "html",                      
			data: ({
				action: 'refinanciar',
        tipo_refinanciar: tipo,
        id_multiples: id_multiples,
				cre_id:	idf,
				cre_tip: 3, //tipo de credito 3: garveh
				ref_tip: est,
				fecha_ref: fecha
			}),
			beforeSend: function() {
        $('#modal_mensaje').modal('show');
      },
			success: function(html){
        $('#modal_mensaje').modal('hide');
        if(html != 'sin_datos'){
          $('#div_refinanciar_form').html(html);
          modal_width_auto('modal_creditogarveh_refinanciar',85);
          modal_height_auto('modal_creditogarveh_refinanciar');
          $('#modal_creditogarveh_refinanciar').modal('show');
          modal_hidden_bs_modal('modal_creditogarveh_refinanciar', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
			}
		});
	}
	else{
		$('#cmb_fil_opc_cre').val('');
		alerta_warning('Importante', 'Debe ingresar una fecha');
	}   
}

function credito_resolver_vigente(act, tip, est, id){
	console.log(act + ' // tipo: ' + tip + ' // estad: ' + est + ' // id: '+ id);
	//return false;
	$.ajax({
		type: "POST",
		url: VISTA_URL + "resolver/resolver_form.php",
		async:true,
		dataType: "html",
		data: ({
			action: act,
			cre_tip_cre_aden: tip,
			cre_tip: 3,
			cre_est: est,
			cre_id:	id
		}),
		beforeSend: function() {
//			$('#div_credito_his').dialog('open');
//			$('#msj_credito').html("Cargando...");
//			$('#msj_credito').show(100);
		},
		success: function(html){
			$('#div_resolver_form').html(html);
			modal_width_auto('modal_creditogarveh_resolver',85);
      modal_height_auto('modal_creditogarveh_resolver');
      $('#modal_creditogarveh_resolver').modal('show');
      modal_hidden_bs_modal('modal_creditogarveh_resolver', 'limpiar'); //funcion encontrada en public/js/generales.js
		},
		complete: function(){
			//credito_tabla();
		}
	});
}

function registrar_polizas(credito_id){
$.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarveh/polizas_reg.php",
        dataType: "json",
        data: ({
            action: 'insertar_de_credito',
            credito_id: credito_id,          
            creditotipo_id: 3          
        }),
        beforeSend: function() {
          //$('#msj_clientenota').html("Guardando datos...");
          //$('#msj_clientenota').show(100);
        },
        success: function(data){
            console.log(data);
          if(data.estado>0){
          	alerta_success("EXITO",data.msj);
          }
          else
                alerta_error("ERROR",data.msj);
        },
        complete: function(data){
          console.log(data);
          if(data.statusText == "parsererror"){
            console.log(data);
            $('#msj_creditoplaca').text('ERROR: ' + data.responseText);
          }
        }
      });
    };

// JUAN : 07-08-2023

function liquidacion_formato_form(credito_id){
  var fecha_hoy = $('#txt_fecha_ref').val();

  $.confirm({
    title: 'Fecha de Liquidación',
    content: '' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
    '<label>Ingresa tu Fecha Aquí:</label>' +
    '<input type="date" class="name form-control" required />' +
    '</div>' +
    '</form>',
    buttons: {
      formSubmit: {
        text: 'Generar',
        btnClass: 'btn-blue',
        action: function () {
          var fecha_liquidacion = this.$content.find('.name').val();
          if(!fecha_liquidacion){
            $.alert('Debes ingresar una fecha valida');
            return false;
          }
          var tipo_cambio = $('#hdd_tipo_cambio_venta').val();

          $.ajax({
            type: "POST",
            url: VISTA_URL + "liquidacion/liquidacion_formato_form.php",
            async:true,
            dataType: "html",
            data: ({
              credito_id: credito_id,
              creditotipo_id: 3, //estamos en la carpeta de garveh, por ende es el 3
              fecha_liquidacion: fecha_liquidacion,
              tipo_cambio: tipo_cambio
            }),
            beforeSend: function() {
              $('#modal_mensaje').modal('show');
            },
            success: function(html){
              $('#modal_mensaje').hide('show');
              $('#div_modal_liquidacion').html(html);
              modal_width_auto('modal_liquidacion_formato',92);
              modal_height_auto('modal_liquidacion_formato');
              $('#modal_liquidacion_formato').modal('show');
              modal_hidden_bs_modal('modal_liquidacion_formato', 'limpiar'); //funcion encontrada en public/js/generales.js
            },
            complete: function(){
              //credito_tabla();
            }
          });
        }
      },
      Cancelar: function () {
          //close
      },
    },
    onContentReady: function () {
      // bind to events
      var jc = this;
      this.$content.find('form').on('submit', function (e) {
          // if the user submits the form by pressing enter in the field.
          e.preventDefault();
          jc.$$formSubmit.trigger('click'); // reference the button and click it
      });
    }
  });
}

//JUAN 16-09-2023

function creditolinea_form(action, creditolinea_id, creditotipo_id, credito_id) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "creditolinea/creditolinea_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			action:	action,
      creditolinea_id: creditolinea_id,
      creditotipo_id: creditotipo_id,
      credito_id: credito_id,
      modulo: 'creditogarveh'
		}),
		beforeSend: function() {
    },
		success: function(data){
      if(data != 'sin_datos'){
        $('#div_creditolinea_form').html(data);
        // modal_width_auto('modal_creditogarveh_placa',80);
        // modal_height_auto('modal_creditogarveh_placa');
        $('#modal_creditolinea_form').modal('show');
        modal_hidden_bs_modal('modal_creditolinea_form', 'limpiar'); //funcion encontrada en public/js/generales.js
      }		
		}
	});
}

// GERSON (11-11-23)
function generarSimulador(proceso_id, usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/generar_simulador.php",
    async: true,
    dataType: "html",
    data: ({
      proceso_id: proceso_id,
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_generar_simulador').html(data);
      $('#modal_generar_simulador').modal('show');
      modal_height_auto('modal_generar_simulador'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_generar_simulador', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

// GERSON (17-02-24)
function verChequesGenerados(usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"creditogarveh/ver_cheques_generados.php",
    async: true,
    dataType: "html",
    data: ({
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_cheques_generados').html(data);
      $('#modal_cheques_generados').modal('show');
      modal_height_auto('modal_cheques_generados'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_cheques_generados', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
}

function obtener_valor_seguro_gps(){

  var cuota = $("#simu_cuota").val();

  if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else{

    var numero_cuotas = parseInt(cuota);
    if(numero_cuotas <= 0)
      return false;

    $.ajax({
      type: "POST",
      url: VISTA_URL + "segurogps/segurogps_controller.php",
      async: true,
      dataType: "json",
      data: {
        action: "valores",
        numero_cuotas: numero_cuotas
      },
      beforeSend: function () {
      },
      success: function (data) {
        console.log(data);
        $('#hdd_seguro_porcentaje').val(data.valor_seguro);
        $('#hdd_gps_precio').val(data.valor_gps);
        $('#span_seguro_gps').val(data.texto);
        calcularSimulacion();
      },
      complete: function (data) {},
    });

  }
  
}

function guardarSimulador(id, value, tipo_dato, vista) {
  if(vista === 'procesos'){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async: false,
      dataType: "json",
      data: ({
        action: 'guardar_campo_simulador',
        proceso_id: $("#hdd_simu_proceso_id").val(),
        id: id, 
        value: value,
        tipo_dato: tipo_dato,
      }),
        beforeSend: function () {
        },
        success: function (data) {
          if(data.estado == 1){
            notificacion_success(data.mensaje, 3000);
          }else{
            notificacion_warning(data.mensaje, 3000);
          }
        },
        complete: function (data) {
        }
    });
  }
}

function calcularSimulacion(){

  var chk_gps = 0;
  if($('input[name="chk_gps"]:checked').val()){
    chk_gps = 1;
  }

  var fecha = $("#simu_fecha").val();
  var moneda = $("#simu_moneda").val();
  var cambio = $("#simu_cambio").val();
  var cuota = $("#simu_cuota").val();
  var interes = $("#simu_interes").val();
  var costo = $("#simu_costo").val();
  var inicial = $("#simu_inicial").val();
  var valor = $("#simu_valor").val();

  var vista = $("#hdd_vista").val();
  if(vista==='procesos'){
    if(inicial=='' || parseFloat(inicial)==0){
      notificacion_warning("Ingrese el monto de inicial.", 3000);
    }
  }

  if(cambio=='' || parseFloat(cambio)==0){
    notificacion_warning("Ingrese el tipo de cambio.", 3000);
  }else if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else if(interes=='' || parseFloat(interes)==0){
    notificacion_warning("Ingrese el monto de interés.", 3000);
  }else if(costo=='' || parseFloat(costo)==0){
    notificacion_warning("Ingrese el costo de vehículo.", 3000);
  }else if(valor=='' || parseFloat(valor)==0){
    notificacion_warning("Ingrese el valor del préstamo.", 3000);
  }else{

    $.ajax({
      type: "POST",
      url: VISTA_URL + "proceso/proceso_controller.php",
      async:true,
      dataType: "html",                      
      data: ({
        action: 'detalle_simulacion',
        proceso_id: parseInt($('#hdd_simu_proceso_id').val()),
        usuario_id: parseInt($('#hdd_simu_usuario_id').val()),
        fecha: fecha,
        moneda: moneda,
        cambio: cambio,
        cuota: cuota,
        interes: interes,
        costo: costo,
        inicial: inicial,
        valor: valor,
        chk_gps: chk_gps,
        seguro_porcentaje: $('#hdd_seguro_porcentaje').val(),
        gps_precio: $('#hdd_gps_precio').val(),
        span_seguro_gps: $('#span_seguro_gps').val(),
      }),
      beforeSend: function() {
      },
      success: function(html){
        $('#div_simulacion').html(html);  
      }
    });

  }
  
}

function generarSimuladorExcelPDF(proceso_id, usuario_id, tipo) {

  var chk_gps = 0;
  if($('input[name="chk_gps"]:checked').val()){
    chk_gps = 1;
  }

  var fecha = $("#simu_fecha").val();
  var moneda = $("#simu_moneda").val();
  var cambio = $("#simu_cambio").val();
  var cuota = $("#simu_cuota").val();
  var interes = $("#simu_interes").val();
  var costo = $("#simu_costo").val();
  var inicial = $("#simu_inicial").val();
  var valor = $("#simu_valor").val();

  var seguro_porcentaje = $('#hdd_seguro_porcentaje').val();
  var gps_precio        = $('#hdd_gps_precio').val();
  var span_seguro_gps   = $('#span_seguro_gps').val();

  var vista = $("#hdd_vista").val();
  if(vista==='procesos'){
    if(inicial=='' || parseFloat(inicial)==0){
      notificacion_warning("Ingrese el monto de inicial.", 3000);
    }
  }

  if(cambio=='' || parseFloat(cambio)==0){
    notificacion_warning("Ingrese el tipo de cambio.", 3000);
  }else if(cuota=='' || parseInt(cuota)==0){
    notificacion_warning("Ingrese el número de cuotas.", 3000);
  }else if(interes=='' || parseFloat(interes)==0){
    notificacion_warning("Ingrese el monto de interés.", 3000);
  }else if(costo=='' || parseFloat(costo)==0){
    notificacion_warning("Ingrese el costo de vehículo.", 3000);
  }else if(valor=='' || parseFloat(valor)==0){
    notificacion_warning("Ingrese el valor del préstamo.", 3000);
  }else{

    //window.open("http://www.ipdnsac.com/ipdnsac/vista/cuenta/cuenta_reporte_excel.php?fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&cuenta_tipo=" + cuenta_tipo + "&sede=" + sede);
    if(tipo=='excel'){
      window.open(VISTA_URL + "proceso/generar_simulador_excel.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id+"&fecha="+fecha+"&moneda="+moneda+"&cambio="+cambio+"&cuota="+cuota+"&interes="+interes+"&costo="+costo+"&inicial="+inicial+"&valor="+valor+"&seguro_porcentaje="+seguro_porcentaje+"&gps_precio="+gps_precio+"&span_seguro_gps="+span_seguro_gps+"&chk_gps="+chk_gps);
    }else{
	    //window.open("vista/inventarioproducto/pdf_inventarioproducto_tabla.php?inventario_id="+inventario_id,"_blank");

      window.open("vista/proceso/generar_simulador_pdf.php?proceso_id="+proceso_id+"&usuario_id="+usuario_id+"&fecha="+fecha+"&moneda="+moneda+"&cambio="+cambio+"&cuota="+cuota+"&interes="+interes+"&costo="+costo+"&inicial="+inicial+"&valor="+valor+"&seguro_porcentaje="+seguro_porcentaje+"&gps_precio="+gps_precio+"&span_seguro_gps="+span_seguro_gps+"&chk_gps="+chk_gps,"_blank");
    }

  }
}

function solo_numero(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}
//

// GERSON (04-0824)
function credito_acciones(cre_id){
  $.ajax({
      type: "POST",
      url: VISTA_URL + "creditogarveh/creditogarveh_seguimiento.php",
      async:true,
      dataType: "html",                      
      data: ({
          credito_id:	cre_id
      }),
      beforeSend: function() {
      },
      success: function(data){
          if(data != 'sin_datos'){
            $('#div_credito_seguimiento').html(data);
            modal_width_auto('modal_creditogarveh_seguimiento',80);
            modal_height_auto('modal_creditogarveh_seguimiento');
            $('#modal_creditogarveh_seguimiento').modal('show');
            modal_hidden_bs_modal('modal_creditogarveh_seguimiento', 'limpiar'); //funcion encontrada en public/js/generales.js

            //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo

          }	
      }
  });
}
//