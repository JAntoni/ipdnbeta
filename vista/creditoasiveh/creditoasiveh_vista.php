<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <!-- < ?php $link_ipnd1 = 'http://ipdnsac.com/app/modulos/creditogarveh/';?>
                < ?php $link_ipnd2 = '../ipdnsac/procesos/';?> -->
                
                <!-- <a class="btn btn-primary btn-sm" title="Agregar"><i class="fa fa-plus"></i> Agregar</a>
                <a class="btn bg-purple btn-sm" title="Actualizar"><i class="fa fa-refresh"></i> Actualizar</a>
                <a class="btn btn-info btn-sm" title="Items ASVEH"><i class="fa fa-list"></i> Items ASVEH</a>
                <a class="btn btn-warning btn-sm" title="Items Reventa"><i class="fa fa-list"></i> Items Reventa</a>
                <a class="btn btn-success btn-sm" title="Excel"><i class="fa fa-file-excel"></i> Excel Vencimientos</a> -->                
                <div class="row mt-4">
                    <div class="col-md-12">
                        <label for="txt_filtro" class="control-label">Filtrar por</label>
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <?php require_once('creditoasiveh_filtro.php');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="creditoasiveh_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_creditogarveh_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('creditogarveh_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="txt_fecha_ref" value="<?php echo date("d-m-Y");?>">
            <div id="div_modal_creditoasiveh_form"></div>
            <div id="div_creditoasiveh_ver"></div>




            <!-- <div id="div_credito_pdf_ver"></div>
            <div id="div_exonerar_creditogarvehpdf"></div>
            <div id="div_modal_cuentadeposito_form"></div>
            <div id="div_modal_representante_form"></div>
            <div id="div_modal_carousel"></div>
            <div id="div_controlseguro_form"></div>
            <div id="div_credito_file"></div>
            <div id="div_modal_creditogarveh_inicial"></div>
            <div id="div_credito_desembolso_form"></div>
            <div id="div_modal_creditogarveh_pagos"></div>
            <div id="div_modal_adenda_form"></div>
            <div id="div_creditogarveh_his"></div>
            <div id="div_refinanciar_form"></div>
            <div id="div_renovar_cuotas"></div>
            <div id="div_clientenota_form"></div>
            <div id="div_titulo_form"></div>
            <div id="div_placa_form"></div>
            <div id="div_verificacion_titulo_form"></div>
            <div id="div_credito_seguimiento"></div>
            <div id="div_amortizar_form"></div>
            <div id="div_resolver_form"></div>
            <div id="div_acuerdopago_form"></div>
            <div id="div_acuerdopago_anular_form"></div>
            <div id="div_acuerdopago_pagos_form"></div>
            <div id="div_modal_cliente_form"></div>
            <div id="div_modal_transferente_form"></div>
            <div id="div_modal_vehiculomarca_form"></div>
            <div id="div_modal_vehiculoclase_form"></div>
            <div id="div_modal_vehiculomodelo_form"></div>
            <div id="div_modal_vehiculotipo_form"></div>
            <div id="div_historial_doc_pdf"></div>
            <div id="div_modal_liquidacion"></div>
            <div id="div_creditolinea_form"></div>
            <div id="div_modal_generar_simulador"> -->
                <!-- INCLUIMOS EL MODAL PARA GENERAR SIMULACION DE CUOTAS -->
            <!-- </div> -->

            <!-- <div id="div_modal_cheques_generados"> -->
                <!-- INCLUIMOS EL MODAL PARA VISUALIZAR LOS CHEQUES -->
            <!-- </div> -->

            <!-- <div id="div_modal_cheque_retiro"> -->
            <!-- INCLUIMOS EL MODAL PARA VISUALIZAZR RETIROS DED CHEQUE -->
            <!-- </div> -->

            <!-- <div id="div_modal_gps_simple"></div> -->
            <!-- <div id="div_historial_cambios_legal"></div> -->
            <!-- <div id="div_modal_ejecucion_verpdf_form"></div> -->
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->

            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
