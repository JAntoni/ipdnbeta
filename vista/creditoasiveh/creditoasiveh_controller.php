<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

  require_once ("../../core/usuario_sesion.php");
  require_once("../funciones/funciones.php");
  require_once("../funciones/fechas.php");
  require_once('Creditoasiveh.class.php');
  $oCreditoasiveh = new Creditoasiveh();

  $data = array();
  $creditotipo_id = 2; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
  $usuario_id = intval($_SESSION['usuario_id']);
  $action = $_POST['action'];

  if($action == "asesor"){
    $asesor_id = $_POST['asesor_id'];
    $cre_id = $_POST['cre_id'];
    if( $oCreditoasiveh->modificar_campo($cre_id, 'tb_credito_usureg', $asesor_id,'INT') == 1 ){
      $data['estado']  = 1;
      $data['mensaje'] = 'Asesor modificado correctamente';
    }
    else{
      $data['estado']  = 0;
      $data['mensaje'] = "Se presentó un inconveniente al momento de realizar el el cambio de asesor. <br>Error SQL: Creditoasiveh.class :: modificar_campo() :: linea: 327";
    }
  }

  echo json_encode($data);
?>
