<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../../public/librerias/html2pdf/_tcpdf_5.9.206/tcpdf.php');
  require_once("../funciones/funciones.php");
  require_once("../funciones/fechas.php");
  require_once("../funciones/operaciones.php");

  require_once ("../creditoasiveh/Creditoasiveh.class.php");
  $oCreditoasiveh = new Creditoasiveh();
  require_once("../cuota/Cuotadetalle.class.php");
  $oCuotadetalle = new Cuotadetalle();
  require_once ("../usuario/Usuario.class.php");
  $oUsuario = new Usuario();
  require_once ("../ubigeo/Ubigeo.class.php");
  $oUbigeo = new Ubigeo();
  require_once ("../cuota/Cuota.class.php");
  $oCuota = new Cuota();  
  require_once ("../cuentadeposito/Cuentadeposito.class.php");
  $oCuentadeposito = new Cuentadeposito();
  require_once ("../vehiculoadicional/Vehiculoadicional.class.php");
  $oVehiculoadicional = new Vehiculoadicional();

  $title='Letras de Cambio';
  $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);

  $dts = $oCreditoasiveh->mostrarUno($_GET['d1']);

    $cre_id     = $dts['data']['tb_credito_id'];
    $reg        = mostrar_fecha_hora($dts['data']['tb_credito_reg']);
    $mod        = mostrar_fecha_hora($dts['data']['tb_credito_mod']);
    $apr        = mostrar_fecha_hora($dts['data']['tb_credito_apr']);

    $usureg     = $dts['data']['tb_credito_usureg'];
    $usumod     = $dts['data']['tb_credito_usumod'];
    $usuapr     = $dts['data']['tb_credito_usuapr'];

    $cuotip_id  = $dts['data']['tb_cuotatipo_id'];
    $mon_id     = $dts['data']['tb_moneda_id'];

    if($mon_id == 1){ $moneda = 'S/.'; }else{ $moneda = '$'; }
    
    //datos para calculos de porcentaje inicial
    $cre_tipcam = floatval($dts['data']['tb_credito_tipcam']);

    //fechas
    $cre_feccre=mostrar_fecha($dts['data']['tb_credito_feccre']);
    $cre_fecdes=mostrar_fecha($dts['data']['tb_credito_fecdes']);
    $cre_fecfac=mostrar_fecha($dts['data']['tb_credito_fecfac']);
    $cre_fecpro=mostrar_fecha($dts['data']['tb_credito_fecpro']);
    $cre_fecent=mostrar_fecha($dts['data']['tb_credito_fecent']);

    $cre_fecren=mostrar_fecha($dts['data']['tb_credito_fecren']); //vencimiento de SOAT
    $cre_fecseg=mostrar_fecha($dts['data']['tb_credito_fecseg']); //vencimiento STR
    $cre_fecgps=mostrar_fecha($dts['data']['tb_credito_fecgps']); //vencimiento GPS

    $cre_preaco = floatval($dts['data']['tb_credito_preaco']);
    $cre_int = $dts['data']['tb_credito_int'];
    $cre_numcuo = $dts['data']['tb_credito_numcuo'];
    $cre_linapr = floatval($dts['data']['tb_credito_linapr']);
    $cre_numcuomax = $dts['data']['tb_credito_numcuomax'];
    $cre_tip2 = $dts['data']['tb_credito_tip2']; //5 refinanciado amortizado, 6 refinanciado sin amortizar
    $cre_subtip = $dts['data']['tb_credito_tip1']; //sub tipo de credito 1 venta nueva, 2 adenda, 3 AP, 4 reventa

    $transferencia_bien=$cre_linapr;

    //cuenta deposito
    $cuedep_id = $dts['data']['tb_cuentadeposito_id'];

    $rws=$oCuentadeposito->mostrarUno($cuedep_id);
      $cuedep_num     = $rws['data']['tb_cuentadeposito_num'];
      $cuedep_ban     = $rws['data']['tb_cuentadeposito_ban'];
      $cuedep_mon_nom = $rws['data']['tb_moneda_nom'];
      $cuedep_mon_des = $rws['data']['tb_moneda_des'];
    $rws=null;

    $cre_pla        = $dts['data']['tb_credito_pla'];
    $cre_comdes     = $dts['data']['tb_credito_comdes'];
    $cre_obs        = $dts['data']['tb_credito_obs'];
    $cre_webref     = $dts['data']['tb_credito_webref'];

    $cre_est        = $dts['data']['tb_credito_est'];

    //cliente
    $cli_id         = $dts['data']['tb_cliente_id'];

    $cli_estciv     = $dts['data']['tb_cliente_estciv']; // 1 soltero, 2 casado
    $cli_bien       = $dts['data']['tb_cliente_bien']; // 1 separacion de bienes, 2 sin separacion
    $cli_firm       = $dts['data']['tb_cliente_firm']; // 1 firma solo, 2 firma con conyuge
    // $cli_zon_id     = $dts['data']['tb_zonaregistral_id']; // id de la zona registral donde está el N° de partida
    $cli_numpar     = $dts['data']['tb_cliente_numpar']; //numero de partida del cliente

    $cli_doc        = $dts['data']['tb_cliente_doc'];
    $cli_nom        = $dts['data']['tb_cliente_nom'];
    $cli_dir        = $dts['data']['tb_cliente_dir'];
    $cli_ema        = $dts['data']['tb_cliente_ema'];
    $cli_fecnac     = $dts['data']['tb_cliente_fecnac'];
    $cli_tel        = $dts['data']['tb_cliente_tel'];
    $cli_cel        = $dts['data']['tb_cliente_cel'];
    $cli_telref     = $dts['data']['tb_cliente_telref'];
    $cli_ubigeo     = $dts['data']['tb_ubigeo_cod'];

    //RAZON SOCIAL
    $cli_tip        = $dts['data']['tb_cliente_tip']; // 1 persona natural, 2 persona juridica
    $cliente_empruc = $dts['data']['tb_cliente_empruc'];
    $cliente_emprs  = $dts['data']['tb_cliente_emprs'];
    $cliente_empdir = $dts['data']['tb_cliente_empdir'];

    //vehiculo
    $veh_pre        = $dts['data']['tb_credito_vehpre'];
    $vehmar_id      = $dts['data']['tb_vehiculomarca_id'];
    $vehmar_nom     = $dts['data']['tb_vehiculomarca_nom'];
    $vehmod_id      = $dts['data']['tb_vehiculomodelo_id'];
    $vehmod_nom     = $dts['data']['tb_vehiculomodelo_nom'];
    $vehcla_id      = $dts['data']['tb_vehiculoclase_id'];
    $vehcla_nom     = $dts['data']['tb_vehiculoclase_nom'];
    $vehtip_id      = $dts['data']['tb_vehiculotipo_id'];
    $vehtip_nom     = $dts['data']['tb_vehiculotipo_nom'];
    $veh_pla        = $dts['data']['tb_credito_vehpla'];
    $veh_mot        = $dts['data']['tb_credito_vehsermot'];
    $veh_cha        = $dts['data']['tb_credito_vehsercha'];
    $veh_ano        = $dts['data']['tb_credito_vehano'];
    $veh_col        = $dts['data']['tb_credito_vehcol'];
    $veh_numpas     = $dts['data']['tb_credito_vehnumpas'];
    $veh_numasi     = $dts['data']['tb_credito_vehnumasi'];
    $veh_kil        = $dts['data']['tb_credito_vehkil'];
    $gps_id         = $dts['data']['tb_credito_gps'];
    $gps_pre        = $dts['data']['tb_credito_gpspre'];
    $str_id         = $dts['data']['tb_credito_str'];
    $str_pre        = $dts['data']['tb_credito_strpre'];
    $soa_id         = $dts['data']['tb_credito_soa'];
    $soa_pre        = $dts['data']['tb_credito_soapre'];
    $gas_id         = $dts['data']['tb_credito_gas'];
    $gas_pre        = $dts['data']['tb_credito_gaspre'];
    $otr_pre        = $dts['data']['tb_credito_otrpre'];
    $veh_est        = $dts['data']['tb_credito_vehest'];

    if($str_id>0)
    {
      $rws=$oVehiculoadicional->mostrarUno($str_id);
        // $nom=$dt['tb_vehiculoadicional_nom'];
        // $pre=$dt['tb_vehiculoadicional_pre'];
        // $mon_id=$dt['tb_moneda_id'];
        // $protip_id=$dt['tb_proveedortipo_id'];
        $str_nom = $rws['data']['tb_proveedor_nom'];
      $rws=null;
    }

    if($gas_id>0)
    {
      $rws=$oVehiculoadicional->mostrarUno($gas_id);
        // $nom=$dt['tb_vehiculoadicional_nom'];
        // $pre=$dt['tb_vehiculoadicional_pre'];
        // $mon_id=$dt['tb_moneda_id'];
        // $protip_id=$dt['tb_proveedortipo_id'];
        $gas_nom=$rws['data']['tb_proveedor_nom'];
      $rws=null;
    }
    
  $dts = null;
  
  if($cli_ubigeo>0)
  {
    $dts = $oUbigeo->mostrarUbigeo($cli_ubigeo);
      $ubi_dep = $dts['data']['Departamento'];
      $ubi_pro = $dts['data']['Provincia'];
      $ubi_dis = $dts['data']['Distrito'];
    $dts = null;
  }

  if($usureg>0)
  {
    $rws = $oUsuario->mostrarUno($usureg);
      $usureg = $rws['data']['tb_usuario_nom']." ".$rws['data']['tb_usuario_ape'];
      $rws = null;
  }

  if($usumod>0)
  {
    $rws=$oUsuario->mostrarUno($usumod);
      $usumod = $rws['data']['tb_usuario_nom']." ".$rws['data']['tb_usuario_ape'];
    $rws = null;
  }

  if($usuapr>0)
  {
    $mos_usuapr = 1;
    $rws=$oUsuario->mostrarUno($usuapr);
      $usuapr = $rws['data']['tb_usuario_nom']." ".$rws['data']['tb_usuario_ape'];
    $rws = null;
  }

  //obtenemos información del cliente, sus nombres, su esposa(o), copropietario o apoderado
  $esposa = ''; $apoderado = '';
  $clientes = '<strong>'.$cli_nom.'</strong>, identificado con DNI N° <strong>'.$cli_doc.'</strong> con domicilio en <strong>'.trim($cli_dir).'</strong>, del Distrito de <strong>'.$ubi_dis.'</strong>, Provincia de <strong>'.$ubi_pro.'</strong>, del Departamento de <strong>'.$ubi_dep.'</strong>';

  //si el cliente es casado
  if($cli_estciv == 2){
    $esposa = devolver_datos_esposa_apoderado($cli_id, $cli_bien, $cli_firm, 1); //parametro 1 CONYUGAL
    $clientes = $clientes.' '.$esposa;
  }

  $apoderado = devolver_datos_esposa_apoderado($cli_id, $cli_bien, $cli_firm, 3); //parametro 3 apoderado

  if($apoderado != '')
    $clientes = $clientes.', '.$apoderado;

  //verificar si el cliente tiene co-propietarios
  $copro = devolver_datos_copropietarios($cli_id);

  if($copro != '')
    $clientes .= ' '.$copro;

  $clientes = str_replace(' ;', ';', $clientes);

  list($day, $month, $year) = explode('-', $cre_feccre);

  $feccre_dia = $day;
  $feccre_mes = $month;
  $feccre_ano = $year;
  $fec_width = 'width="32%"';

  global $image_file;

  $tip1_cliente_nom = $cli_nom;
  $tip1_cliente_dir = $cli_dir;
  $tip1_cliente_doc = $cli_doc;
  $tip2_cliente_nom = '';
  $tip2_cliente_doc = '';

  if(intval($cli_tip) == 2){
    $tip1_cliente_nom = $cliente_emprs;
    $tip1_cliente_dir = $cliente_empdir;
    $tip1_cliente_doc = $cliente_empruc;
    $tip2_cliente_nom = $cli_nom;
    $tip2_cliente_doc = $cli_doc;
  }

  class MYPDF extends TCPDF {

    public function Header() {
      $image_file = K_PATH_IMAGES.'logo.jpg';
      //$this->Image($image_file, 20, 10, 71, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
      // Set font
      //$this->SetFont('helvetica', 'B', 20);
      // Title
      //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    public function Footer() {

      $style = array(
        'position' => 'L',
        'align' => 'L',
        'stretch' => false,
        'fitwidth' => true,
        'cellfitalign' => '',
        'border' => false,
        'padding' => 0,
        'fgcolor' => array(0,0,0),
        'bgcolor' => false,
        'text' => false
      //     'font' => 'helvetica',
      //     'fontsize' => 8,
      //     'stretchtext' => 4
      );

      $this -> SetY(-24);
      // Page number
      $this->SetFont('helvetica', '', 9);
      //$this->SetTextColor(0,0,0);
      //$this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
      
      $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
      
      //$this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
      //$this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
    }
  }

  // create new PDF document
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.inticap.com');
    $pdf->SetTitle($title);
    $pdf->SetSubject('www.inticap.com');
    $pdf->SetKeywords('www.inticap.com');

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(10, 10, 10);// left top right
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, 15);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //set some language-dependent strings
    $pdf->setLanguageArray($l);

    // ---------------------------------------------------------
    // add a page
  $pdf->AddPage('P', 'A4');

  $estilos = '
    <style>
      .tb_cab{
        border: 1px solid black;
      }
      .verticalText {
        
        transform: rotate(180deg);
      }
    </style>
  ';

  $a = 0;
  $i = 0;
  $total = $cre_numcuo;
  $vacia = '<table width="100%" style="border-bottom: 1px solid black">
          <tr>
            <td width ="20%" border="1" style="font-size:6pt;" align="justify">(1) En caso de mora, esta Letra de Cambio generará las tasas de intereses compensatorios y moratorio más alta que la ley permita a su último Tenedor.<br>(2) El plazo de sus vencimiento podrá ser prorrogado por el Tenedor por el plazo que éste señale, sin que sea necesaria la intervención del obligado principal ni de los solidarios.<br>(3) Su importe debe ser pagado solo en la misma moneda que se expresa este título valor.<br>(4) Esta Letra de Cambio no requiere ser protestada por falta de pago.<br>
              <p><br></p>

              <p width="100" align="center">_______________________<br>
                Aceptante</p>
              <p><br></p>
              <p><br></p>
              <p width="100" align="center">_______________________<br>
                Aceptante</p>
              <p><br></p>
            </td>
            <td width ="80%">
              
              <table width="100%" cellpadding="2" border="1">
                <tr>
                  <th align="center" style="font-size:6pt;">NUMERO</th>
                  <th align="center" style="font-size:6pt;">REF. DEL GIRADOR</th>
                  <th align="center" style="font-size:6pt;">LUGAR DE GIRO</th>
                  <th align="center" style="font-size:6pt;">FECHA DE GIRO</th>
                  <th align="center" style="font-size:6pt;">VENCIMIENTO</th>
                  <th align="center" style="font-size:6pt;">MONEDA E IMPORTE</th>
                </tr>
                <tr>
                  <td align="center" style="font-size:6pt;" rowspan="2"></td>
                  <td align="center" style="font-size:6pt;" rowspan="2"></td>
                  <td align="center" style="font-size:6pt;" rowspan="2"></td>
                  <td align="center" style="font-size:6pt;">DIA / MES / AÑO</td>
                  <td align="center" style="font-size:6pt;">DIA / MES / AÑO</td>
                  <td align="center" style="font-size:6pt;" rowspan="2"></td>
                </tr>
                <tr>
                  <td align="center" style="font-size:6pt;"></td>
                  <td align="center" style="font-size:6pt;"></td>
                </tr>
              </table>
              
              <table width="100%" cellpadding="2">
                <tr>
                  <td style="font-size:6pt;">Por esta LETRA DE CAMBIO, se servirá(n) pagar incondicionalmente a la Orden de <b>Inversiones y Préstamos del Norte SAC</b>. La cantidad de:</td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" border="1"></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;">En el siguiente lugar de pago, o con cargo en la cuenta del Banco:</td>
                </tr>
              </table>
              
              <table width="100%" class="tb_cab" cellpadding="2">
                <tr>
                  <td style="font-size:6pt;" colspan="5">Girado: <b></b></td>
                  <td style="font-size:6pt;" colspan="5" border="1">Importe a debitar en la siguiente cuenta del Banco que se indica</td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Domicilio: <b></b></td>
                  <td style="font-size:6pt;" border="1">BANCO</td>
                  <td style="font-size:6pt;" border="1">OFICINA</td>
                  <td style="font-size:6pt;" colspan="2" border="1">NUMERO DE CUENTA</td>
                  <td style="font-size:6pt;" border="1">D.C.</td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">D.0.I: <b></b></td>
                  <td style="font-size:6pt;" border="1" rowspan="2"></td>
                  <td style="font-size:6pt;" border="1" rowspan="2"></td>
                  <td style="font-size:6pt;" colspan="2" border="1" rowspan="2"></td>
                  <td style="font-size:6pt;" border="1" rowspan="2"></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Telf: <b></b></td>
                </tr>
              </table>

              <table width="100%" class="tb_cab" cellpadding="2">
                <tr>
                  <td style="font-size:6pt;" colspan="5">Fiador: <b></b></td>
                  <td style="font-size:6pt; border-left: 1px solid black;" colspan="2">Nombre / Denominación o Razón Social del Girador</td>
                  <td colspan="3" style="font-size:8pt;"><b>INVERSIONES Y PRÉSTAMOS DEL NORTE SAC</b></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Aval Permanente: <b></b></td>
                  <td style="font-size:6pt; border-left: 1px solid black;">D.O.I.</td>
                  <td colspan="4"></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Domicilio: <b></b></td>
                  <td style="font-size:6pt; border-left: 1px solid black;"></td>
                  <td style="font-size:6pt;" colspan="2" align="center" rowspan="2">
                    _______________________<br>
                    Firma
                  </td>
                  <td style="font-size:6pt;" colspan="2" align="center" rowspan="2">
                    _______________________<br>
                    Firma
                  </td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="3">D.O.I: <b></b></td>
                  <td style="font-size:6pt;" colspan="2">Telf:</td>
                  <td colspan="5" style="font-size:6pt; border-left: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Firma: <b></b></td>
                  <td colspan="5" style="font-size:6pt; border-left: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Nombre del Representante: <b></b>
                    <p><br></p>
                  </td>
                  <td colspan="5" style="font-size:6pt; border-left: 1px solid black;">Nombre del Representante(s): <b></b></td>
                </tr>
              </table>       
            </td>
          </tr>
        </table>
        <table width="100%">
          <tr width="100%">
            <td style="font-size:6pt;">No escribir ni firmar debajo de esta línea</td>
          </tr>
        </table>
        <p><hr></p>
      ';

  $registros = $oCuota->filtrar(2, $cre_id); // 2 creditotipo 2 CRE ASIVEH

  for ($i=0; $i<count($registros);$i++)
  {
    // while ($dt = mysql_fetch_array($dts)){
      $html .= '
        <table width="100%" style="border-bottom: 1px solid black;">
          <tr>
            <td width ="20%" border="1" style="font-size:6pt;" align="justify">(1) En caso de mora, esta Letra de Cambio generará las tasas de intereses compensatorios y moratorio más alta que la ley permita a su último Tenedor.<br>(2) El plazo de sus vencimiento podrá ser prorrogado por el Tenedor por el plazo que éste señale, sin que sea necesaria la intervención del obligado principal ni de los solidarios.<br>(3) Su importe debe ser pagado solo en la misma moneda que se expresa este título valor.<br>(4) Esta Letra de Cambio no requiere ser protestada por falta de pago.<br>
              <p><br></p>

              <p width="100" align="center">_______________________<br>
                Aceptante</p>
              <p><br></p>
              <p><br></p>
              <p width="100" align="center">_______________________<br>
                Aceptante</p>
            </td>
            <td width ="80%">              
              <table width="100%" cellpadding="2" border="1">
                <tr>
                  <th align="center" style="font-size:6pt;">NUMERO</th>
                  <th align="center" style="font-size:6pt;">REF. DEL GIRADOR</th>
                  <th align="center" style="font-size:6pt;">LUGAR DE GIRO</th>
                  <th align="center" style="font-size:6pt;">FECHA DE GIRO</th>
                  <th align="center" style="font-size:6pt;">VENCIMIENTO</th>
                  <th align="center" style="font-size:6pt;">MONEDA E IMPORTE</th>
                </tr>
                <tr>
                  <td align="center" style="font-size:6pt;" rowspan="2">'.$dt['tb_cuota_num'].'/'.$cre_numcuo.'</td>
                  <td align="center" style="font-size:6pt;" rowspan="2"></td>
                  <td align="center" style="font-size:6pt;" rowspan="2">Chiclayo</td>
                  <td align="center" style="font-size:6pt;">DIA / MES / AÑO</td>
                  <td align="center" style="font-size:6pt;">DIA / MES / AÑO</td>
                  <td align="center" style="font-size:6pt;" rowspan="2">'.$moneda.' '.formato_moneda($dt['tb_cuota_cuo']).'</td>
                </tr>
                <tr>
                  <td align="center" style="font-size:6pt;">'.mostrar_fecha($dt['tb_cuota_fec']).'</td>
                  <td align="center" style="font-size:6pt;">'.mostrar_fecha($dt['tb_cuota_fec']).'</td>
                </tr>
              </table>              
              <table width="100%" cellpadding="2">
                <tr>
                  <td style="font-size:6pt;">Por esta LETRA DE CAMBIO, se servirá(n) pagar incondicionalmente a la Orden de <b>Inversiones y Préstamos del Norte SAC</b>. La cantidad de:</td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" border="1">'.numtoletras($dt['tb_cuota_cuo'], $mon_id).'</td>
                </tr>
                <tr>
                  <td style="font-size:6pt;">En el siguiente lugar de pago, o con cargo en la cuenta del Banco:</td>
                </tr>
              </table>              
              <table width="100%" class="tb_cab" cellpadding="2">
                <tr>
                  <td style="font-size:6pt;" colspan="5">Girado: <b>'.$tip1_cliente_nom.'</b></td>
                  <td style="font-size:6pt;" colspan="5" border="1">Importe a debitar en la siguiente cuenta del Banco que se indica</td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Domicilio: <b>'.$tip1_cliente_dir.'</b></td>
                  <td style="font-size:6pt;" border="1">BANCO</td>
                  <td style="font-size:6pt;" border="1">OFICINA</td>
                  <td style="font-size:6pt;" colspan="2" border="1">NUMERO DE CUENTA</td>
                  <td style="font-size:6pt;" border="1">D.C.</td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">D.0.I: <b>'.$tip1_cliente_doc.'</b></td>
                  <td style="font-size:6pt;" border="1" rowspan="2"></td>
                  <td style="font-size:6pt;" border="1" rowspan="2"></td>
                  <td style="font-size:6pt;" colspan="2" border="1" rowspan="2"></td>
                  <td style="font-size:6pt;" border="1" rowspan="2"></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Telf: <b>'.$cli_cel.'</b></td>
                </tr>
              </table>
              <table width="100%" class="tb_cab" cellpadding="2">
                <tr>
                  <td style="font-size:6pt;" colspan="5">Fiador: <b></b></td>
                  <td style="font-size:6pt; border-left: 1px solid black;" colspan="2">Nombre / Denominación o Razón Social del Girador</td>
                  <td colspan="3" style="font-size:6pt;"><b>INVERSIONES Y PRÉSTAMOS DEL NORTE SAC</b></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Aval Permanente: <b>'.$tip2_cliente_nom.'</b></td>
                  <td style="font-size:6pt; border-left: 1px solid black;">D.O.I.</td>
                  <td colspan="4" style="font-size:6pt;"><b>20600752872</b></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Domicilio: <b></b></td>
                  <td style="font-size:6pt; border-left: 1px solid black;"></td>
                  <td colspan="4" rowspan="2">
                  </td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="3">D.O.I: <b>'.$tip2_cliente_doc.'</b></td>
                  <td style="font-size:6pt;" colspan="2">Telf:</td>
                  <td colspan="5" style="font-size:6pt; border-left: 1px solid black;"></td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Firma: <b></b></td>
                  <td style="font-size:6pt; border-left: 1px solid black;"></td>
                  <td style="font-size:6pt;" colspan="2" align="center">
                    _______________________<br>
                    Firma
                  </td>
                  <td style="font-size:6pt;" colspan="2" align="center">
                    _______________________<br>
                    Firma
                  </td>
                </tr>
                <tr>
                  <td style="font-size:6pt;" colspan="5">Nombre del Representante: <b></b>
                    <p><br></p>
                  </td>
                  <td colspan="5" style="font-size:6pt; border-left: 1px solid black;">
                    Nombre del Representante(s): <b>Sr. Victor Manuel Vargas Torres</b><br>
                    D.O.I. <b>44760801</b>
                  </td>
                </tr>
              </table>       
            </td>
          </tr>
        </table>
        <table width="100%">
          <tr width="100%">
            <td style="font-size:6pt;">No escribir ni firmar debajo de esta línea</td>
          </tr>
        </table>
      ';

      $a++;

      if($a == 1 || $a == 2){
        $html .= '
          <p><hr></p>';
      }
      if($a == 3)
        $a = 0;

      $pdf->writeHTML($estilos.$html, true, 0, true, true);

      $i++;

      if($i%3 == 0){
        $pdf->AddPage();
        $pdf->Ln();
      }
      if($i == $total){
        $pdf->writeHTML($estilos.$vacia, true, 0, true, true);
      }

      $html = '';
    }
  $registros = null;

  // set core font
  //$pdf->SetFont('arial', '', 10);

  $pdf->lastPage();

  //Close and output PDF document
  $nombre_archivo=$codigo."_".$title.".pdf";
  $pdf->Output($nombre_archivo, 'I');
  //============================================================+
  // END OF FILE                                                
  //============================================================+
?>