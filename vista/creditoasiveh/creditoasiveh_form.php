<?php
require_once("../../core/usuario_sesion.php");
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once('../creditoasiveh/Creditoasiveh.class.php');
require_once('../usuario/Usuario.class.php');
require_once('../cuota/Cuota.class.php');

$oCreditoasiveh = new Creditoasiveh();
$oUsuario = new Usuario();
$oCuota = new Cuota();

$action = $_POST['action'];
$creditoasiveh_id = $_POST['creditoasiveh_id'];

$dts = $oCreditoasiveh->mostrarUno($creditoasiveh_id);


// --------- DATOS DEL CLIENTE --------- //
if ($dts['estado'] == 1) {
	$reg		 = mostrar_fecha_hora($dts['data']['tb_credito_reg']);
	$mod		 = mostrar_fecha_hora($dts['data']['tb_credito_mod']);
	$apr		 = mostrar_fecha_hora($dts['data']['tb_credito_apr']);
	$usureg		 = $dts['data']['tb_credito_usureg'];
	$usumod		 = $dts['data']['tb_credito_usumod'];
	$usuapr		 = $dts['data']['tb_credito_usuapr'];
	$cre_tip1 	 = $dts['data']['tb_credito_tip1'];
	$cre_tip2 	 = $dts['data']['tb_credito_tip2']; //1 credito regular, 2 credito especifico, 3 reporgramado, 4 cuota balon, 7 transferido
	$mon_id		 = $dts['data']['tb_moneda_id'];
	$cuedep_id 	 = $dts['data']['tb_cuentadeposito_id'];
	$cre_tipcam	 = $dts['data']['tb_credito_tipcam'];
	$cre_preaco  = $dts['data']['tb_credito_preaco'];
	$cre_int 	 = $dts['data']['tb_credito_int'];
	$cre_numcuo  = $dts['data']['tb_credito_numcuo'];
	$cre_linapr  = $dts['data']['tb_credito_linapr'];
	$cre_ini 	 = $dts['data']['tb_credito_ini'];
	$cre_feccre	 = mostrar_fecha($dts['data']['tb_credito_feccre']);
	$cre_fecdes	 = mostrar_fecha($dts['data']['tb_credito_fecdes']);
	$cre_fecfac	 = mostrar_fecha($dts['data']['tb_credito_fecfac']);
	$cre_fecpro	 = mostrar_fecha($dts['data']['tb_credito_fecpro']);
	$cre_fecent	 = mostrar_fecha($dts['data']['tb_credito_fecent']);
	$cre_fecren	 = mostrar_fecha($dts['data']['tb_credito_fecren']); //venicimiento del soat
	$cre_fecseg	 = mostrar_fecha($dts['data']['tb_credito_fecseg']); //vencimiento del seguro
	$cre_fecgps	 = mostrar_fecha($dts['data']['tb_credito_fecgps']); // venicimiento del gps
	$cre_fpagimp = mostrar_fecha($dts['data']['tb_credito_pagimp']); //fecha del pago del inpuesto vehicular
	$cre_comdes  = $dts['data']['tb_credito_comdes'];
	$cre_obs 	 = $dts['data']['tb_credito_obs'];
	$cre_est	 = $dts['data']['tb_credito_est'];

	$cli_id 	   = $dts['data']['tb_cliente_id'];
	$cli_doc 	   = $dts['data']['tb_cliente_doc'];
	$cli_nom	   = $dts['data']['tb_cliente_nom'];
	$cli_ape	   = $dts['data']['tb_cliente_ape'];
	$cli_dir	   = $dts['data']['tb_cliente_dir'];
	$cli_ema	   = $dts['data']['tb_cliente_ema'];
	$cli_fecnac    = $dts['data']['tb_cliente_fecnac'];
	$cli_tel	   = $dts['data']['tb_cliente_tel'];
	$cli_protel    = $dts['data']['tb_cliente_protel']; //propietario telefono
	if (empty($cli_protel)) {
		$cli_protel = '-';
	}
	$cli_cel	   = $dts['data']['tb_cliente_cel'];
	$cli_procel    = $dts['data']['tb_cliente_procel']; //propietario celular
	if (empty($cli_procel)) {
		$cli_procel = '-';
	}
	$cli_telref    = $dts['data']['tb_cliente_telref'];
	$cli_protelref = $dts['data']['tb_cliente_protelref']; //propietario telefono referencial
	if (empty($cli_protelref)) {
		$cli_protelref = '-';
	}

	$veh_pre		= $dts['data']['tb_credito_vehpre'];
	$vehmar_id		= $dts['data']['tb_vehiculomarca_id'];
	$tb_vehiculomarca_nom = $dts['data']['tb_vehiculomarca_nom'];
	$vehmod_id		= $dts['data']['tb_vehiculomodelo_id'];
	$tb_vehiculomodelo_nom = $dts['data']['tb_vehiculomodelo_nom'];
	$vehcla_id		= $dts['data']['tb_vehiculoclase_id'];
	$tb_vehiculoclase_nom = $dts['data']['tb_vehiculoclase_nom'];
	$vehtip_id		= $dts['data']['tb_vehiculotipo_id'];
	$tb_vehiculotipo_nom = $dts['data']['tb_vehiculotipo_nom'];
	$veh_pla		= $dts['data']['tb_credito_vehpla'];
	$veh_mot		= $dts['data']['tb_credito_vehsermot'];
	$veh_cha		= $dts['data']['tb_credito_vehsercha'];
	$veh_ano		= $dts['data']['tb_credito_vehano'];
	$veh_col		= $dts['data']['tb_credito_vehcol'];
	$veh_numpas	 	= $dts['data']['tb_credito_vehnumpas'];
	$veh_numasi	 	= $dts['data']['tb_credito_vehnumasi'];
	$veh_kil		= $dts['data']['tb_credito_vehkil'];
	$gps_id			= $dts['data']['tb_credito_gps'];
	$str_id			= $dts['data']['tb_credito_str'];
	$soa_id			= $dts['data']['tb_credito_soa'];
	$gas_id			= $dts['data']['tb_credito_gas'];
	$gps_pre		= $dts['data']['tb_credito_gpspre'];
	$str_pre		= $dts['data']['tb_credito_strpre'];
	$soa_pre		= $dts['data']['tb_credito_soapre'];
	$gas_pre		= $dts['data']['tb_credito_gaspre'];
	$otr_pre		= $dts['data']['tb_credito_otrpre'];
	$veh_est		= $dts['data']['tb_credito_vehest'];
	$cre_entcon 	= $dts['data']['tb_credito_entcon'];
	$cre_tamtan 	= $dts['data']['tb_credito_tamtan'];
	$cre_captan 	= $dts['data']['tb_credito_captan'];
	$subper			= $dts['data']['tb_cuotasubperiodo_id'];

	if ($usureg > 0) {
		$consultarDatosUsuario = $oUsuario->mostrarUno($usureg);
		$usureg	= $consultarDatosUsuario['data']['tb_usuario_nom'] . " " . $consultarDatosUsuario['data']['tb_usuario_ape'];
	}

	if ($usumod > 0) {
		$consultarDatosUsuario = $oUsuario->mostrarUno($usumod);
		$usumod	= $consultarDatosUsuario['data']['tb_usuario_nom'] . " " . $consultarDatosUsuario['data']['tb_usuario_ape'];
	}

	if ($usuapr > 0) {
		$mos_usuapr = 1;
		$consultarDatosUsuario = $oUsuario->mostrarUno($usuapr);
		$usuapr	= $consultarDatosUsuario['data']['tb_usuario_nom'] . " " . $consultarDatosUsuario['data']['tb_usuario_ape'];
	}
} else {
	echo '<span> Resultados no encontrados. </span> -->' . $dts['estado'] - ' -- ' . $creditoasiveh_id;
	exit();
}
// --------- DATOS DEL CLIENTE --------- //

?>
<script type="text/javascript">
	$(function() {
		cmb_cuedep_id('<?php echo $cuedep_id?>');
	});

	function cmb_cuedep_id(ids)
	{	
		$.ajax({
			type: "POST",
			url: "./vista/cuentadeposito/cuentadeposito_select.php",
			async:false,
			dataType: "html",
			data: ({
				cuentadeposito_id: ids
			}),
			beforeSend: function() {
				$('#cmb_cuedep_id').html('<option value="">Cargando...</option>');
			},
			success: function(html){
				$('#cmb_cuedep_id').html(html);
			}
		});
	}
</script>

<style>
	.form-control:disabled {
		background-color: white;
	}
	.row>.col-md-5elements {
		width: 20%;
		padding: 10px;
		float: left;
	}
</style>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_creditoasiveh" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<input type="hidden" name="hdd_cre_id" id="hdd_cre_id" value="<?php echo $creditoasiveh_id;?>">
			<input type="hidden" name="hdd_cre_tip" id="hdd_cre_tip" value="<?php echo $cre_tip1 ?>">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
				<h4 class="modal-title text-center"> INFORMACIÓN DE CRÉDITO ASCVEH: <?php echo $creditoasiveh_id; ?> </h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-12">
							<h4 class="text-teal"> DATOS DEL CLIENTE </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
						<div class="row">
							<div class="col-md-12">
								<div class="row mt-4 mb-4">
									<div class="col-lg-2">
										<div class="form-group">
											<label for="txt_ven_cli_doc">RUC/DNI:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-address-card"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_doc" id="txt_ven_cli_doc" class="form-control text-center" value="<?php echo $cli_doc; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="txt_ven_cli_nom">Cliente:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-user"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_nom" id="txt_ven_cli_nom" class="form-control text-center" value="<?php echo $cli_nom; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="txt_ven_cli_dir">Dirección:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_dir" id="txt_ven_cli_dir" class="form-control text-center" value="<?php echo $cli_dir; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="txt_ven_cli_fecnac">Fecha de Nacimiento:</label>
											<div class="input-group">
												<input type="text" name="txt_ven_cli_fecnac" id="txt_ven_cli_fecnac" class="form-control text-center" value="<?php echo mostrar_fecha($cli_fecnac); ?>" disabled />
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row mb-4">
									<div class="col-md-3">
										<div class="form-group">
											<label for="txt_ven_cli_ema">Email:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-envelope"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_ema" id="txt_ven_cli_ema" class="form-control text-center" value="<?php echo $cli_ema; ?>" disabled />
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="txt_ven_cli_tel" class="control-label">Teléfono:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_tel" id="txt_ven_cli_tel" class="form-control text-center" value="<?php echo $cli_tel; ?>" disabled /><br>
												<span class="input-group-btn">
													<button type="button" class="btn btn-default bt-sm"> <?php echo $cli_protel; ?> </button>
												</span>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="txt_ven_cli_cel" class="control-label">Celular:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_cel" id="txt_ven_cli_cel" class="form-control text-center" value="<?php echo $cli_cel; ?>" disabled /><br>
												<span class="input-group-btn">
													<button type="button" class="btn btn-default bt-sm"> <?php echo $cli_procel; ?> </button>
												</span>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="txt_ven_cli_telref" class="control-label">Teléfono de Ref:</label>
											<div class="input-group">
												<div class="input-group-addon">
													<span><i class="fa fa-phone-alt"></i></span>
												</div>
												<input type="text" name="txt_ven_cli_telref" id="txt_ven_cli_telref" class="form-control text-center" value="<?php echo $cli_telref; ?>" disabled /><br>
												<span class="input-group-btn">
													<button type="button" class="btn btn-default bt-sm"> <?php echo $cli_protelref; ?> </button>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row mt-4 mb-4">
						<div class="col-md-6">
							<h4 class="text-teal"> DATOS DEL VEHÍCULO </h4>
							<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
								<div class="row mt-4 mb-4">
									<div class="col-md-6">
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Marca:</label>
													<input type="text" class="form-control" value="<?php echo $tb_vehiculomarca_nom; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Modelo:</label>
													<input type="text" class="form-control" value="<?php echo $tb_vehiculomodelo_nom; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Placa:</label>
													<input type="text" class="form-control" value="<?php echo $veh_pla; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Serie Motor:</label>
													<input type="text" class="form-control" value="<?php echo $veh_mot; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Color:</label>
													<input type="text" class="form-control" value="<?php echo $veh_col; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>N° Pasajeros:</label>
													<input type="text" class="form-control" value="<?php echo $veh_numpas; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>GPS:</label>
													<!-- <select name="cmb_gps_id" id="cmb_gps_id" class="form-control" disabled></select> -->
													<input type="text" class="form-control" value="-" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>STR:</label>
													<!-- <select name="cmb_str_id" id="cmb_str_id" class="form-control" disabled></select> -->
													<input type="text" class="form-control" value="-" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>GPS Precio:</label>
													<input type="text" class="form-control" value="<?php echo $gps_pre; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>STR Precio:</label>
													<input type="text" class="form-control" value="<?php echo $str_pre; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Otros:</label>
													<input type="text" class="form-control" value="<?php echo $otr_pre; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Vehículo Precio:</label>
													<input type="text" class="form-control" name="txt_veh_pre" type="text" id="txt_veh_pre" value="<?php echo $veh_pre; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-12">
												<div class="form-group">
													<label>Entregado con:</label>
													<textarea class="form-control" name="txt_cre_entcon" id="txt_cre_entcon" style="resize: vertical;" rows="3" disabled><?php echo $cre_entcon; ?></textarea>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label for="cmb_cre_tamtan">Tipo de Tanque</label>
													<select class="form-control" name="cmb_cre_tamtan" id="cmb_cre_tamtan" disabled>
														<option value="">-</option>
														<option value="1" <?php if ($cre_tamtan == 1) echo 'selected' ?>>LENTEJA</option>
														<option value="2" <?php if ($cre_tamtan == 2) echo 'selected' ?>>CILÍNDRICO</option>
														<option value="3" <?php if ($cre_tamtan == 3) echo 'selected' ?>>TOROIDAL</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="cmb_cre_captan">Capacidades de Tanque</label>
													<select class="form-control" name="cmb_cre_captan" id="cmb_cre_captan" disabled>
														<option value="">-</option>
														<option value="1" <?php if ($cre_captan == 1) echo 'selected' ?>>6.5 GALONES</option>
														<option value="2" <?php if ($cre_captan == 2) echo 'selected' ?>>7 GALONES</option>
														<option value="3" <?php if ($cre_captan == 3) echo 'selected' ?>>9.5 GALONES</option>
														<option value="4" <?php if ($cre_captan == 4) echo 'selected' ?>>9-12 GALONES</option>
														<option value="5" <?php if ($cre_captan == 5) echo 'selected' ?>>14.4-20 GALONES</option>
														<option value="6" <?php if ($cre_captan == 6) echo 'selected' ?>>16-20 GALONES</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Clase:</label>
													<input type="text" class="form-control" value="<?php echo $tb_vehiculoclase_nom; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Tipo:</label>
													<input type="text" class="form-control" value="<?php echo $tb_vehiculotipo_nom; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>Serie Chasis:</label>
													<input type="text" class="form-control" value="<?php echo $veh_cha; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Año:</label>
													<input type="text" class="form-control" value="<?php echo $veh_ano; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>N° Asientos:</label>
													<input type="text" name="txt_veh_numasi" id="txt_veh_numasi" class="form-control" value="<?php echo $veh_numasi; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Kilometraje:</label>
													<input type="text" name="txt_veh_kil" id="txt_veh_kil" class="form-control" value="<?php echo $veh_kil; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>SOAT:</label>
													<!-- <select name="cmb_soa_id" id="cmb_soa_id" class="form-control" disabled></select> -->
													<input type="text" class="form-control" value="-" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>GAS:</label>
													<!-- <select name="cmb_gas_id" id="cmb_gas_id" class="form-control" disabled></select> -->
													<input type="text" class="form-control" value="-" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-6">
												<div class="form-group">
													<label>SOAT Precio:</label>
													<input type="text" class="form-control" value="<?php echo $soa_pre; ?>" disabled>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>GAS Precio:</label>
													<input type="text" class="form-control" value="<?php echo $gas_pre; ?>" disabled>
												</div>
											</div>
										</div>
										<div class="row mb-4">
											<div class="col-md-12">
												<div class="form-group">
													<label for="txt_veh_est">Estado:</label>
													<textarea class="form-control" name="txt_veh_est" id="txt_veh_est" style="resize: vertical;" rows="3" disabled><?php echo $veh_est ?></textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="txt_ven_cli_fecnac">Vencimiento GPS:</label>
													<div class="input-group">
														<input type="text" class="form-control" value="<?php echo mostrar_fecha($cre_fecgps); ?>" disabled />
														<div class="input-group-addon">
															<span><i class="fa fa-calendar-day"></i></span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="txt_ven_cli_fecnac">Vencimiento STR:</label>
													<div class="input-group">
														<input type="text" class="form-control" value="<?php echo mostrar_fecha($cre_fecseg); ?>" disabled />
														<div class="input-group-addon">
															<span><i class="fa fa-calendar-day"></i></span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row mt-4">
											<div class="col-md-6">
												<div class="form-group">
													<label for="txt_ven_cli_fecnac">Vencimiento SOAT:</label>
													<div class="input-group">
														<input type="text" class="form-control" value="<?php echo mostrar_fecha($cre_fecren); ?>" disabled />
														<div class="input-group-addon">
															<span><i class="fa fa-calendar-day"></i></span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="txt_ven_cli_fecnac">Vencimiento I.V:</label>
													<div class="input-group">
														<input type="text" class="form-control" value="<?php echo mostrar_fecha($cre_fpagimp); ?>" disabled />
														<div class="input-group-addon">
															<span><i class="fa fa-calendar-day"></i></span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<h4 class="text-blue"> DATOS DEL CRÉDITO </h4>
							<div class="container-fluid" style="border: 1px solid #71baf5; border-radius: 8px; background-color: rgba(113, 186, 245, 0.3);">
								<div class="row mt-4 mb-4">
									<div class="col-md-3">
										<div class="form-group">
											<label>Tipo de Crédito:</label>
											<select id="cmb_cre_tip" name="cmb_cre_tip" class="form-control" disabled>
												<option value="">-</option>
												<option value="1" <?php if ($cre_tip1 == 1) echo 'selected' ?>>VENTA NUEVA</option>
												<option value="2" <?php if ($cre_tip1 == 2) echo 'selected' ?>>ADENDA</option>
												<option value="3" <?php if ($cre_tip1 == 3) echo 'selected' ?>>ACUERDO PAGO</option>
												<option value="4" <?php if ($cre_tip1 == 4) echo 'selected' ?>>RE-VENTA</option>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Tipo Cuota:</label>
											<select id="cmb_cuotip_id" name="cmb_cuotip_id" class="form-control" disabled>
												<option value="4">CUOTA FIJA</option>
												<option value="3">CUOTA LIBRE</option>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div id="tip_ade" style="display: block;">
											<div class="form-group">
												<label>Tipo Adenda:</label>
												<select id="cmb_cre_tipade" name="cmb_cre_tipade" class="form-control">
													<option value="1" <?php if ($cre_tip2 == 1) echo 'selected' ?>>REGULAR</option>
													<option value="4" <?php if ($cre_tip2 == 4) echo 'selected' ?>>CUOTA BALON</option>
												</select>
											</div>
										</div>
										<div id="div_cuo_res" style="display: none;">
											<div class="form-group">
												<label>Cuotas Restantes:</label>
												<input type="number" id="txt_cre_cuo_res" name="txt_cre_cuo_res" class="form-control" value="<?php echo $cuo_res; ?>">
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Periodo:</label>
											<select id="cmb_cre_per" name="cmb_cre_per" class="form-control" disabled>
												<option value="4">MENSUAL</option>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Tipo de Préstamo:</label>
											<input type="text" class="form-control" value="CREDITO ASCVEH" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>Inicial:</label>
											<input type="text" class="form-control text-right moneda" name="txt_cre_ini" id="txt_cre_ini" disabled value="<?php echo $cre_ini ?>">
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>Cambio:</label>
											<input type="text" id="txt_cre_tipcam" name="txt_cre_tipcam" class="form-control text-right" value="<?php echo $cre_tipcam ?>" disabled>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="cmb_mon_id">Moneda:</label>
											<select name="cmb_mon_id" id="cmb_mon_id" class="form-control" disabled>
												<option value="0">Seleccione:</option>
												<option value="1" <?php if ($mon_id == 1) echo 'selected' ?>>Soles (S/)</option>
												<option value="2" <?php if ($mon_id == 2) echo 'selected' ?>>Dolares ($)</option>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Sub Periodo:</label>
											<select name="cmb_cre_subper" id="cmb_cre_subper" class="form-control">
												<option value="1" <?php if ($subper == 1) echo 'selected' ?>>MENSUAL</option>
												<option value="2" <?php if ($subper == 2) echo 'selected' ?>>QUINCENAL</option>
												<option value="3" <?php if ($subper == 3) echo 'selected' ?>>SEMANAL</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row mb-4">
									<div class="col-md-3">
										<div class="form-group">
											<label>Precio Acordado:</label>
											<?php if ($action != 'ad_regular') { ?>
												<input type="text" class="form-control text-right moneda2" id="txt_cre_preaco" name="txt_cre_preaco" value="<?php echo $cre_preaco ?>" maxlength="10">
											<?php } else { ?>
												<input type="text" class="form-control text-right moneda2" id="txt_cre_preaco_ad" name="txt_cre_preaco_ad" value="<?php echo $cre_preaco ?>" maxlength="10">
											<?php } ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>Interés (%):</label>
											<input type="text" class="form-control text-right porcentaje" id="txt_cre_int" name="txt_cre_int" value="<?php echo $cre_int ?>">
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>N° Cuotas:</label>
											<input type="text" id="txt_cre_numcuo" name="txt_cre_numcuo" class="form-control text-center" value="<?php echo $cre_numcuo ?>" maxlength="2">
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label>Línea Aprobada:</label>
											<input type="text" id="txt_cre_linapr" name="txt_cre_linapr" class="form-control text-right" value="<?php echo $cre_linapr ?>" maxlength="10" disabled>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label>Retener en Caja Seguros: </label>
											<select class="form-control" id="cmb_cre_retener" name="cmb_cre_retener">
												<option value="">-</option>
												<option value="1">NO</option>
												<option value="2">SI</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row mb-4">
									<div class="col-md-8">
										<div class="form-group">
											<label>
												<?php if ($action == 'ad_regular' || $cre_tip1 == 2) echo 'Finalidad';
												else echo 'Observación' ?>
											</label>
											<textarea class="form-control" id="txt_cre_obs" name="txt_cre_obs" style="resize: vertical;" rows="5"><?php echo $cre_obs; ?></textarea>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Cuenta a Depositar:</label>
											<select class="form-control" id="cmb_cuedep_id" name="cmb_cuedep_id" disabled></select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Comprobante de Desembolso</label>
											<input type="text" class="form-control text-center" id="txt_cre_comdes" name="txt_cre_comdes" value="<?php echo $cre_comdes ?>" maxlength="250" disabled>
										</div>
									</div>
								</div>
								<div class="row mb-4">
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Fecha de Credito:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_feccre" name="txt_cre_feccre" class="form-control fecha" value="<?php echo $cre_feccre ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Fecha de Desembolso:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_fecdes" name="txt_cre_fecdes" class="form-control fecha" value="<?php echo $cre_fecdes ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Fecha de Facturación:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_fecfac" name="txt_cre_fecfac" class="form-control fecha" value="<?php echo $cre_fecfac ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Fecha de Prorrateo:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_fecpro" name="txt_cre_fecpro" class="form-control fecha" value="<?php echo $cre_fecpro ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-5elements">
										<div class="form-group">
											<label>Fecha de Entrega:</label>
											<div class="input-group">
												<input type="text" id="txt_cre_fecent" name="txt_cre_fecent" class="form-control fecha" value="<?php echo $cre_fecent ?>" disabled>
												<div class="input-group-addon">
													<span><i class="fa fa-calendar-day"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<h4 class="text-teal"> HISTORIAL </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
						<div class="row">
							<div class="col-md-6">
								<div class="row mt-4 mb-4">
									<div class="col-md-12">
										<ul>
											<li>
												<h5>Registrado por: <?php echo $usureg . ', el ' . $reg; ?></h5>
											</li>
											<li>
												<h5> <?php if ($mos_usuapr == 1) { echo 'Aprobado por: ' . $usuapr . ', el ' . $apr; } else { echo "Sin aprobar"; } ?> </h5>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row mt-4 mb-4">
									<div class="col-md-12">
										<ul>
											<li>
												<h5>Modificado por: <?php echo $usumod . ', el ' . $mod; ?></h5>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row mt-4">
						<div class="col-md-12">
							<h4 class="text-orange"> IMPRIMIR FORMATOS </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #fada89; border-radius: 8px; background-color: rgba(250, 218, 137, 0.3);">
						<div class="row mt-4 mb-4">
							<div class="col-md-12">
								<!-- VENTA NUEVA-->
								<?php if ($cre_tip1 == 1): ?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_venta_contrato.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Contrato </a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_anexo_01.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Anexo 01 </a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_venta_anexo02.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Anexo 02 </a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_venta_anexo03.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Anexo 03 </a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_venta_anexo04.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Anexo 04 </a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_venta_anexo05.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Anexo 05 </a> &nbsp;
								<?php endif ?>

								<!-- ADENDAS-->
								<?php if ($cre_tip1 == 2): ?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_adenda_contrato.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Contrato </a> &nbsp;
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_anexo_01.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Anexo 01 </a> &nbsp;
								<?php endif ?>
								
								<!-- REVENTA-->
								<?php if ($cre_tip1 == 4): ?>
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_reventa_anexo05.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Anexo 05 </a> &nbsp;
								<?php endif ?>

								<!-- DOCUMENTOS IGUALES PARA TODOS-->
									<a class="btn btn-warning" target="_blank" title="Imprimir" href="<?php echo 'https://ipdnsac.com/app/modulos/creditoasiveh/doc_adenda_retencion.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id; ?>"> <i class="fa fa-print fa-fw"></i> Ad. Retención </a> &nbsp;
									<?php
										if ($dt['tb_credito_est'] == 8 && !empty($dt['tb_credito_file']))
											echo '<a class="btn btn-warning" target="_blank" title="Imprimir" href="'.$dt['tb_credito_file'].'"><i class="fa fa-print fa-fw"></i> Resolución</a> &nbsp;';
										if ($dt['tb_credito_est'] == 8 && !empty($dt['tb_credito_img']))
											echo '<a class="btn btn-warning" target="_blank" title="Imprimir" href="'.$dt['tb_credito_img'].'"><i class="fa fa-print fa-fw"></i> Img Resolución</a> &nbsp;';
										if ($dt['tb_credito_est'] == 4)
											echo '<a class="btn btn-warning" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditoasiveh/doc_adenda_paralizar.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id.'"><i class="fa fa-print fa-fw"></i> Paralización</a> &nbsp;';
										if ($dt['tb_credito_tip2'] == 3)
											echo '<a class="btn btn-warning" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditoasiveh/doc_adenda_repro.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id.'"><i class="fa fa-print fa-fw"></i> Reprogramado</a> &nbsp;';
										if ($dt['tb_credito_tip2'] == 7) {
											echo '<a class="btn btn-warning" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditoasiveh/doc_transferencia.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id.'"><i class="fa fa-print fa-fw"></i> Transferencia</a> &nbsp;
											<a class="btn btn-warning" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditoasiveh/doc_trans_decjurada.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id.'"><i class="fa fa-print fa-fw"></i> Dec. Jurada Transf</a> &nbsp;';
										}
										echo '<a class="btn btn-warning" target="_blank" title="Imprimir" href="https://ipdnsac.com/app/modulos/creditoasiveh/doc_letras.php?d1='.$creditoasiveh_id.'&d2='.$creditoasiveh_id.'"><i class="fa fa-print fa-fw"></i> Letras de Pago</a> &nbsp;';
									?>
								</a>
							</div>
						</div>
					</div>


					<div class="row mt-4">
						<div class="col-md-12">
							<h4 class="text-primary"> ADJUNTOS </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
						<div class="row mt-4 mb-4">
							<div class="col-md-12">
								<!-- <a class="btn btn-primary" onClick="credito_file_form();" style="border-radius: 8px;">Subir Imagen</a> -->
								<div id="div_credito_file_section"></div>
								<div id="div_credito_file_tabla"></div>
							</div>
						</div>
					</div>

					<div class="row mt-4">
						<div class="col-md-12">
							<h4 class="text-teal"> CRONOGRAMA DE CRÉDITO </h4>
						</div>
					</div>

					<div class="container-fluid" style="border: 1px solid #4ccbd4; border-radius: 8px; background-color: rgba(147, 238, 245, 0.3);">
						<div class="row mt-0 mb-0">
							<div class="col-md-12">
								<div id="div_credito_cronograma"></div> <hr>
								<?php if ($cre_tip1 != 3) { echo '<div id="div_cuota_tabla"></div>'; } else { echo '<div id="div_acuerdo_tabla"></div>'; } ?>
							</div>
						</div>
						<!-- <div class="row mt-4 mb-4">
							<div class="col-md-12">
								<div id="div_cuota_tabla"></div>
							</div>
						</div>
						<div class="row mt-4 mb-4">
							<div class="col-md-3">
								<div id="div_acuerdo_tabla"></div>
							</div>
						</div> -->
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					<span aria-hidden="true">Cerrar</span>
				</button>
			</div>
		</div>
	</div>
</div>