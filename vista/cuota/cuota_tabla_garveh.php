<?php
  if(defined('VISTA_URL')){
      require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
      require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../creditogarveh/Creditogarveh.class.php");
$oCreditogarveh = new Creditogarveh();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../tarifario/Tarifario.class.php");
$oTarifario = new Tarifario();


$result = $oTarifario->mostrar_por_fecha_periodo(date('Y-m-d'));
    if($result['estado'] == 1){
        $igv = moneda_mysql($result['data']['tb_tarifario_igv']);
    }
$result = "";

$result1 = $oCreditogarveh->mostrarUno($_POST['cre_id']);
  if($result1['estado'] == 1){
      $subper = $result1['data']['tb_cuotasubperiodo_id']; //sub periodo del credito: 1 mes, 2 quincena, 3 monto_cuo
      $mon_pro = floatval($result1['data']['tb_credito_monpro']); //monto del prorrateo si es que tiene
      $cre_numcuo = intval($result1['data']['tb_credito_numcuomax']);
  }

  $mon_pro = $mon_pro / $cre_numcuo;
  $result1 = "";

if($subper == 1){
  $cols = 1;
  $name_per = "MENSUAL";
}
if($subper == 2){
  $cols = 2;
  $name_per = "QUINCENAL";
}
if($subper == 3){
  $cols = 4;
  $name_per = "SEMANAL";
}

$cretip_id=3; //tipo 3 garveh
$result2=$oCuota->filtrar($cretip_id,$_POST['cre_id']);
$num_rows = count($result2['data']);
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h4>Cronograma de Crédito</h4>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row"> 
            <div class="col-md-12"> 
                
                <table id="cuota-table" class="table table-bordered table-hover">
                  <thead>
                    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                      <th id="filacabecera">N°</th>
                      <th id="filacabecera" colspan="<?php echo $cols?>">FECHAS</th>
                      <th id="filacabecera">CAPITAL</th>
                      <th id="filacabecera">AMORTIZACIÓN</th>
                      <th id="filacabecera">INTERÉS</th>
                      <th id="filacabecera">IGV</th>
                      <th id="filacabecera">PRORRATEO</th>
                      <th id="filacabecera">CUOTA</th>
                      <th id="filacabecera">GPS</th>
                      <th id="filacabecera"><?php echo $name_per?></th>
                    </tr>
                  </thead>
                  <?php
                    if($num_rows>=1){
                  ?> 
                  <tbody>
                    <?php
                        foreach ($result2['data'] as $key => $value) {
                          if($value['tb_moneda_id'] == '1')
                          {
                        $moneda = 'S/. ';
                      }
                      if($value['tb_moneda_id'] == '2')
                          {
                        $moneda = 'US$. ';
                      } 
                    ?>
                    <tr class="filaTabla">
                      <td id="fila"><?php echo $value['tb_cuota_num']?></td>
                      <?php 
                        $cd=0;
                        $result3=$oCuotadetalle->filtrar($value['tb_cuota_id']);
                        $num_rows = count($result3['data']);
                        foreach ($result3['data'] as $key => $value1) {
                          $cd++;
                          $monto_cuo=$value1['tb_cuotadetalle_cuo'];
                          $fecha= mostrar_fecha($value1['tb_cuotadetalle_fec']);

                          if($cd == $cols){
                            $mos_fecha = '';
                            if($value1['tb_cuotadetalle_est'] == 1){
                              $mos_fecha = '<strong>'.$fecha.'</strong>';
                            }
                            elseif ($value1['tb_cuotadetalle_est'] == 2) {
                              $mos_fecha = '<strong style ="color: green;">'.$fecha.'</strong>';
                            }
                            else{
                              $mos_fecha = '<strong style ="color: brown;">'.$fecha.'</strong>';
                            }

                            echo '<td align="center">'.$mos_fecha.'</td>';
                          }
                          else{
                            $mos_fecha = '';
                            if($value1['tb_cuotadetalle_est'] == 1){
                              $mos_fecha = '<span>'.$fecha.'</span>';
                            }
                            elseif ($value1['tb_cuotadetalle_est'] == 2) {
                              $mos_fecha = '<span style ="color: green;">'.$fecha.'</span>';
                            }
                            else{
                              $mos_fecha = '<span style ="color: brown;">'.$fecha.'</span>';
                            }

                            echo '<td align="center">'.$mos_fecha.'</td>';
                          }
                        }
                        $resul3 = "";

                        $interes_igv = moneda_mysql($value['tb_cuota_int']) - (moneda_mysql($value['tb_cuota_int']) / 1.18);
                        $interes_sin_igv = $value['tb_cuota_int'] - $interes_igv;
                      ?>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($value['tb_cuota_cap']);?></td>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($value['tb_cuota_amo']);?></td>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($interes_sin_igv);?></td>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($interes_igv);?></td>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($value['tb_cuota_pro']);?></td>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($value['tb_cuota_cuo']);?></td>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($value['tb_cuota_pregps']);?></td>
                      <td id="fila"><?php echo $moneda . mostrar_moneda($monto_cuo);?></td>
                    </tr>
                    <?php
                    }
                    $result2 = "";
                        ?>
                  </tbody>
                  <?php
                    }
                  ?>
                </table>
                </div>
            </div>
    </div>
</div>
