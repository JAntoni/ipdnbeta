<?php

if (defined('APP_URL')){
require_once(APP_URL . 'datos/conexion.php');}
else{
require_once('../../datos/conexion.php');}

class Cuotadetalle extends Conexion {

    public $cuotadetalle_id;
    public $tb_cuotadetalle_id;
    public $tb_cuotadetalle_reg;
    public $tb_cuotadetalle_xac = 1;
    public $tb_cuota_id;
    public $tb_moneda_id;
    public $tb_cuotadetalle_num;
    public $tb_cuotadetalle_fec;
    public $tb_cuotadetalle_cuo;
    public $tb_cuotadetalle_mor;
    public $tb_cuotadetalle_moraut;
    public $tb_cuotadetalle_not;
    public $tb_cuotadetalle_est;
    public $tb_cuotadetalle_morest;
    public $tb_cuotadetalle_estap;
    public $tb_cuotadetalle_acupag;
    public $tb_cuotadetalle_impapl;
    public $tb_cuotadetalle_per;

    function insertar() {
        $this->tb_cuotadetalle_acupag = 0;
        if ($this->tb_cuotadetalle_est == 10) {
            $this->tb_cuotadetalle_acupag = 1; //indica que esta cuota es un acuerdo de pago
            $this->tb_cuotadetalle_est = 1;
        }
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_cuotadetalle (
                                        tb_cuotadetalle_xac, 
                                        tb_cuota_id, 
                                        tb_moneda_id,
                                        tb_cuotadetalle_num, 
                                        tb_cuotadetalle_fec, 
                                        tb_cuotadetalle_cuo, 
                                        tb_cuotadetalle_est,
                                        tb_cuotadetalle_acupag)
                                VALUES (
                                        :tb_cuotadetalle_xac, 
                                        :tb_cuota_id, 
                                        :tb_moneda_id,
                                        :tb_cuotadetalle_num, 
                                        :tb_cuotadetalle_fec, 
                                        :tb_cuotadetalle_cuo, 
                                        :tb_cuotadetalle_est,
                                        :tb_cuotadetalle_acupag)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuotadetalle_xac", $this->tb_cuotadetalle_xac, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuota_id", $this->tb_cuota_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_num", $this->tb_cuotadetalle_num, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_fec", $this->tb_cuotadetalle_fec, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_cuo", $this->tb_cuotadetalle_cuo, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_est", $this->tb_cuotadetalle_est, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_acupag", $this->tb_cuotadetalle_acupag, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $cuotadetalle_id = $this->dblink->lastInsertId();
            $this->dblink->commit();

            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['cuotadetalle_id'] = $cuotadetalle_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

//    function insertar(){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "INSERT INTO tb_cuotadetalle (
//              tb_cuotadetalle_xac, tb_credito_id, tb_creditotipo_id, 
//              tb_moneda_id, tb_cuotadetalle_num, tb_cuotadetalle_fec, 
//              tb_cuotadetalle_cap, tb_cuotadetalle_amo, tb_cuotadetalle_int, 
//              tb_cuotadetalle_cuo, tb_cuotadetalle_pro, tb_cuotadetalle_persubcuo, 
//              tb_cuotadetalle_est, tb_cuotadetalle_acupag
//            ) 
//            VALUES (
//              :cuotadetalle_xac, :credito_id, :creditotipo_id, 
//              :moneda_id, :cuotadetalle_num, :cuotadetalle_fec, 
//              :cuotadetalle_cap, :cuotadetalle_amo, :cuotadetalle_int, 
//              :cuotadetalle_cuo, :cuotadetalle_pro, :cuotadetalle_persubcuo, 
//              :cuotadetalle_est, :cuotadetalle_acupag
//            );";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":cuotadetalle_xac", $this->cuotadetalle_xac, PDO::PARAM_INT);
//        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
//        $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_INT);
//        $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_num", $this->cuotadetalle_num, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_fec", $this->cuotadetalle_fec, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_cap", $this->cuotadetalle_cap, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_amo", $this->cuotadetalle_amo, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_int", $this->cuotadetalle_int, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_cuo", $this->cuotadetalle_cuo, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_pro", $this->cuotadetalle_pro, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_persubcuo", $this->cuotadetalle_persubcuo, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_est", $this->cuotadetalle_est, PDO::PARAM_INT);
//        $sentencia->bindParam(":cuotadetalle_acupag", $this->cuotadetalle_acupag, PDO::PARAM_INT);
//
//        $result = $sentencia->execute();
//        $cuotadetalle_id = $this->dblink->lastInsertId();
//        $this->dblink->commit();
//
//        $data['estado'] = $result; //si es correcto el ingreso retorna 1
//        $data['cuotadetalle_id'] = $cuotadetalle_id;
//        return $data;
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }

    function eliminar($cuotadetalle_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_cuotadetalle WHERE tb_cuotadetalle_id =:cuotadetalle_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cuotadetalle_id", $cuotadetalle_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    
    function mostrarUno($cuotadetalle_id) {
        try {
            $sql="SELECT * 
                        FROM tb_cuotadetalle cd 
                        INNER JOIN tb_moneda m ON cd.tb_moneda_id=m.tb_moneda_id
                        WHERE tb_cuotadetalle_id=:cuotadetalle_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cuotadetalle_id", $cuotadetalle_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function cuotadetalles_credito($credito_id, $creditotipo_id, $credito_tabla) {
        try {
            $sql = "SELECT * FROM tb_cuotadetalle cuo 
          INNER JOIN $credito_tabla cre on cre.tb_credito_id = cuo.tb_credito_id 
          INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id 
          LEFT JOIN tb_moneda mon on mon.tb_moneda_id = cre.tb_moneda_id
          WHERE cuo.tb_credito_id =:credito_id and cuo.tb_creditotipo_id =:creditotipo_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function modificar_campo($cuotadetalle_id, $cuotadetalle_columna, $cuotadetalle_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($cuotadetalle_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_cuotadetalle SET " . $cuotadetalle_columna . " =:cuotadetalle_valor WHERE tb_cuotadetalle_id =:cuotadetalle_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":cuotadetalle_id", $cuotadetalle_id, PDO::PARAM_INT);
                if ($param_tip == 'INT') {
                    $sentencia->bindParam(":cuotadetalle_valor", $cuotadetalle_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":cuotadetalle_valor", $cuotadetalle_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrar_cuotasdetalle_condicion_estado_por_cuota($condicion, $cuota_id) {
        try {
            $sql = "SELECT * FROM tb_cuotadetalle WHERE tb_cuotadetalle_est IN(:condicion) AND tb_cuota_id =:cuota_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":condicion", $condicion, PDO::PARAM_STR);
            $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_cuotasdetalle_moras($credito_id, $creditotipo_id) {
        try {
            $sql = "SELECT * FROM tb_cuota cu 
            INNER JOIN tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id 
            where tb_credito_id =:credito_id and tb_creditotipo_id =:creditotipo_id and tb_cuotadetalle_mor > 0";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function filtrar($cre_id) {
        try {
            $sql = "SELECT 
                        * 
                FROM 
                        tb_cuotadetalle cd 
                        INNER JOIN tb_moneda m ON cd.tb_moneda_id=m.tb_moneda_id
                WHERE 
                        tb_cuotadetalle_xac=1 AND tb_cuota_id=:cre_id
                        ORDER BY tb_cuotadetalle_id";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "FILTRO DE CUOTA DETALLE";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function verificar_acuerdopago_en_una_fecha($tabla, $cre_id, $fecha) {
        try {
            $sql = "SELECT * FROM $tabla cre
                            inner join tb_cuota cu on cu.tb_credito_id = cre.tb_credito_id
                            inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id
                            where cre.tb_credito_xac =1 and cre.tb_credito_est != 8 and tb_cuotadetalle_fec = :fecha and tb_cuotadetalle_acupag = 1 and tb_cuotadetalle_est != 2";
            if ($tabla == 'tb_creditoasiveh') {
                $sql .= " and cre.tb_credito_tip1 = 3 and cre.tb_credito_acupag =:cre_id limit 1";
            }

            if ($tabla == 'tb_creditogarveh') {
                $sql .= " and cre.tb_credito_tip = 3 and cre.tb_credito_idori =:cre_id limit 1";
            }

            if ($tabla == 'tb_creditohipo') {
                $sql .= " and cre.tb_credito_tip = 3 and cre.tb_credito_idori =:cre_id limit 1";
            }
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se Encontro ningun acuerdo de pago";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /*ANTHONY */
    function mostrar_una_cuotadetalle_por_creditoid($cre_id,$cre_tip){
      try {
        $sql = "SELECT cd.*,c.tb_cuota_int, c.tb_cuota_cap, tb_cuota_amo 
                from tb_cuota c 
                inner join tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id 
                where c.tb_credito_id =:cre_id and c.tb_creditotipo_id =:cre_tip 
                order by tb_cuotadetalle_fec limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function ultima_cuotadetalle_pagada_pagoparcial($cre_id, $cre_tip){
      try {
        $sql = "SELECT * from tb_cuota c 
                inner join tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id 
                where c.tb_credito_id = :cre_id 
                and c.tb_creditotipo_id =:cre_tip 
                and tb_cuotadetalle_est != 1 
                order by tb_cuotadetalle_fec desc limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function  listar_cuotasdetalle_menor_fecha_impagas_pagoparcial($cre_id, $cre_tip, $cond, $fecha){
      try {
        $sql="SELECT * 
                    from tb_cuota c inner join tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id 
                    where c.tb_credito_id = :cre_id 
                    and c.tb_creditotipo_id =:cre_tip 
                    and tb_cuotadetalle_fec $cond '$fecha' and tb_cuotadetalle_est !=2";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id",$cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip",$cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "FILTRO DE CUOTA DETALLE";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function agregar_acuerdopago($cuod_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuotadetalle 
                set tb_cuotadetalle_estap =1 
                where tb_cuotadetalle_id =:cuod_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuod_id",$cuod_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar_xac_por_credito($cre_id,$valor,$cre_tip){
      $this->dblink->beginTransaction();
      try {

          $sql = "UPDATE tb_cuotadetalle set 
                 tb_cuotadetalle_xac =:valor 
                 where tb_cuota_id in 
                 (SELECT cu.tb_cuota_id FROM 
                 tb_creditoasiveh cre 
                 inner join tb_cuota cu on cu.tb_credito_id = cre.tb_credito_id 
                 where cre.tb_credito_id =:cre_id and cu.tb_creditotipo_id =:cre_tip)";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":valor", $valor, PDO::PARAM_INT);
          $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
          $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function  mostrar_cuotadetalle_in($cud_ids){
      try {
        $sql="SELECT * FROM tb_cuotadetalle where tb_cuotadetalle_id IN(:cud_ids) AND tb_cuotadetalle_xac = 1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cud_ids",$cud_ids, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "FILTRO DE CUOTA DETALLE";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function  mostrar_cuotasdetalle_por_creditoid_condicion($cre_id, $cre_tip, $cond, $fecha){
      try {
        $sql="SELECT cd.* 
                from tb_cuota c 
                inner join tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id 
                where c.tb_credito_id = :cre_id and c.tb_creditotipo_id =:cre_tip and tb_cuotadetalle_fec $cond :fecha";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id",$cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip",$cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha",$fecha, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "FILTRO DE CUOTA DETALLE";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_cuotasdetalle_por_creditoid($cre_id,$cre_tip){
      try {
        $sql = "SELECT cd.*,c.tb_cuota_int, c.tb_cuota_cap, tb_cuota_amo 
                from tb_cuota c 
                inner join tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id 
                where c.tb_credito_id = :cre_id and c.tb_creditotipo_id =:cre_tip";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*AANTHONY*/

    function listar_moras_garveh($credito_id, $creditotipo_id) {
      try {
          $sql = "SELECT * 
          FROM tb_cuota cu 
          INNER JOIN tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id 
          INNER JOIN tb_creditogarveh cg on cu.tb_credito_id = cg.tb_credito_id
          where cg.tb_credito_id =:credito_id and tb_creditotipo_id =3 and tb_cuotadetalle_mor > 0";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
          } else {
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
          }

          return $retorno;
      } catch (Exception $e) {
          throw $e;
      }
    }

    function mostrarUno_credito($credito_id, $creditotipo_id, $credito_tabla) {
      try {
          $sql = "SELECT * FROM tb_cuotadetalle cuodet
        INNER JOIN tb_cuota cuo on cuo.tb_cuota_id = cuodet.tb_cuota_id
        INNER JOIN $credito_tabla cre on cre.tb_credito_id = cuo.tb_credito_id 
        INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id 
        LEFT JOIN tb_moneda mon on mon.tb_moneda_id = cre.tb_moneda_id
        WHERE cuo.tb_credito_id =:credito_id and cuo.tb_creditotipo_id =:creditotipo_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
          $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
          } else {
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
          }

          return $retorno;
      } catch (Exception $e) {
          throw $e;
      }
  }

}

?>
