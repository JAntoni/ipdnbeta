<?php
  if(defined('VISTA_URL')){
      require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'cuota/Cuota.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
      require_once('../../core/usuario_sesion.php');
    require_once('../cuota/Cuota.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }
  $oCuota = new Cuota();

  $credito_id = (intval($credito_id) > 0)? $credito_id : 0;
  $creditotipo_id = (intval($creditotipo_id) > 0)? $creditotipo_id : 0;
  $bandera = 0;
  $credito_tabla = retorna_nombre_tabla_creditotipo($creditotipo_id); //nombre de la tabla del crédito /funciones.php

  $result = $oCuota->cuotas_credito($credito_id, $creditotipo_id, $credito_tabla);
  if($result['estado'] == 1)
    $bandera = 1;
?>
<?php if($creditotipo_id == 1 && $credito_id > 0 && $bandera == 1):?>
  <table style="width:100%" class="table table-bordered table-hover">
    <thead>
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">N°</th>
        <th id="tabla_cabecera_fila">FECHA</th>
        <th id="tabla_cabecera_fila">CAPITAL</th>
        <th id="tabla_cabecera_fila">AMORTIZACIÓN</th>
        <th id="tabla_cabecera_fila"> % </th>
        <th id="tabla_cabecera_fila">INTERÉS</th>
        <th id="tabla_cabecera_fila">CUOTA</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        foreach ($result['data'] as $key => $value): ?>
          <tr id="tabla_cabecera_fila">
            <td id="tabla_fila"><?php echo $value['tb_cuota_num'];?></td>
            <td id="tabla_fila"><?php echo mostrar_fecha($value['tb_cuota_fec']);?></td>
            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($value['tb_cuota_cap']);?></td>
            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($value['tb_cuota_amo']);?></td>
            <td id="tabla_fila"><?php echo $value['tb_cuota_interes'].'<b> %</b> ';?></td>
            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($value['tb_cuota_int']);?></td>
            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($value['tb_cuota_cuo']);?></td>
          </tr>
          <?php
        endforeach;
        $result = NULL;
      ?>
    </tbody>
  </table>
<?php endif;?>
<?php if($creditotipo_id > 0 && $creditotipo_id != 1 && $credito_id > 0 && $bandera == 1):?>
  <table style="width:100%" class="table table-bordered table-hover">
    <thead>
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">N°</th>
        <th id="tabla_cabecera_fila">FECHA</th>
        <th id="tabla_cabecera_fila">CAPITAL</th>
        <th id="tabla_cabecera_fila">AMORTIZACIÓN</th>
        <th id="tabla_cabecera_fila">INTERÉS</th>
        <th id="tabla_cabecera_fila">CUOTA</th>
      </tr>
    </thead>
    <tbody>
        <tr id="tabla_cabecera_fila">
        <td id="tabla_fila" ><?php echo $j;?></td>
        <td id="tabla_fila"><?php echo $fecha_facturacion;?></td>
        <td id="tabla_fila"><?php echo $moneda_simbolo . mostrar_moneda($C);?></td>
        <td id="tabla_fila"><?php echo $moneda_simbolo . mostrar_moneda($amo);?></td>
        <td id="tabla_fila"><?php echo $moneda_simbolo . mostrar_moneda($int);?></td>
        <td id="tabla_fila"><?php echo $moneda_simbolo . mostrar_moneda($R);?></td>
      </tr>
    </tbody>
  </table>
<?php endif;?>
<?php if($creditotipo_id == 0 || $credito_id == 0):?>
  <div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
    <h4><i class="icon fa fa-info"></i>Mensaje</h4>
    <?php echo 'Este crédito no tiene un cronograma registrado. El tipo de crédito es: '.$creditotipo_id.', para el Crédito ID: '.$credito_id;?>
  </div>
<?php endif;?>
