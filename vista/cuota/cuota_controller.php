<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cuota/Cuota.class.php');
  $oCuota = new Cuota();
  require_once('../creditomenor/Creditomenor.class.php');
  $oCreditomenor = new Creditomenor();
  require_once ("../funciones/funciones.php");
  require_once ("../funciones/fechas.php");

 	$action = $_POST['cuota_action']; //así debe ir en todos los crditos cuota_action

 	if($action == 'insertar_creditomenor'){
 
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar las Cuotas.';
 		//CUOTAS
    $C = moneda_mysql($_POST['txt_credito_preaco']);
    $i = moneda_mysql($_POST['txt_credito_int']);
    $n = intval($_POST['txt_credito_numcuo']);
    $cuotatipo_id = intval($_POST['cmb_cuotatipo_id']); //2 cuota fija, 1 cuota libre
    if($cuotatipo_id == 1)
      $n = 1; //si es cuota libre solo se ingresa una sola cuota

    $uno = $i / 100;
    $dos = $uno + 1;
    $tres = pow($dos, $n);
    $cuatroA = ($C * $uno) * $tres;
    $cuatroB = $tres - 1;
    $R = $cuatroA / $cuatroB;
    $r_sum = $R*$n;

    $credito_fecdes = $_POST['txt_credito_fecdes'];
    list($day, $month, $year) = explode('-', $credito_fecdes);

    for ($j = 1; $j <= $n; $j++){ 
      if($j > 1){
        $C = $C - $amo;
        $int = $C*($i/100);
        $amo = $R - $int;
        if($cuotatipo_id == 1)
          $amo = 0;
      }
      else{
        $int = $C*($i/100);
        $amo = $R - $int;
        if($cuotatipo_id == 1)
          $amo = 0;
      }
      $month = intval($month + 1);
      if($month == 13){
        $month = 1;
        $year = $year + 1;
      }

      $fecha_facturacion = validar_fecha_facturacion($day, $month, $year);

      $oCuota->credito_id = intval($_POST['hdd_creditomenor_id']);
      $oCuota->creditotipo_id = 1; // credito menor
      $oCuota->moneda_id = $_POST['cmb_moneda_id'];
      $oCuota->cuota_num = $j; 
      $oCuota->cuota_fec = fecha_mysql($fecha_facturacion);
      $oCuota->cuota_cap = $C; 
      $oCuota->cuota_amo = $amo; 
      $oCuota->cuota_int = $int; 
      $oCuota->cuota_cuo = $R; 
      $oCuota->cuota_pro = 0;
      $oCuota->cuota_persubcuo = 1; 
      $oCuota->cuota_est = 1;
      $oCuota->cuota_acupag = 0;
      $oCuota->cuota_interes = $i;

      $oCuota->insertar();
    }
    //activamos al crédito modificando su xac = 1
    $credito_id = intval($_POST['hdd_creditomenor_id']);
    //$oCreditomenor->modificar_campo($credito_id, "tb_credito_xac", 1, "INT"); // ahora el crédito está listo si la encuesta está creada sino no se creará el crédito

    $data['estado'] = 1;
    $data['mensaje'] = 'Se han guardado las cuotas correctamente';

 		echo json_encode($data);
 	}
 	elseif($action == 'eliminar'){
 		$cuota_id = intval($_POST['hdd_cuota_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la cuota.';

 		if($ocuota->eliminar($cuota_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'cuota eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>