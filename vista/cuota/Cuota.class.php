<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}
class Cuota extends Conexion{
    public $cuota_id;

    public $cuota_xac = 1;
    public $credito_id;
    public $creditotipo_id;
    public $moneda_id;
    public $cuota_num; 
    public $cuota_fec; 
    public $cuota_cap; 
    public $cuota_amo; 
    public $cuota_int; 
    public $cuota_cuo; 
    public $cuota_pro;
    public $cuota_persubcuo; 
    public $cuota_est;
    public $cuota_acupag;
    public $cuota_interes;
    public $cuota_preseguro = 0;
    public $cuota_pregps = 0;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cuota (
              tb_cuota_xac, tb_credito_id, tb_creditotipo_id, 
              tb_moneda_id, tb_cuota_num, tb_cuota_fec, 
              tb_cuota_cap, tb_cuota_amo, tb_cuota_int, 
              tb_cuota_cuo, tb_cuota_pro, tb_cuota_persubcuo, 
              tb_cuota_est, tb_cuota_acupag,tb_cuota_interes,
              tb_cuota_preseguro, tb_cuota_pregps) 
            VALUES (
              :cuota_xac, :credito_id, :creditotipo_id, 
              :moneda_id, :cuota_num, :cuota_fec, 
              :cuota_cap, :cuota_amo, :cuota_int, 
              :cuota_cuo, :cuota_pro, :cuota_persubcuo, 
              :cuota_est, :cuota_acupag,:cuota_interes,
              :cuota_preseguro, :cuota_pregps);";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_xac", $this->cuota_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":moneda_id", $this->moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_num", $this->cuota_num, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_fec", $this->cuota_fec, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_cap", $this->cuota_cap, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_amo", $this->cuota_amo, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_int", $this->cuota_int, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_cuo", $this->cuota_cuo, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_pro", $this->cuota_pro, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_persubcuo", $this->cuota_persubcuo, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_est", $this->cuota_est, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_acupag", $this->cuota_acupag, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_interes", $this->cuota_interes, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_preseguro", $this->cuota_preseguro, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_pregps", $this->cuota_pregps, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $cuota_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['cuota_id'] = $cuota_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    /*function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_cuota(
                :cuota_id,
                :cuota_xac, :credito_id, :cuotatipo_id,
                :cuota_can, :cuota_pro, :cuota_val,
                :cuota_valtas, :cuota_ser, :cuota_kil,
                :cuota_pes, :cuota_tas, :cuota_det,
                :cuota_web)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $this->cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_xac", $this->cuota_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuotatipo_id", $this->cuotatipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_can", $this->cuota_can, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_pro", $this->cuota_pro, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_val", $this->cuota_val, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_valtas", $this->cuota_valtas, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_ser", $this->cuota_ser, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_kil", $this->cuota_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_pes", $this->cuota_pes, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_tas", $this->cuota_tas, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_det", $this->cuota_det, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_web", $this->cuota_web, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }*/
    function eliminar($cuota_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuota SET tb_cuota_xac = 0 WHERE tb_cuota_id =:cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function  agregar_nota_cuota($cuo_id,$nota){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuota set tb_cuota_not = CONCAT(:tb_cuota_not,tb_cuota_not) where tb_cuota_id =:tb_cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuota_not", $nota, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($cuota_id){
      try {
          
        $sql="SELECT * 
                    FROM tb_cuota c 
                    INNER JOIN tb_moneda m ON c.tb_moneda_id=m.tb_moneda_id
                    WHERE tb_cuota_id=:cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function informacion_credito_cuota($cre_tip, $cre_id){
      try {
          
                $tabla = 'tb_creditomenor';
            if($cre_tip == 2)
              $tabla = 'tb_creditoasiveh';
            if($cre_tip == 3)
              $tabla = 'tb_creditogarveh';
            if($cre_tip == 4)
              $tabla = 'tb_creditohipo';

            $sql = "SELECT * FROM $tabla WHERE tb_credito_id = :tb_credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function obtener_cuotas_por_credito($cre_tip, $cre_id){
      try {
          
            $sql = "SELECT * FROM tb_cuota where tb_credito_id =:tb_credito_id and tb_creditotipo_id =:tb_creditotipo_id and tb_cuota_xac = 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarUno_CreditoMenor($cuota_id){
      try {
        //$sql = "SELECT * FROM tb_cuota WHERE tb_cuota_id =:cuota_id";
          
          /* se agrego la siguiente linea de codigo al mostrarUno inner join tb_creditomenor tm ON tm.tb_credito_id=c.tb_credito_id
            para la funcion que se encuentra en cuotapago_controller.php y poder modificar solo cuando sea de tipo
           *            */
        $sql="SELECT * 
                    FROM tb_cuota c 
                    INNER JOIN tb_moneda m ON c.tb_moneda_id=m.tb_moneda_id
                    inner join tb_creditomenor tm ON tm.tb_credito_id=c.tb_credito_id
                    WHERE tb_cuota_id=:cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function cuotas_credito($credito_id, $creditotipo_id, $credito_tabla){
      try {
        $sql = "SELECT * FROM tb_cuota cuo 
          INNER JOIN $credito_tabla cre on cre.tb_credito_id = cuo.tb_credito_id 
          INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id 
          LEFT JOIN tb_moneda mon on mon.tb_moneda_id = cre.tb_moneda_id
          WHERE cuo.tb_credito_id =:credito_id and cuo.tb_creditotipo_id =:creditotipo_id and tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function modificar_campo($cuota_id, $cuota_columna, $cuota_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($cuota_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_cuota SET ".$cuota_columna." =:cuota_valor WHERE tb_cuota_id =:cuota_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":cuota_valor", $cuota_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":cuota_valor", $cuota_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          if($cuota_columna == 'tb_cuota_est'){
            //cada vez que se afecte el estado de una cuota pagada, actualizamos el estado de la misma cuota en la tabla COBRANZA
            $this->modificar_campo_cobranza($cuota_id, $cuota_valor);
          }

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    /* GERSON (03-03-23) */

    function modificar_campo_cobranza($cuota_id, $cuota_est) {
      try {
        $sql = "SELECT * FROM tb_cobranza where tb_cobranza_xac = 1 AND tb_cuota_id =:tb_cuota_id ORDER BY tb_cobranza_id DESC LIMIT 0,1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->execute();

        $resultado = NULL;
        if ($sentencia->rowCount() > 0){
          $resultado = $sentencia->fetch(); //resaul['tb:_sas']
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }

        if($resultado != NULL){

          $cobranza_id = $resultado['tb_cobranza_id'];

          $sql = "UPDATE tb_cobranza SET tb_cobranza_estado =:cuota_est WHERE tb_cobranza_id =:cobranza_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cobranza_id", $cobranza_id, PDO::PARAM_INT);
          $sentencia->bindParam(":cuota_est", $cuota_est, PDO::PARAM_INT);

          $result = $sentencia->execute();
          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
    }
    /*  */

    function listar_cuotas_moras($credito_id, $creditotipo_id){
      try {
        $sql = "SELECT * FROM tb_cuota where tb_credito_id =:credito_id and tb_creditotipo_id =:creditotipo_id and tb_cuota_mor > 0 and tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    //listado de moras impagas por cuota
    function listar_cuotas_moras_impagas($credito_id, $creditotipo_id){
      try {
        $sql = "SELECT * FROM tb_cuota where tb_credito_id =:credito_id and tb_creditotipo_id =:creditotipo_id and tb_cuota_mor > 0 and tb_cuota_morest = 1 and tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    //listado de cuotas facturadas impagas
    function listar_cuotas_facturadas_tipocredito($tabla_credito, $creditotipo_id, $cuota_fec1, $cuota_fec2, $credito_est){
      try {
        $sql = "SELECT tb_cuota_id, cre.tb_credito_id, tb_credito_est, tb_cuotatipo_id, tb_cuota_cuo, tb_cuota_int, tb_cuota_est, tb_cuota_mor, tb_cuota_cap  FROM tb_cuota cuo 
        inner join $tabla_credito cre on cre.tb_credito_id = cuo.tb_credito_id 
        where tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_creditotipo_id =:creditotipo_id and tb_cuota_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_fec1", $cuota_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_fec2", $cuota_fec2, PDO::PARAM_STR);
        //$sentencia->bindParam(":credito_est", $credito_est, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    //listado de cuotas impagas menores a una fecha
    function listar_cuotas_impagas_fecha($tabla_credito, $creditotipo_id, $cuota_fec, $credito_est){
      try {
        $sql = "SELECT tb_cuota_id, tb_cuota_fec, tb_cuotatipo_id, tb_cuota_cuo, tb_cuota_int  FROM tb_cuota cuo 
        inner join $tabla_credito cre on cre.tb_credito_id = cuo.tb_credito_id 
        where tb_cuota_xac = 1 and tb_cuota_est in (1,3) and tb_creditotipo_id =:creditotipo_id and tb_cuota_fec < :cuota_fec and tb_credito_est in (:credito_est)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_fec", $cuota_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_est", $credito_est, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function listar_cuotas_pagadas($cre_id, $cre_tip){
      try {
        $sql = "SELECT * FROM tb_cuota where tb_credito_id =:tb_credito_id and tb_creditotipo_id =:tb_creditotipo_id and tb_cuota_est = 2 and tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function listar_ultima_cuota_pagada_pagoparcial_facturada($cre_id, $cre_tip, $fecha){
        
      try {
        $sql = "SELECT * FROM tb_cuota where tb_credito_id =:tb_credito_id and tb_creditotipo_id = :tb_creditotipo_id
                 and tb_cuota_xac = 1 and (tb_cuota_est = 2 OR tb_cuota_est = 3) and tb_cuota_fec <=:tb_cuota_fec  order by tb_cuota_fec desc limit 1";
        //$sql = "SELECT * FROM tb_cuota where tb_credito_id =:tb_credito_id and tb_creditotipo_id = :tb_creditotipo_id
        //and tb_cuota_xac = 1 and (tb_cuota_est = 2 OR tb_cuota_est = 3) and tb_cuota_fec <=:tb_cuota_fec order by tb_cuota_fec desc limit 1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuota_fec", $fecha, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }   
    
    function listar_cuotas_impagas($cre_id, $cre_tip){
      try {
        $sql = "SELECT * FROM tb_cuota where tb_credito_id = :tb_credito_id and tb_creditotipo_id =:tb_creditotipo_id and tb_cuota_est = 1 and tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

  function agregar_amortizacion_cuota_libre($cuo_id, $monto){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuota set tb_cuota_amo = tb_cuota_amo + :cuota_amo where tb_cuota_id =:tb_cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuota_id",$cuo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_amo",$monto, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  function modificar_interes_cuota_libre($cuo_id, $monto){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuota set tb_cuota_int = $monto where tb_cuota_id =:tb_cuota_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuota_id",$cuo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  function modificar_cuota_libre($cuo_id){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_cuota set tb_cuota_cuo =tb_cuota_cap+tb_cuota_int where tb_cuota_id =:tb_cuota_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_cuota_id",$cuo_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

    function filtrar($cretip_id,$cre_id){
        
         try {
            $sql="SELECT * 
                    FROM tb_cuota c 
                    INNER JOIN tb_moneda m ON c.tb_moneda_id=m.tb_moneda_id
                    WHERE tb_cuota_xac=1
                    AND tb_creditotipo_id=:tb_creditotipo_id
                    AND tb_credito_id=:tb_credito_id
                    ORDER BY tb_cuota_id";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_creditotipo_id", $cretip_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    } 

    function NLetrasNuevaCuota($tb_credito_id){
        
         try {
            $sql="SELECT * 
                    FROM tb_cuota c 
                    INNER JOIN tb_moneda m ON c.tb_moneda_id=m.tb_moneda_id
                    inner join tb_creditomenor tm ON tm.tb_credito_id=c.tb_credito_id
                    WHERE  tm.tb_credito_id=:tb_credito_id  AND tb_cuota_xac=1";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $tb_credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    } 
    
     function modificar_Estado_Anulacion($tb_credito_id,$tb_cuota_num){
      $this->dblink->beginTransaction();
      try {

          $sql = "UPDATE tb_cuota set tb_cuota_xac=0 WHERE tb_credito_id=:tb_credito_id AND tb_cuota_num>:tb_cuota_num AND tb_creditotipo_id=1";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_credito_id", $tb_credito_id, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_cuota_num", $tb_cuota_num, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    /* GERSON (31-05-23) */
    function modificar_estado_anulacion_cobranza($tb_credito_id,$cuota_id){
      $this->dblink->beginTransaction();
      try {

          $sql = "UPDATE tb_cobranza set tb_cobranza_estado=1 WHERE tb_credito_id=:tb_credito_id AND tb_cuota_id=:tb_cuota_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_credito_id", $tb_credito_id, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_cuota_id", $cuota_id, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    /*  */
    
    function ReestablecerInteres($interes,$tb_cuota_id){
      $this->dblink->beginTransaction();
      try {

          $sql = "update tb_cuota set 
                                    tb_cuota_int=tb_cuota_cap*(:interes/100),
                                    tb_cuota_cuo=tb_cuota_cap+tb_cuota_int,
                                    tb_cuota_amo=0
		                                WHERE 
                                    tb_cuota_id=:tb_cuota_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":interes", $interes, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_cuota_id", $tb_cuota_id, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function ReestablecerInteresFija($interes,$tb_cuota_id){
      $this->dblink->beginTransaction();
      try {

          $sql = "update tb_cuota set 
                                    tb_cuota_int=tb_cuota_cap*(:interes/100),
                                    tb_cuota_cuo=tb_cuota_amo+tb_cuota_int
		                                WHERE 
                                    tb_cuota_id=:tb_cuota_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":interes", $interes, PDO::PARAM_INT);
          $sentencia->bindParam(":tb_cuota_id", $tb_cuota_id, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar_Estado_Anulacion_fijas($creditotipo_id, $tb_credito_id,$tb_cuota_num,$tb_cuota_cuo,$tipo){
      $this->dblink->beginTransaction();
      
      try {
        if ($tipo == 1) {//si es 1 entonces cambia el estados de las cuotas anteriores
          $sql = "UPDATE tb_cuota SET tb_cuota_xac=1 WHERE tb_creditotipo_id =:credito_tipo AND tb_credito_id=:tb_credito_id AND tb_cuota_cuo=:tb_cuota_cuo AND tb_cuota_num>:tb_cuota_num";
        }
        if ($tipo == 2) {// si es 2 entonces cambia el estado de las cuotas creadas recientemente
          $sql = "UPDATE tb_cuota SET tb_cuota_xac=0 WHERE tb_creditotipo_id =:credito_tipo AND tb_credito_id=:tb_credito_id AND tb_cuota_cuo<>:tb_cuota_cuo AND tb_cuota_num>:tb_cuota_num";
        }

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_tipo", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_id", $tb_credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuota_cuo", $tb_cuota_cuo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuota_num", $tb_cuota_num, PDO::PARAM_INT);
        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function cliente_por_cuotaid($cuo_id){
        
         try {
            $sql = "SELECT tb_cuota_id, tb_creditotipo_id, tb_credito_id FROM tb_cuota where tb_cuota_id =:tb_cuota_id and tb_cuota_xac = 1";
            
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_cuota_id", $cuo_id, PDO::PARAM_INT);
                $sentencia->execute();
                
                if ($sentencia->rowCount() > 0) {
                  $resultado = $sentencia->fetch();
                  $retorno["estado"] = 1;
                  $retorno["mensaje"] = "exito";
                  $retorno["data"] = $resultado;
                  $sentencia->closeCursor(); //para libera memoria de la consulta
                  
                  $cre_tip = intval($resultado['tb_creditotipo_id']);
                  $cre_id = intval($resultado['tb_credito_id']);
                  
                  $credito = 'CM-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
                    $tabla = 'tb_creditomenor';

                    if($cre_tip == 2){
                      $credito = 'CAV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
                      $tabla = 'tb_creditoasiveh';
                    }
                    if($cre_tip == 3){
                      $credito = 'CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
                      $tabla = 'tb_creditogarveh';
                    }
                    if($cre_tip == 4){
                      $credito = 'CH-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
                      $tabla = 'tb_creditohipo';
                    }
                    
                    $sql = "SELECT '$credito' as credito, tb_cliente_nom FROM $tabla cre INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id where tb_credito_id =:tb_credito_id";
                    $sentencia = $this->dblink->prepare($sql);
                    $sentencia->bindParam(":tb_credito_id", $cre_id, PDO::PARAM_INT);
                    $sentencia->execute();
                    if ($sentencia->rowCount() > 0) {
                      $resultado = $sentencia->fetch();
                      $retorno["estado"] = 1;
                      $retorno["mensaje"] = "exito";
                      $retorno["data"] = $resultado;
                    }
                }
                else{
                  $retorno["estado"] = 0;
                  $retorno["mensaje"] = "No hay tipos de crédito registrados";
                  $retorno["data"] = "";
                }

                return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    } 
    
    function ultima_cuota_por_credito($cre_id,$cre_tip){
      try {
        $sql = "SELECT c.*,count(c.tb_cuota_id) as num_subcuo 
                from tb_cuota c 
                inner join tb_cuotadetalle cud on cud.tb_cuota_id = c.tb_cuota_id 
                where tb_credito_id =:cre_id and tb_creditotipo_id =:cre_tip and tb_cuota_xac = 1 
                group by 1 order by c.tb_cuota_fec DESC limit 1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();
        
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function primera_cuota_por_credito($cre_id,$cre_tip){
      try {
        $sql = "SELECT * FROM tb_cuota where tb_credito_id = :cre_id and tb_creditotipo_id = :cre_tip and tb_cuota_xac = 1 order by tb_cuota_id limit 1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();
        
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function listar_cuotas_credito_sin_acuerdo($cre_id, $cre_tip, $num_cuo, $periodo, $fecha){
        
         try {
            $sql="SELECT * 
                  from (SELECT @i := @i+1 as fila,cud.*,cu.tb_cuota_fec 
                        FROM (select @i :=0) T,tb_cuota cu inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id 
                        where cu.tb_creditotipo_id =:cre_tip 
                        and cu.tb_credito_id =:cre_id 
                        and cud.tb_cuotadetalle_est = 1 
                        and tb_cuotadetalle_fec >= :fecha) T 
                  where fila % :periodo = 0 limit :num_cuo";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $sentencia->bindParam(":periodo", $periodo, PDO::PARAM_INT);
        $sentencia->bindParam(":num_cuo", $num_cuo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_xac_por_credito($cre_id,$valor,$cre_tip){
      $this->dblink->beginTransaction();
      try {

          $sql = "UPDATE tb_cuota set tb_cuota_xac =:valor where tb_credito_id =:cre_id and tb_creditotipo_id =:cre_tip";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":valor", $valor, PDO::PARAM_INT);
          $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
          $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function cuotas_impagas_vencidas_cliente($cliente_id){
      try {
        $sql = "SELECT cre.tb_credito_id, tb_cuota_fec FROM tb_cliente cli 
        INNER JOIN tb_creditomenor cre ON cre.tb_cliente_id = cli.tb_cliente_id
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 1)
        WHERE tb_credito_xac = 1 AND tb_credito_est IN(3,5) AND tb_cuota_est IN(1,3) AND tb_cuota_xac = 1 AND tb_cuota_fec < DATE(NOW()) AND cre.tb_cliente_id =:cliente_id";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_cuotas_credito($tabla_credito, $creditotipo_id, $credito_id, $credito_est){
      try {
        $sql = "SELECT cuo.*  FROM tb_cuota cuo 
        inner join $tabla_credito cre on cre.tb_credito_id = cuo.tb_credito_id 
        where tb_cuota_xac = 1 and tb_cuota_est in ($credito_est) 
        and tb_creditotipo_id =:creditotipo_id
        and cre.tb_credito_id = :credito_id;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        $retorno['sql'] = $sql;
        $retorno['cantidad'] = $sentencia->rowCount();
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    //Anthony 25-09-2024
    function listar_moras_menor($credito_id, $creditotipo_id){
      try {
        $sql = "SELECT * 
        FROM tb_cuota c
        INNER JOIN tb_creditomenor cm on c.tb_credito_id = cm.tb_credito_id
        where c.tb_credito_id =:credito_id 
        and c.tb_creditotipo_id = 1
        and c.tb_cuota_mor > 0 
        and c.tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    //juan 10-09-2024
    function listar_cuotas_facturadas_credito_id($tabla_credito, $credito_id, $creditotipo_id, $cuota_fec1, $cuota_fec2, $credito_est){
      try {
        $sql = "SELECT tb_cuota_id, tb_cuota_fec, cre.tb_credito_id, tb_credito_est, tb_cuotatipo_id, tb_cuota_cuo, tb_cuota_int, tb_cuota_est, tb_cuota_mor, tb_cuota_cap  FROM tb_cuota cuo 
        inner join $tabla_credito cre on cre.tb_credito_id = cuo.tb_credito_id 
        where tb_credito_xac = 1 AND cre.tb_credito_id =:credito_id and tb_cuota_xac = 1 and tb_creditotipo_id =:creditotipo_id and tb_cuota_fec between :cuota_fec1 and :cuota_fec2 and tb_credito_est in ($credito_est)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cuota_fec1", $cuota_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":cuota_fec2", $cuota_fec2, PDO::PARAM_STR);
        //$sentencia->bindParam(":credito_est", $credito_est, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
