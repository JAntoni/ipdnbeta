<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class UsuarioPerfil extends Conexion{

    function insertar($usuarioperfil_nom, $usuarioperfil_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_usuarioperfil(tb_usuarioperfil_xac, tb_usuarioperfil_nom, tb_usuarioperfil_des)
          VALUES (1, :usuarioperfil_nom, :usuarioperfil_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuarioperfil_nom", $usuarioperfil_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":usuarioperfil_des", $usuarioperfil_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function modificar($usuarioperfil_id, $usuarioperfil_nom, $usuarioperfil_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_usuarioperfil SET tb_usuarioperfil_nom =:usuarioperfil_nom, tb_usuarioperfil_des =:usuarioperfil_des WHERE tb_usuarioperfil_id =:usuarioperfil_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);
        $sentencia->bindParam(":usuarioperfil_nom", $usuarioperfil_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":usuarioperfil_des", $usuarioperfil_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function eliminar($usuarioperfil_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_usuarioperfil WHERE tb_usuarioperfil_id =:usuarioperfil_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function mostrarUno($usuarioperfil_id){
      try {
        $sql = "SELECT * FROM tb_usuarioperfil WHERE tb_usuarioperfil_id =:usuarioperfil_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuarioperfil_id", $usuarioperfil_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }
    function listar_usuarioperfiles(){
      try {
        $sql = "SELECT * FROM tb_usuarioperfil";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
  }

?>
