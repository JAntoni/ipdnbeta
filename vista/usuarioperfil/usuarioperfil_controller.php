<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../usuarioperfil/UsuarioPerfil.class.php');
  $oUsuarioPerfil = new UsuarioPerfil();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$usuarioperfil_nom = mb_strtoupper($_POST['txt_usuarioperfil_nom'], 'UTF-8');
 		$usuarioperfil_des = $_POST['txt_usuarioperfil_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Usuario Perfil.';
 		if($oUsuarioPerfil->insertar($usuarioperfil_nom, $usuarioperfil_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Perfil registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$usuarioperfil_id = intval($_POST['hdd_usuarioperfil_id']);
 		$usuarioperfil_nom = mb_strtoupper($_POST['txt_usuarioperfil_nom'], 'UTF-8');
 		$usuarioperfil_des = $_POST['txt_usuarioperfil_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Usuario Perfil.';

 		if($oUsuarioPerfil->modificar($usuarioperfil_id, $usuarioperfil_nom, $usuarioperfil_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Perfil modificado correctamente. '.$usuarioperfil_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$usuarioperfil_id = intval($_POST['hdd_usuarioperfil_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Usuario Perfil.';

 		if($oUsuarioPerfil->eliminar($usuarioperfil_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Perfil eliminado correctamente. '.$usuarioperfil_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>