<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../usuarioperfil/UsuarioPerfil.class.php');
  $oUsuarioPerfil = new UsuarioPerfil();
  require_once('../funciones/funciones.php');

  $direc = 'usuarioperfil';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $tb_usuarioperfil_id = $_POST['usuarioperfil_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Usuario Perfil Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Usuario Perfil';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Usuario Perfil';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Usuario Perfil';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en usuarioperfil
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'usuarioperfil'; $modulo_id = $tb_usuarioperfil_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del usuarioperfil por su ID
    if(intval($tb_usuarioperfil_id) > 0){
      $result = $oUsuarioPerfil->mostrarUno($tb_usuarioperfil_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el usuarioperfil seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $usuarioperfil_nom = $result['data']['tb_usuarioperfil_nom'];
          $usuarioperfil_des = $result['data']['tb_usuarioperfil_des'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_usuarioperfil" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_usuarioperfil" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_usuarioperfil_id" value="<?php echo $tb_usuarioperfil_id;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_usuarioperfil_nom" class="control-label">Usuario Perfil</label>
              <input type="text" name="txt_usuarioperfil_nom" id="txt_usuarioperfil_nom" class="form-control input-sm mayus" value="<?php echo $usuarioperfil_nom;?>">
            </div>
            <div class="form-group">
              <label for="txt_usuarioperfil_des" class="control-label">Descripción</label>
              <input type="text" name="txt_usuarioperfil_des" id="txt_usuarioperfil_des" class="form-control input-sm" value="<?php echo $usuarioperfil_des;?>">
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Usuario Perfil?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="usuarioperfil_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_usuarioperfil">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_usuarioperfil">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_usuarioperfil">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/usuarioperfil/usuarioperfil_form.js';?>"></script>
