
$(document).ready(function(){
  $('#form_usuarioperfil').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"usuarioperfil/usuarioperfil_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_usuarioperfil").serialize(),
				beforeSend: function() {
					$('#usuarioperfil_mensaje').show(400);
					$('#btn_guardar_usuarioperfil').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#usuarioperfil_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#usuarioperfil_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		usuarioperfil_tabla();
		      		$('#modal_registro_usuarioperfil').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#usuarioperfil_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#usuarioperfil_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_usuarioperfil').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#usuarioperfil_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#usuarioperfil_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_usuarioperfil_nom: {
				required: true,
				minlength: 2
			},
			txt_usuarioperfil_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_usuarioperfil_nom: {
				required: "Ingrese un nombre para el Usuario Perfil",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_usuarioperfil_des: {
				required: "Ingrese una descripción para el Usuario Perfil",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
