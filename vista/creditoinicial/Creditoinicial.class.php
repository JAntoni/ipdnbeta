<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Creditoinicial extends Conexion{

    function insertar($creditotipo_id, $creditoinicial_tipini, $creditoinicial_idini,$credito_id,$creditoinicial_uniq){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT tb_creditoinicial ( 
			tb_creditotipo_id, tb_creditoinicial_tipini, 
			tb_creditoinicial_idini, tb_credito_id, 
			tb_creditoinicial_uniq
			)
			VALUES (
			 	:creditotipo_id, :creditoinicial_tipini, 
			 	:creditoinicial_idini, :credito_id, 
			 	:creditoinicial_uniq
			);";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditoinicial_tipini", $creditoinicial_tipini, PDO::PARAM_INT);
        $sentencia->bindParam(":creditoinicial_idini", $creditoinicial_idini, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditoinicial_uniq", $creditoinicial_uniq, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function actualizar_credito_uniq($credito_uniq, $credito_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_creditoinicial SET tb_credito_id =:credito_id WHERE tb_creditoinicial_uniq = :credito_uniq";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_uniq", $credito_uniq, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($creditoinicial_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_creditoinicial WHERE tb_creditoinicial_id =:creditoinicial_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditoinicial_id", $creditoinicial_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function resetear_inicial($credito_uniq){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_creditoinicial WHERE tb_creditoinicial_uniq =:tb_creditoinicial_uniq";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_creditoinicial_uniq", $credito_uniq, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($creditoinicial_id){
      try {
        $sql = "SELECT * FROM tb_creditoinicial WHERE tb_creditoinicial_id =:creditoinicial_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":creditoinicial_id", $creditoinicial_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_creditoiniciales($credito_id, $creditotipo_id){
      try {
        $sql = "SELECT * FROM tb_creditoinicial WHERE tb_credito_id =:credito_id AND tb_creditotipo_id =:creditotipo_id ORDER BY tb_creditoinicial_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
