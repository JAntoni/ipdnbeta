<?php
require_once('../../core/usuario_sesion.php');
require_once("Creditoinicial.class.php");
$oCreditoinicial = new Creditoinicial();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

if($_POST['action'] == 'insertar'){
  
  $creditotipo_id = $_POST['creditotipo_id']; 
  $creditoinicial_tipini = $_POST['creditoinicial_tipini']; 
  $creditoinicial_idini = $_POST['creditoinicial_idini']; 
  $credito_id = intval($_POST['credito_id']); 
  $creditoinicial_uniq = $_POST['creditoinicial_uniq'];

  $oCreditoinicial->insertar($creditotipo_id, $creditoinicial_tipini, $creditoinicial_idini, $credito_id, $creditoinicial_uniq);

  $data['estado'] = 1;
  $data['mensaje'] = 'Se registró la Inicial';

  echo json_encode($data);
}

if($_POST['action'] == 'eliminar'){
	$cheque_id = $_POST['cheque_id'];

  $oCreditoinicial->eliminar($cheque_id);

  echo "Registro de depósito eliminado";
}

if($_POST['action'] == 'reset'){
  $credito_uniq = $_POST['credito_uniq'];

  $oCreditoinicial->resetear_inicial($credito_uniq);

  $data['estado'] = 1;
  $data['mensaje'] = 'Iniciales Borrados';

  echo json_encode($data);
}

?>