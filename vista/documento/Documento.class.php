<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');

  
class Documento extends Conexion{
    public $tb_documento_id;
    public $tb_documento_abr;
    public $tb_documento_nom;
    public $tb_documento_tip;
    public $tb_documento_def;
    public $tb_documento_actimp;
    public $tb_impresora_id;
    
    
    
        function listar_Documentos(){
      try {
        $sql = "SELECT * FROM tb_documento";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Documentos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
    
    function Documento_select($documento_tip){
      try {
        $sql = "SELECT * FROM tb_documento where tb_documento_tip=:tb_documento_tip ORDER BY tb_documento_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_documento_tip",$documento_tip, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Documentos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
        } catch (Exception $e) {
          echo 'Error!: '.$e->getMessage();
        }
    }
    
    
}
