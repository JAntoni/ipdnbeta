<?php
require_once 'Documento.class.php';
$oDocumento= new Documento();

$documento_id = (isset($_POST['documento_id']))? trim($_POST['documento_id']) : $documento_id;
$documento_tipo = (isset($_POST['documento_tipo']))? trim($_POST['documento_tipo']) : $documento_tipo;
$vista_documento = (isset($_POST['vista_documento']))? trim($_POST['vista_documento']) : $vista_documento;


if($vista_documento != 'documento'){
	$option = '<option value="0">-</option>';
	//PRIMER NIVEL
	$result = $oDocumento->listar_Documentos();
		if($result['estado'] == 1)
		{
		  foreach ($result['data'] as $key => $value)
		  {
		  	$selected = '';
		  	if($documento_id == $value['tb_documento_id'])
		  		$selected = 'selected';

		    $option .= '<option value="'.$value['tb_documento_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_documento_abr'].' - '.$value['tb_documento_nom'].'</option>';
		  }
		}
	$result = NULL;
	//FIN PRIMER NIVEL
	echo $option;
}
else{
	$option = '<option value="0">-</option>';
	//PRIMER NIVEL
	$result = $oDocumento->Documento_select($documento_tipo);
		if($result['estado'] == 1)
		{
		  foreach ($result['data'] as $key => $value)
		  {
		  	$selected = '';
		  	if($documento_id == $value['tb_documento_id'])
		  		$selected = 'selected';

		    $option .= '<option value="'.$value['tb_documento_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_documento_abr'].' - '.$value['tb_documento_nom'].'</option>';
		  }
		}
	$result = NULL;
	//FIN PRIMER NIVEL
	echo $option;
}
?>
