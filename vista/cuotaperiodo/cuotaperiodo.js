function listar_usuarios(){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"/usuario/usuario_tabla.php",
		async: true,
		dataType: "html",
		data: ({as: 1}),
		beforeSend: function() {

		},
		success: function(data){
			console.log(data);
		},
		complete: function(data){
			//console.log(data);
			//if(data.statusText != "success")
				//console.log(data.responseText);
		},
		error: function(data){
			console.log(data.responseText);
		}
	});
}
function cuotaperiodo_form(usuario_act, cuotaperiodo_id){
  console.log('ejecutado..');
  $.ajax({
		type: "POST",
		url: VISTA_URL+"/cuotaperiodo/cuotaperiodo_form.php",
		async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      cuotaperiodo_id: cuotaperiodo_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_cuotaperiodo_form').html(data);
      $('#modal_registro_cuotaperiodo').modal('show');
		},
		complete: function(data){

		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
$(document).ready(function(){
  /*$('#form_creditotipo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"login/iniciar_sesion.php",
				async: true,
				dataType: "json",
				data: $("#form_login").serialize(),
				beforeSend: function() {
					$('#modal_mensaje').modal({
			        show: 'true'
			    });
			    $('#box_modal_mensaje').addClass('box-info').removeClass('box-danger');
					$('#overlay_modal_mensaje').addClass('overlay').html('<i class="fa fa-refresh fa-spin"></i>');
					$('#body_modal_mensaje').html('Por favor espere unos segundos...');
				},
				success: function(data){
					if(parseInt(data.estado) == 200){
						//console.log('usuario iniciado: '+ data.mensaje + ' // '+ data.tipo_usuario);
						if(parseInt(data.tipo_usuario) > 0){
							$('#box_modal_mensaje').addClass('box-success').removeClass('box-info');
							$('#overlay_modal_mensaje').removeClass('overlay').empty();
							$('#body_modal_mensaje').html('Inicio de Sesión Correcto');
							window.location="./principal";
						}
						else{
							$('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
							$('#overlay_modal_mensaje').empty();
							$('#body_modal_mensaje').html('El usuario iniciado no tiene tipo de usuario');
						}
					}
					else if(parseInt(data.estado) == 500){
						$('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
						$('#overlay_modal_mensaje').removeClass('overlay').empty();
						$('#body_modal_mensaje').html('No puede iniciar sesión porque: <b>' + data.mensaje + '</b>');
						//console.log('usuario nooooo iniciado: '+ data.mensaje + ' // '+ data.tipo_usuario);
					}
					else{
						$('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
						$('#overlay_modal_mensaje').removeClass('overlay').empty();
						$('#body_modal_mensaje').html('ERRROR!: existe un error al momento de ingresar al sistema // '+ data);
						console.log('ERRROR!: existe un error al momento de ingresar al sistema // '+ data);
					}
				},
				complete: function(data){
					//console.log(data);
					//if(data.statusText != "success")
						//console.log(data.responseText);
				},
				error: function(data){
					$('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
					$('#overlay_modal_mensaje').removeClass('overlay').empty();
					$('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
					console.log(data.responseText);
				}
			});
	  },
	  rules: {
			txt_usuario_usu: {
				required: true,
				minlength: 2
			},
			txt_usuario_pass: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_usuario_usu: {
				required: "Por favor ingresa tu usuario o email",
				minlength: "Tu usuario como mínimo debe debe tener 2 caracteres"
			},
			txt_usuario_pass: {
				required: "Por favor ingresa tu contraseña",
				minlength: "Tu contraseña debe ser mayor a 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});*/
});
