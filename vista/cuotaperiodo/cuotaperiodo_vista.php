<?php
	session_name("ipdnsac");
	session_start();
	if(!isset($_SESSION['usuario_id'])){
		echo 'Terminó la sesión';
		exit();
	}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Tipos de Cuota
			<small>Mantenimiento de Periodo de Cuotas</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Créditos</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit));?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<!-- INCLUIMOS EL LISTADO DE USUARIOS-->
			<div class="box-body">
	
					<h4 class="box-title">Nuevo Tipo <button class="btn btn-primary btn-sm" onclick="cuotaperiodo_form('I',0)"><i class="fa fa-plus"></i>&nbsp Agregar  </button></h4>
				
			
					<div id="div_tbl_cuotaperiodos" class="box-body table-responsive">
						<?php require_once('cuotaperiodo_tabla.php');?>
					</div>
				
			</div>
			<div id="div_modal_cuotaperiodo_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
