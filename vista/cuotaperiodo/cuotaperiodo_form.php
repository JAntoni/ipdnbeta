<?php
  session_name('ipdnsac');
  session_start();

  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();

  $direc = 'cuotaperiodo';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cuotaperiodo_id = $_POST['cuotaperiodo_id'];

  $titulo = '';
  if($action == 'I')
    $titulo = 'Registrar Periodo de Cuota';
  elseif($action == 'M')
    $titulo = 'Editar Periodo de Cuota';
  else
    $titulo = 'Periodo de Cuota Registrado';

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cuotaperiodo
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }
    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($action, $array_permisos))
      $bandera = 1;
    elseif(!in_array($action, $array_permisos) && $action == 'M'){
      $mensaje = 'QUIERES EDITAR PERO NO TIENES EL PERMISO, VERIFICAMOS SI TE DIERON UN PERMISO';
      $bandera = 2;
    }
    elseif(!in_array($action, $array_permisos) && $action == 'I'){
      $mensaje = 'QUIERES INSERTAR PERO NO TIENES EL PERMISO, SOLICITA AL ADMIN';
      $bandera = 3;
    }
    elseif(!in_array($action, $array_permisos) && $action == 'L'){
      $mensaje = 'QUIERES LEER PERO NO TIENES EL PERMISO, SOLICITA AL ADMIN';
      $bandera = 3;
    }
    else{
      $mensaje = 'EL TIPO DE ACCION ES DESCONOCIDO: '.$action;
      $bandera = 4;
    }
  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cuotaperiodo" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cuotaperiodo" method="post">
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_cuotaperiodo_nom" class="control-label">Periodo de Cuota</label>
              <input type="text" name="txt_cuotaperiodo_nom" id="txt_cuotaperiodo_nom" class="form-control input-sm">
            </div>
          <div class="form-group">
              <label for="txt_cuotaperiodo_des" class="control-label">Rando de Días</label>
              <input type="text" name="txt_cuotaperiodo_des" id="txt_cuotaperiodo_des" class="form-control input-sm">
            </div>
            <div class="form-group">
              <label for="txt_cuotaperiodo_des" class="control-label">Descripción</label>
              <input type="text" name="txt_cuotaperiodo_des" id="txt_cuotaperiodo_des" class="form-control input-sm">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <?php if($action == 'I' || $action == 'M'): ?>
              <button type="submit" class="btn btn-info pull-right">Guardar</button>
            <?php endif; ?>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 2): ?>
  <div class="modal modal-warning" tabindex="-1" role="dialog" id="modal_registro_cuotaperiodo">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Sin Permiso de Editar</h4>
        </div>
        <form id="form_cuotaperiodo" method="post">
          <div class="modal-body">
            <div class="box-body">
              <p><?php echo $mensaje;?></p>
              <div class="form-group">
                <label for="txt_cuotaperiodo_des" class="control-label">Solicitar:</label>
                <input type="text" name="txt_cuotaperiodo_des" id="txt_cuotaperiodo_des" class="form-control input-sm">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 3): ?>
  <div class="modal modal-warning" tabindex="-1" role="dialog" id="modal_registro_cuotaperiodo" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Sin Permiso de Registro o Lectura</h4>
        </div>
        <form id="form_cuotaperiodo" method="post">
          <div class="modal-body">
            <div class="box-body">
              <p><?php echo $mensaje;?></p>
              <div class="form-group">
                <label for="txt_cuotaperiodo_des" class="control-label">Solicitar:</label>
                <input type="text" name="txt_cuotaperiodo_des" id="txt_cuotaperiodo_des" class="form-control input-sm">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cuotaperiodo">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Acceso Denegado</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
