<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Cuotaperiodo.class.php');
  $oCuotaperiodo = new Cuotaperiodo();

  $result = $oCuotaperiodo->listar_cuotaperiodos();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_cuotaperiodo_id'].'</td>
          <td>'.utf8_encode($value['tb_cuotaperiodo_nom']).'</td>
          <td>'.$value['tb_cuotaperiodo_randia'].'</td>
          <td>'.$value['tb_cuotaperiodo_des'].'</td>
          <td align="center">
            <div class="btn-group">
              <a class="btn btn-info btn-xs" title="Ver" onclick="cuotaperiodo_form(\'L\','.$value['tb_cutoaperiodo_id'].')"><i class="fa fa-eye"></i></a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="cuotaperiodo_form(\'M\','.$value['tb_cuotaperido_id'].')"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash"></i></a>
              <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
            </div>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_cuotatipos" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Rango de días</th>
      <th>Descripción</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
