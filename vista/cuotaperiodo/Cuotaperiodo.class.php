<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Cuotaperiodo extends Conexion{

    function insertar($cuoper_nom, $cuoper_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_creditotipo(tb_creditotipo_xac, tb_creditotipo_nom, tb_creditotipo_des)
          VALUES (1, :cretip_nom, :cretip_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cretip_nom", $usu_id, PDO::PARAM_STR);
        $sentencia->bindParam(":cretip_des", $navega, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function listar_cuotaperiodos(){
      try {
        $sql = "SELECT * FROM tb_cuotaperiodo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay periodo de cuotas registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
