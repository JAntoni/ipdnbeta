<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../vehiculotipo/Vehiculotipo.class.php');
  $oVehiculotipo = new Vehiculotipo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$vehiculotipo_nom = mb_strtoupper($_POST['txt_vehiculotipo_nom'], 'UTF-8');
 		$vehiculotipo_des = $_POST['txt_vehiculotipo_des'];
 		$vehiculoclase_id = $_POST['cmb_vehiculoclase_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Tipo de Vehículo.';
                $result = $oVehiculotipo->insertar($vehiculotipo_nom, $vehiculotipo_des, $vehiculoclase_id);
 		if($result['estado']){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Vehículo registrado correctamente.';
                        $data['tipo_id'] = $result['vehiculotipo_id'];
                        $data['clase_id'] = $result['vehiculoclase_id'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$vehiculotipo_id = intval($_POST['hdd_vehiculotipo_id']);
 		$vehiculotipo_nom = mb_strtoupper($_POST['txt_vehiculotipo_nom'], 'UTF-8');
 		$vehiculotipo_des = $_POST['txt_vehiculotipo_des'];
 		$vehiculoclase_id = $_POST['cmb_vehiculoclase_id'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Tipo de Vehículo.';

 		if($oVehiculotipo->modificar($vehiculotipo_id, $vehiculotipo_nom, $vehiculotipo_des, $vehiculoclase_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Vehículo modificado correctamente. ';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$vehiculotipo_id = intval($_POST['hdd_vehiculotipo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Tipo de Vehículo.';

 		if($oVehiculotipo->eliminar($vehiculotipo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Tipo de Vehículo eliminado correctamente. '.$vehiculotipo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>