
$(document).ready(function(){
  $('#form_vehiculotipo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"vehiculotipo/vehiculotipo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_vehiculotipo").serialize(),
				beforeSend: function() {
					$('#vehiculotipo_mensaje').show(400);
					$('#btn_guardar_vehiculotipo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#vehiculotipo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#vehiculotipo_mensaje').html(data.mensaje);
                        $('#modal_registro_vehiculotipo').modal('hide');
                        var modulo = $("#hdd_modulo").val();
                        if(modulo = 'creditogarveh'){
                            cargar_vehiculoclase(data.clase_id);
                            cargar_vehiculotipo(data.tipo_id);
                        }
                        if(modulo != 'creditogarveh'){
		      	setTimeout(function(){ 
		      		vehiculotipo_tabla();
		      		 }, 1000
		      	);
                        }
					}
					else{
		      	$('#vehiculotipo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#vehiculotipo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_vehiculotipo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#vehiculotipo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#vehiculotipo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_vehiculotipo_nom: {
				required: true,
				minlength: 1
			},
			txt_vehiculotipo_des: {
				required: true,
				minlength: 1
			},
			cmb_vehiculoclase_id: {
				required: true,
				min: 1
			}
		},
		messages: {
			txt_vehiculotipo_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_vehiculotipo_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			},
			cmb_vehiculoclase_id: {
				required: "Seleccione una vehiculoclase",
				min: "Elija una vehiculoclase por favor"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
