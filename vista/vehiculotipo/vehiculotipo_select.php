<?php
require_once('Vehiculotipo.class.php');
$oVehiculotipo = new Vehiculotipo();

$vehiculoclase_id = (empty($vehiculoclase_id))? intval($_POST['vehiculoclase_id']) : $vehiculoclase_id;
$vehiculotipo_id = (empty($vehiculotipo_id))? $_POST['vehiculotipo_id'] : $vehiculotipo_id; //

$option = '<option value="0">Seleccione...</option>';

//PRIMER NIVEL
$result = $oVehiculotipo->listar_vehiculotipos($vehiculoclase_id);
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($vehiculotipo_id == $value['tb_vehiculotipo_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_vehiculotipo_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_vehiculotipo_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>