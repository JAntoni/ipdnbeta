<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Vehiculotipo extends Conexion{

    function insertar($vehiculotipo_nom, $vehiculotipo_des, $vehiculoclase_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_vehiculotipo(tb_vehiculotipo_xac, tb_vehiculotipo_nom, tb_vehiculotipo_des, tb_vehiculoclase_id)
          VALUES (1, :vehiculotipo_nom, :vehiculotipo_des, :vehiculoclase_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculotipo_nom", $vehiculotipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculotipo_des", $vehiculotipo_des, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoclase_id", $vehiculoclase_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $vehiculotipo_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result;
        $data['vehiculotipo_id'] = $vehiculotipo_id;
        $data['vehiculoclase_id'] = $vehiculoclase_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($vehiculotipo_id, $vehiculotipo_nom, $vehiculotipo_des, $vehiculoclase_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_vehiculotipo SET tb_vehiculotipo_nom =:vehiculotipo_nom, tb_vehiculotipo_des =:vehiculotipo_des, tb_vehiculoclase_id =:vehiculoclase_id WHERE tb_vehiculotipo_id =:vehiculotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculotipo_id", $vehiculotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculotipo_nom", $vehiculotipo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculotipo_des", $vehiculotipo_des, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoclase_id", $vehiculoclase_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($vehiculotipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_vehiculotipo WHERE tb_vehiculotipo_id =:vehiculotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculotipo_id", $vehiculotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($vehiculotipo_id){
      try {
        $sql = "SELECT * FROM tb_vehiculotipo vehiculotipo INNER JOIN tb_vehiculoclase vehiculoclase ON vehiculotipo.tb_vehiculoclase_id = vehiculoclase.tb_vehiculoclase_id WHERE tb_vehiculotipo_id =:vehiculotipo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculotipo_id", $vehiculotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculotipos($vehiculoclase_id){
        $concatenar = '';
        if(intval($vehiculoclase_id)>0){
            $concatenar = ' WHERE vt.tb_vehiculoclase_id =:vehiculoclase_id';
        }
      try {
        $sql = "SELECT * FROM tb_vehiculotipo vt 
                INNER JOIN tb_vehiculoclase vc ON vt.tb_vehiculoclase_id = vc.tb_vehiculoclase_id".$concatenar;

        $sentencia = $this->dblink->prepare($sql);
        if(intval($vehiculoclase_id)>0){
            $sentencia->bindParam(":vehiculoclase_id", $vehiculoclase_id, PDO::PARAM_INT);
        }
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
