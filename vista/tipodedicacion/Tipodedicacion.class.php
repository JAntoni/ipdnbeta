<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Tipodedicacion extends Conexion{

    function insertar($tipodedicacion_nom, $tipodedicacion_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tipodedicacion(tb_tipodedicacion_nom, tb_tipodedicacion_des)
          VALUES (:tipodedicacion_nom, :tipodedicacion_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tipodedicacion_nom", $tipodedicacion_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tipodedicacion_des", $tipodedicacion_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($tipodedicacion_id, $tipodedicacion_nom, $tipodedicacion_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_tipodedicacion SET tb_tipodedicacion_nom =:tipodedicacion_nom, tb_tipodedicacion_des =:tipodedicacion_des WHERE tb_tipodedicacion_id =:tipodedicacion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tipodedicacion_id", $tipodedicacion_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tipodedicacion_nom", $tipodedicacion_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tipodedicacion_des", $tipodedicacion_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($tipodedicacion_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_tipodedicacion SET tb_tipodedicacion_xac = 0  WHERE tb_tipodedicacion_id =:tipodedicacion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tipodedicacion_id", $tipodedicacion_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($tipodedicacion_id){
      try {
        $sql = "SELECT * FROM tb_tipodedicacion WHERE tb_tipodedicacion_id =:tipodedicacion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tipodedicacion_id", $tipodedicacion_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_tipodedicaciones(){
      try {
        $sql = "SELECT * FROM tb_tipodedicacion";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "Aún no hay motivos de préstamos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
