<?php
 	require_once('../tipodedicacion/Tipodedicacion.class.php');
  $oTipodedicacion = new Tipodedicacion();

 	$action = $_POST['action'];

	$tipodedicacion_nom = mb_strtoupper($_POST['txt_tipodedicacion_nom'], 'UTF-8');
	$tipodedicacion_des = mb_strtoupper($_POST['txt_tipodedicacion_nom'], 'UTF-8'); // cambiar si el detalle sea otro texto

 	if($action == 'insertar'){
 	
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Motivo del Préstamo.';
 		if($oTipodedicacion->insertar($tipodedicacion_nom, $tipodedicacion_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Motivo del Préstamo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$tipodedicacion_id = intval($_POST['hdd_tipodedicacion_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Motivo del Préstamo.';

 		if($oTipodedicacion->modificar($tipodedicacion_id, $tipodedicacion_nom, $tipodedicacion_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Motivo del Préstamo modificado correctamente. '.$tipodedicacion_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$tipodedicacion_id = intval($_POST['hdd_tipodedicacion_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Motivo del Préstamo.';

 		if($oTipodedicacion->eliminar($tipodedicacion_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Motivo del Préstamo eliminado correctamente. '.$tipodedicacion_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>