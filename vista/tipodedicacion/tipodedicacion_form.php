<?php
  require_once ('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../tipodedicacion/Tipodedicacion.class.php');
  $oTipodedicacion = new Tipodedicacion();
  require_once('../funciones/funciones.php');

  $direc = 'tipodedicacion';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $tipodedicacion_id = $_POST['tipodedicacion_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Motivo del Préstamo Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Registrar Motivo del Préstamo';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Motivo del Préstamo';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Motivo del Préstamo';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en tipodedicacion
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'tipodedicacion'; $modulo_id = $tipodedicacion_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del tipodedicacion por su ID
    if(intval($tipodedicacion_id) > 0){
      $result = $oTipodedicacion->mostrarUno($tipodedicacion_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el tipodedicacion seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $tipodedicacion_nom = $result['data']['tb_tipodedicacion_nom'];
          $tipodedicacion_des = $result['data']['tb_tipodedicacion_des'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_tipodedicacion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_tipodedicacion" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_tipodedicacion_id" value="<?php echo $tipodedicacion_id;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_tipodedicacion_nom" class="control-label">Motivo del Préstamo</label>
              <input type="text" name="txt_tipodedicacion_nom" id="txt_tipodedicacion_nom" class="form-control input-sm mayus" value="<?php echo $tipodedicacion_nom;?>">
            </div>
            <div class="form-group">
              <label for="txt_tipodedicacion_des" class="control-label">Descripción</label>
              <input type="text" name="txt_tipodedicacion_des" id="txt_tipodedicacion_des" class="form-control input-sm" value="<?php echo $tipodedicacion_des;?>">
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Motivo del Préstamo?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="tipodedicacion_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_tipodedicacion">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_tipodedicacion">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_tipodedicacion">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/tipodedicacion/tipodedicacion_form.js';?>"></script>
