<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
    require_once(VISTA_URL.'tipodedicacion/Tipodedicacion.class.php');
  } 
  else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
    require_once('../tipodedicacion/Tipodedicacion.class.php');
  }
  
  $oTipodedicacion = new Tipodedicacion();

  $result = $oTipodedicacion->listar_tipodedicaciones();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_tipodedicacion_id'].'</td>
          <td>'.$value['tb_tipodedicacion_nom'].'</td>
          <td>'.$value['tb_tipodedicacion_des'].'</td>
          <td align="center">
            
              <a class="btn btn-info btn-xs" title="Ver" onclick="tipodedicacion_form(\'L\','.$value['tb_tipodedicacion_id'].')"><i class="fa fa-eye"></i></a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="tipodedicacion_form(\'M\','.$value['tb_tipodedicacion_id'].')"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="tipodedicacion_form(\'E\','.$value['tb_tipodedicacion_id'].')"><i class="fa fa-trash"></i></a>
              <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
            
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    $tr = '<tr><td>'.$result['mensaje'].'</td></tr>';
    $result = null;
  }

?>
<table id="tbl_tipodedicacions" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
