<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Upload extends Conexion{
    public $upload_id;

    public $modulo_nom;
    public $modulo_id;
    public $upload_uniq;
    public $upload_url = '...';

    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    /* GERSON (21-07-23) */
    public $usuario_id; //extensiones de las imágenes
    public $comentario_id; //comentario al cual está enlazado
    /*  */
    public $incluye_xac = 1;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO upload(modulo_nom, modulo_id, upload_url, upload_uniq, tb_usuario_id) 
                VALUES(:modulo_nom, :modulo_id, :upload_url, :upload_uniq, :usuario_id)";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $this->modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":upload_url", $this->upload_url, PDO::PARAM_STR);
        $sentencia->bindParam(":upload_uniq", $this->upload_uniq, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);

        $result1 = $sentencia->execute();
        $this->upload_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $nombre_imagen = $this->modulo_nom.'_'.$this->modulo_id.'_upload_'. $this->upload_id .'.'. $this->fileParts['extension'];
        $upload_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $upload_url)){
          //insertamos las imágenes
          $sql = "UPDATE upload SET upload_url =:upload_url WHERE upload_id =:upload_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":upload_id", $this->upload_id, PDO::PARAM_INT);
          $sentencia->bindParam(":upload_url", $upload_url, PDO::PARAM_STR);
          $sentencia->execute();
        }
        else{
          $this->eliminar($this->upload_id);
        }

        if($result1 == 1)
          return 'exito';
        else
          return 'Error al registrar la imagen subida';
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function insertar_doc(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO upload(modulo_nom, modulo_id, upload_url, upload_uniq, tb_usuario_id) 
                VALUES(:modulo_nom, :modulo_id, :upload_url, :upload_uniq, :usuario_id)";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $this->modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":upload_url", $this->upload_url, PDO::PARAM_STR);
        $sentencia->bindParam(":upload_uniq", $this->upload_uniq, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);

        $result1 = $sentencia->execute();
        $this->upload_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $nombre_imagen = $this->modulo_nom.'_'.$this->modulo_id.'_upload_'. $this->upload_id .'.'. $this->fileParts['extension'];
        $upload_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $upload_url)){
          //insertamos las imágenes
          $sql = "UPDATE upload SET upload_url =:upload_url WHERE upload_id =:upload_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":upload_id", $this->upload_id, PDO::PARAM_INT);
          $sentencia->bindParam(":upload_url", $upload_url, PDO::PARAM_STR);
          $sentencia->execute();
        }
        else{
          $this->eliminar($this->upload_id);
        }
        
        if($result1 == 1){
          $data['estado'] = 1;
          $data['mensaje'] = 'exito';
          $data['url'] = $upload_url;
          $data['nuevo'] = $this->upload_id;
          return $data;
        }
        else{
          $data['estado'] = 0;
          $data['mensaje'] = 'Error al guardar el documento, revisar el caso con sistemas';
          $data['url'] = $upload_url;
          return $data;
        }
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    /* GERSON (21-07-23) */
    function insertarProcesoComentario(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO upload(modulo_nom, modulo_id, upload_url, upload_uniq, tb_usuario_id, tb_comentario_id) 
                VALUES(:modulo_nom, :modulo_id, :upload_url, :upload_uniq, :usuario_id, :comentario_id)";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $this->modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":upload_url", $this->upload_url, PDO::PARAM_STR);
        $sentencia->bindParam(":upload_uniq", $this->upload_uniq, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":comentario_id", $this->comentario_id, PDO::PARAM_INT);

        $result1 = $sentencia->execute();
        $this->upload_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $nombre_imagen = $this->modulo_nom.'_'.$this->modulo_id.'_upload_'. $this->upload_id .'.'. $this->fileParts['extension'];
        $upload_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $upload_url)){
          //insertamos las imágenes
          $sql = "UPDATE upload SET upload_url =:upload_url WHERE upload_id =:upload_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":upload_id", $this->upload_id, PDO::PARAM_INT);
          $sentencia->bindParam(":upload_url", $upload_url, PDO::PARAM_STR);
          $sentencia->execute();
        }
        else{
          $this->eliminar($this->upload_id);
        }

        if($result1 == 1)
          return 'exito';
        else
          return 'Error al registrar la imagen subida';
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    /*  */

    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_upload(
                :upload_id,
                :upload_xac, :credito_id, :uploadtipo_id,
                :upload_can, :upload_pro, :upload_val,
                :upload_valtas, :upload_ser, :upload_kil,
                :upload_pes, :upload_tas, :upload_det,
                :upload_web)";

        $sentencia = $this->dblink->prepare($sql);
        // $sentencia->bindParam(":upload_id", $this->upload_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":upload_xac", $this->upload_xac, PDO::PARAM_INT);
        // $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":uploadtipo_id", $this->uploadtipo_id, PDO::PARAM_INT);
        // $sentencia->bindParam(":upload_can", $this->upload_can, PDO::PARAM_INT);
        // $sentencia->bindParam(":upload_pro", $this->upload_pro, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_val", $this->upload_val, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_valtas", $this->upload_valtas, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_ser", $this->upload_ser, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_kil", $this->upload_kil, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_pes", $this->upload_pes, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_tas", $this->upload_tas, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_det", $this->upload_det, PDO::PARAM_STR);
        // $sentencia->bindParam(":upload_web", $this->upload_web, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_modulo_id_imagen($modulo_id, $upload_uniq){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE upload SET modulo_id =:modulo_id WHERE upload_uniq =:upload_uniq";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":upload_uniq", $upload_uniq, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($upload_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM upload WHERE upload_id =:upload_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":upload_id", $upload_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($upload_id){
      try {
        $sql = "SELECT * FROM upload WHERE upload_id =:upload_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":upload_id", $upload_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_uploads($modulo_nom, $modulo_id){
      try {
        $sql = "SELECT * FROM upload where modulo_nom =:modulo_nom and modulo_id =:modulo_id AND upload_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $modulo_nom, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay imágenes subidas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarUno_modulo($modulo_id,$modulo_nom){
      try {
        $sql_add = '';
        if (!empty($this->incluye_xac)) {
          $sql_add = ' AND upload_xac=1';
        }
        $sql = "SELECT * FROM upload WHERE modulo_id=:modulo_id AND modulo_nom =:modulo_nom $sql_add";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_nom", $modulo_nom, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    public function eliminar_xac($upload_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE upload SET upload_xac = 0 WHERE upload_id =:upload_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":upload_id", $upload_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    public function actualizar_file($mod) {
      $this->dblink->beginTransaction();
      try {
        $nombre_pdf = "{$this->modulo_nom}_{$this->modulo_id}_upload_{$this->upload_id}_$mod.{$this->fileParts['extension']}";
        $new_ruta = $this->directorio . strtolower($nombre_pdf);

        $retorno['estado'] = 0;
        $retorno['mensaje'] = 'No se ha ejecutado el update';

        $sql = "UPDATE upload SET upload_url = :ruta_mod, upload_xac=1  WHERE upload_id = :upload_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ruta_mod", $new_ruta, PDO::PARAM_STR);
        $sentencia->bindParam(":upload_id", $this->upload_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        if ($result == 1) {
          $retorno['mensaje'] = 'SQL OK. Falta mover el archivo';
          
          if (move_uploaded_file($this->tempFile, '../../' . $new_ruta)) {
            $retorno['estado'] = 1;
            $retorno['mensaje'] = 'exito';
            $this->dblink->commit();
          }
        }
        return $retorno; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    /* Daniel odar 22-04-2024 */
    public function modificar_campo($upload_id, $upload_columna, $upload_valor, $param_tip) {
      $this->dblink->beginTransaction();
      try {
        if (!empty($upload_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
          $sql = "UPDATE upload SET " . $upload_columna . " = :upload_valor WHERE upload_id = :upload_id;";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":upload_id", $upload_id, PDO::PARAM_INT);
          if ($param_tip == 'INT')
            $sentencia->bindParam(":upload_valor", $upload_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":upload_valor", $upload_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          if ($result == 1)
            $this->dblink->commit();

          return $result; //si es correcto el egreso retorna 1
        } else
          return 0;
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  }

?>
