
$(document).ready(function () {
    console.log('tus cambios realizados 1112222')
  $('#btn_cerrar_upload').click(function (event) {
    var modulo_nom = $('#hdd_modulo_nom').val();

    if(modulo_nom == 'estadollave')
        upload_lista_docs()
  });
  $('#file_upload').uploadifive({
      'auto': false,
      //'checkScript'      : 'programacion_file_check.php',
      'formData': {
          'action': 'insertar_doc',
          'modulo_nom': $('#hdd_modulo_nom').val(),
          'modulo_id': $('#hdd_modulo_id').val(),
          'upload_uniq': $('#hdd_upload_uniq').val(),
          'usuario_id': $('#hdd_usuario_id').val()
      },
      'queueID': 'queue',
      'uploadScript': VISTA_URL + 'upload/upload_controller.php',
      'queueSizeLimit': 10,
      'uploadLimit': 10,
      'multi': true,
      'buttonText': 'Seleccionar Archivo',
      'height': 20,
      'width': 180,
      'fileSizeLimit': '5MB',
      'fileType': ["image\/gif", "image\/jpeg", "image\/png", "image\/jpg", "pdf"],
      'onUploadComplete': function (file, data) {
          if (data != 'exito') {
              $('#alert_img').show(300);
              $('#alert_img').append('<strong>El archivo: ' + file.name + ', no se pudo subir -> ' + data + '</strong><br>');
          }
      },
      'onError': function (errorType) {
          var message = "Error desconocido.";
          if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
              message = "Tamaño de archivo debe ser menor a 3MB.";
          } else if (errorType == 'FORBIDDEN_FILE_TYPE') {
              message = "Tipo de archivo no válido.";
              alert(message);
          }
          //$(".uploadifive-queue-item.error").hide();
          //
          //$.FormFeedbackError(message);
      },
      'onAddQueueItem': function (file) {
          if($('#hdd_modulo_nom').val()=='proc_proceso_fase_comentario'){

          }
          //console.log('The file ' + file.name + ' was added to the queue!');
          //alert(file.type);
      },
      /*'onCheck'      : function(file, exists) {
       if (exists) {
       alert('El archivo ' + file.name + ' ya existe.');
       }
       }*/
  });

  $('#form_upload_doc').submit(function (event) {
      event.preventDefault();
      $.ajax({
          type: "POST",
          url: VISTA_URL + "upload/upload_controller.php",
          async: true,
          dataType: "json",
          data: $("#form_upload_doc").serialize(),
          beforeSend: function () {
              $('#upload_mensaje').show(400);
              $('#btn_guardar_upload').prop('disabled', true);
          },
          success: function (data) {
              if (parseInt(data.estado) > 0) {
                  $('#upload_mensaje').removeClass('callout-info').addClass('callout-success')
                  $('#upload_mensaje').html(data.mensaje);
                  var upload_id = $('#hdd_upload_id').val();
                  $('#uploadid_' + upload_id).hide(400);
                  $('#modal_registro_upload').modal('hide');
//                                                $(codigo).val();
              } else {
                  $('#upload_mensaje').removeClass('callout-info').addClass('callout-warning')
                  $('#upload_mensaje').html('Alerta: ' + data.mensaje);
                  $('#btn_guardar_upload').prop('disabled', false);
              }
          },
          complete: function (data) {
              console.log(data);
          },
          error: function (data) {
              $('#upload_mensaje').removeClass('callout-info').addClass('callout-danger')
              $('#upload_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
          }
      });
  });

});