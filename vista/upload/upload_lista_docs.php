<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
  require_once(VISTA_URL . 'upload/Upload.class.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../upload/Upload.class.php');
}
$oUpload = new Upload();

$modulo_id = (isset($_POST['modulo_id'])) ? $_POST['modulo_id'] : intval($modulo_id);
$modulo_nom = (isset($_POST['modulo_nom'])) ? $_POST['modulo_nom'] : intval($modulo_nom);
$galeria = '';
$result = $oUpload->listar_uploads($modulo_nom, $modulo_id);
$contador = 1;

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $url = str_replace('public/pdf/'.$modulo_nom.'/', '', $value['upload_url']);
    $galeria .= '
      <tr>
        <td>'.$contador.'</td>
        <td>'.$url.'</td>
        <td>
          <a class="btn btn-success btn-xs" target="_blank" href="'.$value['upload_url'].'">Ver</a>
          <button type="button" class="btn btn-danger btn-xs" onclick="eliminar_upload_doc('.$value['upload_id'].')">Eliminar</button>
        </td>
      </tr>
    ';
    $contador ++;
  }
} else {
  $galeria = '
    <tr>
      <th>No existe documentos para este módulo</th>
    </tr>';
}
$result = NULL;
?>
<input type="hidden" id="hdd_upload_modulo_nom" value="<?php echo $modulo_nom;?>">
<table class="table table-bordered">
  <tbody>
    <tr>
      <th>N°</th>
      <th>Nombre Documento</th>
      <th>Acciones</th>
    </tr>
    <?php echo $galeria; ?>
  </tbody>
</table>

<script type="text/javascript">
  function eliminar_upload_doc(upload_id){
    Swal.fire({
      title: '¿Está seguro que desea eliminar este documento?',
      icon: 'info',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText: "SI",
      showDenyButton: true,
      denyButtonText: "NO",
      confirmButtonColor: '#3b5998',
      denyButtonColor: '#21ba45',
      }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type: "POST",
          url: VISTA_URL+"upload/upload_controller.php",
          async: true,
          dataType: "json",
          data: ({
            action: 'eliminar',
            hdd_upload_id: upload_id
          }),
          beforeSend: function() {
          },
          success: function(data){
            var modulo_nom = $('#hdd_upload_modulo_nom').val();

            if(parseInt(data.estado) > 0){
              notificacion_success('Documento eliminado correctamente')

              if(modulo_nom == 'estadollave')
                upload_lista_docs()
            }
            else
              alerta_error('IMPORTANTE', data.mensaje);
          },
          complete: function(data){
            console.log(data)
          },
          error: function(data){
            alerta_error('IMPORTANTE', 'Error revisa consola');
            console.log(data)
          }
        });

      } else if (result.isDenied) {

      }
    });
  }
</script>