<?php
  require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
  require_once('../proceso/Proceso.class.php');
 	$oProceso = new Proceso();
 	require_once('../upload/Upload.class.php');
  $oUpload = new Upload();
  require_once ("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();

 	$action = $_POST['action'];

 	if($action == 'insertar'){

    if (!empty($_FILES)){
      $modulo_nom = trim($_POST['modulo_nom']);
      $modulo_id = intval($_POST['modulo_id']);
      $upload_uniq = $_POST['upload_uniq']; //id temporal
      //$usuario_id = $_POST['usuario_id']; // NO LLEGA EL VALOR, SE OPTA POR USAR LA SESSION
      $usuario_id = intval($_SESSION['usuario_id']); 

      $directorio = 'public/images/'.$modulo_nom.'/';
      $fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'pdf'); // Allowed file extensions

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetFile = $directorio . $_FILES['Filedata']['name'];
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
        //move_uploaded_file($tempFile, $targetFile); insertamos las imágenes
        $oUpload->modulo_nom = $modulo_nom;
        $oUpload->modulo_id = $modulo_id;
        $oUpload->upload_uniq = $upload_uniq;

        $oUpload->tempFile = $tempFile; //temp servicio de PHP
        $oUpload->directorio = $directorio; //nombre con el que irá guardado en la carpeta
        $oUpload->fileParts = $fileParts; //extensiones de las imágenes
        $oUpload->usuario_id = $usuario_id;

        if($modulo_nom=='proc_proceso_fase_comentario'){
          $res= $oProceso->insertarComentario(intval($_POST['modulo_id']), $_SESSION['usuario_id'], '', ''); // crear el comentario al cual se asociará
          if(intval($res['estado']) == 1){
            $oUpload->usuario_id = $usuario_id; //usuario que sube las imagenes
            $oUpload->comentario_id = $res['comentario_id']; //comentario enlazado
            $return = $oUpload->insertarProcesoComentario();
          }
          
        }else{
          $return = $oUpload->insertar();
        }
        echo trim($return);
      } else {
        // The file type wasn't allowed
        echo 'Tipo de archivo no válido.';
      }
    }
    else{
      echo 'No hay ningún archivo para subir';
    }
 	}

  elseif($action == 'insertar_doc'){

    if (!empty($_FILES)){
      $modulo_nom = trim($_POST['modulo_nom']);
      $modulo_id = intval($_POST['modulo_id']);
      $upload_uniq = $_POST['upload_uniq']; //id temporal
      //$usuario_id = $_POST['usuario_id']; // NO LLEGA EL VALOR, SE OPTA POR USAR LA SESSION
      $usuario_id = $_SESSION['usuario_id'];

      $directorio = 'public/pdf/'.$modulo_nom.'/';
      $fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'pdf'); // Allowed file extensions

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetFile = $directorio . $_FILES['Filedata']['name'];
      $fileParts = pathinfo($_FILES['Filedata']['name']);

      if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
        //move_uploaded_file($tempFile, $targetFile); insertamos las imágenes
        $oUpload->modulo_nom = $modulo_nom;
        $oUpload->modulo_id = $modulo_id;
        $oUpload->upload_uniq = $upload_uniq;

        $oUpload->tempFile = $tempFile; //temp servicio de PHP
        $oUpload->directorio = $directorio; //nombre con el que irá guardado en la carpeta
        $oUpload->fileParts = $fileParts; //extensiones de las imágenes
        $oUpload->usuario_id = $usuario_id;

        $url_doc = '';
        $return = $oUpload->insertar_doc();
          if($return['estado'] == 1){
            $url_doc = $return['url'];
          }
        $return = NULL;

        if($modulo_nom == 'refinanciar'){
          $oCredito->modificar_campo($modulo_id, 'tb_credito_doc_ref', $url_doc, 'STR');
        }

        echo 'exito';

      } else {
        // The file type wasn't allowed
        echo 'Tipo de archivo no válido.';
      }
    }
    else{
      echo 'No hay ningún archivo para subir';
    }
 	}
 	elseif($action == 'modificar'){
    $upload_id = intval($_POST['hdd_upload_id']);
    $oUpload->upload_id = $upload_id;

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Vehículo.';

 		if($oUpload->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'upload modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$upload_id = intval($_POST['hdd_upload_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al upload.';

    $ruta_imagen = '';
    //? VAMOS A OBTENER LA URL DE LA IMAGEN PARA PODER ELIMINARLA POR COMPLETO DEL DIRECTORIO
    $result = $oUpload->mostrarUno($upload_id);
      if($result['estado'] == 1){
        $ruta_imagen = '../../'.$result['data']['upload_url'];
      }
    $result = NULL;
    
    if($ruta_imagen != ''){
      // Verificar si la imagen existe
      if (file_exists($ruta_imagen)) {
        // Intentar eliminar la imagen
        if (unlink($ruta_imagen)) {
          if($oUpload->eliminar($upload_id)){
            $data['estado'] = 1;
            $data['mensaje'] = 'Imagen de garantía eliminado correctamente.';
          }
        }else {
          $data['mensaje'] = 'Existe un error al eliminar el documento, se rechaza la eliminacion de la imagen en el directorio.';
        }
      }else {
        $data['mensaje'] = "La imagen no existe en el directorio: ".$ruta_imagen;
      }
    }

 		echo json_encode($data);
 	}
 	else{
	 	echo 'No se ha identificado ningún tipo de transacción para '.$action;
 	}

?>
