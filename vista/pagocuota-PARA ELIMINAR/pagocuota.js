/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
       
    $( "#txt_fil_cli" ).autocomplete({
          minLength: 1,
          source: function(request, response){
              $.getJSON(
                      VISTA_URL+"cliente/cliente_autocomplete.php",
                      {term: request.term}, //
                      response
              );
          },
          select: function(event, ui){
//            console.log(ui);
              $('#hdd_fil_cli_id').val(ui.item.cliente_id);
              $('#txt_fil_cli').val(ui.item.cliente_nom);
              pagocuota_tabla();
            event.preventDefault();
              $('#txt_fil_cli').focus();
          }
      }); 
});

function pagocuota_tabla(){ 
  $.ajax({
    type: "POST",
    url: VISTA_URL+"pagocuota/pagocuota_tabla.php",
    async:true,
    dataType: "html",                      
    data: $("#pagocuota_filtro").serialize(),
    beforeSend: function() {
      $('#msj_pagocuota_tabla').html("Cargando datos...");
      $('#msj_pagocuota_tabla').show(100);
      $('#div_pagocuota_tabla').addClass("ui-state-disabled");
    },
    success: function(html){
      $('#div_pagocuota_tabla').html(html);
    },
    complete: function(){     
      $('#div_pagocuota_tabla').removeClass("ui-state-disabled");
      $('#msj_pagocuota_tabla').hide(100);
    }
  });    
}





function cuotapago_menor_form(act,idf){

  
  Swal.fire({
    title: 'SELECCIONE EL LUGAR DONDE HACE EL PAGO EL CLIENTE',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-home"></i> Pago en Oficina!',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-bank"></i> Pago en Banco!',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#21ba45',
  }).then((result) => {

    if (result.isConfirmed) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Oficina!',1500);
      cuotapago_menor_oficina_form(act,idf);
    } else if (result.isDenied) {
      swal_success('¡Hecho!', '¡Has elegido pagar en Banco!',1500);
      //cuotapago_banco('asiveh', idf);
    }

  });

}


function cuotapago_menor_oficina_form(act,idf){ 
  $.ajax({
        type: "POST",
        url: VISTA_URL+"pagocuota/pagocuota_menor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            cuo_id: idf,
            vista:  'vencimiento_tabla'
        }),
        beforeSend: function() {
//            $('#h3_modal_title').text('Cargando Formulario');
//            $('#modal_mensaje').modal('show');
        },
        success: function(data){
//          $('#modal_mensaje').modal('hide');
            $('#div_modal_pagocuota_menor_form').html(data);
            $('#modal_registro_pagocuotamenor').modal('show');

            modal_height_auto('modal_registro_pagocuotamenor'); //funcion encontrada en public/js/generales.js
        },
        complete: function(data){
//            console.log(data);
        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
        }
	});
}