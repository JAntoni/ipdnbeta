/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    var pc_nom = $('#hdd_vista_pc_nombre').val();
    var pc_mac = $('#hdd_vista_pc_mac').val();
    $('#hdd_pc_nombre').val(pc_nom);
    $('#hdd_pc_mac').val(pc_mac);
  
  /*CALCULAR LA MORA DEL CREDITO*/
    calcular_mora();
    
    $('.moneda').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999999.00'
	});
        $('.moneda3').autoNumeric({
	aSep: ',',
	aDec: '.',
	//aSign: 'S/. ',
	//pSign: 's',
	vMin: '0.00',
	vMax: '999999999.00'
});

/*FORMATO DE FECHA PARA QUE NO SEA MAYOR A LA ACTUAL*/
$('#ingreso_fil_picker1').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
});


$("#for_cuopag").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"pagocuota/cuotapago_menor_reg.php",
				async:true,
				dataType: "json",
				data: $("#for_cuopag").serialize(),
				beforeSend: function() {
					$('#msj_vencimiento').html("Guardando...");
					$('#msj_vencimiento').show(100);
				},
				success: function(data){	
					if(parseInt(data.cuopag_id) > 0){

						if(confirm('¿Desea imprimir?'))
							cuotapago_imppos_datos(data.cuopag_id);
						
						$('#msj_vencimiento').html(data.cuopag_msj);
//						$("#div_vencimiento_form" ).dialog( "close" );
                                                var vista=$("#hdd_vista").val();

                                                    if(vista=="cmb_ven_id"){
                                                        vista+"."+(data.ven_id);
                                                    }
                                                    if(vista=="vencimiento_tabla"){
                                                        vencimiento_tabla();
                                                    }

					}
					else{
						$('#btn_guar_cuopag').show();
	          $('#msj_menor_form').show(100);
	          $('#msj_menor_form').text(data.cuopag_msj);
	        }
				},
				complete: function(data){
					if(data.statusText != 'success'){
						$('#btn_guar_cuopag').show();
            $('#msj_menor_form').text(data.responseText);
            console.log(data);
          }
				}
			});
		},
		rules: {
			txt_cuopag_fec: {
				required: true,
				dateITA: true
			},
			txt_cuopag_mon:{
				required:true
			}
		},
		messages: {
			txt_cuopag_fec: {
				required: '*',
				dateITA: '?'
			},
			txt_cuopag_mon:{
				required:'*'
			}
		}
	});



});

 

function calcular_mora(){	
    $.ajax({
            type: "POST",
            url: VISTA_URL+"pagocuota/cuotapago_menor_reg.php",
            async:true,
            dataType: "json",                      
            data: ({
                    action: "calcular_mora",
                    fechaPago: $('#txt_cuopag_fec').val(),
                    fecha: $("#hdd_cuo_fec").val(),
                    cre_preaco: $('#hdd_cre_preaco').val()
            }),
            success: function(data){
                    $('#txt_cuopag_mor').val(data.mora);
                    if(parseInt(data.estado) == 0){
                            //no existe tarifario para esas fechas
                            $('#msj_menor_form').html(data.msj);
                            $('#msj_menor_form').show();
                    }
                    //alert(data.mora);
            }, 
            complete: function(data){
            }
    });
}


function comprobar(obj){   
    if (obj.checked){     
       $("#txt_cuopag_mor").attr("readonly",false);
   } 
   else{
        $("#txt_cuopag_mor").attr("readonly",true);
   }     
}

