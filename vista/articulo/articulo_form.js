$(document).ready(function () {
    $('#form_articulo').validate({
        submitHandler: function () {
            var form_serializado = $('#form_articulo').serialize();
            //console.log(form_serializado);return;
            $.ajax({
                type: "POST",
                url: VISTA_URL + "articulo/articulo_controller.php",
                async: true,
                dataType: "json",
                data: form_serializado,
                beforeSend: function () {
                    $('#articulo_mensaje').show(400);
                    $('#btn_guardar_articulo').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        console.log(data.estado + ' // '+ data.mensaje);//return;
                        $('#articulo_mensaje').removeClass('callout-info').addClass('callout-success');
                        $('#articulo_mensaje').html(data.mensaje);
                        
                        setTimeout(function () {
                            $('#modal_registro_articulo').modal('hide');
                            articulogarantia_tabla();
                        }, 3000);
                    } else {
                        $('#articulo_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#articulo_mensaje').html('Alerta: ' + data.mensaje);
                        $('#btn_guardar_articulo').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    //console.log(data);
                },
                error: function (data) {
                    $('#articulo_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#articulo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_articulo_nom: {
                required: true
            }
        },
        messages: {
            txt_articulo_nom: {
                required: "*"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });
});