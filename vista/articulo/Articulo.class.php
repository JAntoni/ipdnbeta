<?php
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Articulo extends Conexion
{
    public $tb_articulo_id;
    public $tb_articulo_nombre;
    public $tb_articulo_fecreg;
    public $tb_articulo_usureg;
    public $tb_articulo_fecmod;
    public $tb_articulo_usumod;
    public $tb_articulo_xac;
    public $tb_creditotipo_id;

    public function listar(){
        try {
            $extra = !empty($this->tb_creditotipo_id) ? "WHERE tb_creditotipo_id = $this->tb_creditotipo_id" : '';
            $sql = "SELECT * FROM tb_articulo
                    $extra
                    ORDER BY tb_articulo_nombre ASC;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }

    function insertar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_articulo (tb_articulo_nombre, tb_articulo_usureg, tb_articulo_usumod)
                    VALUES(:param0, :param1, :param2);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tb_articulo_nombre, PDO::PARAM_STR);
            $sentencia->bindParam(":param1", $this->tb_articulo_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":param2", $this->tb_articulo_usumod, PDO::PARAM_INT);

            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    /* GERSON (17-05-23) */
    function listar_garantias_sin_articulo($filtro){
        try {
            if ($filtro == 1) {
                $and = "AND tb_articulo_id = 0";
            }

            $sql = "SELECT tb_garantia_id, tb_credito_id, tb_garantia_pro,tb_articulo_id
                  FROM tb_garantia 
                  WHERE tb_credito_id > 0 $and
                  AND tb_garantia_xac = 1
                  ORDER BY tb_credito_id desc LIMIT 1000";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            $retorno['sql'] = $sql;
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    /*  */
}
