<?php
session_name("ipdnsac");
session_start();
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../articulo/Articulo.class.php');
$oArticulo = new Articulo();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'articulo';
$usuarioperfil_id = intval($_SESSION['usuarioperfil_id']);
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$articulo_id = intval($_POST['articulo_id']);
$vista = $_POST['vista'];

$titulo = '';
if ($usuario_action == 'L')
    $titulo = 'Articulo Registrado';
if ($usuario_action == 'I')
    $titulo = 'Registrar Articulo';
elseif ($usuario_action == 'M')
    $titulo = 'Editar Articulo';
elseif ($usuario_action == 'E')
    $titulo = 'Eliminar Articulo';
else
    $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_articulo" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo $titulo; ?></h4>
            </div>
            <form id="form_articulo" method="post">
                <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                <input type="hidden" name="hdd_articulo_id" value="<?php echo $articulo_id; ?>">
                <input type="hidden" id="articulo_vista" name="articulo_vista" value="<?php echo $vista; ?>">
                <input type="hidden" id="hdd_articulo_registro" name="hdd_articulo_registro" value='<?php echo json_encode($result); ?>'>

                <div class="modal-body">
                    <?php if ($action == 'eliminar')
                        echo '<label style="color: green; font-size: 12px">- No se podrá eliminar si hay documentos registrados en este periodo.</label><p>' ?>
                    <div class="tab-content">
                        <div class="row form-group">
                            <div class="col-md-10">
                                <label for="txt_articulo_nom">Nombre de Artículo:</label>
                                <input type="text" name="txt_articulo_nom" id="txt_articulo_nom" class="form-control input-sm" value="<?php echo $articulo_nom;?>">
                            </div>
                        </div>
                    </div>
                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($action == 'eliminar') : ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Articulo?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="articulo_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_articulo">Guardar</button>
                        <?php endif; ?>
                        <?php if ($usuario_action == 'E') : ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_articulo">Aceptar</button>
                        <?php endif; ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/articulo/articulo_form.js?ver=17050004'; ?>"></script>