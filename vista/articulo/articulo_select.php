<?php
require_once('Articulo.class.php');
$oArticulo = new Articulo();

if(empty($_POST["articulo_id"]) && empty($articulo_id)){
	$articulo_id = 0;
}
elseif(!empty($articulo_id)){
	$articulo_id = intval($articulo_id);
}
else{
	$articulo_id = intval($_POST["articulo_id"]);
}
if (!empty($creditotipo_id)) {
	$oArticulo->tb_creditotipo_id = intval($creditotipo_id);
}
elseif (!empty($_POST["creditotipo_id"])) {
	$oArticulo->tb_creditotipo_id = intval($_POST["creditotipo_id"]);
}

$option = '<option value="-2" style="font-weight: bold;">SELECCIONE</option>';

if($_SESSION['usuariogrupo_id'] == 2){
	$option .= '<option value="0" style="font-weight: bold;">AÑADIR ARTICULO</option>';
}

//PRIMER NIVEL
$result = $oArticulo->listar();
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($articulo_id == $value['tb_articulo_id'])
			$selected = 'selected';

		$option .= '<option value="' . $value['tb_articulo_id'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_articulo_nombre'] . '</option>';
	}
}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>