<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../articulo/Articulo.class.php');
$oArticulo = new Articulo();

$action = $_POST['action'];
$usuario_id = intval($_SESSION['usuario_id']);
$articulo_nom = trim(strtoupper($_POST['txt_articulo_nom']));
$data['estado'] = 0;

if($action == 'insertar'){
    $oArticulo->tb_articulo_nombre = $articulo_nom;
    $oArticulo->tb_articulo_usureg = $usuario_id;
    $oArticulo->tb_articulo_usumod = $usuario_id;

    $res = $oArticulo->insertar();

    if($res['estado'] == 1){
        $data['estado'] = 1;
    }
    $data['mensaje'] = $res['mensaje'];
}

echo json_encode($data);