<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Galon extends Conexion{

    function insertar($galon_nom, $galon_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_galon(tb_galon_xac, tb_galon_nom, tb_galon_des)
          VALUES (1, :galon_nom, :galon_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":galon_nom", $galon_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":galon_des", $galon_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($galon_id, $galon_nom, $galon_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_galon SET tb_galon_nom =:galon_nom, tb_galon_des =:galon_des WHERE tb_galon_id =:galon_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":galon_id", $galon_id, PDO::PARAM_INT);
        $sentencia->bindParam(":galon_nom", $galon_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":galon_des", $galon_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($galon_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_galon WHERE tb_galon_id =:galon_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":galon_id", $galon_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($galon_id){
      try {
        $sql = "SELECT * FROM tb_galon WHERE tb_galon_id =:galon_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":galon_id", $galon_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_galones(){
      try {
        $sql = "SELECT * FROM tb_galon";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
