<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cochera/Cochera.class.php');
  	$oCochera = new Cochera();

 	$action = $_POST['action'];

	$ubigeo_cod = '';
	if (intval($_POST['cmb_ubigeo_coddis']) > 0){
		$ubigeo_cod = $_POST['cmb_ubigeo_coddep'] . $_POST['cmb_ubigeo_codpro'] . $_POST['cmb_ubigeo_coddis'];
	}
    
 	if($action == 'insertar'){
 		$cochera_nom = mb_strtoupper($_POST['txt_cochera_nom'], 'UTF-8');
 		$cochera_des = $_POST['txt_cochera_des'];
		$cochera_horaten = $_POST['txt_cochera_horaten'];
		$proveedorid = $_POST['cmb_nombreproveedor'];


 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Cochera.';
 		if($oCochera->insertar($cochera_nom, $cochera_des,$ubigeo_cod, $cochera_horaten,$proveedorid)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cochera registrada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$cochera_id = intval($_POST['hdd_cochera_id']);
 		$cochera_nom = mb_strtoupper($_POST['txt_cochera_nom'], 'UTF-8');
 		$cochera_des = $_POST['txt_cochera_des'];
		$cochera_horaten = $_POST['txt_cochera_horaten'];
		$proveedorid = $_POST['cmb_nombreproveedor'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Cochera.';

 		if($oCochera->modificar($cochera_id, $cochera_nom, $cochera_des,$ubigeo_cod, $cochera_horaten,$proveedorid)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cochera modificada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$cochera_id = intval($_POST['hdd_cochera_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Cochera.';

 		if($oCochera->eliminar($cochera_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Cochera eliminada correctamente. '.$cochera_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>
