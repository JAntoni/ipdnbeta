/*Funcion Para seleccionar Provincia*/
function ubigeo_provincia_select(ubigeo_coddep) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_provincia_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep
        }),
        beforeSend: function () {
            $('#cmb_ubigeo_codpro').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_ubigeo_codpro').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}

/*Funcion Para seleccionar Distrito*/
function ubigeo_distrito_select(ubigeo_coddep, ubigeo_codpro) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ubigeo/ubigeo_distrito_select.php",
        async: true,
        dataType: "html",
        data: ({
            ubigeo_coddep: ubigeo_coddep,
            ubigeo_codpro: ubigeo_codpro
        }),
        beforeSend: function () {
            $('#cmb_ubigeo_coddis').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_ubigeo_coddis').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}


$(document).ready(function(){

	$('#cmb_ubigeo_coddep').change(function (event) {
        var codigo_dep = $(this).val();
        if (parseInt(codigo_dep) == 0) {
            $('#cmb_ubigeo_codpro').html('');
            $('#cmb_ubigeo_coddis').html('');
        } else {
            ubigeo_provincia_select(codigo_dep);
            $('#cmb_ubigeo_coddis').html('');
        }
    });

    $('#cmb_ubigeo_codpro').change(function (event) {
        var codigo_dep = $('#cmb_ubigeo_coddep').val();
        var codigo_pro = $(this).val();

        if (parseInt(codigo_pro) == 0)
            $('#cmb_ubigeo_coddis').html('');
        else
            ubigeo_distrito_select(codigo_dep, codigo_pro);
    });

  $('#form_cochera').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"cochera/cochera_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_cochera").serialize(),
				beforeSend: function() {
					$('#cochera_mensaje').show(400);
					$('#btn_guardar_cochera').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#cochera_mensaje').removeClass('callout-info').addClass('callout-success')
						$('#cochera_mensaje').html(data.mensaje);
						setTimeout(function(){ 
							cochera_tabla();
							$('#modal_registro_cochera').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#cochera_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#cochera_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_cochera').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#cochera_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#cochera_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_cochera_nom: {
				required: true,
				minlength: 2
			},
			txt_cochera_des: {
				required: true,
				minlength: 5
			},
			txt_cochera_horaten: {
				required: true,
				minlength: 5
			},
			cmb_ubigeo_coddis: {
                required: function () {
                    if ($('#che_cliente_emp').is(":checked")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
		},
		messages: {
			txt_cochera_nom: {
				required: "Ingrese un nombre para la Cochera",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_cochera_des: {
				required: "Ingrese una dirección para la Cochera",
				minlength: "La dirección debe tener como mínimo 5 caracteres"
			},
			txt_cochera_horaten: {
				required: "Ingrese un horario de atención para el Usuario Grupo",
				minlength: "El horario de atención debe tener como mínimo 5 caracteres"
			},
			cmb_ubigeo_coddis: {
                required: "Elija un distrito para el Cliente"
            }
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
