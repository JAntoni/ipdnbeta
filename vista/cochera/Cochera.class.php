<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Cochera extends Conexion{

    function insertar($cochera_nom, $cochera_des, $ubigeo_cod, $cochera_horaten,$proveedor_id){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_cochera(tb_cochera_xac, tb_cochera_nom, tb_cochera_direc, tb_ubigeo_cod, tb_cochera_horaten,tb_proveedor_id)
                VALUES (1, :cochera_nom, :cochera_des, :ubigeo_cod, :cochera_horaten, :proveedor_id)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cochera_nom", $cochera_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":cochera_des", $cochera_des, PDO::PARAM_STR);
            $sentencia->bindParam(":cochera_horaten", $cochera_horaten, PDO::PARAM_STR);
            $sentencia->bindParam(":ubigeo_cod", $ubigeo_cod, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar($cochera_id, $cochera_nom, $cochera_des, $ubigeo_cod, $cochera_horaten,$proveedor_id){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_cochera SET tb_cochera_nom = :cochera_nom, tb_cochera_direc = :cochera_des, tb_ubigeo_cod = :ubigeo_cod, tb_cochera_horaten = :cochera_horaten, tb_proveedor_id = :proveedor_id WHERE tb_cochera_id = :cochera_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cochera_id", $cochera_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cochera_nom", $cochera_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":cochera_des", $cochera_des, PDO::PARAM_STR);
            $sentencia->bindParam(":cochera_horaten", $cochera_horaten, PDO::PARAM_STR);
            $sentencia->bindParam(":ubigeo_cod", $ubigeo_cod, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            echo 'Error!: '.$e->getMessage();
        }
    }

    function eliminar($cochera_id){
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_cochera WHERE tb_cochera_id = :cochera_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cochera_id", $cochera_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            echo 'Error!: '.$e->getMessage();
        }
    }

    function mostrarUno($cochera_id){
        try {
            $sql = "SELECT * FROM tb_cochera WHERE tb_cochera_id = :cochera_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cochera_id", $cochera_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para liberar memoria de la consulta
            }
            else{
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cocheras registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            echo 'Error!: '.$e->getMessage();
        }
    }

    function listar_cocheras(){
        try {
            $sql = "SELECT * FROM tb_cochera WHERE tb_cochera_xac = 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para liberar memoria de la consulta
            }
            else{
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cocheras registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            echo 'Error!: '.$e->getMessage();
        }
    }

    function NombreCochera($cochera_id){
        try {
            $sql = "SELECT * FROM tb_cochera WHERE tb_cochera_id = :cochera_id";
    
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cochera_id", $cochera_id, PDO::PARAM_INT);
            $sentencia->execute();
    
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para liberar memoria de la consulta
            }
            else{
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cocheras registradas";
                $retorno["data"] = "";
            }
    
            return $retorno;
        } catch (Exception $e) {
            echo 'Error!: '.$e->getMessage();
        }
    }
}


?>
