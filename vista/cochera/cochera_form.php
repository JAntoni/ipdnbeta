<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../cochera/Cochera.class.php');
$oCochera = new Cochera();
require_once('../funciones/funciones.php');

$direc = 'cochera';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$cochera_id = $_POST['cochera_id'];

$titulo = '';
if ($usuario_action == 'L')
  $titulo = 'Cochera Registrada';
if ($usuario_action == 'I')
  $titulo = 'Registrar Cochera';
elseif ($usuario_action == 'M')
  $titulo = 'Editar Cochera';
elseif ($usuario_action == 'E')
  $titulo = 'Eliminar Cochera';
else
  $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cochera
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id'];
    $modulo = 'cochera';
    $modulo_id = $cochera_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $result = NULL;
      echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
      exit();
    }
    $result = NULL;
  }

  //si la accion es modificar, mostramos los datos del cochera por su ID
  if (intval($cochera_id) > 0) {
    $result = $oCochera->mostrarUno($cochera_id);
    if ($result['estado'] != 1) {
      $mensaje =  'No se ha encontrado ningún registro para el cochera seleccionado, inténtelo nuevamente.';
      $bandera = 4;
    } else {
      $cochera_nom = $result['data']['tb_cochera_nom'];
      $cochera_des = $result['data']['tb_cochera_direc'];
      $cochera_horaten = $result['data']['tb_cochera_horaten'];
      $ubigeo_cod = $result['data']['tb_ubigeo_cod'];
      $proveedor_id= $result['data']['tb_proveedor_id'];

      $ubigeo_coddep1 = substr($ubigeo_cod, 0, 2);
      $ubigeo_codpro1 = substr($ubigeo_cod, 2, 2);
      $ubigeo_coddis1 = substr($ubigeo_cod, 4, 2);
    }
    $result = NULL;
  }
} else {
  $mensaje =  $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cochera" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo; ?></h4>
        </div>
        <form id="form_cochera" method="post">
          <input type="hidden" name="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_cochera_id" value="<?php echo $cochera_id; ?>">

          <div class="modal-body">
            <div class="form-group">
              <label for="txt_cochera_nom" class="control-label">Nombre de Cochera</label>
              <input type="text" name="txt_cochera_nom" id="txt_cochera_nom" class="form-control input-sm mayus" value="<?php echo $cochera_nom; ?>">
            </div>
            <div class="form-group">
              <label for="txt_cochera_des" class="control-label">Dirección</label>
              <input type="text" name="txt_cochera_des" id="txt_cochera_des" class="form-control input-sm" value="<?php echo $cochera_des; ?>">
            </div>
            <!--APARTADO DATOS UBIGEO-->
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="cmb_ubigeo_coddep" class="control-label">Departamento</label>
                  <select name="cmb_ubigeo_coddep" id="cmb_ubigeo_coddep" class="form-control input-sm">
                    <?php $ubigeo_coddep = $ubigeo_coddep1; ?>
                    <?php require('../ubigeo/ubigeo_departamento_select.php'); ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="cmb_ubigeo_codpro" class="control-label">Provincia</label>
                  <select name="cmb_ubigeo_codpro" id="cmb_ubigeo_codpro" class="form-control input-sm">
                    <?php $ubigeo_codpro = $ubigeo_codpro1; ?>
                    <?php require('../ubigeo/ubigeo_provincia_select.php'); ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="cmb_ubigeo_coddis" class="control-label">Distrito</label>
                  <select name="cmb_ubigeo_coddis" id="cmb_ubigeo_coddis" class="form-control input-sm">
                    <?php $ubigeo_coddis = $ubigeo_coddis1; ?>
                    <?php require('../ubigeo/ubigeo_distrito_select.php'); ?>
                  </select>
                </div>
              </div>
            </div>
            <!--- FIN DEL APARTADO DATOS UBIGEO -->
            <div class="form-group">
              <label for="txt_cochera_horaten" class="control-label">Horario de Atención</label>
              <input type="text" name="txt_cochera_horaten" id="txt_cochera_horaten" class="form-control input-sm" value="<?php echo $cochera_horaten; ?>">
            </div>

            <div class="form-group">
              <label for="cmb_nombreproveedor" class="control-label">Proveedor</label>
              <select id="cmb_nombreproveedor" name="cmb_nombreproveedor" class="form-control" value="<?php echo $proveedor_id; ?>">
                <?php require_once('../proveedor/proveedor_select.php') ?>
              </select>
            </div>



            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if ($action == 'eliminar') : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Cochera?</h4>
              </div>
            <?php endif; ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cochera_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cochera">Guardar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cochera">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cochera">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cochera/cochera_form.js?ver=1112229'; ?>"></script>