function cochera_form(usuario_act, cochera_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"cochera/cochera_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      cochera_id: cochera_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_cochera_form').html(data);
      	$('#modal_registro_cochera').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_cochera'); //funcion encontrada en public/js/generales.js
      	  modal_hidden_bs_modal('modal_registro_cochera', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'cochera';
      	var div = 'div_modal_cochera_form';
      	permiso_solicitud(usuario_act, cochera_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function cochera_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cochera/cochera_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#cochera_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_cochera_tabla').html(data);
      $('#cochera_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#cochera_mensaje_tbl').html('ERROR AL CARGAR DATOS DE COCHERA: ' + data.responseText);
    }
  });
}

function cochera_tarifa(usuario_act, tarifacochera_id, cochera_id){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"tarifacochera/tarifacochera_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      tarifacochera_id: tarifacochera_id,
      cochera_id: cochera_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_tarifacochera_form').html(data);
      	$('#modal_registro_tarifacochera').modal('show');

      	modal_hidden_bs_modal('modal_registro_tarifacochera', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      alerta_error('ERROR', data.responseText)
			console.log(data);
		}
	});
}

function estilos_datatable(){
  datatable_global = $('#tbl_cocheras').DataTable({
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Página mostrada _PAGE_ de _PAGES_",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
      columnDefs: [
         { orderable: false, targets: [4, 6] }
      ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}
$(document).ready(function() {
  estilos_datatable();
});

$(document).ready(function() {
  console.log('Perfil menu');

});
