<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL .'cochera/Cochera.class.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../cochera/Cochera.class.php');
  }

  $oCochera = new Cochera();

  $result = $oCochera->listar_cocheras();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr id="tabla_cabecera_fila">';
        $tr.='
          <td id="tabla_fila">'.$value['tb_cochera_id'].'</td>
          <td id="tabla_fila">'.$value['tb_cochera_nom'].'</td>
          <td id="tabla_fila">'.$value['tb_cochera_direc'].'</td>
          <td id="tabla_fila">'.$value['tb_ubigeo_cod'].'</td>
          <td id="tabla_fila">'.$value['tb_cochera_horaten'].'</td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-warning btn-xs" title="Asignar" onclick="cochera_tarifa(\'I\',0,'.$value['tb_cochera_id'].')"><i class="fa fa-plus"></i> Asignar</a>
          </td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="cochera_form(\'L\','.$value['tb_cochera_id'].')"><i class="fa fa-eye"></i> Ver</a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="cochera_form(\'M\','.$value['tb_cochera_id'].')"><i class="fa fa-edit"></i> Editar</a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cochera_form(\'E\','.$value['tb_cochera_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_cocheras" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila">NOMBRE</th>
      <th id="tabla_cabecera_fila">DIRECCIÓN</th>
      <th id="tabla_cabecera_fila">CODÍGO UBIGEO</th>
      <th id="tabla_cabecera_fila">HORARIO DE ATENCIÓN</th>
      <th id="tabla_cabecera_fila">TARIFA</th>
      <th id="tabla_cabecera_fila">OPCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
