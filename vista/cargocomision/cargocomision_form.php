<?php

require_once('../../core/usuario_sesion.php');
require_once("../cargocomision/Cargocomision.class.php");
$oComision = new Cargocomision();
require_once("../cargocomision/Serviciocomision.class.php");
$oServiciocomision = new Serviciocomision();
require_once("../empresa/Empresa.class.php");
$oEmpresa = new Empresa();
require_once("../cargo/Cargo.class.php");
$oCargo = new Cargo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usuario_action = $_POST['action'];
$cargo_id = $_POST['cargo_id'];
$comision_id = intval($_POST['comision_id']);
$serviciocomision_id = intval($_POST['serviciocomision_id']);
$vista = $_POST['vista'];

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

if($cargo_id > 0){
  $result = $oCargo->mostrarUno($cargo_id);
    if($result['estado'] == 1){
      $cargo_nom = $result['data']['tb_cargo_nom'];
    }
  $result = NULL;
}

if($serviciocomision_id > 0){
  $result = $oServiciocomision->mostrarUno($serviciocomision_id);
    if($result['estado'] == 1){
      $serviciocomision_nom = $result['data']['tb_serviciocomision_nom'];
    }
  $result = NULL;
}

$comision_est = 1; // siempre activo por defecto
if ($action == 'modificar') {
  $result = $oComision->mostrarUno($comision_id);
    if ($result['estado'] == 1) {
      $comision_fecini = mostrar_fecha($result['data']['tb_comision_fecini']);
      $comision_fecfin = mostrar_fecha($result['data']['tb_comision_fecfin']);
      $serviciocomision_id = $result['data']['tb_serviciocomision_id'];
      $comision_for = $result['data']['tb_comision_for'];
      $comision_sedes = $result['data']['tb_comision_sedes'];
      $cargo_id = $result['data']['tb_cargo_id'];
      $comision_est = $result['data']['tb_comision_est'];
    }
  $result = NULL;
}
//echo 'hasta aki estoy entrando normal ';exit();

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cargocomision" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">Fórmulas de Comisión para: <?php echo $serviciocomision_nom;?></h4>
      </div>
      <form id="form_cargocomision">
        <input name="action" id="action" type="hidden" value="<?php echo $action; ?>">
        <input name="hdd_comision_id" id="hdd_comision_id" type="hidden" value="<?php echo $comision_id; ?>">
        <input name="hdd_serviciocomision_id" id="hdd_serviciocomision_id" type="hidden" value="<?php echo $serviciocomision_id; ?>">
        <input name="hdd_cargo_id" id="hdd_cargo_id" type="hidden" value="<?php echo $cargo_id; ?>">

        <div class="modal-body">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="txt_cargo_nom" class="control-label">Cargo del Colaborador:</label>
                <input id="txt_cargo_nom" class="form-control input-sm" value="<?php echo $cargo_nom;?>" readonly/>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="txt_serviciocomision_nom" class="control-label">Servicio para Comisionar:</label>
                <input id="txt_serviciocomision_nom" class="form-control input-sm" value="<?php echo $serviciocomision_nom;?>" readonly/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="txt_comision_fecini" class="control-label">Fecha de Inicio</label>
                <div class="input-group date" id="datetimepicker1">
                  <input type="text" name="txt_comision_fecini" id="txt_comision_fecini" class="form-control input-sm" value="<?php echo $comision_fecini;?>" readonly>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group personal_ocultar fecha_finaliza">
                <label for="txt_comision_fecfin" class="control-label">Fecha de Finalización</label>
                <div class="input-group date" id="datetimepicker2">
                  <input type="text" name="txt_comision_fecfin" id="txt_comision_fecfin" class="form-control input-sm" value="<?php echo $comision_fecfin;?>" readonly>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-6 servicio_rango">
              <div class="form-group">
                <label for="cmb_comision_for" class="control-label">Tipo Fórmula:</label>
                <select name="cmb_comision_for" id="cmb_comision_for" class="form-control input-sm">
                  <option value="1" <?php if ($comision_for == 1) echo 'selected' ?>>Fórmula Fija</option>
                  <option value="2" <?php if ($comision_for == 2) echo 'selected' ?>>Fórmula Variable</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="cmb_comision_est" class="control-label">Estado:</label>
                <select name="cmb_comision_est" id="cmb_comision_est" class="form-control input-sm">
                  <option value="1" <?php if ($comision_est == 1) echo 'selected' ?>>Fórmula Vigente</option>
                  <option value="0" <?php if ($comision_est == 0) echo 'selected' ?>>Fórmula Inactivo</option>
                </select>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label for="cmb_comision_for" class="control-label">En qué sedes comisionará este Cargo:</label>
                <br>
                <?php 
                  $result = $oEmpresa->listar_Sucursales();
                    if($result['estado'] == 1){
                      foreach ($result['data'] as $key => $value) {
                        $checked = '';
                        $posicion = strpos($comision_sedes, $value['tb_empresa_id']);
                        if($posicion !== false)
                          $checked = 'checked';
                        echo '
                          <label class="checkbox-inline" style="padding-left: 0px;">
                            <input type="checkbox" name="che_comision_sedes[]" id="che_comision_sedes" value="'.$value['tb_empresa_id'].'" class="flat-green" '.$checked.'> '.$value['tb_empresa_nomcom'].'
                          </label>
                        ';
                      }
                    }
                  $result = NULL;
                ?>
              </div>
            </div>
          </div>

          <?php if($serviciocomision_id == 4):?>
            <div class="callout callout-warning">
              <p style="text-align: justify; color: black;"><b>En Crédito Menor la fórmula de comisión se basa al % de interés que se cobra al crédito, la comisión se calcula aplicando: PRESTAMO * INTERES * INTERES. Ejemplo: S/.100 (Préstamo) * 0.15(15% de interes) * 0.15 (15% de interés) = 2.25. Es importante también que el asesor de ventas gana el 100% del resultado, el admin de sede gana el 60% del resultado y el Sub Gerente gana el 40% del resultado, si en caso estos valores cambian colocar abajo el porcentaje final a ganar del resultado.</b></p>
            </div>
          <?php endif;?>

          <div class="row servicio_rango">
            <div class="col-md-12">
              <label for="txt_filtro" class="control-label" style="font-family: cambria;font-weight: bold;color: #006633">INGRESO DE RANGO DE MONTOS</label>
              <div class="panel panel-primary">
                <div class="panel-body">
                  <table width="100%" id="tbl_condiciones" class="table table-hover">
                    <tr id="tabla_cabecera" style="text-align: center">
                      <th id="tabla_cabecera_fila" style="text-align: center">DESDE</th>
                      <th id="tabla_cabecera_fila" style="text-align: center">HASTA</th>
                      <th id="tabla_cabecera_fila" style="text-align: center">PORCENTAJE</th>
                      <th id="tabla_cabecera_fila" style="text-align: center">ADD/DEL</th>
                    </tr>
                    <?php
                    if ($action == 'modificar') {
                      $c = 0;
                      $result = $oComision->mostrar_comisiondetalle($comision_id);
                        if ($result['estado'] == 1) {
                          foreach ($result['data'] as $key => $dt) {
                            $display = 'style="display: none;"';
                            $tr_new = '';

                            if ($comision_for == 2 && $c == 0) // TIPO DE FORMULA FIJA 1, 2 VARIABLE
                              $display = '';
                            
                            if ($c > 0)
                              $tr_new = 'class="tr_new"';

                            echo '
                              <tr '.$tr_new.'>
                                <td id="tabla_fila"><input type="text" name="txt_comi_cond1[]" class="moneda form-control input-sm" value="' . mostrar_moneda($dt['tb_comisiondetalle_cond1'], 2, ',') . '"></td>
                                <td id="tabla_fila"><input type="text" name="txt_comi_cond2[]" class="moneda form-control input-sm"  value="' . mostrar_moneda($dt['tb_comisiondetalle_cond2'], 2, ',') . '"></td>
                                <td id="tabla_fila"><input type="text" name="txt_comi_porcen[]" class="porcentaje form-control input-sm" value="' . mostrar_moneda($dt['tb_comisiondetalle_porcen'], 2, ',') . '"></td>
                                <td id="tabla_fila"><a href="#add" id="btn_add" ' . $display . ' class="btn btn-success btn-xs">Agregar</a></td>
                              </tr>';

                            $c++;
                          }
                        }
                      $result = NULL;
                    }
                    ?>
                    <?php if ($action != 'modificar') : ?>
                      <tr>
                        <td id="tabla_fila"><input type="text" id="txt_comi_cond1" name="txt_comi_cond1[]" class="moneda form-control input-sm" size="10"></td>
                        <td id="tabla_fila"><input type="text" name="txt_comi_cond2[]" class="moneda form-control input-sm" id="txt_comi_cond2" size="10"></td>
                        <td id="tabla_fila"><input type="text" name="txt_comi_porcen[]" class="porcentaje form-control input-sm" id="txt_comi_porcen" size="10"></td>
                        <td id="tabla_fila"><a href="#add" id="btn_add" style="display: none;" class="btn btn-success btn-xs">+</a></td>
                      </tr>
                    <?php endif; ?>
                  </table>

                </div>
              </div>
            </div>
          </div>

          <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
          <?php if ($action == 'eliminar') : ?>
            <div class="callout callout-warning">
              <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Fórmula de Comisión?</h4>
            </div>
          <?php endif; ?>
          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="formulacomision_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_formulacomision">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/cargocomision/cargocomision_form.js'; ?>"></script>