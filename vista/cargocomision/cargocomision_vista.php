<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $menu_tit; ?>
      <small><?php echo $menu_des; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Planilla</a></li>
      <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
    </ol>
  </section>
  <style type="text/css">
    .c_cursor{
      cursor: pointer;
    }
  </style>
  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header">
        <div class="panel panel-info">
          <div class="panel-body">
            <div class="callout callout-info">
              <h4><i class="fa fa-info"></i> Si deseas que el Jefe comisione por cada asesor a su cargo: debes seleccionar cargos que deban tener el nombre: JEDE DE SEDE, luego podría ser - MALL, - BOULEVARD, - VEHICULAR, ETC</h4>
            </div>
            <label for="cmb_cargo_id" class="">Selecciona un cargo para poder asignarle los servicios a comisionar:</label>
            <select name="cmb_cargo_id" id="cmb_cargo_id" class="form-control selectpicker" data-live-search="true" data-max-options="1" data-size="12">
              <?php require_once 'vista/cargo/cargo_select.php';?>
            </select>
          </div>
        </div>
      </div>
      <div class="box-body">

        <!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="planilla_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="box box-info shadow-simple">
              <div class="box-header"><h3 class="box-title">Servicios con Comisiones de Rangos</h3></div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-aqua" onclick="cargocomision_form('I', 0, 1)"><i class="fa fa-key"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 1</span>
                        <span class="info-box-number">Asistencia Vehicular</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-red" onclick="cargocomision_form('I', 0, 2)"><i class="fa fa-car"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 2</span>
                        <span class="info-box-number">Garantía Vehicular / Preconstitución</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-green" onclick="cargocomision_form('I', 0, 3)"><i class="fa fa-home"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 3</span>
                        <span class="info-box-number">Crédito Hipotecario</span>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-black" onclick="cargocomision_form('I', 0, 5)"><i class="fa fa-shopping-cart"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 5</span>
                        <span class="info-box-number">Venta de Productos Remate</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-purple" onclick="cargocomision_form('I', 0, 6)"><i class="fa fa-retweet"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 6</span>
                        <span class="info-box-number">Sobre Costo de Remates</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-aqua" onclick="cargocomision_form('I', 0, 7)"><i class="fa fa-taxi"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 7</span>
                        <span class="info-box-number">Venta de Vehículos</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="box box-info shadow-simple col-md-4">
              <div class="box-header"><h3 class="box-title">Servicios con Comisiones Internas</h3></div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-yellow" onclick="cargocomision_form('I', 0, 4)"><i class="fa fa-laptop"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 4</span>
                        <span class="info-box-number">Crédito Menor</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon c_cursor bg-red" onclick="cargocomision_form('I', 0, 8)"><i class="fa fa-bar-chart"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Servicio 8</span>
                        <span class="info-box-number">Gestión de Cobranza</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="cargocomision_tabla"></div>
      </div>

      <div id="div_modal_registro_cargocomision">
      </div>
      <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
      <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
    </div>
  </section>
  <!-- /.content -->
</div>