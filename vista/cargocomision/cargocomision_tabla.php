<?php
  require_once('../cargocomision/Cargocomision.class.php');
  $oComision = new Cargocomision();
  require_once('../funciones/fechas.php');

  $tabla_comisiones = '';
  $cargo_id = intval($_POST['cargo_id']);

  $result = $oComision->listar_comisiones($cargo_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $formula = 'Rango Fijo';
        $estado = '<span class="badge bg-red">Inactivo</span>';
        if($value['tb_comision_for'] == 2)
          $formula = 'Rango Variable';
        if($value['tb_comision_est'] == 1)
        $estado = '<span class="badge bg-green">Activo</span>';

        $tabla_comisiones .= '
          <tr>
            <td>'.$value['tb_comision_id'].'</td>
            <td>'.$value['tb_comision_fecini'].'</td>
            <td>'.$value['tb_comision_fecfin'].'</td>
            <td>'.$value['tb_serviciocomision_nom'].'</td>
            <td>'.$formula.'</td>
            <td>'.$value['tb_comision_sedes'].'</td>
            <td>'.$estado.'</td>
            <td> 
              <a class="btn btn-warning btn-xs" onclick="cargocomision_form(\'M\', '.$value['tb_comision_id'].','.$value['tb_serviciocomision_id'].')"><i class="fa fa-pencil"></i></a> 
            </td>
          </tr>
        ';
      }
    }
  $result = NULL;
?>
<div class="box box-info shadow-simple">
  <div class="box-header"><h3 class="box-title">Comisiones para el Cargo seleccionado</h3></div>
  <div class="box-body">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha Inicio</th>
          <th>Fecha Fin</th>
          <th>Servicio</th>
          <th>Fórmula</th>
          <th>Sedes</th>
          <th>Estado</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        <?php echo $tabla_comisiones;?>
      </tbody>
    </table>
  </div>
</div>