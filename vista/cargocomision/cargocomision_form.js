$(document).ready(function () {
  console.log("Mejorado corre 8888 ");

  ocultar_formula_rango();

  $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#datetimepicker1').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //endDate : new Date()
  });

  $('#datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //endDate : new Date()
  });

  $('.moneda').autoNumeric({
    aSep: ',',
    aDec: '.',
    vMin: '0',
    vMax: '9999999.99'
  });
  $('.porcentaje').autoNumeric({
    vMin: '0',
    vMax: '100.00'
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_comision_fecini').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_comision_fecfin').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $('#btn_add').click(function (event) {
    event.preventDefault();
    var tr = $('<tr class="tr_new"></tr>');
    tr.append('<td id="tabla_fila"><input type="text" name="txt_comi_cond1[]" class="moneda form-control input-sm" size="10"></td>');
    tr.append('<td id="tabla_fila"><input type="text" name="txt_comi_cond2[]" class="moneda form-control input-sm" size="10"></td>');
    tr.append('<td id="tabla_fila"><input type="text" name="txt_comi_porcen[]" class="porcentaje form-control input-sm" size="10"></td>');
    tr.append('<td id="tabla_fila"><a href="#add" class="btn_del btn btn-danger btn-xs">-</a></td>');

    $('#tbl_condiciones').append(tr);
    //moneda y porcentaje
    tr.find('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0',
        vMax: '9999999.99'
    });

    tr.find('.porcentaje').autoNumeric({
        vMin: '0',
        vMax: '100.00'
    });

    ///botonesssss
    $('.btn_del').button({
        icons: {primary: "ui-icon-trash"},
        text: false
    });
    $('.btn_del').click(function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });
  });

  $('#cmb_comision_for').change(function (event) {
    var formu = $(this).val();
    if (parseInt(formu) == 1) {
        $('#btn_add').hide();
        $('.tr_new').remove();
    } else
        $('#btn_add').show();
  });

  $('#form_cargocomision').validate({
    submitHandler: function () {
      var checkboxes = $('input[type="checkbox"][name="che_comision_sedes[]"]');
      var alMenosUnoSeleccionado = checkboxes.is(':checked');
      var cond1 = $('#txt_comi_cond1').val();
      var cond2 = $('#txt_comi_cond2').val();
      var porc = $('#txt_comi_porcen').val();
      var serviciocomision_id = $('#hdd_serviciocomision_id').val();

      console.log('absj ajs aks: ' + cond1)

      if (!alMenosUnoSeleccionado) {
        swal_success("Sedes", 'Selecciona al menos una sede para comisionar este asesor', 2500);
        return false;
      }
      if(serviciocomision_id != 4 && serviciocomision_id != 8){
        if (cond1 == '' || cond2 == '' || porc == ''){
          swal_success("Rangos", 'Ingresa los valores del rango para las comisiones', 2500);
          return false;
        }
      }

      $.ajax({
        type: "POST",
        url: VISTA_URL + "cargocomision/cargocomision_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_cargocomision").serialize(),
        beforeSend: function () {
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {

            notificacion_success(data.mensaje, 6000);
            swal_success("COMISON PARA UN CARGO", data.mensaje, 2500);
            $('#modal_registro_cargocomision').modal('hide');
            cargocomision_tabla();
          } 
          else {
            alerta_error('Error', data.mensaje);
            console.log(data)
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function (data) {
          alerta_error('Error', data.responseText);
          console.log(data)
        }
      });
    },
    rules: {
      txt_comision_fecini: {
        required: true
      },
      txt_comision_fecfin: {
        required: true
      }
    },
    messages: {
      txt_comision_fecini: {
        required: "Ingresa la fecha de inicio"
      },
      txt_comision_fecfin: {
        required: "Ingresa la fecha final"
      }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
  });
});

function ocultar_formula_rango(){
  var serviciocomision_id = $('#hdd_serviciocomision_id').val();
  if(parseInt(serviciocomision_id) == 8)
    $('.servicio_rango').hide()
  else
    $('.servicio_rango').show()
}