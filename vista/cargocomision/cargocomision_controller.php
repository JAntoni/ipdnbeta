<?php
require_once('../../core/usuario_sesion.php');

require_once('../cargocomision/Cargocomision.class.php');
$oComision = new Cargocomision();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];

$oComision->comision_id = intval($_POST['hdd_comision_id']);
$oComision->comision_fecini = fecha_mysql($_POST['txt_comision_fecini']);
$oComision->comision_fecfin = fecha_mysql($_POST['txt_comision_fecfin']);
$oComision->serviciocomision_id = intval($_POST['hdd_serviciocomision_id']);
$oComision->comision_for = intval($_POST['cmb_comision_for']);
$oComision->comision_sedes = ''; // lo realizamos en los if mediante un for
$oComision->cargo_id = intval($_POST['hdd_cargo_id']);
$oComision->comision_est = intval($_POST['cmb_comision_est']);


if ($action == 'insertar') {
  $data['estado'] = 0;
  $data['mensaje'] = 'El contrato ha sido registrado correctamente';

  $sedes = '';
  if(!empty($_POST['che_comision_sedes']))
    $sedes = implode(",", $_POST['che_comision_sedes']);

  $array_cond1 = $_POST['txt_comi_cond1']; //array de condiociones primeras
  $array_cond2 = $_POST['txt_comi_cond2']; //array de condiciones segundas
  $array_porcen = $_POST['txt_comi_porcen']; //array de porcentajes depende a las condiciones

  if(count($array_cond1) == count($array_cond2) && count($array_cond1) == count($array_porcen)){
    $oComision->comision_sedes = $sedes;
    $result = $oComision->insertar();
    $comision_id = $result['comision_id'];

    $cond = count($array_cond1);
    for ($i=0; $i < $cond; $i++) { 
      $oComision->comision_id = $comision_id;
      $oComision->comisiondetalle_cond1 = moneda_mysql($array_cond1[$i]);
      $oComision->comisiondetalle_cond2 = moneda_mysql($array_cond2[$i]);
      $oComision->comisiondetalle_porcen = $array_porcen[$i];
      $oComision->insertar_detalle();
    }

    $data['estado'] = 1;
    $data['mensaje'] = 'El detalle de comisiones para el cargo se guardó correctamente';
  }
  else{
    $data['estado'] = 0;
    $data['mensaje'] = 'El rango para las comisiones no debe estar vacía, cada campo debe estar llenado.';
  }

  echo json_encode($data);
}
elseif ($action == 'modificar') {
  $data['estado'] = 0;
  $data['mensaje'] = 'El contrato ha sido modificado correctamente';

  $sedes = '';
  if(!empty($_POST['che_comision_sedes']))
    $sedes = implode(",", $_POST['che_comision_sedes']);

  $array_cond1 = $_POST['txt_comi_cond1']; //array de condiociones primeras
  $array_cond2 = $_POST['txt_comi_cond2']; //array de condiciones segundas
  $array_porcen = $_POST['txt_comi_porcen']; //array de porcentajes depende a las condiciones

  if(count($array_cond1) == count($array_cond2) && count($array_cond1) == count($array_porcen)){
    $oComision->comision_sedes = $sedes;
    $result = $oComision->editar();

    $comision_id = intval($_POST['hdd_comision_id']);

    //? ANTES DE REGISTRAR EL NUEVO DETALLE DE COMISIONES ELIMINANOS LOS 
    $oComision->eliminar_comisiondetalle($comision_id);
    
    $cond = count($array_cond1);
    for ($i=0; $i < $cond; $i++) { 
      $oComision->comision_id = $comision_id;
      $oComision->comisiondetalle_cond1 = moneda_mysql($array_cond1[$i]);
      $oComision->comisiondetalle_cond2 = moneda_mysql($array_cond2[$i]);
      $oComision->comisiondetalle_porcen = $array_porcen[$i];
      $oComision->insertar_detalle();
    }

    $data['estado'] = 1;
    $data['mensaje'] = 'El detalle de comisiones para el cargo se guardó correctamente';
  }
  else{
    $data['estado'] = 0;
    $data['mensaje'] = 'El rango para las comisiones no debe estar vacía, cada campo debe estar llenado.';
  }

  echo json_encode($data);
}
elseif ($action == 'eliminar') {
  $data['estado'] = 1;
  $data['mensaje'] = 'El contrato ha sido ELIMINADO correctamente';

  $oPersonal->eliminar($oPersonal->personalcontrato_id);

  echo json_encode($data);
}
else {
  $data['estado'] = 0;
  $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

  echo json_encode($data);
}
?>