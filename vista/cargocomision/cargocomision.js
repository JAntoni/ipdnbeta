function cargocomision_form(usuario_act, comision_id, serviciocomision_id) {
  var cargo_id = $('#cmb_cargo_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "cargocomision/cargocomision_form.php",
    async: true,
    dataType: "html",
    data: {
      cargo_id: cargo_id,
      action: usuario_act,
      comision_id: comision_id,
      serviciocomision_id: serviciocomision_id,
      vista: 'cargocomision'
    },
    beforeSend: function () {
      
    },
    success: function (data) {
      if (data != "sin_datos") {
        $("#div_modal_registro_cargocomision").html(data);
        $("#modal_registro_cargocomision").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_cargocomision"); //funcion encontrada en public/js/generales.js

        //funcion js para limbiar el modal al cerrarlo
        modal_hidden_bs_modal("modal_registro_cargocomision", "limpiar"); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        // modal_height_auto("modal_registro_cliente"); //funcion encontrada en public/js/generales.js
        // //funcion js para agregar un ancho automatico al modal, al abrirlo
        // modal_width_auto("modal_registro_cliente", 75); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

function cargocomision_tabla() {
  var cargo_id = $('#cmb_cargo_id').val();

  $.ajax({
    type: "POST",
    url: VISTA_URL + "cargocomision/cargocomision_tabla.php",
    async: true,
    dataType: "html",
    data: {
      cargo_id: cargo_id,
    },
    beforeSend: function () {
    },
    success: function (data) {
      $('.cargocomision_tabla').html(data)
    },
    complete: function (data) {
    },
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      console.log(data);
    }
  });
}

$(document).ready(function () {
  console.log("Mejorado corre 222 ");

  $("#cmb_cargo_id").change(function () {
    console.log('asbja sbkja sb')
    cargocomision_tabla();
  });
});