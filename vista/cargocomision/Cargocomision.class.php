<?php

  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Cargocomision extends Conexion{
  
  public $comision_id;
  public $comision_fecini;
  public $comision_fecfin;
  public $serviciocomision_id;
  public $comision_for;
  public $comision_sedes;
  public $cargo_id;
  public $comision_est;

  //de ta bala comisiondetalle
  public $comisiondetalle_cond1;
  public $comisiondetalle_cond2;
  public $comisiondetalle_porcen;
            
  function insertar(){
    $this->dblink->beginTransaction();
    try {
      $columns = [
        'tb_comision_fecini',
        'tb_comision_fecfin',
        'tb_serviciocomision_id',
        'tb_comision_for',
        'tb_comision_sedes',
        'tb_cargo_id'
      ];

      // Lista de placeholders
      $placeholders = implode(',', array_map(function ($column) {
        return ':' . $column;
      }, $columns));

      $sql = "INSERT INTO tb_comision (" . implode(',', $columns) . ") VALUES ($placeholders)";
            
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_comision_fecini", $this->comision_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_comision_fecfin", $this->comision_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_serviciocomision_id", $this->serviciocomision_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_comision_for", $this->comision_for, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_comision_sedes", $this->comision_sedes, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_cargo_id", $this->cargo_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $comision_id = $this->dblink->lastInsertId();

      $this->dblink->commit();

      $data['estado'] = $result;
      $data['comision_id'] = $comision_id;
      return $data; //si es correcto el ingreso retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
    
  function insertar_detalle(){
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_comisiondetalle(
          tb_comision_id, 
          tb_comisiondetalle_cond1,
          tb_comisiondetalle_cond2,
          tb_comisiondetalle_porcen) 
        VALUES (
          :tb_comision_id, 
          :tb_comisiondetalle_cond1,
          :tb_comisiondetalle_cond2,
          :tb_comisiondetalle_porcen)";
            
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_comision_id", $this->comision_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_comisiondetalle_cond1", $this->comisiondetalle_cond1, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_comisiondetalle_cond2", $this->comisiondetalle_cond2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_comisiondetalle_porcen", $this->comisiondetalle_porcen, PDO::PARAM_STR);
      
      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result;

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
    
  function editar(){
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_comision SET 
        tb_comision_fecini =:tb_comision_fecini,
        tb_comision_fecfin =:tb_comision_fecfin,
        tb_comision_for =:tb_comision_for,
        tb_comision_sedes =:tb_comision_sedes,
        tb_comision_est =:tb_comision_est
        WHERE 
          tb_comision_id =:tb_comision_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_comision_fecini", $this->comision_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_comision_fecfin", $this->comision_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_comision_for", $this->comision_for, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_comision_sedes", $this->comision_sedes, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_comision_est", $this->comision_est, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_comision_id", $this->comision_id, PDO::PARAM_INT);
      
      $result = $sentencia->execute();
      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  
  function eliminar($comi_id){
    $this->dblink->beginTransaction();
    try {
        $sql = "UPDATE tb_comision SET tb_comision_xac = 0 where tb_comision_id =:tb_comision_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_comision_id", $comi_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  
  function eliminar_comisiondetalle($comision_id){
    $this->dblink->beginTransaction();
    try {
        $sql = "DELETE FROM tb_comisiondetalle where tb_comision_id =:comision_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":comision_id",$comision_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }   
  
  function mostrarUno($comi_id){
    try {
        $sql = "SELECT * FROM tb_comision where tb_comision_id =$comi_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_comision_id", $comi_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
    
  function mostrar_comisiondetalle($comision_id){
    try {
        $sql = "SELECT * FROM tb_comisiondetalle where tb_comision_id =:comision_id";
  
      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":comision_id", $comision_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_comisiones($cargo_id){
    try {
        $sql = "SELECT * FROM tb_comision comi
          INNER JOIN tb_serviciocomision ser ON ser.tb_serviciocomision_id = comi.tb_serviciocomision_id
          WHERE tb_cargo_id =:cargo_id";
  
      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_comisiones_vigentes($fecha, $cargo_id){
    try {
      $sql = "SELECT * FROM tb_comision c
        WHERE c.tb_comision_id IN (
          SELECT MAX(tb_comision_id)
          FROM tb_comision
          WHERE :fecha BETWEEN tb_comision_fecini AND tb_comision_fecfin AND tb_cargo_id = :cargo_id AND tb_comision_est = 1
          GROUP BY tb_serviciocomision_id
      );";
  
      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
      $sentencia->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  
  function mostrar_porcentaje_comision_por_monto($comision_id, $monto){
    try {
      $sql = "SELECT * FROM tb_comision comi
      INNER JOIN tb_comisiondetalle det ON det.tb_comision_id = comi.tb_comision_id
      WHERE comi.tb_comision_id =:comision_id AND :monto BETWEEN tb_comisiondetalle_cond1 AND tb_comisiondetalle_cond2 limit 1";
  
      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":comision_id", $comision_id, PDO::PARAM_INT);
      $sentencia->bindParam(":monto", $monto, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      }
      else{
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
?>