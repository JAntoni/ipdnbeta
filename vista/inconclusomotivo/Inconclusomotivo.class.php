<?php
if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Inconclusomotivo extends Conexion {
    public $id;
    public $nom;
    public $reg;
    public $xac;

    public function listar_todos($where, $join, $otros) {
        try {
            $sql = "SELECT im.*";
            if (!empty($join)) {
                for ($j = 0; $j < count($join); $j++) {
                    $sql .= ",\n" . $join[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_inconclusomotivo im\n";
            if (!empty($join)) {
                for ($k = 0; $k < count($join); $k++) {
                    $sql .= $join[$k]['tipo_union'] . " JOIN " . $join[$k]['tabla_alias'] . " ON ";
                    if (!empty($join[$k]['columna_enlace']) && !empty($join[$k]['alias_columnaPK'])) {
                        $sql .= $join[$k]['columna_enlace'] . " = " . $join[$k]['alias_columnaPK'] . "\n";
                    } else {
                        $sql .= $join[$k]['on'] . "\n";
                    }
                }
            }
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i])) {
                    $sql .= ($i > 0) ? "\n{$where[$i]["conector"]} " : "WHERE ";

                    if (!empty($where[$i]["column_name"])) {
                        if (stripos($where[$i]["column_name"], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$where[$i]["column_name"]}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$where[$i]["column_name"]}";
                        }

                        if (!empty($where[$i]["datatype"])) {
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " " . $where[$i]["param$i"];
                        }
                    } else {
                        $sql .= $where[$i]["param$i"];
                    }
                }
            }
            if (!empty($otros["group"])) {
                $sql .= "\nGROUP BY {$otros["group"]["column_name"]}";
            }
            if (!empty($otros["orden"])) {
                $sql .= "\nORDER BY {$otros["orden"]["column_name"]} {$otros["orden"]["value"]}";
            }
            if (!empty($otros["limit"])) {
                $sql .= "\nLIMIT {$otros["limit"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($where); $i++) {
                if (!empty($where[$i]["column_name"]) && !empty($where[$i]["datatype"])) {
                    $_PARAM = strtoupper($where[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $where[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            $retorno["sql"] = $sql;
            $retorno["row_count"] = $sentencia->rowCount();
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }
}
