<?php
require_once('Inconclusomotivo.class.php');
$oInconclusomotivo = new Inconclusomotivo();

if(empty($_POST["inconclusomotivo_id"]) && empty($inconclusomotivo_id)){
	$inconclusomotivo_id = 0;
}
elseif(!empty($inconclusomotivo_id)){
	$inconclusomotivo_id = intval($inconclusomotivo_id);
}
else{
	$inconclusomotivo_id = intval($_POST["inconclusomotivo_id"]);
}

$option = '<option value="0" style="font-weight: bold;">SELECCIONE</option>';

$where[0]['column_name'] = 'tb_inconclusomotivo_xac';
$where[0]['param0'] = 1;
$where[0]['datatype'] = 'INT';

$otros['orden']['column_name'] = 'tb_inconclusomotivo_nom';
$otros['orden']['value'] = 'ASC';

//PRIMER NIVEL
$result = $oInconclusomotivo->listar_todos($where, array(), $otros);

if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($inconclusomotivo_id == $value['tb_inconclusomotivo_id'])
			$selected = 'selected';

		$option .= '<option value="' . $value['tb_inconclusomotivo_id'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_inconclusomotivo_nom'] . '</option>';
	}
}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>