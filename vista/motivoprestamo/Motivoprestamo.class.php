<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Motivoprestamo extends Conexion{

    function insertar($motivoprestamo_nom, $motivoprestamo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_motivoprestamo(tb_motivoprestamo_nom, tb_motivoprestamo_des)
          VALUES (:motivoprestamo_nom, :motivoprestamo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivoprestamo_nom", $motivoprestamo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":motivoprestamo_des", $motivoprestamo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($motivoprestamo_id, $motivoprestamo_nom, $motivoprestamo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_motivoprestamo SET tb_motivoprestamo_nom =:motivoprestamo_nom, tb_motivoprestamo_des =:motivoprestamo_des WHERE tb_motivoprestamo_id =:motivoprestamo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivoprestamo_id", $motivoprestamo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":motivoprestamo_nom", $motivoprestamo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":motivoprestamo_des", $motivoprestamo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($motivoprestamo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_motivoprestamo SET tb_motivoprestamo_xac = 0  WHERE tb_motivoprestamo_id =:motivoprestamo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivoprestamo_id", $motivoprestamo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($motivoprestamo_id){
      try {
        $sql = "SELECT * FROM tb_motivoprestamo WHERE tb_motivoprestamo_id =:motivoprestamo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivoprestamo_id", $motivoprestamo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_motivoprestamos(){
      try {
        $sql = "SELECT * FROM tb_motivoprestamo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "Aún no hay motivos de préstamos registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
