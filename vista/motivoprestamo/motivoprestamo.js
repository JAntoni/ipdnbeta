function motivoprestamo_form(usuario_act, motivoprestamo_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"motivoprestamo/motivoprestamo_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      motivoprestamo_id: motivoprestamo_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_motivoprestamo_form').html(data);
      	$('#modal_registro_motivoprestamo').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_motivoprestamo'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_motivoprestamo', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'motivoprestamo';
      	var div = 'div_modal_motivoprestamo_form';
      	permiso_solicitud(usuario_act, motivoprestamo_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function motivoprestamo_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"motivoprestamo/motivoprestamo_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#motivoprestamo_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_motivoprestamo_tabla').html(data);
      $('#motivoprestamo_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#motivoprestamo_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
$(document).ready(function() {
  console.log('loader de cmotivoprestamo 11');
});