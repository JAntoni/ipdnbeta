$(document).ready(function(){
  $('#form_motivoprestamo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"motivoprestamo/motivoprestamo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_motivoprestamo").serialize(),
				beforeSend: function() {
					$('#motivoprestamo_mensaje').show(400);
					$('#btn_guardar_motivoprestamo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#motivoprestamo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#motivoprestamo_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		motivoprestamo_tabla();
		      		$('#modal_registro_motivoprestamo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#motivoprestamo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#motivoprestamo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_motivoprestamo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#motivoprestamo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#motivoprestamo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_motivoprestamo_nom: {
				required: true,
				minlength: 2
			}
		},
		messages: {
			txt_motivoprestamo_nom: {
				required: "Ingrese un nombre para el Motivo del Préstamo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
