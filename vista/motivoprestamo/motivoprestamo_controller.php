<?php
 	require_once('../motivoprestamo/Motivoprestamo.class.php');
  $oMotivoprestamo = new Motivoprestamo();

 	$action = $_POST['action'];

	 $motivoprestamo_nom = mb_strtoupper($_POST['txt_motivoprestamo_nom'], 'UTF-8');
	 $motivoprestamo_des = mb_strtoupper($_POST['txt_motivoprestamo_nom'], 'UTF-8'); // cambiar si el detalle sea otro texto

 	if($action == 'insertar'){
 	
 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Motivo del Préstamo.';
 		if($oMotivoprestamo->insertar($motivoprestamo_nom, $motivoprestamo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Motivo del Préstamo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$motivoprestamo_id = intval($_POST['hdd_motivoprestamo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Motivo del Préstamo.';

 		if($oMotivoprestamo->modificar($motivoprestamo_id, $motivoprestamo_nom, $motivoprestamo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Motivo del Préstamo modificado correctamente. '.$motivoprestamo_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$motivoprestamo_id = intval($_POST['hdd_motivoprestamo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Motivo del Préstamo.';

 		if($oMotivoprestamo->eliminar($motivoprestamo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Motivo del Préstamo eliminado correctamente. '.$motivoprestamo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>