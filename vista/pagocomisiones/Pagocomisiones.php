<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Pagocomisiones extends Conexion {

    private $tb_pagocomisiones_id;
    private $tb_pagocomisiones_reg;
    private $tb_pagocomisiones_xac;
    private $tb_pagocomisiones_fec;
    private $tb_pagocomisiones_usureg;
    private $tb_usuario_id; // id de asesor de credito
    private $tb_vendedor_id; // id de vendedor de vehiculo
    private $tb_vehiculomarca_id;
    private $tb_vehiculomodelo_id;
    private $tb_pagocomisiones_tasa; //tasacion vehicular 
    private $tb_pagocomisiones_costo; //costo de la polisa
    private $tb_pagocomisiones_formapago; //costo de la polisa
    private $tb_pagocomisiones_comision;
    private $tb_pagocomisiones_tipo; //1.pre constitucion 2.constitucion
    private $tb_ingreso_id;
    private $tb_egreso_id;
    private $tb_pagocomisiones_estado; // 1. cancelado al vendedor - 2. ingresado a caja
    private $tb_pagocomisiones_his; // 1. cancelado al vendedor - 2. ingresado a caja
    private $tb_moneda_id; // 1. cancelado al vendedor - 2. ingresado a caja
    private $tb_cliente_id; // 1. cancelado al vendedor - 2. ingresado a caja
    private $tb_pagocomisiones_tipcam; // 1. cancelado al vendedor - 2. ingresado a caja
    private $tb_pagocomisiones_monven; // monto pagado al vendedor de la consecionaria
    private $tb_pagocomisiones_monipdn; // monto ingresado a caja de ipdn
    private $tb_pagocomisiones_poliza; // monto ingresado a caja de ipdn
    
    public function getTb_pagocomisiones_id() {
        return $this->tb_pagocomisiones_id;
    }

    public function getTb_pagocomisiones_reg() {
        return $this->tb_pagocomisiones_reg;
    }

    public function getTb_pagocomisiones_xac() {
        return $this->tb_pagocomisiones_xac;
    }

    public function getTb_pagocomisiones_fec() {
        return $this->tb_pagocomisiones_fec;
    }

    public function getTb_pagocomisiones_usureg() {
        return $this->tb_pagocomisiones_usureg;
    }

    public function getTb_usuario_id() {
        return $this->tb_usuario_id;
    }

    public function getTb_vendedor_id() {
        return $this->tb_vendedor_id;
    }

    public function getTb_vehiculomarca_id() {
        return $this->tb_vehiculomarca_id;
    }

    public function getTb_vehiculomodelo_id() {
        return $this->tb_vehiculomodelo_id;
    }

    public function getTb_pagocomisiones_tasa() {
        return $this->tb_pagocomisiones_tasa;
    }

    public function getTb_pagocomisiones_costo() {
        return $this->tb_pagocomisiones_costo;
    }

    public function getTb_pagocomisiones_formapago() {
        return $this->tb_pagocomisiones_formapago;
    }

    public function getTb_pagocomisiones_comision() {
        return $this->tb_pagocomisiones_comision;
    }

    public function getTb_pagocomisiones_tipo() {
        return $this->tb_pagocomisiones_tipo;
    }

    public function getTb_ingreso_id() {
        return $this->tb_ingreso_id;
    }

    public function getTb_egreso_id() {
        return $this->tb_egreso_id;
    }

    public function getTb_pagocomisiones_estado() {
        return $this->tb_pagocomisiones_estado;
    }

    public function getTb_pagocomisiones_his() {
        return $this->tb_pagocomisiones_his;
    }

    public function getTb_moneda_id() {
        return $this->tb_moneda_id;
    }

    public function getTb_cliente_id() {
        return $this->tb_cliente_id;
    }

    public function getTb_pagocomisiones_tipcam() {
        return $this->tb_pagocomisiones_tipcam;
    }

    public function getTb_pagocomisiones_monven() {
        return $this->tb_pagocomisiones_monven;
    }

    public function getTb_pagocomisiones_monipdn() {
        return $this->tb_pagocomisiones_monipdn;
    }

    public function getTb_pagocomisiones_poliza() {
        return $this->tb_pagocomisiones_poliza;
    }

    public function setTb_pagocomisiones_id($tb_pagocomisiones_id){
        $this->tb_pagocomisiones_id = $tb_pagocomisiones_id;
    }

    public function setTb_pagocomisiones_reg($tb_pagocomisiones_reg){
        $this->tb_pagocomisiones_reg = $tb_pagocomisiones_reg;
    }

    public function setTb_pagocomisiones_xac($tb_pagocomisiones_xac){
        $this->tb_pagocomisiones_xac = $tb_pagocomisiones_xac;
    }

    public function setTb_pagocomisiones_fec($tb_pagocomisiones_fec){
        $this->tb_pagocomisiones_fec = $tb_pagocomisiones_fec;
    }

    public function setTb_pagocomisiones_usureg($tb_pagocomisiones_usureg){
        $this->tb_pagocomisiones_usureg = $tb_pagocomisiones_usureg;
    }

    public function setTb_usuario_id($tb_usuario_id){
        $this->tb_usuario_id = $tb_usuario_id;
    }

    public function setTb_vendedor_id($tb_vendedor_id){
        $this->tb_vendedor_id = $tb_vendedor_id;
    }

    public function setTb_vehiculomarca_id($tb_vehiculomarca_id){
        $this->tb_vehiculomarca_id = $tb_vehiculomarca_id;
    }

    public function setTb_vehiculomodelo_id($tb_vehiculomodelo_id){
        $this->tb_vehiculomodelo_id = $tb_vehiculomodelo_id;
    }

    public function setTb_pagocomisiones_tasa($tb_pagocomisiones_tasa){
        $this->tb_pagocomisiones_tasa = $tb_pagocomisiones_tasa;
    }

    public function setTb_pagocomisiones_costo($tb_pagocomisiones_costo){
        $this->tb_pagocomisiones_costo = $tb_pagocomisiones_costo;
    }

    public function setTb_pagocomisiones_formapago($tb_pagocomisiones_formapago){
        $this->tb_pagocomisiones_formapago = $tb_pagocomisiones_formapago;
    }

    public function setTb_pagocomisiones_comision($tb_pagocomisiones_comision){
        $this->tb_pagocomisiones_comision = $tb_pagocomisiones_comision;
    }

    public function setTb_pagocomisiones_tipo($tb_pagocomisiones_tipo){
        $this->tb_pagocomisiones_tipo = $tb_pagocomisiones_tipo;
    }

    public function setTb_ingreso_id($tb_ingreso_id){
        $this->tb_ingreso_id = $tb_ingreso_id;
    }

    public function setTb_egreso_id($tb_egreso_id){
        $this->tb_egreso_id = $tb_egreso_id;
    }

    public function setTb_pagocomisiones_estado($tb_pagocomisiones_estado){
        $this->tb_pagocomisiones_estado = $tb_pagocomisiones_estado;
    }

    public function setTb_pagocomisiones_his($tb_pagocomisiones_his){
        $this->tb_pagocomisiones_his = $tb_pagocomisiones_his;
    }

    public function setTb_moneda_id($tb_moneda_id){
        $this->tb_moneda_id = $tb_moneda_id;
    }

    public function setTb_cliente_id($tb_cliente_id){
        $this->tb_cliente_id = $tb_cliente_id;
    }

    public function setTb_pagocomisiones_tipcam($tb_pagocomisiones_tipcam){
        $this->tb_pagocomisiones_tipcam = $tb_pagocomisiones_tipcam;
    }

    public function setTb_pagocomisiones_monven($tb_pagocomisiones_monven){
        $this->tb_pagocomisiones_monven = $tb_pagocomisiones_monven;
    }

    public function setTb_pagocomisiones_monipdn($tb_pagocomisiones_monipdn){
        $this->tb_pagocomisiones_monipdn = $tb_pagocomisiones_monipdn;
    }

    public function setTb_pagocomisiones_poliza($tb_pagocomisiones_poliza){
        $this->tb_pagocomisiones_poliza = $tb_pagocomisiones_poliza;
    }

    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_pagocomisiones(
                                        tb_pagocomisiones_fec,
                                        tb_pagocomisiones_usureg,
                                        tb_usuario_id,
                                        tb_vendedor_id,
                                        tb_vehiculomarca_id,
                                        tb_vehiculomodelo_id,
                                        tb_pagocomisiones_tasa,
                                        tb_pagocomisiones_costo,
                                        tb_pagocomisiones_formapago,
                                        tb_pagocomisiones_comision,
                                        tb_pagocomisiones_tipo,
                                        tb_pagocomisiones_estado,
                                        tb_pagocomisiones_his,
                                        tb_moneda_id,
                                        tb_cliente_id,
                                        tb_pagocomisiones_tipcam,
                                        tb_pagocomisiones_poliza)
                                VALUES (
                                        :tb_pagocomisiones_fec,
                                        :tb_pagocomisiones_usureg,
                                        :tb_usuario_id,
                                        :tb_vendedor_id,
                                        :tb_vehiculomarca_id,
                                        :tb_vehiculomodelo_id,
                                        :tb_pagocomisiones_tasa,
                                        :tb_pagocomisiones_costo,
                                        :tb_pagocomisiones_formapago,
                                        :tb_pagocomisiones_comision,
                                        :tb_pagocomisiones_tipo,
                                        :tb_pagocomisiones_estado,
                                        :tb_pagocomisiones_his,
                                        :tb_moneda_id,
                                        :tb_cliente_id,
                                        :tb_pagocomisiones_tipcam,
                                        :tb_pagocomisiones_poliza)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_pagocomisiones_fec", $this->getTb_pagocomisiones_fec(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_usureg", $this->getTb_pagocomisiones_usureg(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $this->getTb_usuario_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_vendedor_id", $this->getTb_vendedor_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_vehiculomarca_id", $this->getTb_vehiculomarca_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_vehiculomodelo_id", $this->getTb_vehiculomodelo_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_tasa", $this->getTb_pagocomisiones_tasa(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_costo", $this->getTb_pagocomisiones_costo(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_formapago", $this->getTb_pagocomisiones_formapago(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_comision", $this->getTb_pagocomisiones_comision(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_tipo", $this->getTb_pagocomisiones_tipo(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_estado", $this->getTb_pagocomisiones_estado(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_his", $this->getTb_pagocomisiones_his(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_moneda_id", $this->getTb_moneda_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cliente_id", $this->getTb_cliente_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_tipcam", $this->getTb_pagocomisiones_tipcam(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_poliza", $this->getTb_pagocomisiones_poliza(), PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_pagocomisiones SET 
                                tb_pagocomisiones_fec =:tb_pagocomisiones_fec,
                                tb_pagocomisiones_usureg=:tb_pagocomisiones_usureg,
                                tb_usuario_id=:tb_usuario_id,
                                tb_vendedor_id=:tb_vendedor_id,
                                tb_vehiculomarca_id=:tb_vehiculomarca_id,
                                tb_vehiculomodelo_id=:tb_vehiculomodelo_id,
                                tb_pagocomisiones_tasa=:tb_pagocomisiones_tasa,
                                tb_pagocomisiones_costo=:tb_pagocomisiones_costo,
                                tb_pagocomisiones_formapago=:tb_pagocomisiones_formapago,
                                tb_pagocomisiones_comision=:tb_pagocomisiones_comision,
                                tb_pagocomisiones_tipo=:tb_pagocomisiones_tipo,
                                tb_pagocomisiones_estado=:tb_pagocomisiones_estado,
                                tb_pagocomisiones_his=CONCAT(tb_pagocomisiones_his,:tb_pagocomisiones_his),
                                tb_moneda_id=:tb_moneda_id,
                                tb_cliente_id=:tb_cliente_id,
                                tb_pagocomisiones_poliza=:tb_pagocomisiones_poliza
                                
                            WHERE 
                                tb_pagocomisiones_id =:tb_pagocomisiones_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_pagocomisiones_fec", $this->getTb_pagocomisiones_fec(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_usureg", $this->getTb_pagocomisiones_usureg(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_usuario_id", $this->getTb_usuario_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_vendedor_id", $this->getTb_vendedor_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_vehiculomarca_id", $this->getTb_vehiculomarca_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_vehiculomodelo_id", $this->getTb_vehiculomodelo_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_tasa", $this->getTb_pagocomisiones_tasa(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_costo", $this->getTb_pagocomisiones_costo(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_formapago", $this->getTb_pagocomisiones_formapago(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_comision", $this->getTb_pagocomisiones_comision(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_tipo", $this->getTb_pagocomisiones_tipo(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_estado", $this->getTb_pagocomisiones_estado(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_his", $this->getTb_pagocomisiones_his(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_moneda_id", $this->getTb_moneda_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cliente_id", $this->getTb_cliente_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":tb_pagocomisiones_poliza", $this->getTb_pagocomisiones_poliza(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_id", $this->getTb_pagocomisiones_id(), PDO::PARAM_INT);
            

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_his() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_pagocomisiones SET 
                                tb_pagocomisiones_his=CONCAT(tb_pagocomisiones_his,:tb_pagocomisiones_his)
                            WHERE 
                                tb_pagocomisiones_id =:tb_pagocomisiones_id";
            $sentencia = $this->dblink->prepare($sql);
           
            $sentencia->bindParam(":tb_pagocomisiones_his", $this->getTb_pagocomisiones_his(), PDO::PARAM_STR);
            $sentencia->bindParam(":tb_pagocomisiones_id", $this->getTb_pagocomisiones_id(), PDO::PARAM_INT);
            

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_pagocomisiones SET tb_pagocomisiones_xac=0 WHERE tb_pagocomisiones_id =:tb_pagocomisiones_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_pagocomisiones_id", $this->getTb_pagocomisiones_id(), PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($pagocomisiones_id) {
        try {
            $sql = "SELECT * FROM tb_pagocomisiones WHERE tb_pagocomisiones_id =:tb_pagocomisiones_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_pagocomisiones_id", $pagocomisiones_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de Comisiones Pagadas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarPoliza($pagocomisiones_poliza,$pagocomisiones_id) {
        $pagocomisiones='';
        
                

        
        if($pagocomisiones_id>0){
            $pagocomisiones=' AND tb_pagocomisiones_id=:tb_pagocomisiones_id';
        }
        try {
            $sql = "SELECT * FROM tb_pagocomisiones WHERE tb_pagocomisiones_poliza =:tb_pagocomisiones_poliza ".$pagocomisiones;

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_pagocomisiones_poliza", $pagocomisiones_poliza, PDO::PARAM_STR);
            if($pagocomisiones_id>0){
            $sentencia->bindParam(":tb_pagocomisiones_id", $pagocomisiones_id, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de Comisiones Pagadas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_pagocomisiones($fecha1,$fecha2) {
        try {
            $sql = "SELECT * FROM 
				tb_pagocomisiones p 
				INNER JOIN tb_usuario u on u.tb_usuario_id=p.tb_usuario_id
                                INNER JOIN tb_vendedor v on v.tb_vendedor_id=p.tb_vendedor_id
                                inner JOIN tb_vehiculomarca m on m.tb_vehiculomarca_id=p.tb_vehiculomarca_id
                                INNER JOIN tb_vehiculomodelo mo on mo.tb_vehiculomodelo_id=p.tb_vehiculomodelo_id
                                INNER JOIN tb_moneda mon on mon.tb_moneda_id=p.tb_moneda_id
                                INNER JOIN tb_cliente c on c.tb_cliente_id=p.tb_cliente_id
                            WHERE 
                                tb_pagocomisiones_fec BETWEEN :fecha1 AND :fecha2 AND tb_pagocomisiones_xac=1 ORDER BY 1 desc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de Comisiones Pagadas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function modificar_campo($tb_pagocomisiones_id, $tb_pagocomisiones_columna, $tb_pagocomisiones_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($tb_pagocomisiones_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_pagocomisiones SET " . $tb_pagocomisiones_columna . " =:tb_pagocomisiones_valor WHERE tb_pagocomisiones_id =:tb_pagocomisiones_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_pagocomisiones_id", $tb_pagocomisiones_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":tb_pagocomisiones_valor", $tb_pagocomisiones_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":tb_pagocomisiones_valor", $tb_pagocomisiones_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    

    function modificar_pago($tb_pagocomisiones_id, $tb_pagocomisiones_columna, $tb_pagocomisiones_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($tb_pagocomisiones_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_pagocomisiones SET " . $tb_pagocomisiones_columna . " = " . $tb_pagocomisiones_columna . " + :tb_pagocomisiones_valor WHERE tb_pagocomisiones_id =:tb_pagocomisiones_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":tb_pagocomisiones_id", $tb_pagocomisiones_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":tb_pagocomisiones_valor", $tb_pagocomisiones_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":tb_pagocomisiones_valor", $tb_pagocomisiones_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

}
