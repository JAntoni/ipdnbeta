


<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title" style="font-family: cambria"><b>Datos del Cliente</b></h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-primary btn-xs" title="Editar" onclick="cliente_form('I', 0)"><i class="fa fa-user-plus"></i></a>
                    <a class="btn btn-warning btn-xs" title="Editar" onclick="cliente_form('M', 0)"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_doc" class="control-label">DNI</label>
                            <input type="text" name="txt_cliente_fil_cre_doc" id="txt_cliente_fil_cre_doc" class="form-control input-sm numero" value="<?php echo $cliente_doc; ?>">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_nom" class="control-label">Cliente</label>
                            <input type="text" name="txt_cliente_fil_cre_nom" id="txt_cliente_fil_cre_nom" class="form-control input-sm mayus" value="<?php echo $cliente_nom; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_tel" class="control-label">Teléfono</label>
                            <input type="text" name="txt_cliente_fil_cre_tel" id="txt_cliente_fil_cre_tel" class="form-control input-sm" value="<?php echo $cliente_tel; ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_cel" class="control-label">Celular</label>
                            <input type="text" name="txt_cliente_fil_cre_cel" id="txt_cliente_fil_cre_cel" class="form-control input-sm" value="<?php echo $cliente_cel; ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Teléfono de Referencia</label>
                            <input type="text" name="txt_cliente_fil_cre_telref" id="txt_cliente_fil_cre_telref" class="form-control input-sm" value="<?php echo $cliente_telref; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--comienzo de los datos del vendecor de la concesionaria y del asesor de credito-->
         <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title" style="font-family: cambria"><b>Datos del Vehiculo y Comisión</b></h3>
                <div class="box-tools pull-right">
<!--                    <a class="btn btn-primary btn-xs" title="Editar" onclick="cliente_form('I', 0)"><i class="fa fa-user-plus"></i></a>
                    <a class="btn btn-warning btn-xs" title="Editar" onclick="cliente_form('M', 0)"><i class="fa fa-edit"></i></a>-->
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Marca</label>
                            <select class="form-control input-sm" id="txt_vehiculomarca_id" name="txt_vehiculomarca_id">
                                <?php require_once '../vehiculomarca/vehiculomarca_select.php';?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Modelo</label>
                            <select class="form-control input-sm" id="txt_vehiculomodelo_id" name="txt_vehiculomodelo_id">
                                <?php require_once '../vehiculomodelo/vehiculomodelo_select.php';?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Tipo</label>
                            <select class="form-control input-sm" id="txt_pagocomisiones_tipo" name="txt_pagocomisiones_tipo">
                                <option value="0">... Seleccionar ...</option>
                                <option value="1" <?php if ($pagocomisiones_tipo == 1) echo "selected";?> >PRE CONSTITUCIÓN</option>
                                <option value="2" <?php if ($pagocomisiones_tipo == 2) echo "selected";?> >CONSTITUCIÓN</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--<p>-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Tasación de Vehículo</label>
                            <input type="text" class="form-control input-sm moneda" id="txt_pagocomisiones_tasa" name="txt_pagocomisiones_tasa" value="<?php echo $pagocomisiones_tasa ?>" style="font-weight: bold;font-size: 20px">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Costo de Poliza</label>
                            <input type="text" class="form-control input-sm moneda" id="txt_pagocomisiones_costo" name="txt_pagocomisiones_costo" value="<?php echo $pagocomisiones_costo ?>" style="font-weight: bold;font-size: 20px">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">N° Poliza</label>
                            <input type="text" class="form-control input-sm mayus" id="txt_pagocomisiones_poliza" name="txt_pagocomisiones_poliza" value="<?php echo $pagocomisiones_poliza ?>" style="font-weight: bold;font-size: 20px">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Comisión</label>
                            <input type="text" class="form-control input-sm moneda" id="txt_pagocomisiones_comision" name="txt_pagocomisiones_comision" value="<?php echo $pagocomisiones_comision ?>" style="font-weight: bold;font-size: 20px">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Moneda</label>
                            <select id="cmb_moneda_id" name="cmb_moneda_id" class="form-control input-sm">
                                    <?php require_once '../moneda/moneda_select.php'; ?>
                            </select>
                            <input type="hidden" class="form-control input-sm moneda" id="txt_pagocomisiones_tipcam" name="txt_pagocomisiones_tipcam" value="<?php echo $pagocomisiones_tipcam ?>" style="font-weight: bold;font-size: 20px">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_telref" class="control-label">Forma de Pago</label>
                            <textarea type="text" class="form-control input-sm mayus" id="txt_pagocomisiones_forma" name="txt_pagocomisiones_forma"><?php echo $pagocomisiones_formapago ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <!--FIN comienzo de los datos del vendecor de la concesionaria y del asesor de credito-->

    </div>

    <div class="col-md-6">
       
        
        
        
        
        
        
        
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title" style="font-family: cambria"><b>Datos del Asesor y Vendedor de Concesionaria</b></h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-primary btn-xs" title="Editar" onclick="vendedor_form('I', 0)"><i class="fa fa-user-plus"></i></a>
                    <a class="btn btn-warning btn-xs" title="Editar" onclick="vendedor_form('M', 0)"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_doc" class="control-label">Asesor de Crédito</label>
                            <select id="cbo_usuario_id" name="cbo_usuario_id" class="form-control input-sm">
                                <?php require_once '../usuario/usuario_select.php'; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha :</label>
                            <div class='input-group date' id='pagocomisiones_fil_picker1'>
                                <input type='text' class="form-control input-sm" name="txt_pagocomisiones_fec" id="txt_pagocomisiones_fec" value="<?php echo $pagocomisiones_fec?>">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_doc" class="control-label">DNI</label>
                            <input type="text" name="txt_vendedor_dni" id="txt_vendedor_dni" class="form-control input-sm numero" value="<?php echo $vendedor_dni; ?>">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_nom" class="control-label">Vendedor</label>
                            <input type="text" name="txt_vendedor_nombre" id="txt_vendedor_nombre" class="form-control input-sm mayus" value="<?php echo $vendedor_nombre; ?>">
                        </div>
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_doc" class="control-label">Teléfono</label>
                            <input type="text" name="txt_vendedor_telefono" id="txt_vendedor_telefono" class="form-control input-sm numero" value="<?php echo $vendedor_tel; ?>">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_nom" class="control-label">Dirección</label>
                            <input type="text" name="txt_vendedor_direccion" id="txt_vendedor_direccion" class="form-control input-sm mayus" value="<?php echo $vendedor_dir; ?>">
                        </div>
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txt_cliente_fil_cre_nom" class="control-label">Consecionaria</label>
                            <input type="text" name="txt_vendedor_tienda" id="txt_vendedor_tienda" class="form-control input-sm mayus" value="<?php echo $vendedor_tienda; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        
        
        
    </div>
</div>

<!--<div class="row">   
    <div class="col-md-6">
        
    </div>
</div>-->
