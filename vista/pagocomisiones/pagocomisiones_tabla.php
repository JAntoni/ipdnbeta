<?php
if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}


require_once('Pagocomisiones.php');
$oPagocomisiones = new Pagocomisiones();

$fecha1 = date('Y-m-01');
$fecha2 = date('Y-m-t');

$fecha1 = (isset($_POST['txt_filtro_fec1'])) ? fecha_mysql($_POST['txt_filtro_fec1']) : date('Y-01-01');
$fecha2 = (isset($_POST['txt_filtro_fec2'])) ? fecha_mysql($_POST['txt_filtro_fec2']) : date('Y-m-d');
//$fecha1=$_POST['fecha1'];
//$fecha2=$_POST['fecha2'];

$result = $oPagocomisiones->listar_pagocomisiones($fecha1, $fecha2);
$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {

        $estado = $value['tb_pagocomisiones_estado'];
        if ($estado == 0) {
            $estados = '<span class="badge bg-green">Registrado</span>';
        }
        if ($estado == 1) {
            $estados = '<span class="badge bg-teal" title="Pagada la Comision al Vendedor">PAGADO</span>';
        }
        if ($estado == 2) {
            $estados = '<span class="badge bg-blue">CAJA IPDN</span>';
        }
        
         $tipo = $value['tb_pagocomisiones_tipo'];

        if ($tipo == 1) {
            $tipos = '<span class="badge bg-green">PRE CONSTITUCIÓN</span>';
        }
        if ($tipo == 2) {
            $tipos = '<span class="badge bg-yellow">CONSTITUCIÓN</span>';
        }
 

        $tr .= '<tr id="tabla_cabecera_fila">';
        $tr .= '
          <td id="tabla_fila">' . $value['tb_pagocomisiones_id'] . '</td>
          <td id="tabla_fila">' . mostrar_fecha($value['tb_pagocomisiones_fec']) . '</td>
          <td id="tabla_fila">' . $value['tb_usuario_ape'] . ' ' . $value['tb_usuario_nom'] . '</td>
          <td id="tabla_fila">' . $value['tb_cliente_nom'] . '</td>
          <td id="tabla_fila">' . $value['tb_vendedor_nombre'] . '</td>
          <td id="tabla_fila">' . $value['tb_vehiculomarca_nom'] . '</td>
          <td id="tabla_fila">' . $value['tb_vehiculomodelo_nom'] . '</td>
          <td id="tabla_fila">' . $value['tb_moneda_nom'] . '</td>
          <td id="tabla_fila">' . $value['tb_pagocomisiones_poliza'] . '</td>
          <td id="tabla_fila">' . mostrar_moneda($value['tb_pagocomisiones_tasa']) . '</td>
          <td id="tabla_fila">' . mostrar_moneda($value['tb_pagocomisiones_costo']) . '</td>
          <td id="tabla_fila">' . mostrar_moneda($value['tb_pagocomisiones_comision']) . '</td>
          <td id="tabla_fila">' . $value['tb_pagocomisiones_formapago'] . '</td>
          <td id="tabla_fila">' . $tipos . '</td>
          <td id="tabla_fila">' . $estados .'</td>';
        $tr .= ' <td id="tabla_fila" align="center">';
//        if($estado == 0){
        $tr .= ' 
            <a href="javascript:void(0)" class="btn btn-warning btn-xs" title="Editar" onclick="pagocomisiones_form(\'M\',' . $value['tb_pagocomisiones_id'] . ')"><i class="fa fa-edit"></i></a> 
            <a href="javascript:void(0)" class="btn btn-danger btn-xs" title="Eliminar" onclick="pagocomisiones_form(\'E\',' . $value['tb_pagocomisiones_id'] . ')"><i class="fa fa-trash"></i></a> ';
//        }
        if($tipo==1&&$estado == 0){
        $tr .= '<a href="javascript:void(0)" class="btn btn-success btn-xs" title="Pagar a Vendedor" onclick="pagocomisiones_pagar(1,'. $value['tb_pagocomisiones_id'] . ')"><i class="fa fa-money"></i></a> ';
        }
        if($tipo==1&&$estado == 1){
        $tr .= '<a href="javascript:void(0)" class="btn btn-success btn-xs" title="Ingresar a Caja" onclick="pagocomisiones_pagar(2,'. $value['tb_pagocomisiones_id'] . ')"><i class="fa fa-money"></i></a> ';
        }
        if($tipo==2&&$estado == 0){
        $tr .= '<a href="javascript:void(0)" class="btn btn-primary btn-xs" title="Ingresar a Caja" onclick="pagocomisiones_pagar(2,'. $value['tb_pagocomisiones_id'] . ')"><i class="fa fa-money"></i></a> ';
        }
        $tr .= '<a href="javascript:void(0)" class="btn btn-facebook btn-xs" onclick="pagocomisiones_his(' . $value['tb_pagocomisiones_id'] . ')" title="HISTORIAL"><i class="fa fa-history"></i></a>
                </td>';
        $tr .= '</tr>';
    }
    $result = null;
}
?>
<table id="tbl_pagocomisiones" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">FECHA</th>
            <th id="tabla_cabecera_fila">ASESOR</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">VENDEDOR DE TIENDA</th>
            <th id="tabla_cabecera_fila">MARCA</th>
            <th id="tabla_cabecera_fila">MODELO</th>
            <th id="tabla_cabecera_fila">MONEDA</th>
            <th id="tabla_cabecera_fila">N° POLIZA</th>
            <th id="tabla_cabecera_fila">TASACIÓN</th>
            <th id="tabla_cabecera_fila">POLISA</th>
            <th id="tabla_cabecera_fila">COMISION</th>
            <th id="tabla_cabecera_fila">FORMA DE PAGO</th>
            <th id="tabla_cabecera_fila">TIPO</th>
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila" width="10%">OPCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>
