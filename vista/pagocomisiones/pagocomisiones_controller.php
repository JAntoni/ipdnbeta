<?php

require_once('../../core/usuario_sesion.php');

require_once('Pagocomisiones.php');
$oPagocomisiones = new Pagocomisiones();

require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();

require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();

require_once '../cliente/Cliente.class.php';
$oCliente = new Cliente();

require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

//echo 'hasta aki estoy entrando normal al sistema';exit();

$action = $_POST['action'];
$tb_pagocomisiones_id = intval($_POST['hdd_pagocomisiones_id']);
$tb_pagocomisiones_fec = fecha_mysql($_POST['txt_pagocomisiones_fec']);
$tb_pagocomisiones_usureg = intval($_SESSION['usuario_id']);
$tb_usuario_id = $_POST['cbo_usuario_id'];
$tb_vendedor_id = $_POST['hdd_vendedor_id'];
$tb_marca_id = $_POST['txt_vehiculomarca_id'];
$tb_modelo_id = $_POST['txt_vehiculomodelo_id'];
$tb_pagocomisiones_tasa = moneda_mysql($_POST['txt_pagocomisiones_tasa']);
$tb_pagocomisiones_costo = moneda_mysql($_POST['txt_pagocomisiones_costo']);
$tb_pagocomisiones_formapago = mb_strtoupper($_POST['txt_pagocomisiones_forma']);
$tb_pagocomisiones_comision = moneda_mysql($_POST['txt_pagocomisiones_comision']);
$tb_pagocomisiones_tipo = intval($_POST['txt_pagocomisiones_tipo']);
$tb_ingreso_id = $_POST['txt_ingreso_id'];
$tb_egreso_id = $_POST['txt_egreso_id'];
$tb_pagocomisiones_poliza = $_POST['txt_pagocomisiones_poliza'];
$tb_pagocomisiones_estado = 0;

//echo '$tb_pagocomisiones_poliza='.$tb_pagocomisiones_poliza.' $tb_pagocomisiones_id='.$tb_pagocomisiones_id;exit();
//if($tb_pagocomisiones_tipo==2){
//    $tb_pagocomisiones_estado = 2;
//}

$tb_moneda_id = $_POST['cmb_moneda_id'];
$tb_cliente_id = $_POST['hdd_cliente_id'];
$tb_pagocomisiones_tipcam = intval($_POST['txt_pagocomisiones_tipcam']);

// echo '$tb_pagocomisiones_id='.$tb_pagocomisiones_id.' $tb_pagocomisiones_fec='.$tb_pagocomisiones_fec.' $tb_pagocomisiones_usureg='.$tb_pagocomisiones_usureg;exit();       

$oPagocomisiones->setTb_pagocomisiones_id($tb_pagocomisiones_id);
$oPagocomisiones->setTb_pagocomisiones_fec($tb_pagocomisiones_fec);
$oPagocomisiones->setTb_pagocomisiones_usureg($tb_pagocomisiones_usureg);
$oPagocomisiones->setTb_usuario_id($tb_usuario_id);
$oPagocomisiones->setTb_vendedor_id($tb_vendedor_id);
$oPagocomisiones->setTb_vehiculomarca_id($tb_marca_id);
$oPagocomisiones->setTb_vehiculomodelo_id($tb_modelo_id);
$oPagocomisiones->setTb_pagocomisiones_tasa($tb_pagocomisiones_tasa);
$oPagocomisiones->setTb_pagocomisiones_costo($tb_pagocomisiones_costo);
$oPagocomisiones->setTb_pagocomisiones_formapago($tb_pagocomisiones_formapago);
$oPagocomisiones->setTb_pagocomisiones_comision($tb_pagocomisiones_comision);
$oPagocomisiones->setTb_pagocomisiones_tipo($tb_pagocomisiones_tipo);
$oPagocomisiones->setTb_ingreso_id($tb_ingreso_id);
$oPagocomisiones->setTb_egreso_id($tb_egreso_id);
$oPagocomisiones->setTb_pagocomisiones_estado($tb_pagocomisiones_estado);
$oPagocomisiones->setTb_moneda_id($tb_moneda_id);
$oPagocomisiones->setTb_cliente_id($tb_cliente_id);
$oPagocomisiones->setTb_pagocomisiones_tipcam($tb_pagocomisiones_tipcam);
$oPagocomisiones->setTb_pagocomisiones_poliza($tb_pagocomisiones_poliza);

if ($tb_moneda_id == 1) {
    $moneda = "S./ ";
} else {
    $moneda = "US$./ ";
}

if ($action == 'insertar') {

    $result = $oPagocomisiones->mostrarPoliza($tb_pagocomisiones_poliza, $tb_pagocomisiones_id);

    if ($result['estado'] == 0) {

        $tb_pagocomisiones_his = '&FilledSmallSquare; EL USUARIO: <b style="color: #0000cc">' . $_SESSION['usuario_nom'] . '</b> HA REGISTRADO EL PAGO DE COMISIÓN PARA EL VENDEDOR <b style="color: #0000cc">' . $_POST['txt_vendedor_nombre'] . '</b> EL DIA <b style="color: #0000cc">' . date('d-m-Y h:i:s') . '</b> CON EL NÚMERO DE POLIZA <b style="color: #0000cc">' . $tb_pagocomisiones_poliza . ' </b>,
                 CON UNA TASACIÓN VEHICULAR DE <b style="color: #0000cc">' . $moneda . ' ' . mostrar_moneda($tb_pagocomisiones_tasa) . '</b>, CON UN COSTO DE POLIZA DE  <b style="color: #0000cc">' . $moneda . ' ' . mostrar_moneda($tb_pagocomisiones_costo) . '</b>, CON UNA COMISIÓN PARA EL VENDEDOR DE  <b style="color: #0000cc">' . $moneda . ' ' . mostrar_moneda($tb_pagocomisiones_comision) . '</b>'
                . ' CON UNA FORMA DE PAGO <b style="color: #0000cc">' . $tb_pagocomisiones_formapago . '.</b><br><br>';
        $oPagocomisiones->setTb_pagocomisiones_his($tb_pagocomisiones_his);

        $data['estado'] = 0;
        $data['mensaje'] = 'Existe un error al guardar el Pago de Comisión.';
        if ($oPagocomisiones->insertar()) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Pago de Comisión registrado correctamente.';
        }
        echo json_encode($data);
    } else {
        $data['estado'] = 0;
        $data['mensaje'] = 'El Número de Poliza ' . $tb_pagocomisiones_poliza . ' ya se encuentra Registrado en el Sistema por favor Verifique';
        echo json_encode($data);
    }
} elseif ($action == 'modificar') {

//    $result = $oPagocomisiones->mostrarPoliza($tb_pagocomisiones_poliza, $tb_pagocomisiones_id);
//
//    if ($result['estado'] == 1) {

        $tb_pagocomisiones_his = '&FilledSmallSquare; EL USUARIO: <b style="color: #0000cc">' . $_SESSION['usuario_nom'] . '</b> HA MODIFICADO EL PAGO DE COMISIÓN PARA EL VENDEDOR <b style="color: #0000cc">' . $_POST['txt_vendedor_nombre'] . '</b> EL DIA <b style="color: #0000cc">' . date('d-m-Y h:i:s') . '</b> CON EL NÚMERO DE POLIZA <b style="color: #0000cc">' . $tb_pagocomisiones_poliza . ' </b>,
             CON UNA TASACIÓN VEHICULAR DE <b style="color: #0000cc">' . $moneda . ' ' . mostrar_moneda($tb_pagocomisiones_tasa) . '</b>, CON UN COSTO DE POLIZA DE  <b style="color: #0000cc">' . $moneda . ' ' . mostrar_moneda($tb_pagocomisiones_costo) . '</b>, CON UNA COMISIÓN PARA EL VENDEDOR DE  <b style="color: #0000cc">' . $moneda . ' ' . mostrar_moneda($tb_pagocomisiones_comision) . '</b>'
                . ' CON UNA FORMA DE PAGO <b style="color: #0000cc">' . $tb_pagocomisiones_formapago . '.</b><br><br>';
        $oPagocomisiones->setTb_pagocomisiones_his($tb_pagocomisiones_his);

        $data['estado'] = 0;
        $data['mensaje'] = 'Existe un error al modificar el Pago de Comisión.';

        if ($oPagocomisiones->modificar()) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Pago de Comisión modificado correctamente. ';
        }
        echo json_encode($data);
//    } else {
//        $data['estado'] = 0;
//        $data['mensaje'] = 'El Número de Poliza ' . $tb_pagocomisiones_poliza . ' ya se encuentra Registrado en el Sistema por favor Verifique';
//        echo json_encode($data);
//    }
} elseif ($action == 'eliminar') {

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el Pago de Comisión.';

    if ($oPagocomisiones->eliminar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Pago de Comisión eliminado correctamente.';
    }
    echo json_encode($data);
} elseif ($action == 'pagar_vendedor') {

    $pagocomisiones_id = $_POST['hdd_pagocomisiones_id'];
    $fecha = fecha_mysql($_POST['txt_pagocomisiones_fec']);
    $monto_pagado = $_POST['txt_pagar'];
    $tipo = $_POST['hdd_pago'];
    $vendedor = $_POST['txt_vendedor_nom'];

    $result = $oPagocomisiones->mostrarUno($pagocomisiones_id);
    if ($result['estado'] == 1) {
        $vendedor_id = $result['data']['tb_vendedor_id'];
        $pagocomisiones_comision = $result['data']['tb_pagocomisiones_comision'];
        $moneda_id = $result['data']['tb_moneda_id'];
        $usuario_id = $result['data']['tb_usuario_id'];
        $cliente_id = $result['data']['tb_cliente_id'];
        $pagocomisiones_poliza = $result['data']['tb_pagocomisiones_poliza'];
    }
    $result = NULL;

    $result = $oCliente->mostrarUno($cliente_id);
    if ($result['estado'] == 1) {
        $cliente_nombre = $result['data']['tb_cliente_nom'];
    }
    $result = NULL;

    if ($tipo == 1) {
        $tb_pagocomisiones_his = '&FilledSmallSquare; EL USUARIO: <b style="color: #0000cc">' . $_SESSION['usuario_nom'] . '</b> HA REALIZADO EL PAGO EL PAGO DE LA COMISIÓN AL  VENDEDOR  <b style="color: #0000cc">' . $vendedor . '</b>'
                . ' el dia <b style="color: #0000cc">' . date('d-m-Y h:i:s') . '</b> POR UN MONTO DE <b style="color: #0000cc">' . $moneda . ' ' . $monto_pagado . '</b> POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° ' . $pagocomisiones_poliza . '</b> <br><br>';
        $oPagocomisiones->setTb_pagocomisiones_his($tb_pagocomisiones_his);
        $oPagocomisiones->setTb_pagocomisiones_id($pagocomisiones_id);
        $oPagocomisiones->modificar_his();

        $pro_id = 18;

        $egr_det = 'EGRESO: PAGO A COLABORADORES EXTERNOS (VENDEDORES DE CONCESIONARIO), COMISIONES DE SEGUROS, CLIENTE: <b style="color: #0000cc"> ' . $cliente_nombre . '</b> - VENDEDOR: <b style="color: #0000cc">' . $vendedor . '</b>  POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° ' . $pagocomisiones_poliza . '</b> ';
        $caj_id = 1;
        $cue_id = 16; //Gastos operativos
        $subcue_id = 151; //Incentivos a colaboradores Externos
        $mod_id = 63; //pago de comisiones a vendedores de concesionarias
        $modide = 0;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = 'OE' . '-';
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cue_id;
        $oEgreso->subcuenta_id = $subcue_id;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = $caj_id;
        $oEgreso->moneda_id = $moneda_id;
        $oEgreso->modulo_id = $mod_id;
        $oEgreso->egreso_modide = $modide;
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si

        $result = $oEgreso->insertar();
        if (intval($result['estado']) == 1) {
            $egreso_id = $result['egreso_id'];
        }
//            $oEgreso->modificar_campo($egreso_id,'tb_egreso_numdoc','OE-'.$egreso_id, 'STR');
        $oPagocomisiones->modificar_campo($pagocomisiones_id, 'tb_egreso_id', $egreso_id, 'INT');
        $oPagocomisiones->modificar_campo($pagocomisiones_id, 'tb_pagocomisiones_estado', 1, 'INT');
        $oPagocomisiones->modificar_pago($pagocomisiones_id, 'tb_pagocomisiones_monven', $monto_pagado, 'INT');
    }
    if ($tipo == 2) {
        $tb_pagocomisiones_his = '&FilledSmallSquare; EL USUARIO: <b style="color: #0000cc">' . $_SESSION['usuario_nom'] . '</b> HA REALIZADO UN INGRESO A CAJA POR CONCEPTO DE COMISIÓN DEL CLIENTE: <b style="color: #0000cc">' . $cliente_nombre . '</b>'
                . ' el dia <b style="color: #0000cc">' . date('d-m-Y h:i:s') . '</b> POR UN MONTO DE <b style="color: #0000cc">' . $moneda . ' ' .$monto_pagado . '</b>  POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° '.$pagocomisiones_poliza.'</b> <br><br>';
        $oPagocomisiones->setTb_pagocomisiones_his($tb_pagocomisiones_his);
        $oPagocomisiones->setTb_pagocomisiones_id($pagocomisiones_id);
        $oPagocomisiones->modificar_his();

        $cue_id = 3;
        $subcue_id = 10;
        $cliente = 1144;
        $mod_id = 63; //pago de comisiones a vendedores de concesionarias
        $modide = 0;
        $ing_det = 'INGRESO A ACAJA POR CONCEPTO DE PAGO DE  COMISIONES DE SEGUROS TODO RIESGO, CLIENTE: <b style="color: #0000cc"> ' . $cliente_nombre . '  POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° '.$pagocomisiones_poliza.'</b><br>';

        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha;
        $oIngreso->documento_id = 8;
        $oIngreso->ingreso_numdoc = 'OI';
        $oIngreso->ingreso_det = $ing_det;
        $oIngreso->ingreso_imp = moneda_mysql($monto_pagado);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id;
        $oIngreso->cliente_id = $cliente;
        $oIngreso->caja_id = 1;
        $oIngreso->moneda_id = $moneda_id;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id;
        $oIngreso->ingreso_modide = $modide;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];

        $result = $oIngreso->insertar();
        if (intval($result['estado']) == 1) {
            $ingreso_id = $result['ingreso_id'];
        }


//        echo 'hasta ki estoy entrando normal al sistema';exit();
        $cuopag_his .= 'El ingreso generado en caja tuvo el siguiente detalle: ' . $ing_det . ' ||&& ';
        $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho la transacción: ' . $ing_det . '. El monto válido fue: S/. ' . mostrar_moneda($monto) . ' && <b>' . date('d-m-Y h:i a') . '</b>';
        $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
        $oLineaTiempo->tb_lineatiempo_det = $line_det;
        $oLineaTiempo->insertar();

        $oPagocomisiones->modificar_campo($pagocomisiones_id, 'tb_ingreso_id', $ingreso_id, 'INT');
        $oPagocomisiones->modificar_campo($pagocomisiones_id, 'tb_pagocomisiones_estado', 2, 'INT');
        $oPagocomisiones->modificar_pago($pagocomisiones_id, 'tb_pagocomisiones_monipdn', $monto_pagado, 'INT');
    }


    $data['estado'] = 1;
    $data['mensaje'] = 'Pago de Comisión Realizado correctamente.';
    echo json_encode($data);
} elseif ($action == 'pagocomisiones_his') {
    $venin_id = $_POST['pagocomisiones_id'];

    $dts = $oPagocomisiones->mostrarUno($venin_id);

    if ($dts['estado'] == 1) {
        $his = $dts['data']['tb_pagocomisiones_his'];
    }

    echo'<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_pagocomisiones_historial">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                      </button>
                      <h4 class="modal-title" style="font-family: cambria;"><b>Historial de Pago de Comisiones</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12" style="font-family: cambria">
                                            ' . $his . '
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                </div>
            </div>';

//	echo $his;
} else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>