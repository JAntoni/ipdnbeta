<?php
require_once ('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('Pagocomisiones.php');
$oPagocomisiones = new Pagocomisiones();
require_once '../vendedor/Vendedor.class.php';
$oVendedor = new Vendedor();
require_once '../cliente/Cliente.class.php';
$oCliente = new Cliente();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'pagocomisiones';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$pagocomisiones_id = $_POST['pagocomisiones_id'];
//$cliente_id=1;
//$vendedor_id=1;

$titulo = '';
if ($usuario_action == 'L')
    $titulo = 'Pago de Comisiones Registrado';
if ($usuario_action == 'I')
    $titulo = 'Registrar Pago de Comisiones';
elseif ($usuario_action == 'M')
    $titulo = 'Editar Pago de Comisiones';
elseif ($usuario_action == 'E')
    $titulo = 'Eliminar Pago de Comisiones';
else
    $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en pagocomisiones
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';
$moneda_id = 2;
$pagocomisiones_fec=date('d-m-Y');

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if (in_array($usuario_action, $array_permisos))
        $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if ($bandera == 0) {
        $usuario_id = $_SESSION['usuario_id'];
        $modulo = 'pagocomisiones';
        $modulo_id = $pagocomisiones_id;
        $tipo_permiso = $usuario_action;
        $estado = 1;

        $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if ($result['estado'] == 1) {
            $bandera = 1; // si tiene el permiso para la accion que desea hacer
        } else {
            $result = NULL;
            echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
            exit();
        }
        $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del pagocomisiones por su ID
    if (intval($pagocomisiones_id) > 0) {
        $result = $oPagocomisiones->mostrarUno($pagocomisiones_id);
        if ($result['estado'] != 1) {
            $mensaje = 'No se ha encontrado ningún registro para el pagocomisiones seleccionado, inténtelo nuevamente.';
            $bandera = 4;
        } else {

            $pagocomisiones_fec= mostrar_fecha($result['data']['tb_pagocomisiones_fec']);
            $usuario_id=$result['data']['tb_usuario_id'];
            $vendedor_id=$result['data']['tb_vendedor_id'];
            $vehiculomarca_id=$result['data']['tb_vehiculomarca_id'];
            $vehiculomodelo_id=$result['data']['tb_vehiculomodelo_id'];
            $pagocomisiones_tasa=$result['data']['tb_pagocomisiones_tasa'];
            $pagocomisiones_costo= mostrar_moneda($result['data']['tb_pagocomisiones_costo']);
            $pagocomisiones_formapago=$result['data']['tb_pagocomisiones_formapago'];
            $pagocomisiones_comision= mostrar_moneda($result['data']['tb_pagocomisiones_comision']);
            $pagocomisiones_tipo=$result['data']['tb_pagocomisiones_tipo'];
            $pagocomisiones_estado=$result['data']['tb_pagocomisiones_estado'];
            $moneda_id=$result['data']['tb_moneda_id'];
            $cliente_id=$result['data']['tb_cliente_id'];
            $pagocomisiones_tipcam=$result['data']['tb_pagocomisiones_tipcam'];
            $pagocomisiones_poliza=$result['data']['tb_pagocomisiones_poliza'];
            
        }
        $result = NULL;
    }
    if (intval($cliente_id) > 0) {
        $result = $oCliente->mostrarUno($cliente_id);
            if ($result['estado'] == 1) {
                $cliente_nom = $result['data']['tb_cliente_nom'];
                $cliente_doc = $result['data']['tb_cliente_doc'];
                $cliente_tel = $result['data']['tb_cliente_tel'];
                $cliente_cel = $result['data']['tb_cliente_cel'];
                $cliente_telref = $result['data']['tb_cliente_telref'];
            }
            $result = NULL;
    }
    if (intval($vendedor_id) > 0) {
        $result = $oVendedor->mostrarUno($vendedor_id);
            if ($result['estado'] == 1) {
                $vendedor_dni = $result['data']['tb_vendedor_dni'];
                $vendedor_nombre = $result['data']['tb_vendedor_nombre'];
                $vendedor_dir = $result['data']['tb_vendedor_dir'];
                $vendedor_tel = $result['data']['tb_vendedor_tel'];
                $vendedor_tienda = $result['data']['tb_vendedor_tienda'];
            }
            $result = NULL;
    }
    
    
} else {
    $mensaje = $result['mensaje'];
    $bandera = 4;
    $result = NULL;
}
?>
<?php if ($bandera == 1): ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_pagocomisiones" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo; ?></h4>
                </div>
                <form id="form_pagocomisiones" method="post">
                    <input type="hidden" name="action" value="<?php echo $action; ?>">
                    <input type="hidden" name="hdd_pagocomisiones_id" value="<?php echo $pagocomisiones_id; ?>">
                    <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
                    <input type="hidden" name="hdd_vendedor_id" id="hdd_vendedor_id" value="<?php echo $vendedor_id;?>">
                    <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $usuario_id;?>">

                    <div class="modal-body">
                        
                        
                        <?php include_once 'pagocomisiones_form_vista.php';?>
                        
                        
                        <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                        <?php if ($action == 'eliminar'): ?>
                            <div class="callout callout-warning">
                                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Moneda?</h4>
                            </div>
                        <?php endif; ?>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="pagocomisiones_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M'): ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_pagocomisiones">Guardar</button>
                            <?php endif; ?>
                            <?php if ($usuario_action == 'E'): ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_pagocomisiones">Aceptar</button>
                            <?php endif; ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($bandera == 4): ?>
    <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_pagocomisiones">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Mensaje Importante</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <p><?php echo $mensaje; ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/pagocomisiones/pagocomisiones_form.js?ver=13131331'; ?>"></script>
