/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
    var usuario_id=$("#hdd_usuario_id").val();
    console.log(usuario_id);
    $("#cbo_usuario_id").val(usuario_id);
    
    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });

    $('#pagocomisiones_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });
});


function vehiculo_modelo_select(marca_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vehiculomodelo/vehiculomodelo_select.php",
        async: true,
        dataType: "html",
        data: ({
            vehiculomarca_id: marca_id
        }),
        beforeSend: function () {
            $('#txt_vehiculomodelo_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#txt_vehiculomodelo_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}


$('#txt_vehiculomarca_id').change(function () {
    var marca_id = $(this).val();
    vehiculo_modelo_select(marca_id);
});


function monedacambio_tipo_cambio() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambio/monedacambio_controller.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'tipocambio',
            monedacambio_fec: $('#txt_credito_feccre').val(),
            moneda_id: $('#cmb_moneda_id').val()
        }),
        beforeSend: function () {
            $('#txt_pagocomisiones_tipcam').val('Consultando...');
        },
        success: function (data) {
            if (parseInt(data.estado) == 1) {
                $('#txt_pagocomisiones_tipcam').val(data.valor);
            } else {
                $('#txt_pagocomisiones_tipcam').val('');
                $('#span_error').show(300);
//                var link = ' <a href="javascript:void(0)" onclick="monedacambio_form(\'I\', 0)" style="color: black;">Registrar TC</a>'
//                $('#span_error').html(data.mensaje + link);
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#span_error').show(300);
            $('#span_error').html('ERROR AL CONSULTAR: ' + data.responseText);
        }
    });
}

$('#cmb_moneda_id').change(function (event) {
    var moneda_id = parseInt($(this).val());
    if (moneda_id == 1 || moneda_id == 2)
        monedacambio_tipo_cambio();
    else
        $('#txt_credito_tipcam').val(1.00);
});


$("#txt_cliente_fil_cre_nom").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $.getJSON(
                VISTA_URL + "cliente/cliente_autocomplete.php",
                {term: request.term, cliente_emp: 1}, //1 solo clientes de la empresa
                response
                );
    },
    select: function (event, ui) {
        $('#hdd_cliente_id').val(ui.item.cliente_id);
        $('#txt_cliente_fil_cre_doc').val(ui.item.cliente_doc);
        $('#txt_cliente_fil_cre_tel').val(ui.item.cliente_tel);
        $('#txt_cliente_fil_cre_cel').val(ui.item.cliente_cel);
        $('#txt_cliente_fil_cre_telref').val(ui.item.cliente_telref);
    }
});
$("#txt_cliente_fil_cre_doc").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $.getJSON(
                VISTA_URL + "cliente/cliente_autocomplete.php",
                {term: request.term, cliente_emp: 1}, //1 solo clientes de la empresa
                response
                );
    },
    select: function (event, ui) {
        event.preventDefault();
        $('#hdd_cliente_id').val(ui.item.cliente_id);
        $('#txt_cliente_fil_cre_doc').val(ui.item.cliente_doc);
        $('#txt_cliente_fil_cre_nom').val(ui.item.value);
        $('#txt_cliente_fil_cre_tel').val(ui.item.cliente_tel);
        $('#txt_cliente_fil_cre_cel').val(ui.item.cliente_cel);
        $('#txt_cliente_fil_cre_telref').val(ui.item.cliente_telref);
    }
});


$("#txt_vendedor_nombre").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $.getJSON(
                VISTA_URL + "vendedor/vendedor_autocomplete.php",
                {term: request.term}, //1 solo clientes de la empresa
                response
                );
    },
    select: function (event, ui) {
        $('#hdd_vendedor_id').val(ui.item.vendedor_id);
        $('#txt_vendedor_dni').val(ui.item.vendedor_dni);
        $('#txt_vendedor_nombre').val(ui.item.value);
        $('#txt_vendedor_telefono').val(ui.item.vendedor_tel);
        $('#txt_vendedor_direccion').val(ui.item.vendedor_dir);
        $('#txt_vendedor_tienda').val(ui.item.vendedor_tie);
    }
});
$("#txt_vendedor_dni").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $.getJSON(
                VISTA_URL + "vendedor/vendedor_autocomplete.php",
                {term: request.term}, //1 solo clientes de la empresa
                response
                );
    },
    select: function (event, ui) {
        event.preventDefault();
        $('#hdd_vendedor_id').val(ui.item.vendedor_id);
        $('#txt_vendedor_dni').val(ui.item.vendedor_dni);
        $('#txt_vendedor_nombre').val(ui.item.value);
        $('#txt_vendedor_telefono').val(ui.item.vendedor_tel);
        $('#txt_vendedor_direccion').val(ui.item.vendedor_dir);
    }
});

function cliente_form(usuario_act, cliente_id) {
    var cliente_fil_cre_id = parseInt($('#hdd_cliente_id').val());
    if (usuario_act == 'M') {
        if (!Number.isInteger(cliente_fil_cre_id) || cliente_fil_cre_id <= 0)
            return false;
        else
            cliente_id = cliente_fil_cre_id;
    }
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cliente/cliente_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            cliente_id: cliente_id,
            vista: 'credito'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            if (data != 'sin_datos') {
                $('#div_modal_cliente_form').html(data);
                $('#modal_registro_cliente').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_cliente'); //funcion encontrada en public/js/generales.js

                //funcion js para limbiar el modal al cerrarlo
                modal_hidden_bs_modal('modal_registro_cliente', 'limpiar'); //funcion encontrada en public/js/generales.js
                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_cliente'); //funcion encontrada en public/js/generales.js
                //funcion js para agregar un ancho automatico al modal, al abrirlo
                modal_width_auto('modal_registro_cliente', 75); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'cliente';
                var div = 'div_modal_cliente_form';
                permiso_solicitud(usuario_act, cliente_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {
            $('#modal_mensaje').modal('hide');
        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            console.log(data.responseText);
        }
    });
}


$('#form_pagocomisiones').validate({
    submitHandler: function () {

        $.ajax({
            type: "POST",
            url: VISTA_URL + "pagocomisiones/pagocomisiones_controller.php",
            async: true,
            dataType: "json",
            data: $("#form_pagocomisiones").serialize(),
            beforeSend: function () {
                $('#vendedor_mensaje').show(400);
//                    $('#btn_guardar_vendedor').prop('disabled', true);
            },
            success: function (data) {
                if (parseInt(data.estado) > 0) {

                    swal_success("CORRECTO", data.mensaje, 3000);
                    pagocomisiones_tabla();
                    setTimeout(function () {
                        $('#modal_registro_pagocomisiones').modal('hide');
                    }, 1000
                            );
                } else {
                    swal_warning("AVISO", data.mensaje, 3000);
                }
            },
            complete: function (data) {
                //console.log(data);
            },
            error: function (data) {
                swal_error("ALERTA", data.responseText);
            }
        });
    },
    rules: {
        hdd_cliente_id: {
            required: true,
            minlength: 1
        },
        txt_vehiculomarca_id: {
            min: 1
        },
        txt_vehiculomodelo_id: {
            min: 1
        },
        txt_pagocomisiones_tipo: {
            min: 1
        },
        txt_pagocomisiones_tasa: {
            required: true
//            min: 1
        },
        txt_pagocomisiones_costo: {
            required: true
//            min: 1
        },
        txt_pagocomisiones_comision: {
            required: true
//            min: 1
        },
        cmb_moneda_id: {
            min: 1
        },
        txt_pagocomisiones_forma: {
            required: true,
            minlength: 5
        },
        cbo_usuario_id: {
            min: 1
        }

    },
    messages: {
        hdd_cliente_id: {
            required: "Ingrese un Cliente para el pago",
            minlength: "Como mínimo el nombre debe tener 1 caracteres"
        },
        txt_vehiculomarca_id: {
            min: 'Selecciona una Marca'
        },
        txt_vehiculomodelo_id: {
            min: 'Selecciona un Modelo'
        },
        txt_pagocomisiones_tipo: {
            min: "Seleccionar un Tipo"
        },
        txt_pagocomisiones_tasa: {
            required: "Ingrese monto de Tasación",
            minlength: "La Tasación del vehiculo debe ser mayor a 0"
        },
        txt_pagocomisiones_costo: {
            required: "Ingrese monto de Costo",
            minlength: "El Costo debe ser mayor a 0"
        },
        txt_pagocomisiones_comision: {
            required: "Ingrese monto de Comisión",
            minlength: "la Comisión debe ser mayor a 0"
        },
        cmb_moneda_id: {
            min: "Debe Seleccionar un tipo de Moneda"
        },
        txt_pagocomisiones_forma: {
            required: "Ingrese Forma de Pago",
            minlength: "el numero de caracteres de la forma de pago debe ser de 5"
        },
        cbo_usuario_id: {
            minlength: "Seleccione el Asesor del Crédito"
        }

    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
});