/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    pagocomisiones_tabla();

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_filtro_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_filtro_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });

});

function pagocomisiones_form(usuario_act, pagocomisiones_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "pagocomisiones/pagocomisiones_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            pagocomisiones_id: pagocomisiones_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_pagocomisiones_form').html(data);
                $('#modal_registro_pagocomisiones').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_pagocomisiones'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_pagocomisiones', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_width_auto('modal_registro_pagocomisiones', 70)
                modal_height_auto('modal_registro_pagocomisiones');
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'pagocomisiones';
                var div = 'div_modal_pagocomisiones_form';
                permiso_solicitud(usuario_act, pagocomisiones_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}

function pagocomisiones_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "pagocomisiones/pagocomisiones_tabla.php",
        async: true,
        dataType: "html",
        data: $('#form_pagocomisiones_filtro').serialize(),
        beforeSend: function () {
            $('#pagocomisiones_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_pagocomisiones_tabla').html(data);
            $('#pagocomisiones_mensaje_tbl').hide(300);
        },
        complete: function (data) {
            estilos_datatable();
        },
        error: function (data) {
            $('#pagocomisiones_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}


function estilos_datatable() {
    datatable_global = $('#tbl_pagocomisiones').DataTable({
        "pageLength": 100,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [13], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}



function pagocomisiones_his(id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "pagocomisiones/pagocomisiones_controller.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'pagocomisiones_his',
            pagocomisiones_id: id
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_pagocomisiones_his').html(html);
            $('#modal_registro_pagocomisiones_historial').modal('show');
            modal_width_auto('modal_registro_pagocomisiones_historial', 20);
            modal_height_auto('modal_registro_pagocomisiones_historial');
        },
        complete: function (data) {

        }
    });
}

function pagocomisiones_pagar(usuario_act, pagocomisiones_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "pagocomisiones/pagocomisiones_pagar.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'pagar_vendedor',
            pago: usuario_act,
            pagocomisiones_id: pagocomisiones_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('#div_modal_pagocomisiones_pagar').html(data);
            $('#modal_pagar_pagocomisiones').modal('show');

            modal_hidden_bs_modal('modal_pagar_pagocomisiones', 'limpiar'); //funcion encontrada en public/js/generales.js
//            modal_width_auto('modal_pagar_pagocomisiones', 70);
            modal_height_auto('modal_pagar_pagocomisiones');

        },
        complete: function (data) {

        },
        error: function (data) {
            swal_error('ERROR!:',data.responseText,3000);
            console.log(data.responseText);
        }
    });
}
