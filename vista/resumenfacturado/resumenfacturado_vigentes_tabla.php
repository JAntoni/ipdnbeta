<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../resumenfacturado/ResumenFacturado.class.php');
  require_once('../ingreso/Ingreso.class.php');
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oResumenFacturado = new ResumenFacturado();
  $oIngreso = new Ingreso();

  $fecha_inicio = '2022-10-20';
  $fecha_fin = '2022-10-20';

  $fecha_hoy = date('Y-m-d');
  $fecha_inicio = (isset($_POST['fecha_inicio']))? fecha_mysql($_POST['fecha_inicio']) : $fecha_inicio;
  $fecha_fin = (isset($_POST['fecha_fin']))? fecha_mysql($_POST['fecha_fin']) : $fecha_fin;
  $tipo_cambio = (isset($_POST['tipo_cambio']))? floatval($_POST['tipo_cambio']) : 1; //tipo cambio del día
  

  $SUMA_TOTAL_SOLES = 0;
  $SUMA_TOTAL_DOLARES = 0;
  $SUMA_TOTAL_CAMBIO = 0;
  $SUMA_TOTAL_COBRADO = 0;
  $PORCENTAJE_DE_SUMAS = 0;
?>
<div class="table-responsive shadow">
  <table id="tbl_resumenfacturado" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>TIPO DE CREDITO</th>
        <th>SOLES S/.</th>
        <th>DOLARES</th>
        <th>TIPO DE CAMBIO</th>
        <th class="info">VALOR TOTAL EN SOLES S/.</th>
        <th class="success">MONTO RECUPERADO</th>
        <th>% RECUPERADO</th>
        <th>RENDIMIENTO</th>
      </tr>
    </thead>
    <tbody id="">
      <?php
        $result = $oResumenFacturado->listar_facturados_vigentes_rango_fechas($fecha_inicio, $fecha_fin);
          if($result['estado'] == 1){
            foreach ($result['data'] as $key => $value) {
              $facturado_cambio = formato_numero($tipo_cambio * $value['tb_deudacredito_inicio_dol']) + formato_numero($value['tb_deudacredito_inicio_sol']);
              $tipo_credito = 'SIN CREDITO';
              $valor_porcentaje = 0;
              $TOTAL_COBRADO_CREDITO = 0;
              $action = '';

              if($value['tb_deudacredito_credito'] == 1){
                $tipo_credito = 'CREDITO MENOR';
                $action = 'menor';
                //CREDITO MENOR TODO LO RECAUDADO DE CUOTAS FACTURADAS EN EL MES ACTUAL - VIGENTES
                $result2 = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha_inicio, $fecha_fin, 'tb_creditomenor', 1);
                  if($result2['estado'] == 1){
                    foreach ($result2['data'] as $key => $value2) {
                      $ingreso_imp = $value2['tb_ingreso_imp'];
                      $cuota_real = $value2['cuota_real'];
                      $ingreso_real = 0;
                      $cuotapago_cambio = $value2['tb_cuotapago_tipcam'];
                      $moneda_id = intval($value2['tb_moneda_id']); //1 soles, 2 dolares

                      if($ingreso_imp >= $cuota_real)
                        $ingreso_real = $cuota_real;
                      else
                        $ingreso_real = $ingreso_imp;

                      if ($moneda_id == 2)
                        $TOTAL_COBRADO_CREDITO += $ingreso_real * $cuotapago_cambio;
                      else
                        $TOTAL_COBRADO_CREDITO += $ingreso_real;
                    }
                  }
                $result2 = NULL;

                $valor_porcentaje = ($TOTAL_COBRADO_CREDITO * 100) / $facturado_cambio;
              }
              if($value['tb_deudacredito_credito'] == 2){
                $tipo_credito = 'ASISTENCIA VEHICULAR';
                $action = 'asistencia';
                //CREDITO ASISTENCIA CUOTAS PAGADAS VIGENTES
                $result2 = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha_inicio, $fecha_fin, 'tb_creditoasiveh', 2);
                  if($result2['estado'] == 1){
                    foreach ($result2['data'] as $key => $value2) {
                      $ingreso_imp = $value2['tb_ingreso_imp'];
                      $cuotapago_cambio = $value2['tb_cuotapago_tipcam'];
                      $moneda_id = intval($value2['tb_moneda_id']); //1 soles, 2 dolares

                      if ($moneda_id == 2)
                        $TOTAL_COBRADO_CREDITO += $ingreso_imp * $cuotapago_cambio;
                      else
                        $TOTAL_COBRADO_CREDITO += $ingreso_imp;
                    }
                  }
                $result2 = NULL;

                $valor_porcentaje = ($TOTAL_COBRADO_CREDITO * 100) / $facturado_cambio;
              }
              if($value['tb_deudacredito_credito'] == 3){
                $tipo_credito = 'GARANTÍA VEHICULAR';
                $action = 'garantia';
                //CREDITO GARVEH CUOTAS PAGADAS VIGENTES
                $result2 = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha_inicio, $fecha_fin, 'tb_creditogarveh', 3);
                  if($result2['estado'] == 1){
                    foreach ($result2['data'] as $key => $value2) {
                      $ingreso_imp = $value2['tb_ingreso_imp'];
                      $cuotapago_cambio = $value2['tb_cuotapago_tipcam'];
                      $moneda_id = intval($value2['tb_moneda_id']); //1 soles, 2 dolares

                      if ($moneda_id == 2)
                        $TOTAL_COBRADO_CREDITO += $ingreso_imp * $cuotapago_cambio;
                      else
                        $TOTAL_COBRADO_CREDITO += $ingreso_imp;
                    }
                  }
                $result2 = NULL;

                $valor_porcentaje = ($TOTAL_COBRADO_CREDITO * 100) / $facturado_cambio;
              }
              if($value['tb_deudacredito_credito'] == 4){
                $tipo_credito = 'CREDITO HIPOTECARIO';
                $action = 'hipotecario';
                //CREDITO HIPO CUOTAS PAGADAS VIGENTES
                $result2 = $oIngreso->ingreso_credito_rango_fecha_vigente($fecha_inicio, $fecha_fin, 'tb_creditohipo', 4);
                  if($result2['estado'] == 1){
                    foreach ($result2['data'] as $key => $value2) {
                      $ingreso_imp = $value2['tb_ingreso_imp'];
                      $cuotapago_cambio = $value2['tb_cuotapago_tipcam'];
                      $moneda_id = intval($value2['tb_moneda_id']); //1 soles, 2 dolares

                      if ($moneda_id == 2)
                        $TOTAL_COBRADO_CREDITO += $ingreso_imp * $cuotapago_cambio;
                      else
                        $TOTAL_COBRADO_CREDITO += $ingreso_imp;
                    }
                  }
                $result2 = NULL;

                $valor_porcentaje = ($TOTAL_COBRADO_CREDITO * 100) / $facturado_cambio;
              }

              $semaforo_porcentaje = '<span class="badge bg-red">'.formato_numero($valor_porcentaje).' %</span>';
              $linea_progreso = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-danger" style="width: '.formato_numero($valor_porcentaje).'%;"></div></div>';

              if($valor_porcentaje >= 30){
                $semaforo_porcentaje = '<span class="badge bg-green">'.formato_numero($valor_porcentaje).' %</span>';
                $linea_progreso = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-success" style="width: '.formato_numero($valor_porcentaje).'%;"></div></div>';
              }
              if($valor_porcentaje >= 15 && $valor_porcentaje < 30){
                $semaforo_porcentaje = '<span class="badge bg-yellow">'.formato_numero($valor_porcentaje).' %</span>';
                $linea_progreso = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-warning" style="width: '.formato_numero($valor_porcentaje).'%;"></div></div>';
              }

              echo '
                <tr>
                  <td><a onclick="recaudo_form_cliente(\''.$action.'\')">'.$tipo_credito.'</a></td>
                  <td>'.mostrar_moneda($value['tb_deudacredito_inicio_sol']).'</td>
                  <td>'.mostrar_moneda($value['tb_deudacredito_inicio_dol']).'</td>
                  <td>'.$tipo_cambio.'</td>
                  <td class="info">'.mostrar_moneda($facturado_cambio).'</td>
                  <td class="success">'.mostrar_moneda($TOTAL_COBRADO_CREDITO).'</td>
                  <td>'.$semaforo_porcentaje.'</td>
                  <td>'.$linea_progreso.'</td>
                </tr>
              ';

              $SUMA_TOTAL_SOLES += formato_numero($value['tb_deudacredito_inicio_sol']);
              $SUMA_TOTAL_DOLARES += formato_numero($value['tb_deudacredito_inicio_dol']);
              $SUMA_TOTAL_CAMBIO += $facturado_cambio;
              $SUMA_TOTAL_COBRADO += $TOTAL_COBRADO_CREDITO;
              $PORCENTAJE_DE_SUMAS = $SUMA_TOTAL_COBRADO / $SUMA_TOTAL_CAMBIO * 100;
            }
          }
        $result = NULL;
      ?>
    <tfoot>
      <tr>
        <th>SUMAS TOTALES</th>
        <th><?php echo mostrar_moneda($SUMA_TOTAL_SOLES);?></th>
        <th><?php echo mostrar_moneda($SUMA_TOTAL_DOLARES);?></th>
        <th><?php echo $tipo_cambio;?></th>
        <th class="info"><?php echo mostrar_moneda($SUMA_TOTAL_CAMBIO);?></th>
        <th class="success"><?php echo mostrar_moneda($SUMA_TOTAL_COBRADO);?></th>
        <th><?php echo formato_numero($PORCENTAJE_DE_SUMAS).' %';?></th>
        <th><div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-info" <?php echo 'style="width: '.formato_numero($PORCENTAJE_DE_SUMAS).'%;"';?>></div></div></th>
      </tr>
    </tfoot>
  </table>
</div>

