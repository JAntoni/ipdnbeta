<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class ResumenFacturado extends Conexion{

    function mostrarUno($area_id){
      try {
        $sql = "SELECT * FROM tb_area WHERE tb_area_id =:area_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":area_id", $area_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_facturados_vigentes_rango_fechas($fecha_inicio, $fecha_fin){
      try {
        $sql = "SELECT * FROM tb_deudacredito 
          WHERE tb_deudacredito_fecha_inicio >= :fecha_inicio 
          AND tb_deudacredito_fecha_fin <= :fecha_fin AND tb_deudacredito_tipo = 'VIGENTES';
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_facturados_vencidas_rango_fechas($fecha_inicio, $fecha_fin){
      try {
        $sql = "SELECT * FROM tb_deudacredito 
          WHERE tb_deudacredito_fecha_inicio >= :fecha_inicio 
          AND tb_deudacredito_fecha_fin <= :fecha_fin AND tb_deudacredito_tipo = 'VENCIDAS';
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
