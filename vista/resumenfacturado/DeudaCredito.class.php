<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class DeudaCredito extends Conexion{
    public $deudacredito_id;
    public $deudacredito_fecha_inicio;
    public $deudacredito_fecha_fin;
    public $deudacredito_inicio_sol;
    public $deudacredito_inicio_dol;
    public $deudacredito_fin_sol;
    public $deudacredito_fin_dol;
    public $deudacredito_credito;
    public $deudacredito_tipo;
    public $deudacredito_cobrado_sol;
    public $deudacredito_cobrado_dol;
    
    function c_menor_cuotas_vencidas_parcial($fecha_inicio){
      try {
        $sql = "SELECT 
          cli.tb_cliente_id,
          tb_cliente_doc, 
          tb_cliente_nom,
          tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          tb_cuota_est,
          tb_cuota_fec,
          cre.tb_credito_preaco,
          cuo.tb_cuota_id,
          tb_cuota_cap,
          tb_cuota_amo,
          tb_cuota_int, 
          tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
          
        FROM tb_creditomenor cre 
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 1)
        INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
        WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
          AND tb_credito_est IN (3,5)
          AND tb_cuota_est in (1,3) AND tb_cuota_fec < :fecha_inicio";
   
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotas vencidas";
          $retorno["data"] = "";
        }

       return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function c_asiveh_cuotas_vencidas_parcial($fecha_inicio){
      try {
        $sql = "SELECT 
          cli.tb_cliente_id,
          tb_cliente_doc, 
          tb_cliente_nom,
          tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          tb_cuota_est,
          tb_cuotadetalle_est,
          tb_cuota_fec,
          cre.tb_credito_preaco,
          tb_cuotadetalle_id,
          cuo.tb_cuota_id,
          tb_cuota_cap,
          tb_cuota_amo,
          tb_cuota_int, 
          tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) as cuota_real
          
        FROM tb_creditoasiveh cre 
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 2)
        INNER JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id
        INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
        WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
          AND tb_credito_est IN (3)
          AND tb_cuotadetalle_est in (1,3) AND tb_cuota_fec < :fecha_inicio";
   
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotas vencidas";
          $retorno["data"] = "";
        }

       return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function c_garveh_cuotas_vencidas_parcial($fecha_inicio){
      try {
        $sql = "SELECT 
          cli.tb_cliente_id,
          tb_cliente_doc, 
          tb_cliente_nom,
          tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          tb_cuota_est,
          tb_cuotadetalle_est,
          tb_cuota_fec,
          cre.tb_credito_preaco,
          tb_cuotadetalle_id,
          cuo.tb_cuota_id,
          tb_cuota_cap,
          tb_cuota_amo,
          tb_cuota_int, 
          tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) as cuota_real
          
        FROM tb_creditogarveh cre 
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 3)
        INNER JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id
        INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
        WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
          AND tb_credito_est IN (3)
          AND tb_cuotadetalle_est in (1,3) AND tb_cuota_fec < :fecha_inicio";
   
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotas vencidas";
          $retorno["data"] = "";
        }

       return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function c_hipo_cuotas_vencidas_parcial($fecha_inicio){
      try {
        $sql = "SELECT 
          cli.tb_cliente_id,
          tb_cliente_doc, 
          tb_cliente_nom,
          tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          tb_cuota_est,
          tb_cuotadetalle_est,
          tb_cuota_fec,
          cre.tb_credito_preaco,
          tb_cuotadetalle_id,
          cuo.tb_cuota_id,
          tb_cuota_cap,
          tb_cuota_amo,
          tb_cuota_int, 
          tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END) as cuota_real
          
        FROM tb_creditohipo cre 
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 4)
        INNER JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id
        INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
        WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
          AND tb_credito_est IN (3)
          AND tb_cuotadetalle_est in (1,3) AND tb_cuota_fec < :fecha_inicio";
   
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotas vencidas";
          $retorno["data"] = "";
        }

       return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function c_menor_facturado_rango_fechas($fecha_filtro1, $fecha_filtro2){
      try {
        
        $sql = '
          SELECT 
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_cel,
            cre.tb_credito_id,
            cre.tb_credito_est,
            cre.tb_moneda_id, 
            tb_cuota_est,
            tb_cuota_fec,
            cre.tb_credito_preaco,
            tb_cuota_cap,
            tb_cuota_amo,
            tb_cuota_int, 
            tb_cuota_cuo,
            cre.tb_cuotatipo_id,
            (CASE WHEN cre.tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
            
          FROM tb_creditomenor cre 
          INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 1)
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
            AND (tb_credito_est IN (3,5) OR (tb_credito_est IN (4,7) AND tb_cuota_est = 2)) 
            AND tb_cuota_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2;
        ';

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function c_asiveh_facturado_rango_fechas($fecha_filtro1, $fecha_filtro2){
      try {
        
        $sql = '
          SELECT 
          cli.tb_cliente_id,
          tb_cliente_doc, 
          tb_cliente_nom,
          tb_cliente_cel,
          cre.tb_credito_id,
          cre.tb_credito_est,
          cre.tb_moneda_id, 
          tb_cuota_est,
          tb_cuota_fec,
          cre.tb_credito_preaco,
          tb_cuota_cap,
          tb_cuota_amo,
          tb_cuota_int, 
          tb_cuota_cuo,
          cre.tb_cuotatipo_id,
          (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
          
        FROM tb_creditoasiveh cre 
        INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 2)
        INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
        WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
          AND (tb_credito_est IN (3) OR (tb_credito_est IN (7,8) AND tb_cuota_est in (2,3))) 
          AND tb_cuota_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2;
        ';

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function c_gaveh_facturado_rango_fechas($fecha_filtro1, $fecha_filtro2){
      try {
        
        $sql = '
          SELECT 
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_cel,
            cre.tb_credito_id,
            cre.tb_credito_est,
            cre.tb_moneda_id, 
            tb_cuota_est,
            tb_cuota_fec,
            cre.tb_credito_preaco,
            tb_cuota_cap,
            tb_cuota_amo,
            tb_cuota_int, 
            tb_cuota_cuo,
            cre.tb_cuotatipo_id,
            (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
          
          FROM tb_creditogarveh cre 
          INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 3)
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
            AND (tb_credito_est IN (3) OR (tb_credito_est IN (7,8) AND tb_cuota_est IN (2,3))) 
            AND tb_cuota_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2;
        ';

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function c_hipo_facturado_rango_fechas($fecha_filtro1, $fecha_filtro2){
      try {
        
        $sql = '
          SELECT 
            cli.tb_cliente_id,
            tb_cliente_doc, 
            tb_cliente_nom,
            tb_cliente_cel,
            cre.tb_credito_id,
            cre.tb_credito_est,
            cre.tb_moneda_id, 
            tb_cuota_est,
            tb_cuota_fec,
            cre.tb_credito_preaco,
            tb_cuota_cap,
            tb_cuota_amo,
            tb_cuota_int, 
            tb_cuota_cuo,
            cre.tb_cuotatipo_id,
            (CASE WHEN cre.tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuota_cuo END) as cuota_real
          
          FROM tb_creditohipo cre 
          INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 4)
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
          WHERE tb_credito_xac = 1 AND tb_cuota_xac = 1 
            AND (tb_credito_est IN (3) OR (tb_credito_est IN (7,8) AND tb_cuota_est IN (2,3))) 
            AND tb_cuota_fec BETWEEN :fecha_filtro1 AND :fecha_filtro2;
        ';

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_filtro1", $fecha_filtro1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LOS CRÉDITO GARVEH";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function guardar_deudacredito_inicio(){
      $this->dblink->beginTransaction();
      try {  
        $sql = "INSERT INTO tb_deudacredito(
            tb_deudacredito_fecha_inicio,
            tb_deudacredito_fecha_fin,
            tb_deudacredito_inicio_sol,
            tb_deudacredito_inicio_dol,
            tb_deudacredito_credito,
            tb_deudacredito_tipo
          )
          VALUES(
            :deudacredito_fecha_inicio,
            :deudacredito_fecha_fin,
            :deudacredito_inicio_sol,
            :deudacredito_inicio_dol,
            :deudacredito_credito,
            :deudacredito_tipo
          );
        "; 
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deudacredito_fecha_inicio", $this->deudacredito_fecha_inicio, PDO::PARAM_STR);
        $sentencia->bindParam(":deudacredito_fecha_fin", $this->deudacredito_fecha_fin, PDO::PARAM_STR);
        $sentencia->bindParam(":deudacredito_inicio_sol", $this->deudacredito_inicio_sol, PDO::PARAM_STR);
        $sentencia->bindParam(":deudacredito_inicio_dol", $this->deudacredito_inicio_dol, PDO::PARAM_STR);
        $sentencia->bindParam(":deudacredito_credito", $this->deudacredito_credito, PDO::PARAM_INT);
        $sentencia->bindParam(":deudacredito_tipo", $this->deudacredito_tipo, PDO::PARAM_STR);

        $result = $sentencia->execute();
        
        $this->dblink->commit();
        
        return $result; //si es correcto el deuda retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function guardar_deudacredito_fin(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_deudacredito SET  
            tb_deudacredito_fin_sol = :deudacredito_fin_sol,
            tb_deudacredito_fin_dol = :deudacredito_fin_dol
          WHERE 
            tb_deudacredito_fecha_fin =:deudacredito_fecha_fin 
            AND tb_deudacredito_credito =:deudacredito_credito 
            AND tb_deudacredito_tipo =:deudacredito_tipo;";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":deudacredito_fin_sol", $this->deudacredito_fin_sol, PDO::PARAM_STR);
        $sentencia->bindParam(":deudacredito_fin_dol", $this->deudacredito_fin_dol, PDO::PARAM_STR);
        $sentencia->bindParam(":deudacredito_fecha_fin", $this->deudacredito_fecha_fin, PDO::PARAM_STR);
        $sentencia->bindParam(":deudacredito_credito", $this->deudacredito_credito, PDO::PARAM_INT);
        $sentencia->bindParam(":deudacredito_tipo", $this->deudacredito_tipo, PDO::PARAM_STR);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function ultimo_registro_periodo($deuda_per){ 
      try {
         $sql = "SELECT * FROM tb_deuda WHERE tb_deuda_per =:tb_deuda_per ORDER BY tb_deuda_id DESC LIMIT 1;";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_per", $deuda_per, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function ultimo_mes($fecha){ 
      try {
         $sql = "SELECT * FROM tb_deuda WHERE tb_deuda_per = 1 AND DATE(tb_deuda_reg) = :tb_deuda_reg ORDER BY tb_deuda_id DESC LIMIT 1;";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_reg", $fecha, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    // function guardar_por_vencer(){
    //   $this->dblink->beginTransaction();
    //   try {
    //         $sql = "UPDATE tb_deuda SET  
    //                                 tb_deuda_vensol = :tb_deuda_vensol, 
    //                                 tb_deuda_vendol = :tb_deuda_vendol
    //                             WHERE 
    //                                 tb_deuda_id =:tb_deuda_id;";
            
    //     $sentencia = $this->dblink->prepare($sql);
    //     $sentencia->bindParam(":tb_deuda_vensol", $this->tb_deuda_vensol, PDO::PARAM_STR);
    //     $sentencia->bindParam(":tb_deuda_vendol", $this->tb_deuda_vendol, PDO::PARAM_STR);
    //     $sentencia->bindParam(":tb_deuda_id", $this->tb_afps_apor2, PDO::PARAM_INT);
        
    //     $result = $sentencia->execute();
    //     $this->dblink->commit();
    //     return $result; //si es correcto retorna 1
    //   } catch (Exception $e) {
    //     $this->dblink->rollBack();
    //     throw $e;
    //   }
    // }
    
    function eliminar($deuda_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_deuda WHERE tb_deuda_id = :tb_deuda_id"; 

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_id", $deuda_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarTodos($fecha1, $fecha2){
      try {
         $sql = "SELECT * FROM tb_deuda WHERE DATE(tb_deuda_reg) BETWEEN :fecha1 AND :fecha2;";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarUno($id){
      try {
         $sql="SELECT 
                        * 
                FROM 
                        tb_deuda
                WHERE 
                        tb_deuda_id=:tb_deuda_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_deuda_id", $id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
    function modificar_campo($deuda_id, $deuda_columna, $deuda_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($deuda_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_deuda SET ".$deuda_columna." =:deuda_valor WHERE tb_deuda_id =:deuda_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":deuda_id", $deuda_id, PDO::PARAM_INT);
          if ($param_tip == 'INT') {
                    $sentencia->bindParam(":deuda_valor", $deuda_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":deuda_valor", $deuda_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el deuda retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
}