function resumenfacturado_vigentes_tabla(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php

  if(tipo_cambio <= 0){
    alerta_warning('Importante', 'Ingresa el tipo de cambio del día por favor para poder realizar los cálculos en Soles.');
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"resumenfacturado/resumenfacturado_vigentes_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha_inicio: $('#txt_filtro_fec1').val(),
      fecha_fin: $('#txt_filtro_fec2').val(),
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function() {
      $('#load_tabla_vigentes').show(300); 
    },
    success: function(data){
      $('#load_tabla_vigentes').hide(300);
      $('#box_tabla_vigentes').html(data);

      resumenfacturado_vencidas_tabla();
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      alerta_error('ERROR', data.responseText);
    }
  });
}

function resumenfacturado_vencidas_tabla(){
  var tipo_cambio = Number($('#hdd_tipo_cambio_venta').val()); //se encuentra en templates/header.php

  if(tipo_cambio <= 0){
    alerta_warning('Importante', 'Ingresa el tipo de cambio del día por favor para poder realizar los cálculos en Soles.');
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL+"resumenfacturado/resumenfacturado_vencidas_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha_inicio: $('#txt_filtro_fec1').val(),
      fecha_fin: $('#txt_filtro_fec2').val(),
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function() {
      $('#load_tabla_vencidas').show(300); 
    },
    success: function(data){
      $('#load_tabla_vencidas').hide(300);
      $('#box_tabla_vencidas').html(data);
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      alerta_error('ERROR', data.responseText);
    }
  });
}


$(document).ready(function() {
  console.log('Perfil menu 44');

  resumenfacturado_vigentes_tabla()

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
});