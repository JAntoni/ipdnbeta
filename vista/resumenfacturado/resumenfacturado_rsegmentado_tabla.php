<?php 
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  require_once('../reversiones/Reversiones.class.php');
  require_once ("../cliente/Cliente.class.php");

  $oReversiones = new Reversiones();
  $oCliente = new Cliente();

  $tipo_cambio  = $_POST['tipo_cambio'];
  $fecha01      = fecha_mysql($_POST['fecha01']);
  $fecha02      = fecha_mysql($_POST['fecha02']);

  $result = $oReversiones->lista_reversiones_segmentado($fecha01,$fecha02,0,0,0);
    if($result['estado'] == 1){
      
      $sum_vert_soles        = 0;
      $sum_vert_dolares      = 0;      
      $sum_monto_cobrado01   = 0; $sum_monto_cobrado02   = 0; $sum_monto_cobrado03   = 0; $sum_monto_cobrado04   = 0; $sum_monto_cobrado_total   = 0;
      $sum_monto_facturado01 = 0; $sum_monto_facturado02 = 0; $sum_monto_facturado03 = 0; $sum_monto_facturado04 = 0; $sum_monto_facturado_total = 0;
      $sum_tramo_01_pendiente_sol = 0.00; $sum_tramo_01_pendiente_dol = 0.00; $sum_tramo_01_pago_parcial_sol = 0.00; $sum_tramo_01_pago_parcial_dol = 0.00; $sum_tramo_01_pagado_sol = 0.00; $sum_tramo_01_pagado_dol = 0.00;
      $sum_tramo_02_pendiente_sol = 0.00; $sum_tramo_02_pendiente_dol = 0.00; $sum_tramo_02_pago_parcial_sol = 0.00; $sum_tramo_02_pago_parcial_dol = 0.00; $sum_tramo_02_pagado_sol = 0.00; $sum_tramo_02_pagado_dol = 0.00;
      $sum_tramo_03_pendiente_sol = 0.00; $sum_tramo_03_pendiente_dol = 0.00; $sum_tramo_03_pago_parcial_sol = 0.00; $sum_tramo_03_pago_parcial_dol = 0.00; $sum_tramo_03_pagado_sol = 0.00; $sum_tramo_03_pagado_dol = 0.00;
      $sum_tramo_04_pendiente_sol = 0.00; $sum_tramo_04_pendiente_dol = 0.00; $sum_tramo_04_pago_parcial_sol = 0.00; $sum_tramo_04_pago_parcial_dol = 0.00; $sum_tramo_04_pagado_sol = 0.00; $sum_tramo_04_pagado_dol = 0.00;
      $bandera_verde         = 'badge bg-green';
      $bandera_amarillo      = 'badge bg-yellow';
      $bandera_roja          = 'badge bg-red';
      
      foreach ($result['data'] as $key => $value) {
        $moneda       = intval($value['tb_moneda_id']);
        $monto        = formato_numero($value['cuota_real']); // $monto = formato_numero($value['tb_cuotadetalle_cuo']);
        $cuota_estado = intval($value['tb_cuota_est']);
        $cuota_fecha  = fecha_mysql($value['tb_cuota_fec']);
        $cuota_numero = intval($value['tb_cuota_num']);
        
        if( $cuota_numero >= 1 && $cuota_numero <= 3 )
        {
          if($cuota_estado==1) // PENDIENTE
          {
            if($moneda==1) { 
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += $monto;
              $sum_tramo_01_pendiente_sol    += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += ($tipo_cambio * $monto);
              $sum_tramo_01_pendiente_dol    += $monto;
            } 
            else{ 
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += 0;
              $sum_tramo_01_pendiente_sol    += 0;
              $sum_tramo_01_pendiente_dol    += 0;
            }
          }
          elseif($cuota_estado==2) // PAGADO
          {
            if($moneda==1) { 
              $sum_monto_cobrado01           += $monto;
              $sum_monto_facturado01         += $monto;
              $sum_tramo_01_pagado_sol       += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado01           += ($tipo_cambio * $monto);
              $sum_monto_facturado01         += ($tipo_cambio * $monto);
              $sum_tramo_01_pagado_dol       += $monto;
            } 
            else{ 
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += 0;
              $sum_tramo_01_pagado_sol       += 0;
              $sum_tramo_01_pagado_dol       += 0;
            }
          }
          elseif($cuota_estado==3) // PAGO PARCIAL
          {
            if($moneda==1) { 
              $sum_monto_cobrado01           += $monto;
              $sum_monto_facturado01         += $monto;
              $sum_tramo_01_pago_parcial_sol += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado01           += ($tipo_cambio * $monto);
              $sum_monto_facturado01         += ($tipo_cambio * $monto);
              $sum_tramo_01_pago_parcial_dol += $monto;
            } 
            else{ 
              $sum_monto_cobrado01           += 0;
              $sum_monto_facturado01         += 0;
              $sum_tramo_01_pago_parcial_sol += 0;
              $sum_tramo_01_pago_parcial_dol += 0;
            }
          }
          else
          { 
            $sum_monto_cobrado01             += 0;
            $sum_monto_facturado01           += 0;
            $sum_tramo_01_pendiente_sol      += 0;
            $sum_tramo_01_pendiente_dol      += 0;
            $sum_tramo_01_pagado_sol         += 0;
            $sum_tramo_01_pagado_dol         += 0;
            $sum_tramo_01_pago_parcial_sol   += 0;
            $sum_tramo_01_pago_parcial_dol   += 0;
          }
        }
        elseif( $cuota_numero >= 4 && $cuota_numero <= 9 )
        {
          if($cuota_estado==1) // PENDIENTE
          {
            if($moneda==1) { 
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += $monto;
              $sum_tramo_02_pendiente_sol    += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += ($tipo_cambio * $monto);
              $sum_tramo_02_pendiente_dol    += $monto;
            } 
            else{ 
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += 0;
              $sum_tramo_02_pendiente_sol    += 0;
              $sum_tramo_02_pendiente_dol    += 0;
            }
          }
          elseif($cuota_estado==2) // PAGADO
          {
            if($moneda==1) { 
              $sum_monto_cobrado02           += $monto;
              $sum_monto_facturado02         += $monto;
              $sum_tramo_02_pagado_sol       += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado02           += ($tipo_cambio * $monto);
              $sum_monto_facturado02         += ($tipo_cambio * $monto);
              $sum_tramo_02_pagado_dol       += $monto;
            } 
            else{ 
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += 0;
              $sum_tramo_02_pagado_sol       += 0;
              $sum_tramo_02_pagado_dol       += 0;
            }
          }
          elseif($cuota_estado==3) // PAGO PARCIAL
          {
            if($moneda==1) { 
              $sum_monto_cobrado02           += $monto;
              $sum_monto_facturado02         += $monto;
              $sum_tramo_02_pago_parcial_sol += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado02           += ($tipo_cambio * $monto);
              $sum_monto_facturado02         += ($tipo_cambio * $monto);
              $sum_tramo_02_pago_parcial_dol += $monto;
            } 
            else{ 
              $sum_monto_cobrado02           += 0;
              $sum_monto_facturado02         += 0;
              $sum_tramo_02_pago_parcial_sol += 0;
              $sum_tramo_02_pago_parcial_dol += 0;
            }
          }
          else
          { 
            $sum_monto_cobrado02             += 0;
            $sum_monto_facturado02           += 0;
            $sum_tramo_02_pendiente_sol      += 0;
            $sum_tramo_02_pendiente_dol      += 0;
            $sum_tramo_02_pagado_sol         += 0;
            $sum_tramo_02_pagado_dol         += 0;
            $sum_tramo_02_pago_parcial_sol   += 0;
            $sum_tramo_02_pago_parcial_dol   += 0;
          }
        }
        elseif( $cuota_numero >= 10 && $cuota_numero <= 18 )
        {
          if($cuota_estado==1) // PENDIENTE
          {
            if($moneda==1) { 
              $sum_monto_cobrado03           += 0;
              $sum_monto_facturado03         += $monto;
              $sum_tramo_03_pendiente_sol    += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado03           += 0;
              $sum_monto_facturado03         += ($tipo_cambio * $monto);
              $sum_tramo_03_pendiente_dol    += $monto;
            } 
            else{ 
              $sum_monto_cobrado03           += 0;
              $sum_monto_facturado03         += 0;
              $sum_tramo_03_pendiente_sol    += 0;
              $sum_tramo_03_pendiente_dol    += 0;
            }
          }
          elseif($cuota_estado==2) // PAGADO
          {
            if($moneda==1) { 
              $sum_monto_cobrado03           += $monto;
              $sum_monto_facturado03         += $monto;
              $sum_tramo_03_pagado_sol       += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado03           += ($tipo_cambio * $monto);
              $sum_monto_facturado03         += ($tipo_cambio * $monto);
              $sum_tramo_03_pagado_dol       += $monto;
            } 
            else{ 
              $sum_monto_cobrado03           += 0;
              $sum_monto_facturado03         += 0;
              $sum_tramo_03_pagado_sol       += 0;
              $sum_tramo_03_pagado_dol       += 0;
            }
          }
          elseif($cuota_estado==3) // PAGO PARCIAL
          {
            if($moneda==1) { 
              $sum_monto_cobrado03           += $monto;
              $sum_monto_facturado03         += $monto;
              $sum_tramo_03_pago_parcial_sol += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado03           += ($tipo_cambio * $monto);
              $sum_monto_facturado03         += ($tipo_cambio * $monto);
              $sum_tramo_03_pago_parcial_dol += $monto;
            } 
            else{ 
              $sum_monto_cobrado03           += 0;
              $sum_monto_facturado03         += 0;
              $sum_tramo_03_pago_parcial_sol += 0;
              $sum_tramo_03_pago_parcial_dol += 0;
            }
          }
          else
          { 
            $sum_monto_cobrado03             += 0;
            $sum_monto_facturado03           += 0;
            $sum_tramo_03_pendiente_sol      += 0;
            $sum_tramo_03_pendiente_dol      += 0;
            $sum_tramo_03_pagado_sol         += 0;
            $sum_tramo_03_pagado_dol         += 0;
            $sum_tramo_03_pago_parcial_sol   += 0;
            $sum_tramo_03_pago_parcial_dol   += 0;
          }
        }
        elseif( $cuota_numero >= 19 && $cuota_numero <= 72 )
        {
          if($cuota_estado==1) // PENDIENTE
          {
            if($moneda==1) { 
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += $monto;
              $sum_tramo_04_pendiente_sol    += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += ($tipo_cambio * $monto);
              $sum_tramo_04_pendiente_dol    += $monto;
            } 
            else{ 
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += 0;
              $sum_tramo_04_pendiente_sol    += 0;
              $sum_tramo_04_pendiente_dol    += 0;
            }
          }
          elseif($cuota_estado==2) // PAGADO
          {
            if($moneda==1) { 
              $sum_monto_cobrado04           += $monto;
              $sum_monto_facturado04         += $monto;
              $sum_tramo_04_pagado_sol       += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado04           += ($tipo_cambio * $monto);
              $sum_monto_facturado04         += ($tipo_cambio * $monto);
              $sum_tramo_04_pagado_dol       += $monto;
            } 
            else{ 
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += 0;
              $sum_tramo_04_pagado_sol       += 0;
              $sum_tramo_04_pagado_dol       += 0;
            }
          }
          elseif($cuota_estado==3) // PAGO PARCIAL
          {
            if($moneda==1) { 
              $sum_monto_cobrado04           += $monto;
              $sum_monto_facturado04         += $monto;
              $sum_tramo_04_pago_parcial_sol += $monto;
            } 
            elseif($moneda==2) {
              $sum_monto_cobrado04           += ($tipo_cambio * $monto);
              $sum_monto_facturado04         += ($tipo_cambio * $monto);
              $sum_tramo_04_pago_parcial_dol += $monto;
            } 
            else{ 
              $sum_monto_cobrado04           += 0;
              $sum_monto_facturado04         += 0;
              $sum_tramo_04_pago_parcial_sol += 0;
              $sum_tramo_04_pago_parcial_dol += 0;
            }
          }
          else
          { 
            $sum_monto_cobrado04             += 0;
            $sum_monto_facturado04           += 0;
            $sum_tramo_04_pendiente_sol      += 0;
            $sum_tramo_04_pendiente_dol      += 0;
            $sum_tramo_04_pagado_sol         += 0;
            $sum_tramo_04_pagado_dol         += 0;
            $sum_tramo_04_pago_parcial_sol   += 0;
            $sum_tramo_04_pago_parcial_dol   += 0;
          }
        }
        else
        {
          $sum_monto_cobrado01           += 0;
          $sum_monto_facturado01         += 0;
          $sum_tramo_01_pendiente_sol    += 0;
          $sum_tramo_01_pendiente_dol    += 0;
          $sum_tramo_01_pagado_sol       += 0;
          $sum_tramo_01_pagado_dol       += 0;
          $sum_tramo_01_pago_parcial_sol += 0;
          $sum_tramo_01_pago_parcial_dol += 0;
          
          $sum_monto_cobrado02           += 0;
          $sum_monto_facturado02         += 0;
          $sum_tramo_02_pendiente_sol    += 0;
          $sum_tramo_02_pendiente_dol    += 0;
          $sum_tramo_02_pagado_sol       += 0;
          $sum_tramo_02_pagado_dol       += 0;
          $sum_tramo_02_pago_parcial_sol += 0;
          $sum_tramo_02_pago_parcial_dol += 0;
          
          $sum_monto_cobrado03           += 0;
          $sum_monto_facturado03         += 0;
          $sum_tramo_03_pendiente_sol    += 0;
          $sum_tramo_03_pendiente_dol    += 0;
          $sum_tramo_03_pagado_sol       += 0;
          $sum_tramo_03_pagado_dol       += 0;
          $sum_tramo_03_pago_parcial_sol += 0;
          $sum_tramo_03_pago_parcial_dol += 0;
          
          $sum_monto_cobrado04           += 0;
          $sum_monto_facturado04         += 0;
          $sum_tramo_04_pendiente_sol    += 0;
          $sum_tramo_04_pendiente_dol    += 0;
          $sum_tramo_04_pagado_sol       += 0;
          $sum_tramo_04_pagado_dol       += 0;
          $sum_tramo_04_pago_parcial_sol += 0;
          $sum_tramo_04_pago_parcial_dol += 0;
        }
      }

      $sum_monto_cobrado_total   = formato_numero(($sum_monto_cobrado01 + $sum_monto_cobrado02 + $sum_monto_cobrado03 + $sum_monto_cobrado04));
      $sum_monto_facturado_total = formato_numero(($sum_monto_facturado01 + $sum_monto_facturado02 + $sum_monto_facturado03 + $sum_monto_facturado04));
      
      $sum_vert_soles
        = formato_numero(
          ($sum_tramo_01_pendiente_sol+$sum_tramo_01_pagado_sol+$sum_tramo_01_pago_parcial_sol) +
          ($sum_tramo_02_pendiente_sol+$sum_tramo_02_pagado_sol+$sum_tramo_02_pago_parcial_sol) +
          ($sum_tramo_03_pendiente_sol+$sum_tramo_03_pagado_sol+$sum_tramo_03_pago_parcial_sol) +
          ($sum_tramo_04_pendiente_sol+$sum_tramo_04_pagado_sol+$sum_tramo_04_pago_parcial_sol)
        );
        
      $sum_vert_dolares
        = formato_numero(
          ($sum_tramo_01_pendiente_dol+$sum_tramo_01_pagado_dol+$sum_tramo_01_pago_parcial_dol) +
          ($sum_tramo_02_pendiente_dol+$sum_tramo_02_pagado_dol+$sum_tramo_02_pago_parcial_dol) +
          ($sum_tramo_03_pendiente_dol+$sum_tramo_03_pagado_dol+$sum_tramo_03_pago_parcial_dol) +
          ($sum_tramo_04_pendiente_dol+$sum_tramo_04_pagado_dol+$sum_tramo_04_pago_parcial_dol)
        );

      $cal_porcent_recup_1_3    = 0.00; $draw_porcentaje_recup_1_3    = ''; $draw_linea_progreso_1_3    = '';
      $cal_porcent_recup_4_9    = 0.00; $draw_porcentaje_recup_4_9    = ''; $draw_linea_progreso_4_9    = '';
      $cal_porcent_recup_10_18  = 0.00; $draw_porcentaje_recup_10_18  = ''; $draw_linea_progreso_10_18  = '';
      $cal_porcent_recup_19_mas = 0.00; $draw_porcentaje_recup_19_mas = ''; $draw_linea_progreso_19_mas = '';

      if( $sum_monto_cobrado01 > 0 )
        $cal_porcent_recup_1_3 = formato_numero(( $sum_monto_cobrado01 / $sum_monto_facturado01 ) * 100);
      else
        $cal_porcent_recup_1_3 = formato_numero(0);

      if($cal_porcent_recup_1_3 >= 0.00 && $cal_porcent_recup_1_3 <= 59.99){
        $draw_porcentaje_recup_1_3 = '<span class="'.$bandera_roja.'">'.$cal_porcent_recup_1_3.' %</span>';
        $draw_linea_progreso_1_3   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-red" style="width: '.formato_numero($cal_porcent_recup_1_3).'%;"></div></div>';
      }elseif($cal_porcent_recup_1_3 >= 60.00 && $cal_porcent_recup_1_3 <= 89.99){
        $draw_porcentaje_recup_1_3 = '<span class="'.$bandera_amarillo.'">'.$cal_porcent_recup_1_3.' %</span>';
        $draw_linea_progreso_1_3   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-yellow" style="width: '.formato_numero($cal_porcent_recup_1_3).'%;"></div></div>';
      }else{
        $draw_porcentaje_recup_1_3 = '<span class="'.$bandera_verde.'">'.$cal_porcent_recup_1_3.' %</span>';
        $draw_linea_progreso_1_3   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-success" style="width: '.formato_numero($cal_porcent_recup_1_3).'%;"></div></div>';
      }


      if( $sum_monto_cobrado02 > 0 )
        $cal_porcent_recup_4_9 = formato_numero(( $sum_monto_cobrado02 / $sum_monto_facturado02 ) * 100);
      else
        $cal_porcent_recup_4_9 = formato_numero(0);

      if($cal_porcent_recup_4_9 >= 0.00 && $cal_porcent_recup_4_9 <= 59.99){
        $draw_porcentaje_recup_4_9 = '<span class="'.$bandera_roja.'">'.$cal_porcent_recup_4_9.' %</span>';
        $draw_linea_progreso_4_9   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-red" style="width: '.formato_numero($cal_porcent_recup_4_9).'%;"></div></div>';
      }elseif($cal_porcent_recup_4_9 >= 60.00 && $cal_porcent_recup_4_9 <= 89.99){
        $draw_porcentaje_recup_4_9 = '<span class="'.$bandera_amarillo.'">'.$cal_porcent_recup_4_9.' %</span>';
        $draw_linea_progreso_4_9   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-yellow" style="width: '.formato_numero($cal_porcent_recup_4_9).'%;"></div></div>';
      }else{
        $draw_porcentaje_recup_4_9 = '<span class="'.$bandera_verde.'">'.$cal_porcent_recup_4_9.' %</span>';
        $draw_linea_progreso_4_9   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-success" style="width: '.formato_numero($cal_porcent_recup_4_9).'%;"></div></div>';
      }

      
      if( $sum_monto_cobrado03 > 0 )
        $cal_porcent_recup_10_18 = formato_numero(( $sum_monto_cobrado03 / $sum_monto_facturado03 ) * 100);
      else
        $cal_porcent_recup_10_18 = formato_numero(0);

      if($cal_porcent_recup_10_18 >= 0.00 && $cal_porcent_recup_10_18 <= 59.99){
        $draw_porcentaje_recup_10_18 = '<span class="'.$bandera_roja.'">'.$cal_porcent_recup_10_18.' %</span>';
        $draw_linea_progreso_10_18   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-red" style="width: '.formato_numero($cal_porcent_recup_10_18).'%;"></div></div>';
      }elseif($cal_porcent_recup_10_18 >= 60.00 && $cal_porcent_recup_10_18 <= 89.99){
        $draw_porcentaje_recup_10_18 = '<span class="'.$bandera_amarillo.'">'.$cal_porcent_recup_10_18.' %</span>';
        $draw_linea_progreso_10_18   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-yellow" style="width: '.formato_numero($cal_porcent_recup_10_18).'%;"></div></div>';
      }else{
        $draw_porcentaje_recup_10_18 = '<span class="'.$bandera_verde.'">'.$cal_porcent_recup_10_18.' %</span>';
        $draw_linea_progreso_10_18   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-success" style="width: '.formato_numero($cal_porcent_recup_10_18).'%;"></div></div>';
      }


      if( $sum_monto_cobrado04 > 0 )
        $cal_porcent_recup_19_mas = formato_numero(( $sum_monto_cobrado04 / $sum_monto_facturado04 ) * 100);
      else
        $cal_porcent_recup_19_mas = formato_numero(0);

      if($cal_porcent_recup_19_mas >= 0.00 && $cal_porcent_recup_19_mas <= 59.99){
        $draw_porcentaje_recup_19_mas = '<span class="'.$bandera_roja.'">'.$cal_porcent_recup_19_mas.' %</span>';
        $draw_linea_progreso_19_mas   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-red" style="width: '.formato_numero($cal_porcent_recup_19_mas).'%;"></div></div>';
      }elseif($cal_porcent_recup_19_mas >= 60.00 && $cal_porcent_recup_19_mas <= 89.99){
        $draw_porcentaje_recup_19_mas = '<span class="'.$bandera_amarillo.'">'.$cal_porcent_recup_19_mas.' %</span>';
        $draw_linea_progreso_19_mas   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-yellow" style="width: '.formato_numero($cal_porcent_recup_19_mas).'%;"></div></div>';
      }else{
        $draw_porcentaje_recup_19_mas = '<span class="'.$bandera_verde.'">'.$cal_porcent_recup_19_mas.' %</span>';
        $draw_linea_progreso_19_mas   = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-success" style="width: '.formato_numero($cal_porcent_recup_19_mas).'%;"></div></div>';
      }


      $total_promedio_porcent_recup = formato_numero(0);
      if(($cal_porcent_recup_1_3+$cal_porcent_recup_4_9+$cal_porcent_recup_10_18+$cal_porcent_recup_19_mas) > 0 ) 
        $total_promedio_porcent_recup = formato_numero(($cal_porcent_recup_1_3+$cal_porcent_recup_4_9+$cal_porcent_recup_10_18+$cal_porcent_recup_19_mas)/4);
      else
        $total_promedio_porcent_recup = formato_numero(0);

      $draw_linea_progreso_promedio_porcent_recup = '<div class="progress progress-xs progress-striped active"><div class="progress-bar progress-bar-aqua" style="width: '.formato_numero($total_promedio_porcent_recup).'%;"></div></div>';
    
    }
    $result = null;
?>
<!-- <button type="button" class="btn btn-info" onclick="listar_reversiones_segmentadas_tabla()">ACTUALIZAR</button> -->
<div class="table-responsive">
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th style="vertical-align: middle;" class="text-center">RANGO</th>
        <th style="vertical-align: middle;" class="text-center">SOLES S/</th>
        <th style="vertical-align: middle;" class="text-center">DOLARES</th>
        <th style="vertical-align: middle;" class="text-center">TIPO DE CAMBIO</th>
        <th style="vertical-align: middle;" class="text-center info">TOTAL FACTURADO</th>
        <th style="vertical-align: middle;" class="text-center success">MONTO RECUPERADO</th>
        <th style="vertical-align: middle;" class="text-center">% RECUPERADO</th>
        <th style="vertical-align: middle;" class="text-center">RENDIMIENTO</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="vertical-align: middle;" class="text-center">1 - 3 cuotas</td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_01_pendiente_sol+$sum_tramo_01_pago_parcial_sol+$sum_tramo_01_pagado_sol);?></td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_01_pendiente_dol+$sum_tramo_01_pago_parcial_dol+$sum_tramo_01_pagado_dol);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $tipo_cambio;?></td>
        <td style="vertical-align: middle;" class="text-right info"><?php echo mostrar_moneda($sum_monto_facturado01);?></td>
        <td style="vertical-align: middle;" class="text-right success"><?php echo mostrar_moneda($sum_monto_cobrado01);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $draw_porcentaje_recup_1_3;?></td>
        <td style="vertical-align: middle;"><?php echo $draw_linea_progreso_1_3;?></td>
      </tr>
      <tr>
        <td style="vertical-align: middle;" class="text-center">4 - 9 cuotas</td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_02_pendiente_sol+$sum_tramo_02_pago_parcial_sol+$sum_tramo_02_pagado_sol);?></td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_02_pendiente_dol+$sum_tramo_02_pago_parcial_dol+$sum_tramo_02_pagado_dol);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $tipo_cambio;?></td>
        <td style="vertical-align: middle;" class="text-right info"><?php echo mostrar_moneda($sum_monto_facturado02);?></td>
        <td style="vertical-align: middle;" class="text-right success"><?php echo mostrar_moneda($sum_monto_cobrado02);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $draw_porcentaje_recup_4_9;?></td>
        <td style="vertical-align: middle;"><?php echo $draw_linea_progreso_4_9;?></td>
      </tr>
      <tr>
        <td style="vertical-align: middle;" class="text-center">10 - 18 cuotas</td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_03_pendiente_sol+$sum_tramo_03_pago_parcial_sol+$sum_tramo_03_pagado_sol);?></td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_03_pendiente_dol+$sum_tramo_03_pago_parcial_dol+$sum_tramo_03_pagado_dol);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $tipo_cambio;?></td>
        <td style="vertical-align: middle;" class="text-right info"><?php echo mostrar_moneda($sum_monto_facturado03);?></td>
        <td style="vertical-align: middle;" class="text-right success"><?php echo mostrar_moneda($sum_monto_cobrado03);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $draw_porcentaje_recup_10_18;?></td>
        <td style="vertical-align: middle;"><?php echo $draw_linea_progreso_10_18;?></td>
      </tr>
      <tr>
        <td style="vertical-align: middle;" class="text-center">19 a más cuotas</td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_04_pendiente_sol+$sum_tramo_04_pago_parcial_sol+$sum_tramo_04_pagado_sol);?></td>
        <td style="vertical-align: middle;" class="text-right"><?php echo mostrar_moneda($sum_tramo_04_pendiente_dol+$sum_tramo_04_pago_parcial_dol+$sum_tramo_04_pagado_dol);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $tipo_cambio;?></td>
        <td style="vertical-align: middle;" class="text-right info"><?php echo mostrar_moneda($sum_monto_facturado04);?></td>
        <td style="vertical-align: middle;" class="text-right success"><?php echo mostrar_moneda($sum_monto_cobrado04);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $draw_porcentaje_recup_19_mas;?></td>
        <td style="vertical-align: middle;"><?php echo $draw_linea_progreso_19_mas;?></td>
      </tr>
    </tbody>
    <tfoot>
      <tr class="text-bold text-right">
        <td style="vertical-align: middle;" class="text-center">TOTALES</td>
        <td style="vertical-align: middle;"><?php echo mostrar_moneda($sum_vert_soles);?></td>
        <td style="vertical-align: middle;"><?php echo mostrar_moneda($sum_vert_dolares);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $tipo_cambio;?></td>
        <td style="vertical-align: middle;" class="info"><?php echo mostrar_moneda($sum_monto_facturado_total);?></td>
        <td style="vertical-align: middle;" class="success"><?php echo mostrar_moneda($sum_monto_cobrado_total);?></td>
        <td style="vertical-align: middle;" class="text-center"><?php echo $total_promedio_porcent_recup;?> %</td>
        <td style="vertical-align: middle;"><?php echo $draw_linea_progreso_promedio_porcent_recup;?></td>
      </tr>
    </tfoot>
  </table>
</div>