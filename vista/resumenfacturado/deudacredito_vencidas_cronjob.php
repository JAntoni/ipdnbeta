<?php
  require_once ("DeudaCredito.class.php");
  require_once('../ingreso/Ingreso.class.php');
  $oDeuda = new DeudaCredito();
  $oIngreso = new Ingreso();

  require_once("../funciones/funciones.php");
  require_once("../funciones/fechas.php");

  $date = new DateTime(date('Y-m-d'));
  $date->modify('last day of this month');

  $fecha_inicio = date('Y-m-01'); // el inicio del mes actual, todas las cutoas vencidas serán menores a esta fecha
  $fecha_hoy = date('Y-m-d'); //día en que se está ejecutando este archivo
  $fecha_fin = $date->format('Y-m-d'); //este sería el último día de la fecha actual en que se ejecuta este archivo
  $hora = date('h');
  //echo 'inicio: '.$fecha_inicio.'/ hoy: '.$fecha_hoy.' / fin: '.$fecha_fin.' hor '.intval($hora); exit();
  $MENOR_CUOTAS_VENCIDAS_SOLES = 0;

  $ASIVEH_CUOTAS_VENCIDAS_SOLES = 0;
  $ASIVEH_CUOTAS_VENCIDAS_DOLARES = 0;

  $GARVEH_CUOTAS_VENCIDAS_SOLES = 0;
  $GARVEH_CUOTAS_VENCIDAS_DOLARES = 0;

  $HIPO_CUOTAS_VENCIDAS_SOLES = 0;
  $HIPO_CUOTAS_VENCIDAS_DOLARES = 0;

  $bandera = 0;

  // $fecha_hoy = '2022-11-01';
  // $fecha_inicio = '2022-11-01';
  // $fecha_fin = '2022-11-30';
  // $hora = 5;

  if($fecha_hoy == $fecha_inicio && intval($hora) < 6) //inicio de mes, guardamos todo lo vencido a inicio del mes
    $bandera = 1;
  elseif($fecha_hoy == $fecha_fin && intval($hora) > 21) //fin de mes, guardamos todo lo vencido a fin de mes
    $bandera = 1;
  else
    $bandera = 0;

  $file = fopen("estado_cron.txt", "a");

  if($bandera == 0){
    echo 'No es tiempo de guardar datos VENCIDOS '.$bandera;
    fwrite($file, 'No es tiempo de guardar datos VENCIDOS | '.date('d-m-Y h:i a') . PHP_EOL);
    fclose($file);
    exit();
  }

  //valores que se repiten para todos los créditos:
  $oDeuda->deudacredito_fecha_inicio = $fecha_inicio;
  $oDeuda->deudacredito_fecha_fin = $fecha_fin;
  $oDeuda->deudacredito_tipo = 'VENCIDAS';

  //? LISTA DE CUOTAS VENCIDAS DE CREDITO MENOR
  $result = $oDeuda->c_menor_cuotas_vencidas_parcial($fecha_inicio);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        
        $tipo_cuota_pago = 1; // 1 es para pago de cuota
        $cuota_id = intval($value['tb_cuota_id']);
        $cuota_real = floatval($value['cuota_real']);

        if(intval($value['tb_cuota_est']) == 3){
          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
            if($result2['estado'] == 1){
              $pago_parcial = floatval($result2['data']['importe_total']);
              $cuota_real = $cuota_real - $pago_parcial;
            }
          $result2 = NULL;
        }

        $MENOR_CUOTAS_VENCIDAS_SOLES += $cuota_real;
      }

      //guardamos las cuotas vencidas del crédito menor
      $oDeuda->deudacredito_inicio_sol = $MENOR_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_inicio_dol = 0; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $MENOR_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_fin_dol = 0; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 1; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;

  $mensaje_menor = 'Se guardó las cuotas vencidas de C-Menor | '.date('d-m-Y h:i a');

  //*LISTA DE CUOTAS VENCIDAS DE CRÉDITO ASISTENCIA VEHICULAR, INCLUYENDO ADENDAS Y ACUERDOS DE PAGO
  $result = $oDeuda->c_asiveh_cuotas_vencidas_parcial($fecha_inicio);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        
        $tipo_cuota_pago = 2; // 2 es para pago de cuota detalle
        $cuota_id = intval($value['tb_cuotadetalle_id']);
        $cuota_real = floatval($value['cuota_real']);

        if(intval($value['tb_cuotadetalle_est']) == 3){
          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
            if($result2['estado'] == 1){
              $pago_parcial = floatval($result2['data']['importe_total']);
              $cuota_real = $cuota_real - $pago_parcial;
            }
          $result2 = NULL;
        }
        if(intval($value['tb_moneda_id']) == 2)
          $ASIVEH_CUOTAS_VENCIDAS_DOLARES += $cuota_real;
        else
          $ASIVEH_CUOTAS_VENCIDAS_SOLES += $cuota_real;
      }
      //guardamos las cuotas vencidas del crédito menor
      $oDeuda->deudacredito_inicio_sol = $ASIVEH_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_inicio_dol = $ASIVEH_CUOTAS_VENCIDAS_DOLARES; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $ASIVEH_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_fin_dol = $ASIVEH_CUOTAS_VENCIDAS_DOLARES; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 2; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;

  $mensaje_asiveh = 'Se guardó las cuotas vencidas de ASIVEH | '.date('d-m-Y h:i a');

  //! LISTA DE CUOTAS VENCIDAS DE CRÉDITO GARANTÍA VEHICULAR, INCLUYENDO ADENDAS Y ACUERDOS DE PAGO
  $result = $oDeuda->c_garveh_cuotas_vencidas_parcial($fecha_inicio);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        
        $tipo_cuota_pago = 2; // 2 es para pago de cuota detalle
        $cuota_id = intval($value['tb_cuotadetalle_id']);
        $cuota_real = floatval($value['cuota_real']);

        if(intval($value['tb_cuotadetalle_est']) == 3){
          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
            if($result2['estado'] == 1){
              $pago_parcial = floatval($result2['data']['importe_total']);
              $cuota_real = $cuota_real - $pago_parcial;
            }
          $result2 = NULL;
        }
        if(intval($value['tb_moneda_id']) == 2)
          $GARVEH_CUOTAS_VENCIDAS_DOLARES += $cuota_real;
        else
          $GARVEH_CUOTAS_VENCIDAS_SOLES += $cuota_real;
      }
      //guardamos las cuotas vencidas del crédito menor
      $oDeuda->deudacredito_inicio_sol = $GARVEH_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_inicio_dol = $GARVEH_CUOTAS_VENCIDAS_DOLARES; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $GARVEH_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_fin_dol = $GARVEH_CUOTAS_VENCIDAS_DOLARES; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 3; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;

  $mensaje_garveh = 'Se guardó las cuotas vencidas de GARVEH | '.date('d-m-Y h:i a');

  //todo LISTA DE CUOTAS VENCIDAS DE CRÉDITO HIPOTECARIO, INCLUYENDO ADENDAS Y ACUERDOS DE PAGO
  $result = $oDeuda->c_hipo_cuotas_vencidas_parcial($fecha_inicio);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        
        $tipo_cuota_pago = 2; // 2 es para pago de cuota detalle
        $cuota_id = intval($value['tb_cuotadetalle_id']);
        $cuota_real = floatval($value['cuota_real']);

        if(intval($value['tb_cuotadetalle_est']) == 3){
          $result2 = $oIngreso->mostrar_importe_total_cuota($cuota_id, $tipo_cuota_pago, $value['tb_moneda_id']);
            if($result2['estado'] == 1){
              $pago_parcial = floatval($result2['data']['importe_total']);
              $cuota_real = $cuota_real - $pago_parcial;
            }
          $result2 = NULL;
        }
        if(intval($value['tb_moneda_id']) == 2)
          $HIPO_CUOTAS_VENCIDAS_DOLARES += $cuota_real;
        else
          $HIPO_CUOTAS_VENCIDAS_SOLES += $cuota_real;
      }
      //guardamos las cuotas vencidas del crédito menor
      $oDeuda->deudacredito_inicio_sol = $HIPO_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_inicio_dol = $HIPO_CUOTAS_VENCIDAS_DOLARES; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $HIPO_CUOTAS_VENCIDAS_SOLES;
      $oDeuda->deudacredito_fin_dol = $HIPO_CUOTAS_VENCIDAS_DOLARES; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 4; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;

  $mensaje_hipo =  'Se guardó las cuotas vencidas de HIPO | '.date('d-m-Y h:i a');

  
  fwrite($file, $mensaje_menor . PHP_EOL);
  fwrite($file, $mensaje_asiveh . PHP_EOL);
  fwrite($file, $mensaje_garveh . PHP_EOL);
  fwrite($file, $mensaje_hipo . PHP_EOL);
  fclose($file);
?>