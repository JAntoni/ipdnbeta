<?php
  require_once('../ingreso/Ingreso.class.php');
  require_once ("DeudaCredito.class.php");
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oDeuda = new DeudaCredito();
  $oIngreso = new Ingreso();

  $date = new DateTime(date('Y-m-d'));
  $date->modify('last day of this month');

  $fecha_hoy = date('Y-m-d'); //día en que se está ejecutando este archivo
  $fecha_inicio = date('Y-m-01'); // el inicio del mes actual, todas las cutoas vencidas serán menores a esta fecha
  $fecha_fin = $date->format('Y-m-d'); //este sería el último día de la fecha actual en que se ejecuta este archivo
  $hora = date('h');
  
  $FACTURADO_SOLES_MENOR = 0;

  $FACTURADO_SOLES_ASIVEH = 0;
  $FACTURADO_DOLARES_ASIVEH = 0;
  $FACTURADO_CAMBIO_ASIVEH = 0;

  $FACTURADO_SOLES_GARVEH = 0;
  $FACTURADO_DOLARES_GARVEH = 0;
  $FACTURADO_CAMBIO_GARVEH = 0;

  $FACTURADO_SOLES_HIPO = 0;
  $FACTURADO_DOLARES_HIPO = 0;
  $FACTURADO_CAMBIO_HIPO = 0;

  $bandera = 0;

  // $fecha_hoy = '2022-11-01';
  // $fecha_inicio = '2022-11-01';
  // $fecha_fin = '2022-11-30';
  // $hora = 5;

  if($fecha_hoy == $fecha_inicio && intval($hora) < 6) //inicio de mes, guardamos todo lo vencido a inicio del mes
    $bandera = 1;
  elseif($fecha_hoy == $fecha_fin && intval($hora) > 21) //fin de mes, guardamos todo lo vencido a fin de mes
    $bandera = 1;
  else
    $bandera = 0;

  $file = fopen("estado_cron.txt", "a");

  if($bandera == 0){
    echo 'No es tiempo de guardar datos VIGENTES '.$bandera; 
    fwrite($file, 'No es tiempo de guardar datos VIGENTES | '.date('d-m-Y h:i a') . PHP_EOL);
    fclose($file);
    exit();
  }
  /*
    Es importante realtar que cuando se factura una cuota y se paga, luego se liquida el crédito; esta cuota ya no solía aparecer en cuota facturada por cobrar, por ello en estas funciones se está agregando dichas cuotas facturadas pagadas en el estado del crédito de RESUELTO O LIQUIDADO
  */

  //valores que se repiten para todos los créditos:
  $oDeuda->deudacredito_fecha_inicio = $fecha_inicio;
  $oDeuda->deudacredito_fecha_fin = $fecha_fin;
  $oDeuda->deudacredito_tipo = 'VIGENTES';

  //TODO LISTA DE CUOTAS FACTURADAS PARA CREDITO MENOR
  $result = $oDeuda->c_menor_facturado_rango_fechas($fecha_inicio, $fecha_fin);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value){
        $FACTURADO_SOLES_MENOR += formato_numero($value['cuota_real']);
      }

      //guardamos las cuotas vencidas del crédito menor
      $oDeuda->deudacredito_inicio_sol = $FACTURADO_SOLES_MENOR;
      $oDeuda->deudacredito_inicio_dol = 0; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $FACTURADO_SOLES_MENOR;
      $oDeuda->deudacredito_fin_dol = 0; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 1; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;

  $mensaje_menor = 'Se guardó las cuotas VIGENTES de C-Menor | '.date('d-m-Y h:i a');
  
  //* LISTA DE CUOTAS FACTURADAS PARA CREDITO ASIVEH EN ESTE RANGO DE FECHAS
  $result = $oDeuda->c_asiveh_facturado_rango_fechas($fecha_inicio, $fecha_fin);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value){
        if($value['tb_moneda_id'] == 2)
          $FACTURADO_DOLARES_ASIVEH += formato_numero($value['cuota_real']);
        else
          $FACTURADO_SOLES_ASIVEH += formato_numero($value['cuota_real']);
      }
      //guardamos las cuotas VIGENTES del crédito ASIVEH
      $oDeuda->deudacredito_inicio_sol = $FACTURADO_SOLES_ASIVEH;
      $oDeuda->deudacredito_inicio_dol = $FACTURADO_DOLARES_ASIVEH; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $FACTURADO_SOLES_ASIVEH;
      $oDeuda->deudacredito_fin_dol = $FACTURADO_DOLARES_ASIVEH; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 2; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;

  $mensaje_asiveh = 'Se guardó las cuotas VIGENTES de ASIVEH | '.date('d-m-Y h:i a');

  //? LISTA DE CUOTAS FACTURADAS PARA CREDITO GARVEH EN ESTE RANGO DE FECHAS
  $result = $oDeuda->c_gaveh_facturado_rango_fechas($fecha_inicio, $fecha_fin);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value){
        if($value['tb_moneda_id'] == 2)
          $FACTURADO_DOLARES_GARVEH += formato_numero($value['cuota_real']);
        else
          $FACTURADO_SOLES_GARVEH += formato_numero($value['cuota_real']);
      }
      //guardamos las cuotas VIGENTES del crédito GARVEH
      $oDeuda->deudacredito_inicio_sol = $FACTURADO_SOLES_GARVEH;
      $oDeuda->deudacredito_inicio_dol = $FACTURADO_DOLARES_GARVEH; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $FACTURADO_SOLES_GARVEH;
      $oDeuda->deudacredito_fin_dol = $FACTURADO_DOLARES_GARVEH; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 3; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;
  
  $mensaje_garveh = 'Se guardó las cuotas VIGENTES de GARVEH | '.date('d-m-Y h:i a');

  //! LISTA DE CUOTAS FACTURADAS PARA CREDITO HIPOTECARIO EN ESTE RANGO DE FECHAS
  $result = $oDeuda->c_hipo_facturado_rango_fechas($fecha_inicio, $fecha_fin);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value){
        if($value['tb_moneda_id'] == 2)
          $FACTURADO_DOLARES_HIPO += formato_numero($value['cuota_real']);
        else
          $FACTURADO_SOLES_HIPO += formato_numero($value['cuota_real']);
      }
      //guardamos las cuotas VIGENTES del crédito HIPO
      $oDeuda->deudacredito_inicio_sol = $FACTURADO_SOLES_HIPO;
      $oDeuda->deudacredito_inicio_dol = $FACTURADO_DOLARES_HIPO; //menor no tiene dolares
      $oDeuda->deudacredito_fin_sol = $FACTURADO_SOLES_HIPO;
      $oDeuda->deudacredito_fin_dol = $FACTURADO_DOLARES_HIPO; //menor no tiene dolares
      $oDeuda->deudacredito_credito = 4; //credito menor es de tipo 1

      if($fecha_hoy == $fecha_inicio && intval($hora) < 6)
        $oDeuda->guardar_deudacredito_inicio(); //guardamos suma de cuotas vencidas de c-menor de inicio del mes
      if($fecha_hoy == $fecha_fin && intval($hora) > 21)
        $oDeuda->guardar_deudacredito_fin(); //guardamos suma de cuotas vencidas de c-menor al final de mes
    }
  $result = NULL;

  $mensaje_hipo =  'Se guardó las cuotas VIGENTES de HIPO | '.date('d-m-Y h:i a');

    fwrite($file, $mensaje_menor . PHP_EOL);
    fwrite($file, $mensaje_asiveh . PHP_EOL);
    fwrite($file, $mensaje_garveh . PHP_EOL);
    fwrite($file, $mensaje_hipo . PHP_EOL);
  fclose($file);
?>