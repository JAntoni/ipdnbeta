<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
			font-weight: bold;
		}
		div.dataTables_filter label{
			width: 80%;
		}
	</style>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<?php require_once('resumenfacturado_filtro.php');?>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<!-- REPORTE FACTURADO EN RANGO DE FECHAS-->
						<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">FACTURADO VIGENTE EN EL MES ACTUAL</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="box-body">
								<div class="" id="load_tabla_vigentes" style="margin: auto; text-align: center; display:none;">
									<h4>
										<p>Cargando datos de Cobro...</p>
										<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>
									</h4>
								</div>

								<div id="box_tabla_vigentes"></div>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<!-- REPORTE FACTURADO EN RANGO DE FECHAS-->
						<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">FACTURADO VENCIDO HASTA LA FECHA</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="box-body">
								<div class="" id="load_tabla_vencidas" style="margin: auto; text-align: center; display:none;">
									<h4>
										<p>Cargando datos de Cobro...</p>
										<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>
									</h4>
								</div>

								<div id="box_tabla_vencidas"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- REPORTE FACTURADO GARANTIA VEHICULAR-->
				<div id="div_garveh_tabla">
					<?php //require_once('resumenfacturado_garveh_tabla.php');?>
				</div>

			</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
