<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../colaborador/Colaborador.class.php');
  $oColaborador = new Colaborador();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'usuario';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $colaboradorhorario_id = $_POST['colaboradorhorario_id'];
  $usuario_id = $_POST['usuario_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Horario Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Insertar un Horario';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Horario';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Horario';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en usuario
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $session_usuario_id = $_SESSION['usuario_id']; $modulo = 'usuario'; $modulo_id = $usuario_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($session_usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del usuario por su ID
    if(intval($colaboradorhorario_id) > 0){
      $result = $oColaborador->mostrarUno_horario($colaboradorhorario_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el horadio seleccionado.';
          $bandera = 4;
        }
        else{
          $colaboradorhorario_fecini = mostrar_fecha($result['data']['tb_colaboradorhorario_fecini']); 
          $colaboradorhorario_fecfin = mostrar_fecha($result['data']['tb_colaboradorhorario_fecfin']);
          $colaboradorhorario_ing1 = $result['data']['tb_colaboradorhorario_ing1'];
          $colaboradorhorario_sal1 = $result['data']['tb_colaboradorhorario_sal1'];
          $colaboradorhorario_ing2 = $result['data']['tb_colaboradorhorario_ing2'];
          $colaboradorhorario_sal2 = $result['data']['tb_colaboradorhorario_sal2'];
          $colaboradorhorario_hor = $result['data']['tb_colaboradorhorario_hor'];
          $colaboradorhorario_tur = $result['data']['tb_colaboradorhorario_tur'];
          $colaboradorhorario_est = $result['data']['tb_colaboradorhorario_est'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_colaborador_horario" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_colaboradorhorario" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_usuario_id" value="<?php echo $usuario_id;?>">
          <input type="hidden" name="hdd_colaborador_tipo" value="horario">
          <input type="hidden" name="hdd_colaboradorhorario_id" value="<?php echo $colaboradorhorario_id;?>">
          
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-6 col-sm-6">
            		<div class="form-group">
		              <label for="txt_colaboradorhorario_fecini" class="control-label">Fecha de Inicio</label>
		              <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="txt_colaboradorhorario_fecini" id="txt_colaboradorhorario_fecini" value="<?php echo $colaboradorhorario_fecini;?>" />
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
		            </div>       
		            <div class="form-group">
		              <label for="txt_colaboradorhorario_ing1" class="control-label">Ingreso Turno Mañana</label>
		              <input type="text" name="txt_colaboradorhorario_ing1" id="txt_colaboradorhorario_ing1" class="form-control input-sm timepicker" value="<?php echo $colaboradorhorario_ing1;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_colaboradorhorario_ing2" class="control-label">Ingreso Turno Tarde</label>
		              <input type="text" name="txt_colaboradorhorario_ing2" id="txt_colaboradorhorario_ing2" class="form-control input-sm timepicker" value="<?php echo $colaboradorhorario_ing2;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_colaboradorhorario_hor" class="control-label">Total Horas</label>
		              <input type="text" name="txt_colaboradorhorario_hor" id="txt_colaboradorhorario_hor" class="form-control input-sm" value="<?php echo $colaboradorhorario_hor;?>">
		            </div>
                <div class="form-group">
                  <label for="cmb_colaboradorhorario_est" class="control-label">Estado del Horario</label>
                  <select name="cmb_colaboradorhorario_est" id="cmb_colaboradorhorario_est" class="form-control input-sm">
                    <option value="0"></option>
                    <option value="1" <?php if(intval($colaboradorhorario_est)==1)echo 'selected';?> >Vigente</option>
                  </select>
                </div>
            	</div>
            	<!-- GRUPO DERECHO-->
            	<div class="col-md-6 col-sm-6">
            		<div class="form-group">
		              <label for="txt_colaboradorhorario_fecfin" class="control-label">Fecha Fin</label>
		              <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="txt_colaboradorhorario_fecfin" id="txt_colaboradorhorario_fecfin" value="<?php echo $colaboradorhorario_fecfin;?>"/>
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
		            </div>
		            <div class="form-group">
		              <label for="txt_colaboradorhorario_sal1" class="control-label">Salida Turno Mañana</label>
		              <input type="text" name="txt_colaboradorhorario_sal1" id="txt_colaboradorhorario_sal1" class="form-control input-sm timepicker" value="<?php echo $colaboradorhorario_sal1;?>">
		            </div>
		            <div class="form-group">
		              <label for="txt_colaboradorhorario_sal2" class="control-label">Salida Turno Tarde</label>
		              <input type="text" name="txt_colaboradorhorario_sal2" id="txt_colaboradorhorario_sal2" class="form-control input-sm timepicker" value="<?php echo $colaboradorhorario_sal2;?>">
		            </div>
		            <div class="form-group">
		              <label for="cmb_colaboradorhorario_tur" class="control-label">Tipo de Turno</label>
		              <select name="cmb_colaboradorhorario_tur" id="cmb_colaboradorhorario_tur" class="form-control input-sm">
		                <option value=""></option>
                    <option value="1" <?php if(intval($colaboradorhorario_tur)==1)echo 'selected';?> >Full Time</option>
                    <option value="2" <?php if(intval($colaboradorhorario_tur)==2)echo 'selected';?> >Part Time Mañana</option>
                    <option value="3" <?php if(intval($colaboradorhorario_tur)==3)echo 'selected';?> >Part Time Tarde</option>
		              </select>
		            </div>
            	</div>
            </div>
          
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Horario?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="colaborador_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_colaborador_horario">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_colaborador_horario">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_usuario">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/colaborador/colaborador_horario_form.js';?>"></script>
