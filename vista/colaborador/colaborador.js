
//FUNCIONES GENERALES: LISTADO DE COLABORADORES, PERFIL DEL COLABORADOR, DATOS DETALLADOS DEL COLABORADOR
function colaborador_lista(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"colaborador/colaborador_lista.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#colaborador_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_colaborador_lista').html(data);
      $('#colaborador_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#colaborador_mensaje_tbl').html('ERROR: ' + data.responseText);
    }
  });
}
function usuario_perfil(usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"perfil/perfil_usuario_perfil.php",
    async: true,
    dataType: "html",
    data: ({
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      $('#div_usuario_perfil').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando Perfil...</h4>');
    },
    success: function(data){
      $('#div_usuario_perfil').html(data);
    },
    complete: function(data){

    },
    error: function(data){
      
    }
  });
}
function colaborador_datos(usuario_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"colaborador/colaborador_datos.php",
    async: true,
    dataType: "html",
    data: ({
      usuario_id: usuario_id
    }),
    beforeSend: function() {
      $('#colaborador_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_colaborador_lista').html(data);
      $('#colaborador_mensaje_tbl').hide(300);
      usuario_perfil(usuario_id);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#colaborador_mensaje_tbl').html('ERROR: ' + data.responseText);
    }
  });
}

//FUNCIONES DE COLABORADOR HORARIO
function colaborador_horario_form(usuario_act, colaboradorhorario_id){ 
  $.ajax({
    type: "POST",
    url: VISTA_URL+"colaborador/colaborador_horario_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      colaboradorhorario_id: colaboradorhorario_id,
      usuario_id: $('#hdd_usuario_id').val()
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_colaborador_form').html(data);
        $('#modal_registro_colaborador_horario').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_colaboradorhorario'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_colaborador_horario', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
        //llamar al formulario de solicitar permiso
        var modulo = 'usuario';
        var div = 'div_modal_colaborador_form';
        permiso_solicitud(usuario_act, colaborador_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
function colaborador_horario_tabla(){ 
  $.ajax({
    type: "POST",
    url: VISTA_URL+"colaborador/colaborador_horario_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      usuario_id: $('#hdd_usuario_id').val()
    }),
    beforeSend: function() {
      $('#div_horario_tabla').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando Tabla...</h4>');
    },
    success: function(data){
      $('#div_horario_tabla').html(data);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#div_horario_tabla').html('ERRROR!:'+ data.responseText);
    }
  });
}

//FUNCIONES DE DETALLE EN GENERAL DEL COLABORADOR
function colaborador_detalle_form(usuario_act, colaboradordetalle_tip, colaboradordetalle_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"colaborador/colaborador_detalle_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      colaboradordetalle_tip: colaboradordetalle_tip,
      colaboradordetalle_id: colaboradordetalle_id,
      usuario_id: $('#hdd_usuario_id').val()
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_colaborador_form').html(data);
        $('#modal_registro_colaborador_detalle').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_colaboradordetalle'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_colaborador_detalle', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
        //llamar al formulario de solicitar permiso
        var modulo = 'colaborador';
        var div = 'div_modal_colaborador_form';
        permiso_solicitud(usuario_act, colaborador_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
function colaborador_detalle_tabla(colaboradordetalle_tip){ 
  var div_tabla = '';
  if(parseInt(colaboradordetalle_tip) == 1)
    div_tabla = 'div_sueldo_tabla';
  if(parseInt(colaboradordetalle_tip) == 2)
    div_tabla = 'div_cargo_tabla';
  if(parseInt(colaboradordetalle_tip) == 3)
    div_tabla = 'div_contrato_tabla';

  $.ajax({
    type: "POST",
    url: VISTA_URL+"colaborador/colaborador_detalle_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      usuario_id: $('#hdd_usuario_id').val(),
      colaboradordetalle_tip: colaboradordetalle_tip
    }),
    beforeSend: function() {
      $('#'+div_tabla).html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando Tabla...</h4>');
    },
    success: function(data){
      $('#'+div_tabla).html(data);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#'+div_tabla).html('ERRROR!:'+ data.responseText);
    }
  });
}


$(document).ready(function() {
	

});