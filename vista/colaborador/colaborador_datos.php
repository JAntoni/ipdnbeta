<?php 
	$usuario_id = $_POST['usuario_id'];
?>
<input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id;?>">

<div class="col-md-3" id="div_usuario_perfil">
	
</div>
<div class="col-md-9">
	<!-- HORARIO DEL COLABORADOR -->
	<div class="box box-warning box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Horario del Colaborador</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<button class="btn btn-primary btn-sm" onclick="colaborador_horario_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Horario</button>
			<div id="div_horario_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
				<?php require_once('../colaborador/colaborador_horario_tabla.php');?>
			</div>
		</div>
	</div>
	<!-- DETALLE DEL COLABORADOR-->
	<div class="row">
		<!-- SUELDO DEL COLABORADOR-->
		<div class="col-md-6">
			<div class="box box-warning box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Sueldo del Colaborador</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<div class="box-body">
					<button class="btn btn-primary btn-sm" onclick="colaborador_detalle_form('I', 1, 0)"><i class="fa fa-plus"></i> Nuevo Sueldo</button>
					<div id="div_sueldo_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
						<?php 
							$colaboradordetalle_tip = 1;
							require('../colaborador/colaborador_detalle_tabla.php');
						?>
					</div>
				</div>
			</div>
		</div>

		<!-- CARGO DEL COLABORADOR-->
		<div class="col-md-6">
			<div class="box box-warning box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Cargo del Colaborador</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<div class="box-body">
					<button class="btn btn-primary btn-sm" onclick="colaborador_detalle_form('I', 2, 0)"><i class="fa fa-plus"></i> Nuevo Cargo</button>
					<div id="div_cargo_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
						<?php 
							$colaboradordetalle_tip = 2;
							require('../colaborador/colaborador_detalle_tabla.php');
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- CONTRATOS DEL COLABORADOR-->	
	<div class="box box-warning box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Contrato del Colaborador</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<button class="btn btn-primary btn-sm" onclick="colaborador_detalle_form('I', 3, 0)"><i class="fa fa-plus"></i> Nuevo Contrato</button>
			<div id="div_contrato_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
				<?php 
					$colaboradordetalle_tip = 3;
					require('../colaborador/colaborador_detalle_tabla.php');
				?>
			</div>
		</div>
	</div>

</div>
<div class="col-md-12">
	<button class="btn btn-primary btn-sm" onclick="colaborador_lista()"><i class="fa fa-arrow-left"></i> Atrás</button>
</div>