<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('../colaborador/Colaborador.class.php');
  $oColaborador = new Colaborador();
  require_once('../funciones/fechas.php');

  $usuario_id = (empty($_POST['usuario_id']))? $usuario_id : intval($_POST['usuario_id']);
  $colaboradordetalle_tip = (empty($_POST['colaboradordetalle_tip']))? $colaboradordetalle_tip : intval($_POST['colaboradordetalle_tip']);
?>
<!-- TABLA DE SUELDO-->
<?php if($colaboradordetalle_tip == 1):?>
	<table class="table table-bordered table-hover">
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Inicio</th> 
	      <th>Fin</th>
	      <th>Sueldo</th>
	      <th>Estado</th>
	      <th></th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php
	    //PRIMER NIVEL
	    $result = $oColaborador->listar_colaborador_detalle_tipo($usuario_id, $colaboradordetalle_tip);
	    if($result['estado'] == 1){
	      foreach ($result['data'] as $key => $value): ?>
	        <tr>
	          <td><?php echo $value['tb_colaboradordetalle_id'];?></td>
	          <td><?php echo mostrar_fecha($value['tb_colaboradordetalle_fecini']);?></td>
	          <td><?php echo mostrar_fecha($value['tb_colaboradordetalle_fecfin']);?></td>
	          <td>
	            <?php
	              echo $value['tb_colaboradordetalle_suel'];
	            ?>
	          </td>
	          <td>
	            <?php
	              $estado = '<span class="badge bg-red">NO</span>';
	              if(intval($value['tb_colaboradordetalle_est']) == 1)
	                $estado = '<span class="badge bg-green">VIGENTE</span>';

	              echo $estado;
	            ?>             
	          </td>
	          <td align="center">
	            <a class="btn btn-warning btn-xs" title="Editar" onclick="colaborador_detalle_form(<?php echo "'M', 1,".$value['tb_colaboradordetalle_id'];?>)"><i class="fa fa-edit"></i></a>
	            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="colaborador_detalle_form(<?php echo "'E', 1,".$value['tb_colaboradordetalle_id'];?>)"><i class="fa fa-trash"></i></a>
	          </td>
	        </tr>
	        <?php
	      endforeach;
	    }
	    $result = NULL;
	    ?>
	  </tbody>
	</table>
<?php endif;?>

<!-- TABLA DE CARGO-->
<?php if($colaboradordetalle_tip == 2):?>
	<table class="table table-bordered table-hover">
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Inicio</th> 
	      <th>Fin</th>
	      <th>Cargo</th>
	      <th>Estado</th>
	      <th></th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php
	    //PRIMER NIVEL
	    $result = $oColaborador->listar_colaborador_detalle_tipo($usuario_id, $colaboradordetalle_tip);
	    if($result['estado'] == 1){
	      foreach ($result['data'] as $key => $value): ?>
	        <tr>
	          <td><?php echo $value['tb_colaboradordetalle_id'];?></td>
	          <td><?php echo mostrar_fecha($value['tb_colaboradordetalle_fecini']);?></td>
	          <td><?php echo mostrar_fecha($value['tb_colaboradordetalle_fecfin']);?></td>
	          <td>
	            <?php
	              echo $value['tb_cargo_nom'];
	            ?>
	          </td>
	          <td>
	            <?php
	              $estado = '<span class="badge bg-red">NO</span>';
	              if(intval($value['tb_colaboradordetalle_est']) == 1)
	                $estado = '<span class="badge bg-green">VIGENTE</span>';

	              echo $estado;
	            ?>             
	          </td>
	          <td align="center">
	            <a class="btn btn-warning btn-xs" title="Editar" onclick="colaborador_detalle_form(<?php echo "'M', 2,".$value['tb_colaboradordetalle_id'];?>)"><i class="fa fa-edit"></i></a>
	            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="colaborador_detalle_form(<?php echo "'E', 2,".$value['tb_colaboradordetalle_id'];?>)"><i class="fa fa-trash"></i></a>
	          </td>
	        </tr>
	        <?php
	      endforeach;
	    }
	    $result = NULL;
	    ?>
	  </tbody>
	</table>
<?php endif;?>

<!-- TABLA DE CONTRATO-->
<?php if($colaboradordetalle_tip == 3):?>
	<table class="table table-bordered table-hover">
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Inicio</th> 
	      <th>Fin</th>
	      <th>Tipo de Contrato</th>
	      <th>Link Contrato</th>
	      <th>Estado</th>
	      <th></th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php
	    //PRIMER NIVEL
	    $result = $oColaborador->listar_colaborador_detalle_tipo($usuario_id, $colaboradordetalle_tip);
	    if($result['estado'] == 1){
	      foreach ($result['data'] as $key => $value): ?>
	        <tr>
	          <td><?php echo $value['tb_colaboradordetalle_id'];?></td>
	          <td><?php echo mostrar_fecha($value['tb_colaboradordetalle_fecini']);?></td>
	          <td><?php echo mostrar_fecha($value['tb_colaboradordetalle_fecfin']);?></td>
	          <td>
	            <?php
	            	$tipo_contrato = 'SIN TIPO';
	            	if(intval($value['tb_colaboradordetalle_tipcontra']) == 1)
	            		$tipo_contrato = 'Contrato Inicial';
	            	if(intval($value['tb_colaboradordetalle_tipcontra']) == 2)
	            		$tipo_contrato = 'Renovación de Contrato';
	            	if(intval($value['tb_colaboradordetalle_tipcontra']) == 3)
	            		$tipo_contrato = 'Cambio de Sueldo';
	            	if(intval($value['tb_colaboradordetalle_tipcontra']) == 4)
	            		$tipo_contrato = 'Carta de Renuncia';
	            	if(intval($value['tb_colaboradordetalle_tipcontra']) == 5)
	            		$tipo_contrato = 'Despido';

	              echo $tipo_contrato;
	            ?>
	          </td>
	          <td>
	            <?php
	              echo '<a href="'.$value['tb_colaboradordetalle_doc'].'" target="_blank">Ver Link</a>';
	            ?>
	          </td>
	          <td>
	            <?php
	              $estado = '<span class="badge bg-red">NO</span>';
	              if(intval($value['tb_colaboradordetalle_est']) == 1)
	                $estado = '<span class="badge bg-green">VIGENTE</span>';

	              echo $estado;
	            ?>             
	          </td>
	          <td align="center">
	            <a class="btn btn-warning btn-xs" title="Editar" onclick="colaborador_detalle_form(<?php echo "'M', 3,".$value['tb_colaboradordetalle_id'];?>)"><i class="fa fa-edit"></i></a>
	            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="colaborador_detalle_form(<?php echo "'E', 3,".$value['tb_colaboradordetalle_id'];?>)"><i class="fa fa-trash"></i></a>
	          </td>
	        </tr>
	        <?php
	      endforeach;
	    }
	    $result = NULL;
	    ?>
	  </tbody>
	</table>
<?php endif;?>