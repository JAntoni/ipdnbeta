<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('../colaborador/Colaborador.class.php');
  $oColaborador = new Colaborador();
  require_once('../funciones/fechas.php');

  $usuario_id = (empty($_POST['usuario_id']))? $usuario_id : intval($_POST['usuario_id']);
?>
<table id="tbl_horario" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Inicio</th> 
      <th>Fin</th>
      <th>Turno</th>
      <th>Ingreso AM</th>
      <th>Salida AM</th>
      <th>Ingreso PM</th>
      <th>Salida PM</th>
      <th>Horas</th>
      <th>Estado</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oColaborador->listar_colaborador_horario($usuario_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value): ?>
        <tr>
          <td><?php echo $value['tb_colaboradorhorario_id'];?></td>
          <td><?php echo mostrar_fecha($value['tb_colaboradorhorario_fecini']);?></td>
          <td><?php echo mostrar_fecha($value['tb_colaboradorhorario_fecfin']);?></td>
          <td>
            <?php 
              $turno = 'SIN TURNO';
              if(intval($value['tb_colaboradorhorario_tur']) == 1)
                $turno = 'Full Time';
              if(intval($value['tb_colaboradorhorario_tur']) == 2)
                $turno = 'Part Time Mañana';
              if(intval($value['tb_colaboradorhorario_tur']) == 3)
                $turno = 'Part Time Tarde';

              echo $turno;
            ?>
          </td>
          <td><?php echo $value['tb_colaboradorhorario_ing1']; ?></td>
          <td><?php echo $value['tb_colaboradorhorario_sal1']; ?></td>
          <td><?php echo $value['tb_colaboradorhorario_ing2']; ?></td>
          <td><?php echo $value['tb_colaboradorhorario_sal2']; ?></td>
          <td><?php echo $value['tb_colaboradorhorario_hor']; ?></td>
          <td>
            <?php
              $estado = '<span class="badge bg-red">NO</span>';
              if(intval($value['tb_colaboradorhorario_est']) == 1)
                $estado = '<span class="badge bg-green">VIGENTE</span>';

              echo $estado;
            ?>             
          </td>
          <td align="center">
            <a class="btn btn-warning btn-xs" title="Editar" onclick="colaborador_horario_form(<?php echo "'M', ".$value['tb_colaboradorhorario_id'];?>)"><i class="fa fa-edit"></i></a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="colaborador_horario_form(<?php echo "'E', ".$value['tb_colaboradorhorario_id'];?>)"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        <?php
      endforeach;
    }
    $result = NULL;
    ?>
  </tbody>
</table>
