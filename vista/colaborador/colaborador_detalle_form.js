
$(document).ready(function(){
	$('#txt_colaboradordetalle_suel').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '10000.00'
	});

	$(".timepicker").timepicker({
    showInputs: true
  });
  
	$('#datetimepicker1, #datetimepicker2').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		//endDate : new Date()
	});
	
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_colaboradordetalle_fecini').val();
   	$('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_colaboradordetalle_fecfin').val();
   	$('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  console.log('editado333333asasasasa');
  $('#form_colaboradordetalle').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"colaborador/colaborador_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_colaboradordetalle").serialize(),
				beforeSend: function() {
					$('#colaborador_mensaje').show(400);
					$('#btn_guardar_colaborador_detalle').prop('disabled', 'disabled');
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						var tipo_detalle = parseInt(data.tipo);

						$('#colaborador_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#colaborador_mensaje').html(data.mensaje);

		      	setTimeout(function(){ 
		      		colaborador_detalle_tabla(tipo_detalle);
		      		$('#modal_registro_colaborador_detalle').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#colaborador_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#colaborador_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_colaborador_detalle').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#colaborador_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#colaborador_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
	  	txt_colaboradordetalle_fecini: {
				required: true
			},
			txt_colaboradordetalle_fecfin: {
				required: true
			},
			txt_colaboradordetalle_suel: {
				required: function(){
					var detalle_tipo = $('#hdd_colaborador_tipo').val();
					if(detalle_tipo == 'sueldo')
						return true;
					else
						return false;
				}
			},
			cmb_cargo_id: {
				required: function(){
					var detalle_tipo = $('#hdd_colaborador_tipo').val();
					if(detalle_tipo == 'cargo')
						return true;
					else
						return false;
				},
				min: 1
			},
			cmb_colaboradordetalle_tipcontra: {
				required: function(){
					var detalle_tipo = $('#hdd_colaborador_tipo').val();
					if(detalle_tipo == 'contrato')
						return true;
					else
						return false;
				},
				min: 1
			},
			txt_colaboradordetalle_doc: {
				required: function(){
					var detalle_tipo = $('#hdd_colaborador_tipo').val();
					if(detalle_tipo == 'contrato')
						return true;
					else
						return false;
				},
				url: true
			}
		},
		messages: {
			txt_colaboradordetalle_fecini: {
				required: "Ingrese Fecha de Inicio"
			},
			txt_colaboradordetalle_fecfin: {
				required: "Ingrese Fecha Fin"
			},
			txt_colaboradordetalle_suel: {
				required: "Ingrese el monto del Sueldo"
			},
			cmb_cargo_id: {
				required: "Selecciones un Cargo",
				min: "Elija un Cargo de la lista"
			},
			cmb_colaboradordetalle_tipcontra: {
				required: "Selecciones un tipo de Contrato",
				min: "Elija un tipo de Contrato de la lista"
			},
			txt_colaboradordetalle_doc: {
				required: "Ingrese el link del Contrato",
				url: "Ingrese un link válido"
			},
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
