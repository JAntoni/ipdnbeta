<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../colaborador/Colaborador.class.php');
  $oColaborador = new Colaborador();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');

  $direc = 'usuario';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $colaboradordetalle_tip = intval($_POST['colaboradordetalle_tip']);
  $colaboradordetalle_id = intval($_POST['colaboradordetalle_id']);
  $usuario_id = intval($_POST['usuario_id']);

  $detalle_tipo = '';
  if($colaboradordetalle_tip == 1)
  	$detalle_tipo = 'sueldo';
  if($colaboradordetalle_tip == 2)
  	$detalle_tipo = 'cargo';
  if($colaboradordetalle_tip == 3)
  	$detalle_tipo = 'contrato';

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Registrado';
  elseif($usuario_action == 'I')
    $titulo = 'Insertar '.ucwords($detalle_tipo).' Nuevo';
  elseif($usuario_action == 'M')
    $titulo = 'Editar '.ucwords($detalle_tipo);
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar '.ucwords($detalle_tipo);
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en usuario
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $session_usuario_id = $_SESSION['usuario_id']; $modulo = 'usuario'; $modulo_id = $usuario_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($session_usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del usuario por su ID
    if(intval($colaboradordetalle_id) > 0){
      $result = $oColaborador->mostrarUno_detalle($colaboradordetalle_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el horadio seleccionado.';
          $bandera = 4;
        }
        else{
	        $colaboradordetalle_fecini = mostrar_fecha($result['data']['tb_colaboradordetalle_fecini']); 
	        $colaboradordetalle_fecfin = mostrar_fecha($result['data']['tb_colaboradordetalle_fecfin']);
	        $colaboradordetalle_suel = $result['data']['tb_colaboradordetalle_suel'];
	        $colaboradordetalle_tipcontra = $result['data']['tb_colaboradordetalle_tipcontra'];
	        $colaboradordetalle_doc = $result['data']['tb_colaboradordetalle_doc'];
	        $colaboradordetalle_est = $result['data']['tb_colaboradordetalle_est'];
	        $cargo_id = $result['data']['tb_cargo_id'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_colaborador_detalle" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_colaboradordetalle" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_usuario_id" value="<?php echo $usuario_id;?>">
          <input type="hidden" name="hdd_colaborador_tipo" id="hdd_colaborador_tipo" value="<?php echo $detalle_tipo;?>">
          <input type="hidden" name="hdd_colaboradordetalle_id" value="<?php echo $colaboradordetalle_id;?>">
          
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-6 col-sm-6">
            		<div class="form-group">
		              <label for="txt_colaboradordetalle_fecini" class="control-label">Fecha de Inicio</label>
		              <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="txt_colaboradordetalle_fecini" id="txt_colaboradordetalle_fecini" value="<?php echo $colaboradordetalle_fecini;?>" />
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
		            </div>
								<!-- INPUT PARA EL SUELDO -->
		            <?php if($detalle_tipo == 'sueldo'):?>
		            	<div class="form-group">
			              <label for="txt_colaboradordetalle_suel" class="control-label">Monto del Sueldo</label>
			              <input type="text" name="txt_colaboradordetalle_suel" id="txt_colaboradordetalle_suel" class="form-control input-sm" value="<?php echo $colaboradordetalle_suel;?>">
			            </div>
			          <?php endif;?>
								<!-- SELECT PARA EL CARGO -->
			          <?php if($detalle_tipo == 'cargo'):?>
		            	<div class="form-group">
			              <label for="cmb_cargo_id" class="control-label">Cargo del Colaborador</label>
			              <select name="cmb_cargo_id" id="cmb_cargo_id" class="form-control input-sm">
	                    <?php require_once('../cargo/cargo_select.php');?>
	                  </select>
			            </div>
			          <?php endif;?>
			          <!-- CAMPOS DE CONTRATO -->
			          <?php if($detalle_tipo == 'contrato'):?>
		            	<div class="form-group">
			              <label for="cmb_colaboradordetalle_tipcontra" class="control-label">Tipo de Contrato</label>
			              <select name="cmb_colaboradordetalle_tipcontra" id="cmb_colaboradordetalle_tipcontra" class="form-control input-sm">
	                    <option value="0"></option>
	                    <option value="1" <?php if($colaboradordetalle_tipcontra == 1) echo 'selected';?> >Contrato Inicial</option>
	                    <option value="2" <?php if($colaboradordetalle_tipcontra == 2) echo 'selected';?> >Renovación de Contrato</option>
	                    <option value="3" <?php if($colaboradordetalle_tipcontra == 3) echo 'selected';?> >Cambio de Sueldo</option>
	                    <option value="4" <?php if($colaboradordetalle_tipcontra == 4) echo 'selected';?> >Carta de Renuncia</option>
	                    <option value="5" <?php if($colaboradordetalle_tipcontra == 5) echo 'selected';?> >Despido</option>
	                  </select>
			            </div>
			            <div class="form-group">
			              <label for="txt_colaboradordetalle_doc" class="control-label">Link del Documento</label>
			              <input type="text" name="txt_colaboradordetalle_doc" id="txt_colaboradordetalle_doc" class="form-control input-sm" value="<?php echo $colaboradordetalle_doc;?>">
			            </div>
			          <?php endif;?>

            	</div>
            	<!-- GRUPO DERECHO-->
            	<div class="col-md-6 col-sm-6">
            		<div class="form-group">
		              <label for="txt_colaboradordetalle_fecfin" class="control-label">Fecha Fin</label>
		              <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="txt_colaboradordetalle_fecfin" id="txt_colaboradordetalle_fecfin" value="<?php echo $colaboradordetalle_fecfin;?>"/>
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
		            </div>
		            <div class="form-group">
                  <label for="cmb_colaboradordetalle_est" class="control-label"><?php echo 'Estado del '.ucwords($detalle_tipo);?></label>
                  <select name="cmb_colaboradordetalle_est" id="cmb_colaboradordetalle_est" class="form-control input-sm">
                    <option value="0"></option>
                    <option value="1" <?php if(intval($colaboradordetalle_est)==1)echo 'selected';?> >Vigente</option>
                  </select>
                </div>
            	</div>
            </div>
          
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Detalle?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="colaborador_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_colaborador_detalle">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_colaborador_detalle">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_usuario">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/colaborador/colaborador_detalle_form.js';?>"></script>
