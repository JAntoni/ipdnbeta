
$(document).ready(function(){
	$('#txt_colaboradorhorario_hor').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0',
		vMax: '240'
	});

	$(".timepicker").timepicker({
    showInputs: true
  });
  
	$('#datetimepicker1, #datetimepicker2').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		//endDate : new Date()
	});
	
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_colaboradorhorario_fecini').val();
   	$('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_colaboradorhorario_fecfin').val();
   	$('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $('#form_colaboradorhorario').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"colaborador/colaborador_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_colaboradorhorario").serialize(),
				beforeSend: function() {
					$('#colaborador_mensaje').show(400);
					$('#btn_guardar_colaborador_horario').prop('disabled', 'disabled');
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#colaborador_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#colaborador_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		colaborador_horario_tabla();
		      		$('#modal_registro_colaborador_horario').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#colaborador_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#colaborador_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_colaborador_horario').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#colaborador_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#colaborador_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
	  	txt_colaboradorhorario_fecini: {
				required: true
			},
			txt_colaboradorhorario_fecfin: {
				required: true
			},
			txt_colaboradorhorario_ing1: {
				required: true
			},
			txt_colaboradorhorario_sal1: {
				required: true
			},
			txt_colaboradorhorario_ing2: {
				required: true
			},
			txt_colaboradorhorario_sal2: {
				required: true
			},
			txt_colaboradorhorario_hor: {
				required: true
			},
			cmb_colaboradorhorario_tur: {
				required: true,
				min: 1
			},
		},
		messages: {
			txt_colaboradorhorario_fecini: {
				required: "Ingrese Fecha de Inicio"
			},
			txt_colaboradorhorario_fecfin: {
				required: "Ingrese Fecha Fin"
			},
			txt_colaboradorhorario_ing1: {
				required: "Ingrese Hora de Ingreso"
			},
			txt_colaboradorhorario_sal1: {
				required: "Ingrese Hora de la Salida"
			},
			txt_colaboradorhorario_ing2: {
				required: "Ingrese Hora de Ingreso"
			},
			txt_colaboradorhorario_sal2: {
				required: "Ingrese Hora de la Salida"
			},
			txt_colaboradorhorario_hor: {
				required: "Ingrese Total Horas"
			},
			cmb_colaboradorhorario_tur: {
				required: "Seleccione el Tipo de Turno",
				min: "Seleccione el Tipo de Turno"
			},
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
