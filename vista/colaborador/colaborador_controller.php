<?php
  session_name("ipdnsac");
  session_start();
  require_once('../colaborador/Colaborador.class.php');
  $oColaborador = new Colaborador();
  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $action = $_POST['action'];

  if($action == 'insertar'){
    $colaborador_tipo = $_POST['hdd_colaborador_tipo'];

    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha reconocido el tipo de módulo para el Colaborador';

    //INSERTAR HORARIO
    if($colaborador_tipo == 'horario'){
      $colaboradorhorario_fecini = fecha_mysql($_POST['txt_colaboradorhorario_fecini']); 
      $colaboradorhorario_fecfin = fecha_mysql($_POST['txt_colaboradorhorario_fecfin']);
      $colaboradorhorario_ing1 = string_hora($_POST['txt_colaboradorhorario_ing1']); //retorna hora en formato 24 horas
      $colaboradorhorario_sal1 = string_hora($_POST['txt_colaboradorhorario_sal1']); //retorna hora en formato 24 horas
      $colaboradorhorario_ing2 = string_hora($_POST['txt_colaboradorhorario_ing2']); //retorna hora en formato 24 horas
      $colaboradorhorario_sal2 = string_hora($_POST['txt_colaboradorhorario_sal2']); //retorna hora en formato 24 horas
      $colaboradorhorario_hor = intval($_POST['txt_colaboradorhorario_hor']);
      $colaboradorhorario_tur = intval($_POST['cmb_colaboradorhorario_tur']);
      $colaboradorhorario_est = intval($_POST['cmb_colaboradorhorario_est']); 
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->insertar_horario(
          $colaboradorhorario_fecini, $colaboradorhorario_fecfin, 
          $colaboradorhorario_ing1, $colaboradorhorario_sal1, 
          $colaboradorhorario_ing2, $colaboradorhorario_sal2, 
          $colaboradorhorario_hor, $colaboradorhorario_tur, 
          $colaboradorhorario_est, $usuario_id
        )
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Horario del Colaborador Guardado.';
      }
    }
    //INSERTAR SUELDO
    if($colaborador_tipo == 'sueldo'){
      $colaboradordetalle_fecini = fecha_mysql($_POST['txt_colaboradordetalle_fecini']); 
      $colaboradordetalle_fecfin = fecha_mysql($_POST['txt_colaboradordetalle_fecfin']);
      $colaboradordetalle_suel = moneda_mysql($_POST['txt_colaboradordetalle_suel']); //monto con comas: 1, 200.00 pasa a 1200.00
      $colaboradordetalle_est = intval($_POST['cmb_colaboradordetalle_est']);
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->insertar_sueldo(
        $colaboradordetalle_fecini, $colaboradordetalle_fecfin, 
        $colaboradordetalle_suel, $colaboradordetalle_est, $usuario_id)
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Sueldo del Colaborador Guardado.';
        $data['tipo'] = 1;
      }
    }
    //INSERTAR CARGO
    if($colaborador_tipo == 'cargo'){
      $colaboradordetalle_fecini = fecha_mysql($_POST['txt_colaboradordetalle_fecini']); 
      $colaboradordetalle_fecfin = fecha_mysql($_POST['txt_colaboradordetalle_fecfin']);
      $cargo_id = intval($_POST['cmb_cargo_id']);
      $colaboradordetalle_est = intval($_POST['cmb_colaboradordetalle_est']);
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->insertar_cargo(
        $colaboradordetalle_fecini, $colaboradordetalle_fecfin, 
        $cargo_id, $colaboradordetalle_est, $usuario_id)
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Cargo del Colaborador Guardado.';
        $data['tipo'] = 2;
      }
    }
    //INSERTAR CONTRATO
    if($colaborador_tipo == 'contrato'){
      $colaboradordetalle_fecini = fecha_mysql($_POST['txt_colaboradordetalle_fecini']); 
      $colaboradordetalle_fecfin = fecha_mysql($_POST['txt_colaboradordetalle_fecfin']);
      $colaboradordetalle_tipcontra = intval($_POST['cmb_colaboradordetalle_tipcontra']);
      $colaboradordetalle_doc = $_POST['txt_colaboradordetalle_doc'];
      $colaboradordetalle_est = intval($_POST['cmb_colaboradordetalle_est']);
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->insertar_contrato(
        $colaboradordetalle_fecini, $colaboradordetalle_fecfin, 
        $colaboradordetalle_tipcontra, $colaboradordetalle_doc, 
        $colaboradordetalle_est, $usuario_id)
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Contrato del Colaborador Guardado.';
        $data['tipo'] = 3;
      }
    }
    echo json_encode($data);
  }

  elseif($action == 'modificar'){
    $colaborador_tipo = $_POST['hdd_colaborador_tipo'];

    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha reconocido el tipo de módulo para el Colaborador';

    //MODIFICAR HORARIO
    if($colaborador_tipo == 'horario'){
      $colaboradorhorario_id = intval($_POST['hdd_colaboradorhorario_id']);
      $colaboradorhorario_fecini = fecha_mysql($_POST['txt_colaboradorhorario_fecini']); 
      $colaboradorhorario_fecfin = fecha_mysql($_POST['txt_colaboradorhorario_fecfin']);
      $colaboradorhorario_ing1 = string_hora($_POST['txt_colaboradorhorario_ing1']); //retorna hora en formato 24 horas
      $colaboradorhorario_sal1 = string_hora($_POST['txt_colaboradorhorario_sal1']); //retorna hora en formato 24 horas
      $colaboradorhorario_ing2 = string_hora($_POST['txt_colaboradorhorario_ing2']); //retorna hora en formato 24 horas
      $colaboradorhorario_sal2 = string_hora($_POST['txt_colaboradorhorario_sal2']); //retorna hora en formato 24 horas
      $colaboradorhorario_hor = intval($_POST['txt_colaboradorhorario_hor']);
      $colaboradorhorario_tur = intval($_POST['cmb_colaboradorhorario_tur']);
      $colaboradorhorario_est = intval($_POST['cmb_colaboradorhorario_est']); 
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->modificar_horario(
          $colaboradorhorario_id,
          $colaboradorhorario_fecini, $colaboradorhorario_fecfin, 
          $colaboradorhorario_ing1, $colaboradorhorario_sal1, 
          $colaboradorhorario_ing2, $colaboradorhorario_sal2, 
          $colaboradorhorario_hor, $colaboradorhorario_tur, 
          $colaboradorhorario_est, $usuario_id
        )
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Horario del Colaborador Modificado.';
      }
    }
    //MODIFICAR SUELDO
    if($colaborador_tipo == 'sueldo'){
      $colaboradordetalle_id = intval($_POST['hdd_colaboradordetalle_id']);
      $colaboradordetalle_fecini = fecha_mysql($_POST['txt_colaboradordetalle_fecini']); 
      $colaboradordetalle_fecfin = fecha_mysql($_POST['txt_colaboradordetalle_fecfin']);
      $colaboradordetalle_suel = moneda_mysql($_POST['txt_colaboradordetalle_suel']); //monto con comas: 1, 200.00 pasa a 1200.00
      $colaboradordetalle_est = intval($_POST['cmb_colaboradordetalle_est']);
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->modificar_sueldo(
        $colaboradordetalle_id, $colaboradordetalle_fecini, 
        $colaboradordetalle_fecfin, $colaboradordetalle_suel, 
        $colaboradordetalle_est, $usuario_id)
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Sueldo del Colaborador Modificado.';
        $data['tipo'] = 1;
      }
    }
    //MODIFICAR CARGO
    if($colaborador_tipo == 'cargo'){
      $colaboradordetalle_id = intval($_POST['hdd_colaboradordetalle_id']);
      $colaboradordetalle_fecini = fecha_mysql($_POST['txt_colaboradordetalle_fecini']); 
      $colaboradordetalle_fecfin = fecha_mysql($_POST['txt_colaboradordetalle_fecfin']);
      $cargo_id = moneda_mysql($_POST['cmb_cargo_id']); //monto con comas: 1, 200.00 pasa a 1200.00
      $colaboradordetalle_est = intval($_POST['cmb_colaboradordetalle_est']);
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->modificar_cargo(
        $colaboradordetalle_id, $colaboradordetalle_fecini, 
        $colaboradordetalle_fecfin, $cargo_id, 
        $colaboradordetalle_est, $usuario_id)
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Cargo del Colaborador Modificado.';
        $data['tipo'] = 2;
      }
    }
    //MODIFICAR CONTRATO
    if($colaborador_tipo == 'contrato'){
      $colaboradordetalle_id = intval($_POST['hdd_colaboradordetalle_id']);
      $colaboradordetalle_fecini = fecha_mysql($_POST['txt_colaboradordetalle_fecini']); 
      $colaboradordetalle_fecfin = fecha_mysql($_POST['txt_colaboradordetalle_fecfin']);
      $colaboradordetalle_tipcontra = intval($_POST['cmb_colaboradordetalle_tipcontra']);
      $colaboradordetalle_doc = $_POST['txt_colaboradordetalle_doc'];
      $colaboradordetalle_est = intval($_POST['cmb_colaboradordetalle_est']);
      $usuario_id = intval($_POST['hdd_usuario_id']);

      if($oColaborador->modificar_contrato(
        $colaboradordetalle_id, $colaboradordetalle_fecini, 
        $colaboradordetalle_fecfin, $colaboradordetalle_tipcontra, 
        $colaboradordetalle_doc, $colaboradordetalle_est, $usuario_id)
      ){
        $data['estado'] = 1;
        $data['mensaje'] = 'Contrato del Colaborador Modificado.';
        $data['tipo'] = 3;
      }
    }

    echo json_encode($data);
  }
  elseif($action == 'eliminar'){
    $colaborador_tipo = $_POST['hdd_colaborador_tipo'];

    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha reconocido el tipo de módulo para el Colaborador';

    //ELIMINAR HORARIO
    if($colaborador_tipo == 'horario'){
      $colaboradorhorario_id = intval($_POST['hdd_colaboradorhorario_id']);

      if($oColaborador->eliminar_horario($colaboradorhorario_id)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Horario del Colaborador Guardado.';
      }
    }
    //ELIMINAR SUELDO
    if($colaborador_tipo == 'sueldo'){
      $colaboradordetalle_id = intval($_POST['hdd_colaboradordetalle_id']);

      if($oColaborador->eliminar_detalle($colaboradordetalle_id)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Sueldo del Colaborador Eliminado.';
        $data['tipo'] = 1;
      }
    }
    //ELIMINAR CARGO
    if($colaborador_tipo == 'cargo'){
      $colaboradordetalle_id = intval($_POST['hdd_colaboradordetalle_id']);

      if($oColaborador->eliminar_detalle($colaboradordetalle_id)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Cargo del Colaborador Eliminado.';
        $data['tipo'] = 2;
      }
    }
    //ELIMINAR CONTRATO
    if($colaborador_tipo == 'contrato'){
      $colaboradordetalle_id = intval($_POST['hdd_colaboradordetalle_id']);

      if($oColaborador->eliminar_detalle($colaboradordetalle_id)){
        $data['estado'] = 1;
        $data['mensaje'] = 'Contrato del Colaborador Eliminado.';
        $data['tipo'] = 3;
      }
    }

    echo json_encode($data);
  }

  else{
    $data['estado'] = 0;
    $data['mensaje'] = 'Action no identificado para: '.$action;

    echo json_encode($data);
  }
  
?>