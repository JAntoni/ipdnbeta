<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'usuario/Usuario.class.php');
    require_once(VISTA_URL.'colaborador/Colaborador.class.php');
  }
  else{
    require_once('../usuario/Usuario.class.php');
    require_once('../colaborador/Colaborador.class.php');
  }
  $oUsuario = new Usuario();
  $oColaborador = new Colaborador();

  $array_color = array('bg-red', 'bg-green', 'bg-aqua', 'bg-blue');

  $result = $oColaborador->listar_colaboradores();
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value): ?>
        <?php 
          $key_color = array_rand($array_color, 1);

          $cargo_nom = 'SIN CARGO';
          $result2 = $oColaborador->colaborador_cargo_usuario($value['tb_usuario_id']);
            if($result2['estado'] == 1)
              $cargo_nom = $result2['data']['tb_cargo_nom'];
          $result2 = NULL;
        ?>
        <div class="col-md-4">
          <div class="box box-widget widget-user">
            <div class="widget-user-header <?php echo $array_color[$key_color];?>" title="Ver Información" style="cursor: pointer;">
              <h3 class="widget-user-username"><?php echo $value['tb_usuario_nom'];?></h3>
              <h5 class="widget-user-desc"><?php echo $cargo_nom;?></h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="<?php echo $value['tb_usuario_fot'];?>" title="Ver Información" onclick="colaborador_datos(<?php echo $value['tb_usuario_id'];?>)" style="cursor: pointer;">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">3,200</h5>
                    <span class="description-text">Créditos</span>
                  </div>
                </div>
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">3,200</h5>
                    <span class="description-text">Ventas</span>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header">3,200</h5>
                    <span class="description-text">Clientes</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
      endforeach;
    }
  $result = NULL;
?>


