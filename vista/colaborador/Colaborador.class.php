<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Colaborador extends Conexion{
    //FUNCIONES PROPIAS DE COLABORADOR
    function listar_colaboradores(){
      try {
        $sql = "SELECT usu.*,tb_usuariogrupo_nom,tb_usuarioperfil_nom FROM tb_usuario usu INNER JOIN tb_usuariogrupo gru on gru.tb_usuariogrupo_id = usu.tb_usuariogrupo_id INNER JOIN tb_usuarioperfil per on per.tb_usuarioperfil_id = usu.tb_usuarioperfil_id WHERE tb_usuario_xac = 1 AND tb_usuario_blo = 0";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay usuarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    //FUNCIONES DE HORARIO DEL COLABORADOR
    function insertar_horario($colaboradorhorario_fecini, $colaboradorhorario_fecfin, $colaboradorhorario_ing1, $colaboradorhorario_sal1, $colaboradorhorario_ing2, $colaboradorhorario_sal2, $colaboradorhorario_hor, $colaboradorhorario_tur, $colaboradorhorario_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradorhorario_est) == 1){
          $sql = "UPDATE tb_colaboradorhorario SET tb_colaboradorhorario_est = 0 WHERE tb_usuario_id =:usuario_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "INSERT INTO tb_colaboradorhorario(tb_colaboradorhorario_fecini, tb_colaboradorhorario_fecfin, tb_colaboradorhorario_ing1, tb_colaboradorhorario_sal1, tb_colaboradorhorario_ing2, tb_colaboradorhorario_sal2, tb_colaboradorhorario_hor, tb_colaboradorhorario_tur, tb_colaboradorhorario_est, tb_usuario_id) VALUES (:colaboradorhorario_fecini, :colaboradorhorario_fecfin, :colaboradorhorario_ing1, :colaboradorhorario_sal1, :colaboradorhorario_ing2, :colaboradorhorario_sal2, :colaboradorhorario_hor, :colaboradorhorario_tur, :colaboradorhorario_est, :usuario_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradorhorario_fecini", $colaboradorhorario_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_fecfin", $colaboradorhorario_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_ing1", $colaboradorhorario_ing1, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_sal1", $colaboradorhorario_sal1, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_ing2", $colaboradorhorario_ing2, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_sal2", $colaboradorhorario_sal2, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_hor", $colaboradorhorario_hor, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradorhorario_tur", $colaboradorhorario_tur, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradorhorario_est", $colaboradorhorario_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_horario($colaboradorhorario_id, $colaboradorhorario_fecini, $colaboradorhorario_fecfin, $colaboradorhorario_ing1, $colaboradorhorario_sal1, $colaboradorhorario_ing2, $colaboradorhorario_sal2, $colaboradorhorario_hor, $colaboradorhorario_tur, $colaboradorhorario_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradorhorario_est) == 1){
          $sql = "UPDATE tb_colaboradorhorario SET tb_colaboradorhorario_est = 0 WHERE tb_usuario_id =:usuario_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "UPDATE tb_colaboradorhorario SET tb_colaboradorhorario_fecini =:colaboradorhorario_fecini, tb_colaboradorhorario_fecfin =:colaboradorhorario_fecfin, tb_colaboradorhorario_ing1 =:colaboradorhorario_ing1, tb_colaboradorhorario_sal1 =:colaboradorhorario_sal1, tb_colaboradorhorario_ing2 =:colaboradorhorario_ing2, tb_colaboradorhorario_sal2 =:colaboradorhorario_sal2, tb_colaboradorhorario_hor =:colaboradorhorario_hor, tb_colaboradorhorario_tur =:colaboradorhorario_tur, tb_colaboradorhorario_est =:colaboradorhorario_est, tb_usuario_id =:usuario_id WHERE tb_colaboradorhorario_id =:colaboradorhorario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradorhorario_id", $colaboradorhorario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradorhorario_fecini", $colaboradorhorario_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_fecfin", $colaboradorhorario_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_ing1", $colaboradorhorario_ing1, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_sal1", $colaboradorhorario_sal1, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_ing2", $colaboradorhorario_ing2, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_sal2", $colaboradorhorario_sal2, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradorhorario_hor", $colaboradorhorario_hor, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradorhorario_tur", $colaboradorhorario_tur, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradorhorario_est", $colaboradorhorario_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar_horario($colaboradorhorario_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_colaboradorhorario  WHERE tb_colaboradorhorario_id =:colaboradorhorario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradorhorario_id", $colaboradorhorario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno_horario($colaboradorhorario_id){
      try {
        $sql = "SELECT * FROM tb_colaboradorhorario WHERE tb_colaboradorhorario_id =:colaboradorhorario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradorhorario_id", $colaboradorhorario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay usuarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_colaborador_horario($usuario_id){
      try {
        $sql = "SELECT * FROM tb_colaboradorhorario WHERE tb_usuario_id =:usuario_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay usuarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }    
    
    //FUNCIONES DE SUELDO DEL COLABORADOR
    function insertar_sueldo($colaboradordetalle_fecini, $colaboradordetalle_fecfin, $colaboradordetalle_suel, $colaboradordetalle_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradordetalle_est) == 1){
          $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_est = 0 WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip = 1";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "INSERT INTO tb_colaboradordetalle(tb_colaboradordetalle_tip, tb_colaboradordetalle_fecini, tb_colaboradordetalle_fecfin, tb_colaboradordetalle_suel, tb_colaboradordetalle_est, tb_usuario_id) VALUES (1, :colaboradordetalle_fecini, :colaboradordetalle_fecfin, :colaboradordetalle_suel, :colaboradordetalle_est, :usuario_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_fecini", $colaboradordetalle_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_fecfin", $colaboradordetalle_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_suel", $colaboradordetalle_suel, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_est", $colaboradordetalle_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_sueldo($colaboradordetalle_id, $colaboradordetalle_fecini, $colaboradordetalle_fecfin, $colaboradordetalle_suel, $colaboradordetalle_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradordetalle_est) == 1){
          $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_est = 0 WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip = 1";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_fecini =:colaboradordetalle_fecini, tb_colaboradordetalle_fecfin =:colaboradordetalle_fecfin, tb_colaboradordetalle_suel =:colaboradordetalle_suel, tb_colaboradordetalle_est =:colaboradordetalle_est, tb_usuario_id =:usuario_id WHERE tb_colaboradordetalle_id =:colaboradordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_id", $colaboradordetalle_id, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_fecini", $colaboradordetalle_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_fecfin", $colaboradordetalle_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_suel", $colaboradordetalle_suel, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_est", $colaboradordetalle_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    //FUNCIONES DE CARGO DEL COLABORADOR
    function insertar_cargo($colaboradordetalle_fecini, $colaboradordetalle_fecfin, $cargo_id, $colaboradordetalle_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradordetalle_est) == 1){
          $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_est = 0 WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip = 2";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "INSERT INTO tb_colaboradordetalle(tb_colaboradordetalle_tip, tb_colaboradordetalle_fecini, tb_colaboradordetalle_fecfin, tb_cargo_id, tb_colaboradordetalle_est, tb_usuario_id) VALUES (2, :colaboradordetalle_fecini, :colaboradordetalle_fecfin, :cargo_id, :colaboradordetalle_est, :usuario_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_fecini", $colaboradordetalle_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_fecfin", $colaboradordetalle_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_est", $colaboradordetalle_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_cargo($colaboradordetalle_id, $colaboradordetalle_fecini, $colaboradordetalle_fecfin, $cargo_id, $colaboradordetalle_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradordetalle_est) == 1){
          $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_est = 0 WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip = 2";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_fecini =:colaboradordetalle_fecini, tb_colaboradordetalle_fecfin =:colaboradordetalle_fecfin, tb_cargo_id =:cargo_id, tb_colaboradordetalle_est =:colaboradordetalle_est, tb_usuario_id =:usuario_id WHERE tb_colaboradordetalle_id =:colaboradordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_id", $colaboradordetalle_id, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_fecini", $colaboradordetalle_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_fecfin", $colaboradordetalle_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_est", $colaboradordetalle_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function colaborador_cargo_usuario($usuario_id){
      try {
        //el cargo tiene como tipo el número: 2
        $sql = "SELECT * FROM tb_colaboradordetalle coldet INNER JOIN tb_cargo car on car.tb_cargo_id = coldet.tb_cargo_id WHERE '".date('Y-m-d')."' between tb_colaboradordetalle_fecini AND tb_colaboradordetalle_fecfin AND tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip = 2 limit 1";

        $sentencia = $this->dblink->prepare($sql);

        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No se ha encontrado registros para la consulta";
          $retorno["data"] = "";
        }

        return $retorno;

      } catch (Exception $e) {
        throw $e;
      }
    }

    //FUNCIONES DE CARGO DEL COLABORADOR
    function insertar_contrato($colaboradordetalle_fecini, $colaboradordetalle_fecfin, $colaboradordetalle_tipcontra, $colaboradordetalle_doc, $colaboradordetalle_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradordetalle_est) == 1){
          $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_est = 0 WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip = 3";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "INSERT INTO tb_colaboradordetalle(tb_colaboradordetalle_tip, tb_colaboradordetalle_fecini, tb_colaboradordetalle_fecfin, tb_colaboradordetalle_tipcontra, tb_colaboradordetalle_doc, tb_colaboradordetalle_est, tb_usuario_id) VALUES (3, :colaboradordetalle_fecini, :colaboradordetalle_fecfin, :colaboradordetalle_tipcontra, :colaboradordetalle_doc, :colaboradordetalle_est, :usuario_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_fecini", $colaboradordetalle_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_fecfin", $colaboradordetalle_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_tipcontra", $colaboradordetalle_tipcontra, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_doc", $colaboradordetalle_doc, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_est", $colaboradordetalle_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_contrato($colaboradordetalle_id, $colaboradordetalle_fecini, $colaboradordetalle_fecfin, $colaboradordetalle_tipcontra, $colaboradordetalle_doc, $colaboradordetalle_est, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        if(intval($colaboradordetalle_est) == 1){
          $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_est = 0 WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip = 3";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();
        }

        $sql = "UPDATE tb_colaboradordetalle SET tb_colaboradordetalle_fecini =:colaboradordetalle_fecini, tb_colaboradordetalle_fecfin =:colaboradordetalle_fecfin, tb_colaboradordetalle_tipcontra =:colaboradordetalle_tipcontra, tb_colaboradordetalle_doc =:colaboradordetalle_doc, tb_colaboradordetalle_est =:colaboradordetalle_est, tb_usuario_id =:usuario_id WHERE tb_colaboradordetalle_id =:colaboradordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_id", $colaboradordetalle_id, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_fecini", $colaboradordetalle_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_fecfin", $colaboradordetalle_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_tipcontra", $colaboradordetalle_tipcontra, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_doc", $colaboradordetalle_doc, PDO::PARAM_STR);
        $sentencia->bindParam(":colaboradordetalle_est", $colaboradordetalle_est, PDO::PARAM_INT);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    //FUNCIONES GENERALES DE DETALLE DEL COLABORADOR
    function listar_colaborador_detalle_tipo($usuario_id, $colaboradordetalle_tip){
      try {
        $sql = "SELECT * FROM tb_colaboradordetalle WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip =:colaboradordetalle_tip";

        if(intval($colaboradordetalle_tip == 2))
          $sql = "SELECT * FROM tb_colaboradordetalle colde INNER JOIN tb_cargo car ON car.tb_cargo_id = colde.tb_cargo_id WHERE tb_usuario_id =:usuario_id AND tb_colaboradordetalle_tip =:colaboradordetalle_tip";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":colaboradordetalle_tip", $colaboradordetalle_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay usuarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrarUno_detalle($colaboradordetalle_id){
      try {
        $sql = "SELECT * FROM tb_colaboradordetalle WHERE tb_colaboradordetalle_id =:colaboradordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_id", $colaboradordetalle_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay usuarios registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function eliminar_detalle($colaboradordetalle_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_colaboradordetalle  WHERE tb_colaboradordetalle_id =:colaboradordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":colaboradordetalle_id", $colaboradordetalle_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  }

?>
