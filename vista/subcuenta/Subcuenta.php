<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Subcuenta extends Conexion{
    public $tb_subcuenta_id;
    public $tb_subcuenta_xac;
    public $tb_subcuenta_cod;
    public $tb_subcuenta_des;
    public $tb_subcuenta_ord;
    public $tb_cuenta_id;
    public $tb_subcuenta_mos;
    
    
    
    function listar_subcuentas($cuenta_id){
      try {
        $sql = "SELECT 
                        * 
                FROM 
                        tb_subcuenta WHERE tb_cuenta_id=:tb_cuenta_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuenta_id", $cuenta_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Sub cuentas registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
}


?>