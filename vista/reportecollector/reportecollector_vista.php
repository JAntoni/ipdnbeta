<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
			font-weight: bold;
		}
		div.dataTables_filter label{
			width: 80%;
		}
	</style>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="reportecollector_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla ASIVEH...</span></h4>
        </div>
				
				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">REPORTE DE CLIENTES PARA INFORCORP</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<!-- TABLA PARA REPORTAR A INFOCORP -->
						<div class="table-responsive">
							<table id="tbl_infocorp" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Fecha del Reporte</th>
										<th>RUC</th>
										<th>Tipo Documento</th>
										<th>N° Documento</th>
										<th>Tipo Persona</th>
										<th>Tipo Deudor</th>
										<th>Razón Social</th>
										<th>Dirección</th>
										<th>Teléfono</th>
										<th>Email</th>
										<th>Código Ubigeo</th>
										<th>Distrito</th>
										<th>Provincia</th>
										<th>Departamento</th>
										<th>N° Cuenta</th>
										<th>Fecha Vencimiento</th>
										<th>Doc. Crédito</th>
										<th>Tipo Moneda</th>
										<th>Monto Deuda</th>
										<th>Condición Deuda</th>
										<th>Directo</th>
										<th>Directo 2</th>
										<th>Directo 3</th>
										<th>Directo 4</th>
										<th>Tipo Movimiento</th>
										<th>Publicar Infocorp</th>
									</tr>
								</thead>
								<tbody id="lista_infocorp">
									
								</tbody>
							</table>
						</div>
						<!-- FIN REPORTE A INFOCORP -->
					</div>
				</div>		
			</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
