<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Filestorage extends Conexion{
    public $filestorage_id;

    public $modulo_nom;
    public $modulo_id;
    public $modulo_subnom;
    public $filestorage_uniq = '';
    public $filestorage_url = '...';
    public $usuario_id; //extensiones de las imágenes
    public $filestorage_des; //comentario al cual está enlazado

    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    public function insertar(){
      $this->dblink->beginTransaction();
      try {
        $columns = [
          'modulo_nom', 
          'modulo_id',
          'modulo_subnom',
          'filestorage_uniq', 
          'filestorage_url', 
          'tb_usuario_id',
          'filestorage_des'
        ];
  
        // Lista de placeholders
        $placeholders = implode(',', array_map(function ($column) {
            return ':' . $column;
        }, $columns));

        $sql = "INSERT INTO filestorage (" . implode(',', $columns) . ") VALUES ($placeholders)";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $this->modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_subnom", $this->modulo_subnom, PDO::PARAM_STR);
        $sentencia->bindParam(":filestorage_uniq", $this->filestorage_uniq, PDO::PARAM_STR);
        $sentencia->bindParam(":filestorage_url", $this->filestorage_url, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":filestorage_des", $this->filestorage_des, PDO::PARAM_STR);

        $result1 = $sentencia->execute();
        $this->filestorage_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $nombre_imagen = $this->modulo_nom.'_'.$this->modulo_id.'_filestorage_'. $this->filestorage_id .'.'. $this->fileParts['extension'];
        $filestorage_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $filestorage_url)){
          //insertamos las imágenes
          $sql = "UPDATE filestorage SET filestorage_url =:filestorage_url WHERE filestorage_id =:filestorage_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":filestorage_id", $this->filestorage_id, PDO::PARAM_INT);
          $sentencia->bindParam(":filestorage_url", $filestorage_url, PDO::PARAM_STR);
          $sentencia->execute();
        }
        else{
          $this->eliminar($this->filestorage_id);
        }

        if($result1 == 1)
          return 'exito';
        else
          return 'Error al guardar el documento';
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    public function eliminar($filestorage_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM filestorage WHERE filestorage_id =:filestorage_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filestorage_id", $filestorage_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    public function mostrarUno($filestorage_id){
      try {
        $sql = "SELECT * FROM filestorage WHERE filestorage_id =:filestorage_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filestorage_id", $filestorage_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    public function mostrarUnoSubmodulo($modulo_nom, $modulo_subnom, $modulo_id){
      try {
        $sql = "SELECT * FROM filestorage WHERE modulo_nom =:modulo_nom AND modulo_subnom =:modulo_subnom AND modulo_id =:modulo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_subnom", $modulo_subnom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    public function listar_filestorages($modulo_nom, $modulo_id){
      try {
        $sql = "SELECT * FROM filestorage fs
          LEFT JOIN tb_usuario usu ON usu.tb_usuario_id = fs.tb_usuario_id
          WHERE modulo_nom =:modulo_nom and modulo_id =:modulo_id AND filestorage_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $modulo_nom, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay imágenes subidas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    public function eliminar_xac($filestorage_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE filestorage SET filestorage_xac = 0 WHERE filestorage_id =:filestorage_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filestorage_id", $filestorage_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    public function insertar2(){
      $this->dblink->beginTransaction();
      try {
        $columns = [
          'modulo_nom', 
          'modulo_id',
          'modulo_subnom',
          'filestorage_uniq', 
          'filestorage_url', 
          'tb_usuario_id',
          'filestorage_des'
        ];
  
        // Lista de placeholders
        $placeholders = implode(',', array_map(function ($column) {
            return ':' . $column;
        }, $columns));

        $sql = "INSERT INTO filestorage (" . implode(',', $columns) . ") VALUES ($placeholders)";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $this->modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $this->modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_subnom", $this->modulo_subnom, PDO::PARAM_STR);
        $sentencia->bindParam(":filestorage_uniq", $this->filestorage_uniq, PDO::PARAM_STR);
        $sentencia->bindParam(":filestorage_url", $this->filestorage_url, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":filestorage_des", $this->filestorage_des, PDO::PARAM_STR);

        $result1 = $sentencia->execute();
        $this->filestorage_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $nombre_imagen = $this->modulo_nom.'_'.$this->modulo_id.'_filestorage_'. $this->filestorage_id .'.'. $this->fileParts['extension'];
        $filestorage_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $filestorage_url)){
          //insertamos las imágenes
          $sql = "UPDATE filestorage SET filestorage_url =:filestorage_url WHERE filestorage_id =:filestorage_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":filestorage_id", $this->filestorage_id, PDO::PARAM_INT);
          $sentencia->bindParam(":filestorage_url", $filestorage_url, PDO::PARAM_STR);
          $sentencia->execute();

          $retorno["estado"] = 1;
          $retorno["filestorage_id"] = $this->filestorage_id;
          $retorno["url"] = $filestorage_url;
          $retorno["mensaje"] = "exito";
          return $retorno;
        }
        else{
          $this->eliminar($this->filestorage_id);

          $retorno["estado"] = 0;
          $retorno["mensaje"] = "Error al guardar el documento";
          return $retorno;
        }
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    public function listar_files_modulo_submodulo($modulo_nom, $modulo_id, $modulo_subnom){
      try {
        $sql = "SELECT * FROM filestorage fs
          LEFT JOIN tb_usuario usu ON usu.tb_usuario_id = fs.tb_usuario_id
          WHERE modulo_nom =:modulo_nom AND modulo_id =:modulo_id AND modulo_subnom =:modulo_subnom;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":modulo_nom", $modulo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":modulo_id", $modulo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":modulo_subnom", $modulo_subnom, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }
?>
