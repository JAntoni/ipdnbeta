<?php
  require_once('../../core/usuario_sesion.php');
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../filestorage/Filestorage.class.php');
  $oFilestorage = new Filestorage();
  require_once('../funciones/funciones.php');

  $direc = 'filestorage';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)

  $public_carpeta = $_POST['public_carpeta']; //? se refiere a la ruta en donde está la carpeta que sería ejmplo: public/pdf o public/images
  $modulo_nom = $_POST['modulo_nom'];
  $modulo_id = $_POST['modulo_id'];
  $modulo_subnom = isset($_POST['modulo_subnom'])? $_POST['modulo_subnom'] : ''; //? ahora en el mismo modulo se puede guardar documentos por detalle ejemplo: credito menor - contrato - anexo - acta
  $filestorage_uniq = $_POST['filestorage_uniq']; //id temporal
  $filestorage_id = $_POST['filestorage_id'];
  $filestorage_des = $_POST['filestorage_des'];

  $bandera = 1;

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'ACCION NO DISPONIBLE';
  elseif($usuario_action == 'I')
    $titulo = 'Subir Fotos';
  elseif($usuario_action == 'M')
    $titulo = 'ACCION NO DISPONIBLE';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar imagen';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_filestorage" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?php echo $titulo;?></h4>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
        </div>
        <form id="form_filestorage" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_modulo_nom" id="hdd_modulo_nom" value="<?php echo $modulo_nom;?>">
          <input type="hidden" name="hdd_modulo_id" id="hdd_modulo_id" value="<?php echo $modulo_id;?>">
          <input type="hidden" name="hdd_modulo_subnom" id="hdd_modulo_subnom" value="<?php echo $modulo_subnom;?>">
          <input type="hidden" name="hdd_filestorage_uniq" id="hdd_filestorage_uniq" value="<?php echo $filestorage_uniq;?>">
          <input type="hidden" name="hdd_filestorage_id" id="hdd_filestorage_id" value="<?php echo $filestorage_id;?>">
          <input type="hidden" name="hdd_filestorage_des" id="hdd_filestorage_des" value="<?php echo $filestorage_des;?>">
          <input type="hidden" name="hdd_public_carpeta" id="hdd_public_carpeta" value="<?php echo $public_carpeta;?>">

          <div class="modal-body">
            <?php if($action != 'eliminar'):?>
              <div class="row">
                <div class="col-md-12">
                  <input id="file_filestorage" name="file_filestorage" type="file" multiple="true">
                </div>
                <div class="col-md-12">
                  <div id="queue"></div>
                </div>
                <div class="col-md-12">
                  <div class="alert alert-danger alert-dismissible" id="alerta_documento" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>
                      <i class="icon fa fa-ban"></i> Alerta
                    </h4>
                  </div>
                </div>
              </div>
            <?php endif;?>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Imagen?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="filestorage_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="button" class="btn btn-info" id="btn_guardar_filestorage" onclick="javascript:$('#file_filestorage').uploadifive('upload')">Guardar Fotos</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_filestorage">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_cerrar_filestorage">Listo</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_filestorage">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="<?php echo 'static/css/uploadifive/uploadifive.css';?>">
<script type="text/javascript" src="<?php echo 'static/js/uploadifive/jquery.uploadifive.js';?>"></script>
<script type="text/javascript" src="<?php echo 'vista/filestorage/filestorage_form2.js?ver=15102024';?>"></script>
