$(document).ready(function () {
  // $('#btn_cerrar_filestorage').click(function (event) {
  //     filestorage_galeria();
  // }); filestorage
  $("#file_filestorage").uploadifive({
    auto: false,
    //'checkScript'      : 'programacion_file_check.php',
    formData: {
      action: "insertar",
      public_carpeta: $('#hdd_public_carpeta').val(),
      modulo_nom: $("#hdd_modulo_nom").val(),
      modulo_id: $("#hdd_modulo_id").val(),
      modulo_subnom: $("#hdd_modulo_subnom").val(),
      filestorage_uniq: $("#hdd_filestorage_uniq").val(),
      filestorage_des: $('#hdd_filestorage_des').val() 
    },
    queueID: "queue",
    uploadScript: VISTA_URL + "filestorage/filestorage_controller.php",
    queueSizeLimit: 10,
    uploadLimit: 10,
    multi: true,
    buttonText: "Seleccionar Archivo",
    height: 20,
    width: 180,
    fileSizeLimit: "5MB",
    fileType: ["image/gif", "image/jpeg", "image/png", "image/jpg", "pdf"],
    onUpload: function () {
      if ($("#hdd_modulo_nom").val() == 'creditogarveh' && $("#hdd_modulo_subnom").val() == 'post_cierre') {
        var uploadInstance = $("#file_filestorage").data('uploadifive');
        uploadInstance.settings.formData.filestorage_des = $('#txt_filestorage_des').val().toUpperCase();
      }
    },
    onUploadComplete: function (file, data) {
      if (data != "exito") {
        $("#alerta_documento").show(300);
        $("#alerta_documento").append(
          "<strong>El archivo: " +
            file.name +
            ", no se pudo subir -> " +
            data +
            "</strong><br>"
        );
      }
    },
    onError: function (errorType) {
      var message = "Error desconocido.";
      if (errorType == "FILE_SIZE_LIMIT_EXCEEDED") {
        message = "Tamaño de archivo debe ser menor a 3MB.";
      } else if (errorType == "FORBIDDEN_FILE_TYPE") {
        message = "Tipo de archivo no válido.";
        alerta_error('IMPORTANTE', message);
      }
      //$(".filestorageifive-queue-item.error").hide();
      //
      //$.FormFeedbackError(message);
    },
    onAddQueueItem: function (file) {
      
    }
  });

  $('#btn_cerrar_filestorage').click(function () {
    var modulo_nom = $('#hdd_modulo_nom').val();
    var modulo_subnom = $('#hdd_modulo_subnom').val();

    if(modulo_nom == 'cliente')
      listar_sustento_cambio(); // funcion ubicada en el archivo cliente_form_cmenor.js
    if(modulo_nom == 'creditomenor')
      listar_creditomenor_checklist(); //funcion ubicada en el archivo creditomenor_form.js
    if(modulo_nom == 'pagoplanilla')
      pagoplanilla_tabla(); //funcion ubicada en el archivo pagoplanilla.js
    if(modulo_nom == 'cgvtitulos') {
      refreshSpanPDF(); // funcion para refrescar el mensaje de archivo(s) adjuntados
      creditogarveh_modal_titulos_listar_registros();
    }
    
    if(modulo_subnom == 'stockunidad_ajustar_pdf')
      refreshSpanPDF(); // funcion para refrescar el mensaje de archivo(s) adjuntados
    if(modulo_subnom == 'stockunidad_ajustar')
      refreshSpanImages(); // funcion para refrescar el mensaje de archivo(s) adjuntados

    if (modulo_nom == 'creditogarveh' && modulo_subnom == 'post_cierre') {
      listar_docs_post_cierre();
    }
  });
});
