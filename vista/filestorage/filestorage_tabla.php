<?php
  if(defined('VISTA_URL')){
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL.'filestorage/Filestorage.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../../core/usuario_sesion.php');
    require_once('../filestorage/Filestorage.class.php');
    require_once('../funciones/fechas.php');
  }

  $oFilestorage = new Filestorage();

  $modulo_id = (isset($_POST['modulo_id']))? $_POST['modulo_id'] : intval($modulo_id);
  $modulo_nom = (isset($_POST['modulo_nom']))? $_POST['modulo_nom'] : $modulo_nom;
  $usuariogrupo_id = isset($_SESSION['usuariogrupo_id'])? intval($_SESSION['usuariogrupo_id']) : intval($_POST['usuariogrupo_id']);
  $modulo_subnom = !empty($_POST['modulo_subnom']) ? $_POST['modulo_subnom'] : $modulo_subnom;

  $tabla = '
    <table class="table table-hover">
      <tr>
        <th>Subido Por</th>
        '.($modulo_nom == 'creditogarveh' && $modulo_subnom == 'post_cierre' ? '<th>Nombre Doc</th>' : '').'
        <th>Fecha Subida</th>
        <th>Documento</th>
      </tr>
      <tbody>';

  $eliminar = '';

  if (!empty($modulo_subnom)) {
    $result = $oFilestorage->listar_files_modulo_submodulo($modulo_nom, $modulo_id, $modulo_subnom);
  } else {
    $result = $oFilestorage->listar_filestorages($modulo_nom, $modulo_id);
  }
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $tabla .= '
          <tr id="tr_'.$value['filestorage_id'].'">
            <td>'.$value['tb_usuario_nom'].'</td>
            '.($modulo_nom == 'creditogarveh' && $modulo_subnom == 'post_cierre' ? '<td>'.$value['filestorage_des'].'</td>' : '').'
            <td>'.mostrar_fecha_hora($value['filestorage_reg']).'</td>
            <td>
              <a class="btn btn-info btn-xs" href="'.$value['filestorage_url'].'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
        if($usuariogrupo_id == 2)
          $tabla .= '<button type="button" class="btn btn-danger btn-xs" onclick="filestorage_eliminar('.$value['filestorage_id'].')"><i class="fa fa-trash"></i></a>';
        $tabla .= '
            </td>
          </tr>
        ';
      }
      $tabla .='</tbody></table>';
      echo $tabla;
    }
    else
      echo '';
  $result = NULL;
?>