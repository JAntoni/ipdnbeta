<?php
  require_once('../../core/usuario_sesion.php');
 	require_once('../filestorage/Filestorage.class.php');
  $oFilestorage = new Filestorage();
  require_once ("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
  require_once('../rendicioncuentasdetalle/RendicionCuentasDetalle.class.php');
  $oRendicionCuentasDetalle = new RendicionCuentasDetalle();

 	$action = $_POST['action'];

 	if($action == 'insertar'){

    if (!empty($_FILES)){
      $modulo_nom = trim($_POST['modulo_nom']);
      $modulo_id = intval($_POST['modulo_id']);
      $modulo_subnom = trim($_POST['modulo_subnom']);
      $filestorage_uniq = $_POST['filestorage_uniq']; //id temporal
      $filestorage_des = $_POST['filestorage_des'];
      $public_carpeta = $_POST['public_carpeta']; // tenmos 2 ya sea imagenes o pdf
      if ($modulo_nom == 'creditogarveh' && $modulo_subnom == 'post_cierre' && empty(trim($filestorage_des))) {
        echo 'Debe ingresar una descripción para el archivo';
        exit();
      }
      $usuario_id = intval($_SESSION['usuario_id']); 

      $directorio = 'public/'.$public_carpeta.'/'.$modulo_nom.'/';
      $fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'pdf'); // Allowed file extensions

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetFile = $directorio . $_FILES['Filedata']['name'];
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      
      if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
        //move_filestorageed_file($tempFile, $targetFile); insertamos las imágenes
        $oFilestorage->modulo_nom = $modulo_nom;
        $oFilestorage->modulo_id = $modulo_id;
        $oFilestorage->modulo_subnom = $modulo_subnom;
        $oFilestorage->filestorage_uniq = $filestorage_uniq;

        $oFilestorage->tempFile = $tempFile; //temp servicio de PHP
        $oFilestorage->directorio = $directorio; //nombre con el que irá guardado en la carpeta
        $oFilestorage->fileParts = $fileParts; //extensiones de las imágenes
        $oFilestorage->usuario_id = $usuario_id;
        $oFilestorage->filestorage_des = $filestorage_des;

        $return = $oFilestorage->insertar();

        echo trim($return);
      } else {
        // The file type wasn't allowed
        echo 'Tipo de archivo no válido.';
      }
    }
    else{
      echo 'No hay ningún archivo para subir';
    }
 	}
  elseif($action == 'insertar2'){
    if (!empty($_FILES)){
      $modulo_nom = trim($_POST['modulo_nom']);
      $modulo_id = intval($_POST['modulo_id']);
      $modulo_subnom = trim($_POST['modulo_subnom']);
      $filestorage_uniq = $_POST['filestorage_uniq']; //id temporal
      $filestorage_des = $_POST['filestorage_des'];
      $public_carpeta = $_POST['public_carpeta']; // tenmos 2 ya sea imagenes o pdf
      
      $usuario_id = intval($_SESSION['usuario_id']); 

      $directorio = 'public/'.$public_carpeta.'/'.$modulo_nom.'/';
      $fileTypes = array('jpg', 'jpeg', 'gif', 'png', 'pdf'); // Allowed file extensions

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetFile = $directorio . $_FILES['Filedata']['name'];
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      
      if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
        //move_filestorageed_file($tempFile, $targetFile); insertamos las imágenes
        $oFilestorage->modulo_nom = $modulo_nom;
        $oFilestorage->modulo_id = $modulo_id;
        $oFilestorage->modulo_subnom = $modulo_subnom;
        $oFilestorage->filestorage_uniq = $filestorage_uniq;

        $oFilestorage->tempFile = $tempFile; //temp servicio de PHP
        $oFilestorage->directorio = $directorio; //nombre con el que irá guardado en la carpeta
        $oFilestorage->fileParts = $fileParts; //extensiones de las imágenes
        $oFilestorage->usuario_id = $usuario_id;
        $oFilestorage->filestorage_des = $filestorage_des;

        $return = $oFilestorage->insertar2();

        //echo trim($return);
        if($return["estado"] == 1){
          $data['estado'] = 1;
          $data['mensaje'] = "REGISTRO EXITOSO";
          $data['filestorage_id'] = $return["filestorage_id"];
          $data['url'] = $return["url"];
          $oRendicionCuentasDetalle->modificar_campo($modulo_id, 'tb_rendicioncuenta_detalle_archivo', 1, 'INT');
        }else{
          $data['estado'] = 0;
          $data['mensaje'] = $return["mensaje"];
        }
      } else {
        $data['estado'] = 0;
        $data['mensaje'] = "TIPO DE ARCHIVO NO VALIDO";
      }
    }
    else{
      $data['estado'] = 0;
      $data['mensaje'] = "NO HAY NINGUN ARCHIVO PARA SUBIR";
    }
    echo json_encode($data);
 	}
 	elseif($action == 'eliminar'){
 		$filestorage_id = intval($_POST['hdd_filestorage_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al filestorage.';

    $ruta_imagen = '';
    //? VAMOS A OBTENER LA URL DE LA IMAGEN PARA PODER ELIMINARLA POR COMPLETO DEL DIRECTORIO
    $result = $oFilestorage->mostrarUno($filestorage_id);
      if($result['estado'] == 1){
        $ruta_imagen = '../../'.$result['data']['filestorage_url'];
      }
    $result = NULL;
    
    if($ruta_imagen != ''){
      // Verificar si la imagen existe
      if (file_exists($ruta_imagen)) {
        // Intentar eliminar la imagen
        if (unlink($ruta_imagen)) {
          if($oFilestorage->eliminar($filestorage_id)){
            $data['estado'] = 1;
            $data['mensaje'] = 'Imagen de garantía eliminado correctamente.';
          }
        }else {
          $data['mensaje'] = 'Existe un error al eliminar el documento, se rechaza la eliminacion de la imagen en el directorio.';
        }
      }else {
        $data['mensaje'] = "La imagen no existe en el directorio: ".$ruta_imagen;
      }
    }

 		echo json_encode($data);
 	}
   elseif($action == 'eliminar_submodulo'){
    $modulo_nom = $_POST['modulo_nom'];
    $modulo_id = $_POST['modulo_id'];
    $modulo_subnom = $_POST['modulo_subnom'];

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar al filestorage.';

   $ruta_imagen = '';
   $filestorage_id = 0;
   //? VAMOS A OBTENER LA URL DE LA IMAGEN PARA PODER ELIMINARLA POR COMPLETO DEL DIRECTORIO
    $result = $oFilestorage->mostrarUnoSubmodulo($modulo_nom, $modulo_subnom, $modulo_id);
      if($result['estado'] == 1){
        $ruta_imagen = '../../'.$result['data']['filestorage_url'];
        $filestorage_id = $result['data']['filestorage_id'];
      }
    $result = NULL;
   
   if($ruta_imagen != ''){
     // Verificar si la imagen existe
     if (file_exists($ruta_imagen)) {
       // Intentar eliminar la imagen
       if (unlink($ruta_imagen)) {
         if($oFilestorage->eliminar($filestorage_id)){
           $data['estado'] = 1;
           $data['mensaje'] = 'Imagen de garantía eliminado correctamente.';
         }
       }else {
         $data['mensaje'] = 'Existe un error al eliminar el documento, se rechaza la eliminacion de la imagen en el directorio.';
       }
     }else {
       $data['mensaje'] = "La imagen no existe en el directorio: ".$ruta_imagen;
     }
   }

    echo json_encode($data);
  }
 	else{
	 	echo 'No se ha identificado ningún tipo de transacción para '.$action;
 	}

?>
