<?php
  if(defined('VISTA_URL')){
    require_once(APP_URL.'core/usuario_sesion.php');
    require_once(VISTA_URL.'filestorage/filestorage.class.php');
  }
  else{
    require_once('../../core/usuario_sesion.php');
    require_once('../filestorage/filestorage.class.php');
  }
  $ofilestorage = new filestorage();

  $modulo_id = (isset($_POST['modulo_id']))? $_POST['modulo_id'] : intval($modulo_id);
  $modulo_nom = (isset($_POST['modulo_nom']))? $_POST['modulo_nom'] : intval($modulo_nom);
  $galeria = '';
  $result = $ofilestorage->listar_filestorages($modulo_nom, $modulo_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $galeria .= '
          <div class="col-md-12 col-xs-12">
            <br>
            <a href="'.$value['filestorage_url'].'" target="_blank"><img class="img-responsive" src="'.$value['filestorage_url'].'"  style="width: 100%"></a>
          </div>
          <p>
        ';
      }
    }
    else{
      $galeria = '
          <div class="alert alert-error alert-dismissible">
            <h4>
              <i class="icon fa fa-info"></i> Este módulo no tiene imágenes para mostrar
            </h4>
          </div>';
    }
  $result = NULL;
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_filestorage_simple" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria">Imagen</h4>
      </div>
      <div class="modal-body">
        <div class="box box-primary">
          <div class="box-header">
            <div class="row">
              <div class="col-md-12">
                <?php echo $galeria;?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>         
      </div>
    </div>
  </div>
</div>