$(document).ready(function () {
  // $('#btn_cerrar_filestorage').click(function (event) {
  //     filestorage_galeria();
  // }); filestorage
  $("#file_filestorage").uploadifive({
    auto: false,
    //'checkScript'      : 'programacion_file_check.php',
    formData: {
      action: "insertar2",
      public_carpeta: $('#hdd_public_carpeta').val(),
      modulo_nom: $("#hdd_modulo_nom").val(),
      modulo_id: $("#hdd_modulo_id").val(),
      modulo_subnom: $("#hdd_modulo_subnom").val(),
      filestorage_uniq: $("#hdd_filestorage_uniq").val(),
      filestorage_des: $('#hdd_filestorage_des').val() 
    },
    queueID: "queue",
    uploadScript: VISTA_URL + "filestorage/filestorage_controller.php",
    queueSizeLimit: 10,
    uploadLimit: 10,
    multi: true,
    buttonText: "Seleccionar Archivo",
    height: 20,
    width: 180,
    fileSizeLimit: "5MB",
    fileType: ["image/gif", "image/jpeg", "image/png", "image/jpg", "pdf"],
    onUploadComplete: function (file, data) {
      let datos = JSON.parse(data);
      console.log(datos)
      if(datos.estado == 1){
        $("#hdd_filestorageid").val(datos.filestorage_id)
        $("#hdd_url").val(datos.url)
        alerta_success("EXITO", data.mensaje)
        rendicioncuentadetalle_tabla()
        $("#modal_registro_filestorage").hide()
      }else{
        alerta_error("ERROR", data.mensaje)
      }
      /*if (data != "exito") {
        $("#alerta_documento").show(300);
        $("#alerta_documento").append(
          "<strong>El archivo: " +
            file.name +
            ", no se pudo subir -> " +
            data +
            "</strong><br>"
        );
      }*/
    },
    onError: function (errorType) {
      var message = "Error desconocido.";
      if (errorType == "FILE_SIZE_LIMIT_EXCEEDED") {
        message = "Tamaño de archivo debe ser menor a 3MB.";
      } else if (errorType == "FORBIDDEN_FILE_TYPE") {
        message = "Tipo de archivo no válido.";
        alerta_error('IMPORTANTE', message);
      }
      //$(".filestorageifive-queue-item.error").hide();
      //
      //$.FormFeedbackError(message);
    },
    onAddQueueItem: function (file) {
      
    }
  });
});
