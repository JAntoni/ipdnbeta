<?php
	session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  $tipo = $_POST['tipo'];
  $arr_url_json = json_decode($_POST['arr_url_img']);
  $arr_url_img = array(json_decode($_POST['arr_url_img'], true));
  $url_pdf = $_POST['url_pdf'];
  //echo $arr_url_img;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_filestorage_archivo">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Galería de Imágenes</h4>
      </div>
      <div class="modal-body">
      <?php 
      	if($tipo == 1 && count($arr_url_img) == 1){ ?>
	        <div class="carousel slide" data-ride="carousel" id="carousel-example-generic" style="width: 100%; height: 60vh;">
						<ol class="carousel-indicators">
							<?php 
	    					foreach ($arr_url_img as $key => $value): ?>
								<li data-target="#carousel-example-generic" data-slide-to="<?php echo $cont;?>" <?php if($cont == 0) echo 'class="active"';?> ></li>
								<?php
								$cont ++;
							endforeach;
							?>
						</ol>
						<div class="carousel-inner" style="width: 100%; height: 100%;">
							<?php 
								$cont = 0;
	    					foreach ($arr_url_img as $key => $value):  
                			?>
								<div <?php ($cont == 0)? print 'class="item active"' : print 'class="item"';?> style="width: auto; height: 100%; max-width: 100%; display: block; margin: auto;">
										<img src="<?php echo $value[$key];?>" style="width: 100%; height: 100%; margin: auto;">
									<div class="carousel-caption"><?php echo 'Foto '.$cont;?></div>
								</div>
								<?php
								$cont ++;
							endforeach;
							?>
							<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
								<span class="fa fa-angle-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
								<span class="fa fa-angle-right"></span>
							</a>
						</div>
					</div>
					<?php 
		}
		else if($tipo == 1 && count($arr_url_img) == 0){
			echo '<h4>NO SE HAN ENCONTRADO FOTOS</h4>';
        }
        else if($tipo == 2 && $url_pdf != ""){ ?>
          <iframe src="<?php echo $url_pdf;?>" style="width: 100%; height: 500px;"></iframe>
        <?php 
        }else if($tipo == 2 && $url_pdf == ""){
          echo '<h4>NO SE HAN ENCONTRADO PDF</h4>';
        }else{
			echo '<h4>HA OCURRIDO UN ERROR</h4>';
		}
		?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
			