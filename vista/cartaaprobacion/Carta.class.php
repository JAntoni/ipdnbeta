<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Carta extends Conexion {

    function insertar($transferente_id, $cliente_id, $carta_vehpre, $carta_vehini, $carta_cre, $carta_vehmar, $carta_vehmod, $carta_vehanio, $carta_vehcol, $usuario_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_carta(tb_transferente_id, tb_cliente_id, tb_carta_vehpre, tb_carta_vehini, tb_carta_cre, tb_carta_vehmar, tb_carta_vehmod, tb_carta_vehanio, tb_carta_vehcol, tb_usuario_id)
          VALUES (:transferente_id, :cliente_id, :carta_vehpre, :carta_vehini, :carta_cre, :carta_vehmar, :carta_vehmod, :carta_vehanio, :carta_vehcol, :usuario_id)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":transferente_id", $transferente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehpre", $carta_vehpre, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehini", $carta_vehini, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_cre", $carta_cre, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehmar", $carta_vehmar, PDO::PARAM_STR);
            $sentencia->bindParam(":carta_vehmod", $carta_vehmod, PDO::PARAM_STR);
            $sentencia->bindParam(":carta_vehanio", $carta_vehanio, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehcol", $carta_vehcol, PDO::PARAM_STR);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

            $result['estado'] = $sentencia->execute();
            $result['nuevo'] = $this->dblink->lastInsertId();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar($carta_id, $transferente_id, $cliente_id, $carta_vehpre, $carta_vehini, $carta_cre, $carta_vehmar, $carta_vehmod, $carta_vehanio, $carta_vehcol, $usuario_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_carta 
          SET tb_transferente_id =:transferente_id, tb_cliente_id =:cliente_id, 
          tb_carta_vehpre =:carta_vehpre, tb_carta_vehini =:carta_vehini, 
          tb_carta_cre =:carta_cre, tb_carta_vehmar =:carta_vehmar, 
          tb_carta_vehmod =:carta_vehmod, tb_carta_vehanio =:carta_vehanio,
          tb_carta_vehcol =:carta_vehcol, tb_usuario_id =:usuario_id WHERE tb_carta_id =:carta_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":transferente_id", $transferente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehpre", $carta_vehpre, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehini", $carta_vehini, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_cre", $carta_cre, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehmar", $carta_vehmar, PDO::PARAM_STR);
            $sentencia->bindParam(":carta_vehmod", $carta_vehmod, PDO::PARAM_STR);
            $sentencia->bindParam(":carta_vehanio", $carta_vehanio, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_vehcol", $carta_vehcol, PDO::PARAM_STR);
            $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":carta_id", $carta_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar($carta_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_carta SET tb_carta_xac = 0 WHERE tb_carta_id =:carta_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":carta_id", $carta_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($carta_id) {
        try {
            $sql = "SELECT ca.*, tr.tb_transferente_doc, tr.tb_transferente_nom, 
            cli.tb_cliente_doc, cli.tb_cliente_nom, CONCAT(usu.tb_usuario_nom,' ',usu.tb_usuario_ape) AS asesor,
            tb_cliente_tip, tb_cliente_emprs, tb_cliente_empruc
            FROM tb_carta ca
            INNER JOIN tb_transferente tr ON ca.tb_transferente_id = tr.tb_transferente_id
            INNER JOIN tb_usuario usu ON ca.tb_usuario_id = usu.tb_usuario_id
            INNER JOIN tb_cliente cli ON ca.tb_cliente_id = cli.tb_cliente_id 
            WHERE ca.tb_carta_id =:carta_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":carta_id", $carta_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay cartas de aprobación registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_cartas($fecha_ini, $fecha_fin) {
        try {
            $sql = "SELECT ca.*, tr.tb_transferente_nom, cli.tb_cliente_nom, CONCAT(usu.tb_usuario_nom,' ',usu.tb_usuario_ape) AS asesor
            FROM tb_carta ca
            INNER JOIN tb_transferente tr ON ca.tb_transferente_id = tr.tb_transferente_id
            INNER JOIN tb_usuario usu ON ca.tb_usuario_id = usu.tb_usuario_id
            INNER JOIN tb_cliente cli ON ca.tb_cliente_id = cli.tb_cliente_id
            WHERE ca.tb_carta_reg >= :fecha_ini AND ca.tb_carta_reg <= DATE_ADD(:fecha_fin, INTERVAL 1 DAY) AND ca.tb_carta_xac = 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>
