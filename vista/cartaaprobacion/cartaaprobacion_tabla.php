<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
    echo 'Terminó la sesión';
    exit();
}
if (defined('VISTA_URL')) {
    require_once(VISTA_URL . 'cartaaprobacion/Carta.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
} else {
    require_once('../cartaaprobacion/Carta.class.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
}
$oCarta = new Carta();
$fecha_ini = fecha_mysql($_POST['fecha_ini']);
$fecha_fin = fecha_mysql($_POST['fecha_fin']);
?>
<table id="tbl_cartaaprobacion" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">ASESOR</th>
            <th id="tabla_cabecera_fila">VALOR VEHÍCULO</th>
            <th id="tabla_cabecera_fila">MONTO INICIAL</th>
            <th id="tabla_cabecera_fila">CRÉDITO APROBADO</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">CONCESIONARIO</th>
            <th id="tabla_cabecera_fila" width="6%"></th>
        </tr>
    </thead>
    <tbody>
<?php
//PRIMER NIVEL
$result = $oCarta->listar_cartas($fecha_ini, $fecha_fin);
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value):
        ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $value['tb_carta_id']; ?></td>
                    <td align="left" id="tabla_fila"><?php echo $value['asesor']; ?></td>
                    <td align="right" id="tabla_fila"><?php echo mostrar_moneda($value['tb_carta_vehpre']); ?></td>
                    <td align="right" id="tabla_fila"><?php echo mostrar_moneda($value['tb_carta_vehini']); ?></td>
                    <td align="right" id="tabla_fila"><?php echo mostrar_moneda($value['tb_carta_cre']); ?></td>
                    <td align="left" id="tabla_fila"><?php echo $value['tb_cliente_nom']; ?></td>
                    <td align="left" id="tabla_fila"><?php echo $value['tb_transferente_nom']; ?></td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-warning btn-xs" title="Editar" onclick="cartaaprobacion_form(<?php echo "'M', " . $value['tb_carta_id']; ?>)"><i class="fa fa-edit"></i> Editar</a>
                        <a class="btn btn-danger btn-xs" title="Eliminar" onclick="cartaaprobacion_form(<?php echo "'E', " . $value['tb_carta_id']; ?>)"><i class="fa fa-trash"></i> Eliminar</a>
                        <a class="btn bg-purple btn-xs" title="Descargar Documento PDF" onclick="reportePDF(<?php echo $value['tb_carta_id']; ?>)"><i class="fa fa-file-pdf-o"></i> PDF</a>
                    </td>
                </tr>
                <?php
            endforeach;
        }
        $result = NULL;
        ?>
    </tbody>
</table>
