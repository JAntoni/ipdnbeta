
$(document).ready(function () {

	$("#txt_transferente_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "transferente/transferente_autocomplete.php",
                    {term: request.term}, 
                    response
                    );
        },
        select: function (event, ui) {
			console.log(ui.item);
            $('#hdd_transferente_id').val(ui.item.transferente_id);
            $('#txt_transferente_nom').val(ui.item.transferente_nom);
            $('#txt_transferente_doc').val(ui.item.transferente_doc);
            event.preventDefault();
            $('#txt_transferente_nom').focus();
        }
    });

	$("#txt_cliente_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, 
                    response
                    );
        },
        select: function (event, ui) {
			console.log(ui.item);
            $('#hdd_cliente_id').val(ui.item.cliente_id);
            $('#txt_cliente_nom').val(ui.item.cliente_nom);
            $('#txt_cliente_doc').val(ui.item.cliente_doc);
            event.preventDefault();
            $('#txt_cliente_nom').focus();
        }
    });

	$('#form_cartaaprobacion').validate({
		submitHandler: function () {
			$.ajax({
				type: "POST",
				url: VISTA_URL + "cartaaprobacion/carta_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_cartaaprobacion").serialize(),
				beforeSend: function () {
					$('#cartaaprobacion_mensaje').show(400);
					$('#btn_guardar_cartaaprobacion').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						$('#cartaaprobacion_mensaje').removeClass('callout-info').addClass('callout-success')
						$('#cartaaprobacion_mensaje').html(data.mensaje);

						cartaaprobacion_tabla();

						/* var vista = $('#cartaaprobacion_vista').val();
						setTimeout(function () {
							if (vista == 'cartaaprobacion')
								cartaaprobacion_tabla();
							$('#modal_registro_cartaaprobacion').modal('hide');
							if (vista == 'compracontadoc') {
								llenar_datos_cartaaprobacion(data);
							}
						}, 1000
						); */
					}
					else {
						$('#cartaaprobacion_mensaje').removeClass('callout-info').addClass('callout-warning')
						$('#cartaaprobacion_mensaje').html('Alerta: ' + data.mensaje);
						$('#btn_guardar_cartaaprobacion').prop('disabled', false);
					}
				},
				complete: function (data) {
					$('#modal_registro_cartaaprobacion').modal('hide');
				},
				error: function (data) {
					$('#cartaaprobacion_mensaje').removeClass('callout-info').addClass('callout-danger')
					$('#cartaaprobacion_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
		},
		rules: {
			
			txt_transferente_nom: {
				required: true,
				minlength: 2
			},
			txt_transferente_doc: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 11
			},
			txt_cliente_nom: {
				required: true,
				minlength: 2
			},
			txt_cliente_doc: {
				required: true,
				digits: true,
				minlength: 8,
				maxlength: 11
			},
			txt_precio_vehiculo: {
				required: true
			},
			txt_monto_inicial: {
				required: true
			},
			txt_monto_credito: {
				required: true
			},
			txt_marca: {
				required: true
			},
			txt_modelo: {
				required: true
			},
			txt_anio_modelo: {
				required: true
			},
			txt_color: {
				required: true
			}

		},
		messages: {

			txt_transferente_nom: {
				required: "Ingrese un nombre para el Transferente",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_transferente_doc: {
				required: "Ingrese el Documento del Transferente",
				digits: "Solo números por favor",
				minlength: "El Documento como mínimo debe tener 8 números",
				maxlength: "El Documento como máximo debe tener 11 números"
			},
			txt_cliente_nom: {
				required: "Ingrese un nombre para el Cliente",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_cliente_doc: {
				required: "Ingrese el Documento del Cliente",
				digits: "Solo números por favor",
				minlength: "El Documento como mínimo debe tener 8 números",
				maxlength: "El Documento como máximo debe tener 11 números"
			},
			txt_precio_vehiculo: {
				required: "Ingrese un monto válido",
			},
			txt_monto_inicial: {
				required: "Ingrese un monto válido",
			},
			txt_monto_credito: {
				required: "Ingrese un monto válido",
			},
			txt_marca: {
				required: "Ingrese un nombre para el Marca"
			},
			txt_modelo: {
				required: "Ingrese un nombre para el Modelo"
			},
			txt_anio_modelo: {
				required: "Ingrese un nombre para el Año del Modelo"
			},
			txt_color: {
				required: "Ingrese un nombre para el Color"
			}

		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "radio") {
				error.insertAfter($("#radio_persjur"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});
});

function solo_decimal(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if((charCode > 31 && (charCode < 46 || charCode > 57))||charCode==47){
		return false;
	}
	return true;
}

function solo_numero(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode > 31 && (charCode < 48 || charCode > 57)){
		return false;
	}
	return true;
} 

function montoTotalPrecio(monto) {
	var inicial = parseFloat($("#txt_monto_inicial").val());
	if(inicial>0){
		if(inicial>monto){
			Swal.fire(
          'Error!',
          "Error: La inicial no puede ser mayor que el precio del vehículo.",
          'error'
        )
		}else{
			var diff = parseFloat(monto) - inicial;
			$("#txt_monto_credito").val(diff);
		}
	}else{
		$("#txt_monto_credito").val(monto);
	}
}

function montoTotalInicial(inicial) {
	var monto = parseFloat($("#txt_precio_vehiculo").val());
	if(inicial>0){
		if(inicial>monto){
			Swal.fire(
          'Error!',
          "Error: La inicial no puede ser mayor que el precio del vehículo.",
          'error'
        )
		}else{
			var diff = parseFloat(monto) - inicial;
			$("#txt_monto_credito").val(diff);
		}
	}else{
		$("#txt_monto_credito").val(monto);
	}
}