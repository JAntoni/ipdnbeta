<?php
error_reporting(E_ERROR | E_PARSE);

define('FPDF_FONTPATH', 'font/');
//require_once('../../libreriasphp/html2pdf/_tcpdf_5.9.206/tcpdf.php');
require_once('../../static/tcpdf/tcpdf.php');

/* require_once ("../../config/Cado.php"); */
require_once ("../cartaaprobacion/Carta.class.php");
$oCarta = new Carta();

/* require_once("../formatos/formato.php");
require_once("../formatos/numletras.php");
require_once("../formatos/fechas.php");
require_once("../formatos/operaciones.php"); */

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$title = 'Carta de Aprobación';
$codigo = 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT);

class MYPDF extends TCPDF {

  public function Header() {
    //$image_file = K_PATH_IMAGES.'logo.jpg';
    //$this->Image($image_file, 10, 10, 40, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    //$this->SetFont('helvetica', 'B', 20);
    // Title
    //$this->Cell(0, 15, 'CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    //$this->Cell(0, 0, 'CAD-'.str_pad($_GET['carta_id'], 7, "0", STR_PAD_LEFT), 0, 1, 'C', 0, 'nil', 0, false, 'M', 'M');
  }

  public function Footer() {

    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'padding' => 0,
      'fgcolor' => array(0,0,0),
      'bgcolor' => false,
      'text' => false
    //     'font' => 'helvetica',
    //     'fontsize' => 8,
    //     'stretchtext' => 4
    );

    $this -> SetY(-24);
    // Page number
    $this->SetFont('helvetica', '', 9);
    $this->SetTextColor(0,0,0);
    $this->Cell(0, 0, 'Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');
    
    $codigo='CAV-'.str_pad($_GET['d1'], 4, "0", STR_PAD_LEFT);
    
    $this->write1DBarcode($codigo, 'C128', '', 273, '', 6, 0.3, $style, 'N');
    $this->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M');        
  }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('www.inticap.com');
  $pdf->SetTitle($title);
  $pdf->SetSubject('www.inticap.com');
  $pdf->SetKeywords('www.inticap.com');

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(20, 20, 20);// left top right
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 30);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  //$pdf->setLanguageArray();

  // ---------------------------------------------------------
  // add a page
$pdf->AddPage('P', 'A4');

$result = $oCarta->mostrarUno($_GET['carta_id']);
if($result['estado'] == 1){
  $transferente_id = $result['data']['tb_transferente_id'];
  $transferente_nom = $result['data']['tb_transferente_nom'];
  $transferente_doc = $result['data']['tb_transferente_doc'];
  $cliente_id = $result['data']['tb_cliente_id'];
  $tipo = intval($result['data']['tb_cliente_tip']);
  $persona_nom = '<b>Sr(a):</b>';
  $persona_doc = '<b>DNI:</b>';

  if($tipo == 2){
    $cliente_nom = $result['data']['tb_cliente_emprs'];
    $cliente_doc = $result['data']['tb_cliente_empruc'];

    $persona_nom = '<b>Emp:</b>';
    $persona_doc = '<b>RUC:</b>';
  }
  else{
    $cliente_nom = $result['data']['tb_cliente_nom'];
    $cliente_doc = $result['data']['tb_cliente_doc'];
  }
  
  $carta_vehpre = $result['data']['tb_carta_vehpre'];
  $carta_vehini = $result['data']['tb_carta_vehini'];
  $carta_cre = $result['data']['tb_carta_cre']; 
  $carta_vehmar = $result['data']['tb_carta_vehmar'];
  $carta_vehmod = $result['data']['tb_carta_vehmod'];
  $carta_vehanio = $result['data']['tb_carta_vehanio'];
  $carta_vehcol = $result['data']['tb_carta_vehcol'];      

}
$result = NULL;
/* $dt = $oCarta->mostrarUno($_GET['carta_id']);
  //$dt = mysql_fetch_array($dts);
    $transferente_id = $dt['tb_transferente_id'];
    $transferente_nom = $dt['tb_transferente_nom'];
    $transferente_doc = $dt['tb_transferente_doc'];
    $cliente_id = $dt['tb_cliente_id'];
    $tipo = intval($dt['tb_cliente_tip']);
    $persona_nom = '<b>Sr(a):</b>';
    $persona_doc = '<b>DNI:</b>';

    if($tipo == 2){
      $cliente_nom = $dt['tb_cliente_emprs'];
      $cliente_doc = $dt['tb_cliente_empruc'];

      $persona_nom = '<b>Emp:</b>';
      $persona_doc = '<b>RUC:</b>';
    }
    else{
      $cliente_nom = $dt['tb_cliente_nom'];
      $cliente_doc = $dt['tb_cliente_doc'];
    }
    
    $carta_vehpre = $dt['tb_carta_vehpre'];
    $carta_vehini = $dt['tb_carta_vehini'];
    $carta_cre = $dt['tb_carta_cre']; 
    $carta_vehmar = $dt['tb_carta_vehmar'];
    $carta_vehmod = $dt['tb_carta_vehmod'];
    $carta_vehanio = $dt['tb_carta_vehanio'];
    $carta_vehcol = $dt['tb_carta_vehcol'];

  $result = null; */

  $html= '
    <table>
      <tr>
        <td colspan="5"><img src="'.K_PATH_IMAGES.'logo.jpg'.'" style="with: 40px; height: 50px;"></td>
        <td colspan="12" style="text-align: center;"><br/><br/><strong><u>CARTA DE APROBACIÓN DE CRÉDITO VEHICULAR N°</u></strong></td>
        <td colspan="3"><br/><br/><b>'.$codigo.'</b></td>
      </tr>
    </table>
    <br/>
    <br/>
    <table>
      <tr>
        <td colspan="5"></td>
        <td colspan="12" style="text-align: center;">
          <b><u>INVERSIONES Y PRÉSTAMOS DEL NORTE SAC</u></b>
          <br/>Av. Mariscal Nieto N° 480, Ext. A – 7 Primer Piso
          <br/>Chiclayo, Chiclayo, Lambayeque
        </td>
        <td colspan="3"></td>
      </tr>
    </table>
    <br/>
    <br/>
    <table>
      <tr>
        <td colspan="5"></td>
        <td colspan="9" style="text-align: center;"></td>
        <td colspan="6">'.get_nombre_dia(date('Y-m-d')).', '.fechaActual(date('Y-m-d')).'</td>
      </tr>
    </table>

    <p align="justify">Sres: <b>'.$transferente_nom.'</b></p>
    <tr>
      <td></td>
      <td colspan="19">Estimados, con esta carta se le hace de su conocimiento la aceptación del financiamiento solicitado por nuestro cliente:</td>
    </tr>
    <p></p>
    <p align="justify">'.$persona_nom.' <b>'.$cliente_nom.'</b> <br/> '.$persona_doc.' <b>'.$cliente_doc.'</b></p>

    <tr>
      <td></td>
      <td colspan="19">Dicha  aprobación  tiene  como  finalidad  la  adquisición  mediante  Crédito  de  Garantía  Mobiliaria  del vehículo cuyos detalles se describen a continuación</td>
    </tr>
    <p></p>
    <p>DEL CRÉDITO:</p>
    <tr>
      <td colspan="8"><b>VALOR DEL VEHÍCULO:</b></td>
      <td colspan="8">$ '.mostrar_moneda($carta_vehpre).'</td>
    </tr>
    <tr>
      <td colspan="8"><b>INICIAL:</b></td>
      <td colspan="8">$ '.mostrar_moneda($carta_vehini).'</td>
    </tr>
    <tr>
      <td colspan="8"><b>CRÉDITO APROBADO:</b></td>
      <td colspan="8">$ '.mostrar_moneda($carta_cre).'</td>
    </tr>
    <p></p>
    <p>DEL VEHÍCULO:</p>

    <tr>
      <td colspan="8"><b>MARCA:</b></td>
      <td colspan="8">'.$carta_vehmar.'</td>
    </tr>
    <tr>
      <td colspan="8"><b>MODELO:</b></td>
      <td colspan="8">'.$carta_vehmod.'</td>
    </tr>
    <tr>
      <td colspan="8"><b>AÑO DE MODELO:</b></td>
      <td colspan="8">'.$carta_vehanio.'</td>
    </tr>
    <tr>
      <td colspan="8"><b>COLOR:</b></td>
      <td colspan="8">'.$carta_vehcol.'</td>
    </tr>
    
    <p></p>

    <tr>
      <td></td>
      <td colspan="19">Informamos que el cliente ha aprobado los filtros pertinentes para calificar para  dicho  financiamiento mediante modalidad de  PRE-COSTITUCIÓN DE GARANTÍA MOBILIARIA.</td>
    </tr>
    <p></p>
    <tr>
      <td></td>
      <td colspan="19">Cabe resaltar que la vigencia de esta aprobación es de 7 días calendarios contados a partir de la fecha de emisión de la presente y está sujeta al depósito de inicial del cliente dentro de la fecha indicada.</td>
    </tr>
    <p></p>
    <tr>
      <td></td>
      <td colspan="19">Siendo como se menciona, nos encontramos a la espera de la entrega de la <b>carta  de  características</b> de la unidad y de sus números de cuenta para realizar los depósitos pertinentes.</td>
    </tr>
    <p></p>
    <tr>
      <td></td>
      <td colspan="19">Sin más por el momento nos despedimos.</td>
    </tr>
    
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>

    <tr>
      <td colspan="20" align="center">_______________________________________ <br/>
      GALVEZ ROJAS HERBERT ENGEL <br/>
      SUB GRTE GRAL INVERSIONES Y PRÉSTAMOS DEL NORTE SAC
      </td>
    </tr>
  ';

// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

$pdf->Ln();

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$codigo."_46464".$title.".pdf";
ob_end_clean();
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>

