<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../cartaaprobacion/Carta.class.php');
  $oCarta = new Carta();
  require_once('../funciones/funciones.php');

  $direc = 'cartaaprobacion';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $cartaaprobacion_id = $_POST['cartaaprobacion_id'];
  $vista = $_POST['vista'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Carta de Aprobación Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Carta de Aprobación';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Carta de Aprobación';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Carta de Aprobación';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cartaaprobacion
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'cartaaprobacion'; $modulo_id = $cartaaprobacion_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cartaaprobacion por su ID
    $usuario_id = 0;
    if(intval($cartaaprobacion_id) > 0){
      $result = $oCarta->mostrarUno($cartaaprobacion_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para la cartaa de probación seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $transferente_id = $result['data']['tb_transferente_id'];
          $transferente_doc = $result['data']['tb_transferente_doc'];
          $transferente_nom = $result['data']['tb_transferente_nom'];
          $cliente_id = $result['data']['tb_cliente_id'];
          $cliente_doc = $result['data']['tb_cliente_doc'];
          $cliente_nom = $result['data']['tb_cliente_nom'];
          $usuario_id = $result['data']['tb_usuario_id'];
          $carta_vehpre = $result['data']['tb_carta_vehpre'];
          $carta_vehini = $result['data']['tb_carta_vehini'];
          $carta_cre = $result['data']['tb_carta_cre'];
          $carta_vehmar = $result['data']['tb_carta_vehmar'];
          $carta_vehmod = $result['data']['tb_carta_vehmod'];
          $carta_vehanio = $result['data']['tb_carta_vehanio'];
          $carta_vehcol = $result['data']['tb_carta_vehcol'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cartaaprobacion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_cartaaprobacion" method="post">
          <input type="hidden" id="cartaaprobacion_vista" value="<?php echo $vista;?>">
          <input type="hidden" name="action" id="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $usuario_id;?>">
          <input type="hidden" name="hdd_transferente_id" id="hdd_transferente_id" value="<?php echo $transferente_id;?>">
          <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          <input type="hidden" name="hdd_cartaaprobacion_id" id="hdd_cartaaprobacion_id" value="<?php echo $cartaaprobacion_id;?>">
          
          <div class="modal-body">
            
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label for="txt_transferente_nom" class="control-label">Nombre de Transferente</label>
                  <input type="text" name="txt_transferente_nom" id="txt_transferente_nom" class="form-control input-sm " placeholder="Ingrese transferente" value="<?php echo $transferente_nom;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_transferente_doc" class="control-label">RUC/DNI Transferente</label>
                  <input type="text" name="txt_transferente_doc" id="txt_transferente_doc" onkeypress="return solo_numero(event)" class="form-control input-sm" readonly value="<?php echo $transferente_doc;?>">
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label for="txt_cliente_nom" class="control-label">Nombre de Cliente</label>
                  <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm " placeholder="Ingrese cliente" value="<?php echo $cliente_nom;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_cliente_doc" class="control-label">RUC/DNI Cliente</label>
                  <input type="text" name="txt_cliente_doc" id="txt_cliente_doc" onkeypress="return solo_numero(event)" class="form-control input-sm" readonly value="<?php echo $cliente_doc;?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_precio_vehiculo" class="control-label">Precio del Vehículo</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                    <input type="text" name="txt_precio_vehiculo" id="txt_precio_vehiculo" onchange="montoTotalPrecio(this.value)" onkeypress="return solo_decimal(event)" class="form-control input-sm" placeholder="Ingrese precio del vehículo" style="text-align: right;" value="<?php echo $carta_vehpre;?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_monto_inicial" class="control-label">Monto de Inicial</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                    <input type="text" name="txt_monto_inicial" id="txt_monto_inicial" onchange="montoTotalInicial(this.value)" onkeypress="return solo_decimal(event)" class="form-control input-sm" placeholder="Ingrese monto de inicial" style="text-align: right;" value="<?php echo $carta_vehini;?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="txt_monto_credito" class="control-label">Monto del Crédito</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                    <input type="text" name="txt_monto_credito" id="txt_monto_credito" onkeypress="return solo_decimal(event)" class="form-control input-sm" placeholder="Ingrese monto del crédito" style="text-align: right;" value="<?php echo $carta_cre;?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_marca" class="control-label">Marca</label>
                  <input type="text" name="txt_marca" id="txt_marca" class="form-control input-sm" placeholder="Ingrese marca" value="<?php echo $carta_vehmar;?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_modelo" class="control-label">Modelo</label>
                  <input type="text" name="txt_modelo" id="txt_modelo" class="form-control input-sm" placeholder="Ingrese modelo" value="<?php echo $carta_vehmod;?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_anio_modelo" class="control-label">Año de Modelo</label>
                  <input type="text" name="txt_anio_modelo" id="txt_anio_modelo" class="form-control input-sm" maxlength="4" placeholder="Ingrese año del modelo" onkeypress="return solo_numero(event)" value="<?php echo $carta_vehanio;?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txt_color" class="control-label">Color</label>
                  <input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="Ingrese color" value="<?php echo $carta_vehcol;?>">
                </div>
              </div>
            </div>
            

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Carta de Aprobación?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="cartaaprobacion_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cartaaprobacion">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_cartaaprobacion">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cartaaprobacion">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cartaaprobacion/cartaaprobacion_form.js?ver=08052023';?>"></script>
