<?php
	session_name("ipdnsac");
  	session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../cartaaprobacion/Carta.class.php');
  	$oCarta = new Carta();
	require_once('../funciones/funciones.php');

 	$action = $_POST['action'];

 	if($action == 'insertar'){
		if(intval($_POST['hdd_usuario_id'])>0){
			$usuario_id = intval($_POST['hdd_usuario_id']);
		}else{
			$usuario_id = $_SESSION['usuario_id'];
		}
 		$transferente_nom = mb_strtoupper($_POST['txt_transferente_nom'], 'UTF-8');
 		$transferente_doc = $_POST['txt_transferente_doc'];
 		$transferente_id = intval($_POST['hdd_transferente_id']);
		$cliente_nom = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
 		$cliente_doc = $_POST['txt_cliente_doc'];
 		$cliente_id = intval($_POST['hdd_cliente_id']);
 		$carta_vehpre = moneda_mysql($_POST['txt_precio_vehiculo']);
 		$carta_vehini = moneda_mysql($_POST['txt_monto_inicial']);
 		$carta_cre = moneda_mysql($_POST['txt_monto_credito']);
 		$carta_vehmar = $_POST['txt_marca'];
 		$carta_vehmod = $_POST['txt_modelo'];
 		$carta_vehanio = $_POST['txt_anio_modelo'];
 		$carta_vehcol = $_POST['txt_color'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar Carta de Aprobación.';
		$res= $oCarta->insertar($transferente_id, $cliente_id, $carta_vehpre, $carta_vehini, $carta_cre, $carta_vehmar, $carta_vehmod, $carta_vehanio, $carta_vehcol, $usuario_id);
 		if(intval($res['estado']) == 1){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Carta de Aprobación registrado correctamente.';
			$data['registro'] = $oCarta->mostrarUno($res['nuevo'])['data'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
		$carta_id = intval($_POST['hdd_cartaaprobacion_id']);
		$usuario_id = intval($_POST['hdd_usuario_id']);
 		$transferente_nom = mb_strtoupper($_POST['txt_transferente_nom'], 'UTF-8');
 		$transferente_doc = $_POST['txt_transferente_doc'];
 		$transferente_id = intval($_POST['hdd_transferente_id']);
		$cliente_nom = mb_strtoupper($_POST['txt_cliente_nom'], 'UTF-8');
 		$cliente_doc = $_POST['txt_cliente_doc'];
 		$cliente_id = intval($_POST['hdd_cliente_id']);
 		$carta_vehpre = moneda_mysql($_POST['txt_precio_vehiculo']);
 		$carta_vehini = moneda_mysql($_POST['txt_monto_inicial']);
 		$carta_cre = moneda_mysql($_POST['txt_monto_credito']);
 		$carta_vehmar = $_POST['txt_marca'];
 		$carta_vehmod = $_POST['txt_modelo'];
 		$carta_vehanio = $_POST['txt_anio_modelo'];
 		$carta_vehcol = $_POST['txt_color'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Carta de Aprobación.';

 		if($oCarta->modificar($carta_id, $transferente_id, $cliente_id, $carta_vehpre, $carta_vehini, $carta_cre, $carta_vehmar, $carta_vehmod, $carta_vehanio, $carta_vehcol, $usuario_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Carta de Aprobación modificado correctamente.';
			$data['registro'] = $oCarta->mostrarUno($carta_id)['data'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$carta_id = intval($_POST['hdd_cartaaprobacion_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Carta de Aprobación.';

 		if($oCarta->eliminar($carta_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Carta de Aprobación eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>