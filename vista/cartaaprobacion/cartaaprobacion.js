var datatable_global;

function cartaaprobacion_form(usuario_act, cartaaprobacion_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"cartaaprobacion/cartaaprobacion_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      cartaaprobacion_id: cartaaprobacion_id,
      vista: 'cartaaprobacion'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_cartaaprobacion_form').html(data);
      	$('#modal_registro_cartaaprobacion').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_cartaaprobacion'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_cartaaprobacion', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'cartaaprobacion';
      	var div = 'div_modal_cartaaprobacion_form';
      	permiso_solicitud(usuario_act, cartaaprobacion_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function cartaaprobacion_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cartaaprobacion/cartaaprobacion_tabla.php",
    async: true,
    dataType: "html",
    data: ({
      fecha_ini: $('#txt_resumen_fec1').val(),
      fecha_fin: $('#txt_resumen_fec2').val()
    }),
    beforeSend: function() {
      $('#cartaaprobacion_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_cartaaprobacion_tabla').html(data);
      $('#cartaaprobacion_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#cartaaprobacion_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_cartaaprobacion').DataTable({
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Página mostrada _PAGE_ de _PAGES_",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
      columnDefs: [
         { orderable: false, targets: [1, 2, 3, 4, 5, 6, 7] }
      ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}

function reportePDF(carta_id){
	window.open("vista/cartaaprobacion/cartaaprobacion_pdf.php?carta_id="+carta_id,"_blank");
}

$(document).ready(function () {
  estilos_datatable();
  cartaaprobacion_tabla();

  $('#datetimepicker1, #datetimepicker2').datepicker({
      language: 'es',
      autoclose: true,
      format: "dd-mm-yyyy",
  });

});