<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Menu.class.php');
  $oMenu = new Menu();

  $menu_idp = (empty($_POST['menu_idp']))? 0 : intval($_POST['menu_idp']);

?>
<table id="tbl_menus_orden" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Directiorio</th>
      <th>Orden</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oMenu->listar_menus($menu_idp);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value): ?>
        <tr>
          <td><?php echo $value['tb_menu_id'];?></td>
          <td>
            <?php 
              echo '
                <div>
                  <span class="fa fa-fw fa-chevron-circle-right"></span> '.$value['tb_menu_tit'].'
                </div>
              ';
            ?>
          </td>
          <td>
            <?php 
              echo $value['tb_menu_dir'];
            ?>
          </td>
          <td>
            <?php 
              echo '
                <input type="text" name="txt_menu_ord[]" id="txt_menu_ord" class="form-control input-sm txt_menu_ord" value="'.$value['tb_menu_ord'].'">
                <input type="hidden" name="txt_menu_id[]" value="'.$value['tb_menu_id'].'">
              ';
            ?>
          </td>
        </tr>
        <?php
      endforeach;
    }
    //FIN PRIMER NIVEL
    $result = NULL;
    ?>
  </tbody>
</table>
