<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Menu extends Conexion{

    function insertar($menu_tit, $menu_dir, $menu_des, $menu_ico, $menu_ord, $menu_idp){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_menu(tb_menu_nom, tb_menu_url, tb_menu_tit, tb_menu_idp, tb_menu_ven, tb_menu_des, tb_menu_ord, tb_menu_dir, tb_menu_ico) VALUES (:menu_tit, :menu_dir, :menu_tit, :menu_idp, :menu_tit, :menu_des, :menu_ord, :menu_dir, :menu_ico)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":menu_tit", $menu_tit, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_dir", $menu_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_tit", $menu_tit, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_idp", $menu_idp, PDO::PARAM_INT);
        $sentencia->bindParam(":menu_tit", $menu_tit, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_des", $menu_des, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_ord", $menu_ord, PDO::PARAM_INT);
        $sentencia->bindParam(":menu_dir", $menu_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_ico", $menu_ico, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($menu_id, $menu_tit, $menu_dir, $menu_des, $menu_ico, $menu_idp){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_menu SET tb_menu_tit =:menu_tit, tb_menu_dir =:menu_dir, tb_menu_des =:menu_des, tb_menu_ico =:menu_ico, tb_menu_idp =:menu_idp WHERE tb_menu_id =:menu_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":menu_id", $menu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":menu_tit", $menu_tit, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_dir", $menu_dir, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_des", $menu_des, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_ico", $menu_ico, PDO::PARAM_STR);
        $sentencia->bindParam(":menu_idp", $menu_idp, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($menu_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_menu WHERE tb_menu_id =:menu_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":menu_id", $menu_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar_orden_menu($menu_id, $menu_ord){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_menu SET tb_menu_ord =:menu_ord WHERE tb_menu_id =:menu_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":menu_ord", $menu_ord, PDO::PARAM_INT);
        $sentencia->bindParam(":menu_id", $menu_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($menu_id){
      try {
        $sql = "SELECT * FROM tb_menu WHERE tb_menu_id =:menu_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":menu_id", $menu_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function informacion_menu_directorio($menu_dir){
      try {
        $sql = "SELECT * FROM tb_menu WHERE tb_menu_dir =:menu_dir";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":menu_dir", $menu_dir, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_menus($menu_idpadre){
      try {
        $sql = "SELECT me.*, COUNT(me2.tb_menu_idp) as cant_submenu FROM tb_menu me left join tb_menu me2 on me2.tb_menu_idp = me.tb_menu_id where me.tb_menu_idp =:menu_idpadre group by me.tb_menu_id order by me.tb_menu_ord";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":menu_idpadre", $menu_idpadre, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "El usuario no tiene un menu asignado";
          $retorno["data"] = "";
        }

        return $retorno;

      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
