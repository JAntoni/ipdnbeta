<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../menu/Menu.class.php');
  $oMenu = new Menu();
  require_once('../funciones/funciones.php');

  $direc = 'menu';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id']; //1 ADMINISTRADOR, 2 VENDEDOR, 5 CONTABILIDAD
  $usuariogrupo_id = $_SESSION['usuariogrupo_id']; //2 GERENCIA, 3 VENTAS, 6 CONTABILIDAD
  $action = $_POST['action']; //para este caso el actios es ordenar
  $menu_id = $_POST['menu_id'];

  $titulo = 'Editar Orden del Menú al Mostrar';
  
  $bandera = 0; $mensaje = '';

  if($usuariogrupo_id == 2){
  	$bandera = 1;
  }
  else{
    $mensaje =  'No tienes permiso para editar el orden de los menús';
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal modal-wide fade" tabindex="-1" role="dialog" id="modal_ordenar_menu" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_menu_ordenar" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_menu_idp" class="control-label">Menú Padre</label>
              <select name="cmb_menu_ipd" id="cmb_menu_ipd" class="selectpicker form-control" data-live-search="true" data-max-options="1">
                <?php require_once('../menu/menu_select.php') ?>
              </select>
            </div>
            
            <!--- MESAJES DE LISTADO DE SUB MENUS -->
            <div class="callout callout-info" id="menu_tabla_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
            </div>

						<div id="div_menu_ordenar_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php require_once('../menu/menu_ordenar_tabla.php') ?>
						</div>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="menu_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <button type="submit" class="btn btn-info" id="btn_guardar_menu_ordenar">Guardar</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_menu">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/menu/menu_ordenar_form.js';?>"></script>
