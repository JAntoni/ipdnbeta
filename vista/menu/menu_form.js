
function menu_idpadre_seleccionado(){
	var action = $('#action').val();
	var menu_idp = $('#hdd_menu_idp').val();
	if(action && menu_idp && (action == 'modificar' || action == 'leer')){
		
	}
}
$(document).ready(function(){
  $('#cmb_menu_ipd').selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: 'ES'
  });

  $('#txt_menu_dir').keypress(function(e) {
  	var c = e.keyCode || e.which;
  	if (c === 0 || c === 32) {
    	return false;
  	}
  });
  
  $('#form_menu').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"menu/menu_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_menu").serialize(),
				beforeSend: function() {
					$('#menu_mensaje').show(400);
					$('#btn_guardar_menu').prop('disabled', true);
				},
				success: function(data){
					console.log(data);
					if(parseInt(data.estado) > 0){
						$('#menu_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#menu_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		menu_tabla();
		      		$('#modal_registro_menu').modal('hide');
		      		}, 1000
		      	);
					}
					else{
		      	$('#menu_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#menu_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_menu').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#menu_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#menu_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
	      	$('#btn_guardar_menu').prop('disabled', false);
				}
			});
	  },
	  rules: {
			txt_menu_tit: {
				required: true,
				minlength: 2
			},
			txt_menu_des: {
				required: true,
				minlength: 5
			},
			txt_menu_ico: {
				required: true,
				minlength: 2,
				nowhitespace: true
			}
		},
		messages: {
			txt_menu_tit: {
				required: "Ingrese un título al Menú",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_menu_des: {
				required: "Ingrese una descripción breve del Menú",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			},
			txt_menu_ico: {
				required: "Copie el nombre del ícono",
				minlength: "El nombre del ícono debe tener mínimo 2 letras",
				nowhitespace: "No espacios ni caracteres especiales"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
