<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Menu.class.php');
  $oMenu = new Menu();
?>
<table id="tbl_menus" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th> 
      <th>Descripción</th>
      <th>Directiorio</th>
      <th>Orden</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oMenu->listar_menus(0);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value): ?>
        <tr>
          <td><?php echo $value['tb_menu_id'];?></td>
          <td>
            <?php 
              $caret = (intval($value['cant_submenu']) > 0)? 'down': 'right';
              echo '
                <div>
                  <span class="fa fa-fw fa-chevron-circle-'.$caret.'"></span> '.$value['tb_menu_tit'].'
                </div>
              ';
            ?>
          </td>
          <td><?php echo $value['tb_menu_des']; ?></td>
          <td><?php echo $value['tb_menu_dir']; ?></td>
          <td><?php echo $value['tb_menu_ord']; ?></td>
          <td align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="menu_form(<?php echo "'L', ".$value['tb_menu_id'];?>)"><i class="fa fa-eye"></i></a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="menu_form(<?php echo "'M', ".$value['tb_menu_id'];?>)"><i class="fa fa-edit"></i></a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="menu_form(<?php echo "'E', ".$value['tb_menu_id'];?>)"><i class="fa fa-trash"></i></a>
            <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>Info</a>
          </td>
        </tr>
        <?php
        //SEGUNDO NIVEL
        $result2 = $oMenu->listar_menus($value['tb_menu_id']);
        if($result2['estado'] == 1){
          foreach ($result2['data'] as $key => $value): ?>
            <tr>
              <td><?php echo $value['tb_menu_id'];?></td>
              <td>
                <?php 
                  $caret = (intval($value['cant_submenu']) > 0)? 'down': 'right';
                  echo '
                    <div class="col-md-offset-1"> 
                      <span class="fa fa-fw fa-chevron-circle-'.$caret.'"></span> '.$value['tb_menu_tit'].'
                    </div>
                  ';
                ?>
              </td>
              <td><?php echo $value['tb_menu_des']; ?></td>
              <td><?php echo $value['tb_menu_dir']; ?></td>
              <td><?php echo '<div class="col-md-offset-1">'.$value['tb_menu_ord'].'</div>'; ?></td>
              <td align="center">
                <a class="btn btn-info btn-xs" title="Ver" onclick="menu_form(<?php echo "'L', ".$value['tb_menu_id'];?>)"><i class="fa fa-eye"></i></a>
                <a class="btn btn-warning btn-xs" title="Editar" onclick="menu_form(<?php echo "'M', ".$value['tb_menu_id'];?>)"><i class="fa fa-edit"></i></a>
                <a class="btn btn-danger btn-xs" title="Eliminar" onclick="menu_form(<?php echo "'E', ".$value['tb_menu_id'];?>)"><i class="fa fa-trash"></i></a>
                <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>Info</a>
              </td>
            </tr>
            <?php
            //TERCER NIVEL
            $result3 = $oMenu->listar_menus($value['tb_menu_id']);
            if($result3['estado'] == 1){
              foreach ($result3['data'] as $key => $value): ?>
                <tr>
                  <td><?php echo $value['tb_menu_id'];?></td>
                  <td>
                    <?php
                      $caret = (intval($value['cant_submenu']) > 0)? 'down': 'right';
                      echo '
                        <div class="col-md-offset-2">
                          <span class="fa fa-fw fa-chevron-circle-'.$caret.'"></span> '.$value['tb_menu_tit'].'
                        </div>
                      ';
                    ?>
                  </td>
                  <td><?php echo $value['tb_menu_des']; ?></td>
                  <td><?php echo $value['tb_menu_dir']; ?></td>
                  <td><?php echo '<div class="col-md-offset-2">'.$value['tb_menu_ord'].'</div>'; ?></td>
                  <td align="center">
                    <a class="btn btn-info btn-xs" title="Ver" onclick="menu_form(<?php echo "'L', ".$value['tb_menu_id'];?>)"><i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning btn-xs" title="Editar" onclick="menu_form(<?php echo "'M', ".$value['tb_menu_id'];?>)"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger btn-xs" title="Eliminar" onclick="menu_form(<?php echo "'E', ".$value['tb_menu_id'];?>)"><i class="fa fa-trash"></i></a>
                    <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>Info</a>
                  </td>
                </tr>
                <?php
                //FIN TERCER NIVEL
              endforeach;
            }
            $result3 = NULL;
            //FIN SEGUNDO NIVEL
          endforeach;
        }
        $result2 = NULL;
        //FIN PRIMER NIVEL
      endforeach;
    }
    $result = NULL;
    ?>
  </tbody>
</table>
