<?php
require_once('Menu.class.php');
$oMenu = new Menu();

$menu_idpadre = (empty($menu_idp))? 0 : $menu_idp; //ya que se obtiene en menu_form el id del menu padre de un submenu

$option = '<option value="0"></option>';
//PRIMER NIVEL
$result = $oMenu->listar_menus(0);
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($menu_idpadre == $value['tb_menu_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_menu_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_menu_tit'].'</option>';
	    //SEGUNDO NIVEL
	    $result2 = $oMenu->listar_menus($value['tb_menu_id']);
		    if($result2['estado'] == 1)
		    {
		    	$option .= '<optgroup>';
		      foreach ($result2['data'] as $key => $value)
		      {
		      	$selected = '';
				  	if($menu_idpadre == $value['tb_menu_id'])
				  		$selected = 'selected';

		        $option .= '<option value="'.$value['tb_menu_id'].'" '.$selected.'>'.$value['tb_menu_tit'].'</option>';	        
		      }
		      $option .= '</optgroup>';
		    }
	    $result2 = NULL;
	    //FIN SEGUNDO NIVEL
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>