<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../menu/Menu.class.php');
  $oMenu = new Menu();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$menu_tit = mb_strtoupper($_POST['txt_menu_tit'], 'UTF-8'); //titulo
 		$menu_dir = (empty($_POST['txt_menu_dir']))? '#' : strtolower($_POST['txt_menu_dir']); //directorio
 		$menu_des = $_POST['txt_menu_des']; //decripcion breve	
 		$menu_ico = $_POST['txt_menu_ico']; //icono que irá en el menu
 		$menu_ord = 1; //el orden en que se muestra el menu por defecto será 1
 		$menu_idp = intval($_POST['cmb_menu_ipd']); //id padre
 		

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Menú.';

 		if($oMenu->insertar($menu_tit, $menu_dir, $menu_des, $menu_ico, $menu_ord, $menu_idp)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Menú registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$menu_id = intval($_POST['hdd_menu_id']);
 		$menu_tit = mb_strtoupper($_POST['txt_menu_tit'], 'UTF-8'); //titulo
 		$menu_dir = (empty($_POST['txt_menu_dir']))? '#' : strtolower($_POST['txt_menu_dir']); //directorio
 		$menu_des = $_POST['txt_menu_des']; //decripcion breve	
 		$menu_ico = $_POST['txt_menu_ico']; //icono que irá en el menu
 		$menu_idp = intval($_POST['cmb_menu_ipd']); //id padre

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Menú.';

 		if($menu_id != $menu_idp){
	 		if($oMenu->modificar($menu_id, $menu_tit, $menu_dir, $menu_des, $menu_ico, $menu_idp)){
	 			$data['estado'] = 1;
	 			$data['mensaje'] = 'Menú modificado correctamente.';
	 		}
	 	}
	 	else
	 		$data['mensaje'] = 'El id del Menú padre no puede ser el mismo Menú';

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$menu_id = intval($_POST['hdd_menu_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Menú.';

	 	if($oMenu->eliminar($menu_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Menú eliminado correctamente.';
 		}


 		echo json_encode($data);
 	}

 	elseif($action == 'ordenar'){
 		$array_menu_orden = $_POST['txt_menu_ord'];
 		$array_menu_id = $_POST['txt_menu_id'];
 		$array_menu_orden = array_filter($array_menu_orden, 'strlen'); //si conservamos los ceros
 		$array_menu_id = array_filter($array_menu_id); //no conservamos los ceros, ya que no debe haber ID=0
	 	
	 	$data['estado'] = 0;
	 	$data['mensaje'] = 'Ha surgido un error al validar los datos de los menus';

	 	if(count($array_menu_orden) != count($array_menu_id))
	 		$data['mensaje'] = 'No se ha especificado un número de orden para cada Menú';

	 	if(count($array_menu_orden) == count($array_menu_id) && count($array_menu_orden) > 0){
		 	for ($i = 0; $i < count($array_menu_orden); $i++) { 
		 		$menu_id = $array_menu_id[$i];
		 		$menu_ord = $array_menu_orden[$i];

		 		if(!$oMenu->modificar_orden_menu($menu_id, $menu_ord))
		 			exit();
		 	}

		 	$data['estado'] = 1;
	 		$data['mensaje'] = 'Se ha registrado el orden de los Menús correctamente';
		}
	 	//$result = NULL;
	 	echo json_encode($data);
 	}
 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>