function menu_ordenar_tabla(menu_idp){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"menu/menu_ordenar_tabla.php",
		async: true,
		dataType: "html",
		data: ({
			menu_idp: menu_idp
		}),
		beforeSend: function() {
			$('#menu_tabla_mensaje').show(400);
		},
		success: function(data){
			$('#menu_tabla_mensaje').hide(400);
			$('#div_menu_ordenar_tabla').html(data);
		},
		complete: function(data){
			//console.log(data);
		},
		error: function(data){
			$('#menu_tabla_mensaje').removeClass('callout-info').addClass('callout-danger')
    	$('#menu_tabla_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
    	$('#btn_guardar_menu_ordenar').prop('disabled', false);
		}
	});
}

$(document).ready(function() {
	$('#cmb_menu_ipd').selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: 'ES'
  });

	$('.txt_menu_ord').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0',
		vMax: '50'
	});

	$('#cmb_menu_ipd').change(function(event) {
		var menu_idp = $(this).val();
		if(parseInt(menu_idp) >= 0)
			menu_ordenar_tabla(menu_idp)
		else
			alert('El Menú seleccionado no tiene un id valido');
	});

	$('#form_menu_ordenar').validate({
		submitHandler: function() {
	    var datos = $("#form_menu_ordenar").serializeArray();
	    if(datos.length <= 2)
	    	return false; // no hay ningun msubmenu para ordenar

	    $.ajax({
				type: "POST",
				url: VISTA_URL+"menu/menu_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_menu_ordenar").serialize(),
				beforeSend: function() {
					$('#menu_mensaje').show(400);
					$('#btn_guardar_menu_ordenar').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#menu_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#menu_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		menu_tabla();
		      		$('#modal_ordenar_menu').modal('hide');
		      	}, 1000);
					}
					else{
		      	$('#menu_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#menu_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_menu_ordenar').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#menu_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#menu_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
	      	$('#btn_guardar_menu_ordenar').prop('disabled', false);
				}
			});
	  },
	  rules: {
			txt_menu_tit: {
				required: true,
				minlength: 2
			},
			txt_menu_dir: {
				required: true,
				minlength: 2,
				alphanumeric: true,
				lettersonly: true
			},
			txt_menu_des: {
				required: true,
				minlength: 5
			},
			txt_menu_ico: {
				required: true,
				minlength: 2,
				nowhitespace: true
			}
		},
		messages: {
			txt_menu_tit: {
				required: "Ingrese un título al Menú",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_menu_dir: {
				required: "Ingrese el nombre del directorio",
				minlength: "El directorio debe tener como mínimo 2 caracteres",
				alphanumeric: "No espacios ni caracteres especiales",
				lettersonly: "Solo ingrese texto"
			},
			txt_menu_des: {
				required: "Ingrese una descripción breve del Menú",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			},
			txt_menu_ico: {
				required: "Copie el nombre del ícono",
				minlength: "El nombre del ícono debe tener mínimo 2 letras",
				nowhitespace: "No espacios ni caracteres especiales"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
