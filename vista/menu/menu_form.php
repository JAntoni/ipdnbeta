<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../menu/Menu.class.php');
  $oMenu = new Menu();
  require_once('../funciones/funciones.php');

  $direc = 'menu';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $menu_id = $_POST['menu_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Menú Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Menú';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Menú';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Menú';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en menu
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'menu'; $modulo_id = $menu_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    $menu_ico = 'fa-circle-o';
    //si la accion es modificar, mostramos los datos del menu por su ID
    if(intval($menu_id) > 0){
      $result = $oMenu->mostrarUno($menu_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el menu seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $menu_tit = $result['data']['tb_menu_tit'];
          $menu_dir = $result['data']['tb_menu_dir'];
          $menu_des = $result['data']['tb_menu_des'];
          $menu_ico = (empty($result['data']['tb_menu_ico']))? 'fa-circle-o' : $result['data']['tb_menu_ico'];
          $menu_idp = $result['data']['tb_menu_idp'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_menu" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_menu" method="post">
          <input type="hidden" id="action" name="action" value="<?php echo $action;?>">
          <input type="hidden" id="hdd_menu_id" name="hdd_menu_id" value="<?php echo $menu_id;?>">
          <input type="hidden" id="hdd_menu_idp" value="<?php echo $menu_idp;?>">
          
          <div class="modal-body">
            <div class="form-group">
              <label for="txt_menu_idp" class="control-label">Menú Padre</label>
              <select name="cmb_menu_ipd" id="cmb_menu_ipd" class="selectpicker form-control" data-live-search="true" data-max-options="1">
                <?php require_once('../menu/menu_select.php') ?>
              </select>
            </div>
            <div class="form-group">
              <label for="txt_menu_tit" class="control-label">Título de Menú</label>
              <input type="text" name="txt_menu_tit" id="txt_menu_tit" class="form-control input-sm mayus" value="<?php echo $menu_tit;?>">
            </div>
            <div class="form-group">
              <label for="txt_menu_dir" class="control-label">Directorio</label>
              <input type="text" name="txt_menu_dir" id="txt_menu_dir" class="form-control input-sm" value="<?php echo $menu_dir;?>" placeholder="Puede ser vacío">
            </div>
            <div class="form-group">
              <label for="txt_menu_des" class="control-label">Descripción Breve</label>
              <input type="text" name="txt_menu_des" id="txt_menu_des" class="form-control input-sm" value="<?php echo $menu_des;?>">
            </div>
            <div class="form-group">
              <label for="txt_menu_ico" class="control-label">Icono <a href="./icons.html" target="_blank">Ver Lista de Íconos</a></label>
              <input type="text" name="txt_menu_ico" id="txt_menu_ico" class="form-control input-sm" placeholder="Pegue el nombre del ícono de la lista" value="<?php echo $menu_ico;?>">
            </div>
      
            <!--- MESAJES DE ALERTA AL ELIMINAR -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Menú?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="menu_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_menu">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_menu">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_menu">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/menu/menu_form.js';?>"></script>
