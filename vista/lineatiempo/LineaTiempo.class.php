<?php

  if(defined('APP_URL')){
  require_once(APP_URL.'datos/conexion.php');}
  else{
  require_once('../../datos/conexion.php');}
  
class LineaTiempo extends Conexion{
    
    public $tb_lineatiempo_id;
    public $tb_lineatiempo_reg;
    public $tb_usuario_id;
    public $tb_lineatiempo_det;
            
    function insertar(){
        $this->dblink->beginTransaction();
            try {
              $sql = "INSERT INTO tb_lineatiempo(tb_usuario_id, tb_lineatiempo_det)
                       VALUES (:tb_usuario_id,:tb_lineatiempo_det)";

              $sentencia = $this->dblink->prepare($sql);
              $sentencia->bindParam(":tb_usuario_id",$this->tb_usuario_id, PDO::PARAM_INT);
              $sentencia->bindParam(":tb_lineatiempo_det",$this->tb_lineatiempo_det, PDO::PARAM_STR);

              $result = $sentencia->execute();
              $lineatiempo_id = $this->dblink->lastInsertId();
              $this->dblink->commit();

              $data['estado'] = $result;
              $data['tb_lineatiempo_id'] = $lineatiempo_id;
              return $data; //si es correcto el ingreso retorna 1

            } catch (Exception $e) {
              $this->dblink->rollBack();
              throw $e;
            }
    }
    
    
    function listar_lineatiempo($usu_id, $fecha1, $fecha2){
      try {
        $sql = "SELECT * FROM tb_lineatiempo WHERE DATE(tb_lineatiempo_reg) between :fecha1 and :fecha2";
            if($usu_id != "")
                $sql .= " AND tb_usuario_id =:usu_id";
                
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usu_id",$usu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha1",$fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2",$fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay areas registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    

    
    
}
