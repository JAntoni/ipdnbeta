<?php
require_once('Vehiculo.class.php');
$oVehiculo = new Vehiculo();

$vehiculo_id = (empty($vehiculo_id))? 0 : $vehiculo_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oVehiculo->listar_vehiculos();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($vehiculo_id == $value['tb_vehiculo_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_vehiculo_id'].'" '.$selected.'>'.$value['tb_vehiculo_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>