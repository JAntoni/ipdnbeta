function vehiculomodelo_select(vehiculomarca_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"vehiculomodelo/vehiculomodelo_select.php",
		async: true,
		dataType: "html",
		data:({
			vehiculomarca_id: vehiculomarca_id
		}),
		beforeSend: function() {
			$('#cmb_vehiculomodelo_id').html('<option>Cargando...</option>');
		},
		success: function(data){
			$('#cmb_vehiculomodelo_id').html(data);
		},
		complete: function(data){
			//console.log(data);
		},
		error: function(data){
    	alert(data.responseText);
		}
	});
}
function vehiculotipo_select(vehiculoclase_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"vehiculotipo/vehiculotipo_select.php",
		async: true,
		dataType: "html",
		data:({
			vehiculoclase_id: vehiculoclase_id
		}),
		beforeSend: function() {
			$('#cmb_vehiculotipo_id').html('<option>Cargando...</option>');
		},
		success: function(data){
			$('#cmb_vehiculotipo_id').html(data);
		},
		complete: function(data){
			//console.log(data);
		},
		error: function(data){
    	alert(data.responseText);
		}
	});
}
$(document).ready(function(){
	
	$('input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#txt_vehiculo_anio, #txt_vehiculo_numpas, #txt_vehiculo_numasi').autoNumeric({
		aSep: '',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0',
		vMax: '9999'
	});

	$('#cmb_vehiculomarca_id').change(function(event) {
		var vehiculomarca_id = parseInt($(this).val());
		vehiculomodelo_select(vehiculomarca_id);
	});

	$('#cmb_vehiculoclase_id').change(function(event) {
		var vehiculoclase_id = parseInt($(this).val());
		vehiculotipo_select(vehiculoclase_id);
	});
console.log('cambios 11111');
  $('#form_vehiculo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"vehiculo/vehiculo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_vehiculo").serialize(),
				beforeSend: function() {
					$('#vehiculo_mensaje').show(400);
					$('#btn_guardar_vehiculo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#vehiculo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#vehiculo_mensaje').html(data.mensaje);

		      	var vista = $('#vehiculo_vista').val();

		      	setTimeout(function(){
		      		if(vista == 'vehiculo')
		      			vehiculo_tabla();
		      		$('#modal_registro_vehiculo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#vehiculo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#vehiculo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_vehiculo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#vehiculo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#vehiculo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
	  	cmb_vehiculomarca_id: {
				min: 1
	  	},
	  	cmb_vehiculomodelo_id: {
	  		min: 1
	  	},
	  	cmb_vehiculoclase_id: {
	  		min: 1
	  	},
	  	cmb_vehiculotipo_id: {
	  		min: 1
	  	},
	  	cmb_vehiculo_tan: {
	  		min: 1
	  	},
	  	cmb_galon_id: {
	  		min: 1
	  	},
	  	txt_vehiculo_pla: {
	  		required: true
	  	},
	  	txt_vehiculo_sermot: {
	  		required: true
	  	},
	  	txt_vehiculo_sercha: {
	  		required: true
	  	},
	  	txt_vehiculo_anio: {
	  		required: true,
				minlength: 4,
				maxlength: 4
	  	},
			txt_vehiculo_col: {
				required: true
			},
			txt_vehiculo_numpas:{
				required: true
			},
			txt_vehiculo_numasi:{
				required: true
			}
		},
		messages: {
			cmb_vehiculomarca_id: {
				min: "Seleccione una Marca"
			},
			cmb_vehiculomodelo_id: {
				min: "Seleccione un Modelo"
			},
			cmb_vehiculoclase_id: {
				min: "Seleccione una Clase"
			},
			cmb_vehiculotipo_id: {
				min: "Seleccione un Tipo"
			},
			cmb_vehiculo_tan: {
				min: "Seleccione un tipo de Tanque"
			},
			cmb_galon_id: {
				min: "Seleccione la capacidad del Tanque"
			},
			txt_vehiculo_pla: {
				required: "Ingrese Placa de Vehículo"
			},
			txt_vehiculo_sermot: {
				required: "Ingrese la serie del mortor"
			},
			txt_vehiculo_sercha: {
				required: "Ingrese la serie del chasis"
			},
			txt_vehiculo_anio: {
				required: "Ingrese el año del Vehículo",
				minlength: "El Año como mínimo debe tener 4 números",
				maxlength: "El Año como máximo debe tener 4 números"
			},
			txt_vehiculo_col: {
				required: "Ingrese el color del Vehículo"
			},
			txt_vehiculo_numpas: {
				required: "Ingrese el número de pasajeros"
			},
			txt_vehiculo_numasi: {
				required: "Ingrese el número de asientos"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
