<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'vehiculo/Vehiculo.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../vehiculo/Vehiculo.class.php');
    require_once('../funciones/fechas.php');
  }
  $oVehiculo = new Vehiculo();

?>
<table id="tbl_vehiculos" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Placa</th>
      <th>Marca</th>
      <th>Modelo</th>
      <th>Año</th>
      <th>Color</th>
      <th>Serie Motor</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oVehiculo->listar_vehiculos();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr>
            <td><?php echo $value['tb_vehiculo_id'];?></td>
            <td><?php echo $value['tb_vehiculo_pla'];?></td>
            <td><?php echo $value['tb_vehiculomarca_nom'];?></td>
            <td><?php echo $value['tb_vehiculomodelo_nom']; ?></td>
            <td><?php echo $value['tb_vehiculo_anio']; ?></td>
            <td><?php echo $value['tb_vehiculo_col']; ?></td>
            <td><?php echo $value['tb_vehiculo_sermot']; ?></td>
            <td align="center">
              <a class="btn btn-info btn-xs" title="Ver" onclick="vehiculo_form(<?php echo "'L', ".$value['tb_vehiculo_id'];?>)"><i class="fa fa-eye"></i></a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="vehiculo_form(<?php echo "'M', ".$value['tb_vehiculo_id'];?>)"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="vehiculo_form(<?php echo "'E', ".$value['tb_vehiculo_id'];?>)"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
