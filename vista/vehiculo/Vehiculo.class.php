<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Vehiculo extends Conexion{
    public $vehiculo_id;
    
    public $vehiculomarca_id;
    public $vehiculomodelo_id;
    public $vehiculoclase_id;
    public $vehiculotipo_id;
    public $vehiculo_tan;
    public $galon_id;

    public $vehiculo_pla;
    public $vehiculo_sermot;
    public $vehiculo_sercha;
    public $vehiculo_anio;
    public $vehiculo_col;
    public $vehiculo_numpas;
    public $vehiculo_numasi;
    public $vehiculo_des;

    public $usuario_id;

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_insertar_vehiculo(
                :vehiculomarca_id, :vehiculomodelo_id, :vehiculoclase_id, 
                :vehiculotipo_id, :vehiculo_tan, :galon_id, 
                :vehiculo_pla, :vehiculo_sermot, :vehiculo_sercha, 
                :vehiculo_anio, :vehiculo_col, :vehiculo_numpas,
                :vehiculo_numasi, :vehiculo_des, :usuario_id)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculotipo_id", $this->vehiculotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_tan", $this->vehiculo_tan, PDO::PARAM_INT);
        $sentencia->bindParam(":galon_id", $this->galon_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_pla", $this->vehiculo_pla, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_sermot", $this->vehiculo_sermot, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_sercha", $this->vehiculo_sercha, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_anio", $this->vehiculo_anio, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_col", $this->vehiculo_col, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_numpas", $this->vehiculo_numpas, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_numasi", $this->vehiculo_numasi, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_des", $this->vehiculo_des, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $this->usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "CALL proc_modificar_vehiculo(
                :vehiculo_id, :vehiculomarca_id, :vehiculomodelo_id, 
                :vehiculoclase_id, :vehiculotipo_id, :vehiculo_tan, 
                :galon_id, :vehiculo_pla, :vehiculo_sermot, 
                :vehiculo_sercha, :vehiculo_anio, :vehiculo_col, 
                :vehiculo_numpas, :vehiculo_numasi, :vehiculo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculo_id", $this->vehiculo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculotipo_id", $this->vehiculotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_tan", $this->vehiculo_tan, PDO::PARAM_INT);
        $sentencia->bindParam(":galon_id", $this->galon_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_pla", $this->vehiculo_pla, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_sermot", $this->vehiculo_sermot, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_sercha", $this->vehiculo_sercha, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_anio", $this->vehiculo_anio, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_col", $this->vehiculo_col, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculo_numpas", $this->vehiculo_numpas, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_numasi", $this->vehiculo_numasi, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculo_des", $this->vehiculo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($vehiculo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_vehiculo WHERE tb_vehiculo_id =:vehiculo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculo_id", $vehiculo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($vehiculo_id){
      try {
        $sql = "SELECT * FROM tb_vehiculo WHERE tb_vehiculo_id =:vehiculo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculo_id", $vehiculo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculos(){
      try {
        $sql = "SELECT * FROM tb_vehiculo ve 
          INNER JOIN tb_vehiculomarca vemar on vemar.tb_vehiculomarca_id = ve.tb_vehiculomarca_id
          INNER JOIN tb_vehiculomodelo vemo on vemo.tb_vehiculomodelo_id = ve.tb_vehiculomodelo_id
          INNER JOIN tb_vehiculoclase vecla on vecla.tb_vehiculoclase_id = ve.tb_vehiculoclase_id
          INNER JOIN tb_vehiculotipo vetip on vetip.tb_vehiculotipo_id = ve.tb_vehiculotipo_id
          INNER JOIN tb_galon ga on ga.tb_galon_id = ve.tb_galon_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
