<?php
if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Documentocontable extends Conexion{
    private $tb_documentocontable_cod;
    private $tb_documentocontable_desc;
    public $tb_documentocontable_frecuente;

    /**
     * Get the value of tb_documentocontable_cod
     */
    public function getTbDocumentocontableCod()
    {
        return $this->tb_documentocontable_cod;
    }

    /**
     * Set the value of tb_documentocontable_cod
     */
    public function setTbDocumentocontableCod($tb_documentocontable_cod)
    {
        $this->tb_documentocontable_cod = $tb_documentocontable_cod;

        return $this;
    }

    /**
     * Get the value of tb_documentocontable_desc
     */
    public function getTbDocumentocontableDesc()
    {
        return $this->tb_documentocontable_desc;
    }

    /**
     * Set the value of tb_documentocontable_desc
     */
    public function setTbDocumentocontableDesc($tb_documentocontable_desc)
    {
        $this->tb_documentocontable_desc = $tb_documentocontable_desc;

        return $this;
    }

    public function listar(){
        try {
            $opcional = !empty($this->tb_documentocontable_frecuente) ? 'WHERE tb_documentocontable_frecuente = 1 ORDER BY tb_documentocontable_desc ASC' : '';
            $sql = "SELECT * from tb_documentocontable $opcional";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if($sentencia->rowCount() > 0){
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Documentos Contables registrados";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th->getMessage();
        }
    }

    function Documentocontable_autocomplete($dato) {
        try {

            $filtro = "%" . $dato . "%";

            $sql = "SELECT *
                    FROM tb_documentocontable 
                    WHERE tb_documentocontable_desc LIKE :filtro OR tb_documentocontable_cod LIKE :filtro";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Documentos contables registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
?>