<?php
require_once('Documentocontable.class.php');
$oDocCont = new Documentocontable();

$documentocontable_cod = (isset($_POST['documentocontable_cod'])) ? trim($_POST['documentocontable_cod']) : $documentocontable_cod;
$oDocCont->tb_documentocontable_frecuente = null;
if(!empty($_POST['documentos_frecuentes']) || !empty($documentos_frecuentes)){
    if(!empty($_POST['documentos_frecuentes'])){
        $oDocCont->tb_documentocontable_frecuente = $_POST['documentos_frecuentes'];
    } else {
        $oDocCont->tb_documentocontable_frecuente = $documentos_frecuentes;
    }
}

$option = ''; //<option value="0">-</option>
//PRIMER NIVEL
$result = $oDocCont->listar();
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $selected = '';
        if ($documentocontable_cod == $value['tb_documentocontable_cod'])
            $selected = 'selected';

        $option .= '<option value="' . $value['tb_documentocontable_cod'] . '" style="font-weight: bold;" ' . $selected . '>' . $value['tb_documentocontable_cod'] . ' - ' . $value['tb_documentocontable_desc'] . '</option>';
    }
}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;

?>