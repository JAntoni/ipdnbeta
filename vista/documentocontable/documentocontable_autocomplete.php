<?php
  require_once ("Documentocontable.class.php");
  $oDocumentocontable= new Documentocontable();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $tb_documentocontable_cod;
    var $tb_documentocontable_desc;


    function __construct($label, $value, $tb_documentocontable_cod, $tb_documentocontable_desc){
      $this->label = $label;
      $this->value = $value;
      $this->tb_documentocontable_cod = $tb_documentocontable_cod;
      $this->tb_documentocontable_desc = $tb_documentocontable_desc;
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];
  $datoBuscar = mb_strtoupper($datoBuscar, 'UTF-8');

  //busco un valor aproximado al dato escrito
  $result = $oDocumentocontable->Documentocontable_autocomplete($datoBuscar);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();

    foreach ($result['data'] as $key => $value) {
      array_push(
        $arrayElementos,
        new ElementoAutocompletar(
          $value["tb_documentocontable_cod"].' - '.$value["tb_documentocontable_desc"],
          $value["tb_documentocontable_cod"].' - '.$value["tb_documentocontable_desc"],
          $value["tb_documentocontable_cod"],
          $value["tb_documentocontable_desc"])
      );
    }

    print_r(json_encode($arrayElementos));
  $result = NULL;
?>
