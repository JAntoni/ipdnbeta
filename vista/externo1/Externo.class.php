<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Externo extends Conexion{

//    function insertar($externo_nom, $externo_dni, $externo_dir, $externo_den){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "INSERT INTO tb_externo(tb_externo_xac, tb_externo_nom, tb_externo_dni, tb_externo_dir, tb_externo_den)
//          VALUES (1, :externo_nom, :externo_dni, :externo_dir, :externo_den)";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":externo_nom", $externo_nom, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_dni", $externo_dni, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_dir", $externo_dir, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_den", $externo_den, PDO::PARAM_STR);
//
//        $result = $sentencia->execute();
//
//        $this->dblink->commit();
//
//        return $result; //si es correcto el ingreso retorna 1
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }
//    function modificar($texterno, $externo_nom, $externo_dni, $externo_dir, $externo_den){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "UPDATE tb_externo SET tb_externo_nom =:externo_nom, tb_externo_dni =:externo_dni, tb_externo_dir =:externo_dir, tb_externo_den =:externo_den WHERE tb_texterno =:texterno";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":texterno", $texterno, PDO::PARAM_INT);
//        $sentencia->bindParam(":externo_nom", $externo_nom, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_dni", $externo_dni, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_dir", $externo_dir, PDO::PARAM_STR);
//        $sentencia->bindParam(":externo_den", $externo_den, PDO::PARAM_STR);
//
//        $result = $sentencia->execute();
//
//        $this->dblink->commit();
//
//        return $result; //si es correcto retorna 1
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }
//    function eliminar($texterno){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "DELETE FROM tb_externo WHERE tb_texterno =:texterno";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":texterno", $texterno, PDO::PARAM_INT);
//
//        $result = $sentencia->execute();
//
//        $this->dblink->commit();
//
//        return $result; //si es correcto retorna 1
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }
    function mostrarUno($externo){
      try {
        $sql = "SELECT * FROM tb_externo WHERE tb_externo_id=:externo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":externo", $externo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarTodos($fecha1, $fecha2){
      try {
        $sql = "SELECT * FROM tb_externo WHERE tb_externo_fec BETWEEN :fecha1 AND :fecha2";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
