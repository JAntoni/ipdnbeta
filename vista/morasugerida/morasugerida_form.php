<?php
  require_once('../../core/usuario_sesion.php');

  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once ("../morasugerida/Morasugerida.class.php");
  $oMorasugerida = new Morasugerida();
  require_once("../funciones/funciones.php");
  require_once("../funciones/fechas.php");

  $direc = 'morasugerida';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $morasugerida_id = $_POST['morasugerida_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Morasugerida Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Morasugerida';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Morasugerida';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Morasugerida';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en retencion
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'morasugerida'; $modulo_id = $morasugerida_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    if($action == "insertar"){
      $fecha=date('d-m-Y');
      $estado='1';
      $caja_id=1;
    }
    //si la accion es modificar, mostramos los datos del morasugerida por su ID
    if(intval($morasugerida_id) > 0){
      $oMorasugerida->tb_morasugerida_id = $morasugerida_id;
      $result = $oMorasugerida->mostrarUno();
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el morasugerida seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }else{
          $creditotipo_id = $result['data']['tb_creditotipo_id'];
          $morasugerida_montominimo = $result['data']['tb_morasugerida_montominimo'];
          $morasugerida_montomaximo = $result['data']['tb_morasugerida_montomaximo'];
          $morasugerida_diaminimo = $result['data']['tb_morasugerida_diaminimo'];
          $morasugerida_diafin = $result['data']['tb_morasugerida_diafin'];
          $morasugerida_porcentajemora = $result['data']['tb_morasugerida_porcentajemora']*100;
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_morasugerida" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_morasugerida" method="post">
        <input name="action_morarang" id="action_morarang" type="hidden" value="<?php echo $_POST['action']?>">
        <input name="hdd_morasugerida_id" id="hdd_morasugerida_id" type="hidden" value="<?php echo $_POST['morasugerida_id']?>">
        <input type="hidden" id="hdd_morasugerida_usureg" name="hdd_morasugerida_usureg" value="<?php echo $_SESSION['usuario_id']?>">
        <input type="hidden" id="hdd_morasugerida_usumod" name="hdd_morasugerida_usumod" value="<?php echo $_SESSION['usuario_id']?>">
        <input type="hidden" id="hdd_emp_id" name="hdd_emp_id" value="<?php echo $_SESSION['empresa_id']?>">
        <input type="hidden" id="hdd_action" name="action_morasugerida" value="<?php echo $action?>">
        <input type="hidden" id="hdd_morasugeridaid" name="hdd_morasugeridaid" value="<?php echo $morasugerida_id?>">
          
          <div class="modal-body">
          <div class="row">
              <div class="form-group col-md-4">
                <label for="cmb_creditotipo" class="control-label">CREDITO TIPO:</label>
                <select name="cmb_creditotipo" id="cmb_creditotipo" class="form-control input-sm">
                  <?php require_once '../creditotipo/creditotipo_select.php'; ?>
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="txt_montominimo" class="control-label">MONTO MINIMO:</label>
                <input type="text" name="txt_montominimo" id="txt_montominimo" class="form-control input-sm moneda" value="<?php echo $morasugerida_montominimo;?>">
              </div>
              <div class="form-group col-md-4">
                <label for="txt_montomaximo" class="control-label">MONTO MAXIMO:</label>
                <input type="text" name="txt_montomaximo" id="txt_montomaximo" class="form-control input-sm moneda" value="<?php echo $morasugerida_montomaximo;?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-4">
                <label for="txt_diaminimo" class="control-label">DIA MINIMO:</label>
                <input type="text" name="txt_diaminimo" id="txt_diaminimo" class="form-control input-sm" value="<?php echo $morasugerida_diaminimo;?>">
              </div>
              <div class="form-group col-md-4">
                <label for="txt_diafin" class="control-label">DIA FIN:</label>
                <input type="text" name="txt_diafin" id="txt_diafin" class="form-control input-sm" value="<?php echo $morasugerida_diafin;?>">
              </div>
              <div class="form-group col-md-4">
                <label for="txt_porcentajemora" class="control-label">MORA SUGERIDA:</label>
                  <div class="input-group input-sm">
                    <input type="text" name="txt_porcentajemora" id="txt_porcentajemora" class="form-control input-sm moneda1" value="<?php echo $morasugerida_porcentajemora;?>">
                    <div class="input-group-btn">
                      <span class="btn btn-danger btn-sm"><i class="fa fa-percent"></i></span>
                    </div>
                  </div>
                
              </div>
              
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Cambio?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="morasugerida_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_morasugerida">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_morasugerida">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_morasugerida">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/morasugerida/morasugerida_form.js';?>"></script>
