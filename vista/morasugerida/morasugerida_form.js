$(document).ready(function() {

	$('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.000',
        vMax: '9999999.000'
    });
	$('.moneda1').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99.000'
    });

  $("#form_morasugerida").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"morasugerida/morasugerida_controller.php",
				async:true,
				dataType: "json",
				data: $("#form_morasugerida").serialize(),
				beforeSend: function() {
					//alert($("#for_cuopag").serialize());
					$('#msj_morasugerida_tabla').html("Guardando...");
					$('#msj_morasugerida_tabla').show(100);
				},
				success: function(data){				
					if(parseInt(data.morasugerida_id) > 0){
						$('#modal_registro_morasugerida').modal('hide');
						morasugerida_tabla();
						swal_success("SISTEMA",data.morasugerida_id+"   "+data.morasugerida_msj,2000);
						$('#msj_morasugerida_tabla').hide();
					}
					else{
						console.log(data.morasugerida_msj);
						//alerta_error('Error',data.morasugerida_msj);
							//swal_success("SISTEMA",data.morasugerida_id+"   "+data.morasugerida_msj,6000);
					}
				},
				complete: function(data){
					if(data.statusText != "success"){
						$('#msj_morasugerida_tabla').text(data.responseText);
            
					}
				}
			});
		},
		rules: {
//			txt_cuopag_fec: {
//				required: true,
//				dateITA: true
//			},
			txt_cuopag_mon:{
				required:true
			}
		},
		messages: {
//			txt_cuopag_fec: {
//				required: '*',
//				dateITA: '?'
//			},
			txt_cuopag_mon:{
				required:'*'
			}
		}
	});
});


function morasugerida_eliminar(morasugerida_id){
	$.ajax({
			type: "POST",
			url: VISTA_URL+"monedacambio/monedacambio_controller.php",
			async:false,
			dataType: "json",
			data: ({
					action_morasugerida: 'eliminar',
					hdd_morasugerida_id:	morasugerida_id,
			}),
			beforeSend: function(){
					$('#msj_pagobanco').hide(100);
					$('#txt_cajcam_tipcam').val('');	
			},
			success: function(data){
					if(data.moncam_val == null){
					  swal_warning('AVISO','Por favor registre el tipo de cambio del día de la fecha de depósito',6000);
					}
					else
					  $('#txt_cajcam_tipcam').val(data.moncam_val);	
			},
			complete: function(data){			
  //                 console.log(data);
			}
		  });		
}