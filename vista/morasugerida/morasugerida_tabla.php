<?php

  if(defined('VISTA_URL')){
      require_once(APP_URL.'core/usuario_sesion.php');
      require_once(VISTA_URL.'funciones/funciones.php');
      require_once(VISTA_URL.'funciones/fechas.php');
      require_once(VISTA_URL.'morasugerida/Morasugerida.class.php');
  }
  else{
      require_once('../../core/usuario_sesion.php');
      require_once('../funciones/funciones.php');
      require_once('../funciones/fechas.php');
      require_once('../morasugerida/Morasugerida.class.php');
  }
  
  $oMorasugerida = new Morasugerida();
  
  $result = $oMorasugerida->listar_morasugerida();
  $bandera = intval($result['estado']);
?>
<?php if($bandera == 1): ?>
  <table id="tbl_morasugerida" class="table table-hover">
    <thead>
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">CREDITO TIPO</th>
        <th id="tabla_cabecera_fila">MONTO MINIMO</th>
        <th id="tabla_cabecera_fila">MONTO MAXIMO</th>
        <th id="tabla_cabecera_fila">DIA MINIMO</th>
        <th id="tabla_cabecera_fila">DIA MAXIMO</th>
        <th id="tabla_cabecera_fila">PORCENTAJE MORA</th>
        <th id="tabla_cabecera_fila">ESTADO</th>
        <th id="tabla_cabecera_fila"></th>
      </tr>
    </thead>
    <tbody> <?php
      foreach ($result['data'] as $key => $value) {
        if($value['tb_morasugerida_xac']==1){
          $estado = 'ACTIVO';
          $color = 'green';
        }
        if($value['tb_morasugerida_xac']==0){
          $estado = 'INACTIVO';
          $color = 'red';
        }
        
        ?>
        <tr class="even" id="tabla_fila">
          <td id="tabla_fila" align="center"><?php echo $value['tb_morasugerida_id'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_creditotipo_nom'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_morasugerida_montominimo'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_morasugerida_montomaximo'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_morasugerida_diaminimo'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_morasugerida_diafin'];?></td>
          <td id="tabla_fila" align="center"><?php echo $value['tb_morasugerida_porcentajemora'];?></td>
          <td id="tabla_fila" align="center"><span class="badge bg-<?php echo $color;?>"><?php echo $estado;?></span></td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-success btn-xs" onClick="morasugerida_form('M', <?php echo $value['tb_morasugerida_id']?>)" title="EDITAR"><i class="fa fa-edit"></i></a>
            <a class="btn btn-danger btn-xs" onClick="morasugerida_eliminar(<?php echo $value['tb_morasugerida_id']?>)" title="ELIMINAR"><i class="fa fa-trash"></i></a>
          </td>
        </tr> <?php
      }
      $result = null; ?>
    </tbody>
  </table>
<?php endif; ?>

<?php if($bandera != 1): ?>
  <table id="tbl_morasugerida" class="table table-hover">
    <thead>
      <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">CREDITO TIPO</th>
        <th id="tabla_cabecera_fila">MONTO MINIMO</th>
        <th id="tabla_cabecera_fila">MONTO MAXIMO</th>
        <th id="tabla_cabecera_fila">DIA MINIMO</th>
        <th id="tabla_cabecera_fila">DIA MAXIMO</th>
        <th id="tabla_cabecera_fila">PORCENTAJE MORA</th>
        <th id="tabla_cabecera_fila">ESTADO</th>
        <th id="tabla_cabecera_fila"></th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
<?php endif; ?>