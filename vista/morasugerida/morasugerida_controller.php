<?php

require_once('../../core/usuario_sesion.php');
require_once ("../morasugerida/Morasugerida.class.php");
$oMorasugerida = new Morasugerida();


require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

if($_POST['action_morasugerida']=="insertar")
{
  $response = [
    'estado' => 0,
    'mensaje' => 'REGISTRO INCORRECTOS'
  ];

  if(!empty($_POST['txt_montominimo']) and !empty($_POST['txt_montomaximo']))
	{
    $moradecimal = $_POST['txt_porcentajemora']/100;
    try {

		  $oMorasugerida->tb_morasugerida_usureg = $_POST['hdd_morasugerida_usureg'];
	  	$oMorasugerida->tb_morasugerida_xac = 1;
	  	$oMorasugerida->tb_creditotipo_id = intval($_POST['cmb_creditotipo']);
	  	$oMorasugerida->tb_morasugerida_montominimo = moneda_mysql($_POST['txt_montominimo']);
	  	$oMorasugerida->tb_morasugerida_montomaximo = moneda_mysql($_POST['txt_montomaximo']);
	  	$oMorasugerida->tb_morasugerida_diaminimo = intval($_POST['txt_diaminimo']);
	  	$oMorasugerida->tb_morasugerida_diafin = intval($_POST['txt_diafin']);
	  	$oMorasugerida->tb_morasugerida_porcentajemora = $moradecimal;
      $result = $oMorasugerida->insertar();

		if($result['estado'] == 1){
			$response['estado'] = 1;
			$response['morasugerida_id'] = $result['morasugerida_id'];
			$response['morasugerida_msj'] = 'Se registró el rango. ';
		}


    }catch (Exception $e) {
      // Actualiza el mensaje con el error
      $response['morasugerida_msj'] = 'Error: ' . $e->getMessage();
      $response['morasugerida_id'] = 0;
    }
  }
  echo json_encode($response);
}
if($_POST['action_morasugerida']=="eliminar")
{
  $response = [
      'estado' => 0,
      'mensaje' => 'ERROR.'
  ];

	if(!empty($_POST['hdd_morasugerida_id']))
	{
    try {
      /**** VALIDAR QUE SEA OPERACION DEL DIA DE HOY****/

      //$oCajacambio->modificar_campo($_POST['cajcam_id'],$_SESSION['usuario_id'],'xac','0');
      $oMorasugerida->tb_morasugerida_xac = 0;
      $oMorasugerida->tb_morasugerida_id = intval($_POST['hdd_morasugerida_id']);
      $result = $oMorasugerida->eliminar();

	  if($result==1){
		  $response['estado'] = 1;
		  $response['mensaje'] = 'Se eliminó el rango. ';
	  }else{
		  $response['mensaje'] = 'No se eliminó el rango. ';
	  }
      $result = null;
    } catch (Exception $e) {
        // Actualiza el mensaje con el error
        $response['mensaje'] = 'Error: ' . $e->getMessage();
    }
	}
  // Devuelve la respuesta en formato JSON
  echo json_encode($response);
}

if($_POST['action_morasugerida']=="modificar")
{
  $response = [
    'estado' => 0,
    'mensaje' => 'REGISTRO INCORRECTOS'
  ];

  if(!empty($_POST['txt_montominimo']) and !empty($_POST['txt_montomaximo']))
	{
    $moradecimal = $_POST['txt_porcentajemora']/100;
    try {

      $oMorasugerida->tb_creditotipo_id = intval($_POST['cmb_creditotipo']);
	  	$oMorasugerida->tb_morasugerida_montominimo = moneda_mysql($_POST['txt_montominimo']);
	  	$oMorasugerida->tb_morasugerida_montomaximo = moneda_mysql($_POST['txt_montomaximo']);
	  	$oMorasugerida->tb_morasugerida_diaminimo = intval($_POST['txt_diaminimo']);
	  	$oMorasugerida->tb_morasugerida_diafin = intval($_POST['txt_diafin']);
	  	$oMorasugerida->tb_morasugerida_porcentajemora = $moradecimal;
		  $oMorasugerida->tb_morasugerida_usumod = $_POST['hdd_morasugerida_usumod'];
	  	$oMorasugerida->tb_morasugerida_id = intval($_POST['hdd_morasugeridaid']);

		if($oMorasugerida->modificar() == 1){
			$response['estado'] = 1;
			$response['morasugerida_id'] = intval($_POST['hdd_morasugeridaid']);
			$response['morasugerida_msj'] = 'Se registró el rango. ';
		}


    }catch (Exception $e) {
      // Actualiza el mensaje con el error
      $response['morasugerida_msj'] = 'Error: ' . $e->getMessage();
      $response['morasugerida_id'] = 0;
    }
  }
  echo json_encode($response);
}

if($_POST['action_morasugerida']=="obtener")
{
  $response = [
    'estado' => 0,
    'morasugerida_msj' => 'UN PARAMETRO ESTÁ FALTANTE'
  ];

  if(!empty($_POST['monto']) || !empty($_POST['fecha_pago']) || !empty($_POST['cuota_fecha']) || !empty($_POST['tipo_credito']))
	{
    $monto = $_POST['monto'];
    $fecha_pago = $_POST['fecha_pago'];
    $cuota_fecha = $_POST['cuota_fecha'];
    $tipo_credito = $_POST['tipo_credito'];
    $moneda_id = $_POST['moneda_id'] ? $_POST['moneda_id']: 1;
    $tipo_camnbio = $_POST['tipo_camnbio'] ? $_POST['tipo_camnbio'] : 1;
    $monto_calculado = $moneda_id == 2 ? round($monto * $tipo_camnbio,2) : round($monto,2);
    
    try {

      if (strtotime($fecha_pago) > strtotime($cuota_fecha)){

        $diferencia_en_segundos = strtotime($fecha_pago) - strtotime($cuota_fecha);

        $diferencia_en_dias = intval($diferencia_en_segundos / (60 * 60 * 24));

        $resultmora = $oMorasugerida->obtener_morasugerida($monto_calculado, $diferencia_en_dias, $tipo_credito);
        if($resultmora['estado']==1){

          $porcentajemora = $resultmora['data']['tb_morasugerida_porcentajemora'];

          $morasugerida = $diferencia_en_dias * $porcentajemora * $monto_calculado;

          $response['estado'] = 1;
          $response['morasugerida'] = round($morasugerida,2);
          $response['monto_calculado'] = $monto_calculado;
          $response['porcentajemora'] = $porcentajemora;
          $response['diferencia_en_dias'] = $diferencia_en_dias;
          $response['morasugerida_msj'] = 'Se obtuvo mora sugerida';
        }else{
          $response['estado'] = 2;
          $response['morasugerida_msj'] = 'NO SE HA REGISTRADO MORA SUGERIDA PARA ESTE CASO';
        }
      }else{
          $response['estado'] = 0;
          $response['morasugerida_msj'] = 'CUOTA NO ESTÁ VENCIDA';
      }

    }catch (Exception $e) {
      // Actualiza el mensaje con el error
      $response['morasugerida_msj'] = 'Error: ' . $e->getMessage();
      $response['morasugerida_id'] = 0;
    }
  }
  echo json_encode($response);
}
?>