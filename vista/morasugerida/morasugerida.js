function morasugerida_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"morasugerida/morasugerida_tabla.php",
    async:false,
    dataType: "html",                      
    data: $('#morasugerida_filtro').serialize(),
    beforeSend: function() {
      $('#msj_morasugerida_tabla').html("Cargando datos...");
      $('#msj_morasugerida_tabla').show(100);
      //$('#div_morasugerida_tabla').addClass("ui-state-disabled");
    },
    success: function(html){
      $('#div_morasugerida_tabla').html(html);
      estilos_datatable();
    },
    complete: function(data){
      console.log(data);
      $('#msj_morasugerida_tabla').removeClass("ui-state-disabled");
      $('#msj_morasugerida_tabla').hide(100);
    }
  });
}

function estilos_datatable(){
  datatable_global = $('#tbl_morasugerida').DataTable({
    "pageLength": 25,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [], orderable: false }
    ]
  });

  //datatable_texto_filtrar();
}

function morasugerida_form(usuario_act, morasugerida_id){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"morasugerida/morasugerida_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      morasugerida_id: morasugerida_id,
      vista: 'morasugerida_tabla'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_morasugerida_form').html(data);
      	$('#modal_registro_morasugerida').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_morasugerida'); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
      	modal_hidden_bs_modal('modal_registro_morasugerida', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'morasugerida';
      	var div = 'div_modal_morasugerida_form';
      	permiso_solicitud(usuario_act, morasugerida_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){

		},
		error: function(data){
      alerta_error('Error', data.responseText)
			console.log(data);
		}
	});
}

function morasugerida_eliminar(id) {
  //$.alert('Contenido aquí', 'Titulo aqui');
  $.confirm({
      icon: 'fa fa-trash',
      title: 'Eliminar',
      content: '¿Está seguro de Eliminar?',
      type: 'blue',
      theme: 'material', // 'material', 'bootstrap'
      typeAnimated: true,
      buttons: {
          si: function () {
              //ejecutamos AJAX
              $.ajax({
                type: "POST",
                url: VISTA_URL + "morasugerida/morasugerida_controller.php",
                async: true,
                dataType: "json",
                data: ({
                  action_morasugerida: "eliminar",
                  hdd_morasugerida_id: id
                }),
                beforeSend: function () {
//      $('#msj_aportes').html("Guardando...");
//      $('#msj_aportes').show(100);
                },
                success: function (data) {
                  console.log(data);
                  if(data.estado == 1){
                    swal_success("SISTEMA", data.mensaje, 2000);
                    morasugerida_tabla();
                  }else{
                    alerta_error('Error',data.mensaje);
                  }
                },
                complete: function (data) {
                    if (data.status != 200)
                        console.log(data);

                }
              });
          },
          no: function () {}
      }
  });
}

$(document).ready(function() {
  console.log('MORA GRUPO');

  morasugerida_tabla()
});