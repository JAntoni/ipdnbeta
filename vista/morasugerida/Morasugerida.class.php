<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Morasugerida extends Conexion{
      
      public $tb_morasugerida_id;
      public $tb_morasugerida_fecreg;
      public $tb_morasugerida_fecmod;
      public $tb_morasugerida_usureg;
      public $tb_morasugerida_usumod;
      public $tb_morasugerida_xac;
      public $tb_creditotipo_id;
      public $tb_morasugerida_montominimo;
      public $tb_morasugerida_montomaximo;
      public $tb_morasugerida_diaminimo;
      public $tb_morasugerida_diafin;
      public $tb_morasugerida_porcentajemora;
              

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_morasugerida(
                                        tb_morasugerida_fecreg ,
                                        tb_morasugerida_usureg ,
                                        tb_morasugerida_xac ,
                                        tb_creditotipo_id ,
                                        tb_morasugerida_montominimo ,
                                        tb_morasugerida_montomaximo ,
                                        tb_morasugerida_diaminimo ,
                                        tb_morasugerida_diafin ,
                                        tb_morasugerida_porcentajemora 
                                        )
                                VALUES(
                                        NOW( ) ,
                                        :tb_morasugerida_usureg ,
                                        :tb_morasugerida_xac ,
                                        :tb_creditotipo_id ,
                                        :tb_morasugerida_montominimo ,
                                        :tb_morasugerida_montomaximo ,
                                        :tb_morasugerida_diaminimo ,
                                        :tb_morasugerida_diafin ,
                                        :tb_morasugerida_porcentajemora 
                                        )";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_morasugerida_usureg", $this->tb_morasugerida_usureg, PDO::PARAM_INT);
        //$sentencia->bindParam(":tb_morasugerida_usumod", $this->tb_morasugerida_usumod, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_xac", $this->tb_morasugerida_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_montominimo", $this->tb_morasugerida_montominimo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_morasugerida_montomaximo", $this->tb_morasugerida_montomaximo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_morasugerida_diaminimo", $this->tb_morasugerida_diaminimo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_diafin", $this->tb_morasugerida_diafin, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_porcentajemora", $this->tb_morasugerida_porcentajemora, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $tb_morasugerida_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['morasugerida_id'] = $tb_morasugerida_id;
        return $data;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_morasugerida
                SET tb_creditotipo_id =:tb_creditotipo_id, 
                tb_morasugerida_montominimo =:tb_morasugerida_montominimo, 
                tb_morasugerida_montomaximo =:tb_morasugerida_montomaximo, 
                tb_morasugerida_diaminimo =:tb_morasugerida_diaminimo, 
                tb_morasugerida_diafin =:tb_morasugerida_diafin, 
                tb_morasugerida_porcentajemora =:tb_morasugerida_porcentajemora, 
                tb_morasugerida_usumod =:tb_morasugerida_usumod,
                tb_morasugerida_fecmod = NOW()
                WHERE tb_morasugerida_id =:tb_morasugerida_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_creditotipo_id", $this->tb_creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_montominimo", $this->tb_morasugerida_montominimo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_morasugerida_montomaximo", $this->tb_morasugerida_montomaximo, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_morasugerida_diaminimo", $this->tb_morasugerida_diaminimo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_diafin", $this->tb_morasugerida_diafin, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_porcentajemora", $this->tb_morasugerida_porcentajemora, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_morasugerida_usumod", $this->tb_morasugerida_usumod, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_id", $this->tb_morasugerida_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function eliminar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_morasugerida
                SET tb_morasugerida_xac =:tb_morasugerida_xac
                WHERE tb_morasugerida_id =:tb_morasugerida_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_morasugerida_xac", $this->tb_morasugerida_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_morasugerida_id", $this->tb_morasugerida_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarUno(){
      try {
        $sql = "SELECT * FROM tb_morasugerida WHERE tb_morasugerida_id =:tb_morasugerida_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_morasugerida_id", $this->tb_morasugerida_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay GRUPO MORA registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_morasugerida(){
      try {
        $sql = "SELECT sg.*,ct.tb_creditotipo_nom
                FROM tb_morasugerida sg
                INNER JOIN tb_creditotipo ct ON sg.tb_creditotipo_id = ct.tb_creditotipo_id
                ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_campo($morasugerida_id, $morasugerida_columna, $morasugerida_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($morasugerida_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_morasugerida SET ".$morasugerida_columna." =:morasugerida_valor WHERE tb_morasugerida_id =:morasugerida_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":morasugerida_id", $morasugerida_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":morasugerida_valor", $morasugerida_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":morasugerida_valor", $morasugerida_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();
          $this->dblink->commit();
          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function obtener_morasugerida($cuota, $dias, $tipocredito){
      try {
        $sql = "SELECT * 
                FROM tb_morasugerida 
                WHERE FLOOR($cuota) BETWEEN tb_morasugerida_montominimo AND tb_morasugerida_montomaximo
                AND ($dias BETWEEN tb_morasugerida_diaminimo AND tb_morasugerida_diafin)
                AND tb_creditotipo_id = $tipocredito
                ORDER BY tb_morasugerida_porcentajemora DESC
                LIMIT 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay MORA registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
