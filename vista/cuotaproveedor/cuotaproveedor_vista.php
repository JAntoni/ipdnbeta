<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <button class="btn btn-primary btn-sm" onclick="cuotaproveedor_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Pago</button>
                <p>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha </label>
                            <div class="input-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control input-sm" name="txt_resumen_fec1" id="txt_resumen_fec1" value="<?php echo date('01-m-Y'); ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="input-group-addon">-</span>
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' class="form-control input-sm" name="txt_resumen_fec2" id="txt_resumen_fec2" value="<?php echo date('t-m-Y'); ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>



            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="cuotaproveedor_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_cuotaproveedor_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('cuotaproveedor_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="div_modal_cuotaproveedor_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_pagarcuotaproveedor_registro">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_proveedor_form">
		<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_egreso_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_upload_form"></div>
            <div id="div_modal_imagen_form"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
