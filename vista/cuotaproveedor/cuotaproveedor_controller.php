<?php

require_once('../../core/usuario_sesion.php');
require_once('Cuotaproveedor.class.php');
$oCuotaproveedor = new Cuotaproveedor();
require_once('Cuotaproveedordetalle.class.php');
$oCuotaproveedordetalle = new Cuotaproveedordetalle();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];
$tipo = $_POST['txt_cliente_tipo'];

if ($action == 'insertar') {

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar Cuota de Proveedor.';

    $array_detalle_fec = $_POST['txt_pagoproveedor_fec'];
    $array_detalle_imp = $_POST['txt_pagoproveedor_imp'];

    if (!is_array($array_detalle_fec)) {
        $data['estado'] = 0;
        $data['mensaje'] = 'Falta el cronograma de pagos = ' . $array_detalle_fec;
        echo json_encode($data);
        exit();
    }



    if (is_array($array_detalle_fec)) {
        for ($i = 0; $i < count($array_detalle_fec); $i++) {
            $polizadetalle_fec = fecha_mysql($array_detalle_fec[$i]);
            $polizadetalle_imp = moneda_mysql($array_detalle_imp[$i]);

            if ($tipo == 1) {
                $oCuotaproveedor->tb_proveedor_id = intval($_POST['hdd_cli_id']);
                $oCuotaproveedor->tb_cliente_id = 0;
            }
            if ($tipo == 2) {
                $oCuotaproveedor->tb_proveedor_id = 0;
                $oCuotaproveedor->tb_cliente_id = intval($_POST['hdd_cli_id']);
            }
            $oCuotaproveedor->tb_cuotaproveedor_id = intval($_POST['hdd_cuotaproveedor_id']);
            $oCuotaproveedor->tb_cuotaproveedor_monto = moneda_mysql($polizadetalle_imp);
            $oCuotaproveedor->tb_moneda_id = intval($_POST['cbm_moneda_id']);
            $oCuotaproveedor->tb_tipcam = moneda_mysql($_POST['txt_tipo_cambio']);
            $oCuotaproveedor->tb_nrocuota = 1;
            $oCuotaproveedor->tb_cuotaproveedor_tipo = intval($_POST['txt_cliente_tipo']);
            $oCuotaproveedor->tb_cuotaproveedor_tip = 1;
            $oCuotaproveedor->tb_cuotaproveedor_det = $_POST['txt_cuotaproveedor_detalle'];
            $oCuotaproveedor->tb_cuotaproveedor_fecven = fecha_mysql($polizadetalle_fec);
            $oCuotaproveedor->tb_cuenta_id = intval($_POST['cmb_cue_id']);
            $oCuotaproveedor->tb_subcuenta_id = intval($_POST['cmb_subcue_id']);
            $oCuotaproveedor->insertar();
        }
    }


    $data['estado'] = 1;
    $data['mensaje'] = 'Cuota Proveedor registrado correctamente.';
    echo json_encode($data);
} elseif ($action == 'modificar') {


    if ($tipo == 1) {
        $oCuotaproveedor->tb_proveedor_id = intval($_POST['hdd_cli_id']);
        $oCuotaproveedor->tb_cliente_id = 0;
    }
    if ($tipo == 2) {
        $oCuotaproveedor->tb_proveedor_id = 0;
        $oCuotaproveedor->tb_cliente_id = intval($_POST['hdd_cli_id']);
    }
    $oCuotaproveedor->tb_cuotaproveedor_id = intval($_POST['hdd_cuotaproveedor_id']);
    $oCuotaproveedor->tb_cuotaproveedor_monto = moneda_mysql($_POST['txt_monto']);
    $oCuotaproveedor->tb_moneda_id = intval($_POST['cbm_moneda_id']);
    $oCuotaproveedor->tb_tipcam = moneda_mysql($_POST['txt_tipo_cambio']);
    $oCuotaproveedor->tb_nrocuota = 1;
    $oCuotaproveedor->tb_cuotaproveedor_tipo = intval($_POST['txt_cliente_tipo']);
    $oCuotaproveedor->tb_cuotaproveedor_tip = 1;
    $oCuotaproveedor->tb_cuotaproveedor_det = $_POST['txt_cuotaproveedor_detalle'];
    $oCuotaproveedor->tb_cuotaproveedor_fecven = fecha_mysql($_POST['txt_cuotadetalle_fec']);
    $oCuotaproveedor->tb_cuenta_id = intval($_POST['cmb_cue_id']);
    $oCuotaproveedor->tb_subcuenta_id = intval($_POST['cmb_subcue_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar la Cuota de Proveedor.';

    if ($oCuotaproveedor->modificar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Cuota de Proveedor modificado correctamente. ';
    }

    echo json_encode($data);
} elseif ($action == 'abonar_interno') {

    $pago_en_banco = $_POST['cbo_pago'];
    //DANIEL ODAR 27-07-2024 si está haciendo pago por banco, validar que monto deposito y monto pagar sean iguales
        if ($pago_en_banco == 2 && $_POST['txt_monto_pagar'] != $_POST['txt_cuopag_mon']) {
            $data['estado'] = 0;
            $data['mensaje'] = 'El monto a pagar debe ser igual al monto de depósito';
            $data['estado2'] = 1; //error en el monto de pago

            echo json_encode($data); exit();
        }
    //

    $cuotaproveedor_id = intval($_POST['cuotaproveedor_id']);

    $result = $oCuotaproveedor->mostrarUno($cuotaproveedor_id);
    if ($result['estado'] == 1) {
        $proveedor_id = $result['data']['tb_proveedor_id'];
        $cliente_id = $result['data']['tb_cliente_id'];
        $cuotaproveedor_monto = $result['data']['tb_cuotaproveedor_monto'];
        $moneda_id = $result['data']['tb_moneda_id'];
        $tipcam = $result['data']['tb_tipcam'];
        $nrocuota = $result['data']['tb_nrocuota'];
        $cuotaproveedor_tipo = $result['data']['tb_cuotaproveedor_tipo'];
        $cuotaproveedor_tip = $result['data']['tb_cuotaproveedor_tip'];
        $cuotaproveedor_det = $result['data']['tb_cuotaproveedor_det'];
        $cuotaproveedor_fecven = $result['data']['tb_cuotaproveedor_fecven'];
        $cuenta_id = $result['data']['tb_cuenta_id'];
        $subcuenta_id = $result['data']['tb_subcuenta_id'];
    }
    $result = NULL;
    $moneda_depo=="S/. ";
    if($moneda_id==2)
      $moneda_depo=="US$/. ";
    //echo '$cuenta_id='.$cuenta_id.' $subcuenta_id='.$subcuenta_id.' $proveedor_id='.$proveedor_id.' $moneda_id='.$moneda_id;exit();

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al intentar registrar el pago de la Cuota';

    $oCuotaproveedordetalle->tb_cuotaproveedor_id = intval($_POST['cuotaproveedor_id']);
    $oCuotaproveedordetalle->tb_cuotaproveedordetalle_fec = fecha_mysql($_POST['txt_fecha_pago']);
    $oCuotaproveedordetalle->tb_cuotaproveedordetalle_sub = moneda_mysql($_POST['txt_monto_pagar']);
    $oCuotaproveedordetalle->tb_cuotaproveedordetalle_det = mb_strtoupper($_POST['txt_detalleproveedor']);

    $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
    $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
    $oEgreso->egreso_fec = fecha_mysql($_POST['txt_fecha_pago']);
    $oEgreso->documento_id = 9;
    $oEgreso->egreso_numdoc = '';
    $oEgreso->egreso_det = mb_strtoupper($cuotaproveedor_det . ' // ' . $_POST['txt_detalleproveedor'], 'UTF-8');
    $oEgreso->egreso_imp = moneda_mysql($_POST['txt_monto_pagar']);
    $oEgreso->egreso_tipcam = $tipcam;
    $oEgreso->egreso_est = 1;
    $oEgreso->cuenta_id = $cuenta_id;
    $oEgreso->subcuenta_id = $subcuenta_id;

    $oEgreso->proveedor_id = $proveedor_id;
    $oEgreso->cliente_id = 0;

    $oEgreso->usuario_id = 0;
    $oEgreso->caja_id = 1;
    $oEgreso->moneda_id = $moneda_id;
    $oEgreso->modulo_id = 0;
    $oEgreso->egreso_modide = 0;
    $oEgreso->empresa_id = $_SESSION['empresa_id'];
    $oEgreso->egreso_ap = 0; //si

    $resultado = $oCuotaproveedordetalle->insertar();
    if ($resultado['estado'] == 1) {
        $Cuotaproveedordetalle_id = $resultado['cuotaproveedordetalle_id'];
        $resultado2 = $oEgreso->insertar();
        if ($resultado2['estado'] == 1) {

            $egreso_id = $resultado2['egreso_id'];

            $data['estado'] = 1;
            $data['ing_id'] = $cuotaproveedor_id;
            $data['mensaje'] = 'Cuota Proveedor Registrado correctamente.';
        }

        $oCuotaproveedordetalle->modificar_campo($Cuotaproveedordetalle_id, 'tb_egreso_id', $egreso_id, 'INT');
    }
    
    if ($pago_en_banco == 2) {

         if (!empty($_POST['txt_cuopag_mon'])) {
                $cuopag_tot = $_POST['txt_cuopag_tot']; //monto total que debe pagar el cliente
                $fecha = fecha_mysql($_POST['txt_cuopag_fec']);

                //registro de ingreso
                $cue_id = 43; // INGRESO / EGRESO PORBANCO
                $subcue_id = 0; 
                
                if($_POST['cmb_banco_id']==1){
                    $subcue_id=147;// BANCO BCP
                }
                if($_POST['cmb_banco_id']==2){
                    $subcue_id=0;// no esta registrado todavia en el sistema para interbank
                }
                if($_POST['cmb_banco_id']==3){
                    $subcue_id = 148; // BANCO BBVA CONTINENTAL
                }
                
                $mod_id = 0; //cuota pago
                $modide=0;

                $ing_det = 'INGRESO FICTICIO DE CAJA EN BANCO = Pago abonado en el banco, la fecha de depósito fue: [ ' . $_POST['txt_cuopag_fec'] . ' ], el N° de operación fue: [ ' . $_POST['txt_cuopag_numope'] . ' ], el monto depositado fue: [ ' . $moneda_depo . ' ' . $_POST['txt_cuopag_mon'] . ' ] POR CONCEPTO DE=['.mb_strtoupper($cuotaproveedor_det . ' // ' . $_POST['txt_detalleproveedor'], 'UTF-8').']';

                $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
                $oIngreso->ingreso_fec = $fecha;
                $oIngreso->documento_id = 8;
                $oIngreso->ingreso_numdoc = '';
                $oIngreso->ingreso_det = $ing_det;
                $oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_mon']);
                $oIngreso->cuenta_id = $cue_id;
                $oIngreso->subcuenta_id = $subcue_id;
                $oIngreso->cliente_id = 1144;
                $oIngreso->caja_id = 1;
                $oIngreso->moneda_id =$moneda_id;
                //valores que pueden ser cambiantes según requerimiento de ingreso
                $oIngreso->modulo_id = $mod_id;
                $oIngreso->ingreso_modide = $modide;
                $oIngreso->empresa_id = $_SESSION['empresa_id'];
                
                $result = $oIngreso->insertar();
                if (intval($result['estado']) == 1) {
                    $ingr_id = $result['ingreso_id'];
                }
                $oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
                $oIngreso->ingreso_numope = $_POST['cuopag_numope'];
                $oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_cuopag_mon']);
                $oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
                $oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
                $oIngreso->banco_id = $_POST['cmb_banco_id'];
                $oIngreso->ingreso_id = $ingr_id;

                $oIngreso->registrar_datos_deposito_banco();
                //$oCuotaproveedordetalle->modificar_campo($Cuotaproveedordetalle_id, 'tb_ingreso_id', $ingr_id, 'INT');
                
            }
        
    }

    echo json_encode($data);
} elseif ($action == 'eliminar') {
    $cuotaproveedor_id = intval($_POST['hdd_cuotaproveedor_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el Area.';

    if ($oCuotaproveedor->eliminar($cuotaproveedor_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Area eliminado correctamente.';
    }

    echo json_encode($data);
} else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>