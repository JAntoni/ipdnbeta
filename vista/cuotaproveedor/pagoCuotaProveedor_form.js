
$(document).ready(function () {
    $('.monedas').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: parseFloat($("#hdd_saldo").val()) + 1
    });
    
        $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999999.99'
//        vMax: $("#hdd_saldo").val()
    });
    
    $('.moneda3').autoNumeric({
            aSep: ',',
            aDec: '.',
            //aSign: 'S/. ',
            //pSign: 's',
            vMin: '0.00',
            vMax: '999999.99'
    });
    $('.moneda4').autoNumeric({
            aSep: ',',
            aDec: '.',
            //aSign: 'S/. ',
            //pSign: 's',
            vMin: '0.00',
            vMax: '99999.00'
    });

    $(function () {
        $('#form_pago_cuotaproveedor').submit(function (event) {
            event.preventDefault();
            var pag = Number($('#txt_monto_pagar').autoNumeric('get'));
            if (!pag || pag == 0) {
//                alert('Ingrese un monto a pagar');
                swal_warning("AVISO",'Ingrese un monto a pagar',2500);
                return false;
            }

            //DANIEL ODAR 27-07-2024 si está haciendo pago por banco, validar que monto deposito y monto pagar sean iguales
            var forma_pago = parseInt($('#cbo_pago option:selected').val());
            var monto_pagar = $('#txt_monto_pagar').val();
            var monto_deposito = $('#txt_cuopag_mon').val();
            if (forma_pago == 2 && monto_deposito != monto_pagar) {
                alerta_error('SISTEMA', 'EL MONTO PAGO Y DE DEPOSITO DEBEN SER IGUALES');
                $('#txt_monto_pagar, #txt_cuopag_mon').addClass('input-shadow req');
                return;
            }

            $.ajax({
                type: "POST",
                url: VISTA_URL+"cuotaproveedor/cuotaproveedor_controller.php",
                async: true,
                dataType: "json",
                data: $('#form_pago_cuotaproveedor').serialize(),
                beforeSend: function () {

                },
                success: function (data) {
                    if (parseInt(data.ing_id) > 0) {
                        swal_success("SISTEMA",data.msj,2500);
                        cuotaproveedor_cronograma(data.ing_id);
                        cuotaproveedor_tabla();
                        $('#modal_pagarcuotaproveedor_registro').modal('hide');
                    }

                    //DANIEL ODAR 27-07-2024 validacion desde php de controller
                    if (data.estado == 0) {
                        alerta_error('SISTEMA', data.mensaje);
                        if (data.estado2 == 1) {
                            $('#txt_monto_pagar, #txt_cuopag_mon').addClass('input-shadow req');
                        }
                        return;
                    }
                },
                complete: function (data) {
                    if (data.statusText != "success") {
                        $('#msj_pagointerno').text('Error: ' + data.responseText);
                        console.log(data);
                    }
                }
            });

        });
    });
    
    
    
    
    $('#txt_cuopag_mon, #txt_cuopag_comi').change(function(event) {
        
            var monto_deposito = Number($('#txt_cuopag_mon').autoNumeric('get'));
            var monto_comision = Number($('#txt_cuopag_comi').autoNumeric('get'));
            var saldo_pagar = Number($('#hdd_saldo').autoNumeric('get'));
//            var cuenta_deposito = $('#cmb_cuedep_id').val();
            var saldo = 0;

            saldo = monto_deposito - monto_comision;
            
            if(saldo>(saldo_pagar)){
                swal_warning("AVISO","No puede pagar mas del Saldo a Pagar",3500);
                $('#txt_cuopag_mon').autoNumeric('set',saldo_pagar.toFixed(2));
                return false;
            }
            
            
            

            if(monto_deposito > 0 && monto_comision >= 0 && saldo > 0){
                    $('#txt_cuopag_montot').autoNumeric('set',saldo.toFixed(2));
                    $('#txt_cuopag_monval').autoNumeric('set',saldo.toFixed(2));
                    $('#txt_monto_pagar').autoNumeric('set',saldo.toFixed(2));
                    
            }
            else{
                    $('#txt_cuopag_montot').autoNumeric('set',0);
                    $('#txt_cuopag_monval').autoNumeric('set',0);
                    $('#txt_monto_pagar').autoNumeric('set',0);
            }

    });
    
    
    
    
    

});

$("#cbo_pago").change(function (){
    var pagohecho=$(this).val();
    if(pagohecho==1){
        activar_desactivar_campos_oficina_banco(true);
    }
    else if(pagohecho==2){
        activar_desactivar_campos_oficina_banco(false);
    }
});

function activar_desactivar_campos_oficina_banco(estado) {//funcion para activar ao deshabilitare los campos de acuerdo al pago hecho en banco o en oficina
    $("#txt_cuopag_fecdep").attr('readonly', estado);
    $("#txt_cuopag_fec").attr('readonly', estado);
    $("#cmb_cuedep_id").attr('readonly', estado);
    $("#cmb_banco_id").attr('readonly', estado);
    $("#txt_cuopag_numope").attr('readonly', estado);
    $("#txt_cuopag_mon").attr('readonly', estado);
    $("#txt_cuopag_comi").attr('readonly', estado);
}



