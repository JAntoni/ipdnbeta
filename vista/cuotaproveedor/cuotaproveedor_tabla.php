<?php
require_once ('../../core/usuario_sesion.php');
require_once('Cuotaproveedor.class.php');
$oCuotaproveedor = new Cuotaproveedor();
require_once('Cuotaproveedordetalle.class.php');
$oCuotaproveedordetalle = new Cuotaproveedordetalle();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../proveedor/Proveedor.class.php');
$oProveedor = new Proveedor();

require_once ('../funciones/funciones.php');
require_once ('../funciones/fechas.php');

$fecha1 = $_POST['fec1'];
$fecha2 = $_POST['fec2'];

$result = $oCuotaproveedor->listar_cuotas_Proveedores(fecha_mysql($fecha1), fecha_mysql($fecha2));

$hoy = date('Y-m-d');
$color = '';

$tr = '';
$moneda = 'S./';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $est = 0;
        if ($value['tb_moneda_id'] == 2) {
            $moneda = '$$';
        }
        if ($value['tb_cuotaproveedor_tipo'] == 1) {
            $result2 = $oProveedor->mostrarUno($value['tb_proveedor_id']);
            if ($result2['estado'] == 1) {
                $nombre = $result2['data']['tb_proveedor_nom'].$concatenar;
                if(strlen($result2['data']['tb_proveedor_doc'])>8){
                    $nombre .= '<br> <b style="color:green">'. 'Contacto : '.$result2['data']['tb_proveedor_con'].'</b>';
                }
                
            }
            $result2 = NULL;
        }
        if ($value['tb_cuotaproveedor_tipo'] == 2) {
            $result2 = $oCliente->mostrarUno($value['tb_cliente_id']);
            if ($result2['estado'] == 1) {
                $nombre = $result2['data']['tb_cliente_nom'];
            }
            $result2 = NULL;
        }

        $result2 = $oCuotaproveedordetalle->listar_cuotas_Proveedores($value['tb_cuotaproveedor_id']);
        if ($result2['estado'] == 1) {
            foreach ($result2['data']as $key => $value2) {
                $mon_pag += $value2['tb_cuotaproveedordetalle_sub'];
            }
        }
        $result2 = NULL;

        if ($mon_pag >= $value['tb_cuotaproveedor_monto']) {
            $estado = '<span class="badge bg-red">Cancelado</span>';
            $est = 0;
        } else {
            $estado = '<span class="badge bg-green">Vigente</span>';
            $est = 1;
        }

        if ((strtotime($hoy) > strtotime($value['tb_cuotaproveedor_fecven'])) && $est == 1) {
            $color = ' style="color:red;font-weight: bold"';
        }

        $dias = (strtotime($value['tb_cuotaproveedor_fecven']) - strtotime($hoy)) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);

        if ($dias <= 2 && $est == 1) {
            $color = ' style="color:red;font-weight: bold"';
        }


        $tr .= '<tr id="tabla_cabecera_fila" ' . $color . '>';
        $tr .= '
            <td id="tabla_fila">' . $value['tb_cuotaproveedor_id'] . '</td>
            <td id="tabla_fila">' . mostrar_fecha($value['tb_cuotaproveedor_fecven']) . '</td>
            <td id="tabla_fila">' . $nombre . '</td>
            <td id="tabla_fila">' . $value['tb_cuotaproveedor_monto'] . '</td>
            <td id="tabla_fila">' . $value['tb_cuotaproveedor_det'] . '</td>
            <td id="tabla_fila">' . $moneda . '</td>
            <td id="tabla_fila">' . $value['tb_tipcam'] . '</td>
            <!--<td id="tabla_fila">' . $value['tb_nrocuota'] . '</td>-->
            <td id="tabla_fila">' . $estado . '</td>
          <td id="tabla_fila" align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="cuotaproveedor_form(\'L\',' . $value['tb_cuotaproveedor_id'] . ')"><i class="fa fa-eye"></i></a>';
             if($est==1){   
            $tr .='<a class="btn btn-warning btn-xs" title="Editar" onclick="cuotaproveedor_form(\'M\',' . $value['tb_cuotaproveedor_id'] . ')"><i class="fa fa-edit"></i></a>
            <!--<a class="btn btn-danger btn-xs" title="Eliminar" onclick="cuotaproveedor_form(\'E\',' . $value['tb_cuotaproveedor_id'] . ')"><i class="fa fa-trash"></i></a>-->';
             }
          $tr .='</td>';
        $tr .= '</tr>';
        $color = '';
        $est = 0;
        $estado = '';
        $mon_pag = 0;
    }
    $result = null;
}
?>
<table id="tbl_cuotaproveedors" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">FEC. VENCIMIENTO</th>
            <th id="tabla_cabecera_fila">CLIENTE / PROVEEDOR</th>
            <th id="tabla_cabecera_fila">MONTO</th>
            <th id="tabla_cabecera_fila">DETALLE</th>
            <th id="tabla_cabecera_fila">MONEDA</th>
            <th id="tabla_cabecera_fila">T. CAMBIO</th>
            <!--<th id="tabla_cabecera_fila">NRO CUOTAS</th>-->
            <th id="tabla_cabecera_fila">ESTADO</th>
            <th id="tabla_cabecera_fila">OPCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>
