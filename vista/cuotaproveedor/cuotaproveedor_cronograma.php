
<?php
require_once('../../core/usuario_sesion.php');
require_once('Cuotaproveedor.class.php');
$oCuotaproveedor = new Cuotaproveedor();
require_once('Cuotaproveedordetalle.class.php');
$oCuotaproveedordetalle = new Cuotaproveedordetalle();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

//$tb_cuotaproveedor_id = 1;
$tb_cuotaproveedor_id = $_POST['cuotaproveedor_id'];
$fecha_hoy = date('Y-m-d');
// 
//----------------- INICIO DE FORMULAR ES ÚNICA PAR TODO TIPO DE PRÉSTAMO -------------------//
//----------------------------- FIN FORMULA--------------------------------//
?>


<?php
$result = $oCuotaproveedordetalle->listar_cuotas_Proveedores($tb_cuotaproveedor_id);
$j = 0;
if ($result['estado'] == 1) {
    ?>
    <table style="width:100%" class="table table-bordered table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila" align="center">ID</th>
                <th id="tabla_cabecera_fila" align="center">DOCUMENTO</th>
                <th id="tabla_cabecera_fila" align="center">FECHA</th>
                <th id="tabla_cabecera_fila" align="center">MONTO</th>
                <th id="tabla_cabecera_fila" align="center" width="50%">DETALLE</th>
                <th id="tabla_cabecera_fila" align="center"></th>
            </tr>
        </thead>
        <tbody>

            <?php
            foreach ($result['data']as $key => $value) {
                $j++;
                $resultado=$oEgreso->mostrarUno($value['tb_egreso_id']);
                if($resultado['estado']==1){
                    $doc=$resultado['data']['tb_documento_abr'];
                    $doc2=$resultado['data']['tb_egreso_numdoc'];
                }
                ?>
                <tr id="tabla_cabecera_fila" >
                    <td id="tabla_fila" align="center"><?php echo $j; ?></td>
                    <td id="tabla_fila"><a style="color: blue;font-family: cambria;font-weight: bold;" title="Imprimir" href="javascript:void(0)" onClick="egreso_imppos_datos(<?php echo $value['tb_egreso_id'] ?>)"><?php echo $doc.'-'.$doc2; ?></a></td>
                    <td id="tabla_fila" align="center"><?php echo mostrar_fecha($value['tb_cuotaproveedordetalle_fec']) ?></td>
                    <td id="tabla_fila" align="center"><?php echo mostrar_moneda($value['tb_cuotaproveedordetalle_sub']) ?></td>
                    <td id="tabla_fila" align="center"><?php echo $value['tb_cuotaproveedordetalle_det'] ?></td>
                    <td id="tabla_fila" align="center">
                        <!--<a class="btn btn-danger btn-xs" title="Eliminar" onclick="EliminarCuotaDetalle(<?php echo $value['tb_egreso_id'] ?>)"><i class="fa fa-trash"></i></a>-->
                        <!--<a class="btn btn-warning btn-xs" title="Editar" onclick="egreso_form('M',<?php echo $value['tb_egreso_id'] ?>)"><i class="fa fa-edit"></i></a>-->
                        <a class="btn btn-success btn-xs" title="Subir Imagen" onclick="upload_form('I',<?php echo $value['tb_egreso_id'] ?>)"><i class="fa fa-picture-o"></i></a>
                        <a class="btn btn-facebook btn-xs" title="Ver Imagen" onclick="Abrir_imagen(<?php echo $value['tb_egreso_id'] ?>)"><i class="fa fa-camera"></i></a>
                        <?php if ($value['tb_cuotaproveedordetalle_fec'] == $fecha_hoy) { ?>
                            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="egreso_form('E',<?php echo $value['tb_egreso_id'] ?>)"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
