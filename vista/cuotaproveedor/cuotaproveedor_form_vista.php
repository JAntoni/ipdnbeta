<div class="row">
    
    <?php
    if($action == 'insertar'){
        $col_md="col-md-7";
        $display="display: block";
    }
    else{
         $col_md="col-md-12";
         $display="display: none";
    }
    
    ?>
    <div class="<?php echo $col_md?>" id="div_7">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-4">
                        <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="txt_cliente_tipo" id="txt_cliente_tipo1" class="flat-green" value="1" <?php if ($cuotaproveedor_tipo == 1 || $cuotaproveedor_tipo == 0) echo 'checked'; ?> > Proveedor 
                        </label>
                        <!--                <label class="radio-inline">
                                            <input type="radio" name="txt_cliente_tipo" id="txt_cliente_tipo2" class="flat-green" value="2" <?php if ($cuotaproveedor_tipo == 2) echo 'checked'; ?> > Cliente
                                        </label>-->
                    </div>
                    <div class="col-md-4">
                        <label class="radio-inline" style="padding-left: 0px;">
                            <input type="radio" name="txt_proveedor_variable" id="txt_proveedor_variable1" class="flat-green" value="1" <?php if ($cuotaproveedor_variable == 1 || $cuotaproveedor_variable == 0) echo 'checked'; ?> > Fijo 
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="txt_proveedor_variable" id="txt_proveedor_variabl2" class="flat-green" value="2" <?php if ($cuotaproveedor_variable == 2) echo 'checked'; ?> > Variable
                        </label>
                    </div>
                    
                    <div class="col-md-4">
                        <input type="text" id="txt_cuotas" name="txt_cuotas" class="form-control input-sm mayus" value="1" style="font-size: 15px;text-align: center;font-weight: bold">
                       
                    </div>

                </div>
                <p>
                <div class="row">
                    <div class="col-md-8">
                        <label for="txt_cli_nom" class="control-label">Proveedor</label>
                        <div class="input-group">
                            <input type="text" id="txt_cli_nom" name="txt_cli_nom" class="form-control input-sm mayus" value="<?php echo $nombre; ?>">
                            <input type="hidden" id="hdd_cli_id" name="hdd_cli_id" class="form-control input-sm mayus" value="<?php echo $proveedor_id; ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="button" onclick="proveedor_form('I', 0)">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                     <div class="form-group col-md-4">
                        <label for="txt_cuotadetalle_fec" class="control-label">Fecha</label>
                        <div class='input-group date' id='datetimepicker11'>
                            <input type='text' class="form-control" name="txt_cuotadetalle_fec" id="txt_cuotadetalle_fec" value="<?php echo mostrar_fecha($cuotaproveedor_fecven); ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Moneda</label>
                            <select class="form-control input-sm" name="cbm_moneda_id" id="cbm_moneda_id">
                                <?php include_once '../moneda/moneda_select.php'; ?>
                            </select>
                        </div>
                    </div>
<!--                </div>
                <p>
                <div class="row">-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Monto Total</label>
                            <div class="input-group">
                                <input type="text" id="txt_monto" name="txt_monto" class="form-control input-sm mayus moneda" value="<?php echo $cuotaproveedor_monto; ?>" style="font-size: 15px;text-align: center;font-weight: bold">
                            <span class="input-group-btn">
                             
                                <?php
                                if($action == 'insertar'){
                                ?>  
                                <button class="btn btn-primary btn-sm" type="button" onclick="agregar_detalle()">
                                    <span class="fa fa-plus icon"></span>
                                </button>
                                <?php
                                }
 
                                ?>  
                                
                                
                            </span>
                        </div>
                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tipo Cambio</label>
                            <input type="text" class="form-control input-sm" name="txt_tipo_cambio"  id="txt_tipo_cambio" readonly=""  value="<?php echo $tipcam; ?>" style="font-size: 15px;text-align: center;font-weight: bold">
                        </div>
                    </div>
                </div>
                <p>
                <div class="row">    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Cuenta</label>
                            <select name="cmb_cue_id" id="cmb_cue_id" class="form-control input-sm">
                                <?php include '../cuenta/cuenta_select.php'; ?>
                            </select>
                        </div>
                    </div>
<!--                </div>
                <div class="row"> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Sub Cuenta</label>
                            <select name="cmb_subcue_id" id="cmb_subcue_id" class="form-control input-sm">
                                <?php include '../subcuenta/subcuenta_select.php'; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <p>
                <div class="row">      
                    <div class="col-md-11">
                        <div class="form-group">
                            <label for="">Detalle General</label>
                            <textarea class="form-control input-sm" name="txt_cuotaproveedor_detalle" id="txt_cuotaproveedor_detalle"><?php echo $cuotaproveedor_det; ?></textarea>
                        </div>
                    </div>
                    <?php if ($usuario_action == 'L'): ?>
                        <div class="col-md-1">
                            <label>&nbsp;&nbsp;&nbsp;</label>
                            <button type="button" class="btn btn-success btn-sm" onclick="pagarCuotaProveedor(<?php echo $cuotaproveedor_id; ?>)"><i class="fa fa-money"></i></button>
                        </div>
                    <?php endif; ?>
                    <!--            <div class="col-md-1">
                                    <label>&nbsp;&nbsp;&nbsp;</label>
                                    <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-plus"></i></button>
                                </div>-->
                </div>
                <p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="callout callout-info" id="cronograma_mensaje_tbl" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Listando cronograma...</h4>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="div_cuotaproveedor_cronograma">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-5"  id="div_5" style="<?php echo $display?>">
        <div class="box box-primary">
            <div class="box-header">
                
                <div class="row">
                    <div class="col-md-12">
                        <table width="100%" class="table" id="tabla_controlsegurodetalle">
                            <thead>
                                <tr id="tabla_cabecera">
                                    <th id="tabla_cabecera_fila" width="10px">N°</th>
                                    <th id="tabla_cabecera_fila" width="100px">FECHA VENC.</th>
                                    <th id="tabla_cabecera_fila" width="100px">IMPORTE</th>
                                    <th id="tabla_cabecera_fila" width="10px"></th>
                                </tr>
                            </thead>
                            <tbody id="tabla_detalle" style="text-align: center;">

                            </tbody>
                        </table>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
    
</div>