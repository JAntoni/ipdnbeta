/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    cambio_moneda(1);
    Habilitar(true);


//    $("#txt_cuopag_numope").prop("readonly",true);

    $('#datetimepicker11').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate : new Date()
    });

    $('#txt_cuotaproveedor_detalle').autocomplete({
        minLength: 1,
        source: VISTA_URL + "egreso/egreso_complete_det.php"
    });

    $('#txt_cuotaproveedor_detalle').change(function () {
        $(this).val($(this).val().toUpperCase());
    });

    var action = $("#action").val();
    var cuotaproveedor_id = $("#hdd_cuotaproveedor_id").val();

    if (action != 'insertar') {
        cuotaproveedor_cronograma(cuotaproveedor_id);

    }
    if (action == 'insertar') {
        Cuenta_select_form();

    }

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $("#txt_cli_nom").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "proveedor/proveedor_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            console.log(ui);
            $('#hdd_cli_id').val(ui.item.tb_proveedor_id);
            $('#txt_cli_nom').val(ui.item.tb_proveedor_nom);
            event.preventDefault();
            $('#txt_cli_nom').focus();
        }
    });



    $('.numero').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '0',
        vMax: '999999999'
    });

    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        vMin: '0.00',
        vMax: '999999999.00'
    });

});



function Habilitar(estado) {
    $("#txt_cuopag_numope").attr('readonly', estado);
    $("#cmb_cuedep_id").attr('readonly', estado);
    $("#cmb_banco_id").attr('readonly', estado);
    $("#txt_cuopag_mon").attr('readonly', estado);
    $("#txt_cuopag_comi").attr('readonly', estado);
}

$('#cmb_opcion').change(function (event) {
    var valor = $(this).val();
    if (valor == 1) {
        Habilitar(true);
    } else {
        Habilitar(false);
    }
});


function cambio_moneda(monid) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambio/monedacambio_controller.php",
        async: false,
        dataType: "json",
        data: ({
            action: 'obtener_dato',
            mon_id: monid
        }),
        beforeSend: function () {
            $('#msj_pagobanco').hide(100);
        },
        success: function (data) {
            if (data.moncam_val == null) {
                $('#txt_tipo_cambio').val('');
                swal_warning('AVISO', 'Por favor registre el tipo de cambio del día de la fecha de depósito', 6000);
            } else
                $('#txt_tipo_cambio').val(data.moncam_val);
        },
        complete: function (data) {
//                 console.log(data);
        }
    });
}

$('#cbm_moneda_id').change(function (event) {
    var moneda = $('#cbm_moneda_id').val();
    cambio_moneda(moneda);
});





$('#txt_monto, #txt_nrocuotas').change(function (event) {
    var txt_monto = Number($('#txt_monto').autoNumeric('get'));
    var txt_nrocuotas = 1;
//    var txt_nrocuotas = parseInt($('#txt_nrocuotas').val());
    var cuotatipo_id = parseInt($('#txt_cliente_tip').val());
//    
    if (cuotatipo_id == 2) //cuota libre
        txt_monto = txt_monto / txt_nrocuotas; //solo calcular el interés de un solo mes

    if (txt_monto > 0) {

    }
});




function cuotaproveedor_cronograma(cuotaproveedor_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuotaproveedor/cuotaproveedor_cronograma.php",
        async: true,
        dataType: "html",
        data: ({
            cuotaproveedor_id: cuotaproveedor_id
        }),
        beforeSend: function () {
            $('#cronograma_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_cuotaproveedor_cronograma').html(data);
            $('#cronograma_mensaje_tbl').hide(300);
        },
        complete: function (data) {
//            console.log(data);
        },
        error: function (data) {
            $('#cronograma_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRONOGRAMA: ' + data.responseText);
        }
    });
}





$('#form_cuotaproveedor').validate({
    submitHandler: function () {
        $.ajax({
            type: "POST",
            url: VISTA_URL + "cuotaproveedor/cuotaproveedor_controller.php",
            async: true,
            dataType: "json",
            data: $("#form_cuotaproveedor").serialize(),
            beforeSend: function () {
                $('#cuotaproveedor_mensaje').show(400);
                $('#btn_guardar_cuotaproveedor').prop('disabled', true);
            },
            success: function (data) {
                if (parseInt(data.estado) > 0) {
                    $('#cuotaproveedor_mensaje').removeClass('callout-info').addClass('callout-success')
                    $('#cuotaproveedor_mensaje').html(data.mensaje);
                    setTimeout(function () {
                        cuotaproveedor_tabla();
                        $('#modal_registro_cuotaproveedor').modal('hide');
                    }, 1000
                            );
                } else {
                    $('#cuotaproveedor_mensaje').removeClass('callout-info').addClass('callout-warning')
                    $('#cuotaproveedor_mensaje').html('Alerta: ' + data.mensaje);
                    $('#btn_guardar_cuotaproveedor').prop('disabled', false);
                }
            },
            complete: function (data) {
                //console.log(data);
            },
            error: function (data) {
                $('#cuotaproveedor_mensaje').removeClass('callout-info').addClass('callout-danger')
                $('#cuotaproveedor_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
            }
        });
    },
    rules: {
        txt_cuotaproveedor_nom: {
            required: true,
            minlength: 2
        },
        txt_cuotaproveedor_des: {
            required: true,
            minlength: 5
        }
    },
    messages: {
        txt_cuotaproveedor_nom: {
            required: "Ingrese un nombre para el Area",
            minlength: "Como mínimo el nombre debe tener 2 caracteres"
        },
        txt_cuotaproveedor_des: {
            required: "Ingrese una descripción para el Area",
            minlength: "La descripción debe tener como mínimo 5 caracteres"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
});


function pagarCuotaProveedor(cuotaproveedor_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuotaproveedor/pagoCuotaProveedor_form.php",
        async: true,
        dataType: "html",
        data: ({
            cuotaproveedor_id: cuotaproveedor_id
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_pagarcuotaproveedor_registro').html(html);
            $('#modal_pagarcuotaproveedor_registro').modal('show');
            modal_hidden_bs_modal('modal_pagarcuotaproveedor_registro', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_pagarcuotaproveedor_registro',40);
        },
        complete: function (data) {

        }
    });
}





function Cuenta_select_form() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuenta/cuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            tipocuenta: 2
        }),
        beforeSend: function () {
            $('#cmb_cue_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_cue_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}



function SubCuenta_select_form(cuenta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "subcuenta/subcuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            cuenta_id: cuenta_id
//			vista_subcategoria: 'producto'
        }),
        beforeSend: function () {
            $('#cmb_subcue_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cmb_subcue_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}


$('#cmb_cue_id').change(function () {
    SubCuenta_select_form($('#cmb_cue_id').val());
});



function proveedor_form(usuario_act, proveedor_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "proveedor/proveedor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            proveedor_id: proveedor_id,
            vista: 'proveedor'
        }),
        beforeSend: function () {
//            $('#h3_modal_title').text('Cargando Formulario');
//            $('#modal_mensaje').modal('show');
        },
        success: function (data) {

            $('#div_modal_proveedor_form').html(data);
            $('#modal_registro_proveedor').modal('show');

            modal_hidden_bs_modal('modal_registro_proveedor', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {

        },
        error: function (data) {

        }
    });
}



function egreso_form(usuario_act, egreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            egreso_id: egreso_id,
            vista: 'egreso_proveedor'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_egreso_form').html(data);
                $('#modal_registro_egreso').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_egreso'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_egreso'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_egreso', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'egreso';
                var div = 'div_modal_egreso_form';
                permiso_solicitud(usuario_act, egreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}




function upload_form(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'egreso', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
//                modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}


function agregar_detalle() {

    var cuotas = $('#txt_cuotas').val();

    let fecha1 = $('#txt_cuotadetalle_fec').val();
    let newdate = fecha1.split("-").reverse().join("-");
    let fec = new Date(newdate);
    var tabla = '';
    let monto=0.00;

    var rowCount = $("#tabla_detalle tr").length;

    var variable = $('input:radio[name=txt_proveedor_variable]:checked').val();

    if(variable==1){
        monto=$('#txt_monto').val();
    }else{
        monto=$('#txt_monto').val()/cuotas;
    }
    
    if(monto==0 || monto==""){
        swal_warning("AVISO","Debe Ingresar un Monto mayor a 0",2500);
        return false;
    }
    
    
    
//    if (rowCount == 0) {
    if (parseInt(cuotas) > 0) {
        let fechadetalle = new Date(new Date(fec).setMonth(fec.getMonth()));
        let mes = fechadetalle.getMonth() + 1;

        if (mes > 9) {
            mes = mes;
        } else {
            mes = '0' + mes;
        }
        let newdate2 = fechadetalle.getDate() + "-" + (mes) + "-" + fechadetalle.getFullYear();


        for (var i = 1; i <= parseInt(cuotas); i++) {

            tabla += '<tr id="tabla_cabecera_fila">';
            tabla += '<td id="tabla_fila">' + i + '</td>';
            tabla += '<td id="tabla_fila"><input class="form-control input-sm fecha_detalle" type="text" name="txt_pagoproveedor_fec[]" value="' + newdate2 + '" style="font-size: 15px;text-align: center;font-weight: bold"></td>';
            tabla += '<td id="tabla_fila"><input class="form-control input-sm cuota_poliza" type="text" name="txt_pagoproveedor_imp[]" value="' + monto + '" style="font-size: 15px;text-align: center;font-weight: bold"></td>';
            tabla += '<td id="tabla_fila"></td>';
            tabla += '</tr>';

            fechadetalle = new Date(new Date(fec).setMonth(fec.getMonth() + i));
            let mes = fechadetalle.getMonth() + 1;

            if (mes > 9) {
                mes = mes;
            } else {
                mes = '0' + mes;
            }
            newdate2 = fechadetalle.getDate() + "-" + (mes) + "-" + fechadetalle.getFullYear();
        }
        $('#tabla_detalle').html(tabla);

    } else {
        swal_warning("AVISO", "DEBE INGRESAR NÚMERO MAYOR A 0");
        return false;
    }
//        }


    $('#tabla_detalle').find('.fecha_detalle').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $('#tabla_detalle').find('.cuota_poliza').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });
}