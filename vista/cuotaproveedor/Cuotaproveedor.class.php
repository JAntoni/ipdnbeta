<?php
if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Cuotaproveedor extends Conexion{
    public $tb_cuotaproveedor_id;
    public $tb_cuotaproveedor_reg;
    public $tb_cuotaproveedor_xac;
    public $tb_proveedor_id;
    public $tb_cliente_id;
    public $tb_cuotaproveedor_monto;//fecha de pago de la cuotaproveedor
    public $tb_moneda_id;//moneda 1. soles 2.dolares
    public $tb_tipcam;//tipo de cambio segun la moneda
    public $tb_nrocuota;//numero de cuotas a pagar
    public $tb_cuotaproveedor_tipo;//numero de cuotas a pagar
    public $tb_cuotaproveedor_tip;//numero de cuotas a pagar
    public $tb_cuotaproveedor_det;//detalle general de la cuota 
    public $tb_cuotaproveedor_fecven;// fecha de vencimiento
    public $tb_cuenta_id;// fecha de vencimiento
    public $tb_subcuenta_id;// fecha de vencimiento
    
    
    
    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cuotaproveedor (
                                            tb_proveedor_id,
                                            tb_cliente_id,
                                            tb_cuotaproveedor_monto,
                                            tb_moneda_id,
                                            tb_tipcam,
                                            tb_nrocuota,
                                            tb_cuotaproveedor_tipo,
                                            tb_cuotaproveedor_tip,
                                            tb_cuotaproveedor_det,
                                            tb_cuotaproveedor_fecven,
                                            tb_cuenta_id,
                                            tb_subcuenta_id) 
                                    VALUES (
                                            :tb_proveedor_id,
                                            :tb_cliente_id,
                                            :tb_cuotaproveedor_monto,
                                            :tb_moneda_id,
                                            :tb_tipcam,
                                            :tb_nrocuota,
                                            :tb_cuotaproveedor_tipo,
                                            :tb_cuotaproveedor_tip,
                                            :tb_cuotaproveedor_det,
                                            :tb_cuotaproveedor_fecven,
                                            :tb_cuenta_id,
                                            :tb_subcuenta_id);";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_proveedor_id", $this->tb_proveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cliente_id", $this->tb_cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaproveedor_monto", $this->tb_cuotaproveedor_monto, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tipcam", $this->tb_tipcam, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_nrocuota", $this->tb_nrocuota, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaproveedor_tipo", $this->tb_nrocuota, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaproveedor_tip", $this->tb_nrocuota, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaproveedor_det", $this->tb_cuotaproveedor_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedor_fecven", $this->tb_cuotaproveedor_fecven, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuenta_id", $this->tb_cuenta_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_subcuenta_id", $this->tb_subcuenta_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $cuotaproveedor_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['tb_cuotaproveedor_id'] = $cuotaproveedor_id;
        return $data;
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuotaproveedor SET  
                                            tb_proveedor_id =:tb_proveedor_id, 
                                            tb_cuotaproveedor_monto =:tb_cuotaproveedor_monto,
                                            tb_moneda_id=:tb_moneda_id,
                                            tb_tipcam=:tb_tipcam,
                                            tb_cuotaproveedor_fecven=:tb_cuotaproveedor_fecven,                                
                                            tb_cuotaproveedor_det=:tb_cuotaproveedor_det
                                        WHERE 
                                            tb_cuotaproveedor_id =:tb_cuotaproveedor_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_proveedor_id", $this->tb_proveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaproveedor_monto", $this->tb_cuotaproveedor_monto, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_moneda_id", $this->tb_moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tipcam", $this->tb_tipcam, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedor_fecven", $this->tb_cuotaproveedor_fecven, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedor_det", $this->tb_cuotaproveedor_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedor_id", $this->tb_cuotaproveedor_id, PDO::PARAM_INT);
        

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar($cuotaproveedor_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cuotaproveedor WHERE tb_cuotaproveedor_id =:tb_cuotaproveedor_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuotaproveedor_id", $cuotaproveedor_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($cuotaproveedor_id){
      try {
          
        $sql="SELECT * 
                    FROM tb_cuotaproveedor 
                    WHERE tb_cuotaproveedor_id=:cuotaproveedor_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuotaproveedor_id", $cuotaproveedor_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_campo($cuotaproveedor_id, $cuotaproveedor_columna, $cuotaproveedor_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($cuotaproveedor_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_cuotaproveedor SET ".$cuotaproveedor_columna." =:cuotaproveedor_valor WHERE tb_cuotaproveedor_id =:cuotaproveedor_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cuotaproveedor_id", $cuotaproveedor_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":cuotaproveedor_valor", $cuotaproveedor_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":cuotaproveedor_valor", $cuotaproveedor_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function listar_cuotas_Proveedores($fecha1, $fecha2){
      try {
        $sql = "SELECT * FROM tb_cuotaproveedor WHERE tb_cuotaproveedor_xac = 1 AND DATE(tb_cuotaproveedor_fecven) BETWEEN :fecha1 AND :fecha2;";

        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Cuotas de Pagos de Proveedores";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
}
