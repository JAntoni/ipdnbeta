<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');

class Cuotaproveedordetalle extends Conexion{
    
    public $tb_cuotaproveedordetalle_id;
    public $tb_cuotaproveedor_id;//cuota proveedor id
    public $tb_cuotaproveedordetalle_fec;
    public $tb_cuotaproveedordetalle_sub;//sub total del monto a pagar
    public $tb_cuotaproveedordetalle_cuota;//numero de cuota a pagar
    public $tb_cuotaproveedordetalle_estado;//1.pagado 2.sin pagar
//    public $tb_cuotaproveedordetalle_tipo;//1.fijo 2. variable
    public $tb_cuotaproveedordetalle_det;//detalle de la cuota
    public $tb_egreso_id;//detalle de la cuota
    
   
    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cuotaproveedordetalle (
                                            tb_cuotaproveedor_id,
                                            tb_cuotaproveedordetalle_fec,
                                            tb_cuotaproveedordetalle_sub,
                                            tb_cuotaproveedordetalle_det) 
                                    VALUES (
                                            :tb_cuotaproveedor_id,
                                            :tb_cuotaproveedordetalle_fec,
                                            :tb_cuotaproveedordetalle_sub,
                                            :tb_cuotaproveedordetalle_det);";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuotaproveedor_id", $this->tb_cuotaproveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_fec", $this->tb_cuotaproveedordetalle_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_sub", $this->tb_cuotaproveedordetalle_sub, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_det", $this->tb_cuotaproveedordetalle_det, PDO::PARAM_STR);
        
        $result = $sentencia->execute();
        $cuotaproveedordetalle_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $data['estado'] = $result; //si es correcto el ingreso retorna 1
        $data['cuotaproveedordetalle_id'] = $cuotaproveedordetalle_id;
        return $data;
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_cuotaproveedordetalle SET  
                                            cuotaproveedor_id=:cuotaproveedor_id,
                                            tb_cuotaproveedordetalle_fec=:tb_cuotaproveedordetalle_fec,
                                            tb_cuotaproveedordetalle_sub=:tb_cuotaproveedordetalle_sub,
                                            tb_cuotaproveedordetalle_det=:tb_cuotaproveedordetalle_det
                                        WHERE 
                                            tb_cuotaproveedordetalle_id =:tb_cuotaproveedordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuotaproveedor_id", $this->tb_cuotaproveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_fec", $this->tb_cuotaproveedordetalle_fec, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_sub", $this->tb_cuotaproveedordetalle_sub, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_det", $this->tb_cuotaproveedordetalle_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_id", $this->tb_cuotaproveedordetalle_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar($tb_cuotaproveedordetalle_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cuotaproveedordetalle WHERE tb_cuotaproveedordetalle_id =:tb_cuotaproveedordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuotaproveedorproveedor_id", $tb_cuotaproveedordetalle_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminarEgreso($tb_egreso_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cuotaproveedordetalle WHERE tb_egreso_id =:tb_egreso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_egreso_id", $tb_egreso_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUnoEgreso($tb_egreso_id){
      try {
          
        $sql="SELECT * 
                    FROM tb_cuotaproveedordetalle 
                    WHERE tb_egreso_id=:tb_egreso_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_egreso_id", $tb_egreso_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrarUno($cuotaproveedordetalle_id){
      try {
          
        $sql="SELECT * 
                    FROM tb_cuotaproveedordetalle 
                    WHERE tb_cuotaproveedordetalle_id=:tb_cuotaproveedordetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuotaproveedordetalle_id", $cuotaproveedordetalle_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function modificar_campo($cuotaproveedordetalle_id, $cuotaproveedordetalle_columna, $cuotaproveedordetalle_valor, $param_tip){
      $this->dblink->beginTransaction();
      try {
        if(!empty($cuotaproveedordetalle_columna) && ($param_tip == 'INT' || $param_tip == 'STR')){

          $sql = "UPDATE tb_cuotaproveedordetalle SET ".$cuotaproveedordetalle_columna." =:cuotaproveedordetalle_valor WHERE tb_cuotaproveedordetalle_id =:cuotaproveedordetalle_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cuotaproveedordetalle_id", $cuotaproveedordetalle_id, PDO::PARAM_INT);
          if($param_tip == 'INT')
            $sentencia->bindParam(":cuotaproveedordetalle_valor", $cuotaproveedordetalle_valor, PDO::PARAM_INT);
          else
            $sentencia->bindParam(":cuotaproveedordetalle_valor", $cuotaproveedordetalle_valor, PDO::PARAM_STR);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1
        }
        else
          return 0;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function listar_cuotas_Proveedores($tb_cuotaproveedor_id){
      try {
        $sql = "SELECT * FROM tb_cuotaproveedordetalle WHERE tb_cuotaproveedor_id=:tb_cuotaproveedor_id AND tb_cuotaproveedordetalle_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cuotaproveedor_id", $tb_cuotaproveedor_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Cuotas de Pagos de Proveedores";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
}
