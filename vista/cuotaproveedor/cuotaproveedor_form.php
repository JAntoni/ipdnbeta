<?php
require_once ('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('Cuotaproveedor.class.php');
$oCuotaproveedor = new Cuotaproveedor();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once('../proveedor/Proveedor.class.php');
$oProveedor = new Proveedor();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$direc = 'cuotaproveedor';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$cuotaproveedor_id = $_POST['cuotaproveedor_id'];
$cuotaproveedor_fecven=date('d-m-Y');

$titulo = '';
if ($usuario_action == 'L') {
    $titulo = 'Cuota Proveedor Registrado';
}
elseif ($usuario_action == 'I') {
    $titulo = 'Registrar Cuota Proveedor';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Cuota Proveedor';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Cuota Proveedor';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php
//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en cuotaproveedor
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if (in_array($usuario_action, $array_permisos))
        $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if ($bandera == 0) {
        $usuario_id = $_SESSION['usuario_id'];
        $modulo = 'cuotaproveedor';
        $modulo_id = $cuotaproveedor_id;
        $tipo_permiso = $usuario_action;
        $estado = 1;

        $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if ($result['estado'] == 1) {
            $bandera = 1; // si tiene el permiso para la accion que desea hacer
        } else {
            $result = NULL;
            echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
            exit();
        }
        $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del cuotaproveedor por su ID
    if (intval($cuotaproveedor_id) > 0) {
        $result = $oCuotaproveedor->mostrarUno($cuotaproveedor_id);
        if ($result['estado'] != 1) {
            $mensaje = 'No se ha encontrado ningún registro para el cuotaproveedor seleccionado, inténtelo nuevamente.';
            $bandera = 4;
        } else {
            
            $proveedor_id = $result['data']['tb_proveedor_id'];
            $cliente_id = $result['data']['tb_cliente_id'];
            
             if ($result['data']['tb_cuotaproveedor_tipo'] == 1) {
                $result2 = $oProveedor->mostrarUno($result['data']['tb_proveedor_id']);
                if ($result2['estado'] == 1) {
                    $nombre = $result2['data']['tb_proveedor_nom'];
                }
                $result2 = NULL;
            }
            
            if ($result['data']['tb_cuotaproveedor_tipo'] == 2) {
                $result2 = $oCliente->mostrarUno($result['data']['tb_cliente_id']);
                if ($result2['estado'] == 1) {
                    $nombre = $result2['data']['tb_cliente_nom'];
                }
                $result2 = NULL;
            }
            
            $cuotaproveedor_monto = $result['data']['tb_cuotaproveedor_monto'];
            $moneda_id = $result['data']['tb_moneda_id'];
            $tipcam = $result['data']['tb_tipcam'];
            $nrocuota = $result['data']['tb_nrocuota'];
            $cuotaproveedor_tipo = $result['data']['tb_cuotaproveedor_tipo'];
            $cuotaproveedor_tip = $result['data']['tb_cuotaproveedor_tip'];
            $cuotaproveedor_det = $result['data']['tb_cuotaproveedor_det'];
            $cuotaproveedor_fecven = $result['data']['tb_cuotaproveedor_fecven'];
            $cuenta_id = $result['data']['tb_cuenta_id'];
            $subcuenta_id = $result['data']['tb_subcuenta_id'];
            $tipocuenta=2;
        }
        $result = NULL;
    }
} else {
    $mensaje = $result['mensaje'];
    $bandera = 4;
    $result = NULL;
}

    if($action == 'insertar'){
        $modal_md="modal-dialog modal-lg";
    }
    else{
         $modal_md="modal-dialog";
    }
    
?>
<?php if ($bandera == 1): ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cuotaproveedor" data-keyboard="false" data-backdrop="static">
        <div class="<?php echo $modal_md ?>" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo; ?></h4>
                </div>
                <form id="form_cuotaproveedor" method="post">
                    <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
                    <input type="hidden" name="hdd_cuotaproveedor_id" id="hdd_cuotaproveedor_id" value="<?php echo $cuotaproveedor_id; ?>">

                    <div class="modal-body">
                        
                        
                        <?php require_once 'cuotaproveedor_form_vista.php'; ?>
                        
                        
                        
                        
                        <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                        <?php if ($action == 'eliminar'): ?>
                            <div class="callout callout-warning">
                                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Cuota Proveedor?</h4>
                            </div>
                        <?php endif; ?>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="cuotaproveedor_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M'): ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_cuotaproveedor">Guardar</button>
                            <?php endif; ?>
                            <?php if ($usuario_action == 'E'): ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_cuotaproveedor">Aceptar</button>
                            <?php endif; ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($bandera == 4): ?>
    <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_cuotaproveedor">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title">Mensaje Importante</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <p><?php echo $mensaje; ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/cuotaproveedor/cuotaproveedor_form.js?ver=421421421421'; ?>"></script>
