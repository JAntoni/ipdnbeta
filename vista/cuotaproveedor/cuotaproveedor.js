/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    cuotaproveedor_tabla();

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
//        var startVal = $('#txt_resumen_fec1').val();
//        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
        cuotaproveedor_tabla();
    });
//
    $("#datetimepicker2").on("change", function (e) {
//        var endVal = $('#txt_resumen_fec2').val();
//        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
        cuotaproveedor_tabla();
    });

});

function cuotaproveedor_form(usuario_act, cuotaproveedor_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuotaproveedor/cuotaproveedor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            cuotaproveedor_id: cuotaproveedor_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_cuotaproveedor_form').html(data);
                $('#modal_registro_cuotaproveedor').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_cuotaproveedor'); //funcion encontrada en public/js/generales.js
                modal_width_auto('modal_registro_cuotaproveedor', 50);
                modal_height_auto('modal_registro_cuotaproveedor');
                modal_hidden_bs_modal('modal_registro_cuotaproveedor', 'limpiar'); //funcion encontrada en public/js/generales.js
            } else {
                //llamar al formulario de solicitar permiso
                var modulo = 'cuotaproveedor';
                var div = 'div_modal_cuotaproveedor_form';
                permiso_solicitud(usuario_act, cuotaproveedor_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
//            console.log(data.responseText);
        }
    });
}

function cuotaproveedor_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "cuotaproveedor/cuotaproveedor_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            fec1: $('#txt_resumen_fec1').val(),
            fec2: $('#txt_resumen_fec2').val()
        }),
        beforeSend: function () {
            $('#cuotaproveedor_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_cuotaproveedor_tabla').html(data);
            $('#cuotaproveedor_mensaje_tbl').hide(300);
            estilos_datatable('tbl_cuotaproveedors');
        },
        complete: function (data) {
//            console.log(data);
//            console.log('data');
        },
        error: function (data) {
            $('#cuotaproveedor_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
        }
    });
}



function upload_galeria(egreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'egreso', //nombre de la tabla a relacionar
            modulo_id: egreso_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}


function Abrir_imagen(egreso_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "egreso/egreso_img.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_egreso_img').modal('show');
            modal_hidden_bs_modal('modal_egreso_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(egreso_id);
        },
        complete: function (html) {
//            console.log(html);
        }
    });
}


function egreso_imppos_datos(id_egr)
{
    if (confirm("Desea imprimir?"))
    {
        $.ajax({
            type: "POST",
            url: VISTA_URL + "egreso/egreso_imppos_datos.php",
            async: true,
            dataType: "json",
            data: ({egr_id: id_egr}),
            beforeSend: function () {
                //$('#msj_egreso_imp').html("Imprimiendo.");
                //$('#msj_egreso_imp').show(100);
            },
            success: function (data) {
                // $.each(data, function(i, item) {
                //     console.log(item);
                // });
//                            console.log(data);
                egreso_imppos_ticket(data);
            },
            complete: function (data) {
                console.log(data);
            },
            complete: function (data) {
                console.log(data);
            }
        });
    }
}


function egreso_imppos_ticket(datos) {
//    console.log(datos);
//    return 0;
    $.ajax({
        type: "POST",
        url: "http://127.0.0.1/prestamosdelnorte/app/modulos/egreso/egreso_imppos_ticket.php",
        async: true,
        dataType: "html",
        data: datos,
        beforeSend: function () {
            //$('#msj_egreso_imp').html("Imprimiendo...");
            //$('#msj_egreso_imp').show(100);
        },
        success: function (html) {
            $('#msj_egreso_imp').html(html);
        }
    });
}