<?php
require_once('../../core/usuario_sesion.php');
require_once('Cuotaproveedor.class.php');
$oCuotaproveedor = new Cuotaproveedor();
require_once('Cuotaproveedordetalle.class.php');
$oCuotaproveedordetalle = new Cuotaproveedordetalle();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$cuotaproveedor_id = $_POST['cuotaproveedor_id'];

//echo '$cuotaproveedor_id='.$cuotaproveedor_id;exit();
$result = $oCuotaproveedor->mostrarUno($cuotaproveedor_id);

if ($result['estado'] == 1) {
    $proveedor_id = $result['data']['tb_proveedor_id'];
    $cliente_id = $result['data']['tb_cliente_id'];
    $cuotaproveedor_monto = $result['data']['tb_cuotaproveedor_monto'];
    $moneda_id = $result['data']['tb_cuotaproveedor_nom'];
    $tipcam = $result['data']['tb_tipcam'];
    $nrocuota = $result['data']['tb_nrocuota'];
    $cuotaproveedor_tipo = $result['data']['tb_cuotaproveedor_tipo'];
    $cuotaproveedor_tip = $result['data']['tb_cuotaproveedor_tip'];
    $cuotaproveedor_det = $result['data']['tb_cuotaproveedor_det'];
    $cuotaproveedor_fecven = $result['data']['tb_cuotaproveedor_fecven'];
//    $mon_tot = $dts['data']['tb_ventainterna_mon'];
//    $mon_pag = $dts['data']['tb_ventainterna_monpag'];
}
$result = NULL;

$result2 = $oCuotaproveedordetalle->listar_cuotas_Proveedores($cuotaproveedor_id);
if ($result2['estado'] == 1) {
    foreach ($result2['data']as $key => $value2) {
        $mon_pag += $value2['tb_cuotaproveedordetalle_sub'];
    }
}

$saldo = mostrar_moneda($cuotaproveedor_monto - $mon_pag);

if ($cuotaproveedor_monto > $mon_pag) {
    ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_pagarcuotaproveedor_registro" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" style="font-weight: bold;font-family: cambria">PAGO DE CUOTA DE PROVEEDORES</h4>
                </div>
                <form id="form_pago_cuotaproveedor">
                    <input type="hidden" name="action" value="abonar_interno">
                    <input type="hidden" name="hdd_saldo_pag" value="<?php echo $saldo; ?>">
                    <input type="hidden" name="cuotaproveedor_id" value="<?php echo $cuotaproveedor_id; ?>">
                    <input type="hidden" id="hdd_saldo" name="hdd_saldo" value="<?php echo $cuotaproveedor_monto - $mon_pag; ?>" class="moneda">

                    <div class="modal-body">

                        <div class="col-md-6">
                            <label>EN OFICINAS</label>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Pago hecho en:</label>
                                            <select class="form-control input-sm" name="cbo_pago" id="cbo_pago">
                                                <option value="1">Oficinas</option>
                                                <option value="2">Banco</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Fecha:</label>
                                            <input type="text" name="txt_fecha_pago" value="<?php echo date('d-m-Y'); ?>" readonly class="form-control input-sm">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Monto Total S/.</label>
                                            <input type="text" name="txt_monto_total" value="<?php echo mostrar_moneda($cuotaproveedor_monto) ?>" readonly class="form-control input-sm">
                                        </div>
                                    </div>
                                    <p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Pagos Previos S/.</label>
                                            <input type="text" name="txt_pagos_pre" value="<?php echo mostrar_moneda($mon_pag) ?>" readonly class="form-control input-sm moneda">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Saldo a Pagar S/.</label>
                                            <input type="text" name="txt_saldo_pagar" value="<?php echo ($saldo) ?>" readonly class="form-control input-sm moneda">
                                        </div>
                                    </div>
                                    <p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Monto Pagar S/.</label>
                                            <input type="text" name="txt_monto_pagar" id="txt_monto_pagar" class="form-control input-sm monedas"  value="<?php //echo ($saldo) ?>">
                                        </div>
                                    </div>
                                    <p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Detalle de Cuota</label>
                                            <textarea class="form-control input-sm mayus" name="txt_detalleproveedor" id="txt_detalleproveedor"></textarea>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>         



                        <div class="col-md-6">
                            <label>EN BANCO</label>
                            <div class="box box-primary">
                                <div class="box-header">        
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Fecha Depósito :</label>
                                                <div class='input-group date' id='ingreso_fil_picker1'>
                                                    <input type="text" name="txt_cuopag_fecdep" id="txt_cuopag_fecdep" class="form-control input-sm" readonly value="<?php echo date('d-m-Y'); ?>">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Fecha Validación:</label>
                                                    <div class='input-group date' id='ingreso_fil_picker2'>
                                                        <input type="text" name="txt_cuopag_fec" id="txt_cuopag_fec" class="form-control input-sm" readonly value="<?php echo date('d-m-Y'); ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>N° Cuenta:</label>
                                                <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm" readonly>
                                                    <?php require_once '../cuentadeposito/cuentadeposito_select.php'; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>En Banco:</label>
                                                <select name="cmb_banco_id" id="cmb_banco_id" class="form-control input-sm" readonly>
                                                    <option value="">--</option>
                                                    <option value="1">Banco BCP</option>
                                                    <option value="2">Banco Interbak</option>
                                                    <option value="3" selected="true">Banco BBVA</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>N° Operación:</label>
                                                <input type="text" class="form-control input-sm" id="txt_cuopag_numope" name="txt_cuopag_numope" required="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Monto Depósito:</label>
                                                <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_mon" type="text" id="txt_cuopag_mon"  value="<?php echo ($saldo); ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Comisión Banco:</label>
                                                <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_comi" type="text" id="txt_cuopag_comi" value="0.00" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Monto Validar:</label>
                                                <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda3 form-control input-sm" name="txt_cuopag_monval" type="text" id="txt_cuopag_monval" maxlength="10" readonly   value="<?php echo ($saldo); ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Monto Pagado:</label>
                                                <input style="text-align:right;font-weight: bold;font-size: 15px" class="moneda4 form-control input-sm" name="txt_cuopag_montot" type="text" id="txt_cuopag_montot"   value="<?php echo ($saldo); ?>"  maxlength="10" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <!--                                            </div>
                                                                            </div>-->
                                    <!--</div>-->

                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="ingreso_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <button type="submit" class="btn btn-info" id="btn_guardar_menorgarantia">Guardar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_pagarcuotaproveedor_registro" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" style="font-weight: bold;font-family: cambria">PAGO DE CUOTA DE PROVEEDORES</h4>
                </div>
                <form id="form_pago_cuotaproveedor">

                    <div class="modal-body">
                        <div class="box box-primary">
                            <div class="box-header">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 style="font-family: cambria;font-weight: bold;color: #003eff">YA SE HA REALIZADO EL PAGO TOTAL DE LA CUOTA DEL PROVEEDOR</h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="ingreso_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
}
?>

<script type="text/javascript" src="<?php echo 'vista/cuotaproveedor/pagoCuotaProveedor_form.js?ver=64364678'; ?>"></script>


