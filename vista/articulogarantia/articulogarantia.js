$(document).ready(function () {
    articulogarantia_tabla();
    estilos_datatable();
    set_articuloid();
});

function articulogarantia_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "articulogarantia/articulogarantia_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            garantia_fil: $('#cbo_garantia_articulo').val()
        }),
        beforeSend: function () {
            $('#articulogarantia_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_articulogarantia_tabla').html(data);
            $('#articulogarantia_mensaje_tbl').hide(300);

            $('#tbl_garantia_articulo').find('.selectpicker').each(function () {
                $(this).selectpicker('refresh');
            });
            set_articuloid();
        },
        complete: function () {
            estilos_datatable();
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tbl_garantia_articulo').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            { targets: [3, 4], orderable: false }
        ]
    });
    datatable_texto_filtrar();
}

function datatable_texto_filtrar() {
    $('.dataTables_filter input')
        .attr('id', 'txt_datatable_fil')
        .attr('placeholder', 'escriba para buscar');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
};

function set_articuloid() {
    $('#tbl_garantia_articulo').find('.clase_listo').each(function () {
        let articulo_id = $(this).children('input').val();
        let garantia_id = $(this).children('input')[0].id.split('_')[2];
        $(`#cbo_articulo_garantia_${garantia_id}`).val(articulo_id).change();
        articulo_id = null;
        garantia_id = null;
    });
}

function garantia_asignararticulo(garantia_id) {
    let articulo_id = $(`#cbo_articulo_garantia_${garantia_id} option:selected`).val();

    if (articulo_id <= 0) {
        alerta_warning('Importante', 'Selecciona un artículo para la garantía');
        return false;
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "articulogarantia/articulogarantia_controller.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'asignararticulo',
            garantia_id: garantia_id,
            articulo_id: articulo_id
        }),
        beforeSend: function () {
        },
        success: function (data) {
            if (parseInt(data.estado) > 0) {
                notificacion_success(data.mensaje)

                $('.no_listo_' + garantia_id).css('background-color', 'green');
            }
            else
                alerta_error('ERROR', data.mensaje)
        },
        error: function (data) {
            alerta_error('ERROR', data.responseText);
        }
    });
}

function articulo_form(usuario_act, garantia_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "articulo/articulo_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            garantia_id: garantia_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_articulo_form').html(data);
                $('#modal_registro_articulo').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_articulogarantia'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_articulo', 'limpiar'); //funcion encontrada en public/js/generales.js
                modal_width_auto('modal_registro_articulo', 30);
            }
            else {
                //llamar al formulario de solicitar permiso
                var modulo = 'articulogarantia';
                var div = 'div_modal_articulo_form';
                permiso_solicitud(usuario_act, garantia_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}