
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_articulogarantia_filtro" class="form-inline" role="form">
                    <?php if($_SESSION['usuariogrupo_id'] == 2){ ?>
                        <button type="button" class="btn btn-primary btn-sm" onclick="articulo_form('I',0)"><i class="fa fa-plus"></i> Nueva Artículo</button>
                    <?php } ?>
                    <div class="form-group">
                        <select name="cbo_garantia_articulo" id="cbo_garantia_articulo" onchange="articulogarantia_tabla()" class="form-control input-sm">
                            <option value="1">Garantías sin Artículo</option>
                            <option value="2">Todas las Garantías</option>
                        </select>
                    </div>
                    <!-- <button type="button" class="btn btn-info btn-sm" onclick="cuenta_tabla()"><i class="fa fa-search"></i></button> -->
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12" id="creditomenor_mensaje_opc" style="padding-top: 5px;"> 
    </div>
</div>