<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();

$action = $_POST['action'];
//action  garantia_id  articulo_id

if($action == 'asignararticulo'){
	$garantia_id = intval($_POST['garantia_id']);
	$articulo_id = intval($_POST['articulo_id']);

	$res = $oGarantia->modificar_campo($garantia_id, 'tb_articulo_id', $articulo_id, 'INT');

	if ($res > 0) {
		$data['estado'] = 1;
		$data['mensaje'] = "Se asignó el articulo a la garantía";
	} else {
		$data['estado'] = 0;
		$data['mensaje'] = "Hubo un error al asignar el articulo a la garantía";
	}
}

echo json_encode($data);