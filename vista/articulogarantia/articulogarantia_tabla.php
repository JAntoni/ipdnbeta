<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'articulo/Articulo.class.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../articulo/Articulo.class.php');
}
$oArticulo = new Articulo();

$filtro_garantia = empty($_POST['garantia_fil']) ? 1 : intval($_POST['garantia_fil']);

$result = $oArticulo->listar_garantias_sin_articulo($filtro_garantia);

$tr = '';
$select_articulo = lista_articulos();

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        $articulo_id = $value['tb_articulo_id'];

        $clase_listo = "no_listo_".$value['tb_garantia_id'];
        if ($articulo_id > 0)
            $clase_listo = "clase_listo";

        $tr .= '<tr>';
        $tr .= '
            <td id="tabla_fila">' . $value['tb_credito_id'] . '</td>
            <td id="tabla_fila">' . $value['tb_garantia_id'] . '</td>
            <td id="tabla_fila">' . $value['tb_garantia_pro'] . '</td>
            <td id="tabla_fila" align="center" class="' . $clase_listo . '">
                <input type="hidden" name="hdd_garid_'.$value['tb_garantia_id'].'_articuloid" id="hdd_garid_'.$value['tb_garantia_id'].'_articuloid" value="'.$articulo_id.'">
                <select name="cbo_articulo_garantia_' . $value['tb_garantia_id'] . '" id="cbo_articulo_garantia_' . $value['tb_garantia_id'] . '" class="selectpicker form-control" data-live-search="true" data-max-options="1">
                    ' . $select_articulo . '
                </select>
            </td>
            <td id="tabla_fila" align="center">
                <a class="btn btn-success btn-xs" title="Asignar articulo a garantia" onclick="garantia_asignararticulo('. $value['tb_garantia_id'] .')"><i class="fa fa-save"></i> Guardar</a>
            </td>';
        $tr .= '</tr>';
        $articulo_id = null;
    }
} else {
    echo $result['mensaje'];
}

function lista_articulos(){
    $oArticulo = new Articulo();
    $option = '<option value="0"></option>';
    $res = $oArticulo->listar();

    if ($res['estado'] == 1) {
        foreach ($res['data'] as $key => $value) {
            $option .= '<option value="'. $value['tb_articulo_id'] .'" style="font-weight: bold;">'. $value['tb_articulo_nombre'] .'</option>';
        }
    }
    $res = NULL;
    return $option;
}
$result = null;
?>

<table id="tbl_garantia_articulo" class="table table-hover dataTable display" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" style="width: 8%;">Crédito ID</th>
            <th id="tabla_cabecera_fila" style="width: 8%;">Garantía ID</th>
            <th id="tabla_cabecera_fila" style="width: 60%;">Garantía Nombre</th>
            <th id="tabla_cabecera_fila" style="width: 16%;">Artículo</th>
            <th id="tabla_cabecera_fila" style="width: 8%;">Guardar</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>

<style>
    .clase_listo {
        background-color: green !important;
    }
</style>