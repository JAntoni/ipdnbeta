<?php
require_once('Ubigeo.class.php');
$oUbigeo = new Ubigeo();

//esta variable puede ser definida en un archivo en donde se haga un require_once o un include
$ubigeo_coddep = (empty($ubigeo_coddep))? $_POST['ubigeo_coddep'] : $ubigeo_coddep;
$ubigeo_codpro = (empty($ubigeo_codpro))? $_POST['ubigeo_codpro'] : $ubigeo_codpro;

$option = '<option value="0">Seleccione..</option>';

if(intval($ubigeo_coddep) > 0){
	$result = $oUbigeo->listar_provincias($ubigeo_coddep);
		if($result['estado'] == 1)
		{
		  foreach ($result['data'] as $key => $value)
		  {
		  	$selected = '';
		  	if($ubigeo_codpro == $value['tb_ubigeo_codpro'])
		  		$selected = 'selected';

		    $option .= '<option value="'.$value['tb_ubigeo_codpro'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_ubigeo_nom'].'</option>';
		  }
		}
	$result = NULL;
}

echo $option;
?>