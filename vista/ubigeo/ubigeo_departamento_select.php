<?php
require_once('Ubigeo.class.php');
$oUbigeo = new Ubigeo();

//esta variable puede ser definida en un archivo en donde se haga un require_once o un include
$ubigeo_coddep = (empty($ubigeo_coddep))? $_POST['ubigeo_coddep'] : $ubigeo_coddep;

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oUbigeo->listar_departamentos();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($ubigeo_coddep == $value['tb_ubigeo_coddep'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_ubigeo_coddep'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_ubigeo_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>