<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Ubigeo extends Conexion{

    function insertar($ubigeo_nom, $ubigeo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_ubigeo(tb_ubigeo_xac, tb_ubigeo_nom, tb_ubigeo_des)
          VALUES (1, :ubigeo_nom, :ubigeo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ubigeo_nom", $ubigeo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_des", $ubigeo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($ubigeo_id, $ubigeo_nom, $ubigeo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_ubigeo SET tb_ubigeo_nom =:ubigeo_nom, tb_ubigeo_des =:ubigeo_des WHERE tb_ubigeo_id =:ubigeo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ubigeo_id", $ubigeo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":ubigeo_nom", $ubigeo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_des", $ubigeo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($ubigeo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_ubigeo WHERE tb_ubigeo_id =:ubigeo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ubigeo_id", $ubigeo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($ubigeo_id){
      try {
        $sql = "SELECT * FROM tb_ubigeo WHERE tb_ubigeo_id =:ubigeo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ubigeo_id", $ubigeo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_departamentos(){
      try {
        $sql = "SELECT * FROM tb_ubigeo WHERE tb_ubigeo_tip ='Departamento'";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function listar_provincias($ubigeo_coddep){
      try {
        $sql = "SELECT * FROM tb_ubigeo WHERE tb_ubigeo_tip ='Provincia' AND tb_ubigeo_coddep =:ubigeo_coddep";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ubigeo_coddep", $ubigeo_coddep, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function listar_distritos($ubigeo_coddep, $ubigeo_codpro){
      try {
        $sql = "SELECT * FROM tb_ubigeo WHERE tb_ubigeo_tip ='Distrito' AND tb_ubigeo_coddep =:ubigeo_coddep AND tb_ubigeo_codpro =:ubigeo_codpro";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ubigeo_coddep", $ubigeo_coddep, PDO::PARAM_STR);
        $sentencia->bindParam(":ubigeo_codpro", $ubigeo_codpro, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
    /*ANTHONY */
    function mostrarUbigeo($ubigeo){
        $coddep=substr($ubigeo, 0, 2);
        $codpro=substr($ubigeo, 2, 2);
        $coddis=substr($ubigeo, 4, 2);
      try {
        $sql = "SELECT u3.tb_ubigeo_coddep, u3.tb_ubigeo_nom AS Departamento, u2.tb_ubigeo_codpro, u2.tb_ubigeo_nom AS Provincia, u1.tb_ubigeo_coddis, u1.tb_ubigeo_nom AS Distrito
                FROM tb_ubigeo u1
                INNER JOIN tb_ubigeo u2 ON 
                u1.tb_ubigeo_coddep=u2.tb_ubigeo_coddep AND u1.tb_ubigeo_codpro=u2.tb_ubigeo_codpro AND
                u2.tb_ubigeo_coddis='00'
                INNER JOIN tb_ubigeo u3 ON
                u2.tb_ubigeo_coddep=u3.tb_ubigeo_coddep AND
                u3.tb_ubigeo_codpro = '00' AND
                u3.tb_ubigeo_coddis = '00'
                WHERE u1.tb_ubigeo_coddis =:coddis
                AND u1.tb_ubigeo_codpro=:codpro
                AND u1.tb_ubigeo_coddep=:coddep";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":coddis", $coddis, PDO::PARAM_STR);
        $sentencia->bindParam(":codpro", $codpro, PDO::PARAM_STR);
        $sentencia->bindParam(":coddep", $coddep, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
      /*ANTHONY */
  }

?>
