<?php
  //require_once ("Ubigeo.class.php");
  //$oUbigeo = new Ubigeo();

  //defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
  class ElementoAutocompletar {
    var $value;
    var $label;
    var $ubigeo_cod;

    function __construct($label, $value, $ubigeo_cod){ 
      $this->label = $label;
      $this->value = $value;
      //$this->ubigeo_coddep = $ubigeo_coddep;
      //$this->ubigeo_codpro = $ubigeo_codpro;
      //$this->ubigeo_coddis = $ubigeo_coddis;	
      $this->ubigeo_cod = $ubigeo_cod;
    }
  }

  //recibo el dato que deseo buscar sugerencias
  $datoBuscar = $_GET["term"];
  $datoBuscar = mb_strtoupper($datoBuscar, 'UTF-8');

  $arrayElementos = array();
  array_push(
    $arrayElementos, 
    new ElementoAutocompletar(
      'HOLA ', 
      'hOLA SELEC', 
      '1235454'
    )
  );
  array_push(
    $arrayElementos, 
    new ElementoAutocompletar(
      'HOLA ', 
      'hOLA SELEC', 
      '1235454'
    )
  );
  array_push(
    $arrayElementos, 
    new ElementoAutocompletar(
      'HOLA ', 
      'hOLA SELEC', 
      '1235454'
    )
  );
  print_r(json_encode($arrayElementos));
  /*
  //busco un valor aproximado al dato escrito
  $result = $oUbigeo->ubigeo_autocomplete($datoBuscar);

    //creo el array de los elementos sugeridos
    $arrayElementos = array();

    foreach ($result['data'] as $key => $value) {
      $provincia = $value['provincia'];
      $distrito = $value['distrito'];
      if(trim($provincia) != trim($distrito)){
        array_push(
          $arrayElementos, 
          new ElementoAutocompletar(
            $value["provincia"].' / '.$value["distrito"], 
            $value["provincia"].' / '.$value["distrito"], 
            $value["codigo"]
          )
        );
      }
    }

    print_r(json_encode($arrayElementos));
  $result = NULL;*/
?>