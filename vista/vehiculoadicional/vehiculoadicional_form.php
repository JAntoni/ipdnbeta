<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../vehiculoadicional/Vehiculoadicional.class.php');
  $oVehiculoadicional = new Vehiculoadicional();
  require_once('../funciones/funciones.php');

  $direc = 'vehiculoadicional';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $vehiculoadicional_id = $_POST['vehiculoadicional_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Adicional Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Adicional';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Adicional';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Adicional';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en vehiculoadicional
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'vehiculoadicional'; $modulo_id = $vehiculoadicional_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del vehiculoadicional por su ID
    if(intval($vehiculoadicional_id) > 0){
      $result = $oVehiculoadicional->mostrarUno($vehiculoadicional_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el vehiculoadicional seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $vehiculoadicional_tip = intval($result['data']['tb_vehiculoadicional_tip']);
          $proveedortipo_id =  intval($result['data']['tb_proveedortipo_id']);
          $proveedor_id = intval($result['data']['tb_proveedor_id']);
          $vehiculoadicional_nom = $result['data']['tb_vehiculoadicional_nom'];
          $moneda_id =  intval($result['data']['tb_moneda_id']);
          $vehiculoadicional_pre = $result['data']['tb_vehiculoadicional_pre'];
          $vehiculoadicional_preipdn = $result['data']['tb_vehiculoadicional_preipdn'];
          $vehiculoadicional_uti =  $result['data']['tb_vehiculoadicional_uti'];
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vehiculoadicional" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_vehiculoadicional" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_vehiculoadicional_id" value="<?php echo $vehiculoadicional_id;?>">
          
          <div class="modal-body">
            
            <div class="form-group">
              <label for="cmb_vehiculoadicional_tip" class="control-label">Adicional Para</label>
              <select name="cmb_vehiculoadicional_tip" id="cmb_vehiculoadicional_tip" class="form-control input-sm">
                <option value=""></option>
                <option value="1" <?php if($vehiculoadicional_tip == 1) echo 'selected'?>>Vehículos</option>
                <option value="2" <?php if($vehiculoadicional_tip == 2) echo 'selected'?>>Inmuebles</option>
              </select>
            </div>
            <div class="form-group">
              <label for="cmb_proveedortipo_id" class="control-label">Tipo de Proveedor</label>
              <select name="cmb_proveedortipo_id" id="cmb_proveedortipo_id" class="form-control input-sm">
                <?php require_once('../proveedortipo/proveedortipo_select.php');?>
              </select>
            </div>
            <div class="col-md-10" style="padding-left: 0px;">
              <div class="form-group">
                <label for="cmb_proveedor_id" class="control-label">Proveedor</label>
                <select name="cmb_proveedor_id" id="cmb_proveedor_id" class="selectpicker form-control input-sm" data-live-search="true" data-max-options="1">
                  <?php require_once('../proveedor/proveedor_select.php');?>
                </select>
              </div>
            </div>
            <div class="col-md-2" style="margin-top: 4px;">
              <br>
              <button type="button" class="btn btn-info" onclick="proveedor_form('I', 0)"><i class="fa fa-plus-circle"></i> Nuevo</button>
            </div>
            <div class="form-group">
              <label for="txt_vehiculoadicional_nom" class="control-label">Nombre de Adicional</label>
              <input type="text" name="txt_vehiculoadicional_nom" id="txt_vehiculoadicional_nom" class="form-control input-sm" value="<?php echo $vehiculoadicional_nom;?>">
            </div>
            <div class="form-group">
              <label for="cmb_moneda_id" class="control-label">Tipo Moneda</label>
              <select name="cmb_moneda_id" id="cmb_moneda_id" class="form-control input-sm">
                <?php require_once('../moneda/moneda_select.php');?>
              </select>
            </div>
            <div class="form-group">
              <label for="txt_vehiculoadicional_pre" class="control-label">Precio Cliente</label>
              <input type="text" name="txt_vehiculoadicional_pre" id="txt_vehiculoadicional_pre" class="form-control input-sm" value="<?php echo $vehiculoadicional_pre;?>">
            </div>
            <div class="form-group">
              <label for="txt_vehiculoadicional_preipdn" class="control-label">Precio IPDN</label>
              <input type="text" name="txt_vehiculoadicional_preipdn" id="txt_vehiculoadicional_preipdn" class="form-control input-sm" value="<?php echo $vehiculoadicional_preipdn;?>">
            </div>
            <div class="form-group">
              <label for="txt_vehiculoadicional_uti" class="control-label">Utilidad</label>
              <input type="text" name="txt_vehiculoadicional_uti" id="txt_vehiculoadicional_uti" class="form-control input-sm" value="<?php echo $vehiculoadicional_uti;?>" readonly>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Adicional?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="vehiculoadicional_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_vehiculoadicional">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_vehiculoadicional">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_vehiculoadicional">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/vehiculoadicional/vehiculoadicional_form.js';?>"></script>
