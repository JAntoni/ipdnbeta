<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../vehiculoadicional/Vehiculoadicional.class.php');
  $oVehiculoadicional = new Vehiculoadicional();
  require_once('../funciones/funciones.php');

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$vehiculoadicional_tip = intval($_POST['cmb_vehiculoadicional_tip']);
  	$proveedortipo_id =  intval($_POST['cmb_proveedortipo_id']);
  	$proveedor_id = intval($_POST['cmb_proveedor_id']);
		$vehiculoadicional_nom = $_POST['txt_vehiculoadicional_nom'];
		$moneda_id =  intval($_POST['cmb_moneda_id']);
		$vehiculoadicional_pre = moneda_mysql($_POST['txt_vehiculoadicional_pre']);
		$vehiculoadicional_preipdn = moneda_mysql($_POST['txt_vehiculoadicional_preipdn']);
		$vehiculoadicional_uti =  moneda_mysql($_POST['txt_vehiculoadicional_uti']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar al Adicional.';

 		if($oVehiculoadicional->insertar(
 			$vehiculoadicional_tip, $proveedortipo_id, $proveedor_id,
 			$vehiculoadicional_nom, $moneda_id, $vehiculoadicional_pre,
 			$vehiculoadicional_preipdn, $vehiculoadicional_uti)
 		){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Adicional registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$vehiculoadicional_id = intval($_POST['hdd_vehiculoadicional_id']);
 		$vehiculoadicional_tip = intval($_POST['cmb_vehiculoadicional_tip']);
  	$proveedortipo_id =  intval($_POST['cmb_proveedortipo_id']);
  	$proveedor_id = intval($_POST['cmb_proveedor_id']);
		$vehiculoadicional_nom = $_POST['txt_vehiculoadicional_nom'];
		$moneda_id =  intval($_POST['cmb_moneda_id']);
		$vehiculoadicional_pre = moneda_mysql($_POST['txt_vehiculoadicional_pre']);
		$vehiculoadicional_preipdn = moneda_mysql($_POST['txt_vehiculoadicional_preipdn']);
		$vehiculoadicional_uti =  moneda_mysql($_POST['txt_vehiculoadicional_uti']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar al Adicional.';

 		if($oVehiculoadicional->modificar(
 			$vehiculoadicional_id,$vehiculoadicional_tip, $proveedortipo_id, 
 			$proveedor_id, $vehiculoadicional_nom, $moneda_id, 
 			$vehiculoadicional_pre, $vehiculoadicional_preipdn, $vehiculoadicional_uti)
 		){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Adicional modificado correctamente. '.$vehiculoadicional_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$vehiculoadicional_id = intval($_POST['hdd_vehiculoadicional_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al Adicional.';

 		if($oVehiculoadicional->eliminar($vehiculoadicional_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Adicional eliminado correctamente. '.$vehiculoadicional_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>