<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'vehiculoadicional/vehiculoadicional.class.php');
    require_once(VISTA_URL.'funciones/fechas.php');
    require_once(VISTA_URL.'funciones/funciones.php');
  }
  else{
    require_once('../vehiculoadicional/Vehiculoadicional.class.php');
    require_once('../funciones/fechas.php');
    require_once('../funciones/funciones.php');
  }
  $oVehiculoadicional = new Vehiculoadicional();

?>
<table id="tbl_adicional" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Dirijido</th> 
      <th>Tipo Proveedor</th>
      <th>Proveedor</th>
      <th>RUC/DNI</th>
      <th>Adicional</th>
      <th>Moneda</th>
      <th>Precio Cliente</th>
      <th>Utilidad</th>
      <th>Precio IPDN</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php
    //PRIMER NIVEL
    $result = $oVehiculoadicional->listar_vehiculoadicionales();
      if($result['estado'] == 1){
        foreach ($result['data'] as $key => $value): ?>
          <tr>
            <td><?php echo $value['tb_vehiculoadicional_id'];?></td>
            <td>
              <?php 
                if($value['tb_vehiculoadicional_tip'] == 1)
                  echo '<span class="badge bg-blue">Vehículos</span>';
                elseif($value['tb_vehiculoadicional_tip'] == 2)
                  echo '<span class="badge bg-green">Inmuebles</span>';
                else
                  echo 'SIN TIPO DE ADICIONAL';
              ?>
            </td>
            <td><?php echo $value['tb_proveedortipo_nom'];?></td>
            <td><?php echo $value['tb_proveedor_nom']; ?></td>
            <td><?php echo $value['tb_proveedor_doc']; ?></td>
            <td><?php echo $value['tb_vehiculoadicional_nom']; ?></td>
            <td><?php echo $value['tb_moneda_nom']; ?></td>
            <td><?php echo mostrar_moneda($value['tb_vehiculoadicional_pre']); ?></td>
            <td><?php echo mostrar_moneda($value['tb_vehiculoadicional_uti']); ?></td>
            <td><?php echo mostrar_moneda($value['tb_vehiculoadicional_preipdn']); ?></td>
            <td align="center">
              <a class="btn btn-warning btn-xs" title="Editar" onclick="vehiculoadicional_form(<?php echo "'M', ".$value['tb_vehiculoadicional_id'];?>)"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="vehiculoadicional_form(<?php echo "'E', ".$value['tb_vehiculoadicional_id'];?>)"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php
        endforeach;
      }
    $result = NULL;
    ?>
  </tbody>
</table>
