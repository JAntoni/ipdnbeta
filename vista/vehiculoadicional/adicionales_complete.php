<?php

require_once ("Vehiculoadicional.class.php");
$oVehiculoadicional = new Vehiculoadicional();

//defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
class ElementoAutocompletar {

    var $id;
    var $label;
    var $value;
    var $precio_cli;
    var $precio_ipdn;
    var $utilidad;
    var $prov_nom;
    var $moneda_id;

    function __construct($id, $label, $value, $precio_cli, $precio_ipdn, $utilidad, $prov_nom, $moneda_id) {
        $this->id = $id;
        $this->label = $label;
        $this->value = $value;
        $this->precio_cli = $precio_cli;
        $this->precio_ipdn = $precio_ipdn;
        $this->utilidad = $utilidad;
        $this->prov_nom = $prov_nom;
        $this->moneda_id = $moneda_id;
    }

}

//recibo el dato que deseo buscar sugerencias
$datoBuscar = $_GET["term"];

//busco un valor aproximado al dato escrito
$rs = $oVehiculoadicional->complete_nom($datoBuscar);

//creo el array de los elementos sugeridos
$arrayElementos = array();

if ($rs['estado'] == 1) {
//bucle para meter todas las sugerencias de autocompletar en el array
    foreach ($rs['data']as $key => $fila) {
        $mostrar = $fila["tb_proveedortipo_nom"] . ' - ' . $fila["tb_proveedor_nom"] . ' - ' . $fila["tb_moneda_nom"] . ' ' . $fila["tb_vehiculoadicional_pre"];
        
        array_push(
                $arrayElementos,
                new ElementoAutocompletar(
                        $fila["tb_vehiculoadicional_id"],
                        $mostrar, 
                        $mostrar, $fila["tb_vehiculoadicional_pre"], 
                        $fila["tb_vehiculoadicional_preipdn"], 
                        $fila["tb_vehiculoadicional_uti"], 
                        $fila["tb_proveedortipo_nom"], 
                        $fila["tb_moneda_id"]));
    }
}
print_r(json_encode($arrayElementos));
?>