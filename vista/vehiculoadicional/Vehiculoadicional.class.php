<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Vehiculoadicional extends Conexion{

    function insertar($vehiculoadicional_tip, $proveedortipo_id, $proveedor_id, $vehiculoadicional_nom, $moneda_id, $vehiculoadicional_pre, $vehiculoadicional_preipdn, $vehiculoadicional_uti){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_vehiculoadicional(tb_vehiculoadicional_xac, tb_vehiculoadicional_tip, tb_proveedortipo_id, tb_proveedor_id, tb_vehiculoadicional_nom, tb_moneda_id, tb_vehiculoadicional_pre, tb_vehiculoadicional_preipdn, tb_vehiculoadicional_uti)
          VALUES (1, :vehiculoadicional_tip, :proveedortipo_id, :proveedor_id, :vehiculoadicional_nom, :moneda_id, :vehiculoadicional_pre, :vehiculoadicional_preipdn, :vehiculoadicional_uti)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoadicional_tip", $vehiculoadicional_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":proveedortipo_id", $proveedortipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoadicional_nom", $vehiculoadicional_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoadicional_pre", $vehiculoadicional_pre, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoadicional_preipdn", $vehiculoadicional_preipdn, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoadicional_uti", $vehiculoadicional_uti, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($vehiculoadicional_id, $vehiculoadicional_tip, $proveedortipo_id, $proveedor_id, $vehiculoadicional_nom, $moneda_id, $vehiculoadicional_pre, $vehiculoadicional_preipdn, $vehiculoadicional_uti){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_vehiculoadicional SET tb_vehiculoadicional_tip =:vehiculoadicional_tip, tb_proveedortipo_id =:proveedortipo_id, tb_proveedor_id =:proveedor_id, tb_vehiculoadicional_nom =:vehiculoadicional_nom, tb_moneda_id =:moneda_id, tb_vehiculoadicional_pre =:vehiculoadicional_pre, tb_vehiculoadicional_preipdn =:vehiculoadicional_preipdn, tb_vehiculoadicional_uti =:vehiculoadicional_uti WHERE tb_vehiculoadicional_id =:vehiculoadicional_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoadicional_id", $vehiculoadicional_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoadicional_tip", $vehiculoadicional_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":proveedortipo_id", $proveedortipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoadicional_nom", $vehiculoadicional_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":moneda_id", $moneda_id, PDO::PARAM_INT);
        $sentencia->bindParam(":vehiculoadicional_pre", $vehiculoadicional_pre, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoadicional_preipdn", $vehiculoadicional_preipdn, PDO::PARAM_STR);
        $sentencia->bindParam(":vehiculoadicional_uti", $vehiculoadicional_uti, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($vehiculoadicional_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_vehiculoadicional WHERE tb_vehiculoadicional_id =:vehiculoadicional_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoadicional_id", $vehiculoadicional_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($vehiculoadicional_id){
      try {
        $sql = "SELECT * FROM tb_vehiculoadicional WHERE tb_vehiculoadicional_id =:vehiculoadicional_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculoadicional_id", $vehiculoadicional_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculoadicionales(){
      try {
        $sql = "SELECT * FROM tb_vehiculoadicional ve 
          INNER JOIN tb_proveedortipo protip on protip.tb_proveedortipo_id = ve.tb_proveedortipo_id 
          INNER JOIN tb_proveedor pro on pro.tb_proveedor_id = ve.tb_proveedor_id
          INNER JOIN tb_moneda mon on mon.tb_moneda_id = ve.tb_moneda_id order by tb_vehiculoadicional_id desc";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
        /*CODIGO AGREGADO POR DESARROLLADOR CRISTHIAN JOSSIMAR GALVEZ AGUILAR*/
    
    
    function mostrarTodos(){
      try {
        $sql="SELECT 
                    * 
                FROM 
                    tb_vehiculoadicional c 
                    inner join tb_proveedor a on c.tb_proveedor_id = a.tb_proveedor_id
                    inner join tb_proveedortipo b on c.tb_proveedortipo_id= b.tb_proveedortipo_id
                    inner join tb_moneda m on c.tb_moneda_id=m.tb_moneda_id
                WHERE 
                    tb_vehiculoadicional_xac=1
                ORDER BY 
                    tb_vehiculoadicional_nom";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
    
    

    
    function complete_nom($term){
      try {

        $filtro = "%".$term."%";

       $sql = "SELECT 
                     * 
               FROM 
                    tb_vehiculoadicional c 
                    inner join tb_proveedor a on c.tb_proveedor_id = a.tb_proveedor_id 
                    inner join tb_proveedortipo b on c.tb_proveedortipo_id= b.tb_proveedortipo_id 
                    inner join tb_moneda m on c.tb_moneda_id=m.tb_moneda_id 
                WHERE 
                    tb_vehiculoadicional_xac=1 and CONCAT(tb_proveedortipo_nom, ' ',tb_proveedor_nom, ' ', tb_vehiculoadicional_nom) like :filtro limit 7";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No se encuentran resultados para la busqueda seleccionada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    //ANTHONY
        function filtrar($protip_id,$mon_id){
      try {
        $sql = "SELECT * FROM tb_vehiculoadicional a
		INNER JOIN tb_proveedor p ON a.tb_proveedor_id=p.tb_proveedor_id
		INNER JOIN tb_moneda m ON a.tb_moneda_id=m.tb_moneda_id
		WHERE tb_vehiculoadicional_xac=1
                AND tb_proveedortipo_id=:protip_id
                AND a.tb_moneda_id=:mon_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":protip_id", $protip_id, PDO::PARAM_INT);
        $sentencia->bindParam(":mon_id", $mon_id, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
    //ANTHONY
    function mostrarProveedor($adicional_id){
      try {
        $sql = "SELECT * FROM tb_vehiculoadicional a
		INNER JOIN tb_proveedor p ON a.tb_proveedor_id=p.tb_proveedor_id
		WHERE tb_vehiculoadicional_id=:adicional_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":adicional_id", $adicional_id, PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO SE ENCUENTRA REGISTRO";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }

  }

?>
