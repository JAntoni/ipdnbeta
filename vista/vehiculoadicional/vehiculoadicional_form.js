function proveedor_selectpicker(){
	$('#cmb_proveedor_id').selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: 'ES'
  });
  $('#cmb_proveedor_id').selectpicker('refresh');
}
function proveedor_select(){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"proveedor/proveedor_select.php",
		async: true,
		dataType: "html",
		data: ({}),
		beforeSend: function() {
		},
		success: function(data){
			$('#cmb_proveedor_id').selectpicker('destroy');
			$('#cmb_proveedor_id').html(data);
			proveedor_selectpicker();
		},
		complete: function(data){
			//console.log('ajax completado');
		}
	});
}

$(document).ready(function(){
	
	$('#txt_vehiculoadicional_pre, #txt_vehiculoadicional_preipdn, #txt_vehiculoadicional_uti').autoNumeric({
		aSep: ',',
		aDec: '.',
		//aSign: 'S/. ',
		//pSign: 's',
		vMin: '0.00',
		vMax: '999999.00'
	});

	proveedor_selectpicker();

  $('#txt_vehiculoadicional_pre, #txt_vehiculoadicional_preipdn').change(function(event) {
  	var adicional_pre = Number($('#txt_vehiculoadicional_pre').autoNumeric('get'));
  	var adicional_preipdn = Number($('#txt_vehiculoadicional_preipdn').autoNumeric('get'));

  	var utilidad = 0;
  	utilidad = adicional_pre - adicional_preipdn;
  	$('#txt_vehiculoadicional_uti').autoNumeric('set', utilidad.toFixed(2));
  });

console.log('cambio 888');
  $('#form_vehiculoadicional').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"vehiculoadicional/vehiculoadicional_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_vehiculoadicional").serialize(),
				beforeSend: function() {
					$('#vehiculoadicional_mensaje').show(400);
					$('#btn_guardar_vehiculoadicional').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#vehiculoadicional_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#vehiculoadicional_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		vehiculoadicional_tabla();
		      		$('#modal_registro_vehiculoadicional').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#vehiculoadicional_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#vehiculoadicional_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_vehiculoadicional').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#vehiculoadicional_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#vehiculoadicional_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
	  	cmb_vehiculoadicional_tip: {
	  		required: true
	  	},
	  	cmb_proveedortipo_id: {
	  		min: 1
	  	},
	  	cmb_proveedor_id: {
	  		min: 1
	  	},
			txt_vehiculoadicional_nom: {
				required: true,
				minlength: 2
			},
			cmb_moneda_id: {
				min: 1
			},
			txt_vehiculoadicional_pre: {
				required: true
			},
			txt_vehiculoadicional_preipdn: {
				required: true
			},
			txt_vehiculoadicional_uti: {
				required: true
			}
		},
		messages: {
			cmb_vehiculoadicional_tip: {
				required: "Elija a que tipo va dirijido el Adicional"
			},
			cmb_proveedortipo_id: {
				min: "Elija el Tipo de Proveedor"
			},
			cmb_proveedor_id: {
	  		min: "Elija al Proveedor"
	  	},
			txt_vehiculoadicional_nom: {
				required: "Ingrese un nombre para el Adicional",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			cmb_moneda_id: {
	  		min: "Elija el Tipo de Moneda"
	  	},
	  	txt_vehiculoadicional_pre: {
				required: "Ingrese el precio de venta al Cliente"
			},
			txt_vehiculoadicional_preipdn: {
				required: "Ingrese el precio para IPDN"
			},
			txt_vehiculoadicional_uti: {
				required: "La Utilidad no puede estar vacía"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
