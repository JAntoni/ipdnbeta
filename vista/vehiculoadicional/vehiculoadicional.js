function vehiculoadicional_form(usuario_act, vehiculoadicional_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"vehiculoadicional/vehiculoadicional_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vehiculoadicional_id: vehiculoadicional_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_vehiculoadicional_form').html(data);
      	$('#modal_registro_vehiculoadicional').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_vehiculoadicional'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_vehiculoadicional', 'limpiar'); //funcion encontrada en public/js/generales.js
        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto('modal_registro_vehiculoadicional'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'vehiculoadicional';
      	var div = 'div_modal_vehiculoadicional_form';
      	permiso_solicitud(usuario_act, vehiculoadicional_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}
function vehiculoadicional_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"vehiculoadicional/vehiculoadicional_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#vehiculoadicional_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_vehiculoadicional_tabla').html(data);
      $('#vehiculoadicional_mensaje_tbl').hide(300);
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#vehiculoadicional_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function proveedor_form(usuario_act, proveedor_id){ 
  $.ajax({
    type: "POST",
    url: VISTA_URL+"proveedor/proveedor_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      proveedor_id: proveedor_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_proveedor_form').html(data);

        $('#modal_registro_proveedor').modal('show').on('hidden.bs.modal', function() {
          proveedor_select();
          $('#div_modal_proveedor_form').html('');
        });
      }
      else{
        //llamar al formulario de solicitar permiso
        var modulo = 'proveedor';
        var div = 'div_modal_proveedor_form';
        permiso_solicitud(usuario_act, proveedor_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}
$(document).ready(function() {
  console.log('loader de cvehiculoadiciona444');
  vehiculoadicional_tabla();
});