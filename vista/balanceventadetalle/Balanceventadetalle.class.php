<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Balanceventadetalle extends Conexion {

    public $balanceventa_detalle_id;
    public $balanceventa_id;
    public $credito_id;
    public $balanceventa_detalle_colocacion;
    public $balanceventa_detalle_pagos;
    public $balanceventa_detalle_fec;
    public $balanceventa_detalle_usureg;
    public $balanceventa_detalle_usumod;
    public $balanceventa_detalle_xac;

    public function insertar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_balanceventa_detalle (
                tb_balanceventa_id,
                tb_credito_id,
                tb_balanceventa_detalle_colocacion,
                tb_balanceventa_detalle_pagos,
                tb_balanceventa_detalle_fec,
                tb_balanceventa_detalle_usureg,
                tb_balanceventa_detalle_xac)
              VALUES (
                :balanceventa_id,
                :credito_id,
                :balanceventa_detalle_colocacion,
                :balanceventa_detalle_pagos,
                :balanceventa_detalle_fec,
                :balanceventa_detalle_usureg,
                :balanceventa_detalle_xac);";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":balanceventa_id", $this->balanceventa_id, PDO::PARAM_INT);
            $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":balanceventa_detalle_colocacion", $this->balanceventa_detalle_colocacion, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_detalle_pagos", $this->balanceventa_detalle_pagos, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_detalle_fec", $this->balanceventa_detalle_fec, PDO::PARAM_STR);
            $sentencia->bindParam(":balanceventa_detalle_usureg", $this->balanceventa_detalle_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":balanceventa_detalle_xac", $this->balanceventa_detalle_xac, PDO::PARAM_INT);

            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Balanceventa_detalle registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function listar_balancedetalle_balance($balanceventa_id){
        try {
            $sql = "SELECT * FROM tb_balanceventa_detalle WHERE tb_balanceventa_id =:balanceventa_id AND tb_balanceventa_detalle_xac = 1";
    
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":balanceventa_id", $balanceventa_id, PDO::PARAM_INT);
            $sentencia->execute();
    
            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay resultados";
              $retorno["data"] = "";
            }
    
            return $retorno;
        } catch (Exception $e) {
        throw $e;
        }
    }

}