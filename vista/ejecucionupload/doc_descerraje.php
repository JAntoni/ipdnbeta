<?php
//require y use
    require_once '../../vendor/autoload.php';
    use PhpOffice\PhpWord\PhpWord;
    use PhpOffice\PhpWord\IOFactory;
    use PhpOffice\PhpWord\Shared\Html;
    use PhpOffice\PhpWord\Style\Font;
    use PhpOffice\PhpWord\Style\Paragraph;
    require_once '../funciones/funciones.php';
    require_once '../funciones/fechas.php';
    require_once '../ejecucion/Ejecucion.class.php';
    $oEjecucion = new Ejecucion();
    require_once '../creditogarveh/Creditogarveh.class.php';
    $oCredito = new Creditogarveh();
    require_once '../escriturapublica/Escriturapublica.class.php';
    $oEscritura = new Escriturapublica();
    require_once '../ejecucionupload/Ejecucionfasefile.class.php';
    $oEjecucionfasefile = new Ejecucionfasefile();
    require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
    $oCertificado = new Certificadoregistralveh();
    require_once '../cargo/Cargo.class.php';
    $oCargo = new Cargo();
    require_once '../persona/Persona.class.php';
    $oPersona = new Persona();
//

//recibir datos GET
    $ejecucion_id = intval($_GET['d1']);
    $ejecucionfase_id = intval($_GET['d2']);
    $tipo_documento = $_GET['d3']; //solo puede ser WORD Y PDF
//

//validar formato de documento y que la ejecucion exista
    if (empty($tipo_documento) || !in_array($tipo_documento, array('pdf', 'word'))) {
        echo 'NO HAS SELECCIONADO EL TIPO DOCUMENTO. MODIFICA EL FINAL DE LA URL SI SERÁ WORD O PDF';
        exit();
    } elseif ($tipo_documento == 'pdf'){
        require_once '../../static/tcpdf/tcpdf.php';
    }

    $ejecucion_reg = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '');
    if ($ejecucion_reg['estado'] != 1) {
        unset($ejecucion_reg);
        echo 'ERROR EN EL ID DE EJECUCION. NO HUBO RESULTADO PARA LA CONSULTA';
        exit();
    } else {
        $ejecucion_reg = $ejecucion_reg['data'][0];
    }
//

//INFO DEL CASO
    $exped = $ejecucion_reg['tb_ejecucion_expediente'];
    $esp_nom = $ejecucion_reg['tb_ejecucion_especialista'];
    $num_juzgado = ['', '3', '8'];

    //buscar el ULTIMO ESCRITO, para sumar 1 al escrito que toca
        $escritos_anter = $oEjecucionfasefile->listar('', '', $ejecucion_id, 'not null', 2, '', '', '');
        $num_escrito = str_pad(($escritos_anter['cantidad'] + 2), 2, '0', STR_PAD_LEFT);
    //

    //SUMILLA. si el representante es manuel vargas, lo apersonamos
        $txt_apersonar = '';
        $posterior = ' posteriormente';
        $rep_ipdn_id = $ejecucion_reg['tb_persona_id'];//1 Manuel, 2 Herbert Engel
        if ($rep_ipdn_id == 1) {
            $rep_ipdn_id = 2;
            $txt_apersonar = ' apersonamos a nuestro representante, y posteriormente';
            $posterior = '';
        }

        $sumilla = "Solicitamos se ordene descerraje, se curse oficio correspondiente,$txt_apersonar se realice la extracción y$posterior se haga entrega del bien mueble a nuestro representante.";
    //

    //buscar RESOLUCION tipo ADMISIBLE, que su numero no sea vacio, suma 1 y es el num res que toca
        $oEjecucionfasefile->num = 'not null';
        $resoluc_admis = $oEjecucionfasefile->listar('', '', $ejecucion_id, 'not null', 1, 1, '', '');
        if ($resoluc_admis['estado'] == 1) {
            $num_resol_admis = numero_letra($resoluc_admis['data'][0]['tb_ejecucionfasefile_num']);
            $fec_resol_admis = '<b>[fecha resolucion]</b>';
        }
    //
//

//DATOS CREDITO, EJECUCION, VEHICULO y ESCRITURA PUBLICA
    $credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];
    $credito_datos = $oCredito->mostrarUno($ejecucion_reg['tb_credito_id'])['data'];
    $res_escritura = $oEscritura->listar_todos('', $credito_getdocs);
    $res_certificado = $oCertificado->listar_todos('', '', $ejecucion_reg['tb_ejecucion_id'], 1);

    $tipo_cgv = strtolower(str_replace('Ó', 'ó', $ejecucion_reg['cgarvtipo_nombre2']));

    $carroceria = $credito_datos['tb_vehiculoclase_nom'];
    $marca      = $credito_datos['tb_vehiculomarca_nom'];
    $modelo     = $credito_datos['tb_vehiculomodelo_nom'];
    $anio       = $credito_datos['tb_credito_vehano'];
    $nro_serie  = strtoupper(trim($credito_datos['tb_credito_vehsercha']));
    $nro_motor  = strtoupper(trim($credito_datos['tb_credito_vehsermot']));
    $placa      = strtoupper(trim($credito_datos['tb_credito_vehpla']));
    $placa      = str_replace("-", "", $placa);

    $color = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    $combustible = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    //estos datos se deben obtener del certificado registral vehicular
    if ($res_certificado['estado'] == 1) {
        $res_certificado = $res_certificado['data'][0];
        $color          = strtoupper(trim($res_certificado['tb_certificadoregveh_color']));
        $combustible    = strtoupper(trim($res_certificado['tb_certificadoregveh_comb']));
    }
    unset($res_certificado);

    //NRO SERIE EN LETRAS
        $arr_serie_chasis = str_split($nro_serie);
        $serie_chasis_letras = '';
        foreach ($arr_serie_chasis as $key => $value) {
            if (is_numeric($value)) {
                $serie_chasis_letras .= numero_letra($value) . ', '; //en formato.php
            } else {
                switch ($value) {
                    case ".":
                        $serie_chasis_letras .= 'PUNTO, ';
                        break;
                    case "-":
                        $serie_chasis_letras .= 'GUIÓN, ';
                        break;
                    case "*":
                        $serie_chasis_letras .= 'ASTERÍSCO, ';
                        break;
                    case "@":
                        $serie_chasis_letras .= 'ARROBA, ';
                        break;
                    case " ":
                        $serie_chasis_letras .= 'ESPACIO, ';
                        break;
                    default:
                        $serie_chasis_letras .= $value . ', ';
                }
            }
        }
        $serie_chasis_letras = substr($serie_chasis_letras, 0, -2);
    //

    //NRO MOTOR LETRAS
        $arr_nro_motor = str_split($nro_motor);
        $nro_motor_letras = '';
        foreach ($arr_nro_motor as $key => $value) {
            if(is_numeric($value)){
                $nro_motor_letras .= numero_letra($value).', '; //en formato.php
            }
            else{
                switch ($value) {
                case ".":
                    $nro_motor_letras .= 'PUNTO, ';
                    break;
                case "-":
                    $nro_motor_letras .= 'GUIÓN, ';
                    break;
                case "*":
                    $nro_motor_letras .= 'ASTERÍSCO, ';
                    break;
                case "@":
                    $nro_motor_letras .= 'ARROBA, ';
                    break;
                case " ":
                    $nro_motor_letras .= 'ESPACIO, ';
                    break;
                default:
                    $nro_motor_letras .= $value.', ';
                }
            }
        }
        $nro_motor_letras = substr($nro_motor_letras, 0, -2);
    //

    $ep_fecha = fecha_mysql($res_escritura['data'][0]['tb_escriturapublica_fecha']);
//

//CLIENTE INFO
    $cliente_nom = $cliente_repre_nom = '';
    if ($ejecucion_reg['tb_cliente_tip'] == 1) {
        $cliente_nom = $ejecucion_reg['tb_cliente_nom'];

        $cliente_info2 = "<b>$cliente_nom</b>";

        //buscar asociaciones
            $cliente_ids = explode(',', $res_escritura['data'][0]['tb_cliente_ids']);
            if (count($cliente_ids) > 1) {
                $lleva_coma = count($cliente_ids) == 2 ? '' : ',';
                //buscar nom cliente asociado
                require_once '../cliente/Cliente.class.php';
                $oCliente = new Cliente();

                for ($i=1; $i < count($cliente_ids); $i++) {
                    $cli_asoc_id = $cliente_ids[$i];

                    $res_cliente_asoc = $oCliente->mostrarUno($cli_asoc_id);
                    if ($res_cliente_asoc['estado'] == 1) {
                        $tipo_sociedad = $oCliente->verificar_sociedad_cliente($cliente_ids[0], $cliente_ids[$i])['data']['tb_clientedetalle_tip'];
                        if ($tipo_sociedad == 1) {
                            $tipo_sociedad_nom[$i] = 'su cónyugue';
                        } elseif ($tipo_sociedad == 2) {
                            $tipo_sociedad_nom[$i] = 'el copropietario';
                        }
                        $enlace_ult_cli_asoc2 = $i+1 == count($cliente_ids) ? "$lleva_coma y" : "$lleva_coma";

                        $cliente_info2 .= "$enlace_ult_cli_asoc2 {$tipo_sociedad_nom[$i]} <b>{$res_cliente_asoc['data']['tb_cliente_nom']}</b>";
                    }
                    unset($res_cliente_asoc);
                }
            }
        //
    } else {
        $cliente_nom = $ejecucion_reg['tb_cliente_emprs'];
        $cliente_repre_nom = $ejecucion_reg['tb_cliente_nom'];
        $cliente_cargo_emprs = $credito_datos['tb_cliente_empger'];

        $cliente_info2 = "<b>$cliente_nom</b>, debidamente representada por su ".customUcwords($cliente_cargo_emprs)." <b>$cliente_repre_nom</b>";
    }
//

//datos del que irá a descerraje (Engel)
    $rep_ipdn = $oPersona->mostrar_uno_id($rep_ipdn_id)['data'];
    $rep_ipdn_nom = $rep_ipdn['tb_persona_nom'].' '.$rep_ipdn['tb_persona_ape'];
    $rep_ipdn_doc = $rep_ipdn['tb_persona_doc'];
    $rep_ipdn_cargo_nom = 'Sub Gerente General';
//

//parrafo de apersonar
    $parrafo_3 = 'Asimismo, preciso que, para la diligencia solicitada, participará nuestro '.$rep_ipdn_cargo_nom.', '.customUcwords($rep_ipdn_nom).
        ', debidamente identificado con DNI N° '.$rep_ipdn_doc.', ';
    //
    if ($ejecucion_reg['tb_persona_id'] == 1) {
        $parrafo_3 = 'Asimismo, preciso que, para la diligencia solicitada, apersonamos a nuestro '.$rep_ipdn_cargo_nom.', '.customUcwords($rep_ipdn_nom).
            ', debidamente identificado con DNI N° '.$rep_ipdn_doc.', según poderes inscritos en la partida Nº 11218951 del registro de personas jurídicas de '.
            'la Oficina Registral de Chiclayo, a fin de que pueda estar presente al momento del descerraje; ';
    }
    $parrafo_3 .= 'y luego de realizada la extraccion del bien mueble, se le haga la entrega del vehículo de placa: <b>'.$placa.'</b>.';
//

$title = 'DOC_DESCERRAJE_'.str_replace(' ', '', $cliente_nom).''. date('YmdHis');
$fec_hoy = fechaActual(date('Y-m-d'));

//TCPDF
if ($tipo_documento == 'pdf') {
    class MYPDF extends TCPDF {
        public function Header() {
            //vacio pq no necesita header ni footer
        }
        public function Footer() {
            //vacio pq no necesita header ni footer
        }
    }
    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.prestamosdelnortechiclayo.com');
    $pdf->SetSubject('www.prestamosdelnortechiclayo.com');
    $pdf->SetKeywords('www.prestamosdelnortechiclayo.com');
    $pdf->SetTitle($title);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    //set margins
    $pdf->SetMargins(23, 26, 25); // left top right
    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // add a page
    $pdf->AddPage('P', 'A4');
}

$html =
'<table>'.
    //EXPEDIENTE, ESCPECIALISTA, SUMILLA
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Expediente N°</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">'.$exped.'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Esp. legal</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">'.customUcwords($esp_nom).'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Cuaderno</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">Principal</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Escrito N°</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">'.$num_escrito.'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Sumilla</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: justify;">'.$sumilla.'</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PRESENTACION DE EMPRESA Y CLIENTE
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify; font-size: 34px; line-height: 1.6;"><b>SEÑOR MAGISTRADO DEL '.$num_juzgado[$ejecucion_reg['tb_ejecucion_juzgado']].'° JUZGADO CIVIL SUB ESPECIALIDAD COMERCIAL DE CHICLAYO</b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:62%; text-align: justify; font-size: 33px; line-height: 1.6;">'.
                '<b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b>, en los seguidos contra '. $cliente_info2.', sobre INCAUTACIÓN DE BIEN MUEBLE, a Ud. con respeto digo:'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO I
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 29px; line-height: 1.6;">'.
                'Que, mediante Resolucion N° '.$num_resol_admis.' del '.$fec_resol_admis.', se admitió a trámite nuestro requerimiento de incautación '.
                'sobre el vehículo de placa: <b>'.$placa.'</b>, disponiéndose la orden de ubicación e incautación a nivel nacional; sin embargo, del seguimiento '.
                'realizado a través de la plataforma GPS −sistema de posicionamiento global− que permanece vigente, conforme a lo establecido en la Escritura '.
                'Pública de '.ucwords($tipo_cgv).' de Garantía Mobiliaria de fecha '.strtolower(fechaActual($ep_fecha)).', tomamos conocimiento que el bien mueble '.
                'se encuentra estacionado en <b>[ubicacion/lugar]</b>'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO II
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 29px; line-height: 1.6;">'.
                'Al respecto, por medio de este escrito solicitamos se ordene el descerraje para ingresar a <b>[ubicacion/lugar]</b>, dirección ubicada a través del GPS, <b>[cuyo reporte...]</b>'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO III
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 29px; line-height: 1.6;">'.
                $parrafo_3.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO IV
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 29px; line-height: 1.6;">'.
                'Por lo expuesto:'.
            '</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 29px;">'.
                'Pido a Usted, Señor Juez, ordenar el descerraje de <b>[ubicacion/lugar]</b>, con la finalidad que se logre recuperar el '.
                'bien mueble de placa <b>'.$placa.'</b>, carrocería: <b>'.$carroceria.'</b>, marca: <b>'.$marca.'</b>, modelo: <b>'.$modelo.'</b>, '.
                'año de modelo: <b>'.$anio.'</b>, color: <b>'.$color.'</b>, N° de serie: <b>'.$nro_serie.' ('.$serie_chasis_letras.')</b>, N° de motor: '.
                '<b>'.$nro_motor.' ('.$nro_motor_letras.')</b>, combustible: <b>'.$combustible.'</b>; realizándose la extracción del mismo y posterior '.
                'entrega a nuestro representante. Así como se remita el Oficio correspondiente a la Policía Nacional del Perú, para que presten las garantías '.
                'necesarias en el momento de la realización de la diligencia; debiéndose de entregar el oficio a la parte demandante para que coadyuve con tal finalidad.'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO V
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 29px; line-height: 1.6;">'.
                'Otrosí digo: Que, adjunto arancel judicial por derecho de notificación judicial y por diligencias a realizarse fuera del local del juzgado.'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //FINAL
        '<tr>'.
            '<td style="width:62%; text-align: left;"></td>'.
            '<td style="width:38%;  font-size: 29px;">Chiclayo, '.$fec_hoy.'.</td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:32%; text-align: left;"></td>'.
            '<td style="width:68%;"><img src="../../public/pdf/ejecucion_varios/firma_obiol.jpg" width="230" height="160"></td>'.
        '</tr>'.
    //FINAL
'</table>'
;

if ($tipo_documento == 'word') {
    // Crear una instancia de PHPWord
    $phpWord = new PhpWord();

    $phpWord->setDefaultFontName('Arial');
    $phpWord->setDefaultFontSize(10);

    // Definir estilo de párrafo
    $paragraphStyle = array('spaceAfter' => 0, 'spacing' => 0);

    // Crear una nueva sección
    $section1 = $phpWord->addSection();

    // Renderizar el contenido HTML 1
    Html::addHtml($section1, $html, false, false, $paragraphStyle);

    // Guardar el documento
    $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save('doc_descerraje.docx');
    echo 'Documento para cliente: ' . $cliente_nom . ', generado correctamente...';
    header('Location: ../ejecucionupload/doc_descerraje.docx');
}

if ($tipo_documento == 'pdf') {
    $pdf->SetFont('Arial', '', 10);
    $pdf->writeHTML($html, true, 0, true, true);
    $pdf->Ln();
    // reset pointer to the last page
    $pdf->lastPage();
    //Close and output PDF document
    $nombre_archivo = $title . ".pdf";
    $pdf->Output($nombre_archivo, 'I');
}
