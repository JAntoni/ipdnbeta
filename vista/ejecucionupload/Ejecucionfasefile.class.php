<?php
if (defined('APP_URL'))
    require_once APP_URL . 'datos/conexion.php';
else
    require_once '../../datos/conexion.php';
//

class Ejecucionfasefile extends Conexion {
    public $id;
    public $archivonom;
    public $fecnoti;
    public $ejecucionfase_id;
    public $upload_id;
    public $fecreg;
    public $xac;
    public $tipodoc;
    public $resol_est;
    public $escrito_tipo;
    public $genera_gasto;
    public $gastopago_id;
    public $acta_tipo;
    public $num;
    public $oficiocargo_tipo;
    public $cargo_fecingreso;
    public $incluir_redac;

    public function listar($ejecucionfasefile_id, $ejecucionfase_id, $ejecucion_id, $upload_id, $tipo_file, $resol_est, $escrito_tipo, $acta_tipo) {
        $where_opt = '';
        if (!empty($ejecucionfasefile_id)) {
            $where_opt .= ' AND eff.tb_ejecucionfasefile_id = :param_opc0';
        }
        if (!empty($ejecucionfase_id)) {
            $where_opt .= ' AND eff.tb_ejecucionfase_id = :param_opc1';
        }
        if (!empty($ejecucion_id)) {
            $where_opt .= ' AND ef.tb_ejecucion_id = :param_opc2';
        }

        if (is_numeric($upload_id)) {
            $where_opt .= " AND eff.upload_id = $upload_id";
        } elseif ($upload_id == 'not null') {
            $where_opt .= " AND eff.upload_id IS NOT NULL AND TRIM(eff.upload_id) <> ''";
        } elseif ($upload_id == 'null') {
            $where_opt .= " AND eff.upload_id IS NULL OR eff.upload = ''";
        }

        if (!empty($tipo_file)) {
            $where_opt .= ' AND eff.tb_ejecucionfasefile_tipodoc = :param_opc3';
        }
        if (!empty($resol_est)) {
            $where_opt .= ' AND eff.tb_ejecucionfasefile_resol_est = :param_opc4';
        }
        if (!empty($escrito_tipo)) {
            $where_opt .= ' AND eff.tb_ejecucionfasefile_escrito_tipo = :param_opc5';
        }
        if (!empty($acta_tipo)) {
            $where_opt .= ' AND eff.tb_ejecucionfasefile_acta_tipo = :param_opc6';
        }
        if (is_numeric($this->num)) {
            $where_opt .= " AND eff.tb_ejecucionfasefile_num = " . $this->num;
        } elseif ($this->num == 'not null') {
            $where_opt .= " AND eff.tb_ejecucionfasefile_num IS NOT NULL AND TRIM(eff.tb_ejecucionfasefile_num) <> ''";
        } elseif ($this->num == 'null') {
            $where_opt .= " AND eff.tb_ejecucionfasefile_num IS NULL OR eff.upload = ''";
        }

        if (!empty($this->oficiocargo_tipo)) {
            $where_opt .= ' AND eff.tb_ejecucionfasefile_oficiocargo_tipo = :param_opc7';
        }

        if (in_array(strval($this->incluir_redac), array('0', '1'))) {
            $where_opt .= ' AND eff.tb_ejecucionfasefile_incluir_redac = :param_opc8';
        }

        try {
            $sql = "SELECT eff.*,
                        u.upload_url, u.upload_reg,
                        gp.tb_gasto_idfk
                    FROM tb_ejecucionfasefile eff
                    LEFT JOIN upload u ON (eff.upload_id = u.upload_id)
                    LEFT JOIN tb_ejecucionfase ef ON (eff.tb_ejecucionfase_id = ef.tb_ejecucionfase_id)
                    LEFT JOIN tb_gastopago gp ON (eff.tb_gastopago_id = gp.tb_gastopago_id)
                    WHERE eff.tb_ejecucionfasefile_xac = 1 $where_opt
                    ORDER BY eff.tb_ejecucionfasefile_fecreg ASC;";
            //
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($ejecucionfasefile_id)) {
                $sentencia->bindParam(':param_opc0', $ejecucionfasefile_id, PDO::PARAM_INT);
            }
            if (!empty($ejecucionfase_id)) {
                $sentencia->bindParam(':param_opc1', $ejecucionfase_id, PDO::PARAM_INT);
            }
            if (!empty($ejecucion_id)) {
                $sentencia->bindParam(':param_opc2', $ejecucion_id, PDO::PARAM_INT);
            }
            if (!empty($tipo_file)) {
                $sentencia->bindParam(':param_opc3', $tipo_file, PDO::PARAM_INT);
            }
            if (!empty($resol_est)) {
                $sentencia->bindParam(':param_opc4', $resol_est, PDO::PARAM_INT);
            }
            if (!empty($escrito_tipo)) {
                $sentencia->bindParam(':param_opc5', $escrito_tipo, PDO::PARAM_INT);
            }
            if (!empty($acta_tipo)) {
                $sentencia->bindParam(':param_opc6', $acta_tipo, PDO::PARAM_INT);
            }
            if (!empty($this->oficiocargo_tipo)) {
                $sentencia->bindParam(':param_opc7', $this->oficiocargo_tipo, PDO::PARAM_INT);
            }
            if (in_array(strval($this->incluir_redac), array('0', '1'))) {
                $sentencia->bindParam(':param_opc8', $this->incluir_redac, PDO::PARAM_INT);
            }
            $sentencia->execute();
            $retorno["cantidad"] = intval($sentencia->rowCount());

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_ejecucionfasefile (
                    tb_ejecucionfasefile_archivonom,
                    tb_ejecucionfasefile_fecnoti,
                    tb_ejecucionfase_id,
                    tb_ejecucionfasefile_tipodoc,
                    tb_ejecucionfasefile_resol_est,
                    tb_ejecucionfasefile_escrito_tipo,
                    tb_ejecucionfasefile_genera_gasto,
                    tb_ejecucionfasefile_acta_tipo,
                    tb_ejecucionfasefile_num,
                    tb_ejecucionfasefile_oficiocargo_tipo,
                    tb_ejecucionfasefile_cargo_fecingreso,
                    tb_ejecucionfasefile_incluir_redac)
                VALUES (
                    :param0,
                    :param1,
                    :param2,
                    :param3,
                    :param4,
                    :param5,
                    :param6,
                    :param7,
                    :param8,
                    :param9,
                    :param10,
                    :param11);";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->archivonom, PDO::PARAM_STR);
            $sentencia->bindParam(':param1', $this->fecnoti, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->ejecucionfase_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param3', $this->tipodoc, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->resol_est, PDO::PARAM_INT);
            $sentencia->bindParam(':param5', $this->escrito_tipo, PDO::PARAM_INT);
            $sentencia->bindParam(':param6', $this->genera_gasto, PDO::PARAM_INT);
            $sentencia->bindParam(':param7', $this->acta_tipo, PDO::PARAM_INT);
            $sentencia->bindParam(':param8', $this->num, PDO::PARAM_INT);
            $sentencia->bindParam(':param9', $this->oficiocargo_tipo, PDO::PARAM_INT);
            $sentencia->bindParam(':param10', $this->cargo_fecingreso, PDO::PARAM_STR);
            $sentencia->bindParam(':param11', $this->incluir_redac, PDO::PARAM_INT);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function eliminar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_ejecucionfasefile WHERE tb_ejecucionfasefile_id = :param01;";

            $sentencia = $this->dblink->prepare($sql);
            if (!empty($this->id)) {
                $sentencia->bindParam(":param01", $this->id, PDO::PARAM_INT);
            }
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar_campo($ejecucionfasefile_id, $ejecucionfasefile_columna, $ejecucionfasefile_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($ejecucionfasefile_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_ejecucionfasefile SET " . $ejecucionfasefile_columna . " = :ejecucionfasefile_valor WHERE tb_ejecucionfasefile_id = :ejecucionfasefile_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":ejecucionfasefile_id", $ejecucionfasefile_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":ejecucionfasefile_valor", $ejecucionfasefile_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":ejecucionfasefile_valor", $ejecucionfasefile_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    /* GERSON (19-12-24) */
    // obtener ejecucionfasefile
    function mostrarUno($ejecucionfasefile_id){
        try {
          $sql = "SELECT * FROM tb_ejecucionfasefile WHERE tb_ejecucionfasefile_id =:ejecucionfasefile_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":ejecucionfasefile_id", $ejecucionfasefile_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    // obtener ejecucionliberacion
    function mostrarUnoEjecucionLineracion($ejecucionliberacion_id){
        try {
          $sql = "SELECT * FROM tb_ejecucionliberacion WHERE tb_ejecucionliberacion_id =:ejecucionliberacion_id";
  
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":ejecucionliberacion_id", $ejecucionliberacion_id, PDO::PARAM_INT);
          $sentencia->execute();
  
          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
          }
  
          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }

    // modificar campo ejecucionliberacion
    public function modificar_campo_ejecucionliberacion($ejecucionliberacion_id, $ejecucionliberacion_columna, $ejecucionliberacion_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($ejecucionliberacion_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_ejecucionliberacion SET " . $ejecucionliberacion_columna . " = :ejecucionliberacion_valor WHERE tb_ejecucionliberacion_id = :ejecucionliberacion_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":ejecucionliberacion_id", $ejecucionliberacion_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":ejecucionliberacion_valor", $ejecucionliberacion_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":ejecucionliberacion_valor", $ejecucionliberacion_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    // Obtener ejecucionfase
    public function obtenerEjecucionFase($ejecucionfase_id) {
        try {
            $sql = "SELECT ef.tb_ejecucion_id, e.tb_credito_id
                    FROM tb_ejecucionfase ef
                    INNER JOIN tb_ejecucion e ON ef.tb_ejecucion_id = e.tb_ejecucion_id
                    WHERE ef.tb_ejecucionfase_xac = 1
                    AND ef.tb_ejecucionfase_id = :ejecucionfase_id;";
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':ejecucionfase_id', $ejecucionfase_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Revisar si ya existe registro en la tabla 'tb_ejecucionliberacion'
    public function existeEjecucionLiberacion($ejecucion_id) {
        try {
            $sql = "SELECT *
                    FROM tb_ejecucionliberacion
                    WHERE tb_ejecucionliberacion_xac = 1
                    AND tb_ejecucion_id = :ejecucion_id
                    ORDER BY tb_ejecucionliberacion_id DESC;";
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':ejecucion_id', $ejecucion_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Revisar si ya existe registro en la tabla 'tb_ejecucionliberacion' por credito
    public function existeEjecucionLiberacionXCredito($credito_id) {
        try {
            $sql = "SELECT *
                    FROM tb_ejecucionliberacion
                    WHERE tb_ejecucionliberacion_xac = 1
                    AND tb_ejecucion_id = 0
                    AND tb_credito_id = :credito_id
                    ORDER BY tb_ejecucionliberacion_id DESC;";
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':credito_id', $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // obtener credito
    function mostrarCreditoXId($credito_id) {
        try {
            $sql = "SELECT *
                    FROM tb_creditogarveh
                    WHERE tb_credito_id = :credito_id
                    AND tb_credito_xac = 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay créditos registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Registro en la tabla 'tb_ejecucionliberacion'
    public function insertarLiberacion($fec_reg, $usuario_id, $credito_id, $ejecucion_id, $fecini, $fecfin, $monto_cuota, $monto_gasto) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_ejecucionliberacion (
                    tb_ejecucionliberacion_xac,
                    tb_ejecucionliberacion_fecreg,
                    tb_usuario_id,
                    tb_credito_id,
                    tb_ejecucion_id,
                    tb_ejecucionliberacion_fecini,
                    tb_ejecucionliberacion_fecfin,
                    tb_ejecucionliberacion_monto_cuota,
                    tb_ejecucionliberacion_monto_gasto,
                    tb_ejecucionliberacion_fecmod,
                    tb_ejecucionliberacion_usemod,
                    tb_ejecucionliberacion_est)
                VALUES (
                    1,
                    :fec_reg,
                    :usuario_id,
                    :credito_id,
                    :ejecucion_id,
                    :fecini,
                    :fecfin,
                    :monto_cuota,
                    :monto_gasto,
                    NULL,
                    NULL,
                    1);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':fec_reg', $fec_reg, PDO::PARAM_STR);
            $sentencia->bindParam(':usuario_id', $usuario_id, PDO::PARAM_INT);
            $sentencia->bindParam(':credito_id', $credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(':ejecucion_id', $ejecucion_id, PDO::PARAM_INT);
            $sentencia->bindParam(':fecini', $fecini, PDO::PARAM_STR);
            $sentencia->bindParam(':fecfin', $fecfin, PDO::PARAM_STR);
            $sentencia->bindParam(':monto_cuota', $monto_cuota, PDO::PARAM_INT);
            $sentencia->bindParam(':monto_gasto', $monto_gasto, PDO::PARAM_INT);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    //

    // cuotas garveh
    public function mostrar_pagos_garveh($cliente_id, $fecha_incautacion, $fecha_fin='') {
        try {
            $sql = "SELECT i.*, cu.tb_cuentadeposito_nom, cu.tb_cuentadeposito_ban, c.tb_cuota_num, c.tb_cuota_fec, cre.tb_credito_numcuo, cre.tb_credito_numcuomax, cre.tb_cuotatipo_id
                    FROM tb_ingreso i
                    LEFT JOIN tb_cuentadeposito cu ON i.tb_cuentadeposito_id = cu.tb_cuentadeposito_id
                    INNER JOIN tb_cuotapago cp ON i.tb_ingreso_modide = cp.tb_cuotapago_id
                    INNER JOIN tb_cuotadetalle cd ON cp.tb_cuotapago_modid = cd.tb_cuotadetalle_id
                    INNER JOIN tb_cuota c ON cd.tb_cuota_id = c.tb_cuota_id
                    INNER JOIN tb_creditogarveh cre ON c.tb_credito_id = cre.tb_credito_id
                    WHERE i.tb_ingreso_xac = 1
                    AND cre.tb_credito_tip <> 2
                    AND i.tb_cuenta_id = 1
                    AND i.tb_subcuenta_id = 3
                    AND i.tb_modulo_id = 30
                    AND i.tb_cliente_id = :cliente_id
                    AND i.tb_ingreso_fecreg >= :fecha_incautacion";
                if ($fecha_fin!='') {
                    $sql .= " AND i.tb_ingreso_fecreg <= :fecha_fin;";
                } else {
                    $sql .= " AND i.tb_ingreso_fecreg <= NOW();";
                }
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':cliente_id', $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':fecha_incautacion', $fecha_incautacion, PDO::PARAM_STR);
            if ($fecha_fin!='') {
                $sentencia->bindParam(':fecha_fin', $fecha_fin, PDO::PARAM_STR);
            } 
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function mostrar_suma_pagos_garveh($cliente_id, $fecha_incautacion, $fecha_fin='') {
        try {
            $sql = "SELECT SUM(i.tb_ingreso_imp) AS total_garveh, cre.tb_moneda_id
                    FROM tb_ingreso i
                    LEFT JOIN tb_cuentadeposito cu ON i.tb_cuentadeposito_id = cu.tb_cuentadeposito_id
                    INNER JOIN tb_cuotapago cp ON i.tb_ingreso_modide = cp.tb_cuotapago_id
                    INNER JOIN tb_cuotadetalle cd ON cp.tb_cuotapago_modid = cd.tb_cuotadetalle_id
                    INNER JOIN tb_cuota c ON cd.tb_cuota_id = c.tb_cuota_id
                    INNER JOIN tb_creditogarveh cre ON c.tb_credito_id = cre.tb_credito_id
                    WHERE i.tb_ingreso_xac = 1
                    AND cre.tb_credito_tip <> 2
                    AND i.tb_cuenta_id = 1
                    AND i.tb_subcuenta_id = 3
                    AND i.tb_modulo_id = 30
                    AND i.tb_cliente_id = :cliente_id
                    AND i.tb_ingreso_fecreg >= :fecha_incautacion;";
            if ($fecha_fin!='') {
                $sql .= " AND i.tb_ingreso_fecreg <= :fecha_fin;";
            } else {
                $sql .= " AND i.tb_ingreso_fecreg <= NOW();";
            }
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':cliente_id', $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':fecha_incautacion', $fecha_incautacion, PDO::PARAM_STR);
            if ($fecha_fin!='') {
                $sentencia->bindParam(':fecha_fin', $fecha_fin, PDO::PARAM_STR);
            } 
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // cuotas adenda
    public function mostrar_pagos_adenda($cliente_id, $fecha_incautacion) {
        try {
            $sql = "SELECT i.*, cre.tb_credito_id, cu.tb_cuentadeposito_nom, cu.tb_cuentadeposito_ban, c.tb_cuota_num, c.tb_cuota_fec, cre.tb_credito_numcuo, cre.tb_credito_numcuomax, cre.tb_cuotatipo_id
                    FROM tb_ingreso i
                    LEFT JOIN tb_cuentadeposito cu ON i.tb_cuentadeposito_id = cu.tb_cuentadeposito_id
                    INNER JOIN tb_cuotapago cp ON i.tb_ingreso_modide = cp.tb_cuotapago_id
                    INNER JOIN tb_cuotadetalle cd ON cp.tb_cuotapago_modid = cd.tb_cuotadetalle_id
                    INNER JOIN tb_cuota c ON cd.tb_cuota_id = c.tb_cuota_id
                    INNER JOIN tb_creditogarveh cre ON c.tb_credito_id = cre.tb_credito_id
                    WHERE i.tb_ingreso_xac = 1
                    AND i.tb_cuenta_id <> 20 AND i.tb_cuenta_id <> 21
                    AND cre.tb_credito_tip = 2
                    AND i.tb_cliente_id = :cliente_id
                    AND i.tb_ingreso_fecreg >= :fecha_incautacion
                    AND i.tb_ingreso_fecreg <= NOW();";
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':cliente_id', $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':fecha_incautacion', $fecha_incautacion, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function mostrar_suma_pagos_adenda($cliente_id, $fecha_incautacion, $fecha_fin='') {
        try {
            $sql = "SELECT (i.tb_ingreso_imp) AS total_adenda, i.tb_moneda_id
                    FROM tb_ingreso i
                    LEFT JOIN tb_cuentadeposito cu ON i.tb_cuentadeposito_id = cu.tb_cuentadeposito_id
                    INNER JOIN tb_cuotapago cp ON i.tb_ingreso_modide = cp.tb_cuotapago_id
                    INNER JOIN tb_cuotadetalle cd ON cp.tb_cuotapago_modid = cd.tb_cuotadetalle_id
                    INNER JOIN tb_cuota c ON cd.tb_cuota_id = c.tb_cuota_id
                    INNER JOIN tb_creditogarveh cre ON c.tb_credito_id = cre.tb_credito_id
                    WHERE i.tb_ingreso_xac = 1
                    AND i.tb_cuenta_id <> 20 AND i.tb_cuenta_id <> 21
                    AND cre.tb_credito_tip = 2
                    AND i.tb_cliente_id = :cliente_id
                    AND i.tb_ingreso_fecreg >= :fecha_incautacion";
                if ($fecha_fin!='') {
                    $sql .= " AND i.tb_ingreso_fecreg <= :fecha_fin;";
                } else {
                    $sql .= " AND i.tb_ingreso_fecreg <= NOW();";
                }
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':cliente_id', $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':fecha_incautacion', $fecha_incautacion, PDO::PARAM_STR);
            if ($fecha_fin!='') {
                $sentencia->bindParam(':fecha_fin', $fecha_fin, PDO::PARAM_STR);
            } 
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Obtener el monto total de gastos hasta la fecha de incautación
    public function obtenerSumaGastos($cliente_id, $fecha_incautacion, $fecha_fin='') {
        try {
            $sql = "SELECT SUM(i.tb_ingreso_imp) AS total_gasto, i.tb_moneda_id
                    FROM tb_ingreso i
                    WHERE i.tb_ingreso_xac = 1
                    AND i.tb_modulo_id = 71
                    AND i.tb_cliente_id = :cliente_id
                    AND i.tb_ingreso_fecreg >= :fecha_incautacion";
                if ($fecha_fin!='') {
                    $sql .= " AND i.tb_ingreso_fecreg <= :fecha_fin;";
                } else {
                    $sql .= " AND i.tb_ingreso_fecreg <= NOW();";
                }
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':cliente_id', $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':fecha_incautacion', $fecha_incautacion, PDO::PARAM_STR);
            if ($fecha_fin!='') {
                $sentencia->bindParam(':fecha_fin', $fecha_fin, PDO::PARAM_STR);
            } 
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // obtener listado de ejecuciones de incautacion
    public function obtenerHistorialEjecucionLiberacion($credito_id) {
        try {
            $sql = "SELECT *
                    FROM tb_ejecucionliberacion
                    WHERE tb_ejecucionliberacion_xac = 1
                    AND tb_ejecucionliberacion_est <> 3
                    AND tb_ejecucionliberacion_fecini IS NOT NULL
                    AND tb_ejecucionliberacion_fecfin IS NOT NULL
                    AND tb_credito_id = :credito_id
                    ORDER BY tb_ejecucionliberacion_id DESC;";
            
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':credito_id', $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}
