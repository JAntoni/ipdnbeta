<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';
require_once '../upload/Upload.class.php';
$oUpload = new Upload();

$upload_id = intval($_POST['upload_id']);
$modulo_nom = $_POST['modulo_nom'];
$ejecucionfase_id = intval($_POST['ejecucionfase_id']);
$vista = $_POST['vista'];
$ejecucionfase_completado = $_POST['ejecucionfase_completado'];

$nombre_documento = ''; //usable para historial
if ($modulo_nom == 'ejecucion_liquidacion') {
    $nombre_documento = 'Cálculo de Liquidacion';
} elseif ($modulo_nom == 'ejecucion_certificado_regveh') {
    $nombre_documento = 'Certificado Registral Vehicular';
} elseif ($modulo_nom == 'ejecucion_varios-demscan') {
    $nombre_documento = 'Demanda escaneada';
} elseif ($modulo_nom == 'ejecucion_varios-cartascan') {
    $nombre_documento = 'Carta notarial escaneada';
} else {
    $nombre_documento_orig = trim(strtoupper($_POST['archivo_nom']));
    $nombre_documento = substr($nombre_documento_orig, 1);
}

$ver = '';
$result = $oUpload->mostrarUno($upload_id);

if ($result['estado'] == 1) {
    $ejecucion_id = $result['data']['modulo_id'];
    $btn_eliminar = $ejecucionfase_completado != 1 ? '<small class="text-muted pull-right"><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="confirmar_eliminar()">Eliminar <i class="fa fa-trash"></i></a></small>' : '';

    $ver =
    '<div class="item">
        <div class="row">
            <div class="col-md-12">
                <p class="message" style="font-family:cambria">
                    <a href="' . $result['data']['upload_url'] . '" class="name"  target="_blank" style="font-size: 14px;">
                        Pdf '.$nombre_documento.' <i class="fa fa-file-pdf-o" title="VER"></i>
                    </a>
                    '.$btn_eliminar.'
                </p>
            </div>
        </div>
    </div>';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ejecucion_pdf_ver" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000099">ARCHIVO SUBIDO(s) EN PDF</h4>
            </div>
            <div class="modal-body">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                            <div class="btn-group" data-toggle="btn-toggle"></div>
                        </div>
                    </div>

                    <div class="box-body" id="chat-box">
                        <input type="hidden" name="hdd_ejecucionpdf_vista" id="hdd_ejecucionpdf_vista" value="archivo_subido">
                        <input type="hidden" name="hdd_ejecucionpdfver_upload_id" id="hdd_ejecucionpdfver_upload_id" value="<?php echo $upload_id;?>">
                        <input type="hidden" name="hdd_ejecucionpdfver_ejecucion_id" id="hdd_ejecucionpdfver_ejecucion_id" value="<?php echo $ejecucion_id;?>">
                        <input type="hidden" name="hdd_ejecucionpdfver_ejecucionfase_id" id="hdd_ejecucionpdfver_ejecucionfase_id" value="<?php echo $ejecucionfase_id;?>">
                        <input type="hidden" name="hdd_modulo_nom" id="hdd_modulo_nom" value="<?php echo $modulo_nom; ?>">
                        <input type="hidden" name="hdd_ejecucionupload_vista" id="hdd_ejecucionupload_vista" value="<?php echo $vista; ?>">
                        <input type="hidden" name="hdd_tablaorig_id" id="hdd_tablaorig_id" value="<?php echo $_POST['tabla_id2']; ?>">
                        <input type="hidden" name="hdd_tabla_nom" id="hdd_tabla_nom" value="<?php echo $_POST['tabla_nom']; ?>">
                        <input type="hidden" name="hdd_archivo_nom" id="hdd_archivo_nom" value="<?php echo $nombre_documento_orig; ?>">
                        <?php echo $ver ?>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/ejecucionupload/ejecucionpdf_form.js?ver=024'; ?>"></script>