<?php
require_once '../funciones/funciones.php';
$titulo = '';
$usuario_action = $_POST['action'];
if ($usuario_action == 'I') {
    $titulo = 'Subir <b>Archivo</b>';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
$action_ejecucionupload = devuelve_nombre_usuario_action($usuario_action);

$listo = $_POST['action2'] == 'L_ejecucion' ? 0 : 1;
if ($listo == 0) {
    $mensaje = 'Solo permiso de lectura, no puede subir archivo';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ejecucionupload_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo;?></h4>
                </div>
                <form id="form_ejecucionupload" method="post">
                    <input type="hidden" name="hdd_action_ejecucionupload" id="hdd_action_ejecucionupload" value="<?php echo $action_ejecucionupload;?>">
                    <input type="hidden" name="hdd_ejecucion_id" id="hdd_ejecucion_id" value="<?php echo $_POST['ejecucion_id']; ?>">
                    <input type="hidden" name="hdd_ejecucionfase_id" id="hdd_ejecucionfase_id" value="<?php echo $_POST['ejecucionfase_id']; ?>">
                    <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $_POST['credito_id']; ?>">
                    <input type="hidden" name="hdd_modulo_nom" id="hdd_modulo_nom" value="<?php echo $_POST['modulo_nom']; ?>">
                    <input type="hidden" name="hdd_tablaorig_id" id="hdd_tablaorig_id" value="<?php echo $_POST['tabla_id2']; ?>">
                    <input type="hidden" name="hdd_nombre_doc" id="hdd_nombre_doc" value="<?php echo $_POST['nombre_doc']; ?>">
                    <input type="hidden" name="hdd_tabla_nom" id="hdd_tabla_nom" value="<?php echo $_POST['tabla_nom']; ?>">
                    
                    <?php if ($_POST['pdf_is'] != 'new') {
                        $id_reg_mod = explode('-', $_POST['pdf_is']);
                        $mod_or_new = $id_reg_mod[0];
                        $id_reg_mod = $id_reg_mod[1];
                    ?>
                        <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $mod_or_new; ?>">
                        <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="<?php echo intval($id_reg_mod); ?>">
                    <?php } else { ?>
                        <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $_POST['pdf_is']; ?>">
                        <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="0">
                    <?php } ?>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input id="file_upload" name="file_upload" type="file" multiple="false">
                            </div>
                            <div class="col-md-12">
                                <div id="queue"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissible" id="alert_upload_ejecucion" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                    <h4><i class="icon fa fa-ban"></i> Alerta</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <button type="button" class="btn btn-info" id="btn_guardar_creditogarvehpdf" onclick="javascript:$('#file_upload').uploadifive('upload')">Guardar Pdf</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_cerrar_creditogarvehfile">Listo</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo 'static/css/uploadifive/uploadifive.css'; ?>">
<script type="text/javascript" src="<?php echo 'static/js/uploadifive/jquery.uploadifive.js'; ?>"></script>
<script type="text/javascript" src="<?php echo 'vista/ejecucionupload/ejecucionpdf_form.js?ver=432'; ?>"></script>
