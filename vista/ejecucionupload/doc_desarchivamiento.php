<?php
//require y use
    require_once '../../vendor/autoload.php';
    use PhpOffice\PhpWord\PhpWord;
    use PhpOffice\PhpWord\IOFactory;
    use PhpOffice\PhpWord\Shared\Html;
    use PhpOffice\PhpWord\Style\Font;
    use PhpOffice\PhpWord\Style\Paragraph;
    require_once '../funciones/funciones.php';
    require_once '../funciones/fechas.php';
    require_once '../ejecucion/Ejecucion.class.php';
    $oEjecucion = new Ejecucion();
    require_once '../creditogarveh/Creditogarveh.class.php';
    $oCredito = new Creditogarveh();
    require_once '../escriturapublica/Escriturapublica.class.php';
    $oEscritura = new Escriturapublica();
    require_once '../cargo/Cargo.class.php';
    $oCargo = new Cargo();
    require_once '../persona/Persona.class.php';
    $oPersona = new Persona();
//

//recibir datos GET
    $ejecucion_id = intval($_GET['d1']);
    $ejecucionfase_id = intval($_GET['d2']);
    $tipo_documento = $_GET['d3']; //solo puede ser WORD Y PDF
//

//validar formato de documento y que la ejecucion exista
    if (empty($tipo_documento) || !in_array($tipo_documento, array('pdf', 'word'))) {
        echo 'NO HAS SELECCIONADO EL TIPO DOCUMENTO. MODIFICA EL FINAL DE LA URL SI SERÁ WORD O PDF';
        exit();
    } elseif ($tipo_documento == 'pdf'){
        require_once '../../static/tcpdf/tcpdf.php';
    }

    $ejecucion_reg = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '');
    if ($ejecucion_reg['estado'] != 1) {
        unset($ejecucion_reg);
        echo 'ERROR EN EL ID DE EJECUCION. NO HUBO RESULTADO PARA LA CONSULTA';
        exit();
    } else {
        $ejecucion_reg = $ejecucion_reg['data'][0];
    }
//

//INFO DEL CASO
    $exped = $ejecucion_reg['tb_ejecucion_expediente'];
    $num_juzgado = ['', '3', '8'];
//

//CLIENTE INFO
    $demandado_s = 'Demandado';
    $cliente_nom = '';
    if ($ejecucion_reg['tb_cliente_tip'] == 1) {
        $cliente_nom = $ejecucion_reg['tb_cliente_nom'];

        $cliente_info2 = $cliente_nom;

        //buscar asociaciones
            $credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];
            $res_escritura = $oEscritura->listar_todos('', $credito_getdocs);
            $cliente_ids = explode(',', $res_escritura['data'][0]['tb_cliente_ids']);
            if (count($cliente_ids) > 1) {
                //buscar nom cliente asociado
                require_once '../cliente/Cliente.class.php';
                $oCliente = new Cliente();
                $demandado_s = 'Demandados';

                for ($i=1; $i < count($cliente_ids); $i++) {
                    $cli_asoc_id = $cliente_ids[$i];

                    $res_cliente_asoc = $oCliente->mostrarUno($cli_asoc_id);
                    if ($res_cliente_asoc['estado'] == 1) {
                        $cliente_info2 .=
                            //cerrar td y tr actual
                            '</td></tr>'.
                            //crear la nueva tr con espaciado
                            '<tr>'.
                                '<td style="width:43%; line-height: 1.8;"></td>'.
                                '<td style="width:55%; line-height: 1.8;">'.$res_cliente_asoc['data']['tb_cliente_nom'];
                        //
                    }
                    unset($res_cliente_asoc);
                }
            }
        //
    } else {
        $credito_datos = $oCredito->mostrarUno($ejecucion_reg['tb_credito_id'])['data'];
        $cliente_nom = $ejecucion_reg['tb_cliente_emprs'];

        $cliente_info2 = $cliente_nom;
    }
//

//datos del representante ipdn
    $rep_ipdn_id = $ejecucion_reg['tb_persona_id'];//1 Manuel, 2 Herbert Engel
    $rep_ipdn = $oPersona->mostrar_uno_id($rep_ipdn_id)['data'];
    $rep_ipdn_nom = $rep_ipdn['tb_persona_ape'].' '.$rep_ipdn['tb_persona_nom'];
    $rep_ipdn_doc = $rep_ipdn['tb_persona_doc'];

    $rep_ipdn_cargo_id = $ejecucion_reg['tb_cargo_id'];
    $rep_ipdn_cargo_nom = customUcwords($oCargo->mostrarUno($rep_ipdn_cargo_id)['data']['tb_cargo_nom']);
//

$title = 'DOC_DESARCHIVAMIENTO_'.str_replace(' ', '', $cliente_nom).''. date('YmdHis');
$fec_hoy = fechaActual(date('Y-m-d'));

//TCPDF
if ($tipo_documento == 'pdf') {
    class MYPDF extends TCPDF {
        public function Header() {
            //vacio pq no necesita header ni footer
        }
        public function Footer() {
            //vacio pq no necesita header ni footer
        }
    }
    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.prestamosdelnortechiclayo.com');
    $pdf->SetSubject('www.prestamosdelnortechiclayo.com');
    $pdf->SetKeywords('www.prestamosdelnortechiclayo.com');
    $pdf->SetTitle($title);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    //set margins
    $pdf->SetMargins(23, 26, 25); // left top right
    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // add a page
    $pdf->AddPage('P', 'A4');
}

$html =
'<table>'.
    //SOLICITO
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Solicito</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: justify;">Desarchivamiento de expediente</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PRESENTACION DE EMPRESA Y CLIENTE
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify; line-height: 1.6;"><b>SEÑOR JEFE DEL ARCHIVO CENTRAL DE EXPEDIENTES DE LA CORTE SUPERIOR DE JUSTICIA DE LAMBAYEQUE</b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:62%; text-align: justify; line-height: 1.6; font-size: 27px;">'.
                '<b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b>, CON R.U.C. N° 20600752872, inscrita en la partida registral N° 11218951 del Registro de Personas Jurídicas de la Oficina Registral de Chiclayo '.
                'y con domicilio fiscal en Centro Comercial Boulevard Plaza Ext. A-07, primer piso, distrito de Chiclayo, provincia de Chiclayo, departamento de Lambayeque; debidamente representada por su '.
                $rep_ipdn_cargo_nom.' <b>'.$rep_ipdn_nom.'</b>, con DNI N° '.$rep_ipdn_doc.', ante usted con el debido respeto me presento y decimos:'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO I
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; line-height: 1.8; font-size: 30px;">'.
                'Que, por convenir a nuestro derecho, solicito a vuestra Jefatura el Desarchivamiento del expediente judicial que se detalla a continuación:'.
            '</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left; line-height: 1.8;">●</td>'.
            '<td style="width:25%; text-align: justify; line-height: 1.8;"><b>Expediente N°:</b></td>'.
            '<td style="width:55%; text-align: justify; line-height: 1.8;">'.$exped.'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left; line-height: 1.8;">●</td>'.
            '<td style="width:25%; text-align: justify; line-height: 1.8;"><b>Demandante:</b></td>'.
            '<td style="width:55%; text-align: justify; line-height: 1.8;">INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left; line-height: 1.8;">●</td>'.
            '<td style="width:25%; text-align: justify; line-height: 1.8;"><b>'.$demandado_s.':</b></td>'.
            '<td style="width:55%; text-align: justify; line-height: 1.8;">'.$cliente_info2.'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left; line-height: 1.8;">●</td>'.
            '<td style="width:25%; text-align: justify; line-height: 1.8;"><b>Materia:</b></td>'.
            '<td style="width:55%; text-align: justify; line-height: 1.8;">INCAUTACIÓN DE BIEN MUEBLE</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left; line-height: 1.8;">●</td>'.
            '<td style="width:25%; text-align: justify; line-height: 1.8;"><b>Juzgado Origen:</b></td>'.
            '<td style="width:55%; text-align: justify; line-height: 1.8;">'.$num_juzgado[$ejecucion_reg['tb_ejecucion_juzgado']].'° JUZGADO CIVIL − COMERCIAL</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left; line-height: 1.8;">●</td>'.
            '<td style="width:25%; text-align: justify; line-height: 1.8;"><b>Motivo del desarchivo:</b></td>'.
            '<td style="width:55%; text-align: justify; line-height: 1.8;"><b>[COMPLETAR]</b></td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left; line-height: 1.8;">●</td>'.
            '<td style="width:25%; text-align: justify; line-height: 1.8;"><b>Acompañados:</b></td>'.
            '<td style="width:55%; text-align: justify; line-height: 1.8;">NINGUNO</td>'.
        '</tr>'.
    //
    '<br>'.
    //OTROSI, POR LO EXPUESTO
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 140px; line-height: 1.8; font-size: 30px;">'.
                '<u><b>Otrosí digo</b></u>: Adjunto copia simple de Vigencia de poder, así como DNI de nuestro representante y tasa judicial por desarchivamiento.'.
            '</td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 140px; font-size: 29px; line-height: 1.8;">'.
                '<b>Por lo expuesto:</b>'.
            '</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 140px; font-size: 29px;">'.
                'Pido a Usted, se sirva disponer a quien corresponda la remisión del expediente sea derivado al juzgado correspondiente.'.
            '</td>'.
        '</tr>'.
    //
    '<br>'.
    //FINAL
        '<tr>'.
            '<td style="width:62%; text-align: left;"></td>'.
            '<td style="width:38%;  font-size: 29px;">Chiclayo, '.strtolower($fec_hoy).'.</td>'.
        '</tr>'.
    //FINAL
'</table>'
;

if ($tipo_documento == 'word') {
    // Crear una instancia de PHPWord
    $phpWord = new PhpWord();

    $phpWord->setDefaultFontName('Arial');
    $phpWord->setDefaultFontSize(10);

    // Definir estilo de párrafo
    $paragraphStyle = array('spaceAfter' => 0, 'spacing' => 0);

    // Crear una nueva sección
    $section1 = $phpWord->addSection();

    // Renderizar el contenido HTML 1
    Html::addHtml($section1, $html, false, false, $paragraphStyle);

    // Guardar el documento
    $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save('doc_desarchivamiento.docx');
    echo 'Documento para cliente: ' . $cliente_nom . ', generado correctamente...';
    header('Location: ../ejecucionupload/doc_desarchivamiento.docx');
}

if ($tipo_documento == 'pdf') {
    $pdf->SetFont('Arial', '', 10);
    $pdf->writeHTML($html, true, 0, true, true);
    $pdf->Ln();
    // reset pointer to the last page
    $pdf->lastPage();
    //Close and output PDF document
    $nombre_archivo = $title . ".pdf";
    $pdf->Output($nombre_archivo, 'I');
}
