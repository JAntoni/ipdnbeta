
$(document).ready(function () {
	if ($('#hdd_ejecucionpdf_vista').val() != 'archivo_subido') {
		var file_size_max = '50MB';
		$('#file_upload').uploadifive({
			'auto': false,
			'formData': {
				'action': $('#hdd_action_ejecucionupload').val(),
				'ejecucion_id': $('#hdd_ejecucion_id').val(),
				'ejecucionfase_id': $('#hdd_ejecucionfase_id').val(),
				'credito_id': $('#hdd_credito_id').val(),
				'modulo_nom': $('#hdd_modulo_nom').val(),
				'id_reg_mod': $('#hdd_id_reg_mod').val(),
				'pdf_is': $('#hdd_pdf_is').val(),
				'tablaorig_id': $('#hdd_tablaorig_id').val(),
				'nombre_doc': $('#hdd_nombre_doc').val(),
				'tabla_nom': $('#hdd_tabla_nom').val()
			},
			'uploadScript': VISTA_URL + 'ejecucionupload/ejecucionupload_controller.php',
			'queueSizeLimit': 1, //number of files you can have in the queue at one time
			'uploadLimit': 1, //maximum number of files that may be uploaded
			'multi': false, //Whether or not to allow multiple file selection in the browse dialog window
			'buttonText': 'Seleccionar Archivo',
			'height': 25, //height of the browse button in pixels.
			'width': 180, //width of the browse button in pixels.
			'fileSizeLimit': file_size_max,
			'fileType': ['pdf'],
			'onUploadComplete': function (file, data) {
				var response = data.split('|');//0: 1 o 0 dependiendo de exito en la subida, 1: upload_id, 2: url
				if (parseInt(response[0]) != 1 && parseInt(response[0]) != 2) {
					$('#alert_upload_ejecucion').show(300);
					$('#alert_upload_ejecucion').append('<strong>El archivo (' + file.name + ') no se pudo subir -> ' + data + '</strong><br>');
				}
				else {
					alerta_success("EXITO", "SE HA REGISTRADO CORRECTAMENTE EL DOCUMENTO");
					$("#modal_ejecucionupload_form").modal('hide');
					if ($.inArray($('#hdd_modulo_nom').val(), ['ejecucion_certificado_regveh', 'ejecucion_asiento_ins_color', 'ejecucion_asiento_ins_combu', 'ejecucion_varios-medcau', 'ejecucion_varios-dernot', 'ejecucion_varios-dilfue']) > -1) {
						actualizar_vista_upload(response[1], response[2], $('#hdd_modulo_nom').val());
					} else {
						actualizar_contenido_1fase($('#hdd_ejecucionfase_id').val());
						if ($('#hdd_modulo_nom').val() == 'ejecucion_varios-cartascan') {
							completar_tabla_cartanotarials();
						}
						if ($('#hdd_tabla_nom').val() == 'tb_ejecucionfasefile') {
							completar_tabla_files();
						}
					}
				}
			},
			'onError': function (errorType) {
				var message = "Error desconocido.";
				if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
					message = `Tamaño de archivo debe ser menor a ${file_size_max}.`;
					alert(message);
				} else if (errorType == 'FORBIDDEN_FILE_TYPE') {
					message = "Tipo de archivo no válido.";
					alert(message);
				}
			}
		});
	} else {
		var vista = $('#hdd_ejecucionupload_vista').val();
		if (vista == 'checklist') {
			console.log($('div.item').find('a'));
			disabled($('div.item').find('a.btn-danger'));
		}
	}
});

function confirmar_eliminar(){
	$.confirm({
		title: 'Sistema',
		content: `<b>¿Confirma que desea eliminar este archivo?</b>`,
		type: 'red',
		escapeKey: 'close',
		backgroundDismiss: true,
		columnClass: 'small',
		buttons: {
			Confirmar: {
				btnClass: 'btn-red',
				action: function() {
					eliminar_pdf();
				}
			},
			cancelar: function () {
			}
		}
	});
};

function eliminar_pdf() {
	var ejecucionfase_id = $('#hdd_ejecucionpdfver_ejecucionfase_id').val();
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ejecucionupload/ejecucionupload_controller.php",
		async: true,
		dataType: "json",
		data: ({
			action: 'eliminar_pdf',
			ejecucion_id: $('#hdd_ejecucionpdfver_ejecucion_id').val(),
			ejecucionfase_id: ejecucionfase_id,
			upload_id: $('#hdd_ejecucionpdfver_upload_id').val(),
			modulo_nom: $('#hdd_modulo_nom').val(),
			tablaorig_id: $('#hdd_tablaorig_id').val(),
			tabla_nom: $('#hdd_tabla_nom').val(),
			nombre_doc: $('#hdd_archivo_nom').val()
		}),
		success: function (data) {
			if (parseInt(data.estado) > 0) {
				alerta_success("EXITO", "Pdf eliminado correctamente");
				$("#modal_ejecucion_pdf_ver").modal('hide');
				actualizar_contenido_1fase(ejecucionfase_id);

				if ($('#hdd_modulo_nom').val() == 'ejecucion_varios-cartascan') {
					completar_tabla_cartanotarials();
				}
				if ($('#hdd_tabla_nom').val() == 'tb_ejecucionfasefile') {
					completar_tabla_files();
				}
			} else {
				alerta_error('Error', 'ERROR!:' + data.mensaje);
			}
		},
		error: function (data) {
			alerta_error('Error', 'ERROR!:' + data.responseText);
		}
	});
}