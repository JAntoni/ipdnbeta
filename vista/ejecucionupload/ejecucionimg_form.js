var final_response = '';
var files_response = '';
$(document).ready(function () {
	var file_size_max = '50MB';
	$('#file_upload').uploadifive({
		'auto': false,
		'formData': {
			'action': $('#hdd_action_ejecucionupload').val(),
			'ejecucion_id': $('#hdd_ejecucion_id').val(),
			'ejecucionfase_id': $('#hdd_ejecucionfase_id').val(),
			'credito_id': $('#hdd_credito_id').val(),
			'modulo_nom': $('#hdd_modulo_nom').val(),
			'id_reg_mod': $('#hdd_id_reg_mod').val(),
			'img_is': $('#hdd_img_is').val(),
			'tablaorig_id': $('#hdd_tablaorig_id').val(),
			'nombre_doc': $('#hdd_nombre_doc').val(),
			'tabla_nom': $('#hdd_tabla_nom').val(),
			'modulo_subnom': $('#hdd_modulo_subnom').val(),
			'filestorage_des': $('#hdd_filestorage_des').val()
		},
		'uploadScript': VISTA_URL + 'ejecucionupload/ejecucionupload_controller.php',
		'queueSizeLimit': 10, //number of files you can have in the queue at one time
		'uploadLimit': 10, //maximum number of files that may be uploaded
		'multi': true, //Whether or not to allow multiple file selection in the browse dialog window
		'buttonText': 'Seleccionar Archivo',
		'height': 25, //height of the browse button in pixels.
		'width': 180, //width of the browse button in pixels.
		'fileSizeLimit': file_size_max,
		'fileType': ["image/gif", "image/jpeg", "image/png", "image/jpg"],
		'onUploadComplete': function (file, data) {
			final_response += data + '|';
			files_response += file.name + ': ' +data+', ';
        },
		'onQueueComplete': function (file, data) {
			var arr = final_response.split('|');
			var mensaje_error = 0;
			//recorrer arr y si es 1 actualizar vista
			for (var i = 0; i < arr.length-1; i++) {
				if (parseInt(arr[i]) == 0) {
					mensaje_error = 1;
				}
			}

			if (/^comentario_galeria\d*$/.test($('#hdd_modulo_subnom').val())) {
				refresh_galeria($('#hdd_modulo_nom').val(), $('#hdd_ejecucion_id').val(), 'comentario_galeria', $('#hdd_tablaorig_id').val(), 'M_ejecucion', 0);
			}

			var mensaje_opc = mensaje_error == 1 ? ". <b>ALGUNOS ARCHIVOS NO SE HAN SUBIDO CORRECTAMENTE</b><br>"+files_response : "";
			alerta_success("HECHO", "SUBIDA EXITOSA"+mensaje_opc);
			
			//actualizar vista
			if (mensaje_error == 0) {
				$("#modal_ejecucionupload_form").modal('hide');
			}
			final_response = ''; files_response = '';
        },
		'onError': function (errorType) {
			var message = "Error desconocido.";
			if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
				message = `Tamaño de archivo debe ser menor a ${file_size_max}.`;
				alert(message);
			} else if (errorType == 'FORBIDDEN_FILE_TYPE') {
				message = "Tipo de archivo no válido.";
				alert(message);
			}
		}
	});
});

function confirmar_eliminar(){
	$.confirm({
		title: 'Sistema',
		content: `<b>¿Confirma que desea eliminar este archivo?</b>`,
		type: 'red',
		escapeKey: 'close',
		backgroundDismiss: true,
		columnClass: 'small',
		buttons: {
			Confirmar: {
				btnClass: 'btn-red',
				action: function() {
					eliminar_img();
				}
			},
			cancelar: function () {
			}
		}
	});
};