<?php
//require y use
    require_once '../../vendor/autoload.php';
    use PhpOffice\PhpWord\PhpWord;
    use PhpOffice\PhpWord\IOFactory;
    use PhpOffice\PhpWord\Shared\Html;
    use PhpOffice\PhpWord\Style\Font;
    use PhpOffice\PhpWord\Style\Paragraph;
    require_once '../funciones/funciones.php';
    require_once '../funciones/fechas.php';
    require_once '../ejecucion/Ejecucion.class.php';
    $oEjecucion = new Ejecucion();
    require_once '../creditogarveh/Creditogarveh.class.php';
    $oCredito = new Creditogarveh();
    require_once '../escriturapublica/Escriturapublica.class.php';
    $oEscritura = new Escriturapublica();
    require_once '../ejecucionupload/Ejecucionfasefile.class.php';
    $oEjecucionfasefile = new Ejecucionfasefile();
//

//recibir datos GET
    $ejecucion_id = intval($_GET['d1']);
    $ejecucionfase_id = intval($_GET['d2']);
    $tipo_documento = $_GET['d3']; //solo puede ser WORD Y PDF
//

//validar formato de documento y que la ejecucion exista
    if (empty($tipo_documento) || !in_array($tipo_documento, array('pdf', 'word'))) {
        echo 'NO HAS SELECCIONADO EL TIPO DOCUMENTO. MODIFICA EL FINAL DE LA URL SI SERÁ WORD O PDF';
        exit();
    } elseif ($tipo_documento == 'pdf'){
        require_once '../../static/tcpdf/tcpdf.php';
    }

    $ejecucion_reg = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '');
    if ($ejecucion_reg['estado'] != 1) {
        unset($ejecucion_reg);
        echo 'ERROR EN EL ID DE EJECUCION. NO HUBO RESULTADO PARA LA CONSULTA';
        exit();
    } else {
        $ejecucion_reg = $ejecucion_reg['data'][0];
    }
//

//INFO DEL CASO
    $exped = $ejecucion_reg['tb_ejecucion_expediente'];
    $esp_nom = $ejecucion_reg['tb_ejecucion_especialista'];
    $num_juzgado = ['', '3', '8'];

    //buscar el ULTIMO ESCRITO, para sumar 1 al escrito que toca
        $escritos_anter = $oEjecucionfasefile->listar('', '', $ejecucion_id, 'not null', 2, '', '', '');
        $num_escrito = str_pad(($escritos_anter['cantidad'] + 2), 2, '0', STR_PAD_LEFT);
    //
//

//CLIENTE INFO
    $cliente_nom = $cliente_repre_nom = '';
    if ($ejecucion_reg['tb_cliente_tip'] == 1) {
        $cliente_nom = $ejecucion_reg['tb_cliente_nom'];

        $cliente_info2 = "<b>$cliente_nom</b>";

        $credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];
        //buscar asociaciones
            $res_escritura = $oEscritura->listar_todos('', $credito_getdocs);
            $cliente_ids = explode(',', $res_escritura['data'][0]['tb_cliente_ids']);
            if (count($cliente_ids) > 1) {
                $lleva_coma = count($cliente_ids) == 2 ? '' : ',';
                //buscar nom cliente asociado
                require_once '../cliente/Cliente.class.php';
                $oCliente = new Cliente();

                for ($i=1; $i < count($cliente_ids); $i++) {
                    $cli_asoc_id = $cliente_ids[$i];

                    $res_cliente_asoc = $oCliente->mostrarUno($cli_asoc_id);
                    if ($res_cliente_asoc['estado'] == 1) {
                        $tipo_sociedad = $oCliente->verificar_sociedad_cliente($cliente_ids[0], $cliente_ids[$i])['data']['tb_clientedetalle_tip'];
                        if ($tipo_sociedad == 1) {
                            $tipo_sociedad_nom[$i] = 'su cónyugue';
                        } elseif ($tipo_sociedad == 2) {
                            $tipo_sociedad_nom[$i] = 'el copropietario';
                        }
                        $enlace_ult_cli_asoc2 = $i+1 == count($cliente_ids) ? "$lleva_coma y" : "$lleva_coma";

                        $cliente_info2 .= "$enlace_ult_cli_asoc2 {$tipo_sociedad_nom[$i]} <b>{$res_cliente_asoc['data']['tb_cliente_nom']}</b>";
                    }
                    unset($res_cliente_asoc);
                }
            }
        //
    } else {
        $credito_datos = $oCredito->mostrarUno($ejecucion_reg['tb_credito_id'])['data'];
        $cliente_nom = $ejecucion_reg['tb_cliente_emprs'];
        $cliente_repre_nom = $ejecucion_reg['tb_cliente_nom'];
        $cliente_cargo_emprs = $credito_datos['tb_cliente_empger'];

        $cliente_info2 = "<b>$cliente_nom</b>, debidamente representada por su ".customUcwords($cliente_cargo_emprs)." <b>$cliente_repre_nom</b>";
    }
//

$title = 'DOC_LEVANTAMIENTO_ORDEN_CAPTURA_'.str_replace(' ', '', $cliente_nom).''. date('YmdHis');
$fec_hoy = fechaActual(date('Y-m-d'));

//TCPDF
if ($tipo_documento == 'pdf') {
    class MYPDF extends TCPDF {
        public function Header() {
            //vacio pq no necesita header ni footer
        }
        public function Footer() {
            //vacio pq no necesita header ni footer
        }
    }
    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.prestamosdelnortechiclayo.com');
    $pdf->SetSubject('www.prestamosdelnortechiclayo.com');
    $pdf->SetKeywords('www.prestamosdelnortechiclayo.com');
    $pdf->SetTitle($title);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    //set margins
    $pdf->SetMargins(23, 26, 25); // left top right
    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // add a page
    $pdf->AddPage('P', 'A4');
}

$html =
'<table>'.
    //EXPEDIENTE, ESCPECIALISTA, SUMILLA
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Expediente N°</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">'.$exped.'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Esp. legal</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">'.customUcwords($esp_nom).'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Cuaderno</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">Principal</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Escrito N°</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: left;">'.$num_escrito.'</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:20%; text-align: left;"><b>Sumilla</b></td>'.
            '<td style="width:3%; text-align: left;">:</td>'.
            '<td style="width:39%; text-align: justify;">Solicitamos oficio de levantamiento de orden de captura.</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PRESENTACION DE EMPRESA Y CLIENTE
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify; font-size: 34px; line-height: 1.6;"><b>SEÑOR MAGISTRADO DEL '.$num_juzgado[$ejecucion_reg['tb_ejecucion_juzgado']].'° JUZGADO CIVIL SUB ESPECIALIDAD COMERCIAL DE CHICLAYO</b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:38%; text-align: left;"></td>'.
            '<td style="width:62%; text-align: justify; font-size: 33px; line-height: 1.6;">'.
                '<b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b>, en los seguidos contra '. $cliente_info2.', sobre INCAUTACIÓN DE BIEN MUEBLE, a Ud. con respeto digo:'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO I
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 33px; line-height: 1.6;">'.
                'Que, habiendo recuperado el vehículo materia del presente proceso, recurro a su despacho a fin de que tenga a bien '.
                'ordenar el levantamiento de la orden de captura para poder tener acceso a movilizar el vehículo sin contratiempos.'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //PARRAFO II
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 33px; line-height: 1.6;">'.
                '<b>Por lo expuesto:</b>'.
            '</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:6%; text-align: left;"></td>'.
            '<td style="width:94%; text-align: justify; text-indent: 143px; font-size: 33px; line-height: 1.6;">'.
                'A Ud. pido admitir el presente y proveer conforme a lo solicitado.'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //FINAL
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: right; font-size: 33px;"><b>Chiclayo, '.$fec_hoy.'</b>.</td>'.
        '</tr>'.
        '<br><br>'.
        '<tr>'.
            '<td style="width:32%; text-align: left;"></td>'.
            '<td style="width:68%;"><img src="../../public/pdf/ejecucion_varios/firma_obiol.jpg" width="230" height="160"></td>'.
        '</tr>'.
    //FINAL
'</table>'
;

if ($tipo_documento == 'word') {
    // Crear una instancia de PHPWord
    $phpWord = new PhpWord();

    $phpWord->setDefaultFontName('Arial');
    $phpWord->setDefaultFontSize(10);

    // Definir estilo de párrafo
    $paragraphStyle = array('spaceAfter' => 0, 'spacing' => 0);

    // Crear una nueva sección
    $section1 = $phpWord->addSection();

    // Renderizar el contenido HTML 1
    Html::addHtml($section1, $html, false, false, $paragraphStyle);

    // Guardar el documento
    $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save('doc_levantamiento_orden_captura.docx');
    echo 'Documento para cliente: ' . $cliente_nom . ', generado correctamente...';
    header('Location: ../ejecucionupload/doc_levantamiento_orden_captura.docx');
}

if ($tipo_documento == 'pdf') {
    $pdf->SetFont('Arial', '', 10);
    $pdf->writeHTML($html, true, 0, true, true);
    $pdf->Ln();
    // reset pointer to the last page
    $pdf->lastPage();
    //Close and output PDF document
    $nombre_archivo = $title . ".pdf";
    $pdf->Output($nombre_archivo, 'I');
}
