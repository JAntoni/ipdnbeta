<?php
//all require
    require_once '../../core/usuario_sesion.php';
    require_once '../funciones/fechas.php';
    require_once '../upload/Upload.class.php';
    $oUpload = new Upload();
    require_once '../historial/Historial.class.php';
    $oHistorial = new Historial();
    require_once '../ejecucionupload/Ejecucionfasefile.class.php';
    $oEjecucionfasefile = new Ejecucionfasefile();
    require_once '../filestorage/Filestorage.class.php';
    $oFilestorage = new Filestorage();
//
//datos estaticos
    $fecha_hoy = date('d-m-Y h:i a');
    $usuario_id = $_SESSION['usuario_id'];
    $usuario_nom = $_SESSION['usuario_nom'];
//
//datos $_POST
    $action = $_POST['action'];
    $credito_id = $_POST['credito_id'];
    $ejecucion_id = $_POST['ejecucion_id'];
    $ejecucionfase_id = $_POST['ejecucionfase_id'];
    $carpeta_nom = $_POST['modulo_nom'];
    $modulo_subnom = $_POST['modulo_subnom'];
    $filestorage_des = $_POST['filestorage_des'];
//

$nombre_documento = ''; //usable para historial
if ($carpeta_nom == 'ejecucion_liquidacion') {
    $nombre_documento = 'Cálculo de Liquidacion';
} elseif ($carpeta_nom == 'ejecucion_certificado_regveh') {
    $nombre_documento = 'Certificado Registral Vehicular';
} elseif ($carpeta_nom == 'ejecucion_asiento_ins_color') {
    $carpeta_nom = 'ejecucion_asiento_ins';
    $nombre_documento = 'Asiento de inscripción de vehiculo (cambio de color)';
} elseif ($carpeta_nom == 'ejecucion_asiento_ins_combu') {
    $carpeta_nom = 'ejecucion_asiento_ins';
    $nombre_documento = 'Asiento de inscripción de vehiculo (cambio de combustible)';
} elseif ($carpeta_nom == 'ejecucion_varios-medcau') {
    $carpeta_nom = 'ejecucion_varios';
    $nombre_documento = 'Gasto de Demanda (medida cautelar)';
} elseif ($carpeta_nom == 'ejecucion_varios-dernot') {
    $carpeta_nom = 'ejecucion_varios';
    $nombre_documento = 'Gasto de Demanda (derecho notificacion)';
} elseif ($carpeta_nom == 'ejecucion_varios-demscan') {
    $carpeta_nom = 'ejecucion_varios';
    $nombre_documento = 'Demanda escaneada';
} elseif ($carpeta_nom == 'ejecucion_varios-dilfue') {
    $carpeta_nom = 'ejecucion_varios';
    $nombre_documento = 'Gasto de Demanda (tasa por diligencia fuera de juzgado)';
} elseif ($carpeta_nom == 'ejecucion_varios-cartascan') {
    $carpeta_nom = 'ejecucion_varios';
    $nombre_documento = 'Carta notarial escaneada';
} else {
    $tabla_nom = $_POST['tabla_nom'];

    $nombre_documento = trim(strtoupper($_POST['nombre_doc']));
    $primera_letra = substr($nombre_documento, 0, 1);
    $nombre_documento = substr($nombre_documento, 1);
}

if ($action == 'insertar') { //actualmente configurado solo para pdf
    if (!empty($_FILES)) {
        $pdf_is = $_POST['pdf_is'];
        $oUpload->modulo_nom = $carpeta_nom;
        $oUpload->modulo_id = $ejecucion_id;

        $directorio = "public/pdf/$carpeta_nom/";
        $fileTypes = array('pdf'); // Allowed file extensions

        //datos $_FILES
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $fileParts = pathinfo($_FILES['Filedata']['name']);
        //
        $oUpload->tempFile = $tempFile; //temp servicio de PHP
        $oUpload->directorio = $directorio; //directorio de carpeta
        $oUpload->fileParts = $fileParts; //extensiones

        if($pdf_is == 'new'){//se debe hacer un insert
            if (in_array(strtolower($fileParts['extension']), $fileTypes)) { //inserta
                $oUpload->upload_uniq = null;
                $oUpload->usuario_id = $usuario_id;

                $return = $oUpload->insertar_doc();

                if($return['estado'] == 1 && (!in_array($carpeta_nom, ['ejecucion_certificado_regveh', 'ejecucion_asiento_ins']) && (strpos($nombre_documento, 'Gasto de Demanda') === false && (empty($primera_letra) || $primera_letra == '¿')))){ //registra historial
                    $oHistorial->setTbHistUsureg($usuario_id);
                    $oHistorial->setTbHistNomTabla('upload');
                    $oHistorial->setTbHistRegmodid($return['nuevo']);
                    $oHistorial->setTbHistDet("Ha subido el pdf $nombre_documento del crédito $credito_id en el proceso de ejecución legal id $ejecucion_id | <b>$fecha_hoy</b>");
                    $oHistorial->insertar();

                    $oHistorial->setTbHistNomTabla('tb_ejecucionfase');
                    $oHistorial->setTbHistRegmodid($ejecucionfase_id);
                    $oHistorial->insertar();

                    if ($nombre_documento == 'Demanda escaneada') {
                        require_once '../demanda/Demanda.class.php';
                        $oDemanda = new Demanda();

                        $demanda_reg = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '')['data'][0];
                        $oDemanda->modificar_campo($demanda_reg['tb_demanda_id'], 'tb_demanda_doc_scanned', $return['nuevo'], 'INT');
                    }
                    if ($nombre_documento == 'Carta notarial escaneada') {
                        require_once '../cartanotarial/Cartanotarial.class.php';
                        $oCarta = new Cartanotarial();

                        $carta_id = $_POST['tablaorig_id'];
                        $oCarta->modificar_campo($carta_id, 'tb_cartanotarial_doc_scanned', $return['nuevo'], 'INT', '');
                    }
                }

                if ($tabla_nom == 'tb_ejecucionfasefile') {
                    $id = $_POST['tablaorig_id'];
                    $oEjecucionfasefile->modificar_campo($id, 'upload_id', $return['nuevo'], 'INT');
                }

                echo $return['estado'].'|'.$return['nuevo'].'|'.$return['url'];
            } else {
                echo 'Tipo de archivo no válido.';
            }
        }
        else{ //mod1... se debe hacer una modificacion
            if (in_array(strtolower($fileParts['extension']), $fileTypes)){
                $oUpload->upload_id = $_POST['id_reg_mod'];

                $return = $oUpload->actualizar_file($pdf_is);
                if($return['estado']==1){
                    echo 2;
                    $oHistorial->setTbHistUsureg($usuario_id);
                    $oHistorial->setTbHistNomTabla('upload');
                    $oHistorial->setTbHistRegmodid($oUpload->upload_id);
                    $oHistorial->setTbHistDet("Ha actualizado el archivo pdf $nombre_documento del crédito $credito_id en el proceso de ejecución legal id $ejecucion_id | <b>$fecha_hoy</b>");
                    $oHistorial->insertar();

                    $oHistorial->setTbHistNomTabla('tb_ejecucionfase');
                    $oHistorial->setTbHistRegmodid($ejecucionfase_id);
                    $oHistorial->insertar();
                }
                else{
                    echo trim($return['mensaje']);
                }
            }
            else {
                // The file type wasn't allowed
                echo 'Tipo de archivo no válido.';
            }
        }
    }
    else {
        echo 'No hay ningún archivo para subir';
    }
}
elseif ($action == 'eliminar_pdf') {
    $upload_id = $_POST['upload_id'];

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el pdf.';

    if ($oUpload->eliminar_xac($upload_id)) {
        if (!in_array($carpeta_nom, ['ejecucion_certificado_regveh', 'ejecucion_asiento_ins', 'ejecucion_varios'])) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Pdf eliminado correctamente.';
            
            $ruta = $oUpload->mostrarUno($upload_id)['data']['upload_url'];
            $ruta = 'El archivo eliminado está <a href="'.$ruta.'" target="_blank">AQUI</a>';

            $oHistorial->setTbHistUsureg($usuario_id);
            $oHistorial->setTbHistNomTabla('upload');
            $oHistorial->setTbHistRegmodid($upload_id);
            $oHistorial->setTbHistDet("Ha eliminado el pdf $nombre_documento en el proceso de ejecución legal id $ejecucion_id. $ruta | <b>$fecha_hoy</b>");
            $oHistorial->insertar();

            $oHistorial->setTbHistNomTabla('tb_ejecucionfase');
            $oHistorial->setTbHistRegmodid($ejecucionfase_id);
            $oHistorial->setTbHistDet("Ha eliminado el pdf $nombre_documento en el proceso de ejecución legal id $ejecucion_id | <b>$fecha_hoy</b>");
            $oHistorial->insertar();
        } else {
            $data['mensaje'] = "Se cambió el xac a 0 (upload_id $upload_id).";

            $ruta = '';
            //OBTENER URL DE LA IMAGEN PARA ELIMINARLA POR COMPLETO DEL DIRECTORIO
            $result = $oUpload->mostrarUno($upload_id);
            if ($result['estado'] == 1) {
                $ruta = '../../' . $result['data']['upload_url'];
            }
            unset($result);

            if ($ruta != '') {
                // Verificar si la imagen existe
                if (file_exists($ruta)) {
                    // Intentar eliminar la imagen
                    if (unlink($ruta)) {
                        if ($oUpload->eliminar($upload_id)) {
                            $data['estado'] = 1;
                            $data['mensaje'] = 'Pdf "'.$nombre_documento.'" eliminado correctamente.';

                            if ($nombre_documento == 'Demanda escaneada') {
                                require_once '../demanda/Demanda.class.php';
                                $oDemanda = new Demanda();

                                $demanda_reg = $oDemanda->listar_todos('', $ejecucion_id, 1, '', '')['data'][0];
                                $oDemanda->modificar_campo($demanda_reg['tb_demanda_id'], 'tb_demanda_doc_scanned', null, 'INT');
                            }
                            if ($nombre_documento == 'Carta notarial escaneada') {
                                require_once '../cartanotarial/Cartanotarial.class.php';
                                $oCarta = new Cartanotarial();
        
                                $carta_id = $_POST['tablaorig_id'];
                                $oCarta->modificar_campo($carta_id, 'tb_cartanotarial_doc_scanned', null, 'INT', '');
                            }
                            if ($tabla_nom == 'tb_ejecucionfasefile') {
                                $id = $_POST['tablaorig_id'];
                                $oEjecucionfasefile->modificar_campo($id, 'upload_id', null, 'INT');
                            }
                        }
                    } else {
                        $data['mensaje'] .= ' Existe un error al eliminar el documento, se rechaza la eliminacion del pdf en el directorio. | upload_id: '. $upload_id;
                    }
                } else {
                    $data['mensaje'] .= " El pdf no existe en el directorio: $ruta | upload_id: ". $upload_id;
                }
            }
        }
    }

    echo json_encode($data);
}
elseif ($action == 'insertar_archivonom') {
    $nombre_archivo = strtoupper(trim($_POST['filenom']));
    $fechanoti_archivo = $_POST['file_fechanoti'];
    $tipodoc = $_POST['tipodoc'];
    $estado_tipo = $_POST['estado_tipo'];
    $genera_gasto = $_POST['genera_gasto'];
    $num_file = empty($_POST['num_file']) ? null : $_POST['num_file'];
    $cargo_fecingreso = !empty($_POST['cargo_fecingreso']) ? fecha_mysql($_POST['cargo_fecingreso']) : null;
    $incluir_redac = $_POST['incluir_redac'];

    $oEjecucionfasefile->archivonom = $nombre_archivo;
    $oEjecucionfasefile->tipodoc = $tipodoc;
    $oEjecucionfasefile->fecnoti = empty($fechanoti_archivo) ? null : fecha_mysql($fechanoti_archivo);
    $oEjecucionfasefile->resol_est = $tipodoc == 1 ? $estado_tipo : null;
    $oEjecucionfasefile->escrito_tipo = $tipodoc == 2 ? $estado_tipo : null;
    $oEjecucionfasefile->acta_tipo = $tipodoc == 6 ? $estado_tipo : null;
    $oEjecucionfasefile->ejecucionfase_id = $ejecucionfase_id;
    $oEjecucionfasefile->genera_gasto = $genera_gasto == 'true' ? 1 : 0;
    $oEjecucionfasefile->num = $num_file;
    $oEjecucionfasefile->oficiocargo_tipo = $tipodoc == 3 || $tipodoc == 5 ? $estado_tipo : null;
    $oEjecucionfasefile->cargo_fecingreso = $tipodoc == 5 ? $cargo_fecingreso : null;
    $oEjecucionfasefile->incluir_redac = $incluir_redac == 'true' ? 1 : 0;
    
    $result = $oEjecucionfasefile->insertar();

    /* GERSON (19-12-24) */
    // existe fecha de subida de acta de liberacion
    $fecini = null;
    $fecfin = null;
    if(!empty($fechanoti_archivo)){

        $cred_id = 0;
        $ejec_id = 0;

        $ejecucionfase = $oEjecucionfasefile->obtenerEjecucionFase($ejecucionfase_id);

        if($ejecucionfase['estado'] > 0){

            $cred_id = $ejecucionfase['data']['tb_credito_id'];
            $ejec_id = $ejecucionfase['data']['tb_ejecucion_id'];

            if($tipodoc == 6 && ($estado_tipo == 2 || $estado_tipo == 3)){ // si es acta de liberacion y tipo ENTREGA O DESCERRAJE

                $fecini = fecha_mysql($fechanoti_archivo);
                $hora_actual = date('H:i:s');
                $fecha_con_hora = date('Y-m-d H:i:s', strtotime("$fecini $hora_actual"));

                // Registrar ejecucionliberacion
                $liberacion = $oEjecucionfasefile->insertarLiberacion($fecha_con_hora, $usuario_id, $cred_id, $ejec_id, $fecini, $fecfin, 0.00, 0.00);

            }

        }
        
    }
    /*  */

    $data['estado'] = intval($result['estado']);
    $data['mensaje'] = $result['mensaje'];

    unset($result);

    echo json_encode($data);
}
elseif ($action == 'eliminar_archivonom') {
    $ejecucionfasefile_id = $_POST['ejecucionfasefile_id'];

    /* GERSON (19-12-24) */
    // antes de eliminar el acta, eliminar el registro de la tabla ejecucionliberacion
    $cred_id = 0;
    $ejec_id = 0;

    $ejecucionfasefile = $oEjecucionfasefile->mostrarUno($ejecucionfasefile_id);
    if($ejecucionfasefile['estado'] == 1){
        $ejecucionfase = $oEjecucionfasefile->obtenerEjecucionFase($ejecucionfasefile['data']['tb_ejecucionfase_id']);

        if($ejecucionfase['estado'] == 1){
            $cred_id = $ejecucionfase['data']['tb_credito_id'];
            $ejec_id = $ejecucionfase['data']['tb_ejecucion_id'];
    
            $ejecucionLiberacion = $oEjecucionfasefile->existeEjecucionLiberacion($ejec_id);
            if($ejecucionLiberacion['estado'] == 1){ // existe registro de ejecucion

                $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionLiberacion['data']['tb_ejecucionliberacion_id'], 'tb_ejecucionliberacion_xac', 0, 'INT');
                $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionLiberacion['data']['tb_ejecucionliberacion_id'], 'tb_ejecucionliberacion_fecmod', date('Y-m-d H:i:s'), 'STR');
                $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionLiberacion['data']['tb_ejecucionliberacion_id'], 'tb_ejecucionliberacion_usemod', $usuario_id, 'INT');
                $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionLiberacion['data']['tb_ejecucionliberacion_id'], 'tb_ejecucionliberacion_est', 3, 'INT');

            }

        }
    }
    /*  */

    $data['mensaje'] = "Error al eliminar el nombre de archivo";
    $oEjecucionfasefile->id = $ejecucionfasefile_id;
    $data['estado'] = intval($oEjecucionfasefile->eliminar());
    
    if ($data['estado'] == 1) {
        $data['mensaje'] = "Se eliminó el nombre de archivo";
    }
    echo json_encode($data);
}
elseif ($action == 'insertar_img') {
    try {
        if (!empty($_FILES)) {
            $oFilestorage->modulo_nom = $carpeta_nom;
            $oFilestorage->modulo_id = $ejecucion_id;
            $oFilestorage->modulo_subnom = $modulo_subnom;
    
            $directorio = "public/images/$carpeta_nom/";
            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions
    
            $nombreArchivo = $_FILES['Filedata']['name'];
    
            if (intval($_FILES['Filedata']['error']) == 0) {
                $tempFile = $_FILES['Filedata']['tmp_name'];
                $fileParts = pathinfo($nombreArchivo);
    
                // Validar la extensión del archivo
                if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
                    $oFilestorage->tempFile = $tempFile; //temp servicio de PHP
                    $oFilestorage->directorio = $directorio; //directorio de carpeta
                    $oFilestorage->fileParts = $fileParts; //extensiones
                    $oFilestorage->filestorage_des = $filestorage_des;
                    $oFilestorage->filestorage_uniq = null;
                    $oFilestorage->usuario_id = $usuario_id;
    
                    // Insertar documento
                    $return = $oFilestorage->insertar2();
                    echo $return['estado'];
                } else {
                    echo "Error: El archivo '$nombreArchivo' tiene una extensión no permitida.\n";
                }
            } else {
                // Manejar errores de subida
                switch ($_FILES['Filedata']['error']) {
                    case UPLOAD_ERR_FORM_SIZE:
                        echo "Error: El archivo '$nombreArchivo' excede el tamaño permitido.\n";
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        echo "Error: El archivo '$nombreArchivo' se subió parcialmente.\n";
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        echo "Error: No se subió ningún archivo.\n";
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        echo "Error: Falta el directorio temporal.\n";
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        echo "Error: No se pudo escribir el archivo en el disco.\n";
                        break;
                    case UPLOAD_ERR_EXTENSION:
                        echo "Error: Una extensión PHP detuvo la subida del archivo.\n";
                        break;
                    default:
                        echo "Error: Ocurrió un error desconocido al subir el archivo '$nombreArchivo'.\n";
                }
            }
        }
        else {
            echo 'No hay ningún archivo para subir';
        }
    } catch (Exception $th) {
        throw $th;
    }
    
}

/* GERSON (21-12-24) */
elseif ($action == 'modificar_campo_liberacion') {
    $tabla = $_POST['tabla'];
    $columna = $_POST['columna'];
    $valor = $_POST['valor'];
    $tipo_dato = $_POST['tipo_dato'];
    $ejecucionliberacion_id = $_POST['ejecucionliberacion_id'];
    
    $data['estado'] = 0;
    $data['mensaje'] = "Error al eliminar el nombre de archivo";

    $res = $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionliberacion_id, $columna, $valor, $tipo_dato);

    if ($res == 1) {

        $ejecucionLiberacion = $oEjecucionfasefile->mostrarUnoEjecucionLineracion($ejecucionliberacion_id);

        if($ejecucionLiberacion['estado'] == 1){
            $credito = $oEjecucionfasefile->mostrarCreditoXId($ejecucionLiberacion['data']['tb_credito_id']);
            $fecha = date('Y-m-d', strtotime($ejecucionLiberacion['data']['tb_ejecucionliberacion_fecreg']));


            // calcular el monto total de gastos vehiculares
            $monto_gasto = 0.00;
            if($credito['estado'] == 1){
                $cliente_id = $credito['data']['tb_cliente_id'] != null || $credito['data']['tb_cliente_id'] > 0 ? intval($credito['data']['tb_cliente_id']) : 0;
            
                $gastos = $oEjecucionfasefile->obtenerSumaGastos($cliente_id, $fecha, '');
                if($gastos['estado'] > 0){
                    $monto_gasto = floatval($gastos['data']['total_gasto']);
                }

                $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionliberacion_id, 'tb_ejecucionliberacion_monto_gasto', $monto_gasto, 'INT');

            }

            // calcular el monto total de cuotas 
            $monto_cuota = 0.00;
            $monto_cuota_g = 0.00;
            $monto_cuota_a = 0.00;

            if($credito['estado'] == 1){
                $cliente_id = $credito['data']['tb_cliente_id'] != null || $credito['data']['tb_cliente_id'] > 0 ? intval($credito['data']['tb_cliente_id']) : 0;
            
                $cuotas_garveh = $oEjecucionfasefile->mostrar_suma_pagos_garveh($cliente_id, $fecha, '');
                if($cuotas_garveh['estado'] > 0){
                    $monto_cuota_g = floatval($cuotas_garveh['data']['total_garveh']);
                }

                $cuotas_adenda = $oEjecucionfasefile->mostrar_suma_pagos_adenda($cliente_id, $fecha, '');
                if($cuotas_adenda['estado'] > 0){
                    $monto_cuota_a = floatval($cuotas_adenda['data']['total_adenda']);
                }

                $monto_cuota = $monto_cuota_g + $monto_cuota_a;

                $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionliberacion_id, 'tb_ejecucionliberacion_monto_cuota', $monto_cuota, 'INT');

            }
        }
        

        $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionliberacion_id, 'tb_ejecucionliberacion_fecmod', date('Y-m-d H:i:s'), 'STR');
        $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionliberacion_id, 'tb_ejecucionliberacion_usemod', $usuario_id, 'INT');
        $oEjecucionfasefile->modificar_campo_ejecucionliberacion($ejecucionliberacion_id, 'tb_ejecucionliberacion_est', 2, 'INT');

        $data['estado'] = 1;
        $data['mensaje'] = "Éxito en la operación";
    }

echo json_encode($data);
}

elseif ($action == 'registrar_ejecucionliberacion') {
$fecha = fecha_mysql($_POST['fecha']);
$hora_actual = date('H:i:s');
$fecha_con_hora = date('Y-m-d H:i:s', strtotime("$fecha $hora_actual"));
$credito_id = intval($_POST['credito_id']);
$origen = $_POST['origen'];

$data['estado'] = 0;
$data['mensaje'] = "Error al registrar la ejecución de liberación";

// se procede a crear el registro en la tabla 'tb_ejecucionliberacion' para calcular gastos
$res = $oEjecucionfasefile->insertarLiberacion($fecha_con_hora, $usuario_id, $credito_id, 0, $fecha, null, 0.00, 0.00);

if ($res['estado'] == 1) {
    $data['estado'] = 1;
    $data['mensaje'] = "Éxito en la operación";
}

echo json_encode($data);
}
/*  */

else {
    echo 'No se ha identificado ningún tipo de transacción para ' . $action;
}
