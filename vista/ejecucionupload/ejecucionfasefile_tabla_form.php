<?php
require_once '../../core/usuario_sesion.php';
$listo = 1;
$titulo = 'Listado de archivos';

if ($listo == 1) {
    $ejecucionfase_id = intval($_POST['ejecucionfase_id']);
    $action2 = $_POST['action2'];
    $ejecucionfase_completado = $_POST['concluido'];
    $estadoejecucion_id = $_POST['estadoejecucion_id'];
    $ejecucion_id = $_POST['ejecucion_id'];
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_listadoejecucionfasefiles_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="hdd_eff_ejecucionfase_id" id="hdd_eff_ejecucionfase_id" value="<?php echo $ejecucionfase_id; ?>">
                    <input type="hidden" name="hdd_eff_ejecucionfase_completado" id="hdd_eff_ejecucionfase_completado" value="<?php echo $ejecucionfase_completado; ?>">
                    <input type="hidden" name="hdd_eff_estadoejecucion_id" id="hdd_eff_estadoejecucion_id" value="<?php echo $estadoejecucion_id; ?>">

                    <?php if ($ejecucionfase_completado != 1 && $action2 == 'M_ejecucion') {
                        $datos = 'd1='.$ejecucion_id.'&d2='.$ejecucionfase_id;
                        ?>
                        <div class="row form-inline" style="display: flex;">
                            <div class="col-md-12" style="padding: 0px;">
                                <div class="form-group" style="padding-left: 12px; padding-right: 10px;">
                                    <label for="cbo_tipodoc_files" style="padding-right: 2px;">Tipo documento:</label>
                                    <select name="cbo_tipodoc_files" id="cbo_tipodoc_files" class="form-control input-sm input-shadow" style="padding: 6px 5px;">
                                        <option value="0">SELECCIONE</option>
                                        <option value="1">RESOLUCION</option>
                                        <option value="2">ESCRITO</option>
                                        <option value="3">OFICIO</option>
                                        <option value="4">CONSTANCIA PAGO</option>
                                        <option value="5">CARGO POLICIAL</option>
                                        <option value="6">ACTA</option>
                                        <option value="9">OTROS</option>
                                    </select>
                                </div>
                                <div id="div_num_files" class="form-group" style="display: none;">
                                    <div style="padding-right: 10px;">
                                        <label id="lbl_num_files" for="num_files" style="padding-right: 2px;"></label>
                                        <input type="text" name="num_files" id="num_files" placeholder="N°" class="form-control input-sm input-shadow num_file" size="2">
                                    </div>
                                </div>
                                <div id="div_estado_tipo" class="form-group" style="display: none;">
                                    <div style="padding-right: 10px;">
                                        <label for="estado_tipo_files" style="padding-right: 2px;">Estado/tipo:</label>
                                        <select name="estado_tipo_files" id="estado_tipo_files" class="form-control input-sm input-shadow" style="padding: 6px 5px;">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" style="padding-right: 10px;">
                                    <label for="txt_nombrefiles" style="padding-right: 2px;">Nombre Archivo:</label>
                                    <input type="text" name="txt_nombrefiles" id="txt_nombrefiles" placeholder="Ingrese Nombre para el archivo" class="form-control input-sm input-shadow mayus" size="40">
                                </div>
                                <div class="form-group" id="div_fecingreso" style="padding-right: 10px; display: none;">
                                    <label for="txt_cargo_fecingreso_files" style="padding-right: 2px;">Fecha ingreso:</label>
                                    <input type="text" name="txt_cargo_fecingreso_files" id="txt_cargo_fecingreso_files" placeholder="Fec. Ing. MP. Policial" class="form-control input-sm input-shadow" size="16">
                                </div>
                                <div class="form-group" id="div_fec" style="padding-right: 10px;">
                                    <label id="label_fecha_file" for="txt_fechanotif_files" style="padding-right: 2px;">Fecha:</label>
                                    <input type="text" name="txt_fechanotif_files" id="txt_fechanotif_files" placeholder="01-01-2024" class="form-control input-sm input-shadow" size="16">
                                </div>
                                <div id="div_cbo_generagasto" class="form-group" style="padding-top: 6px; padding-right: 10px; display: none;">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="che_genera_gasto" name="che_genera_gasto" class="flat-green" value="1"><b>&nbsp; Genera gasto</b>
                                        </label>
                                    </div>
                                </div>
                                <div id="div_che_incluir_redaccion" class="form-group" style="padding-top: 6px; padding-right: 10px; display: none;">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="che_incluir_redaccion" name="che_incluir_redaccion" class="flat-green" value="1"><b>&nbsp; Incluir en redacciones</b>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" style="padding-right: 0px;">
                                    <a type="button" title="Añadir nombre de documento" class="btn btn-primary btn-sm" onclick="nuevo_archivonom()"><i class="fa fa-plus"></i> Añadir doc</a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <?php if ($estadoejecucion_id == 14) { ?>
                                <div class="col-md-2">
                                    <div style="padding-right: 12px; margin-left: auto;">
                                        <a type="button" class="btn bg-navy btn-sm" target="_blank" href="vista/ejecucionupload/doc_levantamiento_orden_incau.php?<?php echo $datos; ?>&d3=pdf" style="font-weight: bold;">
                                            <i class="fa fa-file"></i>&nbsp; Esc. Lev. Orden Incautacion
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($estadoejecucion_id == 11 || $estadoejecucion_id == 13 || $estadoejecucion_id == 14) { ?>
                                <div class="col-md-1">
                                    <div style="padding-right: 12px; margin-left: auto;">
                                        <a type="button" class="btn bg-navy btn-sm" target="_blank" href="vista/demanda/doc_subsanacion.php?<?php echo $datos; ?>&d3=pdf" style="font-weight: bold;">
                                            <i class="fa fa-file"></i>&nbsp; Subsanación
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($estadoejecucion_id == 13) { ?>
                                <div class="col-md-1">
                                    <div style="padding-right: 12px; margin-left: auto;">
                                        <a type="button" class="btn bg-navy btn-sm" target="_blank" href="vista/ejecucionupload/doc_descerraje.php?<?php echo $datos; ?>&d3=pdf" style="font-weight: bold;">
                                            <i class="fa fa-file"></i>&nbsp; Descerraje
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div style="padding-right: 12px; margin-left: auto;">
                                        <a type="button" class="btn bg-navy btn-sm" target="_blank" href="vista/ejecucionupload/doc_desarchivamiento.php?<?php echo $datos; ?>&d3=pdf" style="font-weight: bold;">
                                            <i class="fa fa-file"></i>&nbsp; Desarchivamiento
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <br>
                    <?php } ?>
                    <div class="row">
                        <input type="hidden" id="hdd_datatable_ejecucionfasefiles_fil">

                        <div class="col-md-12">
                            <!-- tabla -->
                            <div id="div_html_tablaejecucionfasefiles" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/ejecucionupload/ejecucionfasefiles_tabla_form.js?ver='.rand(); ?>"></script>
