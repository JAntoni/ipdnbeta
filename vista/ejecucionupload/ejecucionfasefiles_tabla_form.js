var datatable_global_cartanotarial;

$(document).ready(function () {
    completar_tabla_files();

    $('#txt_fechanotif_files, #txt_cargo_fecingreso_files').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        endDate: new Date()
    });

    $('#cbo_tipodoc_files').change(function () {
        $('#div_fecingreso, #div_num_files, #div_che_incluir_redaccion').hide(150);
        $('#che_incluir_redaccion').iCheck('uncheck');
        $('#div_fec').show(150);
        if ($.inArray(parseInt($(this).val()), [1, 2, 3, 5, 6]) > -1) {
            $('#div_cbo_generagasto').hide(150);
            $('#che_genera_gasto').iCheck('uncheck');
            
            $('#div_estado_tipo').show(150);

            if (parseInt($(this).val()) == 6) {
                $('#num_files').val('');
                $('#estado_tipo_files').html(
                    '<option value="0">SELECCIONE</option><option value="1">INTERVENCION</option><option value="2">ENTREGA</option><option value="3">DESCERRAJE</option><option value="9">OTROS</option>'
                );
                $('#label_fecha_file').html('Fec. acta:');
            }
            else {//entra si es 1/2/3/5
                if (parseInt($(this).val()) == 1 || parseInt($(this).val()) == 2) {
                    parseInt($(this).val()) == 1 ? $('#lbl_num_files').html('N° de Resol:') : $('#lbl_num_files').html('N° de Escrito:');
                    $('#div_num_files').show(150);

                    $('.num_file').autoNumeric({
                        aSep: '',
                        aDec: ',',
                        vMin: '0',
                        vMax: '999'
                    });
                }

                if (parseInt($(this).val()) == 1) {
                    $('#estado_tipo_files').html(
                        '<option value="0">SELECCIONE</option><option value="1">ADMISIBLE</option><option value="2">INADMISIBLE</option>'
                    );
                    $('#label_fecha_file').html('Fec. notif:');
                }
                else if (parseInt($(this).val()) == 2) {
                    $('#estado_tipo_files').html(
                        '<option value="0">SELECCIONE</option><option value="1">SUBSANACION</option><option value="2">DESCERRAJE</option><option value="3">DESARCHIVAMIENTO</option><option value="4">LEVAN. ORD. INCAU.</option><option value="9">OTROS</option>'
                    );
                    $('#label_fecha_file').html('Fec. subida SINOE:');
                }
                else {
                    if (parseInt($(this).val()) == 5) {
                        $('#div_fec').hide(150);
                        $('#div_fecingreso').show(150);
                    } else {
                        $('#label_fecha_file').html('Fec. recojo:');
                    }
                    $('#estado_tipo_files').html(
                        '<option value="0">SELECCIONE</option><option value="1">INCAUTACION</option><option value="2">DESCERRAJE</option><option value="3">LEVANTAMIENTO ORDEN INCAU.</option>'
                    );
                }
            }
        }
        else {
            $('#num_files').val('');
            $('#div_estado_tipo').hide(150);
            $('#estado_tipo_files').html('');

            if (parseInt($(this).val()) == 4) {
                $('#div_cbo_generagasto').show(150);
                $('#label_fecha_file').html('Fec. pago:');
            } else {
                $('#div_cbo_generagasto').hide(150);
                $('#che_genera_gasto').iCheck('uncheck');
                $('#label_fecha_file').html('Fecha:');

                if (parseInt($(this).val()) == 9 && parseInt($('#hdd_eff_estadoejecucion_id').val()) == 2) {
                    $('#div_che_incluir_redaccion').show(150);

                    $('#lbl_num_files').html('N°:');
                    $('#div_num_files').show(150);

                    $('.num_file').autoNumeric({
                        aSep: '',
                        aDec: ',',
                        vMin: '0',
                        vMax: '999'
                    });
                }
            }
        }
    });

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: `icheckbox_flat-green`,
        radioClass: `iradio_flat-green`
    });

    if (parseInt($('#hdd_eff_estadoejecucion_id').val()) == 2) {
        $('#cbo_tipodoc_files option[value!=0][value!=9]').hide();
    }
    if (parseInt($('#hdd_eff_estadoejecucion_id').val()) == 11) {
        $('#cbo_tipodoc_files option[value=3], #cbo_tipodoc_files option[value=5], #cbo_tipodoc_files option[value=6]').hide();
    }
    if (parseInt($('#hdd_eff_estadoejecucion_id').val()) == 12) {
        $('#cbo_tipodoc_files option[value=1], #cbo_tipodoc_files option[value=2], #cbo_tipodoc_files option[value=4], #cbo_tipodoc_files option[value=6], #cbo_tipodoc_files option[value=9]').hide();
    }
});

function completar_tabla_files() {
    var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
    $.ajax({
        type: "POST",
        url: VISTA_URL + "ejecucionupload/ejecucionfasefiles_tabla_form_vista.php",
        async: true,
        dataType: "html",
        data: ({
            ejecucion_id: $('#ejecucion_id').val(),
            ejecucionfase_id: $('#hdd_eff_ejecucionfase_id').val(),
            ejecucionfase_completado: $('#hdd_eff_ejecucionfase_completado').val(),
            estadoejecucion_id: $('#hdd_eff_estadoejecucion_id').val(),
            action2: $('#hdd_ejecucion_action2').val(),
            credito_id: ejecucion_reg.tb_credito_id
        }),
        success: function(data) {
            $('#div_html_tablaejecucionfasefiles').html(data);
            setTimeout(function () {
                estilos_datatable_files();
                $('.fec_ingreso').datepicker({
                    language: 'es',
                    autoclose: true,
                    format: "dd-mm-yyyy",
                    endDate: new Date()
                });
                $('#tbl_ejecucionfasefiles').find('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
                    checkboxClass: `icheckbox_flat-green`,
                    radioClass: `iradio_flat-green`
                });
                on_check_modificar_incluir_redac();
                if ($('#hdd_ejecucion_action2').val() == 'L_ejecucion') {
                    disabled($('#tbl_ejecucionfasefiles').find('input[type="checkbox"].flat-green, input[type="radio"].flat-green'));
                }
            }, 300);

            if ($('#hdd_eff_estadoejecucion_id').val() == 11 || $('#hdd_eff_estadoejecucion_id').val() == 13) {
                verificar_conteo_dias($('#ejecucion_id').val(), $('#hdd_eff_ejecucionfase_id').val(), $('#hdd_eff_estadoejecucion_id').val());
            }
        }
    });
}

function estilos_datatable_files() {
    datatable_global_cartanotarial = $('#tbl_ejecucionfasefiles').DataTable({
        "pageLength": 25,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [], orderable: false}
        ]
    });
    datatable_texto_filtrar_files();
}

function datatable_texto_filtrar_files() {
    $('input[aria-controls*="tbl_ejecucionfasefiles"]')
    .attr('id', 'txt_datatable_ejecucionfasefiles_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_ejecucionfasefiles_fil').keyup(function (event) {
        $('#hdd_datatable_ejecucionfasefiles_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_ejecucionfasefiles_fil').val();
    if (text_fil) {
        $('#txt_datatable_ejecucionfasefiles_fil').val(text_fil);
        datatable_global_cartanotarial.search(text_fil).draw();
    }
};

function nuevo_archivonom() {
    var tipodoc = parseInt($('#cbo_tipodoc_files option:selected').val());
    var estado_tipo = parseInt($('#estado_tipo_files option:selected').val());
    var file_nombre = $('#txt_nombrefiles').val();
    var fecha_notif = $('#txt_fechanotif_files').val();
    var num_file = $('#num_files').val();
    var cargo_fecingreso = $('#txt_cargo_fecingreso_files').val();
    
    if (tipodoc < 1) {
        alerta_error('SISTEMA', 'SELECCIONE UN TIPO DE DOCUMENTO');
        return;
    } else if ($.inArray(tipodoc, [1, 2, 3, 5, 6]) > -1) {
        if (estado_tipo < 1) {
            alerta_error('SISTEMA', 'SELECCIONE EL ESTADO/TIPO DE '+$('#cbo_tipodoc_files option:selected').text());
            return;
        }
        if ((tipodoc == 1 || tipodoc == 2 || tipodoc == 6) && !fecha_notif) {
            alerta_error('SISTEMA', 'FECHA OBLIGATORIO');
            return;
        }
        if ((tipodoc == 1 || (tipodoc == 2 && estado_tipo != 3)) && !num_file) {
            alerta_error('SISTEMA', 'INGRESE NUMERO DE '+$('#cbo_tipodoc_files option:selected').text());
            return;
        }
    }
    if (!file_nombre) {
        alerta_error('SISTEMA', 'INGRESE UN NOMBRE PARA EL NUEVO ARCHIVO');
        return;
    }
    
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'ejecucionupload/ejecucionupload_controller.php',
        async: true,
        dataType: 'json',
        data: ({
            action: 'insertar_archivonom',
            filenom: file_nombre,
            file_fechanoti: fecha_notif,
            ejecucionfase_id: $('#hdd_eff_ejecucionfase_id').val(),
            tipodoc: tipodoc,
            estado_tipo: estado_tipo,
            genera_gasto: $('#che_genera_gasto').prop('checked'),
            num_file: num_file,
            cargo_fecingreso: cargo_fecingreso,
            incluir_redac: $('#che_incluir_redaccion').prop('checked')
        }),
        success: function (data) {
            if (data.estado == 1) {
                notificacion_success('Nombre de archivo añadido', 3000);
                completar_tabla_files();
                $('#txt_nombrefiles').val('');
                $('#txt_fechanotif_files').val('');
                $('#cbo_tipodoc_files').val(0).change();
            } else {       
                console.log(data);         
                notificacion_warning('Error al añadir el nombre de archivo', 3000);
            }
        },
        complete: function (data) {
            console.log(data);
        }
    });
}

function eliminar_archivonom(ejecucionfasefile_id, upload_id, gastopago_id) {
    if (upload_id > 0) {
        alerta_error('SISTEMA', 'DEBE ELIMINAR PRIMERO EL ARCHIVO PDF');
        return;
    }
    if (gastopago_id > 0) {
        alerta_error('SISTEMA', 'DEBE ELIMINAR PRIMERO EL PAGO DEL GASTO');
        return;
    }
    $.confirm({
		title: 'Sistema',
		content: `<b>¿Confirma que desea eliminar este nombre de archivo?</b>`,
		type: 'red',
		escapeKey: 'close',
		backgroundDismiss: true,
		columnClass: 'small',
		buttons: {
			Confirmar: {
				btnClass: 'btn-red',
				action: function() {
					$.ajax({
                        type: 'POST',
                        url: VISTA_URL + 'ejecucionupload/ejecucionupload_controller.php',
                        async: true,
                        dataType: 'json',
                        data: ({
                            action: 'eliminar_archivonom',
                            ejecucionfasefile_id: ejecucionfasefile_id
                        }),
                        success: function (data) {
                            if (data.estado == 1) {
                                notificacion_success(data.mensaje, 3000);
                                completar_tabla_files();
                            } else {
                                notificacion_warning(data.mensaje, 3000);
                            }
                        }
                    });
				}
			},
			cancelar: function () {
			}
		}
	});
}

function actualizar_fec_ingreso(ejecucionfasefile_id, ejecucionfase_id) {
    var valor = $('#fec_ingreso_file_'+ejecucionfasefile_id).val();
    if (!valor) {
        alerta_error('Sistema', 'Complete la fecha de ingreso a MP Pol.');
        $('#fec_ingreso_file_'+ejecucionfasefile_id).addClass('req');
        return;
    }
    $.confirm({
		title: 'Sistema',
		content: `<b>¿Confirma que desea guardar la fecha de ingreso a MP policial?</b>`,
		type: 'orange',
		escapeKey: 'close',
		backgroundDismiss: true,
		columnClass: 'small',
		buttons: {
			Confirmar: {
				btnClass: 'btn-orange',
				action: function() {
                    var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
                    var form_serializado = 'action=modificar_campo&tabla=tb_ejecucionfasefile&tabla_id='+ejecucionfasefile_id+'&columna=tb_ejecucionfasefile_cargo_fecingreso&valor='+valor+'&tipo_dato=STR&ejecucion_id='+ejecucion_reg.tb_ejecucion_id+'&ejecucionfase_id='+ejecucionfase_id;
                    modificar_campo('ejecucionfasefile_fec_ingreso', form_serializado, ejecucionfase_id);
				}
			},
			cancelar: function () {
			}
		}
	});
}

function on_check_modificar_incluir_redac() {
    $('#tbl_ejecucionfasefiles').find("input[id*=che_incluido_redaccion_]").on('ifChanged', function () {
        var tabla_id = this.id.split('_')[3];
        
        var ejecucion_reg = JSON.parse($('#hdd_ejecucion_reg').val());
        var ejecucionfase_id = $('#hdd_eff_ejecucionfase_id').val();
        var form_serializado = `action=modificar_campo&tabla=tb_ejecucionfasefile&columna=tb_ejecucionfasefile_incluir_redac`+
            `&valor=${this.checked}&tipo_dato=INT&ejecucion_id=${ejecucion_reg.tb_ejecucion_id}&ejecucionfase_id=${ejecucionfase_id}&tabla_id=`+tabla_id;
        modificar_campo('ejecucionfasefile_incluir_redaccion', form_serializado, ejecucionfase_id, 'M_ejecucion');
    });
}