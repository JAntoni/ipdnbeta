<?php
require_once '../funciones/fechas.php';
require_once '../ejecucionupload/Ejecucionfasefile.class.php';
$oEjecucionfasefile = new Ejecucionfasefile();

$lista = '';
$ejecucionfase_id = $_POST['ejecucionfase_id'];
$ejecucionfase_completado = $_POST['ejecucionfase_completado'];
$action2 = $_POST['action2'];
$estadoejecucion_id = $_POST['estadoejecucion_id'];
$ejecucion_id = $_POST['ejecucion_id'];
$credito_id = $_POST['credito_id'];

$res = $oEjecucionfasefile->listar('', $ejecucionfase_id, '', '', '', '', '', '');

if ($res['estado'] == 1) {
    foreach ($res['data'] as $key => $file) {
        $btn_elim = $btn_pdf = $estado_tipo = $btn_gasto = $btn_guardar_fec_ingreso = '';
        $tipo_doc_nom = [1 => 'RESOLUCION', 2 => 'ESCRITO', 3 => 'OFICIO', 4 => 'CONSTANCIA PAGO', 5 => 'CARGO POLICIAL', 6 => 'ACTA', 9 => 'OTROS'];
        $resol_est_nom = [1 => 'ADMISIBLE', 2 => 'INADMISIBLE'];
        $escrito_tipo_nom = [1 => 'SUBSANACION',2 => 'DESCERRAJE', 3 => 'DESARCHIVAMIENTO', 4 => 'LEVAN. ORD. INCAU.', 9 => 'OTROS'];
        $oficiocargo_tipo_nom = [1 => 'INCAUTACION',2 => 'DESCERRAJE', 3 => 'LEVANTAMIENTO ORDEN INCAU.'];
        $acta_tipo_nom = [1 => 'INTERVENCION',2 => 'ENTREGA', 3 => 'DESCERRAJE', 9 => 'OTROS'];
        if ($file['tb_ejecucionfasefile_tipodoc'] == 1) {
            $estado_tipo = $resol_est_nom[intval($file['tb_ejecucionfasefile_resol_est'])];
            $tipo_fecha = '(notif)';
        } elseif ($file['tb_ejecucionfasefile_tipodoc'] == 2) {
            $estado_tipo = $escrito_tipo_nom[intval($file['tb_ejecucionfasefile_escrito_tipo'])];
            $tipo_fecha = '(subida SINOE)';
        } elseif ($file['tb_ejecucionfasefile_tipodoc'] == 6) {
            $estado_tipo = $acta_tipo_nom[intval($file['tb_ejecucionfasefile_acta_tipo'])];
            $tipo_fecha = '(subida acta)';
        }
        elseif ($file['tb_ejecucionfasefile_tipodoc'] == 3) {
            $estado_tipo = $oficiocargo_tipo_nom[intval($file['tb_ejecucionfasefile_oficiocargo_tipo'])];
            $tipo_fecha = '(recojo)';
        }
        elseif ($file['tb_ejecucionfasefile_tipodoc'] == 4) {
            $tipo_fecha = '(pago)';
        }
        elseif ($file['tb_ejecucionfasefile_tipodoc'] == 5) {
            $estado_tipo = $oficiocargo_tipo_nom[intval($file['tb_ejecucionfasefile_oficiocargo_tipo'])];
        }

        $num_file = '';
        if ($file['tb_ejecucionfasefile_tipodoc'] == 1 || ($file['tb_ejecucionfasefile_tipodoc'] == 2 && $file['tb_ejecucionfasefile_escrito_tipo'] != 3) || ($file['tb_ejecucionfasefile_tipodoc'] == 9 && !empty($file['tb_ejecucionfasefile_num']))) {
            $num_file = ' N° '. str_pad($file['tb_ejecucionfasefile_num'], 2, '0', STR_PAD_LEFT);
        }

        $btn_pdf = '<button class="btn btn-primary btn-xs" title="Archivo" onclick="'."verejecucion_pdf_form({$file['upload_id']}, 'ejecucion_varios', $ejecucionfase_id, 0, {$file['tb_ejecucionfasefile_id']}, 'tb_ejecucionfasefile', '?{$file['tb_ejecucionfasefile_archivonom']}')".'"><i class="fa fa-file"></i> Ver</button>';
        
        if ($action2 == 'M_ejecucion' && $ejecucionfase_completado == 0) {
            $btn_elim = '<button class="btn btn-danger btn-xs" title="Eliminar" onclick="eliminar_archivonom('.$file['tb_ejecucionfasefile_id'].', '.intval($file['upload_id']).', '.intval($file['tb_gastopago_id']).')"><i class="fa fa-trash"></i> Eliminar</button>';

            if (empty($file['upload_id'])) {
                $btn_pdf = '<button class="btn btn-warning btn-xs" title="Subir Archivo" onclick="'."ejecucionpdf_form($ejecucion_id, $ejecucionfase_id, $credito_id, 'new', 'ejecucion_varios', '$action2', {$file['tb_ejecucionfasefile_id']}, '?{$file['tb_ejecucionfasefile_archivonom']}', 'tb_ejecucionfasefile')".'"><i class="fa fa-upload"></i> Subir</button>';
            }
        } else {
            $btn_pdf = empty($file['upload_id']) ? '<button class="btn btn-warning btn-xs disabled" title="Subir Archivo" onclick=""><i class="fa fa-upload"></i> Subir</button>' :
                        '<button class="btn btn-primary btn-xs" title="Archivo" onclick="'."verejecucion_pdf_form({$file['upload_id']}, 'ejecucion_varios', $ejecucionfase_id, 1, {$file['tb_ejecucionfasefile_id']}, 'tb_ejecucionfasefile', '?{$file['tb_ejecucionfasefile_archivonom']}')".'"><i class="fa fa-file"></i> Ver</button>';
            //
        }
        $fecha = empty($file['tb_ejecucionfasefile_fecnoti']) ? '' : fecha_mysql($file['tb_ejecucionfasefile_fecnoti']).' '.$tipo_fecha;
        if ($file['tb_ejecucionfasefile_tipodoc'] == 5) {
            $fecha = empty($file['tb_ejecucionfasefile_cargo_fecingreso']) ? '<input type="text" name="fec_ingreso_file_'.$file['tb_ejecucionfasefile_id'].'" id="fec_ingreso_file_'.$file['tb_ejecucionfasefile_id'].'" class="form-control input-shadow input-sm fec_ingreso" placeholder="(falta fec. ingreso MP Pol.)" style="height: 25px;">' : fecha_mysql($file['tb_ejecucionfasefile_cargo_fecingreso']).' (ingreso MP Pol.)';
            if (empty($file['tb_ejecucionfasefile_cargo_fecingreso']) && $action2 == 'M_ejecucion') {
                $btn_guardar_fec_ingreso = '<a class="btn btn-xs btn-primary" onclick="actualizar_fec_ingreso('.$file['tb_ejecucionfasefile_id'].', '.$ejecucionfase_id.')"><i class="fa fa-save"></i></a>';
            }
        }

        //espacio para los gastos
            if (!empty($file['tb_gastopago_id'])) {
                $btn_gasto = '<a class="btn bg-olive btn-xs" title="Ver pago" onclick="pagar(\'L\', '.$file['tb_gasto_idfk'].', '.$file['tb_gastopago_id'].', \'ejecucionfasefile\', '.$credito_id.', '.$ejecucion_id.', '.$ejecucionfase_id.')"><i class="fa fa-money"></i> Ver pago</a>';
            } elseif ($file['tb_ejecucionfasefile_genera_gasto'] == 1) {
                $disabled_btn_pago = $action2 == 'M_ejecucion' && $ejecucionfase_completado == 0 ? '' : 'disabled';
                //btn pagar
                $btn_gasto = '<a class="btn btn-warning btn-xs '.$disabled_btn_pago.'" title="Registrar pago" onclick="pagar(\'I\', 0, 0, \'ejecucionfasefile\', '.$credito_id.', '.$ejecucion_id.', '.$ejecucionfase_id.', \''.$file['tb_ejecucionfasefile_archivonom'].'\', '.$file['tb_ejecucionfasefile_id'].')"><i class="fa fa-money"></i> Pagar</a>';
            }
        //

        $column_opcional = '';
        if ($estadoejecucion_id == 2) { //si viene de redaccion de cartas notariales, mostrar check de incluir en redacciones de pdf
            $incluido_redac = $file['tb_ejecucionfasefile_incluir_redac'] == 1 ? 'checked' : '';
            $column_opcional = '<td id="tabla_fila">'.
                '<input type="checkbox" id="che_incluido_redaccion_'.$file['tb_ejecucionfasefile_id'].'" name="che_incluido_redaccion_'.$file['tb_ejecucionfasefile_id'].'" class="flat-green" value="1" '.$incluido_redac.'>'.
            '</td>';
        }

        $lista .=
            '<tr id="tabla_cabecera_fila">'.
                '<td id="tabla_fila">'.$file['tb_ejecucionfasefile_id'].'</td>'.
                '<td id="tabla_fila">'.$tipo_doc_nom[$file['tb_ejecucionfasefile_tipodoc']].$num_file.'</td>'.
                '<td id="tabla_fila">'.$file['tb_ejecucionfasefile_archivonom'].'</td>'.
                '<td id="tabla_fila">'.$estado_tipo.'</td>'.
                '<td id="tabla_fila">'.$fecha.'</td>'.
                '<td id="tabla_fila">'.$file['upload_reg'].'</td>'.
                '<td id="tabla_fila" align="left">'.$file['upload_id'].'</td>'.
                $column_opcional.
                '<td id="tabla_fila">'.
                    $btn_guardar_fec_ingreso.' '.$btn_gasto.' '.$btn_pdf.' '.$btn_elim.
                '</td>'.
            '</tr>';
        //
    }
}
unset($res);
?>
<table id="tbl_ejecucionfasefiles" class="table table-hover" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" style="width: 5%;">Id</th>
            <th id="tabla_cabecera_fila" style="width: 10%;">Tipo doc - N°</th>
            <th id="tabla_cabecera_fila" style="width: 28%;">Nombre Archivo</th>
            <th id="tabla_cabecera_fila" style="width: 10%;">Estado/tipo</th>
            <th id="tabla_cabecera_fila" style="width: 10%;">Fecha</th>
            <th id="tabla_cabecera_fila" style="width: 10%;">Fec. carga</th>
            <th id="tabla_cabecera_fila" style="width: 7%;">Upload id</th>
            <?php if ($estadoejecucion_id == 2) { ?>
                <th id="tabla_cabecera_fila">Incluir redacc</th>
            <?php } ?>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $lista;?>
    </tbody>
</table>
