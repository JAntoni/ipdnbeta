<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }

  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'retencion/Retencion.class.php');
    require_once(VISTA_URL.'funciones/funciones.php');
    require_once(VISTA_URL.'funciones/fechas.php');
  }
  else{
    require_once('../retencion/Retencion.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
  }
  
  $oRetencion = new Retencion();

  $cliente_id = $_POST['hdd_fil_cliente_id'];
  $fecha1 = (isset($_POST['txt_filtro_fec1']))? fecha_mysql($_POST['txt_filtro_fec1']) : date('Y-01-01');
  $fecha2 = (isset($_POST['txt_filtro_fec2']))? fecha_mysql($_POST['txt_filtro_fec2']) : date('Y-m-d');

  $result = $oRetencion->retencion_tabla($cliente_id, $fecha1, $fecha2);
  $bandera = intval($result['estado']);
?>
<?php if($bandera == 1): ?>
  <table id="tbl_retencions" class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>N°</th>
        <th>FECHA</th>
        <th>CLIENTE</th>
        <th>PLACA</th>
        <th>CREDITO ID</th>
        <th>MOTIVO</th>
        <th width="15%">&nbsp;</th>
      </tr>
    </thead>
    <tbody> <?php
      foreach ($result['data'] as $key => $value) {
          $credito_id = 'CAV-'.$value['tb_credito_id'];
          if($value['tipo'] == 3)
            $credito_id = 'CGV-'.$value['tb_credito_id'];
        ?>
        <tr>
          <td><?php echo $value['tb_retencion_id'];?></td>
          <td><?php echo mostrar_fecha($value['tb_retencion_reg']);?></td>
          <td><?php echo $value['tb_cliente_nom'];?></td>
          <td><?php echo $value['tb_retencion_pla'];?></td>
          <td><?php echo $credito_id;?></td>
          <td>
            <?php 
              if($value['tb_retencion_mot'] == 1)
                echo 'Por deuda';
              if($value['tb_retencion_mot'] == 2)
                echo 'Entrega voluntaria';
              if($value['tb_retencion_mot'] == 3)
                echo 'Otros motivos';
            ?>
          </td>
          <td width="150" align="center" valign="middle">
            <a class="btn btn-info" href="#" onClick="retencion_form('M',<?php echo $value['tb_retencion_id']?>)">Editar</a>
            <a class="btn btn-danger" href="#" onClick="retencion_form('E',<?php echo $value['tb_retencion_id']?>)">Eliminar</a>
          </td>
        </tr> <?php
      }
      $result = null; ?>
    </tbody>
  </table>
<?php endif; ?>

<?php if($bandera != 1): ?>
  <table id="tbl_retencions" class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>N°</th>
        <th>FECHA</th>
        <th>CLIENTE</th>
        <th>PLACA</th>
        <th>CREDITO ID</th>
        <th>MOTIVO</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <?php echo $result['mensaje'];?>
    </tbody>
  </table>
<?php endif; ?>