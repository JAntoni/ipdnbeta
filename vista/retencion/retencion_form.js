function datos_credito_cliente(cliente_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"retencion/retencion_controller.php",
		async:true,
		dataType: "JSON",                      
		data: ({
			action: 'credito_cliente',
			credito_tip: $('#cmb_credito_tip').val(),
			cliente_id: cliente_id
		}),
		beforeSend: function() {
			$('#retencion_mensaje').hide(300);
    },
		success: function(data){
			if(parseInt(data.credito_id) > 0){
				$('#txt_credito_id').val(data.credito_id);
				$('#txt_vehiculo_pla').val(data.vehiculo_pla);
				$('#txt_vehtip_nom').val(data.vehiculotipo_nom);
			}
			else{
				$('#retencion_mensaje').show(300);
				$('#retencion_mensaje').html('No se ha encontrado un CREDITO para el cliente')
			}
		},
		complete: function(data){
		}
	});
}

$(document).ready(function() {
	
  $( "#txt_cliente_nom" ).autocomplete({
    minLength: 1,
    source: VISTA_URL+"cliente/cliente_autocomplete.php",
    select: function(event, ui){      
      $("#hdd_cliente_id").val(ui.item.cliente_id);
      datos_credito_cliente(ui.item.cliente_id)
    }
  });

  $("#form_retencion").validate({
		submitHandler: function() {
			$.ajax({
				type: "POST",
				url: VISTA_URL+"retencion/retencion_controller.php",
				async:true,
				dataType: "json",
				data: $('#form_retencion').serialize(),
				beforeSend: function() {
					$('#retencion_mensaje').html("Guardando...");
					$('#retencion_mensaje').show(100);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#retencion_mensaje').show(100);
						$('#retencion_mensaje').html(data.mensaje);
						retencion_tabla();
						$("#modal_registro_retencion" ).modal( "hide" );
					}				
				},
				complete: function(data){
					if(data.statusText != "success"){
						$('#retencion_mensaje').show(100);
						$('#retencion_mensaje').html(data.responseText);
					}
				}
			});
		},
		rules: {
			txt_cliente_nom: {
				required: true
			},
			hdd_cliente_id: {
				required: true
			},
			cmb_retencion_mot: {
				required: true
			},
			txt_retencion_det: {
				required: true
			}
		},
		messages: {
			txt_cliente_nom: {
				required: 'Busque un cliente'
			},
			hdd_cliente_id: {
				required: 'Seleccione un cliente'
			},
			cmb_retencion_mot: {
				required: 'Seleccione el motivo'
			},
			txt_retencion_det: {
				required: 'Ingrese el detalle de la retención'
			}
		}
	});
});