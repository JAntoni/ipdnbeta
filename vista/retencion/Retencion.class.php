<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Retencion extends Conexion{

    function insertar($retencion_cretip, $cliente_id, $credito_id, $retencion_pla, $retencion_mot, $retencion_deu, $retencion_kil, $retencion_det){
      $this->dblink->beginTransaction();
      try {
        $sql = "
          INSERT INTO tb_retencion(
            tb_retencion_cretip, tb_cliente_id, tb_credito_id, 
            tb_retencion_pla, tb_retencion_mot, tb_retencion_deu, 
            tb_retencion_kil, tb_retencion_det, tb_retencion_fec) 
          VALUES (
            :retencion_cretip, :cliente_id, :credito_id, 
            :retencion_pla, :retencion_mot, :retencion_deu, 
            :retencion_kil, :retencion_det, NOW())
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":retencion_cretip", $retencion_cretip, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":retencion_pla", $retencion_pla, PDO::PARAM_STR);
        $sentencia->bindParam(":retencion_mot", $retencion_mot, PDO::PARAM_INT);
        $sentencia->bindParam(":retencion_deu", $retencion_deu, PDO::PARAM_STR);
        $sentencia->bindParam(":retencion_kil", $retencion_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":retencion_det", $retencion_det, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($retencion_id, $retencion_cretip, $cliente_id, $credito_id, $retencion_pla, $retencion_mot, $retencion_deu, $retencion_kil, $retencion_det){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_retencion SET 
          tb_retencion_cretip = :retencion_cretip, tb_cliente_id = :cliente_id, 
          tb_credito_id = :credito_id, tb_retencion_pla = :retencion_pla, 
          tb_retencion_mot = :retencion_mot, tb_retencion_deu = :retencion_deu, 
          tb_retencion_kil = :retencion_kil, tb_retencion_det = :retencion_det
          WHERE tb_retencion_id =:retencion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":retencion_id", $retencion_id, PDO::PARAM_INT);
        $sentencia->bindParam(":retencion_cretip", $retencion_cretip, PDO::PARAM_INT);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":retencion_pla", $retencion_pla, PDO::PARAM_STR);
        $sentencia->bindParam(":retencion_mot", $retencion_mot, PDO::PARAM_INT);
        $sentencia->bindParam(":retencion_deu", $retencion_deu, PDO::PARAM_STR);
        $sentencia->bindParam(":retencion_kil", $retencion_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":retencion_det", $retencion_det, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($retencion_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_retencion WHERE tb_retencion_id =:retencion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":retencion_id", $retencion_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($retencion_id){
      try {
        $sql = "SELECT * FROM tb_retencion WHERE tb_retencion_id =:retencion_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":retencion_id", $retencion_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrar_datos_retencion_credito($credito_id, $credito_tip, $tipo){
      try {
        $sql = "SELECT * FROM tb_retencion 
                where tb_credito_id =:credito_id and tb_creditotipo_id =:credito_tip and tb_retencion_tip =:tipo order by tb_retencion_id desc limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_tip", $credito_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function retencion_tabla($cliente_id, $fecha1, $fecha2){
      try {
        $sql = "SELECT * FROM tb_retencion re INNER JOIN tb_cliente cli on cli.tb_cliente_id = re.tb_cliente_id WHERE tb_retencion_fec between :fecha1 and :fecha2";
        if(intval($cliente_id) > 0)
          $sql .= " and re.tb_cliente_id =:cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        if(intval($cliente_id) > 0)
          $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    function datos_credito_cliente($tabla, $cliente_id){
      try {
        $tipo = 'tb_credito_tip1';
        if($tabla == 'tb_creditogarveh')
          $tipo = 'tb_credito_tip';
        $sql = "SELECT * 
          FROM $tabla c
          INNER JOIN tb_cliente cli ON c.tb_cliente_id = cli.tb_cliente_id
          LEFT JOIN tb_vehiculomarca vm ON c.tb_vehiculomarca_id=vm.tb_vehiculomarca_id
          LEFT JOIN tb_vehiculomodelo vd ON c.tb_vehiculomodelo_id=vd.tb_vehiculomodelo_id
          LEFT JOIN tb_vehiculoclase vc ON c.tb_vehiculoclase_id=vc.tb_vehiculoclase_id
          LEFT JOIN tb_vehiculotipo vt ON c.tb_vehiculotipo_id=vt.tb_vehiculotipo_id
          WHERE cli.tb_cliente_id = :cliente_id and $tipo in (1,4) and tb_credito_est in (3,4)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
