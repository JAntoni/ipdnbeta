<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../retencion/Retencion.class.php');
  $oRetencion = new Retencion();
  require_once('../funciones/funciones.php');

  $direc = 'retencion';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $retencion_id = $_POST['retencion_id'];

  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Retencion Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Retencion';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Retencion';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Retencion';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en retencion
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'retencion'; $modulo_id = $retencion_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del retencion por su ID
    if(intval($retencion_id) > 0){
      $result = $oRetencion->mostrarUno($retencion_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el retencion seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $retencion_cretip = $result['data']['tb_retencion_cretip'];
          $cliente_id = $result['data']['tb_cliente_id'];
          $credito_id = $result['data']['tb_credito_id'];
          $retencion_pla = $result['data']['tb_retencion_pla'];
          $retencion_mot = $result['data']['tb_retencion_mot'];
          $retencion_deu = $result['data']['tb_retencion_deu'];
          $retencion_kil = $result['data']['tb_retencion_kil'];
          $retencion_det = $result['data']['tb_retencion_det'];

          if($retencion_cretip == 2){
            require_once ("../creditoasiveh/Creditoasiveh.class.php");
            $oCredito = new Creditoasiveh();
          }
          if($retencion_cretip == 3){
            require_once ("../creditogarveh/Creditogarveh.class.php");
            $oCredito = new Creditogarveh();
          }

          $result2 = $oCredito->mostrarUno($credito_id);
            if($result2['estado'] == 1){
              $vehtip_nom = $result2['data']['tb_vehiculotipo_nom'];
              $vehtip_img = $result2['data']['tb_vehiculotipo_img'];
              $cliente_nom = $result2['data']['tb_cliente_nom'];
            }
          $result2 == NULL;
        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_retencion" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="form_retencion" method="post">
          <input type="hidden" name="action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_retencion_id" value="<?php echo $retencion_id;?>">
          <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
          
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-md-4">
                <label for="cmb_credito_tip" class="control-label">Tipo de Crédito:</label>
                <select class="form-control" id="cmb_credito_tip" name="cmb_credito_tip">
                  <option value="2" <?php if($retencion_cretip ==2) echo 'selected';?> >ASIVEH</option>
                  <option value="3" <?php if($retencion_cretip ==3) echo 'selected';?> >GARVEH</option>
                </select>
              </div>
              <div class="form-group col-md-8">
                <label for="txt_cliente_nom" class="control-label">Cliente:</label>
                <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm" value="<?php echo $cliente_nom;?>">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="txt_credito_id" class="control-label">Crédito N°:</label>
                <input type="text" name="txt_credito_id" id="txt_credito_id" class="form-control input-sm" value="<?php echo $credito_id;?>" readonly>
              </div>
              <div class="form-group col-md-6">
                <label for="txt_vehiculo_pla" class="control-label">Placa:</label>
                <input type="text" name="txt_vehiculo_pla" id="txt_vehiculo_pla" class="form-control input-sm" value="<?php echo $retencion_pla;?>" readonly>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="txt_vehtip_nom" class="control-label">Tipo de Vehículo:</label>
                <input type="text" name="txt_vehtip_nom" id="txt_vehtip_nom" class="form-control input-sm" value="<?php echo $vehtip_nom;?>" readonly>
              </div>
              <div class="form-group col-md-6">
                <label for="cmb_retencion_mot" class="control-label">Motivo:</label>
                <select class="form-control" id="cmb_retencion_mot" name="cmb_retencion_mot">
                  <option value="0"></option>
                  <option value="1" <?php if($retencion_mot == 1) echo 'selected';?> >Deuda</option>
                  <option value="2" <?php if($retencion_mot == 2) echo 'selected';?> >Entrega Voluntaria</option>
                  <option value="3" <?php if($retencion_mot == 3) echo 'selected';?> >Otros</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="txt_retencion_deu" class="control-label">Deuda de:</label>
                <input type="text" name="txt_retencion_deu" id="txt_retencion_deu" class="form-control input-sm" value="<?php echo $retencion_deu;?>">
              </div>
              <div class="form-group col-md-6">
                <label for="txt_retencion_kil" class="control-label">Kilometraje:</label>
                <input type="text" name="txt_retencion_kil" id="txt_retencion_kil" class="form-control input-sm" value="<?php echo $retencion_kil;?>">
              </div>
            </div>

            <div class="form-group">
              <label for="txt_retencion_det" class="control-label">Descripción</label>
              <input type="text" name="txt_retencion_det" id="txt_retencion_det" class="form-control input-sm" value="<?php echo $retencion_det;?>">
            </div>
            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta retencion?</h4>
              </div>
            <?php endif;?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="retencion_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_retencion">Guardar</button>
              <?php endif; ?>
              <?php if($usuario_action == 'E'): ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_retencion">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_retencion">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/retencion/retencion_form.js';?>"></script>
