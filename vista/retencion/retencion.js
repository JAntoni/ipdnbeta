function retencion_form(usuario_act, retencion_id){ 
  $.ajax({
    type: "POST",
    url: VISTA_URL+"retencion/retencion_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      retencion_id: retencion_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_retencion_form').html(data);
        $('#modal_registro_retencion').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_retencion'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_retencion', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
        //llamar al formulario de solicitar permiso
        var modulo = 'retencion';
        var div = 'div_modal_retencion_form';
        permiso_solicitud(usuario_act, retencion_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function(data){
      
    },
    error: function(data){
      alerta_error('EROR',data.responseText);
      console.log(data.responseText);
    }
  });
}
function retencion_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"retencion/retencion_tabla.php",
    async:false,
    dataType: "html",                      
    data: $('#form_retencion_filtro').serialize(),
    beforeSend: function() {
      $('#div_retencion_tabla').html('<option value="">Cargando...</option>');
    },
    success: function(html){
      $('#div_retencion_tabla').html(html);
    },
    complete: function(data){
      console.log(data);
    }
  });
}
$(document).ready(function() {
  console.log('Perfil menu');

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate : new Date()
  });
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_filtro_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_filtro_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });

  $( "#txt_fil_cliente_nom" ).autocomplete({
    minLength: 1,
    source: VISTA_URL+"cliente/cliente_autocomplete.php",
    select: function(event, ui){
      $("#hdd_fil_cliente_id").val(ui.item.cliente_id);
      //retencion_tabla();               
    }
  });
});