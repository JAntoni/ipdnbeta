<?php
	$filtro_fec1 = date('01-01-Y');
	$filtro_fec2 = date('d-m-Y');
?>
<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-info">
		  <div class="panel-body">
		    <form id="form_retencion_filtro" class="form-inline" role="form">
          <button type="button" class="btn btn-primary btn-sm" onclick="retencion_form('I',0)"><i class="fa fa-plus"></i> Nueva Retención</button>
          <div class="form-group">
            <div class='input-group date' id='datetimepicker1'>
              <input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $filtro_fec1;?>"/>
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <div class='input-group date' id='datetimepicker2'>
              <input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $filtro_fec2;?>"/>
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="txt_fil_cliente_nom" class="control-label">Cliente:</label>
            <input type="text" name="txt_fil_cliente_nom" id="txt_fil_cliente_nom" class="form-control input-sm mayus" value="" size="40">
            <input type="hidden" name="hdd_fil_cliente_id" id="hdd_fil_cliente_id" class="form-control input-sm mayus" value="">
          </div>
          <button type="button" class="btn btn-info btn-sm" onclick="retencion_tabla()"><i class="fa fa-search"></i> Buscar</button>
        </form>
		  </div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12" id="retencion_mensaje_opc" style="padding-top: 5px;"> 
	</div>
</div>