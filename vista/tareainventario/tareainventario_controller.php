<?php

require_once '../../core/usuario_sesion.php';
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once('../tareainventario/Tareainventario.class.php');
$oTareainventario = new TareaInventario();
require_once ('../almacen/Almacen.class.php');

//echo 'en que estás 🙌';exit();

$accion = $_POST['accion'];
$garantia_id = intval($_POST['garantia_id']);
$almacen_destino = $_POST['alm_destino'];
$almacen_destino_id = intval($_POST['alm_destino_id']);
$almacen_actual_id= intval($_POST['almacen_actual_id']);
$almacen_actual_nom= $_POST['almacen_actual_nom'];

$fecha_hoy = date('d-m-Y h:i a');

$garantia_datos = $oGarantia->mostrarUno($garantia_id)["data"];

if($accion == 'envio'){
    try{
        $oGarantia->modificar_campo($garantia_id, 'tb_garantia_almest', 2, 'INT');
        
        $oTareainventario->tb_tarea_det = 'Registró el ENVÍO del producto '.$garantia_id.' del '.$almacen_actual_nom.' al '.$almacen_destino.' | '.$fecha_hoy;
        $oTareainventario->tb_tarea_usureg = $_SESSION['usuario_id'];
        $oTareainventario->tb_garantia_id = $garantia_id;
        $oTareainventario->tb_almacen_id1 = $almacen_actual_id;
        $oTareainventario->tb_almacen_id2 = $almacen_destino_id;
        $oTareainventario->insertar2();
        
        echo $garantia_id . ' - ' . $garantia_datos["tb_garantia_pro"] ;
    } catch (Exception $ex) {
        echo $ex->getMessage();
        throw $ex;
    }
}
elseif($accion == 'recibo'){
    try{
        $oGarantia->modificar_campo($garantia_id, 'tb_garantia_almest', 1, 'INT');
        $oGarantia->modificar_campo($garantia_id, 'tb_almacen_id', $almacen_destino_id, 'INT');
        
        $oTareainventario->tb_tarea_det = 'Recibe y almacena el producto '.$garantia_id.' en '.$almacen_destino.' | '.$fecha_hoy;
        $oTareainventario->tb_tarea_usureg = $_SESSION['usuario_id'];
        $oTareainventario->tb_garantia_id = $garantia_id;
        $oTareainventario->tb_almacen_id1 = $almacen_destino_id;
        $oTareainventario->tb_almacen_id2 = $almacen_actual_id;
        $oTareainventario->insertar2();

        echo $garantia_id . ' - ' . $garantia_datos["tb_garantia_pro"] ;
    } catch (Exception $ex) {
        echo $ex->getMessage();
        throw $ex;
    }
}
elseif($accion == 'cancela'){
    try{
        $oGarantia->modificar_campo($garantia_id, 'tb_garantia_almest', 1, 'INT');
        
        //CANCELAR EL ENVIO PREVIO ANTES DE CREAR UNA NUEVA TAREA

        $oTareainventario->tb_tarea_det = 'Canceló el envío del producto '.$garantia_id.' al '.$almacen_destino.' | '.$fecha_hoy;
        $oTareainventario->tb_tarea_usureg = $_SESSION['usuario_id'];
        $oTareainventario->tb_garantia_id = $garantia_id;
        $oTareainventario->tb_almacen_id1 = $almacen_actual_id;
        $oTareainventario->tb_almacen_id2 = 0;
        $oTareainventario->insertar2();

        echo $garantia_id . ' - ' . $garantia_datos["tb_garantia_pro"] ;
    } catch (Exception $ex) {
        echo $ex->getMessage();
        throw $ex;
    }
}