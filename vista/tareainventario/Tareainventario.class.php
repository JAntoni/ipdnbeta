<?php

if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
class Tareainventario extends Conexion{
    public $tb_tarea_id;
    public $tb_tarea_fecreg;
    public $tb_tarea_usureg;
    public $tb_tarea_det;
    public $tb_usuario_id;
    public $tb_tarea_usutip;
    public $tb_garantia_id;
    public $tb_tarea_est;
    public $tb_tarea_tipo;
    public $tb_almacen_id1;
    public $tb_almacen_id2;

    

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tareainventario(
                                tb_tarea_fecreg, 
                                tb_tarea_usureg, 
                                tb_tarea_det,
                                tb_usuario_id,
                                tb_tarea_usutip,
                                tb_garantia_id, 
                                tb_tarea_tipo) 
                        VALUES ( 
                                NOW(),
                                :tb_tarea_usureg, 
                                :tb_tarea_det,
                                :tb_usuario_id, 
                                :tb_tarea_usutip,
                                :tb_garantia_id,
                                :tb_tarea_tipo)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_tarea_usureg", $this->tb_tarea_usureg, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_tarea_det", $this->tb_tarea_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tarea_usutip", $this->tb_tarea_usutip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_garantia_id", $this->tb_garantia_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_tarea_tipo", $this->tb_tarea_tipo, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function insertar2(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_tareainventario(
                                tb_tarea_usureg,
                                tb_tarea_det,
                                tb_garantia_id,
                                tb_almacen_id1,
                                tb_almacen_id2) 
                        VALUES (
                                :tb_tarea_usureg, 
                                :tb_tarea_det,
                                :tb_garantia_id,
                                :tb_almacen_id1,
                                :tb_almacen_id2)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_tarea_usureg", $this->tb_tarea_usureg, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_tarea_det", $this->tb_tarea_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_garantia_id", $this->tb_garantia_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_almacen_id1", $this->tb_almacen_id1, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_almacen_id2", $this->tb_almacen_id2, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function eliminar_tarea($gar_id, $est, $tipo){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_tareainventario where tb_garantia_id =:gar_id and tb_tarea_est =:est and tb_tarea_tipo =:tipo;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_INT);
        $sentencia->bindParam(":est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    
    function realizar_tarea($gar_id, $est, $tipo){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_tareainventario set tb_tarea_est =1 where tb_garantia_id =:gar_id and tb_tarea_est =:est and tb_tarea_tipo =:tipo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_INT);
        $sentencia->bindParam(":est", $est, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    
      function filtrar_tarea_estado_tipo($gar_id, $est, $tipo){
      try {

        $sql = "SELECT * FROM tb_tareainventario where tb_garantia_id =:gar_id AND tb_tarea_est =:est and tb_tarea_tipo =:tipo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_STR);
        $sentencia->bindParam(":est", $est, PDO::PARAM_STR);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Tareas por Estados registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function filtrar_tareas_por_hacer($usu_id, $usu_tip){
      try {
            $sql = "SELECT * FROM tb_tareainventario where tb_tarea_est =0 and tb_tarea_usutip =:usu_tip and (tb_usuario_id =:usu_id or tb_usuario_id = 0)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":usu_id", $usu_id, PDO::PARAM_STR);
            $sentencia->bindParam(":usu_tip", $usu_tip, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay Tareas por Estados registrados";
              $retorno["data"] = "";
            }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
      function filtrar_tareas_por($columna,$valor, $tipodato){
        try {
            $sql = "select * from tb_tareainventario ti
                    inner join tb_usuario us on ti.tb_tarea_usureg = us.tb_usuario_id
                    left join tb_garantia gar on ti.tb_garantia_id = gar.tb_garantia_id
                    left join tb_almacen alm on ti.tb_almacen_id1 = alm.tb_almacen_id
                    left join tb_almacen alm1 on ti.tb_almacen_id2 = alm1.tb_almacen_id ";
            
            if( !empty($columna) && (in_array($tipodato, array('INT','STR')))){
                $sql .= " where ".$columna." = :valor ";
            }
            $sql .= " ORDER BY ti.tb_tarea_fecreg DESC;";
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
            
            $sentencia = $this->dblink->prepare($sql);
            if($tipodato == 'INT'){
                $sentencia->bindParam(":valor", $valor, PDO::PARAM_INT);
            }elseif($tipodato == 'STR'){
                $sentencia->bindParam(":valor", $valor, PDO::PARAM_STR);
            }
            $sentencia->execute();
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            return $retorno;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function ultimo_transito($gar_id){
      try {
            $sql = "SELECT ti.*,alm1.tb_almacen_nom as almacen1,alm2.tb_almacen_nom as almacen2 FROM tb_tareainventario ti 
                    INNER JOIN tb_almacen alm1 on ti.tb_almacen_id1 = alm1.tb_almacen_id
                    inner join tb_almacen alm2 on ti.tb_almacen_id2 = alm2.tb_almacen_id
                    where tb_garantia_id = :gar_id and tb_almacen_id1 in ('1','2','3')
                    and tb_almacen_id2 in ('1','2','3') order by tb_tarea_id desc limit 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":gar_id", $gar_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
              $sql= null;
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "La garantia no tiene registros de Transito a otro almacen";
              $retorno["data"] = "";
              $sql= null;
            }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    
}
?>