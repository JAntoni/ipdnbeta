<?php ?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditoasivehfile_form" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Subir imágenes</h4>
      </div>
      <form id="form_asivehfile" name="form_asivehfile" method="post">
        <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $_POST['cre_id']; ?>">
        <input type="hidden" name="hdd_action" value="<?php echo $_POST['action'];?>">

        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <input id="file_upload" name="file_upload" type="file" multiple="true">
            </div>
            <div class="col-md-12">
              <div id="queue"></div>
            </div>
            <div class="col-md-12">
              <div class="alert alert-danger alert-dismissible" id="alert_img" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4>
                  <i class="icon fa fa-ban"></i> Alerta
                </h4>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="button" class="btn btn-info" id="btn_guardar_creditoasivehfile" onclick="javascript:$('#file_upload').uploadifive('upload')">Guardar Fotos</button>
            <button type="button" class="btn btn-default" id="btn_cerrar_creditoasiveh">Listo</button>
          </div>
        </div>
      </form>
      <div id="msj_creditoverificacion" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo 'static/css/uploadifive/uploadifive.css'; ?>">
<script type="text/javascript" src="<?php echo 'static/js/uploadifive/jquery.uploadifive.js?ver=23092024'; ?>"></script>
<script type="text/javascript" src="<?php echo 'vista/creditoasivehfile/credito_file_form.js?ver=23092024'; ?>"></script>