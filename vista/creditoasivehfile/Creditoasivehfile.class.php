<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Creditoasivehfile extends Conexion {
    
    public $creditoasivehfile_xac = 1;
    public $credito_id;
    public $creditoasivehfile_url= '...';
    public $creditoasivehfile_his;
    public $creditoasivehfile_id;
    
    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    function insertar() {
        $this->dblink->beginTransaction();
        try
        {
            $sql = "INSERT INTO tb_creditoasivehfile(tb_creditoasivehfile_xac, tb_credito_id, tb_creditoasivehfile_url, tb_creditoasivehfile_his) VALUES (?, ?, ?, ?)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(1, $this->creditoasivehfile_xac, PDO::PARAM_INT);
            $sentencia->bindParam(2, $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(3, $this->creditoasivehfile_url, PDO::PARAM_STR);
            $sentencia->bindParam(4, $this->creditoasivehfile_his, PDO::PARAM_STR);
            $result = $sentencia->execute();
            $this->creditoasivehfile_id = $this->dblink->lastInsertId();
            $this->dblink->commit();
            
            $nombre_imagen = 'creditoasiveh_'.$this->credito_id.'_creditoasiveh_file_'. $this->creditoasivehfile_id .'.'. $this->fileParts['extension'];
            $creditoasivehfile_url = $this->directorio . strtolower($nombre_imagen);
            
            if(move_uploaded_file($this->tempFile, '../../'. $creditoasivehfile_url)){
                $sql = "UPDATE tb_creditoasivehfile SET tb_creditoasivehfile_url = ? WHERE tb_creditoasivehfile_id  = ?";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(1, $creditoasivehfile_url, PDO::PARAM_STR);
                $sentencia->bindParam(2, $this->creditoasivehfile_id, PDO::PARAM_INT);
                $result2 = $sentencia->execute();
              }
              else{
                $this->eliminar($this->creditoasivehfile_id);
              }
            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_url() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_creditoasivehfile SET 
                        tb_creditoasivehfile_url = :creditoasivehfile_url, 
                        tb_creditoasivehfile_his = CONCAT(tb_creditoasivehfile_his, :creditoasivehfile_his)
                    WHERE 
                        tb_creditoasivehfile_id =:creditoasivehfile_id";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_creditoasivehfile_url", $this->creditoasivehfile_url, PDO::PARAM_STR);
            $sentencia->bindParam(":creditoasivehfile_his", $this->creditoasivehfile_his, PDO::PARAM_STR);
            $sentencia->bindParam(":creditoasivehfile_id", $this->creditoasivehfile_id, PDO::PARAM_INT);
            $result = $sentencia->execute();
            $this->dblink->commit();
            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar($creditoasivehfile_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_creditoasivehfile SET tb_creditogarvehfile_xac = 0 WHERE tb_creditoasivehfile_id = :creditoasivehfile_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditoasivehfile_id", $creditoasivehfile_id, PDO::PARAM_INT);
            $result = $sentencia->execute();
            $this->dblink->commit();
            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($creditoasivehfile_id) {
        try {
            $sql = "SELECT * FROM tb_creditoasivehfile WHERE tb_creditoasivehfile_id = :creditoasivehfile_id AND tb_creditogarvehfile_xac = 1";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditoasivehfile_id", $creditoasivehfile_id, PDO::PARAM_INT);
            $sentencia->execute();
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function filtrar($credito_id) {
        try {
            $sql = "SELECT * FROM tb_creditoasivehfile WHERE tb_credito_id = :tb_credito_id AND tb_creditoasivehfile_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar() {
        try {
            $sql = "SELECT * FROM tb_creditoasivehfile WHERE tb_creditogarvehfile_xac=1 ORDER BY tb_creditoasivehfile_id DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}
?>
