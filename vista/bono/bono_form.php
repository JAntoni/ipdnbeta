<?php
require_once("../../core/usuario_sesion.php");
require_once("Bono.class.php");
$oBono = new Bono();
require_once '../usuario/Usuario.class.php';
$oUsuario = new Usuario();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$bono_id = $_POST['bono_id'];
$usuario_id = 0;
$vista = $_POST['vista'];

if ($vista == 'personalcontrato')
  $usuario_id = intval($_POST['usuario_id']);

$bono_fecini = date('d-m-Y');
$bono_fecfin = date('d-m-Y');


if ($_POST['action'] == "editar") {
  $dts = $oBono->mostrarUno($bono_id);
    if ($dts['estado'] == 1) {
      $usuario_id = intval($dts['data']['tb_usuario_id']);
      $bono_tip = intval($dts['data']['tb_bono_tip']);
      $bono_val = mostrar_moneda($dts['data']['tb_bono_val']);
      $bono_nom = $dts['data']['tb_bono_nom'];
      $bono_det = $dts['data']['tb_bono_det'];
      $bono_fecini = mostrar_fecha($dts['data']['tb_bono_fecini']);
      $bono_fecfin = mostrar_fecha($dts['data']['tb_bono_fecfin']);
    }
  $dts = NULL;
}

if ($usuario_id > 0) {
  $dts2 = $oUsuario->mostrarUno($usuario_id);
    if ($dts2['estado'] == 1) {
      $usuario_ape = $dts2['data']['tb_usuario_ape'];
      $usuario_nom = $dts2['data']['tb_usuario_nom'];
    }
  $dts2 = NULL;
}

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_bono" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000000">INFORMACIÓN DEL BONO</h4>
      </div>
      <form id="form_bono">
        <input name="action" id="action" type="hidden" value="<?php echo $action; ?>">
        <input name="hdd_bono_id" id="hdd_bono_id" type="hidden" value="<?php echo $bono_id; ?>">
        <input type="hidden" id="hdd_usuario_id" value="<?php echo $usuario_id; ?>">
        <input type="hidden" id="hdd_vista" value="<?php echo $vista; ?>">

        <div class="modal-body">
          <div class="box box-primary">
            <div class="box-header">
              <div class="row">
                <?php if($vista != 'personalcontrato'):?>
                  <div class="col-md-4">
                    <label>SEDE :</label>
                    <select name="cmb_empresa_id" id="cmb_empresa_id" class="form-control input-sm mayus">
                      <?php require_once '../empresa/empresa_select.php'; ?>
                    </select>
                  </div>
                <?php endif;?>
                <div class="col-md-4">
                  <label>USUARIO</label>

                  <select name="cmb_usuario_id" id="cmb_usuario_id" class="form-control input-sm mayus">
                    <?php
                    echo '<option value=' . $usuario_id . '>' . $usuario_ape . ' ' . $usuario_nom . '</option>';
                    ?>
                  </select>

                </div>
                <div class="col-md-4">
                  <label>Tipo de Bono:</label>
                  <select name="cmb_bono_tip" id="cmb_bono_tip" class="form-control input-sm mayus">
                    <option value="1" <?php if ($bono_tip == 1) echo 'selected'; ?>>Porcentaje al sueldo</option>
                    <option value="2" <?php if ($bono_tip == 2) echo 'selected'; ?>>Monto fijo</option>
                  </select>
                </div>
              </div>
              <p>
              <div class="row">
                <div class="col-md-4">
                  <label>Valor del Bono:</label>
                  <input type="text" name="txt_bono_val" id="txt_bono_val" class="form-control input-sm moneda" value="<?php echo $bono_val; ?>">
                </div>
                <div class="col-md-8">
                  <label>Nombre:</label>
                  <input type="text" name="txt_bono_nom" id="txt_bono_nom" value="<?php echo $bono_nom; ?>" class="form-control input-sm mayus">
                </div>
              </div>
              <p>
              <div class="row">
                <div class="col-md-12">
                  <label>Detalle:</label>
                  <input type="text" name="txt_bono_det" id="txt_bono_det" value="<?php echo $bono_det; ?>" class="form-control input-sm">
                </div>
              </div>
              <p>
              <div class="row">
                <div class="col-md-5">
                  <label for="">Fecha de Inicio</label>
                </div>
                <div class="col-md-5">
                  <label for="">Fecha de Fin</label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-10">
                  <div class="form-group">
                    <!--<label for="">Fecha de Inicio y Fecha de Fin </label>-->
                    <div class="input-group">
                      <div class='input-group date' id='datetimepicker1'>
                        <input type="text" name="txt_bono_fecini" id="txt_bono_fecini" value="<?php echo $bono_fecini; ?>" class="form-control input-sm">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                      <span class="input-group-addon">-</span>
                      <div class='input-group date' id='datetimepicker2'>
                        <input type="text" name="txt_bono_fecfin" id="txt_bono_fecfin" value="<?php echo $bono_fecfin; ?>" class="form-control input-sm">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
          <?php if ($_POST['action'] == 'eliminar') : ?>
            <div class="callout callout-warning">
              <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Area?</h4>
            </div>
          <?php endif; ?>

          <!--- MESAJES DE GUARDADO -->
          <div class="callout callout-info" id="seguro_mensaje" style="display: none;">
            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
          </div>
        </div>
        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_seguro">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/bono/bono_form.js?ver=1'; ?>"></script>