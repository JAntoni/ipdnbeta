<?php
require_once("../../core/usuario_sesion.php");
require_once("Bono.class.php");
$oBono = new Bono();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$dts = $oBono->mostrarTodos_usuario($personal_usuario_id); //definido en personalcontrato_tabla.php
?>
<table cellspacing="1" id="tabla_bono" class="table table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>FECHA INICIO</th>
      <th>FECHA FIN</th>
      <th>TIPO BONO</th>
      <th>VALOR BONO</th>
      <th>NOMBRE</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <?php if ($dts['estado'] == 1) : ?>
    <tbody>
      <?php foreach ($dts['data'] as $key => $dt) { ?>
        <tr>
          <td><?php echo $dt['tb_bono_id'] ?></td>
          <td><?php echo mostrar_fecha($dt['tb_bono_fecini']); ?></td>
          <td><?php echo mostrar_fecha($dt['tb_bono_fecfin']); ?></td>
          <td>
            <?php
            if ($dt['tb_bono_tip'] == 1)
              echo '% al sueldo';
            elseif ($dt['tb_bono_tip'] == 2)
              echo 'Monto fijo';
            else
              echo 'SIN TIPO: ERROR';
            ?>
          </td>
          <td><?php echo $dt['tb_bono_val'] ?></td>
          <td><?php echo $dt['tb_bono_nom'] ?></td>
          <td align="center">
            <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="bono_form('editar', '<?php echo $dt['tb_bono_id'] ?>')"><i class="fa fa-edit"></i></a>
            <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="eliminar_bono('<?php echo $dt['tb_bono_id'] ?>')"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  <?php endif; ?>
</table>