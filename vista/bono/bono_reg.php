<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Bono.class.php");
$oBono = new Bono();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

if($_POST['action']=="insertar"){

	$data['estado'] = 0;

	$oBono->tb_usuario_id= intval($_POST['cmb_usuario_id']); 
	$oBono->tb_bono_tip= intval($_POST['cmb_bono_tip']); 
	$oBono->tb_bono_val= moneda_mysql($_POST['txt_bono_val']);
	$oBono->tb_bono_nom = $_POST['txt_bono_nom']; 
	$oBono->tb_bono_det = $_POST['txt_bono_det']; 
	$oBono->tb_bono_fecini = fecha_mysql($_POST['txt_bono_fecini']); 
	$oBono->tb_bono_fecfin = fecha_mysql($_POST['txt_bono_fecfin']);
        
	$oBono->insertar();

	$data['estado'] = 1;
	$data['msj'] = 'Se registró el bono correctamente.';

	echo json_encode($data);
}

if($_POST['action']=="editar"){

	$oBono->tb_usuario_id= intval($_POST['cmb_usuario_id']); 
	$oBono->tb_bono_tip= intval($_POST['cmb_bono_tip']); 
	$oBono->tb_bono_val= moneda_mysql($_POST['txt_bono_val']);
	$oBono->tb_bono_nom = $_POST['txt_bono_nom']; 
	$oBono->tb_bono_det = $_POST['txt_bono_det']; 
	$oBono->tb_bono_fecini = fecha_mysql($_POST['txt_bono_fecini']); 
	$oBono->tb_bono_fecfin = fecha_mysql($_POST['txt_bono_fecfin']);
        $oBono->tb_bono_id = intval($_POST['hdd_bono_id']);

	$oBono->editar();

	$data['estado'] = 1;
	$data['msj'] = 'Se modificó el bono correctamente.';

	echo json_encode($data);
}

if($_POST['action']=="eliminar")
{
	$bono_id = intval($_POST['bono_id']);
	$oBono->eliminar($bono_id);
	echo 'Bono eliminado correctamente';
}
?>