<?php

  if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Bono extends Conexion{
    public $tb_bono_id;
    public $tb_bono_reg;
    public $tb_usuario_id;
    public $tb_bono_tip;
    public $tb_bono_val;
    public $tb_bono_nom;
    public $tb_bono_det;
    public $tb_bono_fecini;
    public $tb_bono_fecfin;
    
    function insertar(){
    $this->dblink->beginTransaction();
      try {  
            $sql = "INSERT INTO tb_bono(
                                    tb_usuario_id, 
                                    tb_bono_tip, 
                                    tb_bono_val,
                                    tb_bono_nom, 
                                    tb_bono_det, 
                                    tb_bono_fecini, 
                                    tb_bono_fecfin) 
				VALUES (
                                    :tb_usuario_id, 
                                    :tb_bono_tip, 
                                    :tb_bono_val,
                                    :tb_bono_nom, 
                                    :tb_bono_det, 
                                    :tb_bono_fecini, 
                                    :tb_bono_fecfin)";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_bono_tip", $this->tb_bono_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_bono_val", $this->tb_bono_val, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_nom", $this->tb_bono_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_det", $this->tb_bono_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_fecini", $this->tb_bono_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_fecfin", $this->tb_bono_fecfin, PDO::PARAM_STR);

        $result = $sentencia->execute();
        $tb_bono_id = $this->dblink->lastInsertId();
        $this->dblink->commit();
        $data['estado'] = $result;
        $data['tb_bono_id'] = $tb_bono_id;
        return $data; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function editar(){
      $this->dblink->beginTransaction();
      try {
            $sql = "UPDATE tb_bono SET
                                    tb_usuario_id = :tb_usuario_id, 
                                    tb_bono_tip = :tb_bono_tip, 
                                    tb_bono_val = :tb_bono_val, 
                                    tb_bono_nom = :tb_bono_nom, 
                                    tb_bono_det = :tb_bono_det, 
                                    tb_bono_fecini = :tb_bono_fecini, 
                                    tb_bono_fecfin = :tb_bono_fecfin 
				where 
                                    tb_bono_id =:tb_bono_id";
            
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_bono_tip", $this->tb_bono_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_bono_val", $this->tb_bono_val, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_nom", $this->tb_bono_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_det", $this->tb_bono_det, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_fecini", $this->tb_bono_fecini, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_fecfin", $this->tb_bono_fecfin, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_bono_id", $this->tb_bono_id, PDO::PARAM_INT);
        
        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    
    function eliminar($bono_id){
      $this->dblink->beginTransaction();
      try {
         $sql = "DELETE FROM tb_bono where tb_bono_id = :tb_bono_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_bono_id", $bono_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function mostrarUno($bono_id){
      try {
         $sql = "SELECT * FROM tb_bono where tb_bono_id =:tb_bono_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_bono_id", $bono_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrarTodos(){
      try {
         $sql = "SELECT * FROM tb_bono bo INNER JOIN tb_usuario usu on usu.tb_usuario_id = bo.tb_usuario_id";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function mostrarTodos_usuario($usuario_id){
      try {
         $sql = "SELECT * FROM tb_bono WHERE tb_usuario_id =:usuario_id";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function mostrar_por_fecha_periodo($fecha, $usuario_id){
      try {
         $sql = "SELECT * FROM tb_bono where :fecha between tb_bono_fecini and tb_bono_fecfin and tb_usuario_id =:tb_usuario_id order by tb_bono_id desc limit 1";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (16-03-23) */
    function mostrar_lista_por_fecha_periodo($fecha, $usuario_id){
      try {
         $sql = "SELECT * FROM tb_bono 
         WHERE :fecha BETWEEN tb_bono_fecini AND tb_bono_fecfin 
         AND tb_usuario_id =:tb_usuario_id ORDER BY tb_bono_id DESC";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */
    
    function mostrar_ultimo_bono(){
      try {
         $sql = "SELECT * FROM tb_bono order by tb_bono_id desc limit 1";
    
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay Comisiones registradas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
}
