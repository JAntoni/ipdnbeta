/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
  var empresa_id = $("#cmb_empresa_id").val();
  var usuario_id = $("#hdd_usuario_id").val();

  if (usuario_id == 0) {
    cmb_usu_id(empresa_id);
  }

  $("#cmb_empresa_id").change(function () {
    var empresa_id = $("#cmb_empresa_id").val();
    cmb_usu_id(empresa_id);
  });

  $("#datetimepicker1").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //    startDate: "-0d",
    endDate: new Date(),
  });

  $("#datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //    startDate: "-0d",
    //        endDate: new Date()
  });

  $("#datetimepicker1").on("change", function (e) {
    var startVal = $("#txt_bono_fecini").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $("#txt_bono_fecfin").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
  });

  $(".moneda").autoNumeric({
    aSep: "",
    aDec: ".",
    vMin: "0.00",
    vMax: "9999.00",
  });

  $("#form_bono").validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "bono/bono_reg.php",
        async: true,
        dataType: "json",
        data: $("#form_bono").serialize(),
        beforeSend: function () {
        },
        success: function (data) {
          var vista = $('#hdd_vista').val();

          if (parseInt(data.estado) > 0) {
            if(vista == 'personalcontrato')
              personalcontrato_tabla();
            else
              bono_tabla();
            swal_success("SISTEMA", data.msj, 2000);
            $("#modal_registro_bono").modal("hide");
          }
          else 
            $("#msj_form_bono").html(data.msj);
        },
        complete: function (data) {
          console.log(data);
        },
        error: function (data) {
          $("#msj_form_bono").html(data.responseText);
        },
      });
    },
    rules: {
      cmb_usuario_id: {
        required: true,
      },
      txt_bono_val: {
        required: true,
      },
      txt_bono_nom: {
        required: true,
      },
      txt_bono_det: {
        required: true,
      },
      txt_bono_fecini: {
        required: true,
      },
      txt_bono_fecfin: {
        required: true,
      },
    },
    messages: {
      cmb_usuario_id: {
        required: "Seleccione un usuario",
      },
      txt_bono_val: {
        required: "Ingrese el valor del bono",
      },
      txt_bono_nom: {
        required: "Ingrese un nombre de bono",
      },
      txt_bono_det: {
        required: "Detalle del bono",
      },
      txt_bono_fecini: {
        required: "Seleccione la fecha inicial",
      },
      txt_bono_fecfin: {
        required: "Seleccione la fecha final",
      },
    },
  });
});

function cmb_usu_id(empresa_id) {
  var mostrar = 0;
  $.ajax({
    type: "POST",
    url: VISTA_URL + "usuario/cmb_usu_id_select.php",
    async: false,
    dataType: "html",
    data: {
      mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
      empresa_id: empresa_id,
    },
    beforeSend: function () {
      $("#cmb_usuario_id").html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $("#cmb_usuario_id").html(html);
    },
    complete: function (html) {},
  });
}
