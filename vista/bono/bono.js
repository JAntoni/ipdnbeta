/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    bono_tabla();
});

function bono_tabla()
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "bono/bono_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            //pro_est:	$('#cmb_fil_pro_est').val()
        }),
        beforeSend: function () {
//            $('#div_bono_tabla').addClass("ui-state-disabled");
        },
        success: function (html) {
            $('#div_bono_tabla').html(html);
        },
        complete: function () {
            $('#div_bono_tabla').removeClass("ui-state-disabled");
        }
    });
}


function bono_form(action, bono_id)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "bono/bono_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            bono_id: bono_id,
            vista: 'bono_tabla'
        }),
        beforeSend: function () {
//			$('#msj_bono').hide();
//			$('#div_bono_form').dialog("open");
//			$('#div_bono_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
        success: function (html) {
            $('#div_modal_bono_form').html(html);
            $('#modal_registro_bono').modal('show');
            modal_hidden_bs_modal('modal_registro_bono', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
    });
}


function eliminar_bono(id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: '¿Está seguro de Eliminar el Bono?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "bono/bono_reg.php",
                    async: true,
                    dataType: "html",
                    data: ({
                        action: "eliminar",
                        bono_id: id
                    }),
                    beforeSend: function () {

                    },
                    success: function (html) {
                        swal_success("SISTEMA", html, 2000);
                    },
                    complete: function () {
                        bono_tabla();
                    }
                });
            },
            no: function () {}
        }
    });
}