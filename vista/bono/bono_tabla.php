<?php
require_once ("../../core/usuario_sesion.php");
require_once ("Bono.class.php");
$oBono = new Bono();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$dts = $oBono->mostrarTodos();


?>


<table cellspacing="1" id="tabla_bono" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">USUARIO</th>
            <th id="tabla_cabecera_fila">TIPO BONO</th>
            <th id="tabla_cabecera_fila">VALOR BONO</th>
            <th id="tabla_cabecera_fila">NOMBRE</th>
            <th id="tabla_cabecera_fila">DETALLE</th>
            <th id="tabla_cabecera_fila">FECHA INICIO</th>
            <th id="tabla_cabecera_fila">FECHA FIN</th>
            <th id="tabla_cabecera_fila">&nbsp;</th>
        </tr>
    </thead>
    <?php if ($dts['estado'] == 1): ?>  
        <tbody>
            <?php foreach ($dts['data']as $key => $dt) { ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $dt['tb_bono_id'] ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_usuario_nom'] ?></td>
                    <td id="tabla_fila">
                            <?php
                            if ($dt['tb_bono_tip'] == 1)
                                echo '% al sueldo';
                            elseif ($dt['tb_bono_tip'] == 2)
                                echo 'Monto fijo';
                            else
                                echo 'SIN TIPO: ERROR';
                            ?>
                    </td>
                    <td id="tabla_fila"><?php echo $dt['tb_bono_val'] ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_bono_nom'] ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_bono_det'] ?></td>
                    <td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_bono_fecini']); ?></td>
                    <td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_bono_fecfin']); ?></td>
                    <td id="tabla_fila"align="center">
                        <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="bono_form('editar', '<?php echo $dt['tb_bono_id']?>')"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="eliminar_bono('<?php echo $dt['tb_bono_id'] ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
        <?php
    }
    ?>
        </tbody>
        <?php endif; ?>
</table>