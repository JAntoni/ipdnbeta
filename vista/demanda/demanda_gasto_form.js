
$(document).ready(function () {
	$('#txt_demanda_fechapago').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		endDate: new Date()
	});

	$('#cbo_demanda_forma_egreso').change(function(){
		if ($(this).val() == 2) {
			$('.num_ope').show(150);
		} else {
			$('.num_ope').hide(150);
		}
	});

	//triggers
	disabled($('#form_demandagasto').find('.disabled'));
	$('#cbo_demanda_forma_egreso').change();
	$(".moneda").focus(function(){
        formato_moneda(this);
    });

	$('#form_demandagasto').validate({
		submitHandler: function () {
			var gasto_moneda_nom = $('#cbo_demanda_gastomoneda_id option:selected').text();
			var ejecucionfase_id = $('#hdd_demandagasto_ejecucionfase_id').val();
			var datos_extra = '&gasto_moneda_nom='+gasto_moneda_nom;
			$.ajax({
				type: "POST",
				url: VISTA_URL + "demanda/demanda_controller.php",
				async: true,
				dataType: "json",
				data: serialize_form('form_demandagasto', datos_extra),
				beforeSend: function () {
					$('#demandagasto_mensaje').show(400);
					$('#btn_guardar_demandagasto').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						disabled($('#form_demandagasto').find("button[class*='btn-sm'], select, input, textarea"));
						$('#btn_cancelar_demandagasto').prop('disabled', true);
						$('#demandagasto_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#demandagasto_mensaje').html(`<h4>${data.mensaje}</h4>`);

						setTimeout(function () {
							$('#modal_demanda_gasto_form').modal('hide');
							if (data.estado == 1) {
								actualizar_contenido_1fase(ejecucionfase_id);
							}
						}, 2500);
					}
					else {
						$('#demandagasto_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#demandagasto_mensaje').html(`<h4>Alerta: ${data.mensaje}</h4>`);
						$('#btn_guardar_demandagasto').prop('disabled', false);
					}
				},
				error: function (data) {
					$('#demandagasto_mensaje').removeClass('callout-info').addClass('callout-danger');
					$('#demandagasto_mensaje').html(`<h4>ALERTA DE ERROR: ${data.responseText}</h4>`);
				}
			});
		},
		rules: {
			txt_demanda_fechapago: {
				required: true
			},
			txt_demanda_gastomonto: {
				required: true
			},
			cbo_demanda_forma_egreso: {
				min: 1
			},
			txt_demanda_numope: {
				required: true
			},
			txt_demanda_gastopago_nroticket: {
				required: true
			}
		},
		messages: {
			txt_demanda_fechapago: {
				required: "Obligatorio*"
			},
			txt_demanda_gastomonto: {
				required: "Obligatorio*"
			},
			cbo_demanda_forma_egreso: {
				min: "Obligatorio*"
			},
			txt_demanda_numope: {
				required: "Obligatorio*"
			},
			txt_demanda_gastopago_nroticket: {
				required: "Obligatorio*"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	//mensaje antes de eliminar
    if ($.inArray($('#action_demanda').val(), ['leer_demandagasto', 'eliminar_demandagasto']) > -1) {
		disabled($('#form_demandagasto').find("button[class*='btn-sm'], select, input, textarea, a"));
        if ($('#action_demanda').val() == 'eliminar_demandagasto') {
			verificar_gastopago_eliminado();
        }
    } else if ($('#action_demanda').val() == 'insertar_demandagasto') {
		$('div#subir_file').show(150);
	}
});

function actualizar_vista_upload(upload_id, url) {
	var dominio_server = 'https://ipdnsac.ipdnsac.com/';
	//var dominio_server = 'http://192.168.101.90/ipdnsac/';
	var demanda_gastopago_id = $('#hdd_demanda_gastopago_id').val();

	$('#hdd_demanda_upload_id').val(upload_id);
	$('div#subir_file').hide(200);
	$('div#file_subido').html(`<object id="obj_pdf_viewer" data='${dominio_server}${url}' type='application/pdf' width='670px' height='664px'>Si no se muestra el archivo, puedes visualizarlo desde <a href='../../${url}' target='_blank'><b>AQUI</b></a></object>`).show(200);
	$('#btn_guardar_demandagasto').removeClass('disabled').removeAttr('disabled');
	if (demanda_gastopago_id == '') {
		$('#txt_demanda_gastomonto, #cbo_demanda_forma_egreso, #txt_demanda_numope, #txt_demanda_gastopago_nroticket').removeClass('disabled').removeAttr('disabled');
		$('#extend_datos_gasto').click();
	}
}

function demandagasto_cancelar() {
	var upload_id = $('#hdd_demanda_upload_id').val();

	if (upload_id > 0) {
        if (confirm('Al cancelar el registro, el pdf será eliminado. ¿Desea seguir de todos modos?')) {
			eliminar_pdf_demandagasto(upload_id, 'C');
        }
    } else {
        $('#modal_demanda_gasto_form').modal('hide');
    }
}

function verificar_gastopago_eliminado() {
	var xac = $('#hdd_demanda_gastopago_xac').val();
	if (parseInt(xac) == 1) {
		$('#gastopago_mensaje').removeClass('callout-info').addClass('callout-danger');
		var mens = '<h4><i class="icon fa fa-warning"></i> SOLICITE LA ELIMINACION DEL PAGO DE GASTO A ÁREA DE CONTABILIDAD. ';
		mens += `CREDITO ID ${$('#hdd_demandagasto_credito_id').val()}`;
		mens += `, GASTO ID: ${$('#hdd_demanda_gasto_id').val()}`;
		mens += `, GASTO PAGO ID: ${$('#hdd_demanda_gastopago_id').val()}</h4>`;
		$('#gastopago_mensaje').html(mens);
		$('#gastopago_mensaje').show();
		disabled($('#btn_guardar_demandagasto'));
	} else {
		$('#demandagasto_mensaje').removeClass('callout-info').addClass('callout-warning');
		var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este registro?</h4>';
		$('#demandagasto_mensaje').html(mens);
		$('#demandagasto_mensaje').show();
	}
}

function eliminar_pdf_demandagasto(upload_id, action) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ejecucionupload/ejecucionupload_controller.php",
		async: true,
		dataType: "json",
		data: {
			action: 'eliminar_pdf',
			upload_id: upload_id,
			modulo_nom: 'ejecucion_varios-'+$('#hdd_demanda_gasto_tipo').val()
		},
		beforeSend: function () {
			$('#btn_cancelar_demandagasto, #btn_guardar_demandagasto, #btn_cerrar_dem_gas_form, #btn_cerrar_x').prop('disabled', true);
			$('#demandagasto_mensaje').addClass('callout-info');
			$('#demandagasto_mensaje').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>');
			$('#demandagasto_mensaje').show(300);
		},
		success: function (data) {
			after_eliminar_pdf(data, action);
		},
		error: function (data) {
			$('#demandagasto_mensaje').removeClass('callout-info').addClass('callout-warning');
			$('#demandagasto_mensaje').html('ERROR AL CANCELAR: ' + data.responseText);
		}
	});
}

function after_eliminar_pdf(data, action) {
	if (action == 'C') {
		if (parseInt(data.estado) > 0) {
			$('#demandagasto_mensaje').removeClass('callout-info').addClass('callout-success');
			$('#demandagasto_mensaje').html(`<h4><i class="icon fa fa-check"></i> ${data.mensaje}</h4>`);
			setTimeout(function() {
				$('#modal_demanda_gasto_form').modal('hide');
			}, 2000);
		} else {
			$('#demandagasto_mensaje').removeClass('callout-info').addClass('callout-warning');
			$('#demandagasto_mensaje').html(`<h4><i class="icon fa fa-bug"></i> ${data.mensaje}</h4>`);
			$('#btn_cancelar_demandagasto').removeAttr('disabled');
		}
	}
}