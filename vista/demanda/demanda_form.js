var letras_finales = ['1-D', '1-E', '1-F', '1-G', '1-H', '1-I', '1-J', '1-K', '1-L', '1-M', '1-N', '1-O', '1-P', '1-Q', '1-R', '1-S', '1-T', '1-U', '1-V', '1-W', '1-X', '1-Y', '1-Z'];
var dem_mens;
var array_anexos;
var click_color = 0, click_comb = 0;
$(document).ready(function(){
	$('#txt_demanda_asiento_color_fecha, #txt_demanda_boleta_fecha_emi, #txt_demanda_asiento_combus_fecha')
		.datepicker({
			language: 'es',
			autoclose: true,
			format: "dd-mm-yyyy",
			endDate: new Date()
    	})
		.on("changeDate", function (e) {
			consultar_fecha($(this).val(), this.id);
		})
	;

	$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	$('#che_color').on('ifChanged', function () {
        if (this.checked) {
			$('#panel_color').show(100);
			añadir_fila('color');
			if ($('#panel_boleta').css('display') == 'none' && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
				$('#panel_boleta').show(100);
				añadir_fila('boleta');
			}
        }
        else {
			$('#panel_color').hide(100);
			quitar_fila('color');
			if (!$('#che_combustible').prop('checked') && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
				$('#panel_boleta').hide(100);
				quitar_fila('boleta');
			}
        }
    });

	$('#che_combustible').on('ifChanged', function () {
        if (this.checked) {
			$('#panel_combus').show(100);
			añadir_fila('combus');
			if ($('#panel_boleta').css('display') == 'none' && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
				$('#panel_boleta').show(100);
				añadir_fila('boleta');
			}
        }
        else {
			$('#panel_combus').hide(100);
			quitar_fila('combus');
			if (!$('#che_color').prop('checked') && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
				$('#panel_boleta').hide(100);
				quitar_fila('boleta');
			}
        }
    });

	$('#txt_demanda_boleta_documentosunat').blur(function(){
        var serie = $.trim($(this).val().split("-")[0]);
        var numer = $.trim($(this).val().split("-")[1]);
        if ($(this).val() !== '') {
            if ($(this).val().indexOf("-") <= -1) {
                alerta_error("Verifique", "<b>Formato de Factura debe incluir un guión</b>");
                return;
            }
            else if (serie.length > 4 || serie.length < 2  || numer.length > 8 || numer.length < 1) {
                alerta_error("Verifique", "<b>Cantidad de caracteres en la serie y/o numero</b>");
                return;
            }
            else if (!solo_letras(serie.substring(0, 1))) {
                alerta_error("Verifique", "<b>Formato de Factura, la serie debe incluir una letra al inicio</b>");
                return;
            }
            else if (!solo_numeros(serie.substring(serie.length - 1))){
                alerta_error("Verifique", "<b>Formato de Factura, la serie debe incluir un número</b>");
                return;
            }
            else if (!solo_numeros(numer)) {
                alerta_error("Verifique", "<b>Formato de Factura, el número de factura es incorrecto</b>");
                return;
            }
            $('#txt_demanda_boleta_documentosunat').val(rellenar_serie_ceros(serie)+'-'+rellenar_num_ceros(numer, 8));
        }
    });

	$("#panel_boleta, #panel_color, #panel_combus, #panel_anexos").on("click", function () {
		var captura = $(this).text();
		if (captura == "ANEXOS") {
			$("#btn_guardar_demanda").css("visibility", "visible");
		}
		else {
			$("#btn_guardar_demanda").css("visibility", "hidden");
			if (captura == 'ASIENTO INSC. (COLOR)' && click_color == 0) {
				click_color++;
				cambiar_size_textarea($('#txt_demanda_obs_color_piepag'));
			}
			if (captura == 'ASIENTO INSC. (COMBUSTIBLE)' && click_comb == 0) {
				click_comb++;
				cambiar_size_textarea($('#txt_demanda_obs_combus_piepag'));
			}
		}
	});

	if (parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
		buscar_pdf_boleta();
	}
	disabled($("#form_demanda").find(".disabled"));

	$('#form_demanda').validate({
		submitHandler: function () {
			var datos_extra = '', continuar = 1, mensaje = '';

			if ($('#panel_color').css('display') != 'none' && ($('#txt_demanda_asiento_color_fecha').val() == '' || $('#txt_demanda_obs_color_piepag').val() == '')) {
				mensaje = 'Faltan llenar algunos campos en la pestaña de "ASIENTO INSC. (COLOR)"';
				continuar = 0;
				if ($('#txt_demanda_asiento_color_fecha').val() == '') {
					$('#txt_demanda_asiento_color_fecha').addClass('req');
				}
				if ($('#txt_demanda_obs_color_piepag').val() == '') {
					$('#txt_demanda_obs_color_piepag').addClass('req');
				}
			}

			if ($('#panel_combus').css('display') != 'none' && ($('#txt_demanda_asiento_combus_fecha').val() == '' || $('#txt_demanda_obs_combus_piepag').val() == '')) {
				mensaje = 'Faltan llenar algunos campos en la pestaña de "ASIENTO INSC. (COMBUSTIBLE)"';
				continuar = 0;
				if ($('#txt_demanda_asiento_combus_fecha').val() == '') {
					$('#txt_demanda_asiento_combus_fecha').addClass('req');
				}
				if ($('#txt_demanda_obs_combus_piepag').val() == '') {
					$('#txt_demanda_obs_combus_piepag').addClass('req');
				}
			}

			if (parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1 && $('#panel_boleta').css('display') != 'none' && ($('#txt_demanda_boleta_documentosunat').val() == '' || $('#txt_demanda_boleta_fecha_emi').val() == '')) {
				mensaje = 'Faltan llenar algunos campos en la pestaña de "BOLETA COMPRA VEH"';
				continuar = 0;
				if ($('#txt_demanda_boleta_documentosunat').val() == '') {
					$('#txt_demanda_boleta_documentosunat').addClass('req');
				}
				if ($('#txt_demanda_boleta_fecha_emi').val() == '') {
					$('#txt_demanda_boleta_fecha_emi').addClass('req');
				}
			}

			$('#tbl_detalle_anexos').find('textarea[id*="txt_anexo_nom"]').each(function(){
				if ($(this).val() == ''){
					mensaje = 'Complete los campos obligatorios';
					continuar = 0;
					$(this).addClass('req');
				} else {
					$(this).removeClass('req');
				}
			});

			if (($('#panel_color').css('display') != 'none' && $('#hdd_demanda_asiento_color_upload_id').val() == '')||($('#panel_combus').css('display') != 'none' && $('#hdd_demanda_asiento_combus_upload_id').val() == '')) {
				mensaje = 'Falta subir un pdf de asiento de inscripción vehicular';
				continuar = 0;
			}

			if (continuar != 1) {
                alerta_error('ALERTA', mensaje);
                return;
            }
			
			$.ajax({
				type: "POST",
				url: VISTA_URL + "demanda/demanda_controller.php",
				async: true,
				dataType: "json",
				data: serialize_form('form_demanda', datos_extra),
				beforeSend: function () {
					$('#demanda_mensaje').show(400);
					$('#btn_guardar_demanda').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						disabled($('#form_demanda').find("button[class*='btn-sm'], select, input, textarea"));
						$('#demanda_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#demanda_mensaje').html(`<h4>${data.mensaje}</h4>`);

						setTimeout(function () {
							if (data.mensaje.indexOf('❌') == -1){ //validar si no tiene un error en el mensaje
								$('#modal_demanda_form').modal('hide');
							}
							actualizar_contenido_1fase($('#hdd_demanda_ejecucionfase_id').val());
						}, 2500);
					}
					else {
						$('#demanda_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#demanda_mensaje').html(`<h4>Alerta: ${data.mensaje}</h4>`);
						$('#btn_guardar_demanda').prop('disabled', false);
					}
				},
				error: function (data) {
					$('#demanda_mensaje').removeClass('callout-info').addClass('callout-danger');
					$('#demanda_mensaje').html(`<h4>ALERTA DE ERROR: ${data.responseText}</h4>`);
				}
			});
		},
		rules: {
		},
		messages: {
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	//COMPLETAR FILAS CARGOS
	array_anexos = JSON.parse($('#hdd_anexos_array').val());
	var cont = array_anexos.length;
	if (cont > 8) {
		if ($('#panel_color').css('display') != 'none') {
			añadir_fila('color');
		}
		if ($('#panel_combus').css('display') != 'none') {
			añadir_fila('combus');
		}
		if ($('#panel_boleta').css('display') != 'none' && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
			añadir_fila('boleta');
		}
		if ($('#hdd_anexos_incluir_redacc').val() != '') {
			var array_anexos_incluir = JSON.parse($('#hdd_anexos_incluir_redacc').val());
			var child = 2;
			array_anexos_incluir.forEach(element => {
				añadir_fila('doc_incluir', child);
				child++;
			});
		}
		
		setTimeout(function() {
			var i = 6;
			while(i < cont){
				$(`#txt_anexo_nom_${i}`).html(array_anexos[i]).change();
				i++;
			}
		}, 250);
	}

    if ($.inArray($('#action_demanda').val(), ['leer', 'eliminar']) > -1) {
		disabled($('#form_demanda').find("button[class*='btn-sm'], select, input, textarea, a").not('.disabled, .no-disabled'));
		//mensaje antes de eliminar
        if ($('#action_demanda').val() == 'eliminar') {
            $('#demanda_mensaje').removeClass('callout-info').addClass('callout-warning');
            var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este registro?</h4>';
            $('#demanda_mensaje').html(mens);
            $('#demanda_mensaje').show();
        }
    }

	$('#tbl_detalle_anexos').find('textarea').each(function(){
		cambiar_size_textarea(this);
	});
});

function añadir_fila(tipo, child_opcional) {
	var child;
	if (tipo == 'color') {
		child = 5;
	}
	if (tipo == 'boleta' && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
		child = ($('#che_color').prop('checked') && $('#che_combustible').prop('checked')) ? 7 : 6;
	}
	if (tipo == 'combus') {
		child = $('#che_color').prop('checked') ? 6 : 5;
	}
	if (tipo == 'doc_incluir') {
		child = child_opcional;
	} else {
		child += (array_anexos.length - 8);
	}
	var class_letra = parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1 ? '.preconst' : '';

	$('#txt_anexo_nom_'+child).closest('tr').nextAll().each(function () {
		// Getting <tr> id.
		var id = $(this).attr('id');

		// Gets the row number from <tr> id.
		var dig = parseInt(id.split('_')[3]);

		// Modifying row id.
		$(this).attr('id', `tabla_cabecera_fila_${dig + 1}`);

		//todos los hijos input, select, button se cambiará su id
		$(this).find('input, textarea').each(function () {
			//todos los id solo deben tener tres partes, donde la parte 3 es el num de orden empezando desde cero
			var id_antiguo = $(this)[0].id.split('_')[3];
			$(this).attr('id', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
			$(this).attr('name', $(this)[0].id.replace( id_antiguo, (dig + 1) ) );
		});

		$(this).find(`td.letra${class_letra}`).html(letras_finales[dig-2]);
	});

	class_letra = parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1 ? ' preconst' : '';

	//agregar columnas opcionales
	if (tipo == 'color') {
		$('#tabla_cabecera_fila_'+child).after(
			`<tr id="tabla_cabecera_fila_${child+1}">
				<td id="tabla_fila" class="letra${class_letra}">${letras_finales[child-2]}</td>
				<td id="tabla_fila">
					<textarea name="txt_anexo_nom_${child+1}" id="txt_anexo_nom_${child+1}" class="form-control input-sm input-shadow textarea_asiento_color" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"></textarea>
				</td>
			</tr>`
		);
	}
	if (tipo == 'boleta' && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
		var num_to_restar = ($('#che_color').prop('checked') && $('#che_combustible').prop('checked')) ? 3 : 2;
		$('#tabla_cabecera_fila_'+child).after(
			`<tr id="tabla_cabecera_fila_${child+1}">
				<td id="tabla_fila" class="letra${class_letra}">${letras_finales[child-num_to_restar]}</td>
				<td id="tabla_fila">
					<textarea name="txt_anexo_nom_${child+1}" id="txt_anexo_nom_${child+1}" class="form-control input-sm input-shadow textarea_boleta" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"></textarea>
				</td>
			</tr>`
		);
	}
	if (tipo == 'combus') {
		$('#tabla_cabecera_fila_'+child).after(
			`<tr id="tabla_cabecera_fila_${child+1}">
				<td id="tabla_fila" class="letra${class_letra}">${letras_finales[child-2]}</td>
				<td id="tabla_fila">
					<textarea name="txt_anexo_nom_${child+1}" id="txt_anexo_nom_${child+1}" class="form-control input-sm input-shadow textarea_asiento_combus" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"></textarea>
				</td>
			</tr>`
		);
	}
	if (tipo == 'doc_incluir') {
		$('#tabla_cabecera_fila_'+child).after(
			`<tr id="tabla_cabecera_fila_${child+1}">
				<td id="tabla_fila" class="letra${class_letra}">${letras_finales[child-2]}</td>
				<td id="tabla_fila">
					<textarea name="txt_anexo_nom_${child+1}" id="txt_anexo_nom_${child+1}" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;">${array_anexos[child+1]}</textarea>
				</td>
			</tr>`
		);
	}

	$('#cantidad_anexos').val(parseInt($('#cantidad_anexos').val())+1);
	rellenar_textarea_opc();
}

function quitar_fila(tipo) {
	var child;
	if (tipo == 'color') {
		child = $('textarea.textarea_asiento_color');
	}
	if (tipo == 'combus') {
		child = $('textarea.textarea_asiento_combus');
	}
	if (tipo == 'boleta' && parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
		child = $('textarea.textarea_boleta');
	}

	// Iterating across all the rows obtained to change the index
	child.closest('tr').nextAll().each(function () {
		// Getting <tr> id.
		var id = $(this).attr('id');

		// Gets the row number from <tr> id.
		var dig = parseInt(id.split('_')[3]);

		// Modifying row id.
		$(this).attr('id', `tabla_cabecera_fila_${dig - 1}`);

		//todos los hijos input, select, button se cambiará su id
		$(this).find('input, textarea').each(function () {
			//todos los id solo deben tener tres partes, donde la parte 3 es el num de orden empezando desde cero
			id_antiguo = $(this)[0].id.split('_')[3];
			$(this).attr('id', $(this)[0].id.replace(id_antiguo, (dig - 1)));
			$(this).attr('name', $(this)[0].id.replace(id_antiguo, (dig - 1)));
		});

		$(this).find('td.letra').html(letras_finales[dig-4]);
	});

	// Removing the current row.
	child.closest('tr').remove();
	$('#cantidad_anexos').val(parseInt($('#cantidad_anexos').val())-1);
}

function cambiar_size_textarea(obj) {
	$(obj).on('input change', function() {
        $(this).css('height', 'auto');
        $(this).css('height', this.scrollHeight + 'px');
    });
	setTimeout(() => {
		$(obj).change();
	}, 180);
}

function rellenar_textarea_opc() {
	$('textarea.textarea_asiento_color').on('focus', function(){
		$(this).val('Asiento de inscripción de vehículo, del '+$('#hdd_demanda_asiento_color_fecha').val()+'.');
	});
	$('textarea.textarea_asiento_combus').on('focus', function(){
		$(this).val('Asiento de inscripción de vehículo, del '+$('#hdd_demanda_asiento_combus_fecha').val()+'.');
	});
	$('textarea#txt_demanda_obs_color_piepag').on('focus', function(){
		$(this).val($(this).val().replace('[FECHA_INS_COLOR]', $('#hdd_demanda_asiento_color_fecha').val()));
	});
	$('textarea#txt_demanda_obs_combus_piepag').on('focus', function(){
		$(this).val($(this).val().replace('[FECHA_INS_COMBUS]', $('#hdd_demanda_asiento_combus_fecha').val()));
	});
	if (parseInt($('#hdd_demanda_cgarvtipo_id').val()) == 1) {
		$('textarea.textarea_boleta').on('focus', function(){
			$(this).val('Boleta electrónica '+ $('#txt_demanda_boleta_documentosunat').val().toUpperCase() +', del '+$('#hdd_demanda_boleta_fecha_emi').val()+'.');
		});
	} else {
		$('textarea#txt_demanda_obs_color_piepag').on('focus', function(){
			$(this).val($(this).val().replace('[FECHA_INS_COLOR]', $('#txt_demanda_asiento_color_fecha').val()));
		});
	}
}

function consultar_fecha(fecha, id) {
	$.ajax({
		type: 'POST',
		url: VISTA_URL + 'demanda/demanda_controller.php',
		async: true,
		dataType: 'json',
		data: ({
			action_demanda: 'consultar_fecha',
			fecha: fecha
		}),
		success: function (data) {
			if (data.estado == 1) {
				var id_hdd = id.replace('txt', 'hdd');
				$('#'+id_hdd).val(data.fecha);
			}
		}
	});
}

function demanda_cancelar() {
	var upload_color_id = $('#hdd_demanda_asiento_color_upload_id').val();
	var upload_combus_id = $('#hdd_demanda_asiento_combus_upload_id').val();

	if (upload_color_id > 0 || upload_combus_id > 0) {
        if (confirm('Al cancelar el registro, todo pdf subido será eliminado. ¿Desea seguir de todos modos?')) {
			if (upload_color_id > 0) {
				eliminar_pdf_asiento(upload_color_id, 'color');
			}
			if (upload_combus_id > 0) {
				eliminar_pdf_asiento(upload_combus_id, 'combu');
			}
			dem_mens = '';
        }
    } else {
        $('#modal_demanda_form').modal('hide');
    }
}

function eliminar_pdf_asiento(upload_id, tipo) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ejecucionupload/ejecucionupload_controller.php",
		async: true,
		dataType: "json",
		data: {
			action: 'eliminar_pdf',
			upload_id: upload_id,
			modulo_nom: 'ejecucion_asiento_ins_'+tipo
		},
		beforeSend: function () {
			$('#btn_cancelar_demanda, #btn_guardar_demanda, #btn_cerrar_demanda_form, #btn_cerrar_x').prop('disabled', true);
		},
		success: function (data) {
			after_eliminar_pdf(data, tipo);
		},
		error: function (data) {
			$('#demanda_mensaje').removeClass('callout-info').addClass('callout-warning');
			$('#demanda_mensaje').html('ERROR AL CANCELAR: ' + data.responseText);
		}
	});
}

function after_eliminar_pdf(data, tipo) {
	if (parseInt(data.estado) > 0) {
		$('#demanda_mensaje').removeClass('callout-info callout-warning').addClass('callout-success');
		if (dem_mens == '') {
			dem_mens = `<i class="icon fa fa-check"></i>${data.mensaje} | CIERRE ESTA VENTANA (BOTON X)`;
		} else {
			dem_mens += `<br><i class="icon fa fa-check"></i>${data.mensaje} | CIERRE ESTA VENTANA (BOTON X)`;
		}
	} else {
		$('#demanda_mensaje').removeClass('callout-info callout-success').addClass('callout-warning');
		if (dem_mens == '') {
			dem_mens = `<i class="icon fa fa-bug"></i>${data.mensaje} | tome captura pantalla y envíe a sistemas | CIERRE ESTA VENTANA (BOTON X)`;
		} else {
			dem_mens += `<br><i class="icon fa fa-bug"></i>${data.mensaje} | tome captura pantalla y envíe a sistemas | CIERRE ESTA VENTANA (BOTON X)`;
		}
	}

	$('#panel_anexos').click();
	$('#demanda_mensaje').html(`<h4>${dem_mens}</h4>`).show(100);
	$('#btn_cerrar_x').removeAttr('disabled');
}

function actualizar_vista_upload(upload_id, url, modulo_nom) {
	var dominio_server = 'https://ipdnsac.ipdnsac.com/';
	//var dominio_server = 'http://192.168.101.90/ipdnsac/';

	if (modulo_nom == 'ejecucion_asiento_ins_color') {
		$('#hdd_demanda_asiento_color_upload_id').val(upload_id);
		$('div#subir_file_color').hide(200);
		$('#datos_asiento_color, #div_observacion_color').show(200);
		$('div#file_subido_color').html(`<div class="col-md-12"><object id="obj_pdf_viewer_color" data='${dominio_server}${url}' type='application/pdf' width='700px' height='514px'>Si no se muestra el archivo, puedes visualizarlo desde <a href='../../${url}' target='_blank'><b>AQUI</b></a></object></div>`).show(200);
		cambiar_size_textarea($('#txt_demanda_obs_color_piepag'));
	} else if (modulo_nom == 'ejecucion_asiento_ins_combu') {
		$('#hdd_demanda_asiento_combus_upload_id').val(upload_id);
		$('div#subir_file_combus').hide(200);
		$('#datos_asiento_combus, #div_observacion_combus').show(200);
		$('div#file_subido_combus').html(`<div class="col-md-12"><object id="obj_pdf_viewer_combus" data='${dominio_server}${url}' type='application/pdf' width='700px' height='514px'>Si no se muestra el archivo, puedes visualizarlo desde <a href='../../${url}' target='_blank'><b>AQUI</b></a></object></div>`).show(200);
		cambiar_size_textarea($('#txt_demanda_obs_combus_piepag'));
	}
	$('#btn_guardar_demanda').removeClass('disabled').removeAttr('disabled');
	if ($('#che_combustible').prop('checked')) {
		disabled($('#che_combustible'));
	}
	if ($('#che_color').prop('checked')) {
		disabled($('#che_color'));
	}
}

function buscar_pdf_boleta() {
	var dominio_server = 'https://ipdnsac.ipdnsac.com/';
	//var dominio_server = 'http://192.168.101.90/ipdnsac/';
	$.ajax({
		type: 'POST',
		url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
		async: true,
		dataType: 'json',
		data: ({
			action: 'buscar_documento',
			ejecucion_id: $('#ejecucion_id').val(),
			nombre_doc: 'bol_fac_compra'
		}),
		success: function (data) {
			if (data.estado == 1) {
				$('#pdf_viewer_boleta').prop('data', dominio_server+data.url);
				$('#link_boleta_alternativo').prop('href', '../../'+data.url);
				$('#hdd_demanda_boleta_tabla').val(data.tabla);
				$('#hdd_demanda_boleta_tabla_id').val(data.id_reg);
			}
		}
	});
}