<?php
//revision de sesion activa
    require_once '../../core/usuario_sesion.php';
//
//requires
    require_once '../funciones/fechas.php';
    require_once '../funciones/funciones.php';
    require_once '../demanda/Demanda.class.php';
    $oDemanda = new Demanda();
    require_once '../ejecucion/Ejecucion.class.php';
    $oEjecucion = new Ejecucion();
    require_once '../cartanotarial/Cartanotarial.class.php';
    $oCarta = new Cartanotarial();
    require_once '../escriturapublica/Escriturapublica.class.php';
    $oEscritura = new Escriturapublica();
    require_once '../upload/Upload.class.php';
    $oUpload = new Upload();
    require_once '../ejecucionupload/Ejecucionfasefile.class.php';
    $oEjecucionfasefile = new Ejecucionfasefile();
//

//datos post
$usuario_action = $_POST['action'];
$demanda_id     = intval($_POST['demanda_id']);
$ejecucion_id   = $_POST['ejecucion_id'];
$ejecucionfase_id = $_POST['ejecucionfase_id'];
$action2        = $_POST['action2'];
$credito_id     = $_POST['credito_id'];
$cgarvtipo_id   = $_POST['cgarvtipo_id'];

//datos estaticos
$listo          = 1;
$action         = devuelve_nombre_usuario_action($usuario_action);

$titulo = '';
//titulo del form
if ($usuario_action == 'I') {
    $titulo = 'Registrar demanda';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar demanda';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar demanda';
} elseif ($usuario_action == 'L') {
    $titulo = 'Demanda registrada';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
if ($usuario_action != 'I') {
    $titulo .= ": ID $demanda_id";
}
//

if ($listo == 1) {
    $dominio_server = 'https://ipdnsac.ipdnsac.com/';
    //$dominio_server = 'http://192.168.101.90/ipdnsac/';

    $style_tab_boleta = $style_tab_color = $style_tab_combus = 'display: none;';
    $contar_anexos = 3;

    if (!empty($demanda_id)) {
        $res = $oDemanda->listar_todos($demanda_id, '', 1, '', '');
        if ($res['estado'] == 1) {
            $demanda = $res['data'][0];

            $anexo = explode('*', $demanda['tb_demanda_nombre_anexos']);
            $cambios = explode(',', $demanda['tb_demanda_cambio_det']);
            $incluye_descerraje = intval($demanda['tb_demanda_incluyedescer']) == 1 ? 'checked' : '';

            $color_checked = $cambios[0] == 1 ? 'checked' : '';
            $combustible_checked = $cambios[1] == 1 ? 'checked' : '';

            if ($demanda['tb_demanda_cambio'] == 1) {
                $style_tab_color = $cambios[0] == 1 ? '' : 'display: none;';
                $style_tab_combus = $cambios[1] == 1 ? '' : 'display: none;';

                if ($cambios[0] == 1) {
                    $upload_color_id = $demanda['tb_demanda_asiento_color_upload_id'];
                    $fecha_asiento_color = mostrar_fecha($demanda['tb_demanda_asiento_color_fecha']);
                    $url_asiento_color = $oUpload->mostrarUno($upload_color_id)['data']['upload_url'];
                    $obs_color = $demanda['tb_demanda_obs_piepag_color'];
                }

                if ($cambios[1] == 1) {
                    $upload_combus_id = $demanda['tb_demanda_asiento_combus_upload_id'];
                    $fecha_asiento_combus = mostrar_fecha($demanda['tb_demanda_asiento_combus_fecha']);
                    $url_asiento_combus = $oUpload->mostrarUno($upload_combus_id)['data']['upload_url'];
                    $obs_combus = $demanda['tb_demanda_obs_piepag_combus'];
                }

                if ($cgarvtipo_id == 1) {
                    $style_tab_boleta = '';
                    $documentosunat_boleta = $demanda['tb_demanda_boleta_documentosunat'];
                    $fecha_boleta = mostrar_fecha($demanda['tb_demanda_boleta_fecha_emi']);
                }
            }

            $res_docs_incluir = null;
            if (intval($cgarvtipo_id) != 1) { //si es constitucion buscar si tiene otros documentos para incluir en la redaccion
                $oEjecucionfasefile->incluir_redac = 1;
                $res_docs_incluir = $oEjecucionfasefile->listar('', '', $ejecucion_id, '', '', '', '', '');
                if ($res_docs_incluir['estado'] == 1) {
                    foreach ($res_docs_incluir['data'] as $key => $doc) {
                        $add = ucfirst(strtolower($doc['tb_ejecucionfasefile_archivonom']));
                        if (!empty($doc['tb_ejecucionfasefile_num'])) {
                            $add .= ' N° ' . $doc['tb_ejecucionfasefile_num'];
                        }
                        if (!empty($doc['tb_ejecucionfasefile_fecnoti'])) {
                            $add .= ' de fecha ' . strtolower(fechaActual(fecha_mysql($doc['tb_ejecucionfasefile_fecnoti'])));
                        }
                        $anexo[$contar_anexos] = 'Copia legalizada de ' . $add . '.';
                        $contar_anexos++;
                    }
                    $res_docs_incluir = json_encode($res_docs_incluir['data']);
                }
                $oEjecucionfasefile->incluir_redac = null;
            }
        }
        unset($res);
    } else {
        //valores recomendados al abrir form
        $res_ejec = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '')['data'][0];
        $res_carta_rep = $oCarta->listar_todos('', $ejecucion_id, 2, '')['data'][0];
        $diligencia_rep = fecha_mysql($res_carta_rep['tb_cartanotarial_fecnotariarecepciona']) == fecha_mysql($res_carta_rep['tb_cartanotarial_fecdiligencia']) ? 'mismo día' : strtolower(fechaActual(fecha_mysql($res_carta_rep['tb_cartanotarial_fecdiligencia'])));
        $res_carta_cli = $oCarta->listar_todos('', $ejecucion_id, 1, 'tb_escriturapublica_clientedir')['data'][0];
        $diligencia_cli = fecha_mysql($res_carta_cli['tb_cartanotarial_fecnotariarecepciona']) == fecha_mysql($res_carta_cli['tb_cartanotarial_fecdiligencia']) ? 'mismo día' : strtolower(fechaActual(fecha_mysql($res_carta_cli['tb_cartanotarial_fecdiligencia'])));
        $placa = strtoupper(trim($res_ejec['tb_credito_vehpla']));
        $placa = str_replace("-", "", $placa);
        $tipo_cgv = intval($cgarvtipo_id) == 1 ? strtolower(str_replace('Ó', 'ó', $res_ejec['cgarvtipo_nombre2'])) : 'constitución';

        $credito_getdocs = empty($res_ejec['tb_credito_id_padreorigen']) ? $res_ejec['tb_credito_id'] : $res_ejec['tb_credito_id_padreorigen'];
        $escr_pub = $oEscritura->listar_todos('', $credito_getdocs)['data'][0];
        $conc_firmas = $escr_pub['tb_escriturapublica_fechasiguales'] != 1 ? ', con conclusión de firmas el ' . strtolower(fechaActual(fecha_mysql($escr_pub['tb_escriturapublica_fechaconcfirmas']))) : '';

        $anexo[0] = 'Vigencia de poder de la Empresa Inversiones y Préstamos del Norte S.A.C.';
        $anexo[1] = 'Copia del DNI del ' . customUcwords($res_ejec['tb_cargo_nom']) . '.';
        $anexo[2] = 'Copia legalizada del Testimonio de la Escritura Pública de ' . ucwords($tipo_cgv) . ' de Garantía Mobiliaria.';
        
        if (intval($cgarvtipo_id) != 1) {
            $obs_color = "En la Escritura Pública de ".ucwords($tipo_cgv)." de garantía mobiliaria de fecha ".strtolower(fechaActual(fecha_mysql($escr_pub['tb_escriturapublica_fecha']))). $conc_firmas.
                ", se consignó como COLOR: [CAMBIAR]; sin embargo, es preciso indicar que con fecha [FECHA_INS_COLOR] se inscribió el cambio de características –asiento de inscripción que adjuntamos, ".
                "consignándose actualmente el COLOR: [CAMBIAR].";
            //

            $obs_combus = "En la Escritura Pública de ".ucwords($tipo_cgv)." de garantía mobiliaria de fecha ".strtolower(fechaActual(fecha_mysql($escr_pub['tb_escriturapublica_fecha']))). $conc_firmas.
                ", se consignó como COMBUSTIBLE: [CAMBIAR]; sin embargo, es preciso indicar que con fecha [FECHA_INS_COMBUS] se inscribió el cambio de características –asiento de inscripción que adjuntamos, ".
                "consignándose actualmente el COMBUSTIBLE: [CAMBIAR].";
            //

            $res_docs_incluir = null;
            if (intval($cgarvtipo_id) != 1) { //si es constitucion buscar si tiene otros documentos para incluir en la redaccion
                $oEjecucionfasefile->incluir_redac = 1;
                $res_docs_incluir = $oEjecucionfasefile->listar('', '', $ejecucion_id, '', '', '', '', '');
                if ($res_docs_incluir['estado'] == 1) {
                    foreach ($res_docs_incluir['data'] as $key => $doc) {
                        $add = ucfirst(strtolower($doc['tb_ejecucionfasefile_archivonom']));
                        if (!empty($doc['tb_ejecucionfasefile_num'])) {
                            $add .= ' N° ' . $doc['tb_ejecucionfasefile_num'];
                        }
                        if (!empty($doc['tb_ejecucionfasefile_fecnoti'])) {
                            $add .= ' de fecha ' . strtolower(fechaActual(fecha_mysql($doc['tb_ejecucionfasefile_fecnoti'])));
                        }
                        $anexo[$contar_anexos] = 'Copia legalizada de ' . $add . '.';
                        $contar_anexos++;
                    }
                    $res_docs_incluir = json_encode($res_docs_incluir['data']);
                }
                $oEjecucionfasefile->incluir_redac = null;
            }
        }
        else {
            $obs_color = 'Precisamos que en el Testimonio de la Escritura Pública N° ' . $escr_pub['tb_escriturapublica_num'] . ', de fecha ' . date('d/m/Y', strtotime($escr_pub['tb_escriturapublica_fecha'])) .
                ' de ' . $tipo_cgv . ' de Garantía Mobiliaria (Escritura que se suscribe antes de la inmatriculación vehicular, se elabora con los datos que figuran en la factura emitida por el concesionario y su ' .
                'ingreso a registros públicos se lleva a cabo después del ingreso de la inmatriculación con sus cambios de características requeridas por el propietario), se consignó como COLOR: [CAMBIAR] ' .
                '(como consta en la factura que adjuntamos); sin embargo, en la solicitud de inmatriculación se requirió también el cambio a COLOR: [CAMBIAR].';
            //
    
            $obs_combus = 'En la Escritura Pública de ' . $tipo_cgv . ' de Garantía Mobiliaria N° ' . $escr_pub['tb_escriturapublica_num'] . ', de fecha ' . date('d/m/Y', strtotime($escr_pub['tb_escriturapublica_fecha'])) .
                $conc_firmas . ', se consignó como COMBUSTIBLE: [CAMBIAR]; sin embargo, es preciso indicar que con fecha [FECHA_INS_COMBUS] se inscribió el cambio de características –asiento de inscripción que adjuntamos, ' .
                'consignándose actualmente el COMBUSTIBLE: [CAMBIAR].';
            //
        }

        $anexo[$contar_anexos] = 'Copia legalizada de la Carta Notarial de fecha ' . strtolower(fechaActual(fecha_mysql($res_carta_rep['tb_cartanotarial_fecnotariarecepciona']))) . ' diligenciada el ' . $diligencia_rep . ', dirigida al representante.';
        $anexo[($contar_anexos+1)] = 'Copia legalizada de la Carta Notarial de fecha ' . strtolower(fechaActual(fecha_mysql($res_carta_cli['tb_cartanotarial_fecnotariarecepciona']))) . ' diligenciada el ' . $diligencia_cli . ', dirigida al deudor /constituyente /depositario.';
        $anexo[($contar_anexos+2)] = 'Certificado de Gravámenes y Cargas, otorgado por el Registro de Propiedad Vehicular de Chiclayo del vehículo de Placa ' . $placa . ', donde consta inscrita la Garantía Mobiliaria y su forma de ejecución.';
        $anexo[($contar_anexos+3)] = 'Arancel Judicial por requerimiento de incautación.';
        $anexo[($contar_anexos+4)] = 'Cédulas de notificación.';
    }
    $nega_asiento_color_subido = $style_tab_color == '' ? 'display: none;' : '';
    $nega_asiento_combus_subido = $style_tab_combus == '' ? 'display: none;' : '';
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_demanda_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button id="btn_cerrar_x" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_demanda" method="post">
                    <div class="modal-body" style="padding: 5px 15px;">
                        <input type="hidden" name="action_demanda" id="action_demanda" value="<?php echo $action; ?>">
                        <input type="hidden" name="hdd_demanda_id" id="hdd_demanda_id" value="<?php echo $demanda_id; ?>">
                        <input type="hidden" name="hdd_demanda_reg" id="hdd_demanda_reg" value='<?php echo json_encode($demanda); ?>'>
                        <input type="hidden" name="hdd_demanda_ejecucion_id" id="hdd_demanda_ejecucion_id" value="<?php echo $ejecucion_id; ?>">
                        <input type="hidden" name="hdd_demanda_ejecucionfase_id" id="hdd_demanda_ejecucionfase_id" value="<?php echo $ejecucionfase_id; ?>">
                        <input type="hidden" name="hdd_demanda_credito_id" id="hdd_demanda_credito_id" value="<?php echo $credito_id; ?>">
                        <input type="hidden" name="hdd_demanda_cgarvtipo_id" id="hdd_demanda_cgarvtipo_id" value="<?php echo $cgarvtipo_id; ?>">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs" style="font-family: cambria;font-weight: bold">
                                        <?php if ($cgarvtipo_id == 1) { ?>
                                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" class="no-disabled" id="panel_boleta" style="<?php echo $style_tab_boleta; ?>padding: 7px 10px">BOLETA COMPRA VEH</a></li>
                                        <?php } ?>
                                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" class="no-disabled" id="panel_color" style="<?php echo $style_tab_color; ?>padding: 7px 10px">ASIENTO INSC. (COLOR)</a></li>
                                        <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false" class="no-disabled" id="panel_combus" style="<?php echo $style_tab_combus; ?>padding: 7px 10px">ASIENTO INSC. (COMBUSTIBLE)</a></li>
                                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" class="no-disabled" id="panel_anexos" style="padding: 7px 10px">ANEXOS</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>CAMBIOS ENTRE CERTIFICADO REG VEH Y ESC PUBLICA</label>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="checkbox" style="display: inline-block; margin-left: 10px;">
                                                        <label>
                                                            <input type="checkbox" id="che_color" name="che_color" class="flat-green" value="1" <?php echo $color_checked; ?>>&nbsp; Color
                                                        </label>
                                                    </div>
                                                    <div class="checkbox" style="display: inline-block; margin-left: 10px;">
                                                        <label>
                                                            <input type="checkbox" id="che_combustible" name="che_combustible" class="flat-green" value="1" <?php echo $combustible_checked; ?>>&nbsp; Combustible
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <span style="font-weight: bold;">ANEXOS:</span>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="hdd_anexos_array" name="hdd_anexos_array" value='<?php echo json_encode($anexo); ?>'>
                                                    <input type="hidden" id="hdd_anexos_incluir_redacc" name="hdd_anexos_incluir_redacc" value='<?php echo $res_docs_incluir; ?>'>
                                                    <input type="hidden" name="cantidad_anexos" id="cantidad_anexos" value="8">
                                                    <table id="tbl_detalle_anexos" name="tbl_detalle_anexos" class="table table-hover" style="word-wrap:break-word;">
                                                        <thead>
                                                            <tr id="tabla_cabecera">
                                                                <th id="tabla_cabecera_fila" style="width: 8%;">Letra</th>
                                                                <th id="tabla_cabecera_fila" style="width: 92%;">Nombre</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbody_anexos">
                                                            <tr id="tabla_cabecera_fila_0">
                                                                <td id="tabla_fila">1-A</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_0" id="txt_anexo_nom_0" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[0]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                            <tr id="tabla_cabecera_fila_1">
                                                                <td id="tabla_fila">1-B</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_1" id="txt_anexo_nom_1" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[1]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                            <tr id="tabla_cabecera_fila_2">
                                                                <td id="tabla_fila">1-C</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_2" id="txt_anexo_nom_2" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[2]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                            <tr id="tabla_cabecera_fila_3">
                                                                <td id="tabla_fila" class="letra">1-D</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_3" id="txt_anexo_nom_3" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[$contar_anexos]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                            <tr id="tabla_cabecera_fila_4">
                                                                <td id="tabla_fila" class="letra">1-E</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_4" id="txt_anexo_nom_4" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[($contar_anexos+1)]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                            <tr id="tabla_cabecera_fila_5">
                                                                <td id="tabla_fila" class="letra">1-F</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_5" id="txt_anexo_nom_5" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[($contar_anexos+2)]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                            <tr id="tabla_cabecera_fila_6">
                                                                <td id="tabla_fila" class="letra preconst">1-G</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_6" id="txt_anexo_nom_6" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[($contar_anexos+3)]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                            <tr id="tabla_cabecera_fila_7">
                                                                <td id="tabla_fila" class="letra preconst">1-H</td>
                                                                <td id="tabla_fila">
                                                                    <textarea name="txt_anexo_nom_7" id="txt_anexo_nom_7" class="form-control input-sm input-shadow" rows="1" style="height: 25px; resize: none; overflow: hidden; padding: 3px 10px;"><?php echo $anexo[($contar_anexos+4)]; ?></textarea>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="che_incluye_descerraje" name="che_incluye_descerraje" class="flat-green" value="1" <?php echo $incluye_descerraje; ?>><b>&nbsp; Incluye descerraje</b>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="callout callout-info" id="demanda_mensaje" style="display: none;"></div>
                                        </div>
                                        <?php if ($cgarvtipo_id == 1) { ?>
                                            <div class="tab-pane" id="tab_2">
                                                <div class="row form-group">
                                                    <div class="col-md-3">
                                                        <label for="txt_demanda_boleta_documentosunat">Numeración:</label>
                                                        <input type="text" name="txt_demanda_boleta_documentosunat" id="txt_demanda_boleta_documentosunat" placeholder="B001-00000001" class="form-control input-sm input-shadow mayus" value="<?php echo $documentosunat_boleta; ?>">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="hidden" name="hdd_demanda_boleta_fecha_emi" id="hdd_demanda_boleta_fecha_emi">
                                                        <label for="txt_demanda_boleta_fecha_emi">Fecha emisión:</label>
                                                        <div class='input-group date'>
                                                            <input type="text" name="txt_demanda_boleta_fecha_emi" id="txt_demanda_boleta_fecha_emi" class="form-control input-sm input-shadow" placeholder="Fec. emi. boleta" value="<?php echo $fecha_boleta; ?>">
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
    
                                                <div class="row" id="boleta_file_subido">
                                                    <input type="hidden" name="hdd_demanda_boleta_tabla" id="hdd_demanda_boleta_tabla">
                                                    <input type="hidden" name="hdd_demanda_boleta_tabla_id" id="hdd_demanda_boleta_tabla_id">
                                                    <div class="col-md-12">
                                                        <object id="pdf_viewer_boleta" data='' type='application/pdf' width='700px' height="514px">Si no se muestra el archivo, puedes visualizarlo desde <a id="link_boleta_alternativo" href='' target='_blank'><b>AQUI</b></a></object>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="tab-pane" id="tab_3">
                                            <input type="hidden" name="hdd_demanda_asiento_color_upload_id" id="hdd_demanda_asiento_color_upload_id" value="<?php echo $upload_color_id; ?>">

                                            <div class="row" id="subir_file_color" style="<?php echo $nega_asiento_color_subido; ?>">
                                                <div class="col-md-12">
                                                    <a class="btn btn-sm btn-github" onclick="ejecucionpdf_form(<?php echo $ejecucion_id . ', ' . $ejecucionfase_id . ', ' . $credito_id . ', \'new\', \'ejecucion_asiento_ins_color\', \'' . $action2 . '\''; ?>)" title="Subir asiento inscripción">Subir pdf asiento (color)</a>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="callout callout-warning">
                                                        <h4>Advertencia!</h4>
                                                        <p>No existe archivo PDF de Asiento de inscripción de Vehiculo. Debe Subir uno.</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group" id="datos_asiento_color" style="<?php echo $style_tab_color; ?>">
                                                <div class="col-md-4">
                                                    <input type="hidden" name="hdd_demanda_asiento_color_fecha" id="hdd_demanda_asiento_color_fecha">
                                                    <label for="txt_demanda_asiento_color_fecha">Fecha asiento:</label>
                                                    <div class='input-group date'>
                                                        <input type="text" name="txt_demanda_asiento_color_fecha" id="txt_demanda_asiento_color_fecha" class="form-control input-sm input-shadow" placeholder="Fec. Asiento" value="<?php echo $fecha_asiento_color; ?>">
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group" id="div_observacion_color" style="<?php echo $style_tab_color; ?>">
                                                <div class="col-md-12">
                                                    <label for="txt_demanda_obs_color_piepag">Observación:</label>
                                                    <textarea name="txt_demanda_obs_color_piepag" id="txt_demanda_obs_color_piepag" class="form-control input-sm input-shadow" style="resize: none; overflow: hidden;" placeholder="Observación"><?php echo $obs_color; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="row" id="file_subido_color" style="<?php echo $style_tab_color; ?>">
                                                <?php if (!empty($url_asiento_color)) { ?>
                                                    <div class="col-md-12">
                                                        <object id="obj_pdf_viewer_color" data='<?php echo $dominio_server . $url_asiento_color; ?>' type='application/pdf' width='700px' height="514px">Si no se muestra el archivo, puedes visualizarlo desde <a href='<?php echo "../../" . $url_asiento_color; ?>' target='_blank'><b>AQUI</b></a></object>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_4">
                                            <input type="hidden" name="hdd_demanda_asiento_combus_upload_id" id="hdd_demanda_asiento_combus_upload_id" value="<?php echo $upload_combus_id; ?>">

                                            <div class="row" id="subir_file_combus" style="<?php echo $nega_asiento_combus_subido; ?>">
                                                <div class="col-md-12">
                                                    <a class="btn btn-sm btn-github" onclick="ejecucionpdf_form(<?php echo $ejecucion_id . ', ' . $ejecucionfase_id . ', ' . $credito_id . ', \'new\', \'ejecucion_asiento_ins_combu\', \'' . $action2 . '\''; ?>)" title="Subir asiento inscripción">Subir pdf asiento (combustible)</a>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="callout callout-warning">
                                                        <h4>Advertencia!</h4>
                                                        <p>No existe archivo PDF de Asiento de inscripción de Vehiculo. Debe Subir uno.</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group" id="datos_asiento_combus" style="<?php echo $style_tab_combus; ?>">
                                                <div class="col-md-4">
                                                    <input type="hidden" name="hdd_demanda_asiento_combus_fecha" id="hdd_demanda_asiento_combus_fecha">
                                                    <label for="txt_demanda_asiento_combus_fecha">Fecha asiento:</label>
                                                    <div class='input-group date'>
                                                        <input type="text" name="txt_demanda_asiento_combus_fecha" id="txt_demanda_asiento_combus_fecha" class="form-control input-sm input-shadow" placeholder="Fec. Asiento" value="<?php echo $fecha_asiento_combus; ?>">
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group" id="div_observacion_combus" style="<?php echo $style_tab_combus; ?>">
                                                <div class="col-md-12">
                                                    <label for="txt_demanda_obs_combus_piepag">Observación:</label>
                                                    <textarea name="txt_demanda_obs_combus_piepag" id="txt_demanda_obs_combus_piepag" class="form-control input-sm input-shadow" style="resize: none; overflow: hidden;" placeholder="Observación"><?php echo $obs_combus; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="row" id="file_subido_combus" style="<?php echo $style_tab_combus; ?>">
                                                <?php if (!empty($url_asiento_combus)) { ?>
                                                    <div class="col-md-12">
                                                        <object id="obj_pdf_viewer_combus" data='<?php echo $dominio_server . $url_asiento_combus; ?>' type='application/pdf' width='700px' height="514px">Si no se muestra el archivo, puedes visualizarlo desde <a href='<?php echo "../../" . $url_asiento_combus; ?>' target='_blank'><b>AQUI</b></a></object>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_demanda">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_demanda">Aceptar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') { ?>
                                <button type="button" class="btn btn-danger" onclick="demanda_cancelar()" id="btn_cancelar_demanda">Cancelar</button>
                            <?php } else { ?>
                                <button type="button" id="btn_cerrar_demanda_form" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                            <?php } ?>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/demanda/demanda_form.js?ver='.rand(); ?>"></script>