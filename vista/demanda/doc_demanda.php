<?php
//require y use
    require_once '../../vendor/autoload.php';
    use PhpOffice\PhpWord\PhpWord;
    use PhpOffice\PhpWord\IOFactory;
    use PhpOffice\PhpWord\Shared\Html;
    use PhpOffice\PhpWord\Style\Font;
    use PhpOffice\PhpWord\Style\Paragraph;
    require_once '../funciones/funciones.php';
    require_once '../funciones/fechas.php';
    require_once '../demanda/Demanda.class.php';
    $oDemanda = new Demanda();
    require_once '../ejecucion/Ejecucion.class.php';
    $oEjecucion = new Ejecucion();
    require_once '../creditogarveh/Creditogarveh.class.php';
    $oCredito = new Creditogarveh();
    require_once '../persona/Persona.class.php';
    $oPersona = new Persona();
    require_once '../cargo/Cargo.class.php';
    $oCargo = new Cargo();
    require_once '../escriturapublica/Escriturapublica.class.php';
    $oEscritura = new Escriturapublica();
    require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
    $oCertificado = new Certificadoregistralveh();
    require_once '../zona/Zona.class.php';
    $oZona = new Zona();
    require_once '../cartanotarial/Cartanotarial.class.php';
    $oCarta = new Cartanotarial();
    require_once '../ejecucionupload/Ejecucionfasefile.class.php';
    $oEjecucionfasefile = new Ejecucionfasefile();
//

//recibir datos GET
    $ejecucion_id = intval($_GET['d1']);
    $demanda_id = intval($_GET['d2']);
    $tipo_documento = $_GET['d3']; //solo puede ser WORD Y PDF
//

//validar formato de documento y que tanto la demanda como ejecucion existan
    if (empty($tipo_documento) || !in_array($tipo_documento, array('pdf', 'word'))) {
        echo 'NO HAS SELECCIONADO EL TIPO DOCUMENTO. MODIFICA EL FINAL DE LA URL SI SERÁ WORD O PDF';
        exit();
    } elseif ($tipo_documento == 'pdf'){
        require_once '../../static/tcpdf/tcpdf.php';
    }

    $demanda_reg = $oDemanda->listar_todos($demanda_id, $ejecucion_id, 1, '', '');
    if ($demanda_reg['estado'] != 1) {
        unset($demanda_reg);
        echo 'ERROR EN EL ID DE DEMANDA. NO HUBO RESULTADO PARA LA CONSULTA';
        exit();
    } else {
        $demanda_reg = $demanda_reg['data'][0];
    }

    $ejecucion_reg = $oEjecucion->listar_todos($ejecucion_id, '', '', '', '', '');
    if ($ejecucion_reg['estado'] != 1) {
        unset($ejecucion_reg);
        echo 'ERROR EN EL ID DE EJECUCION. NO HUBO RESULTADO PARA LA CONSULTA';
        exit();
    } else {
        $ejecucion_reg = $ejecucion_reg['data'][0];
    }
//

//DATOS CREDITO, EJECUCION, VEHICULO, ESCRITURA PUBLICA
    $credito_getdocs = empty($ejecucion_reg['tb_credito_id_padreorigen']) ? $ejecucion_reg['tb_credito_id'] : $ejecucion_reg['tb_credito_id_padreorigen'];
    $credito_datos = $oCredito->mostrarUno($ejecucion_reg['tb_credito_id'])['data'];
    $res_escritura = $oEscritura->listar_todos('', $credito_getdocs);
    $res_certificado = $oCertificado->listar_todos('', '', $ejecucion_reg['tb_ejecucion_id'], 1);

    $tipo_cgv = intval($ejecucion_reg['tb_cgarvtipo_id']) == 1 ? strtolower(str_replace('Ó', 'ó', $ejecucion_reg['cgarvtipo_nombre2'])) : 'constitución';

    $carroceria = $credito_datos['tb_vehiculoclase_nom'];
    $categoria  = strtoupper($credito_datos['tb_credito_vehcate']);
    $marca      = $credito_datos['tb_vehiculomarca_nom'];
    $modelo     = $credito_datos['tb_vehiculomodelo_nom'];
    $anio       = $credito_datos['tb_credito_vehano'];
    $nro_serie  = strtoupper(trim($credito_datos['tb_credito_vehsercha']));
    $nro_motor  = strtoupper(trim($credito_datos['tb_credito_vehsermot']));
    $placa      = strtoupper(trim($credito_datos['tb_credito_vehpla']));
    $placa      = str_replace("-", "", $placa);
    $zona_reg   = strtoupper($oZona->mostrarUno($credito_datos['tb_zonaregistral_id'])['data']['tb_zona_nom']);

    $color = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    $combustible = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    $nro_partida = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    $fecha_crv = '<b style="color: red;">ERROR EN CERT. REG. VEH</b>';
    //estos datos se deben obtener del certificado registral vehicular
    if ($res_certificado['estado'] == 1) {
        $res_certificado = $res_certificado['data'][0];
        $color          = strtoupper(trim($res_certificado['tb_certificadoregveh_color']));
        $combustible    = strtoupper(trim($res_certificado['tb_certificadoregveh_comb']));
        $nro_partida    = $res_certificado['tb_certificadoregveh_nropartida'];
        $fecha_crv      = date('d/m/Y', strtotime($res_certificado['tb_certificadoregveh_fecgenerado']));
    }
    unset($res_certificado);

    //NRO SERIE EN LETRAS
        $arr_serie_chasis = str_split($nro_serie);
        $serie_chasis_letras = '';
        foreach ($arr_serie_chasis as $key => $value) {
            if (is_numeric($value)) {
                $serie_chasis_letras .= numero_letra($value) . ', '; //en formato.php
            } else {
                switch ($value) {
                    case ".":
                        $serie_chasis_letras .= 'PUNTO, ';
                        break;
                    case "-":
                        $serie_chasis_letras .= 'GUIÓN, ';
                        break;
                    case "*":
                        $serie_chasis_letras .= 'ASTERÍSCO, ';
                        break;
                    case "@":
                        $serie_chasis_letras .= 'ARROBA, ';
                        break;
                    case " ":
                        $serie_chasis_letras .= 'ESPACIO, ';
                        break;
                    default:
                        $serie_chasis_letras .= $value . ', ';
                }
            }
        }
        $serie_chasis_letras = substr($serie_chasis_letras, 0, -2);
    //

    //NRO MOTOR LETRAS
        $arr_nro_motor = str_split($nro_motor);
        $nro_motor_letras = '';
        foreach ($arr_nro_motor as $key => $value) {
            if(is_numeric($value)){
                $nro_motor_letras .= numero_letra($value).', '; //en formato.php
            }
            else{
                switch ($value) {
                case ".":
                    $nro_motor_letras .= 'PUNTO, ';
                    break;
                case "-":
                    $nro_motor_letras .= 'GUIÓN, ';
                    break;
                case "*":
                    $nro_motor_letras .= 'ASTERÍSCO, ';
                    break;
                case "@":
                    $nro_motor_letras .= 'ARROBA, ';
                    break;
                case " ":
                    $nro_motor_letras .= 'ESPACIO, ';
                    break;
                default:
                    $nro_motor_letras .= $value.', ';
                }
            }
        }
        $nro_motor_letras = substr($nro_motor_letras, 0, -2);
    //

    $ep_fecha = fecha_mysql($res_escritura['data'][0]['tb_escriturapublica_fecha']);
    $ep_concl_firmas = $res_escritura['data'][0]['tb_escriturapublica_fechasiguales'] != 1 ? ', con conclusión de firmas el '.fechaActual(fecha_mysql($res_escritura['data'][0]['tb_escriturapublica_fechaconcfirmas'])) : '';
    $ep_nro = intval($res_escritura['data'][0]['tb_escriturapublica_num']);
    $valor_gravamen = $res_escritura['data'][0]['tb_escriturapublica_valorgravamen'];

    $monto_liquidacion = $ejecucion_reg['tb_ejecucion_montoliquidacion'];
    $moneda_liquidacion_id = intval($ejecucion_reg['tb_moneda_id']);//moneda de liquidacion
    $moneda_liquidacion_nom = $moneda_liquidacion_id == 1 ? 'S/. ' : 'US$ ';//nombre de moneda de liquidacion
//

//CLIENTE INFO
    $cliente_nom = $cliente_doc = $cliente_repre_nom = $cliente_repre_doc = '';
    $sing_plural['asumir'] = '';
    if ($ejecucion_reg['tb_cliente_tip'] == 1) {
        $cliente_nom = $ejecucion_reg['tb_cliente_nom'];
        $cliente_doc = $ejecucion_reg['tb_cliente_doc'];

        $cliente_info = '<b>'.$cliente_nom.'</b>, con DNI N° '.$cliente_doc;
        $cliente_info2 = "<b>$cliente_nom</b>";
        $sing_plural['constituir'] = 'constituyó';
        $sing_plural['cumplir'] = 'cumpla';

        //buscar asociaciones
            if ($res_escritura['estado'] == 1) { //modificar el cliente info
                $cliente_ids = explode(',', $res_escritura['data'][0]['tb_cliente_ids']);
                if (count($cliente_ids) > 1) {
                    $lleva_coma = count($cliente_ids) == 2 ? '' : ',';
                    $sing_plural['constituir'] = 'constituyeron';
                    $sing_plural['asumir'] = 'asumieron obligaciones y ';
                    $sing_plural['cumplir'] = 'cumplan';
                    //buscar nom cliente asociado
                    require_once '../cliente/Cliente.class.php';
                    $oCliente = new Cliente();

                    for ($i=1; $i < count($cliente_ids); $i++) {
                        $cli_asoc_id = $cliente_ids[$i];

                        $res_cliente_asoc = $oCliente->mostrarUno($cli_asoc_id);
                        if ($res_cliente_asoc['estado'] == 1) {
                            $tipo_sociedad = $oCliente->verificar_sociedad_cliente($cliente_ids[0], $cliente_ids[$i])['data']['tb_clientedetalle_tip'];
                            if ($tipo_sociedad == 1) {
                                $tipo_sociedad_nom[$i] = 'su cónyugue';
                            } elseif ($tipo_sociedad == 2) {
                                $tipo_sociedad_nom[$i] = 'el copropietario';
                            }
                            $enlace_ult_cli_asoc = $i+1 == count($cliente_ids) ? ' y' : '';
                            $enlace_ult_cli_asoc2 = $i+1 == count($cliente_ids) ? "$lleva_coma y" : "$lleva_coma";

                            $cliente_info .= ",$enlace_ult_cli_asoc {$tipo_sociedad_nom[$i]} <b>".$res_cliente_asoc['data']['tb_cliente_nom']."</b>, con DNI N° ".$res_cliente_asoc['data']['tb_cliente_doc'];
                            $cliente_info2 .= "$enlace_ult_cli_asoc2 {$tipo_sociedad_nom[$i]} <b>{$res_cliente_asoc['data']['tb_cliente_nom']}</b>";
                        }
                        unset($res_cliente_asoc);
                    }
                }
            }
        //
    } else {
        $cliente_nom = $ejecucion_reg['tb_cliente_emprs'];
        $cliente_doc = $ejecucion_reg['tb_cliente_empruc'];
        $cliente_repre_nom = $ejecucion_reg['tb_cliente_nom'];
        $cliente_repre_doc = $ejecucion_reg['tb_cliente_doc'];
        $cliente_cargo_emprs = $credito_datos['tb_cliente_empger'];

        $cliente_info = 'la empresa <b>'.$cliente_nom.'</b>, con RUC N° '.$cliente_doc.', debidamente representada por su '.customUcwords($cliente_cargo_emprs).' <b>'.$cliente_repre_nom.'</b>, con DNI N° '.$cliente_repre_doc;
        $cliente_info2 = "la empresa <b>$cliente_nom</b>, debidamente representada por su ".customUcwords($cliente_cargo_emprs)." <b>$cliente_repre_nom</b>";
        $sing_plural['constituir'] = 'constituyó';
        $sing_plural['cumplir'] = 'cumpla';
    }

    //DIRECCION
        $direccion_cliente = customUcwords($res_escritura['data'][0]['tb_escriturapublica_clientedir']);
        $direccion_cliente .= ', del distrito de '.customUcwords($res_escritura['data'][0]['Distrito_cli']).', provincia de '.customUcwords($res_escritura['data'][0]['Provincia_cli']).', del departamento de '.customUcwords($res_escritura['data'][0]['Departamento_cli']);
    //
//

//datos de demanda
    $pie_pag = '';
    $sup_ind_color = $sup_ind_combu = '';
    $fec_llevar = empty($demanda_reg['tb_demanda_fecllevar']) ? fechaActual(date('Y-m-d')) : fechaActual(fecha_mysql($demanda_reg['tb_demanda_fecllevar']));
    if ($demanda_reg['tb_demanda_cambio'] == 1) {
        $cambios = explode(',', $demanda_reg['tb_demanda_cambio_det']);
        $pie_pag .= '<br>'.
            '<tr>'.
                '<td style="width:5%; text-align: left;"></td>'.
                '<td style="width:95%; text-align: justify;"><b>_______________________</b></td>'.
            '</tr>'
        ;

        if (intval($cambios[0]) == 1) {
            $sup_ind_color = '1';
            $pie_pag .=
                '<tr>'.
                    '<td style="width:5%; text-align: left;"></td>'.
                    '<td style="width:95%; text-align: justify; font-size: 25px;"><sup>1</sup> '.$demanda_reg['tb_demanda_obs_piepag_color'].'</td>'.
                '</tr>'
            ;
        }
        if (intval($cambios[1]) == 1) {
            $sup_ind_combu = $sup_ind_color=='' ? '1' : '2';
            $pie_pag .=
                '<tr>'.
                    '<td style="width:5%; text-align: left;"></td>'.
                    '<td style="width:95%; text-align: justify; font-size: 25px;"><sup>'.$sup_ind_combu.'</sup> '.$demanda_reg['tb_demanda_obs_piepag_combus'].'</td>'.
                '</tr>'
            ;
        }
        $pie_pag .= '<br>';
    }
    $anexo = explode('*', $demanda_reg['tb_demanda_nombre_anexos']);
    $letra_anexo = array('1-A', '1-B', '1-C', '1-D', '1-E', '1-F', '1-G', '1-H', '1-I', '1-J', '1-K', '1-L', '1-M', '1-N', '1-O', '1-P', '1-Q', '1-R', '1-S', '1-T', '1-U', '1-V', '1-W', '1-X', '1-Y', '1-Z');

    $anexos_det = '';
    for ($i = 0; $i < count($anexo); $i++) {
        $anexos_det .=
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:5%; text-align: left;">'.$letra_anexo[$i].'</td>'.
            '<td style="width:86%; text-align: justify;">'.$anexo[$i].'</td>'.
        '</tr>';
    }

    $obs_add = $clausulas_add = '';
    $contador_clausulas2 = 2;
//

//datos del representante
    $rep_ipdn_cargo_id = $ejecucion_reg['tb_cargo_id'];
    $rep_ipdn_cargo_nom = customUcwords($oCargo->mostrarUno($rep_ipdn_cargo_id)['data']['tb_cargo_nom']);

    $rep_ipdn_id = $ejecucion_reg['tb_persona_id'];
    $rep_ipdn = $oPersona->mostrar_uno_id($rep_ipdn_id)['data'];
    $rep_ipdn_nom = $rep_ipdn['tb_persona_ape'].' '.$rep_ipdn['tb_persona_nom'];
    $rep_ipdn_doc = $rep_ipdn['tb_persona_doc'];
//

//datos de las cartas notariales
    $res_carta_rep = $oCarta->listar_todos('', $ejecucion_id, 2, '')['data'][0];
    $res_carta_cli = $oCarta->listar_todos('', $ejecucion_id, 1, 'tb_escriturapublica_clientedir')['data'][0];

    $carta_not_fec['rep'] = date('d/m/Y', strtotime($res_carta_rep['tb_cartanotarial_fecnotariarecepciona']));
    $carta_not_fec['cli'] = date('d/m/Y', strtotime($res_carta_cli['tb_cartanotarial_fecnotariarecepciona']));

    if ($carta_not_fec['cli'] == $carta_not_fec['rep'] && fecha_mysql($res_carta_cli['tb_cartanotarial_fecnotariarecepciona']) == fecha_mysql($res_carta_cli['tb_cartanotarial_fecdiligencia']) && fecha_mysql($res_carta_rep['tb_cartanotarial_fecnotariarecepciona']) == fecha_mysql($res_carta_rep['tb_cartanotarial_fecdiligencia'])) {
        $cartas_diligencia = 'mismo día';
    } else {
        $cartas_diligencia = date('d/m/Y', strtotime($res_carta_rep['tb_cartanotarial_fecdiligencia'])).' y '.date('d/m/Y', strtotime($res_carta_cli['tb_cartanotarial_fecdiligencia']));
    }
//

//ARCHIVOS PARA INCLUIR EN REDACCION. 3 variables. 1: nombres y fecha, 2: solo nombres, 3: nombre + número + fecha
    $res_docs_incluir = null;//array_extras guarda nombre, num y fecha
    $extras_nom_fec = $extras_nom = $extras_nom_num_fec = $tr_medios_probatorios = '';
    $index_medios_probatorios = 2;
    if (intval($ejecucion_reg['tb_cgarvtipo_id']) != 1) { //si es constitucion buscar si tiene otros documentos para incluir en la redaccion
        $oEjecucionfasefile->incluir_redac = 1;
        $res_docs_incluir = $oEjecucionfasefile->listar('', '', $ejecucion_id, '', '', '', '', '');
        if ($res_docs_incluir['estado'] == 1) {
            $i = 1;
            foreach ($res_docs_incluir['data'] as $key => $doc) {
                $enlace_extras = ',';
                if ($i == $res_docs_incluir['cantidad']) {
                    $enlace_extras = ' y';
                }

                $extras_nom .= "$enlace_extras ".customUcwords($doc['tb_ejecucionfasefile_archivonom']);

                //obtener la ultima letra de la primera palabra, si es a asignar "la", de lo contrario "el"
                    $ult_letra = substr(explode(' ', $doc['tb_ejecucionfasefile_archivonom'])[0], -1);
                    $extras_nom_fec .= ($enlace_extras == ',' ? ',' : ', y')." ".(strtolower($ult_letra) == 'a' ? 'la' : 'el')." ".customUcwords($doc['tb_ejecucionfasefile_archivonom']). (!empty($doc['tb_ejecucionfasefile_fecnoti']) ? ", de fecha ".strtolower(fechaActual(fecha_mysql($doc['tb_ejecucionfasefile_fecnoti']))) : '');
                //

                $extras_nom_num_fec = customUcwords($doc['tb_ejecucionfasefile_archivonom']);
                if (!empty($doc['tb_ejecucionfasefile_num'])) {
                    $extras_nom_num_fec .= ' N° ' . $doc['tb_ejecucionfasefile_num'];
                }
                if (!empty($doc['tb_ejecucionfasefile_fecnoti'])) {
                    $extras_nom_num_fec .= ' de fecha ' . strtolower(fechaActual(fecha_mysql($doc['tb_ejecucionfasefile_fecnoti'])));
                }

                $tr_medios_probatorios .=
                '<tr>'.
                    '<td style="width:14%; text-align: left;"></td>'.
                    '<td style="width:3%; text-align: left;">'.$index_medios_probatorios.'.</td>'.
                    '<td style="width:83%; text-align: justify;">'.$extras_nom_num_fec.'.</td>'.
                '</tr>';

                $index_medios_probatorios++; $extras_nom_num_fec = ''; $i++;
            }
        }
        $oEjecucionfasefile->incluir_redac = null;
    }
//

//CLAUSULAS DIFERENTES CUANDO ES CONSTITUCION Y PRE-CONSTITUCION
    $cls_depositario = $ejecucion_reg['tb_cgarvtipo_id'] == 1 ? 'Octava' : 'Quinta';
    $cls_parte_iv = $ejecucion_reg['tb_cgarvtipo_id'] == 1 ? 'Décimo Quinta' : 'Novena';
    $index_anexos_cartas = 3;
    if ($ejecucion_reg['tb_cgarvtipo_id'] != 1) {
        $index_anexos_cartas += $res_docs_incluir['cantidad'];
    }
//

//Redaccion autoincremental
    //obs adicionales en los datos de la demanda
    $clausulas2 =
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:5%; text-align: left;">2.1.</td>'.
            '<td style="width:86%; text-align: justify;">Mediante Escritura Pública de '.$tipo_cgv.' de Garantía Mobiliaria, de fecha '.fechaActual($ep_fecha).$ep_concl_firmas.$extras_nom_fec.
            ', '.$cliente_info2.', '.$sing_plural['constituir'].' a favor de la empresa <b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b>, '.
            'primera y preferencial Garantía Mobiliaria sobre el bien de su propiedad, detallado en el petitorio de la presente  demanda, hasta por el monto de USD $ '.mostrar_moneda($valor_gravamen).' '.
            '('.numtoletras($valor_gravamen, 2).'), garantizando de esta manera ante la Empresa recurrente el cumplimiento de todas sus deudas y obligaciones, conforme consta en la '.
            'cláusula segunda del Testimonio antes referido.</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:5%; text-align: left;">2.2.</td>'.
            '<td style="width:86%; text-align: justify;">Que de acuerdo con la cláusula '.$cls_depositario.' de la Escritura Pública de '.$tipo_cgv.' de Garantía Mobiliaria, '.$cliente_info2.', '
            .$sing_plural['asumir'].'se '.$sing_plural['constituir'].' como DEPOSITARIO del bien mueble otorgado en garantía mobiliaria, asumiendo las responsabilidades civiles y penales que correspondan.</td>'.
        '</tr>'.$obs_add;
    //

    //aqui hacer un bucle para recorrer las clausulas adicionales, se modifica $clausulas2
    //

    $clausulas2 .=
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:5%; text-align: left;">2.'.($contador_clausulas2+1).'.</td>'.
            '<td style="width:86%; text-align: justify;">Ante el incumplimiento de la obligación garantizada en la Escritura Pública de '.$tipo_cgv.' de Garantía Mobiliaria'.$extras_nom.
            ', es que procedimos mediante Cartas Notariales del '.$carta_not_fec['rep'].' y '.$carta_not_fec['cli'].' diligenciadas el '.$cartas_diligencia.' al REPRESENTANTE y a EL DEUDOR/CONSTITUYENTE/DEPOSITARIO, respectivamente; '.
            'a dar por vencidos todos los plazos concedidos y a informar del inicio de las acciones destinadas a procurar el recupero de nuestra acreencia que actualmente asciende a la suma de '.
            $moneda_liquidacion_nom.' '.mostrar_moneda($monto_liquidacion).' ('.numtoletras($monto_liquidacion).'), incluye capital, intereses, moras, costas, costos, gastos por resolución y ejecución; '.
            'y para que en el plazo perentorio de 72 horas (3 días) de recibida la citada carta '.$sing_plural['cumplir'].' con poner a disposición el bien dado en garantía, tal como lo demuestran los anexos '.
            str_replace('-', '', $letra_anexo[$index_anexos_cartas]).' y '.str_replace('-', '', $letra_anexo[($index_anexos_cartas+1)]).' del presente documento.</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:5%; text-align: left;">2.'.($contador_clausulas2+2).'.</td>'.
            '<td style="width:86%; text-align: justify;">En mérito a lo expuesto y al no haberse cumplido con la restitución del bien mueble detallado en el petitorio de la presente demanda, '.
            'es que recurrimos a vuestro despacho a efectos de solicitar su incautación del lugar de depósito, sustentándose en el incumplimiento de las obligaciones garantizadas a través de la '.
            'Escritura Pública de '.$tipo_cgv.' de Garantía Mobiliaria. Debiéndose precisar que por la naturaleza de la pretensión solicitamos se emita las órdenes de incautación prescindiendo de la '.
            'notificación de la demanda de acuerdo al art. 51 ° de la Ley de Garantía Mobiliaria.</td>'.
        '</tr>';
    //
//

$title = 'DEMANDA_CASO_'.str_replace(' ', '', $cliente_nom).''. date('YmdHis');

//TCPDF
if ($tipo_documento == 'pdf') {
    class MYPDF extends TCPDF {
        public function Header() {
            //vacio pq no necesita header ni footer
        }
        public function Footer() {
            //vacio pq no necesita header ni footer
        }
    }
    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('www.prestamosdelnortechiclayo.com');
    $pdf->SetSubject('www.prestamosdelnortechiclayo.com');
    $pdf->SetKeywords('www.prestamosdelnortechiclayo.com');
    $pdf->SetTitle($title);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    //set margins
    $pdf->SetMargins(23, 26, 27); // left top right
    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // add a page
    $pdf->AddPage('P', 'A4');
}

$html =
'<table>'.
    //EXPEDIENTE, ESCPECIALISTA, SUMILLA
        '<tr>'.
            '<td style="width:42%; text-align: left;"></td>'.
            '<td style="width:18%; text-align: left;">Expediente</td>'.
            '<td style="width:40%; text-align: left;">:</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:42%; text-align: left;"></td>'.
            '<td style="width:18%; text-align: left;">Especialista legal</td>'.
            '<td style="width:40%; text-align: left;">:</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:42%; text-align: left;"></td>'.
            '<td style="width:18%; text-align: left;">Escrito N°</td>'.
            '<td style="width:40%; text-align: left;">: 01</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:42%; text-align: left;"></td>'.
            '<td style="width:18%; text-align: left;">Sumilla</td>'.
            '<td style="width:40%; text-align: left;">: <b>INCAUTACIÓN DE BIEN MUEBLE</b></td>'.
        '</tr>'.
    //
    '<br><br>'.
    //EMPRESA INVERSIONES Y PRESTAMOS DEL NORTE CON RUC
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify;"><b>SEÑOR JUEZ DEL JUZGADO ESPECIALIZADO CIVIL SUB ESPECIALIDAD COMERCIAL DE CHICLAYO.</b></td>'.
        '</tr>'.
        '<br><br>'.
        '<tr>'.
            '<td style="width:42%; text-align: left;"></td>'.
            '<td style="width:58%; text-align: justify;">'.
                '<u><b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b></u> CON R.U.C. N° 20600752872, inscrita en la partida registral N° 11218951 del Registro de Personas Jurídicas de la Oficina Registral de Chiclayo '.
                'y con domicilio fiscal en Centro Comercial Boulevard Plaza Ext. A-07, primer piso, distrito de Chiclayo, provincia de Chiclayo, departamento de Lambayeque; con casilla electrónica N° 15474, '.
                'casilla judicial N° 1028; debidamente representada por su '.$rep_ipdn_cargo_nom.' <b>'.$rep_ipdn_nom.'</b>, con DNI N° '.$rep_ipdn_doc.', a usted atentamente decimos:'.
            '</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //I. PETITORIO
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left;"><b>I.</b></td>'.
            '<td style="width:91%; text-align: left;"><b><u>PETITORIO:</u></b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:91%; text-align: justify; text-indent: 22px;">Que interponemos demanda de Incautación de bien Mueble en la vía Proceso Sumarísimo, '.
            'contra '.$cliente_info.', en calidad de '.
            'DEUDOR / CONSTITUYENTE / DEPOSITARIO DEL BIEN MUEBLE DADO EN GARANTÍA MOBILIARIA, a efectos de que se ORDENE LA INCAUTACIÓN DE DICHO BIEN MUEBLE, el mismo que detallamos a continuación; '.
            'el lugar de depósito está ubicado en: <b>'.$direccion_cliente.'</b>, conforme se acordó en la Escritura Pública de '.$tipo_cgv.' de Garantía Mobiliaria.</td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:91%; text-align: justify; text-indent: 22px;">El bien mueble cuya garantía requerimos consiste en un VEHÍCULO CON PLACA DE RODAJE Nº <b>'.$placa.'</b>; '.'
            cuyas características actuales, según CERTIFICADO REGISTRAL VEHICULAR de la partida N° '.$nro_partida.', y de fecha '.$fecha_crv.', son: CARROCERÍA: <b>'.$carroceria.'</b>, MARCA: <b>'.$marca.'</b>, '.
            'MODELO: <b>'.$modelo.'</b>, AÑO DE MODELO: <b>'.$anio.'</b>, COLOR: <b>'.$color.'<sup>'.$sup_ind_color.'</sup></b>, N° DE SERIE: <b>'.$nro_serie.' ('.$serie_chasis_letras.')</b>, '.
            'N° DE MOTOR: <b>'.$nro_motor.' ('.$nro_motor_letras.')</b>, COMBUSTIBLE: <b>'.$combustible.'<sup>'.$sup_ind_combu.'</sup></b>, EL CUAL EN LA ACTUALIDAD MUESTRA COMO PLACA DE RODAJE Nº '.
            '<b>'.$placa.'</b> DEL REGISTRO DE PROPIEDAD VEHICULAR DE LA '.$zona_reg.'.</td>'.
        '</tr>'.
        '<br>'.
        $pie_pag.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:91%; text-align: justify; text-indent: 22px;">Para que se ejecute la orden de incautación solicitamos se ordene el apoyo de la fuerza pública, para lo cual vuestro despacho se servirá disponer su captura a nivel '.
            'nacional, y oficiar a la jefatura de tránsito de la Policía Nacional del Perú - Sede Chiclayo; asimismo dispondrá la entrega inmediata del bien mueble afecto en garantía mobiliaria a la empresa '.
            '<b>INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C.</b>, debidamente representada por su '.$rep_ipdn_cargo_nom.' '.$rep_ipdn_nom.'. Así también, solicitamos se ordene descerraje en caso sea necesario, prescindiendo de '.
            'la notificación al demandado, de acuerdo a lo establecido en el Art. 51° de la Ley de Garantía Mobiliaria N° 28677.</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //II. FUNDAMENTOS DEL HECHO
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left;"><b>II.</b></td>'.
            '<td style="width:91%; text-align: left;"><b><u>FUNDAMENTOS DEL HECHO:</u></b></td>'.
        '</tr>'.
        '<br>'.
        $clausulas2.
    //
    '<br><br>'.
    //III. FUNDAMENTOS DE DERECHO
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left;"><b>III.</b></td>'.
            '<td style="width:91%; text-align: left;"><b><u>FUNDAMENTOS DE DERECHO:</u></b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:6%; text-align: left;">-</td>'.
            '<td style="width:85%; text-align: justify;">Amparamos nuestro petitorio en los artículos 51° y 52° de la Ley de Garantía Mobiliaria y demás pertinentes referentes ala ejecución de Garantía Mobiliaria.</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:6%; text-align: left;">-</td>'.
            '<td style="width:85%; text-align: justify;">Así también, nuestra demanda se viabiliza conforme lo señala el Art. 704°, Art. 705° y demás pertinentes de nuestro Código Procesal Civil.</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //IV. VIA PROCEDIMENTAL Y COMPETENCIA JURISDICIONAL
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left;"><b>IV.</b></td>'.
            '<td style="width:91%; text-align: left;"><b><u>VIA PROCEDIMENTAL Y COMPETENCIA JURISDICIONAL:</u></b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:91%; text-align: justify;">De conformidad a lo establecido en el Art. 51° de la Ley de Garantía Mobiliaria-Ley N° 28677, la presente demanda deberá tramitarse en Vía del '.
            '<b>PROCESO SUMARÍSIMO</b>, asimismo de acuerdo a lo establecido en el artículo antes mencionado es competente el <b>JUEZ ESPECIALIZADO CIVIL SUB ESPECIALIDAD COMERCIAL DE CHICLAYO</b>, pues en la '.
            'cláusula '.$cls_parte_iv.' de la Garantía Mobiliaria las partes se sometieron a la jurisdicción y competencia de los jueces de la ciudad de Chiclayo, esto es del distrito judicial de Lambayeque.</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //V. MEDIOS PROBATORIOS
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left;"><b>V.</b></td>'.
            '<td style="width:91%; text-align: left;"><b><u>MEDIOS PROBATORIOS:</u></b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:9%; text-align: left;"></td>'.
            '<td style="width:91%; text-align: justify;">Que, a fin de respaldar nuestro petitorio y de conformidad con lo dispuesto en los Artículos 188° y 425º del Código Procesal Civil, adjuntamos los siguientes medios probatorios:</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:3%; text-align: left;">1.</td>'.
            '<td style="width:83%; text-align: justify;">Testimonio de la Escritura Pública de '.$tipo_cgv.' de Garantía Mobiliaria Nº '.$ep_nro.', de fecha '.fechaActual($ep_fecha).$ep_concl_firmas.'.</td>'.
        '</tr>'.
        $tr_medios_probatorios.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:3%; text-align: left;">'.$index_medios_probatorios.'.</td>'.
            '<td style="width:83%; text-align: justify;">Carta Notarial de fecha '.fechaActual(fecha_mysql($res_carta_rep['tb_cartanotarial_fecnotariarecepciona'])).' diligenciada el '.fechaActual(fecha_mysql($res_carta_rep['tb_cartanotarial_fecdiligencia'])).', dirigida al representante.</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:3%; text-align: left;">'.($index_medios_probatorios+1).'.</td>'.
            '<td style="width:83%; text-align: justify;">Carta Notarial de fecha '.fechaActual(fecha_mysql($res_carta_cli['tb_cartanotarial_fecnotariarecepciona'])).' diligenciada el '.fechaActual(fecha_mysql($res_carta_cli['tb_cartanotarial_fecdiligencia'])).', dirigida al deudor /constituyente /depositario.</td>'.
        '</tr>'.
        '<tr>'.
            '<td style="width:14%; text-align: left;"></td>'.
            '<td style="width:3%; text-align: left;">'.($index_medios_probatorios+2).'.</td>'.
            '<td style="width:83%; text-align: justify;">Certificado de Gravámenes y Cargas, otorgado por el Registro de Propiedad Vehicular de Chiclayo del vehículo de Placa <b>'.$placa.'</b>, donde consta inscrita la '.
            'Garantía Mobiliaria y su forma de ejecución.</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //VI. ANEXOS
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:4%; text-align: left;"><b>VI.</b></td>'.
            '<td style="width:91%; text-align: left;"><b><u>ANEXOS:</u></b></td>'.
        '</tr>'.
        '<br>'.
        $anexos_det.
    //
    '<br><br>'.
    //OTROSI
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify;"><b>PRIMER OTROSI DECIMOS:</b> De conformidad con lo dispuesto por el Art. 290 ° de la Ley Orgánica del Poder Judicial, otorgamos al letrado que '.
            'autoriza el presente escrito, las facultadas generales de representación a que se refiere el Art. 74° del C.P.C., para cuyo efecto declaramos estar instruido de los alcances de la representación que delegamos.</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //OTROSI
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify;"><b>SEGUNDO OTROSI DECIMOS:</b> De conformidad con lo señalado por el artículo 138 del Código Procesal Civil, autorizamos a '.
            'GIORDANA MATILDE GARCÍA SANTOS, identificada con DNI N° 44404760, para la revisión del expediente, así como la tramitación de oficios y demás actos procesales pertinentes que se realicen '.
            'en el presente proceso.</td>'.
        '</tr>'.
    //
    '<br><br>'.
    //FINAL
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify;"><b>POR LO TANTO:</b></td>'.
        '</tr>'.
        '<br>'.
        '<tr>'.
            '<td style="width:13%; text-align: left;"></td>'.
            '<td style="width:87%; text-align: justify; text-indent: 22px;">Sírvase Usted, Sr. Juez calificar de manera positiva nuestra demanda y dictar la orden de incautación respectiva.</td>'.
        '</tr>'.
        '<br><br>'.
        '<tr>'.
            '<td style="width:5%; text-align: left;"></td>'.
            '<td style="width:95%; text-align: justify;">Chiclayo, '.$fec_llevar.'.</td>'.
        '</tr>'.
    //FINAL
'</table>'
;

if ($tipo_documento == 'word') {
    // Crear una instancia de PHPWord
    $phpWord = new PhpWord();

    $phpWord->setDefaultFontName('Arial');
    $phpWord->setDefaultFontSize(10);

    // Definir estilo de párrafo
    $paragraphStyle = array('spaceAfter' => 0, 'spacing' => 0);

    // Crear una nueva sección
    $section1 = $phpWord->addSection();

    // Renderizar el contenido HTML 1
    Html::addHtml($section1, $html, false, false, $paragraphStyle);

    // Guardar el documento
    $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save('demanda.docx');
    echo 'Documento para cliente: ' . $cliente_nom . ', generado correctamente...';
    header('Location: ../demanda/demanda.docx');
}

if ($tipo_documento == 'pdf') {
    $pdf->SetFont('Arial', '', 10);
    $pdf->writeHTML($html, true, 0, true, true);
    $pdf->Ln();
    // reset pointer to the last page
    $pdf->lastPage();
    //Close and output PDF document
    $nombre_archivo = $title . ".pdf";
    $pdf->Output($nombre_archivo, 'I');
}


