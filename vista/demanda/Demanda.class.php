<?php
require_once '../../datos/conexion.php';

class Demanda extends Conexion {
    public $id;
    public $ejecucion_id;
    public $cambio;
    public $cambio_det = null;
    public $boleta_tabla = null;
    public $boleta_reg_id = null;
    public $boleta_documentosunat = null;
    public $boleta_fecha_emi = null;
    public $asiento_color_upload_id = null;
    public $asiento_color_fecha = null;
    public $asiento_combus_upload_id = null;
    public $asiento_combus_fecha = null;
    public $obs_piepag_color = null;
    public $obs_piepag_combus = null;
    public $nombre_anexos;
    public $fecllevar;
    public $medcau_upload_id;
    public $medcau_gastopago_id;
    public $medcau_gastopago_nroticket;
    public $dernot_upload_id;
    public $dernot_gastopago_id;
    public $dernot_gastopago_nroticket;
    public $usureg;
    public $fecreg;
    public $xac;
    public $doc_scanned;
    public $incluyedescer;
    public $dilfue_upload_id;
    public $dilfue_gastopago_id;
    public $dilfue_gastopago_nroticket;
    
    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_demanda (
                        tb_ejecucion_id,
                        tb_demanda_cambio,
                        tb_demanda_cambio_det,
                        tb_demanda_boleta_tabla,
                        tb_demanda_boleta_reg_id,
                        tb_demanda_boleta_documentosunat,
                        tb_demanda_boleta_fecha_emi,
                        tb_demanda_asiento_color_upload_id,
                        tb_demanda_asiento_color_fecha,
                        tb_demanda_asiento_combus_upload_id,
                        tb_demanda_asiento_combus_fecha,
                        tb_demanda_obs_piepag_color,
                        tb_demanda_obs_piepag_combus,
                        tb_demanda_nombre_anexos,
                        tb_demanda_usureg,
                        tb_demanda_incluyedescer)
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5,
                        :param6,
                        :param7,
                        :param8,
                        :param9,
                        :param10,
                        :param11,
                        :param12,
                        :param13,
                        :param14,
                        :param15)";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->ejecucion_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->cambio, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->cambio_det, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->boleta_tabla, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->boleta_reg_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param5', $this->boleta_documentosunat, PDO::PARAM_STR);
            $sentencia->bindParam(':param6', $this->boleta_fecha_emi, PDO::PARAM_STR);
            $sentencia->bindParam(':param7', $this->asiento_color_upload_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param8', $this->asiento_color_fecha, PDO::PARAM_STR);
            $sentencia->bindParam(':param9', $this->asiento_combus_upload_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param10', $this->asiento_combus_fecha, PDO::PARAM_STR);
            $sentencia->bindParam(':param11', $this->obs_piepag_color, PDO::PARAM_STR);
            $sentencia->bindParam(':param12', $this->obs_piepag_combus, PDO::PARAM_STR);
            $sentencia->bindParam(':param13', $this->nombre_anexos, PDO::PARAM_STR);
            $sentencia->bindParam(':param14', $this->usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param15', $this->incluyedescer, PDO::PARAM_INT);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_todos($demanda_id, $ejecucion_id, $xac, $order_by, $limit) {
        $where_opt = $order = '';
        if (!empty($demanda_id)) {
            $where_opt .= empty($where_opt) ? 'dem.tb_demanda_id = :param_opc0' : ' AND dem.tb_demanda_id = :param_opc0';
        }
        if (!empty($ejecucion_id)) {
            $where_opt .= empty($where_opt) ? 'dem.tb_ejecucion_id = :param_opc1' : ' AND dem.tb_ejecucion_id = :param_opc1';
        }
        if (in_array(strval($xac), array('0', '1'))) {
            $where_opt .= empty($where_opt) ? 'dem.tb_demanda_xac = :param_opc2' : ' AND dem.tb_demanda_xac = :param_opc2';
        }
        if (!empty($order_by)) {
            $order = "ORDER BY $order_by";
        }

        if (!empty($where_opt)) {
            $where_opt = "WHERE $where_opt";
        }
        try {
            $sql = "SELECT dem.*,
                        u1.upload_url as url_medcau, u1.upload_reg as reg_medcau,
                        u2.upload_url as url_dernot, u2.upload_reg as reg_dernot,
                        u3.upload_url as url_demscan, u3.upload_reg as reg_demscan
                    FROM tb_demanda dem
                    LEFT JOIN upload u1 ON dem.tb_demanda_medcau_upload_id = u1.upload_id
                    LEFT JOIN upload u2 ON dem.tb_demanda_dernot_upload_id = u2.upload_id
                    LEFT JOIN upload u3 ON dem.tb_demanda_doc_scanned = u3.upload_id
                    $where_opt
                    $order $limit;";
            //
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($demanda_id)) {
                $sentencia->bindParam(':param_opc0', $demanda_id, PDO::PARAM_INT);
            }
            if (!empty($ejecucion_id)) {
                $sentencia->bindParam(':param_opc1', $ejecucion_id, PDO::PARAM_INT);
            }
            if (in_array(strval($xac), array('0', '1'))) {
                $sentencia->bindParam(':param_opc2', $xac, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function modificar_campo($demanda_id, $demanda_columna, $demanda_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($demanda_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_demanda SET " . $demanda_columna . " = :demanda_valor WHERE tb_demanda_id = :demanda_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":demanda_id", $demanda_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":demanda_valor", $demanda_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":demanda_valor", $demanda_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
