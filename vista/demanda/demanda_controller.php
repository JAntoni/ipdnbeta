<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/fechas.php';
require_once '../funciones/funciones.php';
require_once '../demanda/Demanda.class.php';
$oDemanda = new Demanda();
require_once '../historial/Historial.class.php';
$oHist = new Historial();
require_once '../upload/Upload.class.php';
$oUpload = new Upload();
require_once '../deposito/Deposito.class.php';
$oDeposito = new Deposito();
require_once '../monedacambio/Monedacambio.class.php';
$oMonedaCambio = new Monedacambio();
require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();
require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();
require_once '../gasto/Gasto.class.php';
$oGasto = new Gasto();
require_once '../gastopago/Gastopago.class.php';
$oGastopago = new Gastopago();

$action = $_POST['action_demanda'];

$data['estado'] = 0;
$data['mensaje'] = "Error al $action";
$usuario_reg_id = $_SESSION['usuario_id'];
$demanda_id = $_POST['hdd_demanda_id'];
$demanda_reg_origin = (array) json_decode($_POST['hdd_demanda_reg']);
$ejecucion_id = $_POST['hdd_demanda_ejecucion_id'];
$ejecucionfase_id = $_POST['hdd_demanda_ejecucionfase_id'];
$credito_id = $_POST['hdd_demanda_credito_id'];
$cgarvtipo = $_POST['hdd_demanda_cgarvtipo_id'];
$fecha_hora = date('d-m-Y H:m a');

$incluye_descerraje = intval($_POST['che_incluye_descerraje']);
$cambio_color = intval($_POST['che_color']);
$cambio_combus = intval($_POST['che_combustible']);
$cant_anexos = $_POST['cantidad_anexos'];
$anexos = '';

for ($i=0; $i < $cant_anexos; $i++) {
	$anexos .= $_POST["txt_anexo_nom_$i"].'*';
}
$anexos = substr($anexos, 0, -1);

$oDemanda->ejecucion_id = $_POST['hdd_demanda_ejecucion_id'];
$oDemanda->cambio = ($cambio_color == 1 || $cambio_combus == 1) ? 1 : 0;
$oDemanda->incluyedescer = empty($incluye_descerraje) ? null : $incluye_descerraje;
$oDemanda->nombre_anexos = $anexos;

if ($oDemanda->cambio == 1) {
	if (intval($cgarvtipo) == 1) {
		$oDemanda->boleta_tabla = $_POST['hdd_demanda_boleta_tabla'];
		$oDemanda->boleta_reg_id = $_POST['hdd_demanda_boleta_tabla_id'];
		$oDemanda->boleta_documentosunat = strtoupper($_POST['txt_demanda_boleta_documentosunat']);
		$oDemanda->boleta_fecha_emi = fecha_mysql($_POST['txt_demanda_boleta_fecha_emi']);
	}
	$oDemanda->cambio_det = "$cambio_color,$cambio_combus";
	$oDemanda->asiento_color_upload_id = $_POST['hdd_demanda_asiento_color_upload_id'];
	$oDemanda->asiento_color_fecha = fecha_mysql($_POST['txt_demanda_asiento_color_fecha']);
	$oDemanda->asiento_combus_upload_id = $_POST['hdd_demanda_asiento_combus_upload_id'];
	$oDemanda->asiento_combus_fecha = fecha_mysql($_POST['txt_demanda_asiento_combus_fecha']);
	$oDemanda->obs_piepag_color = $_POST['txt_demanda_obs_color_piepag'];
	$oDemanda->obs_piepag_combus = $_POST['txt_demanda_obs_combus_piepag'];
}

if ($action == 'insertar') {
	$oDemanda->usureg = $usuario_reg_id;
	$res_insert = $oDemanda->insertar();

	if ($res_insert['estado'] == 1) {
		$data['mensaje'] = "✔ Demanda registrada correctamente";

		//actualizar la ruta el modulo id del upload, colocar id del tb_demanda registrados
		if ($oDemanda->cambio == 1) {
			if ($cambio_color == 1 && $oUpload->modificar_campo($oDemanda->asiento_color_upload_id, 'modulo_id', $res_insert['nuevo'], 'INT') != 1) {
				$data['mensaje'] .= "<br>❌ Error al actualizar upload del cambio de color con id del tb_demanda. Actualizar la tabla upload con id {$oDemanda->asiento_color_upload_id}, columna modulo_id: {$res_insert['nuevo']}";
				echo json_encode($data); exit();
			}
			if ($cambio_combus == 1 && $oUpload->modificar_campo($oDemanda->asiento_combus_upload_id, 'modulo_id', $res_insert['nuevo'], 'INT') != 1) {
				$data['mensaje'] .= "<br>❌ Error al actualizar upload del cambio de combustible con id del tb_demanda. Actualizar la tabla upload con id {$oDemanda->asiento_combus_upload_id}, columna modulo_id: {$res_insert['nuevo']}";
				echo json_encode($data); exit();
			}
		}
		$data['estado'] = 1;
		//HISTORIALES
			$oHist->setTbHistUsureg($usuario_reg_id);

			//guardar historial tb_hist de la tabla tb_demanda
			$oHist->setTbHistNomTabla('tb_demanda');
			$oHist->setTbHistRegmodid($res_insert['nuevo']);
			$oHist->setTbHistDet("Registró los datos de la Demanda en el proceso de ejecucion legal $ejecucion_id | <b>". date("d-m-Y h:i a").'</b>');
			$oHist->insertar();

			//guardar historial tb_hist de la tabla tb_ejecucionfase
			$oHist->setTbHistNomTabla('tb_ejecucionfase');
			$oHist->setTbHistRegmodid($ejecucionfase_id);
			$oHist->insertar();
		//
	}
}
elseif ($action == 'consultar_fecha') {
	$fecha = fecha_mysql($_POST['fecha']);
	$data['fecha'] = strtolower(fechaActual($fecha));
	$data['estado'] = $data['fecha'] != '' ? 1 : 0;
}
elseif ($action == 'eliminar') {
	$data['mensaje'] = '';
	if ($oDemanda->cambio == 1) {
		if ($cambio_color == 1) {
			if ($oUpload->eliminar_xac($oDemanda->asiento_color_upload_id)) {
				//OBTENER URL DEL PDF PARA ELIMINARLO POR COMPLETO DEL DIRECTORIO
				$ruta = '';
				$result = $oUpload->mostrarUno($oDemanda->asiento_color_upload_id);
				if ($result['estado'] == 1) {
					$ruta = '../../' . $result['data']['upload_url'];
				}
				unset($result);

				if ($ruta != '') {
					// Verificar si pdf existe
					if (file_exists($ruta)) {
						// Intentar eliminar pdf
						if (unlink($ruta)) {
							if ($oUpload->eliminar($oDemanda->asiento_color_upload_id)) {
								$data['mensaje'] .= '✔ Pdf de asiento (color) eliminado correctamente<br>';

								//eliminar todos los historiales de este upload
								$oHist->eliminar('', 'upload', $oDemanda->asiento_color_upload_id, '', '', '');
							}
						} else {
							$data['mensaje'] .= '❌ Existe un error al eliminar el documento de asiento (color), se rechaza la eliminacion del pdf en el directorio.<br>';
						}
					} else {
						$data['mensaje'] .= "❌ El pdf no existe en el directorio: $ruta<br>";
					}
				}
			}
			else {
				$data['mensaje'] .= '❌ Hubo error al cambiar el xac del upload del color con id: '.$oDemanda->asiento_color_upload_id.'. Además eliminar el file en esa ruta.<br>';
			}
		}
		if ($cambio_combus == 1) {
			if ($oUpload->eliminar_xac($oDemanda->asiento_combus_upload_id)) {
				//OBTENER URL DEL PDF PARA ELIMINARLO POR COMPLETO DEL DIRECTORIO
				$ruta = '';
				$result = $oUpload->mostrarUno($oDemanda->asiento_combus_upload_id);
				if ($result['estado'] == 1) {
					$ruta = '../../' . $result['data']['upload_url'];
				}
				unset($result);

				if ($ruta != '') {
					// Verificar si pdf existe
					if (file_exists($ruta)) {
						// Intentar eliminar pdf
						if (unlink($ruta)) {
							if ($oUpload->eliminar($oDemanda->asiento_combus_upload_id)) {
								$data['mensaje'] .= '✔ Pdf de asiento (combustible) eliminado correctamente<br>';

								//eliminar todos los historiales de este upload
								$oHist->eliminar('', 'upload', $oDemanda->asiento_combus_upload_id, '', '', '');
							}
						} else {
							$data['mensaje'] .= '❌ Existe un error al eliminar el documento de asiento (combustible), se rechaza la eliminacion del pdf en el directorio.<br>';
						}
					} else {
						$data['mensaje'] .= "❌ El pdf no existe en el directorio: $ruta<br>";
					}
				}
			}
			else {
				$data['mensaje'] .= '❌ Hubo error al cambiar el xac del upload del combustible con id: '.$oDemanda->asiento_combus_upload_id.'. Además eliminar el file en esa ruta.<br>';
			}
		}
	}

	$res_elim = $oDemanda->modificar_campo($demanda_id, 'tb_demanda_xac', 0, 'INT');
	if ($res_elim == 1) {
		$data['estado'] = 1;
		$data['mensaje'] .= '✔ Eliminado correctamente';

		//eliminar todos los historiales de esta demanda
		$oHist->eliminar('', 'tb_demanda', $demanda_id, '', '', '');

		$oHist->setTbHistUsureg($usuario_reg_id);
		$oHist->setTbHistNomTabla('tb_ejecucionfase');
		$oHist->setTbHistRegmodid($ejecucionfase_id);
		$oHist->setTbHistDet("Ha eliminado los datos para redaccion de Demanda, en el proceso de ejecución legal id $ejecucion_id, tb_demanda_id $demanda_id | <b>$fecha_hora</b>");
		$oHist->insertar();
	}
}
elseif ($action == 'insertar_demandagasto') {
	//aqui se debe guardar historial del gasto, guardar pdf
	$tipo_gasto = $_POST['hdd_demanda_gasto_tipo'];
	$demanda_id = $_POST['hdd_demanda_id'];
	$ejecucionfase_id = $_POST['hdd_demandagasto_ejecucionfase_id'];
	$ejecucion_id = $_POST['hdd_demandagasto_ejecucion_id'];
	$credito_id = $_POST['hdd_demandagasto_credito_id'];
	$upload_id = $_POST['hdd_demanda_upload_id'];
	$fecha_pago = fecha_mysql($_POST['txt_demanda_fechapago']);
	$moneda_gastopago = $_POST['cbo_demanda_gastomoneda_id'];
	$monto_gasto = $_POST['txt_demanda_gastomonto'];
	$forma_egreso = $_POST['cbo_demanda_forma_egreso'];
	$nro_operacion = $_POST['txt_demanda_numope'];
	$moneda_nom = $_POST['gasto_moneda_nom'];
	$nro_ticket = $_POST['txt_demanda_gastopago_nroticket'];
	$cliente_nom = $_POST['hdd_demandagasto_cliente_nom'];

	//antes de cualquier accion verificar el nro operacion
		if ($forma_egreso == 2) {
			$consulta_deposito = $oDeposito->listar_depositos_paragasto($nro_operacion);
			$deposito_registrado = $consulta_deposito['estado'];
			$deposito = $consulta_deposito['data'];
			$tc_compra_deposito = $deposito['tb_deposito_fec'];

			if($deposito_registrado == 0){
				$data['mensaje'] .=  '. El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN/CONTABILIDAD por favor';
			} else {
				$result = $oMonedaCambio->consultar($tc_compra_deposito);
				if ($result['estado'] == 1) {
					$tc_compra_deposito = $result['data']['tb_monedacambio_com'];

					$importe_ingresos_anteriores = 0;
					$dts = $oIngreso->lista_ingresos_num_operacion($nro_operacion);

					if ($dts['estado'] == 1) {
						foreach ($dts['data'] as $key => $dt) {
							$tb_ingreso_imp = moneda_mysql($dt['tb_ingreso_imp']);
							if (1 != intval($dt['tb_moneda_id'])) {
								$importe_ingresos_anteriores += floatval($tb_ingreso_imp * $tc_compra_deposito);
							} else {
								$importe_ingresos_anteriores += $tb_ingreso_imp;
							}
						}
					}
					unset($dts);

					$moneda_deposito = intval($deposito['tb_moneda_id']);
					$monto_deposito_orig = abs($deposito['tb_deposito_mon']);
					$monto_deposito = abs($deposito['tb_deposito_mon']);
					if (1 != $moneda_deposito) {
						$monto_deposito = floatval($monto_deposito * $tc_compra_deposito);
					}
					$monto_puede_usar = moneda_mysql($monto_deposito - $importe_ingresos_anteriores);

					if (moneda_mysql($monto_gasto) > $monto_puede_usar) {
						$deposito_registrado = 2;
						$data['mensaje'] .= '. El monto ingresado es superior al monto disponible del depósito. No puede usar más de lo depositado. ' .
						'Monto ingresado: S/.' . $monto_gasto . ', monto disponible: S/.' . mostrar_moneda($monto_puede_usar) . ', ';
						$data['mensaje'] .= 'TOTAL DEL DEPOSITO: ' . $moneda_nom[$moneda_deposito] . mostrar_moneda($monto_deposito_orig);
						$data['mensaje'] .= 1 != $moneda_deposito ? ' Ó S/.' . mostrar_moneda($monto_deposito) : '';
					}
				} else {
					$deposito_registrado = 3;
					$data['mensaje'] .= '. Verificar el tipo de cambio de la fecha '. mostrar_fecha($tc_compra_deposito);
				}
				unset($result);
			}
			unset($consulta_deposito);
		}
		if ($forma_egreso == 2 && $deposito_registrado != 1) {
			echo json_encode($data); exit();
		}
	//fin de verificacion nro operacion

	$data['mensaje'] = '';

	//si llega aqui es que todo ok con el nro operación, o el pago fue efectivo. Registrar gasto y gastopago
	//generar el gastopago
		$oGasto->tb_gasto_cretip	= 3;//garveh
		$oGasto->tb_credito_id		= $credito_id;
		$oGasto->tb_gasto_tipgas	= 2;//1:producto, 2:servicio
		$oGasto->tb_proveedor_id	= 18;//IPDN
		$oGasto->tb_gasto_can		= 1;//cantidad
		$oGasto->tb_moneda_id		= 1;//tasas y dere notif siempre en soles
		$oGasto->tb_gasto_pvt		= moneda_mysql($monto_gasto);
		$oGasto->tb_gasto_ptl		= moneda_mysql($monto_gasto);
		$oGasto->tb_gasto_his		= "Datos de gasto ingresados por: <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b> | <b>$fecha_hora</b><br>";
		if ($tipo_gasto == 'dilfue') {
			$tipo_gasto_nom = 'TASA POR DILIGENCIA FUERA DE JUZGADO ';
		}
		elseif ($tipo_gasto == 'medcau') {
			$tipo_gasto_nom = 'MEDIDAS CAUTELARES ';
		} else {
			$tipo_gasto_nom = 'DERECHO DE NOTIFICACIÓN ';
		}
		$oGasto->tb_gasto_des .= 'GASTOS JUDICIALES PARA PRESENTACION DE DEMANDA: '.$tipo_gasto_nom.$moneda_nom.$monto_gasto.'. N° TICKET '.$nro_ticket;
		
		$res_insert_gasto = $oGasto->insertar();
		if ($res_insert_gasto['estado'] == 1) {
			// DATOS PARA EL EGRESO
			$gasto_id = $res_insert_gasto['nuevo'];
			$gasto_desc = $oGasto->tb_gasto_des . ". Del Cliente: $cliente_nom";

			$egr_det = "EGRESO CRÉDITO GARVEH para pagar el SERVICIO de $gasto_desc. NO EMITE DOCUMENTO";
			//generar egreso
			$oEgreso->egreso_usureg	= $usuario_reg_id;
			$oEgreso->egreso_fec	= $fecha_pago;
			$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS, unica opcion documento
			$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
			$oEgreso->egreso_det	= $egr_det;
			$oEgreso->moneda_id		= 1;
			$oEgreso->egreso_tipcam = 1;
			$oEgreso->egreso_imp	= moneda_mysql($monto_gasto);
			$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
			$oEgreso->cuenta_id		= 10; //GASTOS UNIDADES
			$oEgreso->subcuenta_id	= 34; //MANTENIMIENTO DE UNIDADES
			$oEgreso->proveedor_id = $oGasto->tb_proveedor_id;
			$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
			$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
			$oEgreso->caja_id = 1; // 1: CAJA
			$oEgreso->modulo_id = 10; //10 egreso para pago de gastos vehiculares
			$oEgreso->egreso_modide = $gasto_id; //tb_egreso_modide el id de tb_gasto
			$oEgreso->empresa_id = $_SESSION['empresa_id'];
			$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

			$result_insert_egreso = $oEgreso->insertar();
			if ($result_insert_egreso['estado'] > 0) {
				$egreso_id = $result_insert_egreso['egreso_id'];
				unset($result_insert_egreso);
				
				// en gasto actualizar historial
				$mensaje = "Se hizo un pago de S/. $monto_gasto";
				$mensaje .= ", registrado por <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b>. ";
				$mensaje .= "Egreso id : $egreso_id | <b>$fecha_hora</b><br>";
				$mensaje .= "Se registró el egreso con el detalle: $egr_det | <b>$fecha_hora</b><br>";
				$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $oGasto->tb_gasto_his.$mensaje, 'STR');
		
				//en gasto actualizar est dependiendo de si paga monto completo o paga una parte
				$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 2, 'INT');
			} else {
				$data['mensaje'] .= "❌ Error en el registro de egreso";
				echo json_encode($data); exit();
			}
			
			if ($forma_egreso == 2) {
				if(is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BCP'))){
					$banco_id = 1;
					$subcuenta_ingreso_id = 147;
				} elseif (is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BBVA'))){
					$banco_id = 3;
					$subcuenta_ingreso_id = 148;
				} else {
					$banco_id = 4;
					$subcuenta_ingreso_id = 183;
				}

				$ing_det = "INGRESO DE CAJA POR BANCO, desde la siguente cuenta: [ {$deposito['tb_cuentadeposito_nom']} ], por EGRESO OE $egreso_id. El N° de operación fue: [ $nro_operacion ]. ";
				$ing_det.= "Se usó [ S/.$monto_gasto ] ";
				if (1 != $moneda_deposito) {
					$ing_det.= "ó [ US$".mostrar_moneda(floatval(moneda_mysql($monto_gasto) / $valor_tc_compra_hoy))." ] ";
				}
				$ing_det.= "de un depósito total de [ {$deposito['tb_moneda_nom']}". mostrar_moneda($monto_deposito_orig) ." ], ";
				$ing_det.= "por concepto de $gasto_desc";

				$cuenta_ingreso = 43;
				
				$oIngreso->ingreso_usureg = $usuario_reg_id;
				$oIngreso->ingreso_fec = $fecha_pago;
				$oIngreso->documento_id = 8;//OI - OTROS INGRESOS
				$oIngreso->ingreso_numdoc = '';
				$oIngreso->ingreso_det = $ing_det;
				$oIngreso->ingreso_imp = moneda_mysql($monto_gasto);
				$oIngreso->cuenta_id = $cuenta_ingreso;//INGRESO/EGRESO POR BANCO
				$oIngreso->subcuenta_id = $subcuenta_ingreso_id;
				$oIngreso->cliente_id = 1144;// id del cliente q hace el ingreso por banco: INVERSIONES Y PRESTAMOS DEL NORTE SAC
				$oIngreso->caja_id = 1;//CAJA
				$oIngreso->moneda_id = 1;
				$oIngreso->modulo_id = 0;
				$oIngreso->ingreso_modide = 0;
				$oIngreso->empresa_id = $_SESSION['empresa_id'];
				$oIngreso->ingreso_fecdep = ($forma_egreso == 2) ? $deposito['tb_deposito_fec'] : '';
				$oIngreso->ingreso_numope = $nro_operacion;
				$oIngreso->ingreso_mondep = moneda_mysql($monto_gasto);
				$oIngreso->ingreso_comi = 0;
				$oIngreso->cuentadeposito_id = ($forma_egreso == 2) ? $deposito['tb_cuentadeposito_id'] : '';
				$oIngreso->banco_id = $banco_id;
				$oIngreso->ingreso_ap = 0;//acuerdo de pago, 0
				$oIngreso->ingreso_detex = '';
				
				$result_insert_ingreso = $oIngreso->insertar();
				if ($result_insert_ingreso['estado'] != 1) {
					$data['mensaje'] .= "<br>❌ Error en el registro de ingreso ficticio";
					echo json_encode($data); exit();
				} else {
					$ingreso_id = $result_insert_ingreso['ingreso_id'];
					unset($result_insert_ingreso);
				}
			}

			//ahora registrar el gastopago
			$obs = $oGasto->tb_gasto_des.". TOTAL $moneda_nom$monto_gasto";
			$oGastopago->tb_gastopago_monto = moneda_mysql($monto_gasto);
			$oGastopago->tb_empresa_idfk = $_SESSION['empresa_id'];
			$oGastopago->tb_gasto_idfk = $gasto_id;
			$oGastopago->tb_gastopago_formapago = $forma_egreso == 2 ? $forma_egreso : 1;
			$oGastopago->tb_gastopago_numope = $forma_egreso == 2 ? $nro_operacion : '';
			if ($forma_egreso == 2) {
				$oGastopago->tb_ingreso_idfk = $ingreso_id;
			}
			$oGastopago->tb_gastopago_emitedocsunat = 0;
			$oGastopago->tb_gastopago_usureg = $usuario_reg_id;
			$oGastopago->tb_gastopago_obs = $obs;
			$oGastopago->tb_moneda_idfk = 1;
			$oGastopago->tb_egreso_idfk = $egreso_id;
			$oGastopago->tb_gastopago_fecha = date("Y-m-d");

			$res_insert_gastopago = $oGastopago->insertar();
			if($res_insert_gastopago['estado'] == 1){
				$data['mensaje'] = "✔ Pago de gasto registrado correctamente";
				$gastopago_id = $res_insert_gastopago['nuevo'];

				if ($tipo_gasto == 'dilfue') {
					$oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_anexadolegal', 8, 'INT');
				} elseif ($tipo_gasto == 'medcau') {
					$oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_anexadolegal', 6, 'INT');
				} else {
					$oGastopago->modificar_campo($gastopago_id, 'tb_gastopago_anexadolegal', 7, 'INT');
				}
			}else {
				$data['mensaje'] = "❌ Error en el registro de PagoGasto";
			}
			unset($res_insert_gastopago);
		} else {
			$data['mensaje'] .= "<br>❌ Error al registrar el gasto vehicular";
			echo json_encode($data); exit();
		}
		unset($res_insert_gasto);
	//

	//actualizar la ruta el modulo id del upload, colocar id del tb_certificado registrados
	if ($oUpload->modificar_campo($upload_id, 'modulo_id', $demanda_id, 'INT') != 1) {
		$data['mensaje'] .= "<br>❌ Error al actualizar la tabla upload con id de la demanda";
		echo json_encode($data); exit();
	}

	//modificar los 3 campos del tb_demanda
	$oDemanda->modificar_campo($demanda_id, "tb_demanda_{$tipo_gasto}_upload_id", $upload_id, 'INT');
	$oDemanda->modificar_campo($demanda_id, "tb_demanda_{$tipo_gasto}_gastopago_id", $gastopago_id, 'INT');
	$oDemanda->modificar_campo($demanda_id, "tb_demanda_{$tipo_gasto}_gastopago_nroticket", $nro_ticket, 'STR');

	$data['estado'] = 1;

	//HISTORIALES
		//guardar historial del pdf subido
		$oHist->setTbHistUsureg($usuario_reg_id);
		$oHist->setTbHistNomTabla('upload');
		$oHist->setTbHistRegmodid($upload_id);
		$oHist->setTbHistDet("Ha subido el pdf TICKET GASTO VEHICULAR $tipo_gasto_nom en el proceso de ejecución legal id $ejecucion_id | <b>$fecha_hora</b>");
		$oHist->insertar();

		//guardar historial tb_hist de la tabla tb_certificadoregveh
		$oHist->setTbHistNomTabla('tb_demanda');
		$oHist->setTbHistRegmodid($demanda_id);
		$oHist->setTbHistDet("Registró los datos de los gastos de demanda $tipo_gasto_nom en el proceso de ejecucion legal $ejecucion_id | <b>". date("d-m-Y h:i a").'</b>');
		$oHist->insertar();

		//guardar historial tb_hist de la tabla tb_ejecucionfase
		$oHist->setTbHistNomTabla('tb_ejecucionfase');
		$oHist->setTbHistRegmodid($ejecucionfase_id);
		$oHist->insertar();
	//
}
elseif ($action == 'eliminar_demandagasto') {
	$upload_id = $_POST['hdd_demanda_upload_id'];
	//eliminar del pdf subido
	if ($oUpload->eliminar_xac($upload_id)) {
		$ruta = '';
		//OBTENER URL DE LA IMAGEN PARA ELIMINARLA POR COMPLETO DEL DIRECTORIO
		$result = $oUpload->mostrarUno($upload_id);
		if ($result['estado'] == 1) {
			$ruta = '../../' . $result['data']['upload_url'];
		}
		unset($result);

		if ($ruta != '') {
			// Verificar si pdf existe
			if (file_exists($ruta)) {
				// Intentar eliminar pdf
				if (unlink($ruta)) {
					if ($oUpload->eliminar($upload_id)) {
						$data['estado'] = 1;
						$data['mensaje'] = '✔ Pdf eliminado correctamente';
					}
				} else {
					$data['mensaje'] = '❌ Existe un error al eliminar el documento, se rechaza la eliminacion de la imagen en el directorio.';
					echo json_encode($data); exit();
				}
			} else {
				$data['mensaje'] = "❌ El pdf no existe en el directorio: $ruta";
				echo json_encode($data); exit();
			}
		}
    }

	$demanda_id = $_POST['hdd_demanda_id'];
	$tipo_gasto = $_POST['hdd_demanda_gasto_tipo'];
	$ejecucion_id = $_POST['hdd_demandagasto_ejecucion_id'];
	$ejecucionfase_id = $_POST['hdd_demandagasto_ejecucionfase_id'];
	if ($tipo_gasto == 'dilfue') {
		$tipo_gasto_nom = 'TASA POR DILIGENCIA FUERA DE JUZGADO ';
	} elseif ($tipo_gasto == 'medcau') {
		$tipo_gasto_nom = 'MEDIDAS CAUTELARES ';
	} else {
		$tipo_gasto_nom = 'DERECHO DE NOTIFICACIÓN ';
	}
	$oDemanda->modificar_campo($demanda_id, "tb_demanda_{$tipo_gasto}_upload_id", null, 'INT');
	$oDemanda->modificar_campo($demanda_id, "tb_demanda_{$tipo_gasto}_gastopago_id", null, 'INT');
	$oDemanda->modificar_campo($demanda_id, "tb_demanda_{$tipo_gasto}_gastopago_nroticket", null, 'STR');
	$oHist->eliminar('', 'upload', $upload_id, '', '', '');
	$data['mensaje'] .= '<br>✔ Datos de gasto eliminados correctamente';

	//guardar historial tb_hist de la tabla tb_certificadoregveh
	$oHist->setTbHistUsureg($usuario_reg_id);
	$oHist->setTbHistNomTabla('tb_demanda');
	$oHist->setTbHistRegmodid($demanda_id);
	$oHist->setTbHistDet("Eliminó los datos de los gastos de demanda $tipo_gasto_nom en el proceso de ejecucion legal $ejecucion_id | <b>". date("d-m-Y h:i a").'</b>');
	$oHist->insertar();

	//guardar historial tb_hist de la tabla tb_ejecucionfase
	$oHist->setTbHistNomTabla('tb_ejecucionfase');
	$oHist->setTbHistRegmodid($ejecucionfase_id);
	$oHist->insertar();
}
echo json_encode($data);
