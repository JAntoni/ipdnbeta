<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';
require_once '../upload/Upload.class.php';
$oUpload = new Upload();
require_once '../monedacambio/Monedacambio.class.php';
$oMonedacambio = new Monedacambio();
require_once '../gastopago/Gastopago.class.php';
$oGastopago = new Gastopago();
require_once '../demanda/Demanda.class.php';
$oDemanda = new Demanda();

$usuario_action = $_POST['action'];
$tipo_gasto = $_POST['tipo_gasto'];
$titulo = '';
if ($usuario_action == 'L') {
    $titulo = 'Gasto de Demanda Registrado';
} elseif ($usuario_action == 'I') {
    $titulo = 'Registrar Gasto de Demanda';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Gasto de Demanda';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Gasto de Demanda';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
if ($tipo_gasto == 'medcau') {
    $tipo_gasto_nom = '(medida cautelar)';
} else {
    $tipo_gasto_nom = $tipo_gasto == 'dernot' ? '(derecho notificacion)' : '(tasa por diligencia fuera de juzgado)';
}

$titulo .= " $tipo_gasto_nom";
$action = devuelve_nombre_usuario_action($usuario_action);

$listo = 1;
$caja_aperturada = validar_apertura_caja_2();

$fecha_pago = date('Y-m-d');
$result = $oMonedacambio->consultar($fecha_pago);
$tccompra_hoy = 0;
if ($result['estado'] == 1) {
    $tccompra_hoy = $result['data']['tb_monedacambio_com'];
}
unset($result);

if ($listo != 1) {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
} elseif ($caja_aperturada != 1) {
    $mensaje = 'No has aperturado o ya está cerrada la caja de este día: ' . date('d-m-Y') . ' ' . ' para la sede de ' . substr($_SESSION['empresa_nombre'], 7);
} elseif (empty($tccompra_hoy)) {
    $mensaje = "No hay tipo de cambio registrado en el día ". mostrar_fecha($fecha_pago);
} else {
    $demanda_id = $_POST['demanda_id'];
    $credito_id = intval($_POST['credito_id']);
    $ejecucionfase_id = $_POST['ejecucionfase_id'];
    $ejecucion_id = $_POST['ejecucion_id'];
    $dominio_server = 'https://ipdnsac.ipdnsac.com/';
    //$dominio_server = 'http://192.168.101.90/ipdnsac/';

    $extendido = 'collapsed-box';
    $icono_extendido = 'fa-plus';
    if ($demanda_id > 0) {
        $demanda_reg = $oDemanda->listar_todos($demanda_id, '', '', '', '')['data'][0];

        if (($tipo_gasto == 'medcau' && intval($demanda_reg['tb_demanda_medcau_upload_id']) > 0) ||
            ($tipo_gasto == 'dernot' && intval($demanda_reg['tb_demanda_dernot_upload_id']) > 0) ||
            ($tipo_gasto == 'dilfue' && intval($demanda_reg['tb_demanda_dilfue_upload_id']) > 0)) {
            $extendido = '';
            $icono_extendido = 'fa-minus';

            if ($tipo_gasto == 'dilfue') {
                $upload_id = $demanda_reg['tb_demanda_dilfue_upload_id'];
                $gastopago_id = $demanda_reg['tb_demanda_dilfue_gastopago_id'];
                $nro_ticket = $demanda_reg['tb_demanda_dilfue_gastopago_nroticket'];
            }
            else {
                $upload_id = $tipo_gasto == 'medcau' ? $demanda_reg['tb_demanda_medcau_upload_id'] : $demanda_reg['tb_demanda_dernot_upload_id'];
                $gastopago_id = $tipo_gasto == 'medcau' ? $demanda_reg['tb_demanda_medcau_gastopago_id'] : $demanda_reg['tb_demanda_dernot_gastopago_id'];
                $nro_ticket = $tipo_gasto == 'medcau' ? $demanda_reg['tb_demanda_medcau_gastopago_nroticket'] : $demanda_reg['tb_demanda_dernot_gastopago_nroticket'];
            }

            require_once '../upload/Upload.class.php';
            $oUpload = new Upload();

            $url_gasto = $oUpload->mostrarUno($upload_id)['data']['upload_url'];

            $where[0]['column_name'] = 'gp.tb_gastopago_id';
            $where[0]['param0'] = $gastopago_id;
            $where[0]['datatype'] = 'INT';
            $gastopago_reg = $oGastopago->listar_todos($where, array(), array())['data'][0];

            $gasto_id = $gastopago_reg['tb_gasto_idfk'];
            $xac_gastopago = $gastopago_reg['tb_gastopago_xac'];
            $fecha_pago = $gastopago_reg['tb_gastopago_fecha'];
            $monto_pago = mostrar_moneda($gastopago_reg['tb_gastopago_monto']);
            $forma_pago[1] = $gastopago_reg['tb_gastopago_formapago'] == 1 ? 'selected' : '';
            $forma_pago[2] = $gastopago_reg['tb_gastopago_formapago'] == 2 ? 'selected' : '';
            $numope = $gastopago_reg['tb_gastopago_numope'];
        }
    }
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_demanda_gasto_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if (in_array(0, [$listo, $caja_aperturada, $tccompra_hoy])) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button id="btn_cerrar_x" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_demandagasto" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action_demanda" id="action_demanda" value="<?php echo $action;?>_demandagasto">
                        <input type="hidden" name="hdd_demanda_gasto_tipo" id="hdd_demanda_gasto_tipo" value="<?php echo $tipo_gasto;?>">
                        <input type="hidden" name="hdd_demanda_id" id="hdd_demanda_id" value="<?php echo $demanda_id;?>">
                        <input type="hidden" name="hdd_demandagasto_ejecucionfase_id" id="hdd_demandagasto_ejecucionfase_id" value="<?php echo $ejecucionfase_id;?>">
                        <input type="hidden" name="hdd_demandagasto_ejecucion_id" id="hdd_demandagasto_ejecucion_id" value="<?php echo $ejecucion_id;?>">
                        <input type="hidden" name="hdd_demandagasto_credito_id" id="hdd_demandagasto_credito_id" value="<?php echo $credito_id;?>">
                        <input type="hidden" name="hdd_demandagasto_cliente_nom" id="hdd_demandagasto_cliente_nom" value="<?php echo $_POST['cliente_nom'];?>">
                        <div class="row">
                            <div class="col-md-6 div_pdf_viewer" style="padding-top: 0px; padding-bottom: 0px; padding-right: 0px;">
                                <input type="hidden" name="hdd_demanda_upload_id" id="hdd_demanda_upload_id" value="<?php echo $upload_id; ?>">
                                <div id="subir_file" style="display: none;">
                                    <button type="button" class="btn btn-primary" onclick="ejecucionpdf_form(<?php echo $ejecucion_id.', '.$ejecucionfase_id.', '.$credito_id.', \'new\', \'ejecucion_varios-'.$tipo_gasto.'\''; ?>)"><i class="fa fa-plus"></i> Subir PDF</button>
                                    <div class="callout callout-warning">
                                        <h4>Advertencia!</h4>
                                        <p>No existe archivo PDF de Gasto <?php echo $tipo_gasto_nom;?> Debe Subir uno.</p>
                                    </div>
                                </div>
                                <div id="file_subido">
                                    <?php if (!empty($url_gasto)) { ?>
                                        <object id="obj_pdf_viewer" data='<?php echo $dominio_server . $url_gasto; ?>' type='application/pdf' width='670px' height='664px'>Si no se muestra el archivo, puedes visualizarlo desde <a href='<?php echo "../../" . $url_gasto; ?>' target='_blank'><b>AQUI</b></a></object>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box box-primary shadow <?php echo $extendido;?>" style="padding: 2px;">
                                    <div class="box-header with-border" style="border-bottom-color: #e5e5e5; padding-top: 9px; padding-bottom: 6px;">
                                        <h1 class="box-title" style="font-size: 14px; font-weight: bold;">Datos del gasto vehicular</h1>
                                        <div class="box-tools pull-right">
                                            <button id="extend_datos_gasto" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa <?php echo $icono_extendido;?>"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="padding-bottom: 1px;">
                                        <input type="hidden" name="hdd_demanda_gasto_id" id="hdd_demanda_gasto_id" value="<?php echo $gasto_id;?>">
                                        <input type="hidden" name="hdd_demanda_gastopago_id" id="hdd_demanda_gastopago_id" value="<?php echo $gastopago_id;?>">
                                        <input type="hidden" name="hdd_demanda_gastopago_xac" id="hdd_demanda_gastopago_xac" value="<?php echo $xac_gastopago;?>">
                                        <input type="hidden" name="hdd_demanda_tc" id="hdd_demanda_tc" value="<?php echo $tccompra_hoy;?>">
                                        <div class="row form-group">
                                            <div class="col-md-3" style="width: 23.2%;">
                                                <label for="txt_demanda_fechapago">Fecha pago:</label>
                                                <div class='input-group date'>
                                                    <input type="text" name="txt_demanda_fechapago" id="txt_demanda_fechapago" class="form-control input-sm input-shadow mayus disabled" placeholder="dd-mm-aaaa" value="<?php echo mostrar_fecha($fecha_pago); ?>">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="two-fields">
                                                    <label for="txt_demanda_gastomonto">Monto pagado:</label>
                                                    <div class="input-group">
                                                        <select name="cbo_demanda_gastomoneda_id" id="cbo_demanda_gastomoneda_id" class="form-control input-shadow input-sm mayus disabled" style="width: 38%; padding: 5px 8px;">
                                                            <?php $moneda_id = 1;
                                                            require_once '../moneda/moneda_select.php'; ?>
                                                        </select>
                                                        <input type="text" name="txt_demanda_gastomonto" id="txt_demanda_gastomonto" class="form-control input-shadow input-sm moneda disabled" style="width: 62%; padding: 5px 5px;" placeholder="0.00" value="<?php echo $monto_pago;?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="width: 28%;">
                                                <label for="cbo_demanda_forma_egreso">Forma de egreso:</label>
                                                <select name="cbo_demanda_forma_egreso" id="cbo_demanda_forma_egreso" class="form-control input-shadow input-sm mayus disabled" style="padding-left: 5.3px;">
                                                    <option value="1" <?php echo $forma_pago[1];?>>Caja Efectivo</option>
                                                    <option value="2" <?php echo $forma_pago[2];?>>Por Banco</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3" style="width: 23.8%;">
                                                <div class="num_ope" style="display: none;">
                                                    <label for="txt_demanda_numope">N° Operación:</label>
                                                    <input type="text" name="txt_demanda_numope" id="txt_demanda_numope" class="form-control input-shadow input-sm mayus disabled" placeholder="N° operación" value="<?php echo $numope;?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-3">
                                                <label for="txt_demanda_gastopago_nroticket">Nro. Ticket:</label>
                                                <input type="text" name="txt_demanda_gastopago_nroticket" id="txt_demanda_gastopago_nroticket" class="form-control input-sm input-shadow disabled" value="<?php echo $nro_ticket;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="callout callout-info" id="gastopago_mensaje" style="display: none;"></div>
                                <div class="callout callout-info" id="demandagasto_mensaje" style="display: none;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info<?php echo $usuario_action == 'I' ? ' disabled' : ''; ?>" id="btn_guardar_demandagasto">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_demandagasto">Aceptar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'I') { ?>
                                <button type="button" class="btn btn-danger" onclick="demandagasto_cancelar()" id="btn_cancelar_demandagasto">Cancelar</button>
                            <?php } else { ?>
                                <button type="button" id="btn_cerrar_dem_gas_form" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                            <?php } ?>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/demanda/demanda_gasto_form.js?ver=0233'; ?>"></script>