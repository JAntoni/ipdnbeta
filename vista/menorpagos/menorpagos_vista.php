<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Contabilidad</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit));?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
      width: 90%;
			font-weight: bold;
		}
		div.dataTables_filter label{
			width: 80%;
		}
	</style>
	<!-- Main content -->
	<section class="content">
		<?php
			$fecha = new DateTime();
			$fecha->modify('last day of this month');

			$cuota_fec1 = date('01-m-Y');
			$cuota_fec2 = $fecha->format('d-m-Y');
		?>
		<div class="box">
			<div class="box-header">
				<form id="form_creditomenor_filtro" class="form-inline" role="form">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker1'>
							<input type='text' class="form-control input-sm" name="txt_filtro_fec1" id="txt_filtro_fec1" value="<?php echo $cuota_fec1;?>"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<div class='input-group date' id='datetimepicker2'>
							<input type='text' class="form-control input-sm" name="txt_filtro_fec2" id="txt_filtro_fec2" value="<?php echo $cuota_fec2;?>"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<button type="button" class="btn btn-info btn-sm" onclick="pagos_intereses_generados()"><i class="fa fa-search"></i></button>
				</form>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
        <div class="callout callout-info" id="menorpagos_mensaje_tbl" style="display: none;">
          <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando Tabla de Pagos...</span></h4>
        </div>
				
				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">REPORTE DE PAGO DE INTERESES DE CRÉDITO MENOR</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<!-- TABLA PARA REPORTAR A menorpagos -->
						<div class="table-responsive">
							<table id="tbl_menorpagos" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Crédito ID</th>
										<th>Cliente</th>
										<th class="no-sort">Interés Cuota</th>
										<th class="no-sort">Capital Cuota</th>
										<th>Fecha de Cuota</th>
										<th>Fecha de Pago</th>
										<th class="no-sort">Importe Pagado</th>
										<th class="no-sort">Interés Válido</th>
										<th class="no-sort">Monto Amortizado</th>
										<th class="no-sort">Base</th>
										<th class="no-sort">IGV</th>
										<th>Lugar de Pago</th>
									</tr>
								</thead>
								<tbody id="lista_menorpagos">
									
								</tbody>
							</table>
						</div>
						<!-- FIN REPORTE A menorpagos -->
					</div>
				</div>		
			</div>

			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
