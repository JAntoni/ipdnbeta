<?php
  require_once('../creditomenor/Creditomenor.class.php');

  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oCredito = new Creditomenor();

  $fecha_ini = fecha_mysql($_POST['fecha_ini']);
  $fecha_fin = fecha_mysql($_POST['fecha_fin']);
  

  $result = $oCredito->pagos_intereses_generados($fecha_ini, $fecha_fin);
    if($result['estado'] == 1){
      $return['estado'] = 1;
      foreach ($result['data'] as $key => $value) {
        $interes_valido = $value['tb_cuota_int'];
        if($value['tb_cuota_int'] > $value['tb_ingreso_imp'])
          $interes_valido = $value['tb_ingreso_imp'];

        $monto_amortizado = $value['tb_ingreso_imp'] - $interes_valido;
        $monto_base = formato_numero($interes_valido / 1.18);
        $igv = $interes_valido - $monto_base;

        $data .= '
          <tr>
            <td>'.$value['tb_credito_id'].'</td>
            <td>'.$value['tb_cliente_nom'].'</td>
            <td>'.$value['tb_cuota_int'].'</td>
            <td>'.$value['tb_cuota_cap'].'</td>
            <td>'.mostrar_fecha($value['tb_cuota_fec']).'</td>
            <td>'.mostrar_fecha($value['tb_ingreso_fec']).'</td>
            <td class="success">'.$value['tb_ingreso_imp'].'</td>
            <td class="info">'.$interes_valido.'</td>
            <td>'.$monto_amortizado.'</td>
            <td>'.$monto_base.'</td>
            <td>'.$igv.'</td>
            <td>'.$value['lugar'].'</td>
          </tr>
        ';
      }
    }
    else{
      $return['estado'] = 0;
      $data = '
        <tr>
          <td colspan="12">'.$result['mensaje'].'</td>
        </tr>
      ';
    }
  $result = NULL;

  $return['tabla'] = $data;
  
  echo json_encode($return);
?>