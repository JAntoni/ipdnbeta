var datatable_global_certificadoregveh;

$(document).ready(function () {
    completar_tabla_certificadoregvehs();
});

function completar_tabla_certificadoregvehs() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "certificadoregistralveh/certificadoregveh_tabla_form_vista.php",
        async: true,
        dataType: "html",
        data: ({
            ejecucion_id: $('#hdd_crv_ejecucion_id').val(),
            ejecucionfase_completado: $('#hdd_crv_ejecucionfase_completado').val()
        }),
        success: function(data) {
            $('#div_html_tablacertificadoregveh').html(data);
            setTimeout(function () {
                estilos_datatable_crv();
            }, 300);
        }
    });
}

function estilos_datatable_crv() {
    datatable_global_certificadoregveh = $('#tbl_certificadoregvehs').DataTable({
        "pageLength": 25,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [3], orderable: false}
        ]
    });
    datatable_texto_filtrar_crv();
}

function datatable_texto_filtrar_crv() {
    $('input[aria-controls*="tbl_certificadoregvehs"]')
    .attr('id', 'txt_datatable_certificadoregveh_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_certificadoregveh_fil').keyup(function (event) {
        $('#hdd_datatable_certificadoregvehs_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_certificadoregvehs_fil').val();
    if (text_fil) {
        $('#txt_datatable_certificadoregveh_fil').val(text_fil);
        datatable_global_certificadoregveh.search(text_fil).draw();
    }
};

function certificadoregveh_form(action, certificado_id, tiene_pdf_activo) {
    var height = tiene_pdf_activo == 1 ? $(window).height() - ($(window).height()*0.21) : 0;
    var width = $(window).width() - ($(window).width()*0.61);
    $.ajax({
        type: 'POST',
        url: VISTA_URL + 'certificadoregistralveh/certificadoregveh_form.php',
        async: true,
        dataType: 'html',
        data: ({
            action: action,
            certificado_id: certificado_id,
            credito_id: $('#hdd_crv_credito_id').val(),
            ejecucionfase_id: $('#hdd_crv_ejecucionfase_id').val(),
            ejecucion_reg: $('#hdd_ejecucion_reg').val(),
            height: height,
            width: width
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_modal_certificadoregveh_form').html(data);
            $('#modal_certificadoregveh_form').modal('show');
            $('#modal_mensaje').modal('hide');

            modal_width_auto('modal_certificadoregveh_form', 80);
            modal_hidden_bs_modal("modal_certificadoregveh_form", "limpiar");
            //funcion js para agregar un largo automatico al modal, al abrirlo
            if (tiene_pdf_activo)
                $('#modal_certificadoregveh_form').find(".modal-body").css("height", height);
        }
    });
}