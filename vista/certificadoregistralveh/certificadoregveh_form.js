
$(document).ready(function () {
	$('#txt_cert_regveh_fec_generado, #txt_certificadoregveh_fechapago').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		endDate: new Date()
	});
	$('#cbo_cert_regveh_zonaregistral option:eq(0)').text('SELECCIONE');

	$("#cbo_cert_regveh_marca").change(function () {
		cargar_modelo($(this).val());
	});

	$('#cbo_certificadoregveh_forma_egreso').change(function(){
		if ($(this).val() == 2) {
			$('.num_ope').show(150);
		} else {
			$('.num_ope').hide(150);
		}
	});

	//triggers
	disabled($('#form_certificadoregveh').find('.disabled'));
	$('#cbo_certificadoregveh_forma_egreso').change();
	$(".moneda").focus(function(){
        formato_moneda(this);
    });

	$('#form_certificadoregveh').validate({
		submitHandler: function () {
			var ejecucionfase_id = $('#hdd_certificadoregveh_ejecucionfase_id').val();
			var zona_selected = '&zona_selected=' + $('#cbo_cert_regveh_zonaregistral option:selected').text();
			var clase_selected = '&clase_selected=' + $('#cbo_cert_regveh_carroceria option:selected').text();
			var marca_selected = '&marca_selected=' + $('#cbo_cert_regveh_marca option:selected').text();
			var modelo_selected = '&modelo_selected=' + $('#cbo_cert_regveh_modelo option:selected').text();

			var datos_extra = zona_selected + clase_selected + marca_selected + modelo_selected;
			$.ajax({
				type: "POST",
				url: VISTA_URL + "certificadoregistralveh/certificadoregveh_controller.php",
				async: true,
				dataType: "json",
				data: serialize_form('form_certificadoregveh', datos_extra),
				beforeSend: function () {
					$('#certificadoregveh_mensaje').show(400);
					$('#btn_guardar_certificadoregveh').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						disabled($('#form_certificadoregveh').find("button[class*='btn-sm'], select, input, textarea"));
						$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#certificadoregveh_mensaje').html(`<h4>${data.mensaje}</h4>`);

						setTimeout(function () {
							$('#modal_certificadoregveh_form').modal('hide');
							if (data.estado == 1) {
								actualizar_contenido_1fase(ejecucionfase_id);
								completar_tabla_certificadoregvehs();
							}
						}, 2500);
					}
					else {
						$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#certificadoregveh_mensaje').html(`<h4>Alerta: ${data.mensaje}</h4>`);
						$('#btn_guardar_certificadoregveh').prop('disabled', false);
					}
				},
				error: function (data) {
					$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-danger');
					$('#certificadoregveh_mensaje').html(`<h4>ALERTA DE ERROR: ${data.responseText}</h4>`);
				}
			});
		},
		rules: {
			cbo_cert_regveh_zonaregistral: {
				min: 1
			},
			txt_cert_regveh_fec_generado: {
				required: true
			},
			txt_cert_regveh_placa: {
				required: true
			},
			txt_cert_regveh_categ: {
				required: true
			},
			cbo_cert_regveh_carroceria: {
				min: 1
			},
			cbo_cert_regveh_marca: {
				min: 1
			},
			cbo_cert_regveh_modelo: {
				min: 1
			},
			txt_cert_regveh_anio: {
				required: true
			},
			txt_cert_regveh_seriechasis: {
				required: true
			},
			txt_cert_regveh_color: {
				required: true
			},
			txt_cert_regveh_nromotor: {
				required: true
			},
			txt_cert_regveh_combustible: {
				required: true
			},
			txt_cert_regveh_nropartida: {
				required: true
			},
			txt_certificadoregveh_fechapago: {
				required: true
			},
			txt_certificadoregveh_gastomonto: {
				required: true
			},
			cbo_certificadoregveh_forma_egreso: {
				min: 1
			},
			txt_certificadoregveh_numope: {
				required: true
			}
		},
		messages: {
			cbo_cert_regveh_zonaregistral: {
				min: "Obligatorio*"
			},
			txt_cert_regveh_fec_generado: {
				required: "Obligatorio*"
			},
			txt_cert_regveh_placa: {
				required: "Obligatorio*"
			},
			txt_cert_regveh_categ: {
				required: "Obligatorio*"
			},
			cbo_cert_regveh_carroceria: {
				min: "Obligatorio*"
			},
			cbo_cert_regveh_marca: {
				min: "Obligatorio*"
			},
			cbo_cert_regveh_modelo: {
				min: "Obligatorio*"
			},
			txt_cert_regveh_anio: {
				required: "Obligatorio*"
			},
			txt_cert_regveh_seriechasis: {
				required: "Obligatorio*"
			},
			txt_cert_regveh_color: {
				required: "Obligatorio*"
			},
			txt_cert_regveh_nromotor: {
				required: "Obligatorio*"
			},
			txt_cert_regveh_combustible: {
				required: "Obligatorio*"
			},
			txt_cert_regveh_nropartida: {
				required: "Obligatorio*"
			},
			txt_certificadoregveh_fechapago: {
				required: "Obligatorio*"
			},
			txt_certificadoregveh_gastomonto: {
				required: "Obligatorio*"
			},
			cbo_certificadoregveh_forma_egreso: {
				min: "Obligatorio*"
			},
			txt_certificadoregveh_numope: {
				required: "Obligatorio*"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	//mensaje antes de eliminar
    if ($.inArray($('#action_certificadoregveh').val(), ['leer', 'eliminar', 'dar_baja', 'activa']) > -1) {
		disabled($('#form_certificadoregveh').find("button[class*='btn-sm'], select, input, textarea, a"));
        if ($('#action_certificadoregveh').val() == 'eliminar') {
			verificar_gastopago_eliminado();
        } else if ($('#action_certificadoregveh').val() == 'dar_baja') {
			$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-warning');
			var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea dar de baja este registro?</h4>';
			$('#certificadoregveh_mensaje').html(mens).show();
		} else if ($('#action_certificadoregveh').val() == 'activa') {
			$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-warning');
			var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea activar este registro?</h4>';
			$('#certificadoregveh_mensaje').html(mens).show();
		}
    } else if ($('#action_certificadoregveh').val() == 'insertar') {
		$('div#subir_file').show(150);
	}
});

function cargar_modelo(marca_id) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "vehiculomodelo/vehiculomodelo_select.php",
		async: true,
		dataType: "html",
		data: {
			vehiculomarca_id: marca_id,
		},
		beforeSend: function () {
			$("#cbo_cert_regveh_modelo").html('<option value="">Cargando...</option>');
		},
		success: function (html) {
			$("#cbo_cert_regveh_modelo").html(html);
		}
	});
}

function actualizar_vista_upload(upload_id, url) {
	var dominio_server = 'https://ipdnsac.ipdnsac.com/';
	//var dominio_server = 'http://192.168.101.90/ipdnsac/';
	var certificado_regveh_id = $('#hdd_certificadoregveh_id').val();

	$('#hdd_certificadoregveh_upload_id').val(upload_id);
	$('#modal_certificadoregveh_form').find(".modal-body").css('height', '684px');
	$('div#subir_file').hide(200);
	$('div#file_subido').html(`<object id="obj_pdf_viewer" data='${dominio_server}${url}' type='application/pdf' width='674px' height='664px'>Si no se muestra el archivo, puedes visualizarlo desde <a href='../../${url}' target='_blank'><b>AQUI</b></a></object>`).show(200);
	$('#btn_guardar_certificadoregveh').removeClass('disabled').removeAttr('disabled');
	if (certificado_regveh_id == '') {
		$('#txt_cert_regveh_fec_generado, #txt_cert_regveh_color, #txt_cert_regveh_combustible, #txt_cert_regveh_nropartida').removeClass('disabled').removeAttr('disabled');
		$('#txt_certificadoregveh_gastomonto, #cbo_certificadoregveh_forma_egreso, #txt_certificadoregveh_numope').removeClass('disabled').removeAttr('disabled');
		$('#extend_datos_certificado, #extend_datos_gasto').click();
	}
}

function certificadoregveh_cancelar() {
	var upload_id = $('#hdd_certificadoregveh_upload_id').val();

	if (upload_id > 0) {
        if (confirm('Al cancelar el registro, el pdf será eliminado. ¿Desea seguir de todos modos?')) {
			eliminar_pdf_certificado(upload_id, 'C');
        }
    } else {
        $('#modal_certificadoregveh_form').modal('hide');
    }
}

function verificar_gastopago_eliminado() {
	xac = $('#hdd_certificadoregveh_gastopago_xac').val();
	if (parseInt(xac) == 1) {
		$('#gastopago_mensaje').removeClass('callout-info').addClass('callout-danger');
		var mens = '<h4><i class="icon fa fa-warning"></i> SOLICITE LA ELIMINACION DEL PAGO A ÁREA DE CONTABILIDAD. ';
		mens += `CLIENTE ${$('#hdd_certificadoregveh_cliente_nom').val()}, CREDITO ID ${$('#hdd_certificadoregveh_credito_id').val()}`;
		mens += `, GASTO ID: ${$('#hdd_certificadoregveh_gasto_id').val()}`;
		mens += `, GASTO PAGO ID: ${$('#hdd_certificadoregveh_gastopago_id').val()}</h4>`;
		$('#gastopago_mensaje').html(mens);
		$('#gastopago_mensaje').show();
		disabled($('#btn_guardar_certificadoregveh'));
	} else {
		$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-warning');
		var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este registro?</h4>';
		$('#certificadoregveh_mensaje').html(mens).show();
	}
}

function confirmar_eliminar(){
	$.confirm({
		title: 'Sistema',
		content: `<b>¿Confirma que desea eliminar este archivo?</b>`,
		type: 'red',
		escapeKey: 'close',
		backgroundDismiss: true,
		columnClass: 'small',
		buttons: {
			Confirmar: {
				btnClass: 'btn-red',
				action: function() {
					eliminar_pdf_certificado($('#hdd_certificadoregveh_upload_id').val(), 'M');
				}
			},
			cancelar: function () {
			}
		}
	});
};

function eliminar_pdf_certificado(upload_id, action) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "ejecucionupload/ejecucionupload_controller.php",
		async: true,
		dataType: "json",
		data: {
			action: 'eliminar_pdf',
			upload_id: upload_id,
			modulo_nom: 'ejecucion_certificado_regveh'
		},
		beforeSend: function () {
			$('#btn_cancelar_certificadoregveh, #btn_guardar_certificadoregveh, #btn_cerrar_crv_form, #btn_cerrar_x').prop('disabled', true);
			$('#certificadoregveh_mensaje').addClass('callout-info');
			$('#certificadoregveh_mensaje').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(300);
		},
		success: function (data) {
			after_eliminar_pdf(data, action);
		},
		error: function (data) {
			$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-warning');
			$('#certificadoregveh_mensaje').html('ERROR AL CANCELAR: ' + data.responseText);
		}
	});
}

function after_eliminar_pdf(data, action) {
	if (action == 'M') {
		if (parseInt(data.estado) > 0) {
			$('#certificadoregveh_mensaje').hide(300);
			$('div#file_subido').hide(300).html('');
			$('div#subir_file').show(300);
			$('#hdd_certificadoregveh_upload_id').val('');
		} else {
			$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-warning');
			$('#certificadoregveh_mensaje').html(`<h4><i class="icon fa fa-bug"></i> ${data.mensaje}</h4>`);
			$('#btn_cerrar_crv_form, #btn_cerrar_x').removeAttr('disabled');
		}
	}
	else if (action == 'C') {
		if (parseInt(data.estado) > 0) {
			$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-success');
			$('#certificadoregveh_mensaje').html(`<h4><i class="icon fa fa-check"></i> ${data.mensaje}</h4>`);
			setTimeout(function() {
				$('#modal_certificadoregveh_form').modal('hide');
			}, 2000);
		} else {
			$('#certificadoregveh_mensaje').removeClass('callout-info').addClass('callout-warning');
			$('#certificadoregveh_mensaje').html(`<h4><i class="icon fa fa-bug"></i> ${data.mensaje}</h4>`);
			$('#btn_cancelar_certificadoregveh').removeAttr('disabled');
		}
	}
}