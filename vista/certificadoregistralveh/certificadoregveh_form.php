<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/funciones.php';
require_once '../funciones/fechas.php';
require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
$oCertificado = new Certificadoregistralveh();
require_once '../creditogarveh/Creditogarveh.class.php';
$oCreditoGarveh = new Creditogarveh();
require_once '../upload/Upload.class.php';
$oUpload = new Upload();
require_once '../monedacambio/Monedacambio.class.php';
$oMonedacambio = new Monedacambio();
require_once '../gastopago/Gastopago.class.php';
$oGastopago = new Gastopago();

$usuario_action = $_POST['action'];
$titulo = '';
if ($usuario_action == 'L') {
    $titulo = 'Certificado Registral Vehicular Registrado';
} elseif ($usuario_action == 'I') {
    $titulo = 'Registrar Certificado Registral Vehicular';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar Certificado Registral Vehicular';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar Certificado Registral Vehicular';
} elseif ($usuario_action == 'B') {
    $titulo = 'Dar de baja Certificado Registral Vehicular';
} elseif ($usuario_action == 'A') {
    $titulo = 'Activar Certificado Registral Vehicular';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
if ($usuario_action == 'A') {
    $action = 'activa';
} else {
    $action = $usuario_action == 'B' ? 'dar_baja' : devuelve_nombre_usuario_action($usuario_action);
}

$listo = 1;
$caja_aperturada = validar_apertura_caja_2();

$fecha_pago = date('Y-m-d');
$result = $oMonedacambio->consultar($fecha_pago);
$tccompra_hoy = 0;
if ($result['estado'] == 1) {
    $tccompra_hoy = $result['data']['tb_monedacambio_com'];
}
unset($result);

if ($listo != 1) {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
} elseif ($caja_aperturada != 1) {
    $mensaje = 'No has aperturado o ya está cerrada la caja de este día: ' . date('d-m-Y') . ' ' . ' para la sede de ' . substr($_SESSION['empresa_nombre'], 7);
} elseif (empty($tccompra_hoy)) {
    $mensaje = "No hay tipo de cambio registrado en el día ". mostrar_fecha($fecha_pago);
} else {
    $certificado_id = intval($_POST['certificado_id']);
    $credito_id = intval($_POST['credito_id']);
    $ejecucionfase_id = $_POST['ejecucionfase_id'];
    $datos_ejecucion = (array) json_decode($_POST['ejecucion_reg']);
    $dominio_server = 'https://ipdnsac.ipdnsac.com/';
    //$dominio_server = 'http://192.168.101.90/ipdnsac/';
    if (!empty($_POST['height'])) {
        $altura = intval($_POST['height']) - 20;
        $altura = $usuario_action == 'M' ? $altura - 30 : $altura;
        $ancho = intval($_POST['width']);
    }

    $cliente_concat_nom = $datos_ejecucion['tb_cliente_nom'];
    if (intval($datos_ejecucion['tb_cliente_tip']) == 2) {
        $cliente_concat_nom .= ", {$datos_ejecucion['tb_cliente_empruc']} - {$datos_ejecucion['tb_cliente_emprs']}";
    }
    $cliente_id = $datos_ejecucion['tb_cliente_id'];

    $extendido = 'collapsed-box';
    $icono_extendido = 'fa-plus';
    if ($certificado_id > 0) {
        $busq_certificado = $oCertificado->listar_todos($certificado_id, '', '', '');
        if ($busq_certificado['estado'] == 1) {
            $extendido = '';
            $icono_extendido = 'fa-minus';
            $certificadoregveh = $busq_certificado['data'][0];

            $certificadoregveh_id = $certificadoregveh['tb_certificadoregveh_id'];
            $gastopago_id = $certificadoregveh['tb_gastopago_id'];
            $fec_generado = mostrar_fecha($certificadoregveh['tb_certificadoregveh_fecgenerado']);
            $zona_id = $certificadoregveh['tb_zona_id'];
            $vehiculoclase_id = $certificadoregveh['tb_vehiculoclase_id'];
            $vehiculomarca_id = $certificadoregveh['tb_vehiculomarca_id'];
            $vehiculomodelo_id = $certificadoregveh['tb_vehiculomodelo_id'];
            $placa = $certificadoregveh['tb_certificadoregveh_placa'];
            $categoria = $certificadoregveh['tb_certificadoregveh_cat'];
            $anio = $certificadoregveh['tb_certificadoregveh_ano'];
            $serie_chasis = $certificadoregveh['tb_certificadoregveh_seriechasis'];
            $color = $certificadoregveh['tb_certificadoregveh_color'];
            $nro_motor = $certificadoregveh['tb_certificadoregveh_nromotor'];
            $combustible = $certificadoregveh['tb_certificadoregveh_comb'];
            $nro_partida = $certificadoregveh['tb_certificadoregveh_nropartida'];
        } else {
            $titulo .= " No existe certificado activo con el id: $certificado_id";
        }
        unset($busq_certificado);

        $busq_pdf_certregveh = $oUpload->mostrarUno_modulo($certificado_id, 'ejecucion_certificado_regveh')['data'];
        if ($busq_pdf_certregveh['upload_xac'] == 1) {
            $upload_id = $busq_pdf_certregveh['upload_id'];
            $urlCertif = $busq_pdf_certregveh['upload_url'];
        }
        unset($busq_pdf_certregveh);
        
        $where[0]['column_name'] = 'gp.tb_gastopago_id';
        $where[0]['param0'] = $gastopago_id;
        $where[0]['datatype'] = 'INT';

        $inner[0]['alias_columnasparaver'] = 'mc.tb_monedacambio_com';
        $inner[0]['tipo_union'] = 'LEFT';
        $inner[0]['tabla_alias'] = 'tb_monedacambio mc';
        $inner[0]['columna_enlace'] = 'gp.tb_gastopago_fecha';
        $inner[0]['alias_columnaPK'] = 'mc.tb_monedacambio_fec';

        $busq_gastopago = $oGastopago->listar_todos($where, $inner, array());
        if ($busq_gastopago['estado'] == 1) {
            $tccompra_hoy = $busq_gastopago['data'][0]['tb_monedacambio_com'];
            $fecha_pago = mostrar_fecha($busq_gastopago['data'][0]['tb_gastopago_fecha']);
            $monto_pago = mostrar_moneda($busq_gastopago['data'][0]['tb_gastopago_monto']);
            $xac_gastopago = $busq_gastopago['data'][0]['tb_gastopago_xac'];
            $gasto_id = $busq_gastopago['data'][0]['tb_gasto_idfk'];
            
            $formapago = intval($busq_gastopago['data'][0]['tb_gastopago_formapago']);
            $forma_pago[1] = '';
            $forma_pago[2] = $formapago == 2 ? 'selected' : '';
            $forma_pago[3] = '';
            if ($formapago == 1) {
                $gastopago_obs = $busq_gastopago['data'][0]['tb_gastopago_obs'];
                if (strpos($gastopago_obs, 'PLATAFORMA SUNARP') > 0) {
                    $forma_pago[3] = 'selected';
                } else {
                    $forma_pago[1] = 'selected';
                }
            }

            if ($formapago == 2) {
                $numope = $busq_gastopago['data'][0]['tb_gastopago_numope'];
            }
        }
        unset($busq_gastopago);
    } else {
        $busq_credito = $oCreditoGarveh->mostrarUno($credito_id);
        if ($busq_credito['estado'] == 1) {
            $data_default = $busq_credito['data'];
            $zona_id = $data_default['tb_zonaregistral_id'];
            $vehiculoclase_id = $data_default['tb_vehiculoclase_id'];
            $vehiculomarca_id = $data_default['tb_vehiculomarca_id'];
            $vehiculomodelo_id = $data_default['tb_vehiculomodelo_id'];
            $placa = trim($data_default['tb_credito_vehpla']);
            $categoria = $data_default['tb_credito_vehcate'];
            $anio = $data_default['tb_credito_vehano'];
            $serie_chasis = $data_default['tb_credito_vehsercha'];
            $color = strtoupper(trim($data_default['tb_credito_vehcol']));
            $nro_motor = $data_default['tb_credito_vehsermot'];
            $combustible = strtoupper(trim($data_default['tb_credito_vehcomb']));

            $faltan_dts_auto = '';
            //verificar si le falta algun campo en el credito garveh
                if (empty($zona_id)){
                    $faltan_dts_auto .= "- zona registral.<br>";
                }
                if (empty($vehiculoclase_id)) {
                    $faltan_dts_auto .= "- clase de vehiculo.<br>";
                }
                if (empty($vehiculomarca_id)) {
                    $faltan_dts_auto .= "- marca vehiculo.<br>";
                }
                if (empty($vehiculomodelo_id)) {
                    $faltan_dts_auto .= "- modelo de vehiculo.<br>";
                }
                if (empty($placa)) {
                    $faltan_dts_auto .= "- placa de vehiculo.<br>";
                }
                if (empty($categoria)) {
                    $faltan_dts_auto .= "- categoria de vehiculo.<br>";
                }
                if (empty($anio)) {
                    $faltan_dts_auto .= "- año del vehiculo.<br>";
                }
                if (empty($serie_chasis)) {
                    $faltan_dts_auto .= "- serie de chasis.<br>";
                }
                if (empty($color)) {
                    $faltan_dts_auto .= "- color de vehiculo.<br>";
                }
                if (empty($nro_motor)) {
                    $faltan_dts_auto .= "- nro motor.<br>";
                }
                if (empty($combustible)) {
                    $faltan_dts_auto .= "- combustible.";
                }
            //

            if (!empty($faltan_dts_auto)) {
                $mensaje = "Falta algunos datos del vehiculo. Deben ser completados por el asesor de credito. Datos:<br>".$faltan_dts_auto;
            }
        }
    }
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_certificadoregveh_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if (in_array(0, [$listo, $caja_aperturada, $tccompra_hoy]) || !empty($faltan_dts_auto)) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button id="btn_cerrar_x" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_certificadoregveh" method="post">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 div_pdf_viewer" style="padding-top: 0px; padding-bottom: 0px; padding-right: 0px;">
                                <input type="hidden" name="hdd_certificadoregveh_upload_id" id="hdd_certificadoregveh_upload_id" value="<?php echo $upload_id; ?>">
                                <div id="subir_file" style="display: none;">
                                    <button type="button" class="btn btn-primary" onclick="ejecucionpdf_form(<?php echo "{$datos_ejecucion['tb_ejecucion_id']}, $ejecucionfase_id, $credito_id,"; ?> 'new', 'ejecucion_certificado_regveh')"><i class="fa fa-plus"></i> Subir PDF</button>
                                    <div class="callout callout-warning">
                                        <h4>Advertencia!</h4>
                                        <p>No existe archivo PDF de Certificado Registral. Debe Subir uno.</p>
                                    </div>
                                </div>
                                <div id="file_subido">
                                    <?php if (!empty($urlCertif)) {
                                        if ($usuario_action == 'M') { ?>
                                            <button type="button" class="btn btn-danger" onclick="confirmar_eliminar()">Eliminar pdf</button>
                                    <?php } ?>
                                        <object id="obj_pdf_viewer" data='<?php echo $dominio_server . $urlCertif; ?>' type='application/pdf' width='<?php echo $ancho; ?>px' height='<?php echo $altura; ?>px'>Si no se muestra el archivo, puedes visualizarlo desde <a href='<?php echo "../../" . $urlCertif; ?>' target='_blank'><b>AQUI</b></a></object>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box box-primary shadow <?php echo $extendido; ?>" style="padding: 2px;">
                                    <div class="box-header with-border" style="border-bottom-color: #e5e5e5; padding-top: 9px; padding-bottom: 6px;">
                                        <h1 class="box-title" style="font-size: 14px; font-weight: bold;">Datos del gasto vehicular</h1>
                                        <div class="box-tools pull-right">
                                            <button id="extend_datos_gasto" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa <?php echo $icono_extendido; ?>"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="padding-bottom: 1px;">
                                        <input type="hidden" name="hdd_certificadoregveh_gasto_id" id="hdd_certificadoregveh_gasto_id" value="<?php echo $gasto_id;?>">
                                        <input type="hidden" name="hdd_certificadoregveh_gastopago_id" id="hdd_certificadoregveh_gastopago_id" value="<?php echo $gastopago_id;?>">
                                        <input type="hidden" name="hdd_certificadoregveh_gastopago_xac" id="hdd_certificadoregveh_gastopago_xac" value="<?php echo $xac_gastopago;?>">
                                        <input type="hidden" name="hdd_certificadoregveh_tc" id="hdd_certificadoregveh_tc" value="<?php echo $tccompra_hoy;?>">
                                        <div class="row form-group">
                                            <div class="col-md-3" style="width: 23.2%;">
                                                <label for="txt_certificadoregveh_fechapago">Fecha pago:</label>
                                                <div class='input-group date' id='datetimepicker_fec_generacion_cert'>
                                                    <input type="text" name="txt_certificadoregveh_fechapago" id="txt_certificadoregveh_fechapago" class="form-control input-sm input-shadow mayus disabled" placeholder="dd-mm-aaaa" value="<?php echo $fecha_pago; ?>">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="two-fields">
                                                    <label for="txt_certificadoregveh_gastomonto">Monto pagado:</label>
                                                    <div class="input-group">
                                                        <select name="cbo_certificadoregveh_gastomoneda_id" id="cbo_certificadoregveh_gastomoneda_id" class="form-control input-shadow input-sm mayus disabled" style="width: 38%; padding: 5px 8px;">
                                                            <?php $moneda_id = 1;
                                                            require_once '../moneda/moneda_select.php'; ?>
                                                        </select>
                                                        <input type="text" name="txt_certificadoregveh_gastomonto" id="txt_certificadoregveh_gastomonto" class="form-control input-shadow input-sm moneda disabled" style="width: 62%; padding: 5px 5px;" placeholder="0.00" value="<?php echo $monto_pago;?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="width: 32%;">
                                                <label for="cbo_certificadoregveh_forma_egreso">Forma de egreso:</label>
                                                <select name="cbo_certificadoregveh_forma_egreso" id="cbo_certificadoregveh_forma_egreso" class="form-control input-shadow input-sm mayus disabled" style="padding-left: 5.3px;">
                                                    <option value="1" <?php echo $forma_pago[1];?>>Caja Efectivo</option>
                                                    <option value="2" <?php echo $forma_pago[2];?>>Por Banco</option>
                                                    <option value="3" <?php echo $forma_pago[3];?>>Saldo plataforma Sunarp</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3" style="width: 19.8%;">
                                                <div class="num_ope">
                                                    <label for="txt_certificadoregveh_numope">N° Operación:</label>
                                                    <input type="text" name="txt_certificadoregveh_numope" id="txt_certificadoregveh_numope" class="form-control input-shadow input-sm mayus disabled" placeholder="N° operación" value="<?php echo $numope;?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="callout callout-info" id="gastopago_mensaje" style="display: none;"></div>
                                
                                <br>
                                
                                <div class="box box-primary shadow <?php echo $extendido; ?>" style="padding: 2px;">
                                    <input type="hidden" name="action_certificadoregveh" id="action_certificadoregveh" value="<?php echo $action; ?>">
                                    <input type="hidden" name="hdd_certificadoregveh_id" id="hdd_certificadoregveh_id" value="<?php echo $certificadoregveh_id; ?>">
                                    <input type="hidden" name="hdd_certificadoregveh_reg" id="hdd_certificadoregveh_reg" value='<?php echo json_encode($certificadoregveh); ?>'>
                                    <input type="hidden" name="hdd_certificadoregveh_ejecucionfase_id" id="hdd_certificadoregveh_ejecucionfase_id" value="<?php echo $ejecucionfase_id; ?>">
                                    <input type="hidden" name="hdd_certificadoregveh_credito_id" id="hdd_certificadoregveh_credito_id" value="<?php echo $credito_id; ?>">
                                    <input type="hidden" name="hdd_certificadoregveh_ejecucion_id" id="hdd_certificadoregveh_ejecucion_id" value="<?php echo $datos_ejecucion['tb_ejecucion_id']; ?>">
                                    <input type="hidden" name="hdd_certificadoregveh_cliente_nom" id="hdd_certificadoregveh_cliente_nom" value="<?php echo $cliente_concat_nom; ?>">
                                    <input type="hidden" name="hdd_certificadoregveh_cliente_id" id="hdd_certificadoregveh_cliente_id" value="<?php echo $cliente_id; ?>">
                                    <input type="hidden" name="hdd_vehiculo_color_orig" id="hdd_vehiculo_color_orig" value="<?php echo $color; ?>">
                                    <input type="hidden" name="hdd_vehiculo_comb_orig" id="hdd_vehiculo_comb_orig" value="<?php echo $combustible; ?>">

                                    <div class="box-header with-border" style="border-bottom-color: #e5e5e5; padding-top: 9px; padding-bottom: 6px;">
                                        <h1 class="box-title" style="font-size: 14px; font-weight: bold;">Datos del certificado</h1>
                                        <div class="box-tools pull-right">
                                            <button id="extend_datos_certificado" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa <?php echo $icono_extendido; ?>"></i></button>
                                        </div>
                                    </div>

                                    <div class="box-body" style="padding-bottom: 1px;">
                                        <div class="row form-group">
                                            <div class="col-md-8">
                                                <label for="cbo_cert_regveh_zonaregistral">Zona Registral:</label>
                                                <select name="cbo_cert_regveh_zonaregistral" id="cbo_cert_regveh_zonaregistral" class="disabled input-sm form-control mayus input-shadow">
                                                    <?php require_once '../zona/zona_select.php'; ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label for="txt_cert_regveh_fec_generado">Fecha generación Certificado:</label>
                                                <div class='input-group date' id='datetimepicker_fec_generacion_cert'>
                                                    <input type="text" name="txt_cert_regveh_fec_generado" id="txt_cert_regveh_fec_generado" class="form-control input-sm input-shadow mayus<?php echo $usuario_action == 'I' ? ' disabled' : ''; ?>" placeholder="dd-mm-aaaa" value="<?php echo $fec_generado; ?>">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-4">
                                                <label for="txt_cert_regveh_placa">Placa:</label>
                                                <input type="text" name="txt_cert_regveh_placa" id="txt_cert_regveh_placa" class="input-sm disabled form-control mayus input-shadow" placeholder="placa" value="<?php echo $placa; ?>">
                                            </div>

                                            <div class="col-md-4">
                                                <label for="txt_cert_regveh_categ">Categoría:</label>
                                                <input type="text" name="txt_cert_regveh_categ" id="txt_cert_regveh_categ" class="disabled form-control input-sm input-shadow mayus" placeholder="Categoría" value="<?php echo $categoria; ?>">
                                            </div>

                                            <div class="col-md-4">
                                                <label for="cbo_cert_regveh_carroceria">Carrocería:</label>
                                                <select name="cbo_cert_regveh_carroceria" id="cbo_cert_regveh_carroceria" class="disabled input-sm form-control mayus input-shadow">
                                                    <?php require_once '../vehiculoclase/vehiculoclase_select.php'; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-4">
                                                <label for="cbo_cert_regveh_marca">Marca:</label>
                                                <select name="cbo_cert_regveh_marca" id="cbo_cert_regveh_marca" class="input-sm disabled input-shadow form-control mayus">
                                                    <?php require_once '../vehiculomarca/vehiculomarca_select.php'; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label for="cbo_cert_regveh_modelo">Modelo:</label>
                                                <select name="cbo_cert_regveh_modelo" id="cbo_cert_regveh_modelo" class="input-sm disabled input-shadow form-control mayus">
                                                    <?php require_once '../vehiculomodelo/vehiculomodelo_select.php'; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="txt_cert_regveh_anio">Año:</label>
                                                <input type="text" name="txt_cert_regveh_anio" id="txt_cert_regveh_anio" class="disabled form-control input-sm input-shadow mayus" placeholder="aaaa" value="<?php echo $anio; ?>">
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label for="txt_cert_regveh_seriechasis">Serie chasis:</label>
                                                <input type="text" name="txt_cert_regveh_seriechasis" id="txt_cert_regveh_seriechasis" class="disabled input-sm form-control mayus input-shadow" placeholder="serie chasis" value="<?php echo $serie_chasis; ?>">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="txt_cert_regveh_color">Color:</label>
                                                <input type="text" name="txt_cert_regveh_color" id="txt_cert_regveh_color" class="input-sm form-control mayus input-shadow<?php echo $usuario_action == 'I' ? ' disabled' : ''; ?>" placeholder="color" value="<?php echo $color; ?>">
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-4">
                                                <label for="txt_cert_regveh_nromotor">Nro. motor:</label>
                                                <input type="text" name="txt_cert_regveh_nromotor" id="txt_cert_regveh_nromotor" class="disabled input-sm form-control mayus input-shadow" placeholder="Nro. motor" value="<?php echo $nro_motor; ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <label for="txt_cert_regveh_combustible">Combustible:</label>
                                                <input type="text" name="txt_cert_regveh_combustible" id="txt_cert_regveh_combustible" class="input-sm form-control mayus input-shadow<?php echo $usuario_action == 'I' ? ' disabled' : ''; ?>" placeholder="Combustible" value="<?php echo $combustible; ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <label for="txt_cert_regveh_nropartida">Nro. Partida:</label>
                                                <input type="text" name="txt_cert_regveh_nropartida" id="txt_cert_regveh_nropartida" class="input-sm form-control mayus input-shadow<?php echo $usuario_action == 'I' ? ' disabled' : ''; ?>" placeholder="Nro. Partida" value="<?php echo $nro_partida; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="callout callout-info" id="certificadoregveh_mensaje" style="display: none;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info<?php echo $usuario_action == 'I' ? ' disabled' : ''; ?>" id="btn_guardar_certificadoregveh">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E' || $usuario_action == 'B' || $usuario_action == 'A') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_certificadoregveh">Aceptar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'I') { ?>
                                <button type="button" class="btn btn-danger" onclick="certificadoregveh_cancelar()" id="btn_cancelar_certificadoregveh">Cancelar</button>
                            <?php } else { ?>
                                <button type="button" id="btn_cerrar_crv_form" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                            <?php } ?>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/certificadoregistralveh/certificadoregveh_form.js?ver=023'; ?>"></script>