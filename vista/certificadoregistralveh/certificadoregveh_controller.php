<?php
//todos los require
	require_once '../../core/usuario_sesion.php';
	require_once '../funciones/fechas.php';
	require_once '../funciones/funciones.php';
	require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
	$oCertificado = new Certificadoregistralveh();
	require_once '../historial/Historial.class.php';
	$oHist = new Historial();
	require_once '../gasto/Gasto.class.php';
	$oGasto = new Gasto();
	require_once '../deposito/Deposito.class.php';
	$oDeposito = new Deposito();
	require_once '../egreso/Egreso.class.php';
	$oEgreso = new Egreso();
	require_once '../ingreso/Ingreso.class.php';
	$oIngreso = new Ingreso();
	require_once '../upload/Upload.class.php';
	$oUpload = new Upload();
	require_once '../monedacambio/Monedacambio.class.php';
	$oMonedaCambio = new Monedacambio();
	require_once '../gastopago/Gastopago.class.php';
	$oGastopago = new Gastopago();
	require_once '../creditogarveh/Creditogarveh.class.php';
	$oCredito = new Creditogarveh();
//

//datos estaticos
	$action				= $_POST['action_certificadoregveh'];
	$usuario_id			= intval($_SESSION['usuario_id']);
	$fecha_hora 		= date("d-m-Y h:i a");
	$moneda_nom			= array(1 => 'S/.', 2 => 'US$');
	$valor_tc_compra_hoy = floatval($_POST['hdd_certificadoregveh_tc']);
	$data['estado']		= 0;
	$data['mensaje']	= "Error al $action el Certificado registral vehicular";
//

//recibir datos y asignar
	$credito_id			= $_POST['hdd_certificadoregveh_credito_id'];
	$cliente_nom		= $_POST['hdd_certificadoregveh_cliente_nom'];
	$fecha_gasto		= $_POST['txt_certificadoregveh_fechapago'];
	$monto_gasto		= $_POST['txt_certificadoregveh_gastomonto'];
	$forma_egreso		= $_POST['cbo_certificadoregveh_forma_egreso'];
	$nro_operacion		= $_POST['txt_certificadoregveh_numope'];

	$fecha_generado		= $_POST['txt_cert_regveh_fec_generado'];
	$ejecucionfase_id	= $_POST['hdd_certificadoregveh_ejecucionfase_id'];
	$ejecucion_id		= $_POST['hdd_certificadoregveh_ejecucion_id'];
	$upload_id			= $_POST['hdd_certificadoregveh_upload_id'];
	
	$cgv_veh_color	= $_POST['hdd_vehiculo_color_orig'];
	$cgv_veh_comb	= $_POST['hdd_vehiculo_comb_orig'];

	$oCertificado->credito_id = $credito_id;
	$oCertificado->ejecucion_id = $ejecucion_id;
	$oCertificado->zona_id = $_POST['cbo_cert_regveh_zonaregistral'];
	$oCertificado->fecgenerado = fecha_mysql($fecha_generado);
	$oCertificado->placa = strtoupper($_POST['txt_cert_regveh_placa']);
	$oCertificado->cat = strtoupper($_POST['txt_cert_regveh_categ']);
	$oCertificado->vehiculoclase_id = $_POST['cbo_cert_regveh_carroceria'];
	$oCertificado->vehiculomarca_id = $_POST['cbo_cert_regveh_marca'];
	$oCertificado->vehiculomodelo_id = $_POST['cbo_cert_regveh_modelo'];
	$oCertificado->ano = strtoupper($_POST['txt_cert_regveh_anio']);
	$oCertificado->seriechasis = strtoupper($_POST['txt_cert_regveh_seriechasis']);
	$oCertificado->color = strtoupper(trim($_POST['txt_cert_regveh_color']));
	$oCertificado->nromotor = strtoupper($_POST['txt_cert_regveh_nromotor']);
	$oCertificado->comb = strtoupper(trim($_POST['txt_cert_regveh_combustible']));
	$oCertificado->nropartida = strtoupper($_POST['txt_cert_regveh_nropartida']);
	$oCertificado->upload_id = $upload_id;
//

if ($action == 'insertar') {
	//antes de cualquier accion verificar el nro operacion
		if ($forma_egreso == 2) {
			$consulta_deposito = $oDeposito->listar_depositos_paragasto($nro_operacion);
			$deposito_registrado = $consulta_deposito['estado'];
			$deposito = $consulta_deposito['data'];
			$tc_compra_deposito = $deposito['tb_deposito_fec'];

			if($deposito_registrado == 0){
				$data['mensaje'] .=  '. El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor';
			} else {
				$result = $oMonedaCambio->consultar($tc_compra_deposito);
				if ($result['estado'] == 1) {
					$tc_compra_deposito = $result['data']['tb_monedacambio_com'];

					$importe_ingresos_anteriores = 0;
					$dts = $oIngreso->lista_ingresos_num_operacion($nro_operacion);

					if ($dts['estado'] == 1) {
						foreach ($dts['data'] as $key => $dt) {
							$tb_ingreso_imp = moneda_mysql($dt['tb_ingreso_imp']);
							if (1 != intval($dt['tb_moneda_id'])) {
								$importe_ingresos_anteriores += floatval($tb_ingreso_imp * $tc_compra_deposito);
							} else {
								$importe_ingresos_anteriores += $tb_ingreso_imp;
							}
						}
					}
					unset($dts);

					$moneda_deposito = intval($deposito['tb_moneda_id']);
					$monto_deposito_orig = abs($deposito['tb_deposito_mon']);
					$monto_deposito = abs($deposito['tb_deposito_mon']);
					if (1 != $moneda_deposito) {
						$monto_deposito = floatval($monto_deposito * $tc_compra_deposito);
					}
					$monto_puede_usar = moneda_mysql($monto_deposito - $importe_ingresos_anteriores);

					if (moneda_mysql($monto_gasto) > $monto_puede_usar) {
						$deposito_registrado = 2;
						$data['mensaje'] .= '. El monto ingresado es superior al monto disponible del depósito. No puede usar más de lo depositado. ' .
						'Monto ingresado: S/.' . $monto_gasto . ', monto disponible: S/.' . mostrar_moneda($monto_puede_usar) . ', ';
						$data['mensaje'] .= 'TOTAL DEL DEPOSITO: ' . $moneda_nom[$moneda_deposito] . mostrar_moneda($monto_deposito_orig);
						$data['mensaje'] .= 1 != $moneda_deposito ? ' Ó S/.' . mostrar_moneda($monto_deposito) : '';
					}

				} else {
					$deposito_registrado = 3;
					$data['mensaje'] .= '. Verificar el tipo de cambio de la fecha '. mostrar_fecha($tc_compra_deposito);
				}
				unset($result);
			}
			unset($consulta_deposito);
		}
		if ($forma_egreso == 2 && $deposito_registrado != 1) {
			echo json_encode($data); exit();
		}
	//fin de verificacion nro operacion
	
	$oCertificado->usureg = $usuario_id;
	
	$res_insert = $oCertificado->insertar();
	if ($res_insert['estado'] == 1) {
		$data['mensaje'] = "✔ Certificado registral vehicular registrado correctamente";

		//actualizar la ruta el modulo id del upload, colocar id del tb_certificado registrados
		if ($oUpload->modificar_campo($upload_id, 'modulo_id', $res_insert['nuevo'], 'INT') != 1) {
			$data['mensaje'] .= "<br>❌ Error al actualizar upload con id del tb_certificadoregveh";
			echo json_encode($data); exit();
		}

		//generar el gastopago
			$oGasto->tb_gasto_cretip	= 3;//garveh
			$oGasto->tb_credito_id		= $credito_id;
			$oGasto->tb_gasto_tipgas	= 2;//1:producto, 2:servicio
			$oGasto->tb_proveedor_id	= 18;//IPDN
			$oGasto->tb_gasto_can		= 1;//cantidad
			$oGasto->tb_moneda_id		= 1;//certificado reg veh siempre en soles
			$oGasto->tb_gasto_pvt		= moneda_mysql($monto_gasto);
			$oGasto->tb_gasto_ptl		= moneda_mysql($monto_gasto);
			$oGasto->tb_gasto_des		= "PAGO DE CERTIFICADO REGISTRAL VEHICULAR, CASO: $cliente_nom";
			$oGasto->tb_gasto_his		= "Datos de gasto ingresados por: <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b> | <b>$fecha_hora</b><br>";
			
			$res_insert_gasto = $oGasto->insertar();
			if ($res_insert_gasto['estado'] == 1) {
				// DATOS PARA EL EGRESO
				$gasto_id = $res_insert_gasto['nuevo'];
				$vehiculo = $oCertificado->placa;
				$obs = $oGasto->tb_gasto_des;
				if ($forma_egreso == 3) {
					$obs.= '. CARGADO DE PLATAFORMA SUNARP';
				}
				$obs.= ". TOTAL S/$monto_gasto";
				$gasto_desc = $oGasto->tb_gasto_des . ". Del Cliente: $cliente_nom y vehiculo placa: $vehiculo. Comentario: [$obs]";
		
				$egr_det = "EGRESO CRÉDITO GARVEH para pagar el SERVICIO de $gasto_desc. NO EMITE DOCUMENTO";
				//generar egreso
				$oEgreso->egreso_usureg	= $usuario_id;
				$oEgreso->egreso_fec	= fecha_mysql($fecha_gasto);
				$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS, unica opcion documento
				$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
				$oEgreso->egreso_det	= $egr_det;
				$oEgreso->moneda_id		= 1;
				$oEgreso->egreso_tipcam = 1;
				$oEgreso->egreso_imp	= moneda_mysql($monto_gasto);
				$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
				$oEgreso->cuenta_id		= 10; //GASTOS UNIDADES
				$oEgreso->subcuenta_id	= 34; //MANTENIMIENTO DE UNIDADES
				$oEgreso->proveedor_id = $oGasto->tb_proveedor_id;
				$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
				$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
				$oEgreso->caja_id = 1; // 1: CAJA
				$oEgreso->modulo_id = 10; //10 egreso para pago de gastos vehiculares
				$oEgreso->egreso_modide = $gasto_id; //tb_egreso_modide el id de tb_gasto
				$oEgreso->empresa_id = $_SESSION['empresa_id'];
				$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago
		
				$result_insert_egreso = $oEgreso->insertar();
				if ($result_insert_egreso['estado'] > 0) {
					$egreso_id = $result_insert_egreso['egreso_id'];
					unset($result_insert_egreso);
					
					// en gasto actualizar historial
					$mensaje = "Se hizo un pago de S/. $monto_gasto";
					$mensaje .= ", registrado por <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b>. ";
					$mensaje .= "Egreso id : $egreso_id | <b>$fecha_hora</b><br>";
					$mensaje .= "Se registró el egreso con el detalle: $egr_det | <b>$fecha_hora</b><br>";
					$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $oGasto->tb_gasto_his.$mensaje, 'STR');
			
					//en gasto actualizar est dependiendo de si paga monto completo o paga una parte
					$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 2, 'INT');
				} else {
					$data['mensaje'] .= "❌ Error en el registro de egreso";
					echo json_encode($data); exit();
				}
				
				if ($forma_egreso == 2 || $forma_egreso == 3) {
					if ($forma_egreso == 2) {
						if(is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BCP'))){
							$banco_id = 1;
							$subcuenta_ingreso_id = 147;
						} elseif (is_numeric(strpos(strtoupper($deposito['tb_cuentadeposito_ban']),'BBVA'))){
							$banco_id = 3;
							$subcuenta_ingreso_id = 148;
						} else {
							$banco_id = 4;
							$subcuenta_ingreso_id = 183;
						}

						$ing_det = "INGRESO DE CAJA POR BANCO, desde la siguente cuenta: [ {$deposito['tb_cuentadeposito_nom']} ], por EGRESO OE $egreso_id. El N° de operación fue: [ $nro_operacion ]. ";
						$ing_det.= "Se usó [ S/.$monto_gasto ] ";
						if (1 != $moneda_deposito) {
							$ing_det.= "ó [ US$".mostrar_moneda(floatval(moneda_mysql($monto_gasto) / $valor_tc_compra_hoy))." ] ";
						}
						$ing_det.= "de un depósito total de [ {$deposito['tb_moneda_nom']}". mostrar_moneda($monto_deposito_orig) ." ], ";
						$ing_det.= "por concepto de $gasto_desc";

						$cuenta_ingreso = 43;
					} else {
						$ing_det = "INGRESO FICTICIO por EGRESO OE $egreso_id por concepto de $gasto_desc";
						$cuenta_ingreso = 2;
						$subcuenta_ingreso_id = 9;
						$nro_operacion = '';
						$banco_id = 0;
					}
					
					$oIngreso->ingreso_usureg = $usuario_id;
					$oIngreso->ingreso_fec = fecha_mysql($fecha_gasto);
					$oIngreso->documento_id = 8;//OI - OTROS INGRESOS
					$oIngreso->ingreso_numdoc = '';
					$oIngreso->ingreso_det = $ing_det;
					$oIngreso->ingreso_imp = moneda_mysql($monto_gasto);
					$oIngreso->cuenta_id = $cuenta_ingreso;//INGRESO/EGRESO POR BANCO
					$oIngreso->subcuenta_id = $subcuenta_ingreso_id;
					$oIngreso->cliente_id = 1144;// id del cliente q hace el ingreso por banco: INVERSIONES Y PRESTAMOS DEL NORTE SAC
					$oIngreso->caja_id = 1;//CAJA
					$oIngreso->moneda_id = 1;
					$oIngreso->modulo_id = 0;
					$oIngreso->ingreso_modide = 0;
					$oIngreso->empresa_id = $_SESSION['empresa_id'];
					$oIngreso->ingreso_fecdep = ($forma_egreso == 2) ? $deposito['tb_deposito_fec'] : '';
					$oIngreso->ingreso_numope = $nro_operacion;
					$oIngreso->ingreso_mondep = moneda_mysql($monto_gasto);
					$oIngreso->ingreso_comi = 0;
					$oIngreso->cuentadeposito_id = ($forma_egreso == 2) ? $deposito['tb_cuentadeposito_id'] : '';
					$oIngreso->banco_id = $banco_id;
					$oIngreso->ingreso_ap = 0;//acuerdo de pago, 0
					$oIngreso->ingreso_detex = '';
					
					$result_insert_ingreso = $oIngreso->insertar();
					if ($result_insert_ingreso['estado'] != 1) {
						$data['mensaje'] .= "<br>❌ Error en el registro de ingreso ficticio";
						echo json_encode($data); exit();
					} else {
						$ingreso_id = $result_insert_ingreso['ingreso_id'];
						unset($result_insert_ingreso);
					}
				}
		
				//ahora registrar el gastopago
				$oGastopago->tb_gastopago_monto = moneda_mysql($monto_gasto);
				$oGastopago->tb_empresa_idfk = $_SESSION['empresa_id'];
				$oGastopago->tb_gasto_idfk = $gasto_id;
				$oGastopago->tb_gastopago_formapago = $forma_egreso == 2 ? $forma_egreso : 1;
				$oGastopago->tb_gastopago_numope = $forma_egreso == 2 ? $nro_operacion : '';
				if ($forma_egreso == 2 || $forma_egreso == 3){
					$oGastopago->tb_ingreso_idfk = $ingreso_id;
				}
				$oGastopago->tb_gastopago_emitedocsunat = 0;
				$oGastopago->tb_gastopago_usureg = $usuario_id;
				$oGastopago->tb_gastopago_obs = $obs;
				$oGastopago->tb_moneda_idfk = 1;
				$oGastopago->tb_egreso_idfk = $egreso_id;
				$oGastopago->tb_gastopago_fecha = date("Y-m-d");
		
				$res_insert_gastopago = $oGastopago->insertar();
				if($res_insert_gastopago['estado'] == 1){
					$data['mensaje'] .= "<br>✔ Pago de gasto registrado correctamente";
					$gastopago_id = $res_insert_gastopago['nuevo'];
				}else {
					$data['mensaje'] .= "<br>❌ Error en el registro de PagoGasto";
				}
				unset($res_insert_gastopago);
			} else {
				$data['mensaje'] .= "<br>❌ Error al registrar el gasto vehicular";
				echo json_encode($data); exit();
			}
			unset($res_insert_gasto);
		//

		//tb_certificado actualizar el campo de gastopago
		$res_update_crv = $oCertificado->modificar_campo($res_insert['nuevo'], 'tb_gastopago_id', $gastopago_id, 'INT');
		if ($res_update_crv != 1) {
			$data['mensaje'] .= "<br>❌ Error al actualizar el campo 'tb_gastopago_id' del certificado reg veh";
		}
		unset($res_update_crv);

		$data['estado'] = 1;

		//HISTORIALES
			//guardar historial del pdf subido
			$oHist->setTbHistUsureg($usuario_id);
			$oHist->setTbHistNomTabla('upload');
			$oHist->setTbHistRegmodid($upload_id);
			$oHist->setTbHistDet("Ha subido el pdf Certificado registral vehicular del crédito $credito_id en el proceso de ejecución legal id $ejecucion_id, tb_certificadoregveh_id {$res_insert['nuevo']} | <b>$fecha_hora</b>");
			$oHist->insertar();

			//guardar historial tb_hist de la tabla tb_certificadoregveh
			$oHist->setTbHistNomTabla('tb_certificadoregveh');
			$oHist->setTbHistRegmodid($res_insert['nuevo']);
			$oHist->setTbHistDet("Registró los datos del Certificado registral vehicular del vehiculo con placa {$oCertificado->placa} en el proceso de ejecucion legal $ejecucion_id | <b>". date("d-m-Y h:i a").'</b>');
			$oHist->insertar();

			//guardar historial tb_hist de la tabla tb_ejecucionfase
			$oHist->setTbHistNomTabla('tb_ejecucionfase');
			$oHist->setTbHistRegmodid($ejecucionfase_id);
			$oHist->insertar();
		//

		//VERIFICAR SI COINCIDEN EL COLOR Y COMBUSTIBLE DEL CRV Y CREDITO GARVEH
		if ($cgv_veh_color != $oCertificado->color || $cgv_veh_comb != $oCertificado->comb) {
			$mensaje = 'Modificó datos del vehículo:';
			if ($cgv_veh_color != $oCertificado->color && $oCredito->modificar_campo($credito_id, 'tb_credito_vehcol', $oCertificado->color, 'STR') == 1) {
				$mensaje.="<br>- Ha actualizado el COLOR del vehiculo con placa {$oCertificado->placa} en el credito $credito_id, desde el proceso de ejecución legal id $ejecucion_id, tb_certificadoregveh_id {$res_insert['nuevo']}: $cgv_veh_color => <b>{$oCertificado->color}</b>";
			}
			if ($cgv_veh_comb != $oCertificado->comb && $oCredito->modificar_campo($credito_id, 'tb_credito_vehcomb', $oCertificado->comb, 'STR') == 1) {
				$mensaje.="<br>- Ha actualizado el COMBUSTIBLE del vehiculo con placa {$oCertificado->placa} en el credito $credito_id, desde el proceso de ejecución legal id $ejecucion_id, tb_certificadoregveh_id {$res_insert['nuevo']}: $cgv_veh_comb => <b>{$oCertificado->comb}</b>";
			}
			$mensaje.=" | <b>$fecha_hora</b>";

			$oHist->setTbHistUsureg($usuario_id);
			$oHist->setTbHistNomTabla('tb_creditogarveh_vehiculo');
			$oHist->setTbHistRegmodid($credito_id);
			$oHist->setTbHistDet($mensaje);
			$oHist->insertar();
		}
	}
}
elseif ($action == 'modificar') {
	$certregveh_reg_origin = (array) json_decode($_POST['hdd_certificadoregveh_reg']);

	$igual[0] = boolval($oCertificado->zona_id == $certregveh_reg_origin['tb_zona_id']);
	$igual[1] = boolval($fecha_generado == mostrar_fecha($certregveh_reg_origin['tb_certificadoregveh_fecgenerado']));
	$igual[2] = boolval($oCertificado->placa == $certregveh_reg_origin['tb_certificadoregveh_placa']);
	$igual[3] = boolval($oCertificado->cat == $certregveh_reg_origin['tb_certificadoregveh_cat']);
	$igual[4] = boolval($oCertificado->vehiculoclase_id == $certregveh_reg_origin['tb_vehiculoclase_id']);
	$igual[5] = boolval($oCertificado->vehiculomarca_id == $certregveh_reg_origin['tb_vehiculomarca_id']);
	$igual[6] = boolval($oCertificado->vehiculomodelo_id == $certregveh_reg_origin['tb_vehiculomodelo_id']);
	$igual[7] = boolval($oCertificado->ano == $certregveh_reg_origin['tb_certificadoregveh_ano']);
	$igual[8] = boolval($oCertificado->seriechasis == $certregveh_reg_origin['tb_certificadoregveh_seriechasis']);
	$igual[9] = boolval($oCertificado->color == $certregveh_reg_origin['tb_certificadoregveh_color']);
	$igual[10] = boolval($oCertificado->nromotor == $certregveh_reg_origin['tb_certificadoregveh_nromotor']);
	$igual[11] = boolval($oCertificado->comb == $certregveh_reg_origin['tb_certificadoregveh_comb']);
	$igual[12] = boolval($oCertificado->nropartida == $certregveh_reg_origin['tb_certificadoregveh_nropartida']);
	$igual[13] = boolval($oCertificado->upload_id == $certregveh_reg_origin['upload_id']);

	$son_iguales = true;
	$i = 0;
	while ($i < count($igual)) {
		if (!$igual[$i]) {
			$son_iguales = false;
		}
		$i++;
	}
	$i = 0;

	if ($son_iguales) {
		$data['estado'] = 2;
		$data['mensaje'] = "● No se realizó ninguna modificacion";
	}
	else {
		$certificadoregveh_id = $_POST['hdd_certificadoregveh_id'];
		$oCertificado->id = $certificadoregveh_id;

		$res_modificar = $oCertificado->modificar();
		if ($res_modificar == 1) {
			$data['mensaje'] = '✔ Certificado registral vehicular modificado correctamente';

			//actualizar la tabla de upload, set modulo_id con certificadoregveh_id donde el upload sea el recien subido
			if (!$igual[13] && $oUpload->modificar_campo($oCertificado->upload_id, 'modulo_id', $certificadoregveh_id, 'INT') != 1) {
				$data['mensaje'] .= "<br>❌ Error al actualizar upload con id del tb_certificadoregveh";
				echo json_encode($data); exit();
			}
			//si modifica color o combustible se modifica tambien en tb_creditogarveh y guarda historial
			if (!$igual[9] || !$igual[11]) {
				$mensaje = 'Modificó datos del vehículo:';
				if (!$igual[9] && $oCredito->modificar_campo($credito_id, 'tb_credito_vehcol', $oCertificado->color, 'STR') == 1) {
					$mensaje.="<br>- Ha actualizado el COLOR del vehiculo con placa {$oCertificado->placa} en el credito $credito_id, desde el proceso de ejecución legal id $ejecucion_id, tb_certificadoregveh_id $certificadoregveh_id: $cgv_veh_color => <b>{$oCertificado->color}</b>";
				}
				if ($cgv_veh_comb != $oCertificado->comb && $oCredito->modificar_campo($credito_id, 'tb_credito_vehcomb', $oCertificado->comb, 'STR') == 1) {
					$mensaje.="<br>- Ha actualizado el COMBUSTIBLE del vehiculo con placa {$oCertificado->placa} en el credito $credito_id, desde el proceso de ejecución legal id $ejecucion_id, tb_certificadoregveh_id $certificadoregveh_id: $cgv_veh_comb => <b>{$oCertificado->comb}</b>";
				}
				$mensaje.=" | <b>$fecha_hora</b>";

				$oHist->setTbHistUsureg($usuario_id);
				$oHist->setTbHistNomTabla('tb_creditogarveh_vehiculo');
				$oHist->setTbHistRegmodid($credito_id);
				$oHist->setTbHistDet($mensaje);
				$oHist->insertar();
			}
			$data['estado'] = 1;

			$oHist->setTbHistUsureg($usuario_id);
			$oHist->setTbHistNomTabla('upload');
			$oHist->setTbHistRegmodid($oCertificado->upload_id);
			$oHist->setTbHistDet("Ha actualizado el pdf $nombre_documento del crédito $credito_id en el proceso de ejecución legal id $ejecucion_id, tb_certificadoregveh_id $certificadoregveh_id | <b>$fecha_hora</b>");
			$oHist->insertar();

			//insertar historial
				$oHist->setTbHistNomTabla('tb_certificadoregveh');
				$oHist->setTbHistRegmodid($certificadoregveh_id);
				$mensaje = "Modificó el Certificado Reg Veh con id $certificadoregveh_id:";
				if (!$igual[0])
					$mensaje .= "<br> - Cambió la Zona registral: {$certregveh_reg_origin['tb_zona_nom']} => <b>{$_POST['zona_selected']}</b>";
				if (!$igual[1])
					$mensaje .= "<br> - Cambió la fecha en que se generó el certificado: ".mostrar_fecha($certregveh_reg_origin['tb_certificadoregveh_fecgenerado'])." => <b>$fecha_generado</b>";
				if (!$igual[2])
					$mensaje .= "<br> - Cambió la placa: {$certregveh_reg_origin['tb_certificadoregveh_placa']} => <b>{$oCertificado->placa}</b>";
				if (!$igual[3])
					$mensaje .= "<br> - Cambió la categoria: {$certregveh_reg_origin['tb_certificadoregveh_cat']} => <b>{$oCertificado->cat}</b>";
				if (!$igual[4])
					$mensaje .= "<br> - Cambió el carroceria: {$certregveh_reg_origin['tb_vehiculoclase_nom']} => <b>{$_POST['clase_selected']}</b>";
				if (!$igual[5])
					$mensaje .= "<br> - Cambió la marca: {$certregveh_reg_origin['tb_vehiculomarca_nom']} => <b>{$_POST['marca_selected']}</b>";
				if (!$igual[6])
					$mensaje .= "<br> - Cambió el modelo: {$certregveh_reg_origin['tb_vehiculomodelo_nom']} => <b>{$_POST['modelo_selected']}</b>";
				if (!$igual[7])
					$mensaje .= "<br> - Cambió el año: {$certregveh_reg_origin['tb_certificadoregveh_ano']} => <b>{$oCertificado->ano}</b>";
				if (!$igual[8])
					$mensaje .= "<br> - Cambió serie chasis: {$certregveh_reg_origin['tb_certificadoregveh_seriechasis']} => <b>{$oCertificado->seriechasis}</b>";
				if (!$igual[9])
					$mensaje .= "<br> - Cambió el color: {$certregveh_reg_origin['tb_certificadoregveh_color']} => <b>{$oCertificado->color}</b>";
				if (!$igual[10])
					$mensaje .= "<br> - Cambió el nro motor: {$certregveh_reg_origin['tb_certificadoregveh_nromotor']} => <b>{$oCertificado->nromotor}</b>";
				if (!$igual[11])
					$mensaje .= "<br> - Cambió el combustible: {$certregveh_reg_origin['tb_certificadoregveh_comb']} => <b>{$oCertificado->comb}</b>";
				if (!$igual[12])
					$mensaje .= "<br> - Cambió el nro partida: {$certregveh_reg_origin['tb_certificadoregveh_nropartida']} => <b>{$oCertificado->nropartida}</b>";
				if (!$igual[13])
					$mensaje .= "<br> - Actualizó el pdf: Upload id {$certregveh_reg_origin['upload_id']} => <b>{$oCertificado->upload_id}</b>";

				$oHist->setTbHistDet($mensaje);
				$oHist->insertar();
			//

			//guardar historial en la fase
				$oHist->setTbHistNomTabla('tb_ejecucionfase');
				$oHist->setTbHistRegmodid($_POST['hdd_certificadoregveh_ejecucionfase_id']);
				$oHist->setTbHistDet("Editó los datos del Certificado registral vehicular del crédito {$oCertificado->credito_id} | <b>". date("d-m-Y h:i a").'</b>');
				$oHist->insertar();
			//
		}
		unset($res_modificar);
	}
}
elseif ($action == 'eliminar') {
	$certificadoregveh_id = $_POST['hdd_certificadoregveh_id'];
	//hacer eliminar con xac en tb_certificadoregveh
	$res_update_crv = $oCertificado->modificar_campo($certificadoregveh_id, 'tb_certificadoregveh_xac', 0, 'INT');
	if ($res_update_crv != 1) {
		echo json_encode($data); exit();
	} else {
		$data['mensaje'] = '✔ Certificado registral vehicular eliminado correctamente';
	}
	unset($res_update_crv);

	//eliminar del pdf subido
	if ($oUpload->eliminar_xac($upload_id)) {
		$ruta = '';
		//OBTENER URL DE LA IMAGEN PARA ELIMINARLA POR COMPLETO DEL DIRECTORIO
		$result = $oUpload->mostrarUno($upload_id);
		if ($result['estado'] == 1) {
			$ruta = '../../' . $result['data']['upload_url'];
		}
		unset($result);

		if ($ruta != '') {
			// Verificar si pdf existe
			if (file_exists($ruta)) {
				// Intentar eliminar pdf
				if (unlink($ruta)) {
					if ($oUpload->eliminar($upload_id)) {
						$data['estado'] = 1;
						$data['mensaje'] .= '<br>✔ Pdf eliminado correctamente';
					}
				} else {
					$data['mensaje'] .= '<br>❌ Existe un error al eliminar el documento, se rechaza la eliminacion de la imagen en el directorio.';
				}
			} else {
				$data['mensaje'] .= "<br>❌ El pdf no existe en el directorio: $ruta";
			}
		}
    }
}
elseif ($action == 'dar_baja') {
	$certificadoregveh_id = $_POST['hdd_certificadoregveh_id'];
	//hacer eliminar con xac en tb_certificadoregveh
	$res_update_crv = $oCertificado->modificar_campo($certificadoregveh_id, 'tb_certificadoregveh_est', 0, 'INT');
	if ($res_update_crv != 1) {
		echo json_encode($data); exit();
	} else {
		$data['estado'] = 1;
		$data['mensaje'] = '✔ Certificado registral vehicular dado de baja correctamente';
		
		//guardar historial tb_hist de la tabla tb_certificadoregveh
			$oHist->setTbHistUsureg($usuario_id);
			$oHist->setTbHistNomTabla('tb_certificadoregveh');
			$oHist->setTbHistRegmodid($certificadoregveh_id);
			$oHist->setTbHistDet("Dió de baja el Certificado registral vehicular (ID: $certificadoregveh_id) del vehiculo con placa {$oCertificado->placa} en el proceso de ejecucion legal $ejecucion_id | <b>". date("d-m-Y h:i a").'</b>');
			$oHist->insertar();
		//
	}
	unset($res_update_crv);
}
elseif ($action == 'activa') {
	$certificadoregveh_id = $_POST['hdd_certificadoregveh_id'];
	//hacer eliminar con xac en tb_certificadoregveh
	$res_update_crv = $oCertificado->modificar_campo($certificadoregveh_id, 'tb_certificadoregveh_est', 1, 'INT');
	if ($res_update_crv != 1) {
		echo json_encode($data); exit();
	} else {
		$data['estado'] = 1;
		$data['mensaje'] = '✔ Certificado registral vehicular activado correctamente';
		
		//guardar historial tb_hist de la tabla tb_certificadoregveh
			$oHist->setTbHistUsureg($usuario_id);
			$oHist->setTbHistNomTabla('tb_certificadoregveh');
			$oHist->setTbHistRegmodid($certificadoregveh_id);
			$oHist->setTbHistDet("Activó el Certificado registral vehicular (ID: $certificadoregveh_id) del vehiculo con placa {$oCertificado->placa} en el proceso de ejecucion legal $ejecucion_id | <b>". date("d-m-Y h:i a").'</b>');
			$oHist->insertar();
		//
	}
	unset($res_update_crv);
}
else {
	$data['mensaje'] = "No se encontró la accion que intenta realizar";
}

echo json_encode($data);
