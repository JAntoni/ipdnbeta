<?php
require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
$oCertificado = new Certificadoregistralveh();
require_once '../funciones/fechas.php';

$lista = '';
$ejecucion_id = intval($_POST['ejecucion_id']);
$ejecucionfase_completado = $_POST['ejecucionfase_completado'];

$res = $oCertificado->listar_todos('', '', $ejecucion_id, '');
$fecha_hoy = date('Y-m-d');

if ($res['estado'] == 1) {
    $bandera = 1;
    foreach ($res['data'] as $key => $certificado) {
        $btn_eliminar = $certificado['tb_gastopago_fecha'] == $fecha_hoy ? '<button class="btn btn-danger btn-xs" title="Eliminar" onclick="certificadoregveh_form(\'E\', '.$certificado['tb_certificadoregveh_id'].', 1)"><i class="fa fa-trash"></i></button>' : '';
        $btn_modificar = $bandera == 1 && $ejecucionfase_completado != 1 ? '<button class="btn btn-warning btn-xs" title="Editar" onclick="certificadoregveh_form(\'M\', '.$certificado['tb_certificadoregveh_id'].', 1)"><i class="fa fa-edit"></i></button>' : '';

        if ($ejecucionfase_completado != 1) {
            $btn_darbaja_activa = $certificado['tb_certificadoregveh_est'] == 1 ? '<button class="btn btn-github btn-xs" title="Dar de baja Certificado" onclick="certificadoregveh_form(\'B\', '.$certificado['tb_certificadoregveh_id'].', 1)"><i class="fa fa-thumbs-down"></i></button>' : '<button class="btn btn-github btn-xs" title="Activar Certificado" onclick="certificadoregveh_form(\'A\', '.$certificado['tb_certificadoregveh_id'].', 1)"><i class="fa fa-thumbs-up"></i></button>';
        } else {
            $btn_darbaja_activa = '';
        }

        if ($certificado['tb_certificadoregveh_est'] != 1) {
            $color_cert_venc = 'FED3D4';
        } else {
            $color_cert_venc = restaFechas(fecha_mysql($certificado['tb_certificadoregveh_fecgenerado']), date('Y-m-d')) > 30 ? 'FED3D4' : 'D4EFDF';
        }
        $mens_cert_venc = restaFechas(fecha_mysql($certificado['tb_certificadoregveh_fecgenerado']), date('Y-m-d')) > 24 ? ' <b>(tiene '.restaFechas(fecha_mysql($certificado['tb_certificadoregveh_fecgenerado']), date('Y-m-d')).' días de antiguedad)' : '';
        $est_nom = array('dado de baja', 'activo');

        $lista .=
            '<tr id="tabla_cabecera_fila" style="background-color: #'.$color_cert_venc.'">
                <td id="tabla_fila">'.$certificado['tb_certificadoregveh_id']. '</td>
                <td id="tabla_fila"><b>'.strtoupper($est_nom[$certificado['tb_certificadoregveh_est']]).'</b></td>
                <td id="tabla_fila" align="left"><b>'.mostrar_fecha($certificado['tb_certificadoregveh_fecgenerado']).'</b>'.$mens_cert_venc.'</b></td>
                <td id="tabla_fila" align="left">'.$certificado['usu_reg_nom'].'</td>
                <td id="tabla_fila" align="left">'.mostrar_fecha($certificado['tb_certificadoregveh_fecreg']).'</td>
                <td id="tabla_fila">
                    <button class="btn btn-info btn-xs" title="Ver" onclick="certificadoregveh_form(\'L\', '.$certificado['tb_certificadoregveh_id'].', 1)"><i class="fa fa-eye"></i></button>
                    '.$btn_modificar.'
                    '.$btn_eliminar.'
                    '.$btn_darbaja_activa.'
                    <button class="btn btn-success btn-xs" title="Historial" onclick="historial_form(\'tb_certificadoregveh\', '.$certificado['tb_certificadoregveh_id'].')"><i class="fa fa-history"></i></button>
                </td>
            </tr>';
        //
        $bandera = 0;
    }
}
unset($res);
?>
<table id="tbl_certificadoregvehs" class="table table-hover" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">Id</th>
            <th id="tabla_cabecera_fila">Estado</th>
            <th id="tabla_cabecera_fila">Fecha generación</th>
            <th id="tabla_cabecera_fila">Usuario de registro</th>
            <th id="tabla_cabecera_fila">Fecha de registro</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $lista;?>
    </tbody>
</table>
