<?php
require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
$oCertificado = new Certificadoregistralveh();
require_once '../funciones/fechas.php';

$certificado_id = (empty($certificado_id))? $_POST['certificado_id'] : $certificado_id;
$ejecucion_id = (empty($ejecucion_id))? $_POST['ejecucion_id'] : $ejecucion_id;
$incluye_filtro_estado = (empty($incluye_filtro_estado))? $_POST['incluye_filtro_estado'] : $incluye_filtro_estado;

$estado = '';
if ($incluye_filtro_estado == 1) {
	$estado = (empty($estado))? $_POST['estado'] : $estado;
}

$option = '<option value="0">Seleccione</option>';

//PRIMER NIVEL
$result = $oCertificado->listar_todos('', '', $ejecucion_id, $estado);
if ($result['estado'] == 1) {
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		if ($certificado_id == $value['tb_certificadoregveh_id'])
			$selected = 'selected';
		//

		$color_cert_venc = restaFechas(fecha_mysql($value['tb_certificadoregveh_fecgenerado']), date('Y-m-d')) > 30 ? ' background-color: #FED3D4;' : '';
		
		$option .=
		'<option value="'.$value['tb_certificadoregveh_id'].'" data-antig="'.restaFechas(fecha_mysql($value['tb_certificadoregveh_fecgenerado']), date('Y-m-d')).'" style="font-weight: bold;'.$color_cert_venc.'" '.$selected.'>'. "Generado el ". mostrar_fecha($value['tb_certificadoregveh_fecgenerado']).', '. "Nro. partida ".$value['tb_certificadoregveh_nropartida'].', '. "Color ". $value['tb_certificadoregveh_color'].', '. "Combustible ". $value['tb_certificadoregveh_comb'] .'</option>';
	}
}
unset($result);
//FIN PRIMER NIVEL
echo $option;

