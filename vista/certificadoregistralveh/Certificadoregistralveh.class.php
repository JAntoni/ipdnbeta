<?php
if (defined('APP_URL'))
    require_once APP_URL . 'datos/conexion.php';
else
    require_once '../../datos/conexion.php';
//
class Certificadoregistralveh extends Conexion {
    public $id;
    public $credito_id;
    public $ejecucion_id;
    public $upload_id;
    public $gastopago_id;
    public $zona_id;
    public $fecgenerado;
    public $placa;
    public $cat;
    public $vehiculoclase_id;
    public $vehiculomarca_id;
    public $vehiculomodelo_id;
    public $ano;
    public $seriechasis;
    public $color;
    public $nromotor;
    public $comb;
    public $nropartida;
    public $fecreg;
    public $usureg;
    public $xac;
    public $est;

    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_certificadoregveh
                    (
                        tb_credito_id,
                        tb_ejecucion_id,
                        tb_zona_id,
                        tb_certificadoregveh_fecgenerado,
                        tb_certificadoregveh_placa,
                        tb_certificadoregveh_cat,
                        tb_vehiculoclase_id,
                        tb_vehiculomarca_id,
                        tb_vehiculomodelo_id,
                        tb_certificadoregveh_ano,
                        tb_certificadoregveh_seriechasis,
                        tb_certificadoregveh_color,
                        tb_certificadoregveh_nromotor,
                        tb_certificadoregveh_comb,
                        tb_certificadoregveh_nropartida,
                        tb_certificadoregveh_usureg,
                        upload_id
                    )
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5,
                        :param6,
                        :param7,
                        :param8,
                        :param9,
                        :param10,
                        :param11,
                        :param12,
                        :param13,
                        :param14,
                        :param15,
                        :param16
                    )";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->ejecucion_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->zona_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param3', $this->fecgenerado, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->placa, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->cat, PDO::PARAM_STR);
            $sentencia->bindParam(':param6', $this->vehiculoclase_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param7', $this->vehiculomarca_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param8', $this->vehiculomodelo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param9', $this->ano, PDO::PARAM_INT);
            $sentencia->bindParam(':param10', $this->seriechasis, PDO::PARAM_STR);
            $sentencia->bindParam(':param11', $this->color, PDO::PARAM_STR);
            $sentencia->bindParam(':param12', $this->nromotor, PDO::PARAM_STR);
            $sentencia->bindParam(':param13', $this->comb, PDO::PARAM_STR);
            $sentencia->bindParam(':param14', $this->nropartida, PDO::PARAM_STR);
            $sentencia->bindParam(':param15', $this->usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param16', $this->upload_id, PDO::PARAM_INT);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_certificadoregveh SET
                        tb_zona_id = :param0,
                        tb_certificadoregveh_fecgenerado = :param1,
                        tb_certificadoregveh_placa = :param2,
                        tb_certificadoregveh_cat = :param3,
                        tb_vehiculoclase_id = :param4,
                        tb_vehiculomarca_id = :param5,
                        tb_vehiculomodelo_id = :param6,
                        tb_certificadoregveh_ano = :param7,
                        tb_certificadoregveh_seriechasis = :param8,
                        tb_certificadoregveh_color = :param9,
                        tb_certificadoregveh_nromotor = :param10,
                        tb_certificadoregveh_comb = :param11,
                        tb_certificadoregveh_nropartida = :param12,
                        upload_id = :param13
                    WHERE tb_certificadoregveh_id = :param_x;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->zona_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param1", $this->fecgenerado, PDO::PARAM_STR);
            $sentencia->bindParam(":param2", $this->placa, PDO::PARAM_STR);
            $sentencia->bindParam(":param3", $this->cat, PDO::PARAM_STR);
            $sentencia->bindParam(":param4", $this->vehiculoclase_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param5", $this->vehiculomarca_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param6", $this->vehiculomodelo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param7", $this->ano, PDO::PARAM_INT);
            $sentencia->bindParam(":param8", $this->seriechasis, PDO::PARAM_STR);
            $sentencia->bindParam(":param9", $this->color, PDO::PARAM_STR);
            $sentencia->bindParam(":param10", $this->nromotor, PDO::PARAM_STR);
            $sentencia->bindParam(":param11", $this->comb, PDO::PARAM_STR);
            $sentencia->bindParam(":param12", $this->nropartida, PDO::PARAM_STR);
            $sentencia->bindParam(":param13", $this->upload_id, PDO::PARAM_STR);
            $sentencia->bindParam(":param_x", $this->id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_todos($certificadoregveh_id, $credito_id, $ejecucion_id, $est) {
        $where_opt = '';
        if (!empty($certificadoregveh_id)) {
            $where_opt .= ' AND crv.tb_certificadoregveh_id = :param_opc0';
        }
        if (!empty($credito_id)) {
            $where_opt .= ' AND crv.tb_credito_id = :param_opc1';
        }
        if (!empty($ejecucion_id)) {
            $where_opt .= ' AND crv.tb_ejecucion_id = :param_opc2';
        }
        if (in_array(strval($est), array('0', '1'))) {
            $where_opt .= ' AND crv.tb_certificadoregveh_est = :param_opc3';
        }
        
        try {
            $sql = "SELECT crv.*,
                        z.tb_zona_nom,
                        vc.tb_vehiculoclase_nom,
                        vma.tb_vehiculomarca_nom,
                        vmo.tb_vehiculomodelo_nom,
                        CONCAT (u.tb_usuario_ape, ' ', u.tb_usuario_nom) AS usu_reg_nom,
                        gp.tb_gastopago_fecha,
                        up.upload_url
                    FROM tb_certificadoregveh crv
                    INNER JOIN tb_zonaregistral z ON crv.tb_zona_id = z.tb_zona_id
                    INNER JOIN tb_vehiculoclase vc ON crv.tb_vehiculoclase_id = vc.tb_vehiculoclase_id
                    INNER JOIN tb_vehiculomarca vma ON crv.tb_vehiculomarca_id= vma.tb_vehiculomarca_id
                    INNER JOIN tb_vehiculomodelo vmo ON crv.tb_vehiculomodelo_id = vmo.tb_vehiculomodelo_id
                    INNER JOIN tb_usuario u ON crv.tb_certificadoregveh_usureg = u.tb_usuario_id
                    LEFT JOIN tb_gastopago gp ON crv.tb_gastopago_id = gp.tb_gastopago_id
                    LEFT JOIN upload up ON crv.upload_id = up.upload_id
                    WHERE crv.tb_certificadoregveh_xac = 1 $where_opt
                    ORDER BY crv.tb_certificadoregveh_fecreg DESC;"; //no cambiar el orden desc, se usa en doc_demanda.php
            //
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($certificadoregveh_id)) {
                $sentencia->bindParam(':param_opc0', $certificadoregveh_id, PDO::PARAM_INT);
            }
            if (!empty($credito_id)) {
                $sentencia->bindParam(':param_opc1', $credito_id, PDO::PARAM_INT);
            }
            if (!empty($ejecucion_id)) {
                $sentencia->bindParam(':param_opc2', $ejecucion_id, PDO::PARAM_INT);
            }
            if (in_array(strval($est), array('0', '1'))) {
                $sentencia->bindParam(':param_opc3', $est, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function modificar_campo($certificadoregveh_id, $certificadoregveh_columna, $certificadoregveh_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($certificadoregveh_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_certificadoregveh SET " . $certificadoregveh_columna . " = :certificadoregveh_valor WHERE tb_certificadoregveh_id = :certificadoregveh_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":certificadoregveh_id", $certificadoregveh_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":certificadoregveh_valor", $certificadoregveh_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":certificadoregveh_valor", $certificadoregveh_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
