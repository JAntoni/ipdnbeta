<?php
require_once '../funciones/fechas.php';
require_once '../certificadoregistralveh/Certificadoregistralveh.class.php';
$oCertificado = new Certificadoregistralveh();

$out = 'no llegó al listado';
$res = $oCertificado->listar_todos('', '', '', 1);
$out = "res['estado'] = 0";

if ($res['estado'] == 1) {
    $out = "res['estado'] = 1<br>";
    $hubo_mod = 0;
    foreach ($res['data'] as $key => $certificado) {
        if (restaFechas(fecha_mysql($certificado['tb_certificadoregveh_fecgenerado']), date('Y-m-d')) > 30) {
            $hubo_mod = 1;
            $oCertificado->modificar_campo($certificado['tb_certificadoregveh_id'], 'tb_certificadoregveh_est', 0, 'INT');
            $out .= "- se modificó el certificado Reg Veh con id {$certificado['tb_certificadoregveh_id']}<br>";
        }
    }
    if ($hubo_mod == 0) {
        $out .= "- no se modificó ningún certificado reg veh";
    }
}

echo $out;
