<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../inventarioproducto/Inventario.class.php');
  	$oInventario = new Inventario();

 	$action = $_POST['action'];

 	if($action == 'insertar'){

 		$inventario_usereg = $_SESSION['usuario_id']; 
 		$inventario_fecini = date('Y-m-d');
 		$inventario_des = $_POST['txt_inventario_des'];
 		$almacen_id = $_POST['cmb_inventario_almacen_id'];
 		$inventario_id = intval($_POST['inventario_id']);

		/* if($inventario_id>0){
			var_dump($inventario_id);
		} */

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Inventario.';

		$res= $oInventario->insertar($inventario_usereg, $inventario_fecini, $inventario_des, $almacen_id);
 		if(intval($res['estado']) == 1){
			$ultimoReg = $oInventario->mostrarUltimoID();
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Inventario registrado correctamente.';
			$data['registro'] = $oInventario->mostrarUno($res['nuevo'])['data'];
			$data['inventario_id'] = $ultimoReg['data']['tb_inventario_id'];//ultimo registro
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
		$inventario_des = $_POST['txt_inventario_des'];
		$almacen_id = $_POST['cmb_inventario_almacen_id'];
		$inventario_id = intval($_POST['hdd_inventario_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar al Inventario.';

 		if($oInventario->modificar($inventario_id, $inventario_des, $almacen_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Inventario modificado correctamente.';
			$data['registro'] = $oInventario->mostrarUno($inventario_id)['data'];
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar_inventario'){
 		$inventario_id = intval($_POST['inventario_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Inventario.';

		// eliminar detalles del inventario
		$detalles = $oInventario->mostrar_todos_detalle($inventario_id,$codigo="");
		if(!is_null($detalles)){
			if($detalles['estado'] == 1){
				foreach ($detalles['data'] as $key => $detalle) {
					$oInventario->eliminar_detalle($detalle['tb_inventario_detalle_id']);
				}
			}
		}

		// eliminar inventario
 		if($oInventario->eliminar($inventario_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Inventario eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

	 elseif($action == 'guardar_garantia'){

		$inventario_usereg = $_SESSION['usuario_id']; 
		$garantia_id = intval($_POST['garantia_id']);
		$inventario_id = intval($_POST['inventario_id']);

		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al inventariar la Garantía.';

		$existe = $oInventario->existe_garantia($garantia_id, $inventario_id);
		if($existe['data'] == false){

			$res= $oInventario->guardar_garantia_inventario($inventario_usereg, $garantia_id, $inventario_id);
			// sumar garantias inventariadas
			$inventario = $oInventario->mostrarUno($inventario_id);
			$inventario = $oInventario->modificar_campo($inventario_id, 'tb_inventario_total_prod', ($inventario['data']['tb_inventario_total_prod'] + 1), 'INT');
			//
		
			$data['estado'] = 1;
			$data['mensaje'] = 'Garantía inventariada correctamente.';
			$data['inventario_id'] = $inventario_ids;
		
		}else{

			$data['estado'] = 0;
			$data['mensaje'] = 'La garantía seleccionada ya se encuentra inventariada.';
			$data['inventario_id'] = '';

		}
		

		echo json_encode($data);
	}
	
	elseif($action == 'eliminar_garantia'){

		$inventario_detalle_id = intval($_POST['inventario_detalle_id']);
		$inventario_id = intval($_POST['inventario_id']);

		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al eliminar la Garantía.';

		// restar garantias inventariadas
		$inventario = $oInventario->mostrarUno($inventario_id);
		$inventario = $oInventario->modificar_campo($inventario_id, 'tb_inventario_total_prod', ($inventario['data']['tb_inventario_total_prod'] - 1), 'INT');
		//

		if($oInventario->eliminar_detalle($inventario_detalle_id)){
			$data['estado'] = 1;
			$data['mensaje'] = 'Garantía eliminada correctamente.';
			$data['inventario_detalle_id'] = $inventario_detalle_ids;
		}

		echo json_encode($data);
	}

	elseif($action == 'guardar_comentario'){

		$comentario = $_POST['comentario']; 
		$inventario_detalle_id = intval($_POST['inventario_detalle_id']);
		$inventario_id = intval($_POST['inventario_id']);

		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al registrar Comentario.';

		$inventario = $oInventario->modificar_campo_detalle($inventario_detalle_id, 'tb_inventario_detalle_des', $comentario, 'STR');
		
		//if($oProveedor->eliminar($proveedor_id)){
			$data['estado'] = 1;
			$data['mensaje'] = 'Comentario registrado correctamente.';
			$data['inventario_id'] = $inventario_id;
		//}

		echo json_encode($data);
	}

	elseif($action == 'fin_inventario'){

		$inventario_id = intval($_POST['inventario_id']);
		$fecha = date('Y-m-d');

		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al finalizar el Inventario.';

		$res= $oInventario->finalizar_inventario($inventario_id, $fecha);

		//if($oProveedor->eliminar($proveedor_id)){
			$data['estado'] = 1;
			$data['mensaje'] = 'Inventario finalizado correctamente.';
			$data['inventario_id'] = $inventario_ids;
		//}

		echo json_encode($data);
	}

	elseif($action == 'obtener_una_garantia'){

		$almacen_id = intval($_POST['almacen_id']);
		$codigo = $_POST['codigo_barra'];

		$data['estado'] = 0;
		$data['mensaje'] = 'Existe un error al buscar la garantía.';

		$res = $oInventario->mostrar_garantia_codigo_barra(0, $almacen_id, $codigo);

		if($res!=null || $res!=''){
			$data['estado'] = 1;
			$data['mensaje'] = 'Garantía encontrada correctamente.';
			$data['garantia_id'] = $res['data'];
			$data['credito_id'] = $res['data1'];
			$data['producto'] = $res['data2'];
			$data['cliente'] = $res['data3'];
			$data['cantidad'] = $res['data4'];
		}

		echo json_encode($data);
	}

	elseif($action == 'detectar_existe_garantia_en_inventario'){

		$inventario_id = intval($_POST['inventario_id']);
		$garantia_id = intval($_POST['garantia_id']);

		$data['estado'] = 0;
		$data['mensaje'] = 'No existe garantía en inventario.';

		//$res = $oInventario->existe_garantia_en_inventario($inventario_id, $garantia_id);
		$res = $oInventario->existe_garantia($garantia_id, $inventario_id);

		if($res['estado']==1){
			$data['estado'] = 1;
			$data['mensaje'] = 'Garantía encontrada en inventario.';
		}

		echo json_encode($data);
	}
	

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>