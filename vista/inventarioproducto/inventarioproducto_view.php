<?php
  session_name("ipdnsac");
  session_start();
  require_once('../perfilmenu/PerfilMenu.class.php');
  $oPerfilMenu = new PerfilMenu();
  require_once('../permiso/Permiso.class.php');
  $oPermiso = new Permiso();
  require_once('../inventarioproducto/Inventario.class.php');
  $oInventario = new Inventario();
  require_once('../almacen/Almacen.class.php');
  $oAlmacen = new Almacen();
  require_once('../usuario/Usuario.class.php');
  $oUsuario = new Usuario();
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');


  $direc = 'inventario';
  $usuarioperfil_id = $_SESSION['usuarioperfil_id'];
  $usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
  $inventario_id = $_POST['inventario_id'];
  $estado = $_POST['estado'];
  $vista = $_POST['vista'];
  
  $titulo = '';
  if($usuario_action == 'L')
    $titulo = 'Inventario Registrado';
  if($usuario_action == 'I')
    $titulo = 'Registrar Inventario';
  elseif($usuario_action == 'M')
    $titulo = 'Editar Inventario';
  elseif($usuario_action == 'E')
    $titulo = 'Eliminar Inventario';
  else
    $titulo = 'Acción de Usuario Desconocido';

  $action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

  //antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en proveedor
  $result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
  $bandera = 0; $mensaje = '';

  // lista almacenes
  $listadoAlmacen = $oAlmacen->mostrarTodos();

  // Nombre usuario
  /* $usuario_nom = '';
  $user = $oUsuario->mostrarUno($_SESSION['usuario_id']);
  $usuario_nom = $user['data']['tb_usuario_nom'].' '.$user['data']['tb_usuario_ape']; */
  //

  // Fecha hoy
  $hoy = '';
  $hoy = date('d-m-Y');
  //

  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
    }

    $result = NULL;
    $array_permisos = explode('-', $permisos);

    if(in_array($usuario_action, $array_permisos))
      $bandera = 1;

    //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
    if($bandera == 0){
      $usuario_id = $_SESSION['usuario_id']; $modulo = 'inventario'; $modulo_id = $inventario_id; $tipo_permiso = $usuario_action; $estado = 1;

      $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
        if($result['estado'] == 1){
          $bandera = 1; // si tiene el permiso para la accion que desea hacer
        }
        else{
          $result = NULL;
          echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
          exit();
        }
      $result = NULL;
    }

    //si la accion es modificar, mostramos los datos del inventario por su ID
    $usuario_nom = '';
    if(intval($inventario_id) > 0){
      $result = $oInventario->mostrarUno($inventario_id);
        if($result['estado'] != 1){
          $mensaje =  'No se ha encontrado ningún registro para el inventario seleccionado, inténtelo nuevamente.';
          $bandera = 4;
        }
        else{
          $inventario_des = $result['data']['tb_inventario_des'];
          $inventario_fecini = $result['data']['tb_inventario_fecini'];
          $inventario_fecfin = $result['data']['tb_inventario_fecfin'];
          $usuario_id = $result['data']['tb_inventario_usereg'];
          $almacen_id = $result['data']['tb_almacen_id'];

          $user = $oUsuario->mostrarUno($usuario_id);
          $usuario_nom = $user['data']['tb_usuario_nom'].' '.$user['data']['tb_usuario_ape'];

        }
      $result = NULL;
    }

  }
  else{
    $mensaje =  $result['mensaje'];
    $bandera = 4;
    $result = NULL;
  }
?>
<?php if($bandera == 1): ?>
  <style type="text/css">
    #div_garantias_tabla, #div_inventario_detalle_tabla {
      height: 300px;
      border: 1px solid #ddd;
      background: #f1f1f1;
      overflow-y: scroll;
    }

    .row-selected{
    background: yellow;
  }


  .other-clic{
    background: green !important;
    color: white;
  }

  #div_garantias_tabla tbody tr{
    cursor: pointer;
  }

  .my-swal {
    position: fixed;
    z-index: X !important;
  }

  </style>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_view_inventario" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo;?></h4>
        </div>
        <form id="view_inventarioproducto" method="post">
          <input type="hidden" name="action" id="action" value="<?php echo $action;?>">
          <input type="hidden" id="inventario_vista" value="<?php echo $vista;?>">
          <input type="hidden" id="hdd_inventario_id" value="<?php echo $inventario_id;?>">
          <input type="hidden" id="hdd_inventario_id" name="hdd_inventario_id" value="<?php echo $inventario_id;?>">
          <input type="hidden" id="hdd_estado" name="hdd_estado" value="<?php echo $estado;?>">
          
          <div class="modal-body">
            <div class="row">

                <div class="col-md-12">
                  <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Datos Principales</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                    </div>
                    <div class="box-body">
                      
                      <div class="form-group col-md-5">
                        <label for="cmb_inventario_almacen_id" class="control-label">Almacén</label>
                        <select class="form-control input-sm" name="cmb_inventario_almacen_id" id="cmb_inventario_almacen_id" style="width: 100%;">
                          <?php //require_once('../almacen/almacen_select.php'); ?>
                          <?php 
                          if($listadoAlmacen['estado'] == 1) {
                            foreach ($listadoAlmacen['data'] as $key => $value) {
                          ?>
                            <option value="<?php echo $value['tb_almacen_id']; ?>" <?php if($value['tb_almacen_id']==$almacen_id){echo "selected='selected'";} ?> style="font-weight: bold;"><?php echo $value['tb_almacen_nom']; ?></option>
                          <?php 
                            }
                          } 
                          ?>
                        </select>
                      </div>
                      <div class="form-group col-md-5">
                        <label for="txt_use" class="control-label">Responsable</label>
                        <input class="form-control input-sm" type="text" value="<?php echo $usuario_nom; ?>" readonly id="">
                      </div>
                      <div class="form-group col-md-2">
                        <label for="txt_fecini" class="control-label">Fecha</label>
                        <input class="form-control input-sm" type="text" value="<?php echo mostrar_fecha($inventario_fecini); ?>" readonly id="txt_inventario_fecini">
                      </div>
                      <div class="col-md-12">
                        <label for="txt_inventario_des" class="control-label">Descripción</label>
                        <div class="form-group">
                          <textarea class="form-control input-sm" name="txt_inventario_des" id="txt_inventario_des" rows="4" style="resize:none;"><?php echo $inventario_des; ?></textarea>
                        </div>
                      </div>
                    
                    </div>
                  </div>
                </div>
                
            </div>

            <div class="row" id="detalle_inventario">

              <div class="col-md-12">
                <div class="box box-primary box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">Detalle de Inventario</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="box-body">

                    <div class="col-md-12">

                      <?php if($estado == 1){ ?>

                        <div class="col-md-5" style="padding-left: 0px;">
                          <div class="input-group margin">
                            <div class="input-group-btn">
                              <button type="button" class="btn btn-info"><i class="fa fa-barcode"></i> Garantía</button>
                            </div>
                            <input type="text" class="form-control" id="txt_codigo_barra">
                          </div>
                        </div>
                        <div align="center" class="col-md-2" id="">
                            <a class="btn btn-primary btn-app" id="gar_seleccionada" onclick="addGarantia()">
                              <i class="fa fa-arrow-right"></i> Pasar a la derecha
                            </a>
                        </div>
                        <div class="col-md-5" style="padding-right: 0px;">
                          <div class="input-group margin">
                            <input type="text" class="form-control" id="txt_garantia_inventario">
                            <div class="input-group-btn">
                              <button type="button" class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
                            </div>
                          </div>
                        </div>

                      <?php } ?>

                      <div align="center" id="div_garantias_tabla" class="col-md-6">

                      </div>

                      <div align="center" id="div_inventario_detalle_tabla" class="col-md-6">
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if($action == 'eliminar'):?>
                <div class="callout callout-warning">
                  <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Inventario?</h4>
                </div>
              <?php endif;?>

              <!--- MESAJES DE GUARDADO -->
              <div class="callout callout-info" id="inventario_mensaje_view" style="display: none;">
                <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
              </div>

              
              

          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if($estado == 1): ?>
                <?php if($usuario_action == 'I' || $usuario_action == 'M'): ?>
                  <button type="button" class="btn btn-info" id="btn_view_inventario" onclick="actualizarInventario();">Actualizar</button>
                <?php endif; ?>
                <?php if($usuario_action == 'E'): ?>
                  <button type="button" class="btn btn-info" id="btn_view_inventario">Aceptar</button>
                <?php endif; ?>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>         
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if($bandera == 4): ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_view_inventario">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje;?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<div class="modal fade" id="confirm_modal_add_garantia" tabindex="-1">
                  <div class="modal-dialog modal-dialog-centered rounded-0">
                      <div class="modal-content rounded-0">
                          <div class="modal-header py-1">
                              <h5 class="modal-title"><strong>CONFIRMACIÓN</strong></h5>
                          </div>
                          <div class="modal-body">
                          </div>
                          <div class="modal-footer py-1">
                              <button type="button" class="btn btn-primary btn-sm rounded-0 py-1" id="confirm-btn-add-garantia">Confirmar</button>
                              <button type="button" class="btn btn-danger btn-sm rounded-0 py-1" id="cancel-btn-add-garantia" data-bs-dismiss="modal">Cancelar</button>
                          </div>
                      </div>
                  </div>
              </div>

<script type="text/javascript" src="<?php echo 'vista/inventarioproducto/inventarioproducto_form.js?ver=24042023';?>"></script>
