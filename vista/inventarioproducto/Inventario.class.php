<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Inventario extends Conexion {

    public $inventario_id;
    public $inventario_xac = 1;
    public $inventario_fecreg;
    public $inventario_usereg;
    public $inventario_fecini; // fecha inicio de inventario
    public $inventario_fecfin; // fecha fin de inventario
    public $inventario_des; // descripcion
    public $inventario_total_prod; // número total de garantías
    public $almacen_id;

    function insertar($inventario_usereg, $inventario_fecini, $inventario_des, $almacen_id) {
        $this->dblink->beginTransaction();
        try {
            $this->almacen_id = $_SESSION['empresa_id'];
            $sql = "INSERT INTO tb_inventario (
                                        tb_inventario_xac, 
                                        tb_inventario_fecreg,
                                        tb_inventario_usereg, 
                                        tb_inventario_fecini, 
                                        tb_inventario_fecfin, 
                                        tb_inventario_des, 
                                        tb_inventario_total_prod, 
                                        tb_almacen_id)
                                VALUES (
                                        :inventario_xac, 
                                        NOW(),
                                        :inventario_usereg, 
                                        :inventario_fecini, 
                                        NULL, 
                                        :inventario_des, 
                                        0, 
                                        :almacen_id);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":inventario_xac", $this->inventario_xac, PDO::PARAM_INT);
            $sentencia->bindParam(":inventario_usereg", $inventario_usereg, PDO::PARAM_STR);
            $sentencia->bindParam(":inventario_fecini", $inventario_fecini, PDO::PARAM_STR);
            $sentencia->bindParam(":inventario_des", $inventario_des, PDO::PARAM_STR);
            $sentencia->bindParam(":almacen_id", $almacen_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            $garantia_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            $data['garantia_id'] = $garantia_id;
            return $data;
//            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar($inventario_id, $inventario_des, $almacen_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = 'UPDATE tb_inventario SET 
                                tb_inventario_des =:inventario_des, 
                                tb_almacen_id =:almacen_id
                                WHERE tb_inventario_id = :inventario_id';

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":inventario_id", $inventario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":inventario_des", $inventario_des, PDO::PARAM_STR);
            $sentencia->bindParam(":almacen_id", $almacen_id, PDO::PARAM_INT);
            
            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    // en uso
    function guardar_garantia_inventario($inventario_usereg, $garantia_id, $inventario_id){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_inventario_detalle (
                                    tb_inventario_detalle_fecreg,
                                    tb_inventario_detalle_usereg,
                                    tb_inventario_detalle_des,
                                    tb_inventario_id,
                                    tb_garantia_id)
                            VALUES (
                                    NOW(),
                                    :tb_inventario_detalle_usereg,
                                    '',
                                    :tb_inventario_id,
                                    :tb_garantia_id
                            );"; 

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_inventario_detalle_usereg", $inventario_usereg, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_inventario_id", $inventario_id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_garantia_id", $garantia_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    // en uso
    function eliminar($inventario_id) {
        $this->dblink->beginTransaction();
        try {
                // Eliminar inventario
                $sql = "UPDATE tb_inventario SET tb_inventario_xac = 0 WHERE tb_inventario_id = :inventario_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":inventario_id", $inventario_id, PDO::PARAM_INT);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    // en uso
    function eliminar_detalle($inventario_detalle_id) {
        $this->dblink->beginTransaction();
        try {
                // Eliminar detalles del inventario
                $sql = "UPDATE tb_inventario_detalle SET tb_inventario_detalle_xac = 0 WHERE tb_inventario_detalle_id = :inventario_detalle_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":inventario_detalle_id", $inventario_detalle_id, PDO::PARAM_INT);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    // en uso
    function mostrarUltimoID() {
        try {
            $sql = "SELECT tb_inventario_id FROM tb_inventario ORDER BY tb_inventario_id DESC LIMIT 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // en uso
    function mostrarUnoReporte($inventario_id) {
        try {
            $sql = "SELECT inv.*, alm.tb_almacen_nom as almacen, CONCAT(usr.tb_usuario_nom, ' ', usr.tb_usuario_ape) as usuario FROM tb_inventario inv 
            INNER JOIN tb_almacen alm ON inv.tb_almacen_id = alm.tb_almacen_id
            INNER JOIN tb_usuario usr ON inv.tb_inventario_usereg = usr.tb_usuario_id
            WHERE inv.tb_inventario_xac = 1 AND tb_inventario_id =:inventario_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":inventario_id", $inventario_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
            
    
    function mostrarUno($inventario_id) {
        try {
            $sql = "SELECT * FROM tb_inventario WHERE tb_inventario_id =:inventario_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":inventario_id", $inventario_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // en uso
    function mostrarUnDetalle($inventario_detalle_id) {
        try {
            $sql = "SELECT * FROM tb_inventario_detalle WHERE tb_inventario_detalle_id =:inventario_detalle_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":inventario_detalle_id", $inventario_detalle_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // en uso
    function finalizar_inventario($inventario_id, $fecha) {
        $this->dblink->beginTransaction();
        try {

                $sql = "UPDATE tb_inventario SET tb_inventario_fecfin = :fecha WHERE tb_inventario_id = :inventario_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":inventario_id", $inventario_id, PDO::PARAM_INT);
                $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }


    // en uso
    function mostrar_todos_filtro($alm_id, $fecini, $fecfin, $use_id) {
        try {
            $sql = "SELECT inv.*, alm.tb_almacen_nom as almacen, CONCAT(usr.tb_usuario_nom, ' ', usr.tb_usuario_ape) as usuario FROM tb_inventario inv 
                    INNER JOIN tb_almacen alm ON inv.tb_almacen_id = alm.tb_almacen_id
                    INNER JOIN tb_usuario usr ON inv.tb_inventario_usereg = usr.tb_usuario_id
                    WHERE inv.tb_inventario_xac = 1";
            
            if ($alm_id > 0){
                $sql .= ' and inv.tb_almacen_id = :tb_almacen_id';
            }
            if ($fecini != ''){
                $sql .= ' and inv.tb_inventario_fecini >= :tb_inventario_fecini ';
            }
            if ($fecfin != ''){
                $sql .= ' and inv.tb_inventario_fecini <= :tb_inventario_fecfin ';
            }
            if ($use_id > 0)
                $sql .= ' and inv.tb_inventario_usereg = :tb_usuario_id';
            
            $sql .= ' GROUP BY inv.tb_inventario_id order by inv.tb_inventario_id DESC;';
            $sentencia = $this->dblink->prepare($sql);
                
                if (intval($alm_id) > 0)
                    $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
                if ($fecini != '')
                    $sentencia->bindParam(":tb_inventario_fecini", $fecini, PDO::PARAM_STR);
                if ($fecfin != '')
                    $sentencia->bindParam(":tb_inventario_fecfin", $fecfin, PDO::PARAM_STR);
                if (intval($use_id) > 0)
                    $sentencia->bindParam(":tb_usuario_id", $use_id, PDO::PARAM_INT);
        
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // en uso
    function mostrar_todos_detalle($inventario_id, $codigo) {
        try {
            $sql = "SELECT gar.*, invd.tb_inventario_detalle_id, invd.tb_inventario_detalle_des, gar.tb_garantia_pro as producto, cli.tb_cliente_nom
                    FROM tb_inventario_detalle invd
                    INNER JOIN tb_garantia gar ON invd.tb_garantia_id = gar.tb_garantia_id
                    INNER JOIN tb_creditomenor cre ON gar.tb_credito_id = cre.tb_credito_id
                    INNER JOIN tb_cliente cli ON cre.tb_cliente_id = cli.tb_cliente_id
                    WHERE invd.tb_inventario_detalle_xac = 1";
            
            if ($codigo > 0){
                $sql .= ' AND gar.tb_credito_id = :tb_credito_id';
            }

            if ($inventario_id > 0){
                $sql .= ' AND invd.tb_inventario_id = :tb_inventario_id';
            }
            
            $sql .= ' ORDER BY invd.tb_inventario_id DESC;';
            $sentencia = $this->dblink->prepare($sql);
                
            if (intval($codigo) > 0){
                $sentencia->bindParam(":tb_credito_id", $codigo, PDO::PARAM_INT);
            }
            if (intval($inventario_id) > 0){
                $sentencia->bindParam(":tb_inventario_id", $inventario_id, PDO::PARAM_INT);
            }

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "éxito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay garantías registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // en uso
    function modificar_campo($inventario_id, $inventario_columna, $inventario_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($inventario_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_inventario SET " . $inventario_columna . " = :inventario_valor WHERE tb_inventario_id = :inventario_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":inventario_id", $inventario_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":inventario_valor", $inventario_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":inventario_valor", $inventario_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    // en uso
    function modificar_campo_detalle($inventario_detalle_id, $inventario_detalle_columna, $inventario_detalle_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($inventario_detalle_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_inventario_detalle SET " . $inventario_detalle_columna . " = :inventario_detalle_valor WHERE tb_inventario_detalle_id = :inventario_detalle_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":inventario_detalle_id", $inventario_detalle_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":inventario_detalle_valor", $inventario_detalle_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":inventario_detalle_valor", $inventario_detalle_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrar_todos_filtro_garantias($est, $alm_id, $cli_id, $list_ga, $inventario_id, $codigo) {
        //$filtro = "%".$codigo."%";
        try {
            $sql = "SELECT ga.*, cli.tb_cliente_nom, al.tb_almacen_nom FROM tb_garantia ga 
                    INNER JOIN tb_creditomenor cre on ga.tb_credito_id = cre.tb_credito_id
                    INNER JOIN tb_cliente cli on cre.tb_cliente_id = cli.tb_cliente_id
                    INNER JOIN tb_garantiatipo gt on (ga.tb_garantiatipo_id = gt.tb_garantiatipo_id and gt.tb_garantiatipo_id IN(1,2,3))
                    INNER JOIN tb_almacen al on ga.tb_almacen_id = al.tb_almacen_id
                    
                    where ga.tb_garantia_xac =1 and cre.tb_credito_xac =1 and cre.tb_credito_est NOT IN(4,6) and ga.tb_garantia_est =0";
                    //LEFT JOIN tb_tareainventario ti on ga.tb_garantia_id = ti.tb_garantia_id
            if ($est > 0){
                $sql .= ' and ga.tb_garantia_almest = :tb_garantia_almest ';
            }
            if ($alm_id > 0){
                if ($est > 0){
                    if ($est == 1 || $est == 3){
                        $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
                    }
                    elseif ($est == 2){
                        $sql .= ' and ti.tb_almacen_id2 = :tb_almacen_id';
                    }
                }
                else{
                    $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
                }
                /*else{
                    $sql .= ' and ((ga.tb_almacen_id = :tb_almacen_id and ga.tb_garantia_almest in (0,1)) or (ga.tb_garantia_almest=2 and ti.tb_almacen_id2= :tb_almacen_id ))';
                }*/
            }

            if ($cli_id > 0){
                $sql .= ' and cli.tb_cliente_id =:tb_cliente_id';
            }
            if ($list_ga != '' || $list_ga != null){
                $sql .= ' and ga.tb_garantia_id NOT IN'.$list_ga;
            }

            if ($codigo != '' || $codigo != null){

                $codigo_barras = strrpos($codigo, "'"); // si tiene "-" es un codigo de barras completo, sino es una busqueda manual
 
                if ($codigo_barras === false) { // NO es codigo de barras
                    
                    $filtro = $codigo;
                    $sql .= ' AND ga.tb_credito_id = :filtro';

                    
                } else { // SI es codigo de barras
                    
                    $code = array();
                    $code = explode("'", $codigo);
                    //$gar_id = intval($code[0]);
                    $prefijo = intval($code[0]);
                    $cre_id = intval($code[1]);

                    //$filtro = $gar_id;
                    $sql .= ' AND ga.tb_credito_id = '.$cre_id;

                }

            }
            
            $sql .= ' GROUP BY ga.tb_garantia_id order by ga.tb_garantia_id DESC;';
            
            $sentencia = $this->dblink->prepare($sql);
                if ($est > 0){
                    if($est == 3){ //en codigo el 3 corresponde a 0 en BD
                        $est = 0;
                    }
                    $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
                }
                if (intval($alm_id) > 0)
                    $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
                if (intval($cli_id) > 0)
                    $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
                if ($codigo!='' || $codigo!=null)
                    $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
                 
            $sentencia->execute();
        
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrar_todos_garantia($est, $alm_id, $cli_id, $codigo) {
        try {
            $sql = "SELECT ga.*, cli.tb_cliente_nom, al.tb_almacen_nom FROM tb_garantia ga 
                    INNER JOIN tb_creditomenor cre on ga.tb_credito_id = cre.tb_credito_id
                    INNER JOIN tb_cliente cli on cre.tb_cliente_id = cli.tb_cliente_id
                    INNER JOIN tb_garantiatipo gt on (ga.tb_garantiatipo_id = gt.tb_garantiatipo_id and gt.tb_garantiatipo_id IN(1,2,3))
                    INNER JOIN tb_almacen al on ga.tb_almacen_id = al.tb_almacen_id
                    LEFT JOIN tb_tareainventario ti on ga.tb_garantia_id = ti.tb_garantia_id
                    where ga.tb_garantia_xac =1 and cre.tb_credito_xac =1 and cre.tb_credito_est NOT IN(4,6) and tb_garantia_est =0";
            if ($est > 0){
                $sql .= ' and ga.tb_garantia_almest = :tb_garantia_almest ';
            }
            if ($alm_id > 0){
                if ($est == 1 ||$est == 3){
                    $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
                }
                elseif ($est == 2){
                    $sql .= ' and ti.tb_almacen_id2 = :tb_almacen_id';
                }
                else{
                    $sql .= ' and ((ga.tb_almacen_id = :tb_almacen_id and ga.tb_garantia_almest in (0,1)) or (ga.tb_garantia_almest=2 and ti.tb_almacen_id2= :tb_almacen_id ))';
                }
            }

            if ($codigo != '' || $codigo != null){

                $codigo_barras = strrpos($codigo, "'"); // si tiene "-" es un codigo de barras completo, sino es una busqueda manual
 
                if ($codigo_barras === false) { // NO es codigo de barras
                    
                    //$filtro = "%".$codigo."%";
                    $filtro = $codigo;
                    //$sql .= ' AND ga.tb_garantia_id = :filtro'; // por id garantia
                    $sql .= ' AND ga.tb_credito_id = :filtro'; // por id credito

                    
                } else { // SI es codigo de barras
                    
                    $code = array();
                    $code = explode("'", $codigo);
                    //$gar_id = intval($code[0]);
                    $prefijo = intval($code[0]);
                    $cre_id = intval($code[1]);

                    //$filtro = "%".$gar_id."%";
                    //$filtro = $gar_id;
                    $sql .= ' AND (ga.tb_credito_id = '.$cre_id.')';

                }

            }

            if ($cli_id > 0)
                $sql .= ' and cli.tb_cliente_id =:tb_cliente_id';
            
            $sql .= ' GROUP BY ga.tb_garantia_id order by ga.tb_garantia_id DESC;';
            
            $sentencia = $this->dblink->prepare($sql);
                if ($est > 0){
                    if($est == 3){
                        $est = 0;
                    }
                    $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
                }
                if (intval($alm_id) > 0){
                    $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
                }
                if (intval($cli_id) > 0){
                    $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
                }
                if ($codigo!='' || $codigo!=null){
                    $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
                }
        
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // en uso
    function mostrar_garantia_codigo_barra($est, $alm_id, $codigo) {
        try {
            $sql = "SELECT ga.*, cli.tb_cliente_nom, al.tb_almacen_nom FROM tb_garantia ga 
                    INNER JOIN tb_creditomenor cre on ga.tb_credito_id = cre.tb_credito_id
                    INNER JOIN tb_cliente cli on cre.tb_cliente_id = cli.tb_cliente_id
                    INNER JOIN tb_garantiatipo gt on (ga.tb_garantiatipo_id = gt.tb_garantiatipo_id and gt.tb_garantiatipo_id IN(1,2,3))
                    INNER JOIN tb_almacen al on ga.tb_almacen_id = al.tb_almacen_id
                    LEFT JOIN tb_tareainventario ti on ga.tb_garantia_id = ti.tb_garantia_id
                    where ga.tb_garantia_xac =1 and cre.tb_credito_xac =1 and tb_garantia_est =0";
            if ($est > 0){
                $sql .= ' and ga.tb_garantia_almest = :tb_garantia_almest ';
                // Gerson (18-04-23)
                if ($est != 2){
                    $sql .= ' and cre.tb_credito_est NOT IN(4,6) ';
                }
                //
            }else{
                $sql .= ' and cre.tb_credito_est NOT IN(4,6) ';
            }
            if ($alm_id > 0){
                if ($est == 1 ||$est == 3){
                    $sql .= ' and (ga.tb_almacen_id = :tb_almacen_id )';
                }
                elseif ($est == 2){
                    $sql .= ' and ti.tb_almacen_id2 = :tb_almacen_id';
                }
                else{
                    $sql .= ' and ((ga.tb_almacen_id = :tb_almacen_id and ga.tb_garantia_almest in (0,1)) or (ga.tb_garantia_almest=2 and ti.tb_almacen_id2= :tb_almacen_id ))';
                }
            }

            if ($codigo != '' || $codigo != null){

                $codigo_barras = strrpos($codigo, "'"); // si tiene "-" es un codigo de barras completo, sino es una busqueda manual
 
                if ($codigo_barras === false) { // NO es codigo de barras
                    
                    //$filtro = "%".$codigo."%";
                    $filtro = $codigo;
                    //$sql .= ' AND ga.tb_garantia_id = :filtro'; // por id garantia
                    $sql .= ' AND ga.tb_credito_id = :filtro'; // por id credito

                    
                } else { // SI es codigo de barras
                    
                    $code = array();
                    $code = explode("'", $codigo);
                    //$gar_id = intval($code[0]);
                    $prefijo = $code[0];
                    $cre_id = intval($code[1]);

                    //$filtro = $gar_id;
                    //$sql .= ' AND (ga.tb_garantia_id = :filtro AND ga.tb_credito_id = '.$cre_id.')';
                    $sql .= ' AND (ga.tb_credito_id = '.$cre_id.')';

                }

            }

            $sql .= ' GROUP BY ga.tb_garantia_id order by ga.tb_garantia_id DESC;';
            
            $sentencia = $this->dblink->prepare($sql);
                if ($est > 0){
                    if($est == 3){
                        $est = 0;
                    }
                    $sentencia->bindParam(":tb_garantia_almest", $est, PDO::PARAM_INT);
                }
                if (intval($alm_id) > 0){
                    $sentencia->bindParam(":tb_almacen_id", $alm_id, PDO::PARAM_INT);
                }
                if ($codigo!='' || $codigo!=null){
                    $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
                }
        
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado[0]['tb_garantia_id'];
                $retorno["data1"] = 'CM-' .str_pad($resultado[0]['tb_credito_id'], 4, '0', STR_PAD_LEFT);
                $retorno["data2"] = $resultado[0]['tb_garantia_pro'];
                $retorno["data3"] = $resultado[0]['tb_cliente_nom'];
                $retorno["data4"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
                $retorno["data1"] = "";
                $retorno["data2"] = "";
                $retorno["data3"] = "";
                $retorno["data4"] = 0;
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function existe_garantia($garantia_id, $inventario_id) {
        try {
            $sql = "SELECT * FROM tb_inventario_detalle WHERE tb_garantia_id = :garantia_id AND tb_inventario_id = :inventario_id AND tb_inventario_detalle_xac = 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
            $sentencia->bindParam(":inventario_id", $inventario_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "Existe";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No existe";
                $retorno["data"] = '';
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}
?>
