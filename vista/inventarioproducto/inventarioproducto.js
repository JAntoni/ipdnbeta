/* GERSON 29-12-22 */

var ALMACEN_ID = 0;
var garantia_esta = 0;

$(document).ready(function () {

  inventarioproducto_tabla(ALMACEN_ID);

});

var btn_filtro = document.getElementById('filtro_inventario');
btn_filtro.addEventListener("click", function() {
  inventarioproducto_form('I',0,$("#ultimo_inventario").val());
});

function compruebaTecla(evt) {
    var tecla = evt.which || evt.keyCode;
    if (tecla == 27) {
        swal.close();
    }
}

$('#cmb_almacen_id, #txt_fecha_ini, #txt_fecha_fin, #cmb_usuario_id').change(function (event) {
  inventarioproducto_tabla();
});

function inventarioproducto_tabla(){
  $.ajax({
      type: "POST",
      url: VISTA_URL + "inventarioproducto/inventarioproducto_tabla.php",
      async: true,
      dataType: "html",
      data: ({
          alm_id: $('#cmb_almacen_id').val(),
          fecini: $('#txt_fecha_ini').val(),
          fecfin: $('#txt_fecha_fin').val(),
          use_id: $('#cmb_usuario_id').val()
      }),
      beforeSend: function () {
      },
      success: function (html) {
          $('#div_inventarioproducto_tabla').html(html);
      },
      complete: function (html) {
          estilos_datatable();
      }
  });
}

function estilos_datatable(){
  /* datatable_global = $('#tabla_inventarioproducto').DataTable({
    "pageLength": 10,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [1, 2, 3, 5, 6, 7, 8], orderable: false }
    ]
  });

  datatable_texto_filtrar(); */
}

function datatable_texto_filtrar(){
$('.dataTables_filter input').attr('id', 'txt_datatable_fil');

$('#txt_datatable_fil').keyup(function(event) {
  $('#hdd_datatable_fil').val(this.value);
});

var text_fil = $('#hdd_datatable_fil').val();
if(text_fil){
  $('#txt_datatable_fil').val(text_fil);
  datatable_global.search(text_fil).draw();
}
}

$('#datetimepicker1, #datetimepicker2').datepicker({
  language: 'es',
  autoclose: true,
  format: "dd-mm-yyyy",
  //startDate: "-0d",
  //endDate : new Date()
});

$("#datetimepicker1").on("change", function (e) {
  var startVal = $('#txt_fecha_ini').val();
  $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  inventarioproducto_tabla();
});
$("#datetimepicker2").on("change", function (e) {
  var endVal = $('#txt_fecha_fin').val();
  $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  inventarioproducto_tabla();
});

function inventarioproducto_form(usuario_act, inventario_id, flag){ 
  if(parseInt(flag)==1){
    Swal.fire(
      '¡Aviso!',
      "Se encuentra un inventario en proceso.",
      'warning'
    )
  }else{

    $.ajax({
      type: "POST",
      url: VISTA_URL+"inventarioproducto/inventarioproducto_form.php",
      async: true,
      dataType: "html",
      data: ({
        action: usuario_act, // PUEDE SER: L, I, M , E
        inventario_id: inventario_id,
        vista: 'inventario',
      }),
      beforeSend: function() {
        $('#h3_modal_title').text('Cargando Formulario');
        $('#modal_mensaje').modal('show');
      },
      success: function(data){
        $('#modal_mensaje').modal('hide');
        if(data != 'sin_datos'){
          $('#div_modal_inventarioproducto_form').html(data);
          $('#modal_registro_inventario').modal('show');
  
          //desabilitar elementos del form si es L (LEER)
          if(usuario_act == 'L' || usuario_act == 'E')
            form_desabilitar_elementos('form_inventarioproducto'); //funcion encontrada en public/js/generales.js
  
            //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
            modal_width_auto('modal_registro_inventario', 80);
            modal_height_auto('modal_registro_inventario'); //funcion encontrada en public/js/generales.js
            modal_hidden_bs_modal('modal_registro_inventario', 'limpiar'); //funcion encontrada en public/js/generales.js
        }
        else{
          //llamar al formulario de solicitar permiso
          var modulo = 'inventarioproducto';
          var div = 'div_modal_inventarioproducto_form';
          permiso_solicitud(usuario_act, inventario_id, modulo, div); //funcion ubicada en public/js/permiso.js
        }
      },
      complete: function(data){
        
      },
      error: function(data){
        $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
        $('#overlay_modal_mensaje').removeClass('overlay').empty();
        $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
        console.log(data.responseText);
      }
    });

  }
  
}

// se usa
function inventarioprodcuto_ver(usuario_act, inventario_id, estado) {
  $("#hdd_inventario_id").val(inventario_id);
  $("#inventario_id").val(inventario_id);
  $.ajax({
    type: "POST",
    url: VISTA_URL+"inventarioproducto/inventarioproducto_view.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      inventario_id: inventario_id,
      estado: estado, // 0 = finalizado, 1 = en curso 
      vista: 'inventario',
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_inventarioproducto_form').html(data);
        $('#modal_view_inventario').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('view_inventarioproducto'); //funcion encontrada en public/js/generales.js

          //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
          modal_width_auto('modal_view_inventario', 80);
          modal_height_auto('modal_view_inventario'); //funcion encontrada en public/js/generales.js
          modal_hidden_bs_modal('modal_view_inventario', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
        //llamar al formulario de solicitar permiso
        var modulo = 'inventarioproducto';
        var div = 'div_modal_inventarioproducto_form';
        permiso_solicitud(usuario_act, inventario_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

// se usa
function finalizar_inventario(inventario_id){

  Swal.fire({
    title: '¿SEGURO QUE DESEA FINALIZAR INVENTARIO?',
    icon: 'warning',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> Cancelar',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#DD3333',
    }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "inventarioproducto/inventario_controller.php",
        async: false,
        dataType: "json",
        data: ({
          action: 'fin_inventario',
          inventario_id: inventario_id
        }),
        beforeSend: function () {
        },
        success: function (html) {
          $('#div_inventarioproducto_tabla').html(html);
        },
        complete: function (html) {
          Swal.fire(
            '¡Hecho!',
            "Inventario finalizado.",
            'success'
          )
            estilos_datatable();
            $("#ultimo_inventario").val(0);
            inventarioproducto_tabla(); 
        }
      });
    }
  });
  
}

// se usa
function eliminar_inventario(inventario_id){

  Swal.fire({
    title: '¿SEGURO QUE DESEA ELIMINAR INVENTARIO?',
    icon: 'warning',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> Cancelar',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#DD3333',
    }).then((result) => {

    if (result.isConfirmed) {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "inventarioproducto/inventario_controller.php",
        async: false,
        dataType: "json",
        data: ({
          action: 'eliminar_inventario',
          inventario_id: inventario_id
        }),
        beforeSend: function () {
        },
        success: function (html) {
          $('#div_inventarioproducto_tabla').html(html);
        },
        complete: function (html) {
          Swal.fire(
            '¡Hecho!',
            "Inventario eliminado.",
            'success'
          )
            estilos_datatable();
            inventarioproducto_tabla(); 
            $("#ultimo_inventario").val(0);
        }
      });
    }
  });

}

/* GERSON (06-02-23) */
function inventarioprodcuto_reportePDF(inventario_id){
	window.open("vista/inventarioproducto/pdf_inventarioproducto_tabla.php?inventario_id="+inventario_id,"_blank");
}


/* FIN GERSON */

