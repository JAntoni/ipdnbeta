<?php


require_once '../../core/usuario_sesion.php';
require_once ('../inventarioproducto/Inventario.class.php');
$oInventario = new Inventario();
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');
require_once ('../tareainventario/Tareainventario.class.php');
$oTareainventario = new TareaInventario();
require_once('../almacen/Almacen.class.php');
$oAlmacen = new Almacen();
$r = $oAlmacen->mostrarTodos1()['data'];

$alm_id = intval($_POST['alm_id']);
$fecini = fecha_mysql($_POST['fecini']);
$fecfin = fecha_mysql($_POST['fecfin']);
$use_id = $_POST['use_id'];

?>
<input type="hidden" id="usuario_mos" value="<?php echo $_SESSION['usuario_mos']; ?>">
<input type="hidden" id="almacen_id" value="<?php echo $alm_id; ?>">
<input type="hidden" id="arreglo_almacenes" value='<?php echo json_encode($r); ?>'>

<?php 
                                  
$dts = $oInventario->mostrar_todos_filtro($alm_id, $fecini, $fecfin, $use_id);
$cont = 1;
if ($dts['estado'] == 1): ?>
    <table id="tabla_inventarioproducto" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">N°</th>
                <th id="tabla_cabecera_fila">FECHA INICIO</th>
                <th id="tabla_cabecera_fila">FECHA FIN</th>
                <th id="tabla_cabecera_fila">ALMACEN</th>
                <th id="tabla_cabecera_fila">CANT. GARANTÍAS</th>
                <th id="tabla_cabecera_fila">RESPONSABLE</th>
                <th id="tabla_cabecera_fila">DESCRIPCIÓN</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th  id="tabla_cabecera_fila" width="12%">OPCIONES</th>
            </tr>
        </thead>  
        <tbody> <?php
            foreach ($dts['data']as $key => $dt) {
                    ?>
                    <tr id="tabla_cabecera_fila">
                        <td id="tabla_fila"><?php echo $cont ?></td>
                        <!-- <td id="tabla_fila"><?php //echo 'CM-' . str_pad($dt['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td> -->
                        <td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_inventario_fecini']) ?></td>
                        <td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_inventario_fecfin']) ?></td>
                        <td id="tabla_fila"><?php echo $dt['almacen'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_inventario_total_prod'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['usuario'] ?></td>
                        <td id="tabla_fila"><?php echo $dt['tb_inventario_des'] ?></td>
                        <?php
                            $estado = 'COMPLETADO';
                            $color = '#00FF00';
                            if($dt['tb_inventario_fecfin'] == NULL || $dt['tb_inventario_fecfin'] == ''){
                                $estado = 'INCOMPLETO';
                                $color = '#FF0000';
                            }
                        ?>
                        <td id="tabla_fila" style="background: <?php echo $color; ?>; color: #ffffff;">
                            <strong><?php echo $estado; ?></strong>
                            <br>
                            <?php if($dt['tb_inventario_fecfin'] == NULL || $dt['tb_inventario_fecfin'] == ''){ ?>
                                <a class="btn btn-info btn-xs" onclick='finalizar_inventario(<?php echo $dt['tb_inventario_id']; ?>)'><i class="fa fa-fw fa-hourglass-end"></i> Finalizar</a>
                            <?php }  ?>
                        </td>
                        <td id="tabla_fila">
                            <?php if($dt['tb_inventario_fecfin'] == NULL || $dt['tb_inventario_fecfin'] == ''){ ?>
                                <a class="btn btn-primary btn-xs" onclick="inventarioprodcuto_ver('M',<?php echo $dt['tb_inventario_id']; ?>,1)"><i class="fa fa-fw fa-eye"></i> Ver</a>
                                <?php if($_SESSION['usuarioperfil_id'] == 1 ){ ?>
                                    <a class="btn btn-danger btn-xs" onclick='eliminar_inventario(<?php echo $dt['tb_inventario_id']; ?>)'><i class="fa fa-fw fa-trash"></i> Eliminar</a>
                                <?php }  ?>
                            <?php }else{  ?>
                                <a class="btn btn-primary btn-xs" onclick="inventarioprodcuto_ver('M',<?php echo $dt['tb_inventario_id']; ?>,0)"><i class="fa fa-fw fa-eye"></i> Ver</a>
                            <?php }  ?>
                            <div class="btn-group">
                            <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-fw fa-print"></i> Exportar</button>
                                <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <!-- <li><a href="#" onclick='inventarioprodcuto_reporteExcel(<?php echo $dt['tb_inventario_id']; ?>)'><i class="fa fa-fw fa-file-excel-o"></i> Excel</a></li> -->
                                    <li><a href="#" onclick='inventarioprodcuto_reportePDF(<?php echo $dt['tb_inventario_id']; ?>)'><i class="fa fa-fw fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </div>
                            <!-- <a class="btn btn-success btn-xs" onclick='inventarioprodcuto_editar(<?php echo $dt['tb_inventario_id']; ?>)'><i class="fa fa-fw fa-file-excel-o"></i>Excel</a>
                            <a class="btn btn-danger btn-xs" onclick='inventarioprodcuto_editar(<?php echo $dt['tb_inventario_id']; ?>)'><i class="fa fa-fw fa-file-pdf-o"></i>PDF</a> -->
                        </td>
                    </tr> <?php
                    $res = null;
                    $cont++;
            } //FIN DE WHILE
            ?>
        </tbody>
    </table>
<?php endif; ?>

