
<form id="inventario_filtro" name="inventario_filtro">

<?php
session_name("ipdnsac");
session_start();

require_once('Inventario.class.php');
$oInventario = new Inventario();
$flag = 0;
$ultimoReg = $oInventario->mostrarUltimoID();
$inventario = $oInventario->mostrarUno($ultimoReg['data']['tb_inventario_id']);
if(!is_null($inventario)){
    if($inventario['data']['tb_inventario_xac']==1){
        if($inventario['data']['tb_inventario_fecfin']=='' || $inventario['data']['tb_inventario_fecfin']==null){
            $flag = 1; // si hay un inventario realizandose
        }
    }
}
?>
    <div class="row">

        <div class="col-md-2">
            <input type="hidden" id="ultimo_inventario" name="ultimo_inventario" value="<?php echo $flag; ?>">
            <input type="hidden" id="hdd_almacen_id">
            <label for="cmb_almacen_id">Almacén:</label>
            <select class="form-control input-sm mayus" id="cmb_almacen_id" name="cmb_almacen_id">
                <?php include VISTA_URL.'almacen/almacen_select.php'; ?>
            </select>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="">Fecha </label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fecha_ini" id="txt_fecha_ini" value="<?php echo date('d-m-Y'); ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fecha_fin" id="txt_fecha_fin" value="<?php echo date('t-m-Y'); ?>"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="col-md-3">
            <div class="form-group">
                <input type="hidden" id="hdd_usuario_id">
                <label>Colaborador:</label>
                <select id="cmb_usuario_id" name="cmb_usuario_id" class="selectpicker form-control" data-live-search="true" data-max-options="1">
                    <?php 
                        include VISTA_URL.'usuario/usuario_select.php';
                    ?>
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <label for="btn_nuevo"></label>
            <div class="form-group">
                <!-- <button type="button" class="btn btn-primary btn-sm" onclick="inventarioproducto_form('I',0,'<?php echo $flag; ?>')"><i class="fa fa-plus"></i> Nuevo Inventario</button> -->
                <button type="button" class="btn btn-primary btn-sm" id="filtro_inventario" name="filtro_inventario"><i class="fa fa-plus"></i> Nuevo Inventario</button>
            </div>
        </div>

    </div>
    
</form>