<?php
	session_name("ipdnsac");
  session_start();
	require_once('../inventarioproducto/Inventario.class.php');
  $oInventario = new Inventario();
  require_once('../garantia/Garantia.class.php');
  $oGarantia = new Garantia();
  require_once('../funciones/fechas.php');

  $inventario_detalle_id = intval($_POST['inventario_detalle_id']);
  $inventario_id = $_POST['inventario_id'];
  $garantia_nom = '';
  $garantia_com = '';
  // obtener detalle
  $detalle = $oInventario->mostrarUnDetalle($inventario_detalle_id);
  $detalle_com = '';
  if($detalle['data']['tb_inventario_detalle_des']!='' || $detalle['data']['tb_inventario_detalle_des']!=null){
    $detalle_com = $detalle['data']['tb_inventario_detalle_des'];
  }
  // obtener garantia
  $garantia = $oGarantia->mostrarUno($detalle['data']['tb_garantia_id']);
  $garantia_nom = $garantia['data']['tb_garantia_pro'];
  $lista = '';

?>
<div class="modal fade" tabindex="-10" role="dialog" id="modal_comentario_garantia" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <!-- <h4 class="modal-title">COMENTARIO: <?php //echo strtolower($garantia_nom); ?> </h4> -->
        <h4 class="modal-title">COMENTARIO: <?php echo $garantia_nom; ?> </h4>
      </div>
      <form id="form_comentario_inventario" method="post">

      <div class="modal-body">
          <input type="hidden" id="comentario_inventario_id" value="<?php echo $inventario_id;?>">
          <input type="hidden" id="comentario_inventario_detalle_id" value="<?php echo $inventario_detalle_id;?>">
          
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control input-sm" name="txt_inventario_com" id="txt_inventario_com" rows="4" style="resize:none;"><?php echo $detalle_com; ?></textarea>
              </div>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" id="btn_guardar_comentario_inventario" onclick="guardar_comentario()">Guardar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!--<script type="text/javascript" src="<?php //echo 'vista/creditolinea/creditolinea_timeline.js';?>"></script>-->