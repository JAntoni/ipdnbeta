//var ALMACEN_ID = 0;

$(document).ready(function(){

	var action = $('#action').val();
	if(action == 'insertar'){
		$('#detalle_inventario').addClass('hidden');
	}else{
		$('#detalle_inventario').removeClass('hidden');
		inventarioproducto_garantias_tabla($('#hdd_inventario_id').val(),''); // lista de garantias
		inventarioproducto_detalles_tabla($('#hdd_inventario_id').val()); // detalle del inventario
	}

	//inventarioproducto_garantias_tabla(ALMACEN_ID);

	// busqueda codigo de barras
	let codigo = document.getElementById("txt_codigo_barra");
	codigo.addEventListener("keyup", (event,caracteres) => {
		var caracteres = 0;
		var cod = $("#txt_codigo_barra").val();
		//caracteres = cod.length;
		barcode = cod.includes("CM'"); // true o false

		if (event.keyCode === 13) {

			$.ajax({
				type: "POST",
				url: VISTA_URL+"inventarioproducto/inventario_controller.php",
				data: {
					'action': 'obtener_una_garantia',
					'codigo_barra': event.target.value,
					'almacen_id': $('#cmb_inventario_almacen_id').val()
				}
			})
			.done(function( text ) {			
				if(text!='[]'){
					var json = JSON.parse(text);
					$("#gar_select").val(json.garantia_id); // seleccionar la garantia encontrada
					$("#gar_credito_select").val(json.credito_id); // credito seleccionado
					$("#gar_producto_select").val(json.producto); // producto seleccionado
					$("#gar_cliente_select").val(json.cliente); // cliente seleccionado
					
					/* GERSON (18-04-23) */
					var numGarantia = json.cantidad;

					let txt = event.target.value;
					if($("#inventario_id").val()>0){
						var id = $("#inventario_id").val();
					}else{
						if($("#hdd_inventario_id").val()>0){
							var id = $("#hdd_inventario_id").val();
						}else{
							var id = $("#inventario_id").val();
						}
					}
					
					detectar_existe_garantia_en_inventario(barcode,numGarantia,id,json.garantia_id,json.credito_id,json.producto,json.cliente,txt)
					/*  */
					
					/* inventarioproducto_garantias_tabla(id,txt)

					if(barcode && numGarantia==1){ // es codigo de barras
						
						lectura_codigo_barra(json.garantia_id,json.credito_id,json.producto,json.cliente);
						
					} */

					/* if(barcode && numGarantia==1){ // es codigo de barras
						let txt = event.target.value;
						if($("#inventario_id").val()>0){
							var id = $("#inventario_id").val();
						}else{
							if($("#hdd_inventario_id").val()>0){
								var id = $("#hdd_inventario_id").val();
							}else{
								var id = $("#inventario_id").val();
							}
						}
						inventarioproducto_garantias_tabla(id,txt)
						lectura_codigo_barra(json.garantia_id,json.credito_id,json.producto,json.cliente);
						
					}else{
						let txt = event.target.value;
						if($("#inventario_id").val()>0){
							var id = $("#inventario_id").val();
						}else{
							if($("#hdd_inventario_id").val()>0){
								var id = $("#hdd_inventario_id").val();
							}else{
								var id = $("#inventario_id").val();
							}
						}
						inventarioproducto_garantias_tabla(id,txt)
					} */
				}			
			});

        }else{
			/* let txt = event.target.value;
			let id = $("#inventario_id").val();
			inventarioproducto_garantias_tabla(id,txt) */
		}
	});

	/* GERSON (19-04-23) */
	// busqueda CREDITO EN INVENTARIO
	let credito = document.getElementById("txt_garantia_inventario");
	credito.addEventListener("keyup", (event,caracteres) => {
		var caracteres = 0;
		let cod = event.target.value;
		if($("#inventario_id").val()>0){
			var id = $("#inventario_id").val();
		}else{
			if($("#hdd_inventario_id").val()>0){
				var id = $("#hdd_inventario_id").val();
			}else{
				var id = $("#inventario_id").val();
			}
		}

		if (event.keyCode === 13) {

			$.ajax({
				type: "POST",
				url: VISTA_URL + "inventarioproducto/inventarioproducto_detalle_tabla.php",
				async: true,
				dataType: "html",
				data: ({
					alm_id: $('#cmb_inventario_almacen_id').val(), //esta vista es solo para los garantias que se encuentran en tienda, id de almacen tienda es 1
					inventario_id: id,
					credito_id: cod,
					estado: $("#hdd_estado").val()
				}),
				beforeSend: function () {
				},
				success: function (html) {
					$('#div_inventario_detalle_tabla').html(html);
				},
				complete: function (html) {
					estilos_datatable();
				}
			});

		}
	});
	/*  */
	
});

/* GERSON (19-04-23) */
function detectar_existe_garantia_en_inventario(barcode,numGarantia,inventario_id, garantia_id,credito_id,producto,cliente,txt) {
	
	inventarioproducto_garantias_tabla(inventario_id,txt)

	if(barcode && numGarantia==1){ // es codigo de barras
		
		$.ajax({
			type: "POST",
			url: VISTA_URL+"inventarioproducto/inventario_controller.php",
			data: {
				'action': 'detectar_existe_garantia_en_inventario',
				'inventario_id': inventario_id,
				'garantia_id': garantia_id
			}
		})
		.done(function( text ) {			
			if(text!='[]'){
				var json = JSON.parse(text);
				if(json.estado==1){
					//alert('YA EXISTE');
					Swal.fire(
						'Error al registrar Garantía!',
						"La garantía seleccionada ya se encuentra en el inventario",
						'error'
					  )
					$("#txt_codigo_barra").val('');
					inventarioproducto_garantias_tabla($('#hdd_inventario_id').val(),''); // lista de garantias
				}else{
					lectura_codigo_barra(garantia_id,credito_id,producto,cliente);
					
				}
			}			
		});
		
	}

	
}
/*  */

function carousel(modulo_nom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      //console.log(data.responseText);
    }
  });
}


function action3() {
	var result = "";
    var action_display = $('#confirm_modal_add_garantia')
    action_display.find('.modal-body').html(result)
    action_display.modal('show')
	$("#confirm-btn-prueba_gar-garantia").focus();
}

window._confirm = function($message = '', $func = '', $param = []) {
    if ($func != '' && typeof window[$func] == 'function') {
		
        var modal_el = $('#confirm_modal_add_garantia');
        modal_el.find('.modal-body').html($message);
        modal_el.modal('show');
		
		$("#txt_codigo_barra").keyup(function(event) {
			if (event.which === 13) {
				$("#confirm-btn-add-garantia").click();
			}
		});
	 
		modal_el.find("#confirm-btn-add-garantia").click(function(e) {
			e.preventDefault();
			addGarantia();
            modal_el.modal('hide');
		});

		$("#txt_codigo_barra").keyup(function(event) {
			if (event.which === 27) {
				$("#cancel-btn-add-garantia").click();
			}
		});

		modal_el.find("#cancel-btn-add-garantia").click(function(e) {
			e.preventDefault();
			$("#txt_codigo_barra").val('');
			$("#gar_select").val('');
            modal_el.modal('hide');
			inventarioproducto_garantias_tabla($('#hdd_inventario_id').val(),''); // lista de garantias

		});

    } else {
        alert("Function does not exists.")
    }
}

function lectura_codigo_barra(garantia_id,credito,producto,cliente){
	
	var msg = "SEGURO QUE DESEA AÑADIR AL INVENTARIO?";
	var txt = "<ul align='left' style='list-style:none;'><li><strong>N° GARANTÍA: </strong>"+garantia_id+"</li><li><strong>CRÉDITO: </strong>"+credito+"</li><li><strong>PRODUCTO: </strong>"+producto+"</li><li><strong>CLIENTE: </strong>"+cliente+"</li></ul>";

	Swal.fire({
		title: msg,
		html: txt,
		icon: 'info',
		showCloseButton: true,
		showCancelButton: false,
		focusConfirm: false,
		confirmButtonText:'<i class="fa fa-check"></i> Confirmar',
		showDenyButton: true,
		denyButtonText: '<i class="fa fa-band"></i> Cancelar',
		confirmButtonColor: '#3b5998', 
		denyButtonColor: '#DD3333',
		}).then((result) => {
	
		if (result.isConfirmed) {
			$("#gar_select").val(garantia_id);
			addGarantia(); // añadir garantia al inventario

		}else{
			$("#txt_codigo_barra").val('');
			inventarioproducto_garantias_tabla($('#hdd_inventario_id').val(),''); // lista de garantias
		}
	  });
	
}

function nuevoInventario(){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"inventarioproducto/inventario_controller.php",
		async: true,
		dataType: "json",
		data: $("#form_inventarioproducto").serialize(),
		beforeSend: function() {
			$('#inventario_mensaje').show(400);
			$('#btn_guardar_inventario').prop('disabled', true);
		},
		success: function(data){
			if(parseInt(data.estado) > 0){
				$('#inventario_mensaje').removeClass('callout-info').addClass('callout-success')
				$('#inventario_mensaje').html(data.mensaje);

				var vista = $('#inventario_vista').val();
				var action = $('#action').val();

				setTimeout(function(){
					if(vista == 'inventario')
						inventarioproducto_tabla();
						if(action == 'insertar'){
							$('#detalle_inventario').removeClass('hidden'); // aparecer detalle
							$('#inventario_mensaje').hide(); // quitar mensaje de guardado inventario
							//$('#btn_guardar_inventario').prop('disabled', true); // dejar bloqueado el boton de guardar
							$('#inventario_id').val(data.inventario_id); // Obtener el nuevo inventario_id 
							$('#hdd_inventario_id').val(data.inventario_id); // Obtener el nuevo inventario_id 
							inventarioproducto_garantias_tabla(data.inventario_id);

						}else{
							$('#modal_registro_inventario').modal('hide');
						}
					}, 1000
				);
			}else{
				$('#inventario_mensaje').removeClass('callout-info').addClass('callout-warning')
				$('#inventario_mensaje').html('Alerta: ' + data.mensaje);
				$('#btn_guardar_inventario').prop('disabled', false);
			}
		},complete: function(data){
			var action = $('#action').val();
			$('#ultimo_inventario').val(1);
			if(action == 'insertar'){
				$('#txt_inventario_des').prop('disabled', true); // dejar bloqueado campo descripcion
			}else{

			}

		},error: function(data){
			$('#inventario_mensaje').removeClass('callout-info').addClass('callout-danger')
			$('#inventario_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
		}
	});
}
  

function actualizarInventario(){
	
	$.ajax({
		type: "POST",
		url: VISTA_URL+"inventarioproducto/inventario_controller.php",
		async: true,
		dataType: "json",
		data: $("#view_inventarioproducto").serialize(),
		beforeSend: function() {
			$('#inventario_mensaje_view').show(400);
			$('#btn_view_inventario').prop('disabled', true);
		},
		success: function(data){

			if(parseInt(data.estado) > 0){
				$('#inventario_mensaje_view').removeClass('callout-info').addClass('callout-success')
				$('#inventario_mensaje_view').html(data.mensaje);

				var vista = $('#inventario_vista').val();
				var action = $('#action').val();

				setTimeout(function(){
					if(vista == 'inventario')
						inventarioproducto_tabla();
						if(action == 'insertar'){
							$('#detalle_inventario').removeClass('hidden'); // aparecer detalle
							$('#inventario_mensaje_view').hide(); // quitar mensaje de guardado inventario
							//$('#btn_view_inventario').prop('disabled', true); // dejar bloqueado el boton de guardar
							$('#inventario_id').val(data.inventario_id); // Obtener el nuevo inventario_id 
							$('#hdd_inventario_id').val(data.inventario_id); // Obtener el nuevo inventario_id 
							inventarioproducto_garantias_tabla(data.inventario_id);

						}else{
							$('#modal_view_inventario').modal('hide');
							$('#inventario_id').val(data.inventario_id); // Obtener el nuevo inventario_id 
							$('#hdd_inventario_id').val(data.inventario_id); // Obtener el nuevo inventario_id 
							inventarioproducto_garantias_tabla(data.inventario_id);
						}
					}, 1000
				);
			}else{
				$('#inventario_mensaje_view').removeClass('callout-info').addClass('callout-warning')
				$('#inventario_mensaje_view').html('Alerta: ' + data.mensaje);
				$('#btn_view_inventario').prop('disabled', false);
			}
		},complete: function(data){
			var action = $('#action').val();
			if(action == 'insertar'){
				$('#txt_inventario_des').prop('disabled', true); // dejar bloqueado campo descripcion
			}
		},error: function(data){
			$('#inventario_mensaje_view').removeClass('callout-info').addClass('callout-danger')
			$('#inventario_mensaje_view').html('ALERTA DE ERROR: ' + data.responseText);
		}
	});
}
	  	
$('#cmb_inventario_almacen_id').change(function (event) {
	inventarioproducto_garantias_tabla(0,'');
});

// cargado de garantias por sede en form inventarioproducto
function inventarioproducto_garantias_tabla(inventario_id, codigo) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "inventarioproducto/inventarioproducto_garantia_tabla.php",
		async: true,
		dataType: "html",
		data: ({
			alm_id: $('#cmb_inventario_almacen_id').val(), //esta vista es solo para los garantias que se encuentran en tienda, id de almacen tienda es 1
			//inventario_id: $('#inventario_id').val(),
			inventario_id: inventario_id,
			codigo: codigo
		}),
		beforeSend: function () {
		},
		success: function (html) {
			$('#div_garantias_tabla').html(html);
		},
		complete: function (html) {
			estilos_datatable();
		}
	});
}

// cargado de garantias del inventario
function inventarioproducto_detalles_tabla(id) {
	$.ajax({
		type: "POST",
		url: VISTA_URL + "inventarioproducto/inventarioproducto_detalle_tabla.php",
		async: true,
		dataType: "html",
		data: ({
			alm_id: $('#cmb_inventario_almacen_id').val(), //esta vista es solo para los garantias que se encuentran en tienda, id de almacen tienda es 1
			inventario_id: id,
			estado: $("#hdd_estado").val()
		}),
		beforeSend: function () {
		},
		success: function (html) {
			$('#div_inventario_detalle_tabla').html(html);
		},
		complete: function (html) {
			estilos_datatable();
		}
	});
}

// seleccionar una garantía para inventario
function garantiaSelect(val, garantia_id){
	var value = 0;
	var colorOriginal = '';
	if($("#gar_select").val()!=null){
		var idprevio = 0; // ultimo click rehalizado
	}
	idprevio = $("#gar_select").val();

	// sombrear fila
	if($(val).hasClass('row-selected')){
		/* $(val).addClass('other-clic')
		addGarantia(); */
	}else{
		cleanTr()
		$(val).addClass('row-selected')
	}
	//

	// queda la garantia como ultima seleccionada
	if(parseInt(garantia_id) > 0){
		value = garantia_id;
	}
	$("#gar_select").val(value);
	//
}

function cleanTr(){
	$('.row-selected').each(function(index, element){
		$(element).removeClass('row-selected')
		$(element).removeClass('other-clic')
	})
}

// Seleccionar garantía pra inventario
function addGarantia() {
    var valor = 0;
	$("#confirm-btn-add-garantia").focus();

	if($("#gar_select").val()!=''){
		valor = $("#gar_select").val();
		console.log(valor);

		var action = $('#action').val();
		console.log(action);

		if(action == 'insertar'){
			id = parseInt($("#inventario_id").val());
		}else{
			id = parseInt($("#hdd_inventario_id").val());
		}
		guardar_garantia(valor, id)
	}else{
		Swal.fire(
			'¡Aviso!',
			"Debe seleccionar una garantía.",
			'warning'
		  )
	}
}

// en uso
function eliminar_item_inventario(inventario_detalle_id, inventario_id){
	$.ajax({
        type: "POST",
        url: VISTA_URL + "inventarioproducto/inventario_controller.php",
        async: false,
        dataType: "json",
        data: ({
			action: 'eliminar_garantia',
			inventario_detalle_id: inventario_detalle_id,
			inventario_id: inventario_id
		}),
        beforeSend: function () {
            $('#div_inventario_detalle_tabla').text('Consultando garantías...');
            $('#div_inventario_detalle_tabla').show(100);
        },
        success: function (html) {
			inventarioproducto_detalles_tabla(inventario_id);
			notificacion_success('Garantía eliminada satisfactoriamente.', 2000);
        },
        complete: function (data) {
			$('#inventario_id').val(inventario_id); // cargar inventario_id 
			$('#hdd_inventario_id').val(inventario_id); // cargar inventario_id 
			inventarioproducto_tabla();
			inventarioproducto_garantias_tabla(inventario_id,'');
        }
    });
}

// en uso
function guardar_garantia(garantia_id, id){
	$.ajax({
        type: "POST",
        url: VISTA_URL + "inventarioproducto/inventario_controller.php",
        async: false,
        dataType: "json",
        data: ({
			action: 'guardar_garantia',
			garantia_id: garantia_id, 
			inventario_id: id
		}),
        beforeSend: function () {
            $('#div_inventario_detalle_tabla').text('Consultando garantías...');
            $('#div_inventario_detalle_tabla').show(100);

        },
        success: function (data) {
			console.log(data);
			if(data.estado==0){
				inventarioproducto_detalles_tabla(id);
				notificacion_warning(data.mensaje, 2000);
			}else{
				inventarioproducto_detalles_tabla(id);
				notificacion_success('Garantía agregado satisfactoriamente.', 2000);

			}
			
        },
        complete: function (data) {
			inventarioproducto_tabla();
			inventarioproducto_garantias_tabla(id,'');
			$("#txt_codigo_barra").val('');
        }
    });
}

//en uso
function comentario_item_inventario(inventario_detalle_id, inventario_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"inventarioproducto/inventario_comentario.php",
		async: true,
		dataType: "html",
		data: ({
		  inventario_detalle_id: inventario_detalle_id,
		  inventario_id: inventario_id
		}),
		beforeSend: function() {
		  $('#h3_modal_title').text('Cargando Formulario');
		  $('#modal_mensaje').modal('show');
		},
		success: function(data){
		  $('#div_comentario_item_garantia').html(data);
		  $('#modal_comentario_garantia').modal('show');
		  //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
		  modal_width_auto('modal_comentario_garantia', 20);
		  modal_height_auto('modal_comentario_garantia'); //funcion encontrada en public/js/generales.js
		  modal_hidden_bs_modal('modal_comentario_garantia', 'limpiar'); //funcion encontrada en public/js/generales.js
		},
		complete: function(data){
		  $('#modal_mensaje').modal('hide');
		},
		error: function(data){
		  alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
		  console.log(data.responseText);
		}
	  });
}

//en uso
function guardar_comentario(){
	var comentario = $('#txt_inventario_com').val();
	var inventario_detalle_id = $('#comentario_inventario_detalle_id').val();
	var inventario_id = $('#comentario_inventario_id').val();
	$.ajax({
        type: "POST",
        url: VISTA_URL + "inventarioproducto/inventario_controller.php",
        async: false,
        dataType: "json",
        data: ({
			action: 'guardar_comentario',
			comentario: comentario, 
			inventario_detalle_id: inventario_detalle_id, 
			inventario_id: inventario_id
		}),
        beforeSend: function () {
            $('#div_inventario_detalle_tabla').text('Consultando garantías...');
			$('#btn_guardar_comentario_inventario').prop('disabled', true);
            $('#div_inventario_detalle_tabla').show(100);
        },
        success: function (html) {
			inventarioproducto_detalles_tabla(inventario_id);
			notificacion_success('Comentario agregado satisfactoriamente.', 2000);
        },
        complete: function (data) {
			$('#modal_comentario_garantia').modal('hide');
        }
    });

}

