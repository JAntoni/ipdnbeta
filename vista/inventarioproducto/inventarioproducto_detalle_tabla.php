<?php


require_once '../../core/usuario_sesion.php';
require_once ('../inventarioproducto/Inventario.class.php');
$oInventario = new Inventario();
require_once ("../funciones/funciones.php");

$estado = 0; // todos los estados
$alm_id = intval($_POST['alm_id']);
$inventario_id = intval($_POST['inventario_id']);
$estado = $_POST['estado'];
$cli_id = '';
$credito_id='';
if (isset($_POST['credito_id'])) {
	$credito_id=intval($_POST['credito_id']);
}

$fecha_hoy = date('d-m-Y');

?>
<input type="hidden" id="usuario_mos" value="<?php echo $_SESSION['usuario_mos']; ?>">
<input type="hidden" id="almacen_id" value="<?php echo $alm_id; ?>">
<input type="hidden" id="inventario_id" value="<?php echo $inventario_id; ?>">
<input type="hidden" id="credito_id" value="<?php echo $credito_id; ?>">

<?php 
                                  
$dts = $oInventario->mostrar_todos_detalle($inventario_id,$credito_id);

if ($dts['estado'] == 1): ?>
    <input type="hidden" id="det_select" value="">
    <h4><strong>Garantías inventariadas</strong></h4>

    <table id="" class="table">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">N°</th>
                <th id="tabla_cabecera_fila">ID GARANTÍA</th>
                <th id="tabla_cabecera_fila">CRÉDITO</th>
                <th id="tabla_cabecera_fila">GARANTÍA</th>
                <th id="tabla_cabecera_fila">CLIENTE</th>
                <th id="tabla_cabecera_fila" width="12%">OPERACIONES</th>
            </tr>
        </thead>  
        <tbody> <?php
        $cont = 1;
            foreach ($dts['data']as $key => $dt) {
                $borde = '#E08E0B';
                $grosor_borde = 'border-width: 1.5px !important;';
                if($dt['tb_inventario_detalle_des']!=null || $dt['tb_inventario_detalle_des']!=''){
                    $borde = '#00ACD7';
                    $grosor_borde = 'border-width: 2px !important;';
                }
                    ?>
                    <tr class="" name="tabla_cabecera_fila_det" id="garantia<?php echo $dt['tb_garantia_id']; ?>" onclick="">
                        <td id="tabla_fila"><?php echo $cont ?></td>
                        <td align="left" id="tabla_fila"><?php echo $dt['tb_garantia_id']; ?></td>
                        <td id="tabla_fila"><?php echo 'CM-' . str_pad($dt['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td>
                        <td align="left" id="tabla_fila"><?php echo $dt['producto'] ?></td>
                        <td align="left" id="tabla_fila"><?php echo $dt['tb_cliente_nom'] ?></td>
                        <td id="tabla_fila">
                            <a class="btn btn-primary btn-xs" onclick="carousel('garantiafile', <?php echo $dt['tb_garantia_id'];?>)"><i class="fa fa-eye"></i></a>
                            <?php if($estado == 1){ ?>
                                <a class="btn btn-warning btn-xs" style="<?php echo $grosor_borde; ?> border-color: <?php echo $borde; ?> !important; " onclick='comentario_item_inventario(<?php echo $dt['tb_inventario_detalle_id']; ?>, <?php echo $dt['tb_inventario_id']; ?>)'><i class="fa fa-fw fa-commenting-o"></i></a>
                                <a class="btn btn-danger btn-xs" onclick='eliminar_item_inventario(<?php echo $dt['tb_inventario_detalle_id']; ?>, <?php echo $inventario_id; ?>)'><i class="fa fa-fw fa-trash"></i></a>
                            <?php } ?>
                        </td>
                    </tr> <?php
                    $res = null;
                    $cont++;
            } 
            ?>
        </tbody>
    </table>
<?php endif; ?>

