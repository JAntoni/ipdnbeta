<?php
require_once '../../core/usuario_sesion.php';
require_once ('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once ("../funciones/funciones.php");
require_once('../funciones/fechas.php');
require_once ('../inventarioproducto/Inventario.class.php');
$oInventario = new Inventario();
require_once('../almacen/Almacen.class.php');
$oAlmacen = new Almacen();
$r = $oAlmacen->mostrarTodos1()['data'];

$estado = 0; // todos los estados
//$garantia_id = 0;
$alm_id = intval($_POST['alm_id']);
$inventario_id = intval($_POST['inventario_id']);
$codigo = '';
$caracteres_codigo_barra = 0;
if(isset($_POST['codigo'])){
    $codigo = $_POST['codigo']; // credito_id
    $caracteres_codigo_barra = strlen($codigo);

}
$cli_id = '';

$fecha_hoy = date('d-m-Y');

// obtener garantias ya seleccionadas
$list_detalles = '';
$detalles = $oInventario->mostrar_todos_detalle($inventario_id, $codigo);
    $estado_detalles = intval($detalles['estado']);
    if($detalles['estado'] == 1){
        $i = 0;
        $list_detalles .= '(';
        foreach ($detalles['data']as $key => $d) {
            if($i==0){ //primera interaccion
                $list_detalles .= $d['tb_garantia_id'];
            }else{ // resto de interacciones
                $list_detalles .= ', '.$d['tb_garantia_id'];
            }
            $i++;
        }
        $list_detalles .= ')';
    }
$detalles = NULL;
//

?>
<input type="hidden" id="usuario_mos" value="<?php echo $_SESSION['usuario_mos']; ?>">
<input type="hidden" id="almacen_id" value="<?php echo $alm_id; ?>">
<input type="hidden" id="inventario_id" value="<?php echo $inventario_id; ?>">
<input type="hidden" id="arreglo_almacenes" value='<?php echo json_encode($r); ?>'>

<?php 
if($estado_detalles == 1){
    if($list_detalles=='()'){
        $dts = $oInventario->mostrar_todos_garantia($estado, $alm_id, $cli_id, $codigo);
    }else{
        $dts = $oInventario->mostrar_todos_filtro_garantias($estado, $alm_id, $cli_id, $list_detalles, $inventario_id, $codigo);
    }
}else{
    $dts = $oInventario->mostrar_todos_garantia($estado, $alm_id, $cli_id, $codigo);
}

if ($dts['estado'] == 1): ?>
    <input type="hidden" id="gar_select" value="">
    <input type="hidden" id="gar_credito_select" name="gar_credito_select" value="">
    <input type="hidden" id="gar_producto_select" name="gar_producto_select" value="">
    <input type="hidden" id="gar_cliente_select" name="gar_cliente_select" value="">
    <h4><strong>Garantías en almacén</strong></h4>

    <table id="" class="table">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">N°</th>
                <th id="tabla_cabecera_fila">ID GARANTÍA</th>
                <th id="tabla_cabecera_fila">CRÉDITO</th>
                <th id="tabla_cabecera_fila">GARANTÍA</th>
                <th id="tabla_cabecera_fila">CLIENTE</th>
                <th id="tabla_cabecera_fila"></th>
            </tr>
        </thead>  
        <tbody> 
            <?php
                $cont = 1;
                foreach ($dts['data'] as $key => $dt) {
                    $est_almacen = '';
                    if($dt['tb_garantia_almest']==0){
                        $est_almacen = '<strong><i>(OFICINA)</i></strong>';
                    }elseif($dt['tb_garantia_almest']==1){
                        $est_almacen = '<strong><i>(TRÁNSITO)</i></strong>';
                    }elseif($dt['tb_garantia_almest']==2){
                        $est_almacen = '<strong><i>(ALMACENADO)</i></strong>';
                    }elseif($dt['tb_garantia_almest']==3){
                        $est_almacen = '<strong><i>(TRÁNSITO)</i></strong>';
                    }
            ?>
                    <tr class="" name="tabla_cabecera_fila_gar" id="garantia<?php echo $dt['tb_garantia_id']; ?>" onclick="garantiaSelect(this, '<?php echo $dt['tb_garantia_id']; ?>')">
                        <td id="tabla_fila"><?php echo $cont ?></td>
                        <td align="left" id="tabla_fila"><?php echo $dt['tb_garantia_id']; ?></td>
                        <td id="tabla_fila"><?php echo 'CM-' . str_pad($dt['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td>
                        <td align="left" id="tabla_fila"><?php echo $dt['tb_garantia_pro'].' '.$est_almacen; ?></td>
                        <td align="left" id="tabla_fila"><?php echo $dt['tb_cliente_nom']; ?></td>
                        <td align="center" id="tabla_fila">
                            <a class="btn btn-primary btn-xs" onclick="carousel('garantiafile', <?php echo $dt['tb_garantia_id'];?>)"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr> 
            <?php
                    $res = null;
                    $cont++;
                } 
            ?>
        </tbody>
    </table>
<?php endif; ?>

