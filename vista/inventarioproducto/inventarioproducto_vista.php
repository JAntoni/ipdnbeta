
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Inventario</a></li>
            <!-- <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li> -->
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">

            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <label for="txt_filtro" class="control-label">Filtrar por</label>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php include 'inventarioproducto_filtro.php'; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="inventarioproducto_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    
                    <!-- Input para guardar el valor ingresado en el search de la tabla-->
                    <input type="hidden" id="hdd_datatable_fil">
                    <div class="col-sm-12">
                        <div id="div_inventarioproducto_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <input type="hidden" id="hdd_almacen_id">
                            <?php // require_once('inventarioproducto_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="div_modal_inventarioproducto_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>

            <div id="div_modal_inventariofile_galeria">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
            <div id="div_comentario_item_garantia"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <div id="div_modal_carousel"></div>
            
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
