<?php
session_name("ipdnsac");
session_start();

define('FPDF_FONTPATH','font/'); 
include('../../static/fpdf/fpdf.php');

require_once('../inventarioproducto/Inventario.class.php');
$oInventario = new Inventario();
require_once ('../garantia/Garantia.class.php');
$oGarantia = new Garantia();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
	
$inventario_id = "";
if(isset($_GET['inventario_id'])){
	$inventario_id=$_GET['inventario_id'];
}

$dts0 = $oInventario->mostrarUnoReporte($inventario_id);

$fechas = "";
if($dts0['data']['tb_inventario_fecini']!='' || $dts0['data']['tb_inventario_fecini']!=null){
	$fechas .= "FECHA INICIO: ".mostrar_fecha($dts0['data']['tb_inventario_fecini']);
}

if($dts0['data']['tb_inventario_fecfin']!='' || $dts0['data']['tb_inventario_fecfin']!=null){
	$fechas .= " | FECHA FIN: ".mostrar_fecha($dts0['data']['tb_inventario_fecfin']);
}

$responsable = $dts0['data']['usuario'];
$descripcion = $dts0['data']['tb_inventario_des'];
$almacen = $dts0['data']['almacen'];
$cantidad = $dts0['data']['tb_inventario_total_prod'];

$dts = $oInventario->mostrar_todos_detalle($inventario_id,'');
//


$pdf=new FPDF();
$pdf->AddPage('L','A4');

$pdf->SetFont('Arial','B',14);
$pdf->SetFillColor(220,220,220);
$alto=5;
$pdf->Ln(8);
$pdf->Cell(270,5,utf8_decode("INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C."),0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(270,6,utf8_decode("REPORTE DE INVENTARIO"),0,1,'C');

$pdf->SetFont('Arial','B',10);
$pdf->Cell(270,6,utf8_decode($fechas),0,1,'C');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(270,6,utf8_decode("ALMACÉN: ".$almacen),0,1,'L');
$pdf->Cell(270,6,utf8_decode("RESPONSABLE: ".$responsable),0,1,'L');
$pdf->Cell(270,6,utf8_decode("CANTIDAD DE GARANTÍAS: ".$cantidad),0,1,'L');
$pdf->Cell(270,6,utf8_decode("DESCRIPCIÓN: ".$descripcion),0,1,'L');

$pdf->Ln();
$pdf->SetFont('Arial','B',8);
$pdf->Cell(10,8,utf8_decode("#"),1,0,'C',1);
$pdf->Cell(20,8,utf8_decode("CRÉDITO"),1,0,'C',1);
$pdf->Cell(20,8,utf8_decode("GARANTÍA ID"),1,0,'C',1);
$pdf->Cell(80,8,utf8_decode("GARANTÍA"),1,0,'C',1);
$pdf->Cell(80,8,utf8_decode("CLIENTE"),1,0,'C',1);
$pdf->Cell(60,8,utf8_decode("COMENTARIO"),1,0,'C',1);

$i=1;
$pdf->SetFont('Arial','',7);
$pdf->Ln();
$pag_anterior=$pdf->PageNo();


	/* VALORES */
    if($dts['estado'] == 1){

        foreach ($dts['data'] as $key => $dt) {

            $pdf->Cell(8,$alto*2,'',0,0,'C');

            if($pag_anterior!=$pdf->PageNo()){
                $pdf->SetFont('Arial','B',8);
                $pag_anterior=$pdf->PageNo();
                $pdf->Ln(12);
                $pdf->Cell(10,8,utf8_decode("#"),1,0,'C',1);
                $pdf->Cell(20,8,utf8_decode("CRÉDITO"),1,0,'C',1);
                $pdf->Cell(20,8,utf8_decode("GARANTÍA ID"),1,0,'C',1);
                $pdf->Cell(80,8,utf8_decode("GARANTÍA"),1,0,'C',1);
                $pdf->Cell(80,8,utf8_decode("CLIENTE"),1,0,'C',1);
                $pdf->Cell(60,8,utf8_decode("COMENTARIO"),1,0,'C',1);
                $pdf->Ln();
            }else{
                $pdf->Cell(-8,$alto,'',0,0,'C');
            }

            $pdf->SetFont('Arial','',7);

            $pdf->Cell(10,$alto,$i,'T',0,'L');
            $pdf->Cell(20,$alto, 'CM-'.$dt['tb_credito_id'],'T',0,'L');
            $pdf->Cell(20,$alto, $dt['tb_garantia_id'],'T',0,'L');
            $pdf->Cell(80,$alto, "",0,0,'L');
            $pdf->Cell(80,$alto, $dt['tb_cliente_nom'],'T',0,'L');
            $pdf->Cell(60,$alto, "",0,0,'L');
            
            $pdf->Cell(-220);
            $y1 = $pdf->GetY();
            $pdf->MultiCell(80,$alto,utf8_decode($dt['tb_garantia_pro']),'T','L',0);
            $y2 = $pdf->GetY();
            $alto1 = $y2-$y1;
            $pdf->Ln(-1*$alto1);
            $pdf->Cell(210);
            $pdf->MultiCell(60,$alto,utf8_decode($dt['tb_inventario_detalle_des']),'T','L',0);
            $y3 = $pdf->GetY();
            $alto2 = $y3 - $y1;

            if($alto1>$alto2){
                $pdf->Ln($alto1-$alto2);
            }
            $i++;

        }

    }
	
$pdf->SetAutoPageBreak('auto',2); 
$pdf->SetDisplayMode(75);
$pdf->Output();
?>