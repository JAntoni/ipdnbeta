<?php
require_once("../../core/usuario_sesion.php");
require_once("../usuario/Usuario.class.php");
$oUsuario = new Usuario();
require_once("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../refinanciar/Refinanciar.class.php");
$oRefinanciar = new Refinanciar();
require_once("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../cuota/Cuotadetalle.class.php');
$oCuotadetalle = new Cuotadetalle();
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once ("../gasto/Gasto.class.php");
$oGasto = new Gasto();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
require_once("../refinanciar/refinanciar_funciones.php"); //para funciones propias de REFINANCIAR


$creditotipo_id = $_POST['cre_tip']; //tipo de crédito 1 menor, 2 asiveh, 3 garveh, 4 hipo
$action = $_POST['action'];
$credito_id = $_POST['cre_id'];
$ref_tip = $_POST['ref_tip'];
$fecha_ref = trim($_POST['fecha_ref']); //fecha que se ingresa al momento de solicitar una refinanciación
$fecha_hoy = fecha_mysql($fecha_ref); //date('Y-m-d');
$tipo_refinanciar = $_POST['tipo_refinanciar'];
$id_multiples = $_POST['id_multiples'];
$cli_id = 0;

if ($creditotipo_id == 2) {
  require_once("../creditoasiveh/Creditoasiveh.class.php");
  $oCredito = new Creditoasiveh();
}
if ($creditotipo_id == 3) {
  require_once("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
}
if ($creditotipo_id == 4) {
  require_once("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();
}

$TIPO_DE_CAMBIO = 1;
$result = $oMonedacambio->consultar(fecha_mysql($fecha_ref));
if ($result['estado'] == 1) {
  $TIPO_DE_CAMBIO = $result['data']['tb_monedacambio_val'];
}
$result = null;

$validar_fecha = validar_fecha_espanol($fecha_ref);

if (!$validar_fecha) {
  echo '<h2 style="color: red;">POR FAVOR INGRESE UNA FECHA VÁLIDA PARA PODER CALCULAR, RECUERDE QUE EL FORMATO ES COMO EL SIGUIENTE: 01-10-2018 / ' . $fecha_ref . '</h2>';
  exit();
}

$cre_feccre = date('d-m-Y');
$cre_fecdes = date('d-m-Y');
$cre_fecfac = date('d-m-Y');
$cre_fecent = date('d-m-Y');
$cre_fecpro = date('d-m-Y');

if ($tipo_refinanciar == 'simple') {
  $result = $oCredito->mostrarUno($credito_id);
  if ($result['estado'] == 1) {
    $cre_matriz = $result['data']['tb_credito_idori'];
    $credito_matriz_moneda_id = $result['data']['tb_moneda_id'];
    $credito_preaco = floatval($result['data']['tb_credito_preaco']);
    $credito_fecha_facturacion = $result['data']['tb_credito_fecfac'];
  }
  $result = null;

  $simbolo_moneda_matriz = simbolo_moneda($credito_matriz_moneda_id); //1 de vuelve s/, 2 $usd
  $FILA_TABLA = ''; //PARA LOS DETALLES
  $TEXTO_GLOBAL = '';
  $texto_matriz = '<b>CAPITAL PRESTADO: ' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($credito_preaco) . '</b>. La fecha desde donde parte la facturación fue el <b>' . mostrar_fecha($credito_fecha_facturacion) . '</b>';
  $FILA_TABLA_MATRIZ = '
    <tr style="font-family: cambria;font-size: 12px;" class="success">
      <td id="filacabecera"><b>CAPITAL PRESTADO</b> DEL CREDITO MATRIZ</td>
      <td id="filacabecera">' . $credito_id . '</td>
      <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($credito_preaco) . '</td>
      <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
      <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($credito_preaco) . '</td>
      <td id="filacabecera">' . $texto_matriz . '</td>
    </tr>
  ';

  $TEXTO_GLOBAL .= $texto_matriz;

  //* DESDE AQUÍ EMPIEZA EL BUCLE CON TODOS LOS CRÉDITOS QUE PERTENECEN AL CREDITO MATRIZ
  $credito_padre_id = $credito_id;
  $array_creditos = array();
  array_push($array_creditos, $credito_id);

  $result = $oCredito->listar_adendas_credito($credito_id);
  if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
      array_push($array_creditos, $value['tb_credito_id']);
    }
  }
  $result = NULL;
}

if ($tipo_refinanciar == 'multiple') {
  $credito_matriz_moneda_id = 1;
  $array_creditos = explode(',', $id_multiples);
}

$SUMA_GLOBAL_A_PAGAR = 0;
$SUMA_TOTAL_GPS = 0; //PARA CAPTAR EL MONTO FALTANTE DE PAGO DEL GPS
$SUMA_GLOBAL_GASTOS = 0;

for ($i = 0; $i < count($array_creditos); $i++) {

  $credito_id = $array_creditos[$i];

  $ultima_cuota_pagada_fecha = '';
  $cuota_facturada_pagada = 'NO'; //si existe una cuota facturada y pagada

  $TOTAL_INTERESES_PREVIOS = 0;
  $TOTAL_PAGOS_PREVIOS = 0;

  $TOTAl_INTERESES_POSTERIORES = 0;
  $TOTAl_PAGOS_POSTERIORES = 0;
  $PRORRATEO_TOTAL = 0;
  $MONTO_GPS = 0;

  $result = $oCredito->mostrarUno($credito_id);
  if ($result['estado'] == 1) {
    $credito_hijo_moneda_id = $result['data']['tb_moneda_id'];
    $cli_id = $result['data']['tb_cliente_id'];
    $credito_preaco = floatval($result['data']['tb_credito_preaco']);
    $credito_fecha_facturacion = $result['data']['tb_credito_fecfac'];
    $cre_cuotip = intval($result['data']['tb_cuotatipo_id']);
    $cre_subper_id = $result['data']['tb_cuotasubperiodo_id']; //1 mensual, 2 quincenal, 3 semanal
    $cre_fecren = mostrar_fecha($result['data']['tb_credito_fecren']); //fecha de vencimiento del SOAT VER: tb_credito_fecsoat  // ESTABA: tb_credito_fecren
    $cre_fecseg = mostrar_fecha($result['data']['tb_credito_fecseg']); //fecha de vencimiento del STR VER: tb_credito_fecstr // ESTABA: tb_credito_fecseg
    $cre_fecgps = mostrar_fecha($result['data']['tb_credito_fecgps']); //fecha de vencimiento del GPS
    $cre_fpagimp = mostrar_fecha($result['data']['tb_credito_pagimp']); //fecha del pago del inpuesto vehicular
    $credito_int = floatval($result['data']['tb_credito_int']);
    $cre_diaspa = intval($result['data']['tb_credito_diapar']);
    $credito_numcuo = intval($result['data']['tb_credito_numcuo']); //captamos el número de cuotas del crédito
    $credito_pregps = $result['data']['tb_credito_pregps']; // optenemos el valor del gps guardado en el crédito
  }
  $result = null;

  $simbolo_moneda_hijo = simbolo_moneda($credito_hijo_moneda_id); //1 de vuelve s/, 2 $usd
  $titulo1 = '<b>CAPITAL RESTANTE</b> DE LA ADENDA:';
  $titulo2 = '<b>PRORRATEO</b> DE LA ADENDA:';
  $class_fila = 'class="warning"';
  if ($credito_id == $credito_padre_id) {
    $titulo1 = '<b>CAPITAL RESTANTE</b> DEL CREDITO MATRIZ:';
    $titulo2 = '<b>PRORRATEO</b> DEL CREDITO MATRIZ:';
    $class_fila = 'class="success"';
  }

  //? PASO 1: IDENTIFICAR LA ULTIMA CUOTA FACTURADA Y PAGADA: obtenemos capital actual y fecha de cuota, esta función lista las cuotas en estado 2 (pagadas) menores a la fecha de HOY
  $result = $oRefinanciar->ultima_cuotadetalle_facturada_pagada($credito_id, $creditotipo_id);
  if ($result['estado'] == 1) {
    $cuota_facturada_pagada = 'SI';
    $ultima_cuota_pagada_fecha = $result['data']['tb_cuotadetalle_fec'];
  }
  $result = NULL;

  if ($cuota_facturada_pagada == 'SI') {
    //! PASO 2: LISTAMOS LAS CUOTAS MENORES EN FECHA A LA OBTENIDA: esto para poder sumar todas las cuotas pagadas y determinar el CAPITAL actual adeudado
    //listamos todas las cuotas <= a la cuotadetalle para sumar los pagos realizados
    $numero_cuotas_pagadas = 0;
    $MONTO_FACTURADO_GPS = 0;
    $valor_una_cuota_gps = 0;

    $result = $oRefinanciar->listar_cuotasdetalle_menores_condicion_fecha($credito_id, $creditotipo_id, $ultima_cuota_pagada_fecha, '<=');
    if ($result['estado'] == 1) {
      foreach ($result['data'] as $key => $value) {
        $numero_cuotas_pagadas ++;
        $MONTO_FACTURADO_GPS += $value['tb_cuota_pregps'];
        $valor_una_cuota_gps = $value['tb_cuota_pregps'];

        $TOTAL_INTERESES_PREVIOS += ($value['tb_cuota_int'] + $value['tb_cuota_pro']); //? el prorrateo en la cuota se debe considerar como interés adicional
        //OBTENEMOS LOS PAGOS DE CADA CUOTA PAGADA
        $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuotadetalle_id'], 2, $value['tb_moneda_id']); //2 pago cuotadetalle
        if ($result2['estado'] == 1) {
          $TOTAL_PAGOS_PREVIOS += $result2['data']['importe_total'];
        }
        $result2 = NULL;
      }
    }
    $result = NULL;

    //* PASO 3: FECHAS VENCIDAS IMPAGAS MAYORES A ULTIMA CUOTA FACTURADA: identificamos las cuotas mayores a la facturada para contarlas y sumar sus intereses
    $cuotas_vencidas_impagas = '';
      //* la ultima cuota pagada puede ser mayor que la fecha hoy, es decir el cliente pagó una cuota adelantada
    if(strtotime($fecha_hoy) >= strtotime($ultima_cuota_pagada_fecha)){
      $result = $oRefinanciar->listar_cuotasdetalle_impagas_entre_rango($credito_id, $creditotipo_id, $ultima_cuota_pagada_fecha, $fecha_hoy);
        if ($result['estado'] == 1) {
          foreach ($result['data'] as $key => $value) {
            $TOTAl_INTERESES_POSTERIORES += $value['tb_cuota_int'];
            //OBTENEMOS PAGOS SI EN CASO LO TUVIERAN LAS CUOTAS IMPAGAS, EN CASO TENGAN PAGOS PARCIALES
            $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuotadetalle_id'], 2, $value['tb_moneda_id']); //2 pago cuotadetalle
            if ($result2['estado'] == 1) {
              $TOTAl_PAGOS_POSTERIORES += $result2['data']['importe_total'];
            }
            $result2 = NULL;

            $cuotas_vencidas_impagas .= '<b>' . mostrar_fecha($value['tb_cuotadetalle_fec']) . '</b>, ';
          }

          $cuotas_vencidas_impagas = substr($cuotas_vencidas_impagas, 0, -2);
        }
      $result = NULL;
    }

    //TODO PASO 4: CALCULAR EL PRORRATEO DE DÍAS PASADOS HASTA LA FECHA ACTUAL
    $ultima_cuota_facturada_fecha = '';
    $ultima_cuota_facturada_interes_valido = 0;
    $dias_prorrateo = 0;
    $interes_por_dia = 0;

    //PUEDE O NO ESTAR PAGADA ESTA CUOTA FACTURADA, SOLO NOS SIRVE PARA CALCULAR UN PRORRATEO, LISTA TODAS LAS CUOTAS MENORES A HOY, SI EL CLIENTE PAGÓ POR ADELANTADO
    //UNA CUOTA, NO ES NECESARIO CALCULAR UN PRORRATEO
    if(strtotime($fecha_hoy) >= strtotime($ultima_cuota_pagada_fecha)){
      $result = $oRefinanciar->ultima_cuotadetalle_facturada($credito_id, $creditotipo_id);
        if ($result['estado'] == 1) {
          $ultima_cuota_facturada_fecha = $result['data']['tb_cuotadetalle_fec'];
          $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
          $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
          //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
          $ultima_cuota_facturada_interes_valido = formato_numero($operacion);
        }
      $result = NULL;
    }

    if ($ultima_cuota_facturada_fecha != '') {
      list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
      $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
      $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

      $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
      $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
      $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
      $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);
    }

    $monto_gps_restante = $valor_una_cuota_gps * ($credito_numcuo - $numero_cuotas_pagadas); //el monto facturado de gps, suma el monto total pagado

    $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA = $credito_preaco + $TOTAL_INTERESES_PREVIOS + $MONTO_FACTURADO_GPS - $TOTAL_PAGOS_PREVIOS;
    $INTERESES_CUOTAS_IMPAGAS = $TOTAl_INTERESES_POSTERIORES - $TOTAl_PAGOS_POSTERIORES;

    $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA;
    $INTERESES_CUOTAS_CAMBIO = $INTERESES_CUOTAS_IMPAGAS;
    $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;
    $GPS_TOTAL_CAMBIO = $monto_gps_restante;

    if ($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2) {
      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA * $TIPO_DE_CAMBIO;
      $INTERESES_CUOTAS_CAMBIO = $INTERESES_CUOTAS_IMPAGAS * $TIPO_DE_CAMBIO;
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL * $TIPO_DE_CAMBIO;
      if(intval($GPS_TOTAL_CAMBIO) > 0)
        $GPS_TOTAL_CAMBIO = $monto_gps_restante * $TIPO_DE_CAMBIO;
    }
    if ($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1) {
      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA / $TIPO_DE_CAMBIO;
      $INTERESES_CUOTAS_CAMBIO = $INTERESES_CUOTAS_IMPAGAS / $TIPO_DE_CAMBIO;
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
      if(intval($GPS_TOTAL_CAMBIO) > 0)
        $GPS_TOTAL_CAMBIO = $monto_gps_restante / $TIPO_DE_CAMBIO;
    }

    $SUMA_GLOBAL_A_PAGAR += $CAPITAL_RESTANTE_CAMBIO + $INTERESES_CUOTAS_CAMBIO + $PRORRATEO_TOTAL_CAMBIO + $GPS_TOTAL_CAMBIO;

    $texto_detalle = $titulo1 . ' <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>, VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($CAPITAL_RESTANTE_CAMBIO) . '</b>. La última cuota pagada fue el <b>' . mostrar_fecha($ultima_cuota_pagada_fecha) . '</b>, a partir de aquí se procede a: 1. Sumar todos los intereses hasta esta fecha, 2. Identificar el Capital Prestado, 3. Sumar todos los pagos realizados hasta esta fecha. Finalmente se realiza la operación: INTERESES + CAPITAL PRESTADO - PAGOS REALIZADOS.</b>';

    $FILA_TABLA .= '
      <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
        <td id="filacabecera">' . $titulo1 . '</td>
        <td id="filacabecera">' . $credito_id . '</td>
        <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA) . '</td>
        <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
        <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($CAPITAL_RESTANTE_CAMBIO) . '</td>
        <td id="filacabecera">' . $texto_detalle . '</td>
      </tr>
    ';

    $TEXTO_GLOBAL .= $texto_detalle . '</br>';

    if ($cuotas_vencidas_impagas != '') {
      $texto_detalle = '<b>INTERESES CUOTAS VENCIDAS: ' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($INTERESES_CUOTAS_IMPAGAS) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>,  VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($INTERESES_CUOTAS_CAMBIO) . '</b>. Se suman los intereses de las cuotas impagadas facturadas, las fechas impagas fueron: ' . $cuotas_vencidas_impagas . '. La suma de intereses vencidos impagos fueron: <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($TOTAl_INTERESES_POSTERIORES) . '</b> y los pagos parciales en estas cuotas fueron: <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($TOTAl_PAGOS_POSTERIORES) . '</b>';

      $FILA_TABLA .= '
        <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
          <td id="filacabecera" colspan="2"><b>Intereses Cuotas Vencidas</b></td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($INTERESES_CUOTAS_IMPAGAS) . '</td>
          <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($INTERESES_CUOTAS_CAMBIO) . '</td>
          <td id="filacabecera">' . $texto_detalle . '</td>
        </tr>
      ';
      $TEXTO_GLOBAL .= $texto_detalle . '</br>';
    }
    if ($PRORRATEO_TOTAL > 0) {
      $texto_detalle = $titulo2 . ' <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($PRORRATEO_TOTAL) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>, VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($PRORRATEO_TOTAL_CAMBIO) . '</b>. Días pasados desde la última cuota facturada <b>' . mostrar_fecha($ultima_cuota_facturada_fecha) . '</b> hasta el día de hoy <b>' . $fecha_ref . '</b>. Han pasado <b>' . $dias_prorrateo . ' días</b> y el interés diario para este periodo es de <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($interes_por_dia) . '</b>';

      $FILA_TABLA .= '
        <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
          <td id="filacabecera">' . $titulo2 . '</td>
          <td id="filacabecera">' . $credito_id . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($PRORRATEO_TOTAL) . '</td>
          <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($PRORRATEO_TOTAL_CAMBIO) . '</td>
          <td id="filacabecera">' . $texto_detalle . '</td>
        </tr>
      ';
      $TEXTO_GLOBAL .= $texto_detalle . '</br>';
    }
    if (intval($GPS_TOTAL_CAMBIO) > 0) {
      $texto_detalle = 'El precio total del GPS es de: <b>'.mostrar_moneda($credito_numcuo * $valor_una_cuota_gps).'</b>, se ha pagado hasta el momento: <b>'.mostrar_moneda($valor_una_cuota_gps * $numero_cuotas_pagadas).', ('.$numero_cuotas_pagadas.' cuotas pagadas)</b>, falta por pagar hasta la fecha: <b>'.mostrar_moneda($GPS_TOTAL_CAMBIO).'</b>';

      $FILA_TABLA .= '
        <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
          <td id="filacabecera">VALOR DEL GPS</td>
          <td id="filacabecera">' . $credito_id . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($monto_gps_restante) . '</td>
          <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($GPS_TOTAL_CAMBIO) . '</td>
          <td id="filacabecera">' . $texto_detalle . '</td>
        </tr>
      ';
      $TEXTO_GLOBAL .= $texto_detalle . '</br>';
    }
  }

  //? FIN IF DE UNA CUOTA PAGADA = SI

  if ($cuota_facturada_pagada == 'NO') {
    //SI NO TIENE UNA CUOTA PAGADA DE FECHA MENOR A HOY, VEAMOS SI TIENE CUOTAS IMPAGAS MENOR A HOY. TAMBIEN PUEDE SER QUE NO TENGA NINGUNA CUOTA FACTURADA, ES DECIR ESTÁ PAGANDO ANTES DE TIEMPO
    $ultima_cuota_facturada_fecha = '';
    $ultima_cuota_facturada_interes_valido = 0;
    $dias_prorrateo = 0;
    $interes_por_dia = 0;

    $result = $oRefinanciar->ultima_cuotadetalle_facturada($credito_id, $creditotipo_id); //PUEDE ESTAR PAGADA O NO
    if ($result['estado'] == 1) {
      $ultima_cuota_facturada_fecha = $result['data']['tb_cuotadetalle_fec'];
      $ultima_cuota_facturada_capital_restante = $result['data']['tb_cuota_cap'] - $result['data']['tb_cuota_amo'];
      $valor_una_cuota_gps = $result['data']['tb_cuota_pregps'];
      //para calcular el prorrateo se debe dividir el interés, pero debe ser el interés de la cuota siguiente a la facturada porque el interés de esta facturada ya se estaría contando junto al capital prestado
      if ($ultima_cuota_facturada_capital_restante > 0) {
        $operacion = $ultima_cuota_facturada_capital_restante * $credito_int / 100;
        $ultima_cuota_facturada_interes_valido = formato_numero($operacion);
      } else
        $ultima_cuota_facturada_interes_valido = formato_numero($result['data']['tb_cuota_int']);
    }
    $result = NULL;

    if ($ultima_cuota_facturada_fecha != '') {
      //listamos todas las cuotas <= a la cuotadetalle para sumar los pagos realizados
      $todo_cuotas_impagas = '';
      $result = $oRefinanciar->listar_cuotasdetalle_menores_condicion_fecha($credito_id, $creditotipo_id, $ultima_cuota_facturada_fecha, '<=');
      if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
          $TOTAL_INTERESES_PREVIOS += ($value['tb_cuota_int'] + $value['tb_cuota_pro']); //? el prorrateo en la cuota se debe considerar como interés adicional
          $todo_cuotas_impagas .= '<b>' . mostrar_fecha($value['tb_cuotadetalle_fec']) . '</b>, ';
          //OBTENEMOS LOS PAGOS DE CADA CUOTA PAGADA
          $result2 = $oIngreso->mostrar_importe_total_cuota($value['tb_cuotadetalle_id'], 2, $value['tb_moneda_id']); //2 pago cuotadetalle
          if ($result2['estado'] == 1) {
            $TOTAL_PAGOS_PREVIOS += $result2['data']['importe_total'];
          }
          $result2 = NULL;
        }

        $todo_cuotas_impagas = substr($todo_cuotas_impagas, 0, -2);
      }
      $result = NULL;

      list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $ultima_cuota_facturada_fecha);
      $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
      $interes_por_dia = formato_numero($ultima_cuota_facturada_interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

      $fecha_facturada_time = new DateTime($ultima_cuota_facturada_fecha); //cuantos días han pasado desde la ultima facturada hasta hoy
      $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
      $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
      $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);

      $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA = $credito_preaco + $TOTAL_INTERESES_PREVIOS - $TOTAL_PAGOS_PREVIOS;

      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA;
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;

      if ($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2) {
        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA * $TIPO_DE_CAMBIO;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL * $TIPO_DE_CAMBIO;
      }
      if ($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1) {
        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA / $TIPO_DE_CAMBIO;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
      }

      $SUMA_GLOBAL_A_PAGAR += $CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO;

      $texto_detalle = $titulo1 . ' <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>, VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($CAPITAL_RESTANTE_CAMBIO) . '</b>. Se suma el Capital prestado de <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($credito_preaco) . '</b> con las fechas vencidas impagas del: <b>' . $todo_cuotas_impagas . '</b>, a esto restamos los pagos previos existentes que fueron: <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($TOTAL_PAGOS_PREVIOS) . '</b>';

      $FILA_TABLA .= '
        <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
          <td id="filacabecera">' . $titulo1 . '</td>
          <td id="filacabecera">' . $credito_id . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($CAPITAL_RESTANTE_HASTA_ULTIMA_FACTURADA) . '</td>
          <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($CAPITAL_RESTANTE_CAMBIO) . '</td>
          <td id="filacabecera">' . $texto_detalle . '</td>
        </tr>
      ';
      $TEXTO_GLOBAL .= $texto_detalle . '</br>';

      if ($PRORRATEO_TOTAL > 0) {
        $texto_detalle = $titulo2 . ' <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($PRORRATEO_TOTAL) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>, VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($PRORRATEO_TOTAL_CAMBIO) . '</b>. Días pasados desde la última cuota vencida <b>' . mostrar_fecha($ultima_cuota_facturada_fecha) . '</b> hasta el día de hoy <b>' . $fecha_ref . '</b>. Han pasado <b>' . $dias_prorrateo . ' días</b> y el interés diario para este periodo es de <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($interes_por_dia) . '</b>';

        $FILA_TABLA .= '
          <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
            <td id="filacabecera">' . $titulo2 . '</td>
            <td id="filacabecera">' . $credito_id . '</td>
            <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($PRORRATEO_TOTAL) . '</td>
            <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
            <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($PRORRATEO_TOTAL_CAMBIO) . '</td>
            <td id="filacabecera">' . $texto_detalle . '</td>
          </tr>
        ';

        $TEXTO_GLOBAL .= $texto_detalle . '</br>';
      }
    }

    if ($ultima_cuota_facturada_fecha == '') {
      //SI NO TIENE FECHAS IMPAGAS VENCIDAS, ENTONCES ES UNA LIQUIDACION ANTES DE TIEMPO
      //CALULAMOS LOS DÍAS PASADOS PARA EL PRORRATEO Y EL INTERÉS VALIDO PARA EL CALCULO
      $result2 = $oRefinanciar->obtener_primera_cuotadetalle($credito_id, $creditotipo_id);
        if($result2['estado'] == 1){
          $valor_una_cuota_gps = $result2['data']['tb_cuota_pregps'];
        }
      $result2 = NULL;

      list($anio_facturado, $mes_facturado, $dia_facturado) = explode('-', $credito_fecha_facturacion);
      $interes_valido = formato_numero($credito_preaco * $credito_int / 100);
      $dias_del_mes_facturado = cal_days_in_month(CAL_GREGORIAN, $mes_facturado, $anio_facturado); //número de días de una fecha dada
      $interes_por_dia = formato_numero($interes_valido / $dias_del_mes_facturado); //sabemos el interés diario

      $fecha_facturada_time = new DateTime($credito_fecha_facturacion); //cuantos días han pasado desde la ultima facturada hasta hoy
      $fecha_hoy_time = new DateTime($fecha_hoy); //fecha hoy
      $dias_prorrateo = $fecha_hoy_time->diff($fecha_facturada_time)->format("%a"); //días de prorrateo
      $PRORRATEO_TOTAL = formato_numero($dias_prorrateo * $interes_por_dia);

      //CALCULAMOS TODOS LOS PAGOS QUE HAYA TENIDO ESTE CRÉDITO SIN CUOTAS FACTURADAS, SUELE PASAR QUE TIENE PAGOS
      $ingreso_total_credito = 0;
      $result2 = $oIngreso->ingreso_total_por_credito_cuotadetalle($credito_id, $creditotipo_id, $credito_hijo_moneda_id);
      if ($result2['estado'] == 1) {
        foreach ($result2['data'] as $key => $value) {
          $ingreso_total_credito = floatval($value['importe_total']);
        }
      }
      $result2 = null;

      $CAPITAL_REAL_RESTANTE = $credito_preaco - $ingreso_total_credito;

      $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE;
      $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL;

      if ($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2) {
        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE * $TIPO_DE_CAMBIO;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL * $TIPO_DE_CAMBIO;
      }
      if ($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1) {
        $CAPITAL_RESTANTE_CAMBIO = $CAPITAL_REAL_RESTANTE / $TIPO_DE_CAMBIO;
        $PRORRATEO_TOTAL_CAMBIO = $PRORRATEO_TOTAL / $TIPO_DE_CAMBIO;
      }

      $SUMA_GLOBAL_A_PAGAR += $CAPITAL_RESTANTE_CAMBIO + $PRORRATEO_TOTAL_CAMBIO;

      $texto_detalle = $titulo1 . ' <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($CAPITAL_REAL_RESTANTE) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>, VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($CAPITAL_RESTANTE_CAMBIO) . '</b>. Este crédito no tiene cuotas facturadas, se toma el capital prestado de <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($credito_preaco) . '</b> y luego se le sumará el prorrateo desde la fecha facturación <b>' . mostrar_fecha($credito_fecha_facturacion) . '</b> hasta la fecha de hoy <b>' . $fecha_ref . '</b>. Finalmente se le resta los pagos que haya tenido este crédito que fueron: <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($ingreso_total_credito) . '</b>';

      $FILA_TABLA .= '
        <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
          <td id="filacabecera">' . $titulo1 . '</td>
          <td id="filacabecera">' . $credito_id . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($CAPITAL_REAL_RESTANTE) . '</td>
          <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($CAPITAL_RESTANTE_CAMBIO) . '</td>
          <td id="filacabecera">' . $texto_detalle . '</td>
        </tr>
      ';
      $TEXTO_GLOBAL .= $texto_detalle . '</br>';

      if ($PRORRATEO_TOTAL > 0) {
        $texto_detalle = $titulo2 . ' <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($PRORRATEO_TOTAL) . '</b>, TIPO DE CAMBIO: <b>' . $TIPO_DE_CAMBIO . '</b>, VALOR A MONEDA MATRIZ: <b>' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($PRORRATEO_TOTAL_CAMBIO) . '</b>. Días pasados desde la fecha de facturación del crédito: <b>' . mostrar_fecha($credito_fecha_facturacion) . '</b> hasta el día de hoy <b>' . $fecha_ref . '</b>. Han pasado <b>' . $dias_prorrateo . ' días</b> y el interés diario para este periodo es de <b>' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($interes_por_dia) . '</b> ' . $interes_valido . ' / ' . $dias_del_mes_facturado;

        $FILA_TABLA .= '
          <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
            <td id="filacabecera"> // '.$valor_una_cuota_gps.' // ' . $titulo2 . '</td>
            <td id="filacabecera">' . $credito_id . '</td>
            <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($PRORRATEO_TOTAL) . '</td>
            <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
            <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($PRORRATEO_TOTAL_CAMBIO) . '</td>
            <td id="filacabecera">' . $texto_detalle . '</td>
          </tr>
        ';
        $TEXTO_GLOBAL .= $texto_detalle . '</br>';
      }
    }

    $monto_gps_restante = $valor_una_cuota_gps * $credito_numcuo; //el monto facturado de gps, suma el monto total pagado
    $GPS_TOTAL_CAMBIO = $monto_gps_restante;

    if ($credito_matriz_moneda_id == 1 && $credito_hijo_moneda_id == 2 && intval($GPS_TOTAL_CAMBIO) > 0) {
      $GPS_TOTAL_CAMBIO = $monto_gps_restante * $TIPO_DE_CAMBIO;
    }
    if ($credito_matriz_moneda_id == 2 && $credito_hijo_moneda_id == 1 && intval($GPS_TOTAL_CAMBIO) > 0) {
      $GPS_TOTAL_CAMBIO = $monto_gps_restante / $TIPO_DE_CAMBIO;
    }

    $SUMA_GLOBAL_A_PAGAR += $GPS_TOTAL_CAMBIO;

    if (intval($GPS_TOTAL_CAMBIO) > 0) {
      $texto_detalle = 'El precio total del GPS es de: <b>'.mostrar_moneda($credito_numcuo * $valor_una_cuota_gps).'</b>, no se ha pagado ninguna cuota del GPS';

      $FILA_TABLA .= '
        <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
          <td id="filacabecera">VALOR DEL GPS</td>
          <td id="filacabecera">' . $credito_id . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_hijo . ' ' . mostrar_moneda($monto_gps_restante) . '</td>
          <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
          <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($GPS_TOTAL_CAMBIO) . '</td>
          <td id="filacabecera">' . $texto_detalle . '</td>
        </tr>
      ';
      $TEXTO_GLOBAL .= $texto_detalle . '</br>';
    }
  }

  //? PASO FINAL SERÁ SUMAR LOS GASTOS LEGALES PARA LIQUIDAR
  $gastos_generados = 0;
  $result2 = $oGasto->listar_gastos_credito_tipo($credito_id, $creditotipo_id);
    if($result2['estado'] == 1){
      foreach ($result2['data'] as $key => $value) {
        $gastos = $value['tb_gasto_ptl'] - $value['tb_gasto_devo']; //? DESDE EL MODULO DE GASTOS HAY DEVOLUCIONES QUE SE REALIZAN A CADA GASTO, se debe restar
        if($credito_matriz_moneda_id == 1 && $value['tb_moneda_id'] == 2){
          $gastos = ($value['tb_gasto_ptl'] - $value['tb_gasto_devo']) * $TIPO_DE_CAMBIO;
        }
        if($credito_matriz_moneda_id == 2 && $value['tb_moneda_id'] == 1){
          $gastos = ($value['tb_gasto_ptl'] - $value['tb_gasto_devo']) / $TIPO_DE_CAMBIO;
        }
        $gastos_generados += $gastos;
      }
    }
  $result2 = NULL;
  
  if($gastos_generados > 0){
    $SUMA_GLOBAL_GASTOS += $gastos_generados;
    $texto_detalle = 'Este crédito contiene GASTOS VEHICULARES, las cuales suma: <b>'.mostrar_moneda($gastos_generados).'</b>';

    $FILA_TABLA .= '
      <tr style="font-family: cambria;font-size: 12px;" ' . $class_fila . '>
        <td id="filacabecera">GASTOS VEHICULARES</td>
        <td id="filacabecera">' . $credito_id . '</td>
        <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($gastos_generados) . '</td>
        <td id="filacabecera">' . $TIPO_DE_CAMBIO . '</td>
        <td id="filacabecera" width="12%">' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($gastos_generados) . '</td>
        <td id="filacabecera">' . $texto_detalle . '</td>
      </tr>
    ';
  }
} //FIN FOR

$AMORTIZACION_TOTAL_PREVIO = 0; //amortización previo
$mod_id = $creditotipo_id; //el modulo en INGRESO para amortizacion es 1 menor, 2 asve, 3 garve, 4 hipo
$modide = $credito_padre_id; //el modide en ingreso para amortización es el ID  del crédito

$result2 = $oIngreso->mostrar_por_modulo($creditotipo_id, $credito_padre_id, $credito_matriz_moneda_id, '1'); // parametro 1 ya que es el estado del ingreso 1 activo
if ($result2['estado'] == 1) {
  foreach ($result2['data'] as $key => $value) {
    $AMORTIZACION_TOTAL_PREVIO += floatval($value['tb_ingreso_imp']);
  }
}
$result2 = NULL;

$AMORTIZACION_TOTAL_PREVIO = formato_moneda($AMORTIZACION_TOTAL_PREVIO); //amortización previo a este crédito
$TOTAL_A_LIQUIDAR = formato_moneda($SUMA_GLOBAL_A_PAGAR + $SUMA_GLOBAL_GASTOS);
$SUMA_GLOBAL_A_PAGAR = formato_moneda($SUMA_GLOBAL_A_PAGAR + $SUMA_GLOBAL_GASTOS - $AMORTIZACION_TOTAL_PREVIO);
$TEXTO_GLOBAL .= '<b>SUMA TOTAL DE CAPITALES + INTERESES:</b> ' . $simbolo_moneda_matriz . ' ' . mostrar_moneda($TOTAL_A_LIQUIDAR);
$prorrateo_total = 0;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_refinanciar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Refinanciar Crédito</h4>
      </div>
      <form id="form_refi">
        <input name="action_credito" id="action_credito" type="hidden" value="insertar">
        <input type="hidden" name="hdd_cli_id" id="hdd_cre_cli_id" value="<?php echo $cli_id; ?>">
        <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $credito_padre_id ?>">
        <input name="hdd_cre_tip" id="hdd_cre_tip" type="hidden" value="<?php echo $creditotipo_id ?>">
        <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo $credito_matriz_moneda_id; ?>">
        <input type="hidden" name="hdd_prorrateo" id="hdd_prorrateo" value="<?php echo $prorrateo_total; ?>">
        <input name="hdd_capital" id="hdd_capital" type="hidden" value="<?php echo $TOTAL_A_LIQUIDAR; ?>">
        <input name="hdd_capital" id="hdd_matriz" type="hidden" value="<?php echo $cre_matriz; ?>">
        <input name="hdd_detalle_refinanciar" id="hdd_detalle_refinanciar" type="hidden" value="<?php echo $TEXTO_GLOBAL; ?>">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Capital de Adendas y Crédito</h3>
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                          <table id="tbl_cap" class="table table-bordered table-hover">
                            <thead>
                              <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                                <th id="filacabecera">TIPO</th>
                                <th id="filacabecera">ID</th>
                                <th id="filacabecera" width="12%">CAPITAL</th>
                                <th id="filacabecera">CAMBIO</th>
                                <th id="filacabecera" width="12%">TOTAL</th>
                                <th id="filacabecera">DETALLE</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php echo $FILA_TABLA_MATRIZ; ?>
                            </tbody>
                          </table>

                          <p></p>

                          <table id="tbl_cap" class="table table-bordered table-hover">
                            <thead>
                              <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                                <th id="filacabecera">TIPO</th>
                                <th id="filacabecera">ID</th>
                                <th id="filacabecera" width="12%">CAPITAL</th>
                                <th id="filacabecera">CAMBIO</th>
                                <th id="filacabecera" width="12%">TOTAL</th>
                                <th id="filacabecera">DETALLE</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php echo $FILA_TABLA; ?>

                              <tr class="filaTabla">
                                <td colspan="4"><b>SUMA TOTAL DE CAPITALES + INTERESES:</b></td>
                                <td><?php echo $simbolo_moneda_matriz . ' ' . mostrar_moneda($TOTAL_A_LIQUIDAR); ?></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <?php if ($creditotipo_id == 2 || $creditotipo_id == 3) : ?>
                        <div class="box box-warning shadow">
                          <div class="box-header with-border">
                            <h3 class="box-title">GPS CONTRATADOS</h3>
                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                            </div>
                          </div>
                          <div class="box-body no-padding">
                            <div class="row">

                              <div class="col-md-12">
                                <table class="table table-striped">
                                  <tbody>
                                    <tr>
                                      <th>TIPO</th>
                                      <th>FECHA INICIO</th>
                                      <th>FECHA FIN</th>
                                      <th>ESTADO</th>
                                      <th>PLACA</th>
                                      <th>CREDITO ID</th>
                                    </tr>
                                    <?php 
                                      $result = $oPoliza->mostrar_gps_cliente_tipo_credito($cli_id, 3); // tipo 3 credito garveh
                                        if($result['estado'] == 1){
                                          foreach ($result['data'] as $key => $value) {
                                            $dias = restaFechas(date('Y-m-d'), $value['tb_poliza_fecfin']);
                                            $estado = '<span style="color: green"><b>Vigente (vence en '.$dias.' días)</b></span>';
                                            if(intval($dias) <= 0)
                                              $estado = '<span style="color: red"><b>Vencido (venció hace '.$dias.' días)</b></span>';
                                            echo "
                                              <tr>
                                                <td>GPS</td>
                                                <td>".mostrar_fecha($value['tb_poliza_fecini'])."</td>
                                                <td>".mostrar_fecha($value['tb_poliza_fecfin'])."</td>
                                                <td>".$estado."</td>
                                                <td>".$value['tb_credito_vehpla']."</td>
                                                <td>GARVEH-".$value['tb_credito_id']."</td>
                                              </tr>
                                            ";
                                          }
                                        }
                                      $result = NULL;
                                    ?>
                                  </tbody>
                                </table>
                              </div>

                            </div>
                          </div>
                        </div>
                      <?php endif; ?>

                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Datos del Crédito</h3>
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="box-body no-padding">
                          <div class="row">
                            <div class="col-lg-2">
                              <div class="form-group">
                                <label for="cmb_cuo_tip" class="control-label">Tipo de Cuota :</label>
                                <select class="form-control input-sm" name="cmb_cuo_tip" id="cmb_cuo_tip">
                                  <option value="">- </option>
                                  <option value="3">LIBRE</option>
                                  <option value="4">FIJA</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label for="cmb_cre_tipref" class="control-label">Tipo Refinanciado :</label>
                                <select class="form-control input-sm" name="cmb_cre_tipref" id="cmb_cre_tipref">
                                  <option value="">- </option>
                                  <option value="5">REFINANCIADO AMORTIZADO</option>
                                  <option value="6">REFINANCIADO</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-2">
                              <div class="form-group">
                                <label for="cmb_cre_per" class="control-label">Periodo :</label>
                                <select class="form-control input-sm" name="cmb_cre_per" id="cmb_cre_per">
                                  <option value="4">MENSUAL</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-5">
                              <div class="row">
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="cmb_cre_subper" class="control-label">Sub Periodo :</label>
                                    <select class="form-control input-sm" name="cmb_cre_subper" id="cmb_cre_subper">
                                      <option value="1">MENSUAL</option>
                                      <option value="2">QUINCENAL</option>
                                      <option value="3">SEMANAL</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-4">
                                  <div class="row">
                                    <div class="col-lg-10">
                                      <div class="form-group">
                                        <label for="cmb_mon_id" class="control-label">Moneda :</label>
                                        <select class="form-control input-sm" name="cmb_mon_id" id="cmb_mon_id" disabled>
                                          <?php 
                                            $moneda_id = $credito_matriz_moneda_id;
                                            require_once('../moneda/moneda_select.php'); 
                                          ?>
                                        </select>
                                      </div>
                                    </div>
                                    <!-- <div class="col-lg-2">
                                                  <a class="btn btn-primary btn-xs" title="Agregar" onclick="monedacambio_form('insertar')"><i class="fa fa-plus"></i></a>
                                                </div>-->
                                  </div>
                                </div>
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="txt_cre_tipcam" class="control-label">Cambio :</label>
                                    <input type="text" name="txt_cre_tipcam" id="txt_cre_tipcam" class="form-control input-sm moneda" value="" readonly>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-2">
                              <div class="form-group">
                                <label for="txt_cre_preaco" class="control-label">Precio Acordado :</label>
                                <input type="text" name="txt_cre_preaco" id="txt_cre_preaco" class="form-control input-sm moneda2" value="<?php echo formato_moneda($SUMA_GLOBAL_A_PAGAR) ?>">
                              </div>
                            </div>
                            
                            <div class="col-lg-2">
                              <div class="form-group">
                                <label for="txt_cre_amor" class="control-label">Amortización :</label>
                                <input type="text" name="txt_cre_amor" id="txt_cre_amor" class="form-control input-sm moneda2" value="<?php echo formato_moneda($AMORTIZACION_TOTAL_PREVIO) ?>" readonly>
                              </div>
                            </div>
                            <div class="col-lg-2">
                              <div class="form-group" style="margin-top: 4px">
                                <label for="txt_cre_amor" class="control-label"> </label><br>
                                <a class="btn btn-primary btn-sm" title="Amortizar" onclick="amortizar_form('amortizar')"><i class="fa fa-plus"></i> Amortizar</a>
                                <a class="btn btn-primary btn-sm" title="Liquidar" onclick="amortizar_form('liquidar')"><i class="fa fa-plus"></i> Liquidar</a>
                              </div>
                            </div>
                            
                            <div class="col-lg-6">
                              <div class="row">
                                <div class="col-lg-3">
                                  <div class="form-group">
                                    <label for="txt_cre_int" class="control-label">Interés (%) :</label>
                                    <input type="text" name="txt_cre_int" id="txt_cre_int" class="form-control input-sm porcentaje" value="">
                                  </div>
                                </div>
                                <div class="col-lg-3">
                                  <div class="form-group">
                                    <label for="txt_cre_tecm" class="control-label">TCEM (%) :</label>
                                    <input type="text" name="txt_cre_tecm" id="txt_cre_tecm" class="form-control input-sm porcentaje" value="" readonly>
                                  </div>
                                </div>
                                <div class="col-lg-3">
                                  <div class="form-group">
                                    <label for="txt_cre_numcuo" class="control-label">N° Cuotas :</label>
                                    <input type="text" name="txt_cre_numcuo" id="txt_cre_numcuo" class="form-control input-sm porcentaje" value="">
                                  </div>
                                </div>
                                <div class="col-lg-3">
                                  <div class="form-group">
                                    <label for="txt_cre_linapr" class="control-label">Línea Aprobada :</label>
                                    <input type="text" name="txt_cre_linapr" id="txt_cre_linapr" class="form-control input-sm moneda2" value="" readonly>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-5">
                              <label for="txt_cre_obs" class="control-label">Observación :</label>
                              <div class="form-group">

                                <textarea class="form-control mayus" name="txt_cre_obs" id="txt_cre_obs" rows="2" cols="35"></textarea>
                              </div>
                            </div>
                            <div class="col-lg-4">
                              <div class="form-group">
                                <label for="cmb_cuedep_id" class="control-label">Cuenta a Depositar :</label>
                                <select class="form-control input-sm" name="cmb_cuedep_id" id="cmb_cuedep_id">
                                  <?php require_once('../cuentadeposito/cuentadeposito_select.php'); ?>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3" id="cre_fijo">
                              <div class="form-group">
                                <label for="txt_credito_montoche" class="control-label">CHEK PARA AGREGAR: <?php echo $credito_gps;?></label><br/>
                                <label>
                                  AGREGAR GPS: <input class="flat-green" type="checkbox" name="che_gps" id="che_gps" value="2" <?php if(intval($credito_gps) > 0) echo 'checked="true"'; ?> />
                                </label>
                              </div>
                              <input type="hidden" id="hdd_seguro_porcentaje" name="hdd_seguro_porcentaje"/>
                              <input type="hidden" id="hdd_gps_precio" name="hdd_gps_precio"/>
                              <input type="hidden" id="hdd_gps_simple_id" name="hdd_gps_simple_id" />
                              <span class="badge bg-green" id="span_seguro_gps"></span>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2">
                              <div class="form-group">
                                <label for="">Fecha de Credito :</label>
                                <div class="input-group">
                                  <div class='input-group date' id='datetimepicker5'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_feccre" id="txt_cre_feccre" value="<?php echo mostrar_fecha($cre_feccre); ?>" readonly />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <label for="">Fecha de Desembolso :</label>
                                <div class="input-group">
                                  <div class='input-group date' id='datetimepicker6'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecdes" id="txt_cre_fecdes" value="<?php echo mostrar_fecha($cre_fecdes); ?>" readonly />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <label for="">Fecha de Facturación :</label>
                                <div class="input-group">
                                  <div class='input-group date' id='datetimepicker7'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecfac" id="txt_cre_fecfac" value="<?php echo mostrar_fecha($cre_fecfac); ?>" readonly />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <label for="">Fecha de Prorrateo :</label>
                                <div class="input-group">
                                  <div class='input-group date' id='datetimepicker8'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecpro" id="txt_cre_fecpro" value="<?php echo mostrar_fecha($cre_fecpro); ?>" readonly />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <label for="">Fecha de Entrega :</label>
                                <div class="input-group">
                                  <div class='input-group date' id='datetimepicker9'>
                                    <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecent" id="txt_cre_fecent" value="<?php echo mostrar_fecha($cre_fecent); ?>" readonly />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div id="msj_credito_ref" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px;"></div>
                      <div id="div_credito_cronograma"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-info" id="btn_guardar_creditogarveh">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>

    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/refinanciar/refinanciar_form.js?ver=1112'; ?>"></script>