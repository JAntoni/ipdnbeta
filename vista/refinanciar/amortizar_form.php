<?php

require_once ("../../core/usuario_sesion.php");
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();

require_once("../funciones/fechas.php");
require_once("../funciones/funciones.php");

$action = $_POST['action'];
$fecha_hoy = date('d-m-Y');
$cre_id = $_POST['cre_id'];
$cre_tip = $_POST['cre_tip'];
$mon_id = $_POST['mon_id'];
$prorrateo = $_POST['prorrateo'];
$capital = $_POST['capital'];
$detalle_amortizacion = $_POST['detalle_amortizacion'];

$mon_nombre = 'S/.';
if($mon_id == 2)
  $mon_nombre = 'US$.';
if($cre_tip == 2){
  require_once ("../creditoasiveh/Creditoasiveh.class.php");
  $oCredito = new Creditoasiveh();
  $codigo = 'CRÉDITO ASIVEH CAV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
}
if($cre_tip == 3){
  require_once ("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
  $codigo = 'CRÉDITO GARVEH CGV-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
}
if($cre_tip == 4){
  require_once ("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();
  $codigo = 'CRÉDITO HIPOTECARIO CH-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT);
}

if($action == "amortizar")
  $concepto = 'INGRESO POR AMORTIZACIÓN.';

if($action == "liquidar")
  $concepto = 'INGRESO POR LIQUIDACIÓN TOTAL DEL CRÉDITO.';

$dts = $oCredito->mostrarUno($cre_id);
if($dts['estado']==1){
  $mon_id = $dts['data']['tb_moneda_id'];
  $cli_id = $dts['data']['tb_cliente_id'];
  $cliente = $dts['data']['tb_cliente_nom'];
}
$dts = null;

//formato_moneda -> devuelve un flot con dos decimales sin comas
$amorti_prev = 0; //amortización previo
$mod_id = $cre_tip; //el modulo en INGRESO para amortizacion es 1 menor, 2 asve, 3 garve, 4 hipo
$modide = $cre_id; //el modide en ingreso para amortización es el ID  del crédito

$dts = $oIngreso->mostrar_por_modulo($mod_id,$modide,$mon_id, 1); // parametro 1 ya que es el estado del ingreso 1 activo
if($dts['estado']==1){
    foreach ($dts['data'] as $key => $value) {
        $amorti_prev += $value['tb_ingreso_imp'];
    }
}
$dts = null;

$amorti_prev = formato_moneda($amorti_prev); //amortización previo a este crédito
$capital = formato_moneda($capital);
$saldo_capital = formato_moneda($capital - $amorti_prev);
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_credito_amortizar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $concepto;?></h4>
        </div>
        <form id="for_amortizar">
          <input type="hidden" name="hdd_action" value="<?php echo $action;?>">
          <input type="hidden" name="hdd_amor_mon_id" id="hdd_amor_mon_id" value="<?php echo $mon_id;?>">
          <input type="hidden" name="hdd_amor_det" value="<?php echo $codigo;?>">
          <input type="hidden" name="hdd_cli_id" value="<?php echo $cli_id;?>">
          <input type="hidden" name="hdd_cre_id" id="hdd_cre_id" value="<?php echo $cre_id;?>">
          <input type="hidden" name="hdd_cre_tip" value="<?php echo $cre_tip;?>">
          <input type="hidden" name="hdd_prorrateo" id="hdd_prorrateo" value="<?php echo $prorrateo; ?>">
          <input type="hidden" name="moneda_deposito" id="moneda_deposito">
          <input name="hdd_detalle_refinanciar" type="hidden" value="<?php echo $detalle_amortizacion;?>">

          <div class="modal-body" style="font-family: cambria">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="col-sm-4 control-label" style="text-align: right">Forma de Pago :</label>
                        <div class="col-md-5">
                          <select name="cmb_forma_pag" id="cmb_forma_pag" class="form-control input-sm">
                            <option value="">--</option>
                            <option value="1" selected="true">En Oficinas</option>
                            <option value="2">Pago en Banco</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="" class="col-sm-7 control-label" style="text-align: right">Tipo Cambio Del Día :</label>
                                    <div class="col-md-3">
                                        <input type="text" name="txt_amor_cuopag_tipcam" id="txt_amor_cuopag_tipcam" class="form-control input-sm numero" value="<?php echo $cliente_doc; ?>" readonly>
                                    </div> 
                                </div>    
                            </div>
                            <div class="col-md-2">
                            </div> 
                        </div>
                    </div> 
                </div>    
            </div>
            <div class="col-md-6">
                <!--<h5 style="font-weight: bold;font-family: cambria">Datos Generales</h5>-->
                <div class="panel panel-primary">
                    <div class="panel-heading" style="font-weight: bold;font-family: cambria;font-size: 15px">Datos Generales</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Cliente:</label>
                                        <div class="col-md-7">
                                          <input type="text" class="form-control input-sm" name="td_cliente" id="td_cliente" value="<?php echo $cliente?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Crédito:</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control input-sm" name="td_codigo" id="td_codigo" value="<?php echo $codigo?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Concepto :</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control input-sm" name="td_concepto" id="td_concepto" value="<?php echo $concepto?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Capital Total (<?php echo $mon_nombre?>) :</label>
                                        <div class="<?php if($action=="liquidar"){ echo "col-md-6";}else{echo "col-md-5";}?>">
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm moneda3" name="txt_capital_tot" id="txt_capital_tot" value="<?php echo formato_moneda($capital)?>" readonly>
                                                <?php if($action=="liquidar"){?>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-primary btn-sm" onclick="Editar()" title="Editar Interés">
                                                        <span class="fa fa-check-square-o"></span>
                                                    </button>
                                                </span>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Amortización Previa :</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control input-sm moneda3" name="txt_amort_pre" id="txt_amort_pre" value="<?php echo formato_moneda($amorti_prev)?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Saldo Total :</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control input-sm moneda3" name="txt_capital" id="txt_capital" value="<?php echo formato_moneda($saldo_capital)?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Fecha de Amortización :</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control input-sm" name="txt_cuopag_fec" id="txt_cuopag_fec" value="<?php echo mostrar_fecha($fecha_hoy)?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="cmb_amor_mon_id" class="col-sm-5 control-label">Moneda :</label>
                                        <div class="col-md-5">
                                            <select class="form-control input-sm mayus" name="cmb_amor_mon_id" id="cmb_amor_mon_id">
                                                <option value="1" <?php if($mon_id == 1) echo 'selected';?>>S/.</option>
                                                <option value="2" <?php if($mon_id == 2) echo 'selected';?>>US$</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row div_amor_tipocambio" style="display: none;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Monto :</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control input-sm moneda2" name="txt_cuopag_moncam" id="txt_cuopag_moncam" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Monto Pago (<?php echo $mon_nombre?>) :</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control input-sm moneda3" name="txt_cuopag_mon" id="txt_cuopag_mon" value="<?php if($action=="liquidar"){ echo $saldo_capital;}?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Paga Con (<?php echo $mon_nombre?>) :</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control input-sm moneda3" name="txt_paga_con" id="txt_paga_con" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="cmb_cuopag_vuel" class="col-sm-5 control-label">Dar Vuelto En :</label>
                                        <div class="col-md-5">
                                            <select class="form-control input-sm mayus" name="cmb_cuopag_vuel" id="cmb_cuopag_vuel">
                                                <option value="0">-</option>
                                                <option value="1">S/.</option>
                                                <option value="2">US$</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="tr_vuelto" style="display: none;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 control-label">Total Vuelto :</label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control input-sm moneda3" name="txt_vuelto" id="txt_vuelto" value="" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                         <div class="panel-heading" style="font-weight: bold;font-family: cambria;font-size: 15px">Llenar en caso sea un depósito</div>
                        <div class="panel-body"  id="banco">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-md-6 control-label">Fecha Depósito:</label>
                                        <div class="col-md-6">
                                            <div class='input-group date' id='cuota_fil_picker1'>
                                                <input type="text" class="form-control input-sm" name="txt_cuopag_fecdep" id="txt_cuopag_fecdep" readonly value="<?php echo mostrar_fecha($fecha_hoy)?>">
                                                <span class="input-group-addon">
                                                  <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<p>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-6 control-label">Fecha Validación:</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" name="txt_cuopag_fec" id="txt_cuopag_fec" value="<?php echo mostrar_fecha($fecha_hoy)?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<p>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-6 control-label">N° Operación:</label>
                                        <div class="col-md-6">
                                            <input type="text"  class="form-control input-sm" name="txt_cuopag_numope" id="txt_cuopag_numope">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<p>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-6 control-label">Monto Depósito:</label>
                                        <div class="col-md-6">
                                            <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_mon_dep" type="text" id="txt_cuopag_mon_dep" size="12" maxlength="10">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-6 control-label">Comisión Banco:</label>
                                        <div class="col-md-6">
                                            <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_comi" type="text" id="txt_cuopag_comi" size="12" maxlength="10" value="0.00">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-6 control-label">Monto Validar:</label>
                                        <div class="col-md-6">
                                            <input style="text-align:right; font-size: 9pt;" class="form-control input-sm moneda3" name="txt_cuopag_monval" type="text" id="txt_cuopag_monval" size="12" maxlength="10" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-6 control-label">N° Cuenta:</label>
                                        <div class="col-md-6">
                                            <select name="cmb_cuedep_id_banco" id="cmb_cuedep_id_banco" class="form-control input-sm">
                                                <?php require_once '../cuentadeposito/cuentadeposito_select.php';?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-6 control-label">En Banco:</label>
                                        <div class="col-md-6">
                                            <select name="cmb_banco_id"  class="form-control input-sm mayus">
                                                <option value="">--</option>
                                                <option value="1" selected="true">Banco BCP</option>
                                                <option value="2">Banco Interbak</option>
                                                <option value="3">Banco BBVA</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <strong style="color: green;margin-left:20px;">* Si el ingreso es mediante Banco, el campo de Monto Validar debe ser igual al Monto Pago de la columna de la izquierda *</strong>
                </div> 
            </div> 
          </div>    
          <div id="msj_amortizar" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-info" id="btn_guardar_creditogarveh">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/refinanciar/amortizar_form.js?ver=112233';?>"></script>
