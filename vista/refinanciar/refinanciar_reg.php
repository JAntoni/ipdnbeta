<?php
require_once ("../../core/usuario_sesion.php");

require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once('../cliente/Cliente.class.php');
$oCliente = new Cliente();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();

$usuario_id = intval($_SESSION['usuario_id']);

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$cre_tip = $_POST['hdd_cre_tip']; //tipo del credito 2 asiveh, 3 garveh y 4 hipo
$cre_cod = ''; $credito_nom = '';
if($cre_tip == 2){
  require_once ("../creditoasiveh/Creditoasiveh.class.php");
  $oCredito = new Creditoasiveh();
  $cre_cod = 'CAV';
  $credito_nom = 'Crédito ASIVEH';
  $creditotipo_id = 2; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
}
if($cre_tip == 3){
  require_once ("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
  $cre_cod = 'CGV';
  $credito_nom = 'Crédito GARVEH';
  $creditotipo_id = 3; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
}
if($cre_tip == 4){
  require_once ("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();
  $cre_cod = 'CH';
  $credito_nom = 'Crédito HIPOTECARIO';
  $creditotipo_id = 4; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
}

if($_POST['action_credito']=='insertar'){
  $modulo = $_POST['modulo'];
  $cre_id = $_POST['hdd_cre_id']; //del credito original creamos otro credito refinanciado
  $fec_gps = $_POST['txt_cre_fecgps'];
  $fec_str = $_POST['txt_cre_fecseg'];
  $fec_soat = $_POST['txt_cre_fecren'];
  $fec_iv = $_POST['txt_cre_fpagimp'];
  $tip_ref = $_POST['cmb_cre_tipref']; //tipo de refinanciamiento: 5 refinanciado amortizado, 6 refinanciado sin amortizar
  $subper = $_POST['cmb_cre_subper'];
  $mon_id = $_POST['hdd_mon_id'];
  $tip_cam = $_POST['txt_cre_tipcam'];
  $preaco = $_POST['txt_cre_preaco'];
  $amor = $_POST['txt_cre_amor']; //monto que amortiza el cliente
  $int = $_POST['txt_cre_int'];
  $numcuo = $_POST['txt_cre_numcuo']; 
  $linapr = $_POST['txt_cre_linapr']; 
  $obs = $_POST['txt_cre_obs'];
  if($modulo == 'multiple')
    $obs = 'Refinanciación Múltiple de las Adendas N° ('.$_POST['hdd_adendas_id'].') | '.$_POST['txt_cre_obs'];
  $feccre = $_POST['txt_cre_feccre']; 
  $fecdes = $_POST['txt_cre_fecdes']; 
  $fecfac = $_POST['txt_cre_fecfac']; 
  $fecpro = $_POST['txt_cre_fecpro']; 
  $usureg = $_SESSION['usuario_id'];
  $cuotip_id = intval($_POST['cmb_cuo_tip']);

  if(empty($cre_id) || empty($linapr) || empty($preaco)){
    $data['cre_id'] = 0;
    $data['msj'] = 'El id del crédito o la línea aprobada no son válidas';
    echo json_encode($data);
    exit();
  }
  
  $dts = $oCredito->mostrarUno($cre_id);
    if($dts['estado']==1){
      $sub_cre_tip = intval($dts['data']['tb_credito_tip']);
      if($cre_tip == 2)
        $sub_cre_tip = intval($dts['data']['tb_credito_tip1']);
    }
  $dts = null;
  
  $cre_id_new = 0;
  //es un credito nuevo basado en un credito anterior
  if($cre_tip == 2 || $cre_tip == 3){
    $result = $oCredito->mostrarUno($cre_id);
    if($result['estado'] == 1){
        $oCredito->credito_cus =$result['data']['tb_credito_cus'];
        $oCredito->cliente_id =$result['data']['tb_cliente_id'];
        $oCredito->moneda_id =$result['data']['tb_moneda_id'];
        $oCredito->representante_id =$result['data']['tb_representante_id'];
        $oCredito->cuentadeposito_id =$result['data']['tb_cuentadeposito_id'];
        $oCredito->vehiculomarca_id =$result['data']['tb_vehiculomarca_id'];
        $oCredito->vehiculomodelo_id =$result['data']['tb_vehiculomodelo_id'];
        $oCredito->vehiculoclase_id =$result['data']['tb_vehiculoclase_id'];
        $oCredito->vehiculotipo_id =$result['data']['tb_vehiculotipo_id'];
        $oCredito->credito_vehpla =$result['data']['tb_credito_vehpla'];
        $oCredito->credito_vehsermot =$result['data']['tb_credito_vehsermot'];
        $oCredito->credito_vehsercha =$result['data']['tb_credito_vehsercha'];
        $oCredito->credito_vehano =$result['data']['tb_credito_vehano'];
        $oCredito->credito_vehcol =$result['data']['tb_credito_vehcol'];
        $oCredito->credito_vehnumpas =$result['data']['tb_credito_vehnumpas'];
        $oCredito->credito_vehnumasi =$result['data']['tb_credito_vehnumasi'];
        $oCredito->credito_vehkil =$result['data']['tb_credito_vehkil'];
        $oCredito->credito_vehest =$result['data']['tb_credito_vehest'];
        $oCredito->credito_vehent =$result['data']['tb_credito_vehent'];
        $oCredito->credito_gas =$result['data']['tb_credito_gas'];
        $oCredito->credito_vehpre =$result['data']['tb_credito_vehpre'];
        $oCredito->credito_fecent =$result['data']['tb_credito_fecent'];
        $oCredito->credito_pla =$result['data']['tb_credito_pla'];
        $oCredito->credito_comdes =$result['data']['tb_credito_comdes'];
        $oCredito->credito_webref =$result['data']['tb_credito_webref'];
        $oCredito->credito_parele =$result['data']['tb_credito_parele'];
        $oCredito->credito_solreg =$result['data']['tb_credito_solreg'];
        $oCredito->credito_fecsol =$result['data']['tb_credito_fecsol'];
        $oCredito->credito_tipus =$result['data']['tb_credito_tipus'];
        $oCredito->credito_tip =$result['data']['tb_credito_tip'];
        $oCredito->credito_valtas =$result['data']['tb_credito_valtas'];
        $oCredito->credito_valrea =$result['data']['tb_credito_valrea'];
        $oCredito->credito_valgra =$result['data']['tb_credito_valgra'];
        $oCredito->credito_vehmode =$result['data']['tb_credito_vehmode'];
        $oCredito->credito_vehcate =$result['data']['tb_credito_vehcate'];
        $oCredito->credito_vehcomb =$result['data']['tb_credito_vehcomb'];
        $oCredito->credito_numtit =$result['data']['tb_credito_numtit'];
        $oCredito->zonaregistral_id =$result['data']['tb_zonaregistral_id'];
    }
    $result = null;
    $result = $oCredito->registrar_refinanciado($cre_id, fecha_mysql($fec_gps), fecha_mysql($fec_str), fecha_mysql($fec_soat), fecha_mysql($fec_iv), $tip_ref, $subper, $tip_cam, moneda_mysql($preaco), moneda_mysql($amor), moneda_mysql($int), $numcuo, moneda_mysql($linapr), $obs, fecha_mysql($feccre), fecha_mysql($fecdes), fecha_mysql($fecfac), fecha_mysql($fecpro), $usureg, $cuotip_id);
      if(intval($result['estado']) == 1){
        $cre_id_new = $result['credito_id'];
      }
    $result = null;
    
    //? EVALUAMOS SI SE HA REGISTRADO UN GPS PARA EL CREDITO, Y ASÍ PODER ACTIVAR EL XAC DEL GPS EN LA BD
    $poliza_id = intval($_POST['hdd_gps_simple_id']);

    if($poliza_id > 0){
      $oPoliza->modificar_campo($poliza_id, 'tb_poliza_xac', 1, 'INT'); // el GPS se crea en xac = 0 para luego actulizarla con el ID del crédito resultante
      $oPoliza->modificar_campo($poliza_id, 'tb_credito_id', $cre_id, 'INT'); // el GPS se crea en xac = 0 para luego actulizarla con el ID del crédito resultante
    }
  }


  $moneda = 'S/.';
  $tipo_refi = 'REFINANCIADO SIN AMORTIZAR';

  if($mon_id == 2)
    $moneda = 'US$';
  if($tip_ref == 5)
    $tipo_refi = 'REFINANCIADO CON AMORTIZACIÓN (MONTO '.$moneda.' '.$amor.')';
  
  //? GUARDAMOS EL HISTORIAL DE LA LIQUIDACION DEL CRÉDITO, TODA LA TABLA DE DETALLE LO GUARDAMOS EN LA ESTRELLA
  $detalle_refinanciar = $_POST['hdd_detalle_refinanciar'].'<br>';
  //$oCredito->registro_historial($cre_id, $detalle_refinanciar); //guardamos en el crédito ORIGINAL
  $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $detalle_refinanciar);

  //registramos el historial para el credito nuevo creado
  $his = '<span>Crédito refinanciado tipo: '.$tipo_refi.', creado por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: este crédito refinanciado ha sido creado en base al crédito matriz de ID: '.$cre_cod.'-'.$cre_id.'. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
  //$oCredito->registro_historial($cre_id_new, $his);
  $oCreditolinea->insertar($creditotipo_id, $cre_id_new, $usuario_id, $his);

  $line_det = '<b>'.$_SESSION['usuario_nom'].'</b> ha refinanciado el '.$credito_nom.' de número: '.$cre_cod.'-'.str_pad($cre_id, 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';
  $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
  $oLineaTiempo->tb_lineatiempo_det = $line_det;
  $oLineaTiempo->insertar();

  if($sub_cre_tip != 2 && $modulo != 'multiple'){
    //listamos las adendas del crédipo para resolverlas, cuando se hace un refinanciar de una adenda, esta adenda no tiene otras adendas, por ello solo se lista para creditos pero no para adendas
    $dts = $oCredito->listar_adendas_credito($cre_id);
      if($dts['estado']==1){
        foreach ($dts['data'] as $key => $value) {
          $aden_id = $value['tb_credito_id'];
          $est=8;
          $oCredito->aprobar($aden_id, $_SESSION['usuario_id'], $est);
          $oCredito->modificar_campo($aden_id, 'tb_credito_fecresol', date('Y-m-d'),'STR');

          //registramos el historial para el credito
          $his = '<span style="color: #F56B02;">Adenda Resuelta por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: ha sido resuelta ya que su saldo capital se ha sumado al crédito nuevo refinanciado, ID Crédito Nuevo: '.$cre_cod.'-'.$cre_id_new.'. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
          //$oCredito->registro_historial($aden_id, $his);
          $oCreditolinea->insertar($creditotipo_id, $aden_id, $usuario_id, $his);
        }
      }
    $dts = null;
  }

  //resolvemos el crédito matriz
  $est=8;
  if($modulo != 'multiple'){
    $oCredito->aprobar($cre_id, $_SESSION['usuario_id'], $est);
    $oCredito->modificar_campo($cre_id, 'tb_credito_fecresol', date('Y-m-d'),'STR');
    //registramos el historial para el credito matriz
    $his = '<span style="color: #F56B02;">Crédito Resuelto por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: ha sido resuelto este crédito ya que el cliente decidió refinanciarlo, ID Crédito Nuevo: '.$cre_cod.'-'.$cre_id_new.'. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
    //$oCredito->registro_historial($cre_id, $his);
    $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his);
  }
  
  if($modulo == 'multiple'){
    $adendas = $_POST['hdd_adendas_id'];
    $array_adendas = explode(',', $adendas);

    for ($i = 0; $i < count($array_adendas); $i++) {
      $cre_id = $array_adendas[$i];
      $oCredito->aprobar($cre_id, $_SESSION['usuario_id'], $est); // estado 8 resuelto a las adendas
      $oCredito->modificar_campo($cre_id, 'tb_credito_fecresol', date('Y-m-d'),'STR');
      //registramos el historial para el credito matriz
      $his = '<span style="color: #F56B02;">Crédito Resuelto por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: ha sido resuelto este crédito ya que el cliente decidió refinanciarlo. Refinanciación Múltiple de las Adendas N° ('.$_POST['hdd_adendas_id'].'). ID Crédito Nuevo: '.$cre_cod.'-'.$cre_id_new.'. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
      //$oCredito->registro_historial($cre_id, $his);
      $oCreditolinea->insertar($creditotipo_id, $cre_id, $usuario_id, $his);
    }
  }

  $cambio = 1;
  $dts = $oMonedacambio->consultar(fecha_mysql(date('d-m-Y')));
    if($dts['estado']==1){
      $cambio = floatval($dts['data']['tb_monedacambio_val']);
    }
  $dts = null;

  //?VALORES DEL % DEL SEGURO Y PRECIO DEL GPS POR TODO EL TIEMPO DEL CRÉDITO
  $valor_seguro = formato_moneda($_POST['hdd_seguro_porcentaje']);
  $valor_gps = formato_moneda($_POST['hdd_gps_precio']);
  $estado_gps = intval($_POST['che_gps']); // si es 0 no se eligió GPS para el crédito

  //?REGISTRAMOS LOS VALORES DEL % DEL SEGURO Y EL VALOR DEL GPS
  $oCredito->modificar_campo($cre_id_new, 'tb_credito_preseguro', $valor_seguro, 'STR');
  if($estado_gps > 0)
    $oCredito->modificar_campo($cre_id_new, 'tb_credito_pregps', $valor_gps, 'STR');

  //CUOTAS
  $mon_id = $_POST['hdd_mon_id'];
  $C = moneda_mysql($preaco);
  $i = moneda_mysql($int);
  $n = $numcuo;
  $R = $C / $n;

  if($mon_id == 1)
    $valor_gps = formato_moneda($valor_gps * $cambio); // el gps tiene precio en dolares si el cronograma es en soles se genera el tipo de cambio
    
  $gps_mensual = 0;
  $seguro_mensual = 0;

  if(floatval($valor_seguro) > 0)
    $i += $valor_seguro;
  if(intval($estado_gps) <= 0)
    $valor_gps = 0;
  if(floatval($valor_gps) > 0 && $n > 0)
    $gps_mensual = formato_moneda($valor_gps / $n);

  //? ACTUALIZAMOS EL VALOR NUEVO DEL INTERÉS AL CRÉDITO
  $oCredito->modificar_campo($cre_id_new, 'tb_credito_int', $i, 'INT');

  if($cuotip_id == 3)
    $n = 1;
  
  if($i != 0){
    $uno = $i / 100;
    $dos = $uno + 1;
    $tres = pow($dos,$n);
    $cuatroA = ($C * $uno) * $tres;
    $cuatroB = $tres - 1;
    $R = $cuatroA / $cuatroB;
    $r_sum = $R*$n;
  }
  //para la formula del prorrateo, se toma la fecha del prorrateo y se cuenta cuántos días tiene el mes de la fecha del prorrateo, ejem: prorrateo 10/12/2016, el mes 12 tiene 31 días, para saber eso usamos la funcion de mktime
  //formula prorrateo = (interes de la primera cuota / total días del mes prorrateo) * dias prorrateo
  list($dia,$mes,$anio) = explode('-',$fecpro);
  $dias_mes = date('t', mktime(0, 0, 0, $mes, 1, $anio));
  $fecha_fac = $fecfac; //antes era con fecha de desembolso ahora será con fecha de facturacion
  list($day, $month, $year) = explode('-',$fecha_fac);
  $fec_desem1 = new DateTime($fecdes); //fecha de desembolso del dinero
  $fec_prorra1 = new DateTime($fecpro);//fecha de prorrateo si es que se va a cobrar
  $dias_prorra = $fec_desem1->diff($fec_prorra1)->format("%a");//diferencia de días

  $subperiodo = 1;
  if($_POST['cmb_cre_subper'] == 2)
  	$subperiodo = 2;
  if($_POST['cmb_cre_subper'] == 3)
  	$subperiodo = 4;
  
  $monto_pro = 0; //esta variable captura el onto del prorrateo para ser guardado en el credito refinanciado
  
  for ($j=1; $j <= $n; $j++){ 
    if($j>1)
    {
      $C = $C - $amo;
      $int = $C*($i/100);
      $amo = $R - $int;
    }
    else
    {
      $int = $C*($i/100);
      $amo = $R - $int;
    } 

    $seguro_mensual = $C*($valor_seguro/100);

    //fecha facturacion
    $month = $month + 1;
    if($month == '13'){
      $month = 1;
      $year = $year + 1;
    }
    if($j == 1){
      $int_pro = ($int / $dias_mes) * $dias_prorra;
      $monto_pro = $int_pro;
      $pro_div_cuo = $int_pro / $n;
      //el prorrateo se divide sobre numero de cuotas, ese monto se asigna a la cuota, pero la cuota tiene subcuotas: 1 si es mensual, 2 si es quincena y 4 si es semanal, entonces el prorrateo para las subcuotas debe ser el prorrateo sobre (numero de cuotas x el subperiodo)
      $pro_div_subcuo = $int_pro / ($n * $subperiodo);
    }
    else
      $int_pro = 0;
    $fecha=validar_fecha_facturacion($day,$month,$year);
    $xac = 1;
    $cretip_id = $cre_tip;//credito tipo 1=menor 2=asive 3=garve
    $est = 1;
    $persubcuo = 4;
    $l = 3;
    if($_POST['cmb_cre_subper'] == 2){
      $persubcuo = 2;
      $l = 2;
    }else{
      if($_POST['cmb_cre_subper'] == 1){
        $persubcuo = 1;
        $l = 0;
      }
    }
    
    $oCuota->credito_id = $cre_id_new;
    $oCuota->creditotipo_id = $cretip_id;
    $oCuota->moneda_id = $mon_id;
    $oCuota->cuota_num = $j;
    $oCuota->cuota_fec = fecha_mysql($fecha);
    $oCuota->cuota_cap = $C;
    $oCuota->cuota_amo = $amo;
    $oCuota->cuota_int = $int;
    $oCuota->cuota_cuo = ($R + $pro_div_cuo + $gps_mensual);
    $oCuota->cuota_pro = $pro_div_cuo;
    $oCuota->cuota_persubcuo = $persubcuo;
    $oCuota->cuota_est = $est;
    $oCuota->cuota_acupag = 0;
    $oCuota->cuota_interes = $i;
    $oCuota->cuota_preseguro = $seguro_mensual; 
    $oCuota->cuota_pregps = $gps_mensual;
    
    $result = $oCuota->insertar();
    if(intval($result['estado']) == 1){
        $cuo_id = $result['cuota_id'];
    }
    $result = null;
    
    for ($k=0; $k < $persubcuo; $k++) {
      list($day,$mon,$year) = explode('-',$fecha);
      $fecha_new=date('d-m-Y',mktime(0,0,0,$mon,$day-(7*$l),$year));

      $oCuotadetalle->tb_cuota_id = $cuo_id;
      $oCuotadetalle->tb_moneda_id = $mon_id;
      $oCuotadetalle->tb_cuotadetalle_num = $k+1;
      $oCuotadetalle->tb_cuotadetalle_fec = fecha_mysql($fecha_new);
      $oCuotadetalle->tb_cuotadetalle_cuo = ($R/$persubcuo + $pro_div_subcuo + $gps_mensual);
      $oCuotadetalle->tb_cuotadetalle_est = $est;
      $oCuotadetalle->tb_cuotadetalle_acupag = 0;
      
      $result = $oCuotadetalle->insertar();
    
      if($_POST['cmb_cre_subper'] == 2){
        $l = 0;
      }else{
        if($_POST['cmb_cre_subper'] != 1){
          $l--;
        }
      }
    }
    //$l = 3;
  }
  //agregamos el monto del prorrateo del credito ADENDA
  $oCredito->actualizar_monto_prorrateo($cre_id_new, moneda_mysql($monto_pro));

  $data['msj'] = 'El crédito refinanciado se ha creado correctamente.';
  $data['cre_id'] = $cre_id_new;
  $data['estado'] = 1;
  echo json_encode($data);
}

?>