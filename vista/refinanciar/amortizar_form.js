$(document).ready(function () {

  console.log("Cambios actualizas AL --28-10-2024");

  $("#for_amortizar").validate({
    submitHandler: function () {
      var mon_pag = Number($("#txt_cuopag_mon").autoNumeric("get"));
      var paga_con = Number($("#txt_paga_con").autoNumeric("get"));
      var monto_cambio = Number($("#txt_cuopag_moncam").autoNumeric("get"));
      var for_pag = parseInt($("#cmb_forma_pag").val());

      var cmb_amor_mon_id = parseInt($("#cmb_amor_mon_id").val()); //PAGAR CON MONEDA DIFERENTE AL CREDITO
      var moneda_id_credito = parseInt($("#hdd_amor_mon_id").val()); //moneda original del credito
      var moneda_cuenta_banco = parseInt($("#moneda_deposito").val());

      console.log("moneda original: " + moneda_id_credito + " // moneda seleccionada: " + cmb_amor_mon_id);

      if (for_pag == 2){
        if(moneda_cuenta_banco <= 0){
          alerta_warning("CUENTA BANCARIA", "Selecciona la cuenta bancaria del depósito");
          return false;
        }
        if(moneda_cuenta_banco != cmb_amor_mon_id){
          alerta_warning("CUENTA BANCARIA", "La Moneda de la cuenta bancaria debe ser igual a la Moneda de la izquierda");
          return false;
        }
      }

      if (for_pag == 1) {
        if (mon_pag == 0 || paga_con == 0 || mon_pag > paga_con) {
          alerta_warning(
            "ALERTA",
            "Los pagos no pueden ser 0 y el Paga Con debe ser mayor al Monto Pagado"
          );
          ///console.log('a pagar: ' + mon_pag + ' // paga con: ' + paga_con);
          return false;
        }
      }

      var mon_banco = Number($("#txt_cuopag_monval").autoNumeric("get"));

      if (cmb_amor_mon_id == moneda_id_credito && for_pag == 2 && mon_pag != mon_banco) {
        alerta_error(
          "TENER EN CUENTA CON LOS MONTOS",
          "Los montos de banco debe ser igual a los de la columna izquierda por favor."
        );
        return false;
      }

      if (cmb_amor_mon_id != moneda_id_credito && for_pag == 2 && monto_cambio != mon_banco) {
        alerta_error(
          "TENER EN CUENTA CON LOS MONTOS",
          "Los montos de banco debe ser igual a los de la columna izquierda por favor."
        );
        return false;
      }

      $.ajax({
        type: "POST",
        url: VISTA_URL + "refinanciar/amortizar_reg.php",
        async: true,
        dataType: "json",
        data: $("#for_amortizar").serialize(),
        beforeSend: function () {
          $("#msj_amortizar").html("Guardando...");
          $("#msj_amortizar").show(100);
        },
        success: function (data) {
          //console.log(data);
          if (parseInt(data.estado) == 1) {
            alerta_success(
              "EXITO",
              "Se ha amortizado correctamente el crédito del cliente: " +
                $("#td_cliente").text()
            );
            //se ha amortizado correctamente, el monto amortizado tenemos que restar al capital total, data.amor es el monto amortizado
            //localStorage.setItem("amortizado", data.amor);
            var cre_id = $("#hdd_cre_id").val();

            $("#msj_credito_ref").html(data.msj);
            $("#msj_credito_ref").show(70);
            $("#modal_credito_amortizar").modal("hide");
            //$('#modal_creditogarveh_refinanciar').modal('hide');
            cambiar_amortizacion(data.cre_id, 3);

            //refinanciar_form(cre_id, 1); // parametro 1 ya que puede ser cualqioer estado, porque no es está usando por el momento
          } else if (parseInt(data.estado) == 2) {
            //se ha liquidado totalmente el crédito
            $("#msj_credito").show(100);
            alerta_success(
              "EXITO",
              "Se ha liquidado por completo el crédito del cliente: " +
                $("#td_cliente").text()
            );
            $("#modal_credito_amortizar").modal("hide");
            //$('#modal_creditogarveh_refinanciar').modal('hide');
            cambiar_amortizacion(data.cre_id, 3);
          } else if (parseInt(data.estado) == 0) {
            //('#msj_amortizar').html(data.msj);
            alerta_error("ERROR", data.msj);
          } else alerta_error("ERROR", data.msj);
        },
        complete: function (data) {
          //console.log(data);
          if (data.statusText != "success")
            $("#msj_amortizar").html(data.responseText);
        },
      });
    },
    rules: {
      txt_cuopag_fec: {
        required: true,
      },
      txt_cuopag_mon: {
        required: function () {
          var for_pag = parseInt($("#cmb_forma_pag").val());
          if (for_pag == 1) return true;
          else return false;
        },
      },
      txt_paga_con: {
        required: function () {
          var for_pag = parseInt($("#cmb_forma_pag").val());
          if (for_pag == 1) return true;
          else return false;
        },
      },
      txt_cuopag_numope: {
        required: function () {
          var for_pag = parseInt($("#cmb_forma_pag").val());
          if (for_pag == 2) return true;
          else return false;
        },
      },
      txt_cuopag_mon_dep: {
        required: function () {
          var for_pag = parseInt($("#cmb_forma_pag").val());
          if (for_pag == 2) return true;
          else return false;
        },
      },
      txt_cuopag_comi: {
        required: function () {
          var for_pag = parseInt($("#cmb_forma_pag").val());
          if (for_pag == 2) return true;
          else return false;
        },
      },
      txt_cuopag_monval: {
        required: function () {
          var for_pag = parseInt($("#cmb_forma_pag").val());
          if (for_pag == 2) return true;
          else return false;
        },
      },
    },
    messages: {
      txt_cuopag_fec: {
        required: "*",
      },
      txt_cuopag_mon: {
        required: "*",
      },
      txt_paga_con: {
        required: "*",
      },
      txt_cuopag_numope: {
        required: "Ingrese número de Operación",
      },
      txt_cuopag_mon_dep: {
        required: "Ingrese el monto del deposito",
      },
      txt_cuopag_comi: {
        required: "Ingrese la comisión del banco",
      },
      txt_cuopag_monval: {
        required: "No debe estar vacío",
      },
    },
  });

  cambio_moneda_amor();
  $("#banco").find("input, textarea").attr("readonly", "readonly");

  $("#cmb_forma_pag").change(function (event) {
    var forma_pago = parseInt($("#cmb_forma_pag").val());
    if (forma_pago == 1) {
      $("#banco").find("input, textarea").attr("readonly", "readonly");
      //            $('#txt_paga_con').val('');
      //            $('#txt_paga_con').prop('readonly', true);
      //            $("#txt_paga_con").autoNumeric('set','');
      //            $("#txt_paga_con").attr('readonly','readonly');
    } else {
      $("#banco").find("input, textarea").attr("readonly", false);
    }
  });

  $("#txt_capital_tot").change(function (event) {
    var capital_total = Number($(this).autoNumeric("get"));
    $("#txt_capital").autoNumeric("set", capital_total.toFixed(2));
    $("#txt_cuopag_mon").autoNumeric("set", capital_total.toFixed(2));
  });

  // $('#txt_cuopag_mon_dep').change(function(event) {
  //     var monto = Number($(this).autoNumeric('get'));
  //     $('#txt_cuopag_mon').autoNumeric('set',monto.toFixed(2));
  //     $('#txt_paga_con').autoNumeric('set',monto.toFixed(2));
  //     $('#txt_cuopag_mon').prop('readonly',true);
  //     $('#txt_paga_con').prop('readonly',true);
  // });

  $("#cmb_forma_pag").change(function (event) {
    var forma = $(this).val();

    if (forma == 2) {
      //console.log("Estoy entrando a este campo");
      $("#banco").attr("disabled", false);
      $("#txt_cuopag_montot").attr("disabled", true);
    } else {
      $("#banco").attr("disabled", true);
      $("#txt_cuopag_montot").attr("disabled", false);
    }
  });
  $(".moneda2").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "999999.00",
  });
  $(".moneda3").autoNumeric({
    aSep: ",",
    aDec: ".",
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: "0.00",
    vMax: "9999999.99",
  });

  $("#cuota_fil_picker1").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });

  //    $('#txt_cuopag_fecdep').change(function(event) {
  //        var forma_pago = parseInt($('#cmb_forma_pag').val());
  //        if(forma_pago == 2){
  //            var fecha = $('#txt_cuopag_fecdep').val();
  //            calcular_datos_cuota_fechadeposito(fecha);
  //            calcular_mora(fecha);
  //        }
  //        else{
  //
  //        }
  //    });

  $("#cmb_amor_mon_id").change(function (e) {
    div_amor_tipocambio();
    cambio_moneda_amor(2); //sea soles o dolares siempre necesitamos el tipo de cambio en dolares

    var cre_mon_id = parseInt($("#hdd_amor_mon_id").val());
    var mon_select = parseInt($("#cmb_amor_mon_id").val());

    var tip_cam = Number($("#txt_amor_cuopag_tipcam").val());
    var monto_pagar = Number($("#txt_cuopag_mon").autoNumeric("get"));
    var res = 0;

    if (tip_cam <= 0 || monto_pagar <= 0) return false;

    if (cre_mon_id == 1 && mon_select == 2) {
      //si el credito es en soles pero se quiere pagar en dólares
      res = monto_pagar / tip_cam;
      $("#txt_cuopag_moncam").autoNumeric("set", res.toFixed(2));
    }
    if (cre_mon_id == 2 && mon_select == 1) {
      //si el credito es en dolares pero se quiere pagar en soles
      res = monto_pagar * tip_cam;
      $("#txt_cuopag_moncam").autoNumeric("set", res.toFixed(2));
    }
  });

  $("#txt_cuopag_moncam").change(function (event) {
    calcular_cambio_moneda_amor();
  });

  $("#cmb_cuopag_vuel").change(function (event) {
    var mon_select = $("#cmb_amor_mon_id").val();
    var mon_vuel = $(this).val();
    var tip_cam = Number($("#txt_amor_cuopag_tipcam").val());
    var mon_pago = Number($("#txt_cuopag_mon").autoNumeric("get"));
    var paga_con = Number($("#txt_paga_con").autoNumeric("get"));
    //solo se dará vuelto si es que paga en dolares y desea vuelto en soles
    if (paga_con <= mon_pago) {
      alerta_warning(
        "ALERTA",
        "Para dar vuelto el Paga Con debe ser mayor al Monto Pagado"
      );
      $(this).val("");
      return false;
    }
    if (mon_vuel == 0) {
      $("#tr_vuelto").hide();
      $("#txt_vuelto").val("");
      $(this).val("");
      return false;
    }
    if (mon_select == 2 && mon_vuel == 1) {
      var res = 0;
      res = paga_con - mon_pago;
      res = res * tip_cam;
      $("#tr_vuelto").show();
      $("#txt_vuelto").autoNumeric("set", res.toFixed(2));
    } else {
      alerta_warning(
        "ALERT",
        "Solo se puede dar vuelto en soles cuando se ingresa dólares"
      );
      $("#txt_vuelto").val("");
      $(this).val("");
    }
  });

  $("#txt_cuopag_mon_dep, #txt_cuopag_comi").change(function (event) {
    var monto_deposito = Number($("#txt_cuopag_mon_dep").autoNumeric("get"));
    var monto_comision = Number($("#txt_cuopag_comi").autoNumeric("get"));
    var cuenta_deposito = $("#cmb_cuedep_id_banco").val();
    var saldo = 0;

    saldo = monto_deposito - monto_comision;

    if (monto_deposito > 0 && monto_comision >= 0 && saldo > 0) {
      //$('#txt_cuopag_montot').autoNumericSet(saldo.toFixed(2));
      $("#txt_cuopag_monval").autoNumeric("set", saldo.toFixed(2));
    } else {
      //$('#txt_cuopag_montot').autoNumericSet(0);
      $("#txt_cuopag_monval").autoNumeric("set", 0);
    }
  });

  $("#txt_cuopag_cuo").focus();

  $("#cmb_cuedep_id_banco").change(function (event) {
    var tipo_moneda = parseInt($(this).find(":selected").attr("data-moneda"));
    $("#moneda_deposito").val(tipo_moneda);
  });

  $("#txt_cuopag_mon").change(function (event) {
    $("#txt_paga_con").val($(this).val());
    $("#txt_vuelto").val("");
    $("#cmb_cuopag_vuel").val("");
  });
  $("#txt_paga_con").change(function (event) {
    $("#txt_vuelto").val("");
    $("#cmb_cuopag_vuel").val("");
  });

  $("#txt_cuopag_moncam").change(function (event) {
    calcular_cambio_moneda_amor();
  });
});

function cambio_moneda_amor() {
  var monid = $("#cmb_amor_mon_id").val();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "monedacambio/monedacambio_reg.php",
    async: false,
    dataType: "json",
    data: {
      action: "obtener_dato",
      fecha: $("#txt_cuopag_fec").val(),
      mon_id: monid,
    },
    beforeSend: function () {},
    success: function (data) {
      console.log(data)
      $("#txt_amor_cuopag_tipcam").val(data.moncam_val);
    },
    complete: function () {
      // if($("#txt_cre_tipcam").val()!=""){
      //  $('#btn_cmb_amor_mon_id_2').hide();
      // }
    },
  });
}

function calcular_cambio_moneda_amor() {
  var mon_select = $("#cmb_amor_mon_id").val();
  var mon_id = $("#hdd_amor_mon_id").val();
  var tip_cam = $("#txt_amor_cuopag_tipcam").val();
  if (tip_cam == "" || parseInt(tip_cam) == 0) {
    alerta_warning("ALERTA", "Ingrese el tipo de cambio de moneda");
    $("#txt_cuopag_moncam").val("");
    return false;
  }

  var cambio = Number(
    $("#txt_cuopag_moncam")
      .val()
      .replace(/[^0-9\.]+/g, "")
  );
  var result = 0;
  if (mon_select == 1) {
    //ha seleccionado soles
    result = cambio / tip_cam;
    console.log(cambio + " / tip: " + tip_cam + " / res: " + result);
    $("#txt_cuopag_mon").autoNumeric("set", result.toFixed(2));
    $("#txt_paga_con").autoNumeric("set", result.toFixed(2));

    $("#txt_vuelto").val("");
    $("#cmb_cuopag_vuel").val("");
  } else {
    //ha seleccionado dolares
    result = cambio * tip_cam;
    $("#txt_cuopag_mon").autoNumeric("set", result.toFixed(2));
    $("#txt_paga_con").autoNumeric("set", result.toFixed(2));

    $("#txt_vuelto").val("");
    $("#cmb_cuopag_vuel").val("");
  }
}

function div_amor_tipocambio() {
  var mon_select = $("#cmb_amor_mon_id").val();
  var mon_id = $("#hdd_amor_mon_id").val();
  console.log("moooon: " + mon_id);
  if (mon_id != mon_select) {
    $(".div_amor_tipocambio").show();
  } else {
    $(".div_amor_tipocambio").hide();
  }
}

function Editar() {
  $("#txt_capital_tot").attr("readonly", false);
}
