$(document).ready(function() {
  console.log('calcuialas 33333');

  $("#form_refi").validate({
    submitHandler: function() {
      var poliza_id = parseInt($('#hdd_gps_simple_id').val()) || 0;
      console.log('poliza id: ' + poliza_id);

      if ($('#che_gps').is(':checked') && poliza_id <= 0){
        alerta_warning('IMPORTANTE', 'Has seleccionado el GPS para agregarlo al crédito pero aún no has llenado los datos, debes llenar los datos del GPS antes de guardar el crédito');

        return false;
      }
      
      $.ajax({
        type: "POST",
        url: VISTA_URL +"refinanciar/refinanciar_reg.php",
        async:true,
        dataType: "json",
        data: $("#form_refi").serialize(),
        beforeSend: function() {
          //alert($("#for_cre").serialize());
          //$('#btn_guar_ref').hide();
          $('#msj_credito_ref').html("Guardando registro...");
          $('#msj_credito_ref').show(100);
        },
        success: function(data){
          if(parseInt(data.estado) > 0){
            $('#msj_credito').html(data.msj);
            $('#msj_credito').show(100);
            $('#modal_creditogarveh_refinanciar').modal('hide');
            creditogarveh_tabla();
          }
          else
            $('#msj_credito_ref').html(data.msj);
        },
        complete: function(data){
          //console.log(data);
          if(data.statusText == 'success'){
            $('#div_refinanciar_form').empty();
            $('#btn_guar_ref').show();
          }
          else{
            $('#msj_credito_ref').html('Reporte este error: ' + data.responseText);
            console.log(data.responseText);
          }
        }
      });
    },
    rules: {
      cmb_cre_tipref:{
        required: true
      },
      cmb_mon_id: {
        required: true
      },
      cmb_cuedep_id: {
        required: true
      },
      txt_cre_tipcam: {
        required: true
      },
      txt_cre_preaco: {
        required: true
      },
      txt_cre_int: {
        required: true
      },
      txt_cre_numcuo: {
        required: true
      },
      txt_cre_linapr: {
        required: true
      },
      txt_cre_feccre: {
        required: true,
      },
      txt_cre_fecdes: {
        required: true,
      },
      txt_cre_obs: {
        required: true
      },
      cmb_cuo_tip: {
        required: true
      },
      txt_cre_fecgps: {
        required: true
      },
      txt_cre_fecseg:{
        required: true
      }
    },
    messages: {
      cmb_cre_tipref:{
        required: 'Tipo de Refinanciamiento'
      },
      cmb_mon_id: {
        required: '*'
      },
      cmb_cuedep_id: {
        required: '*'
      },
      txt_cre_tipcam: {
        required: '*'
      },
      txt_cre_preaco: {
        required: '*'
      },
      txt_cre_obs: {
        required: '*'
      },
      txt_cre_int: {
        required: '*'
      },
      txt_cre_numcuo: {
        required: '*'
      },
      txt_cre_linapr: {
        required: '*'
      },
      txt_cre_feccre: {
        required: '*'
      },
      txt_cre_fecdes: {
        required: '*'
      },
      cmb_cuo_tip: {
        required: 'Elija tipo de cuotas'
      },
      txt_cre_fecgps: {
        required: 'Ingrese fecha de Vencimiento'
      },
      txt_cre_fecseg:{
        required: 'Ingrese fecha de Vencimiento'
      }
    }
  });

  cambio_moneda($("#hdd_mon_id").val());
  //cmb_cuedep_id('');
  $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-yellow'
  });

  $('#che_gps').on('ifChanged', function(event){
    // Obtener el estado actual del checkbox
    var isChecked = $(this).prop('checked');
    credito_cronograma();

    if (isChecked) {
      gps_form_simple();
    }
    
  });

  $('#datetimepicker3, #datetimepicker4, #datetimepicker5, #datetimepicker6,#datetimepicker7, #datetimepicker8, #datetimepicker9' ).datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker3").on("change", function (e) {
    var startVal = $('#txt_cre_fecren').val();
    $('#datetimepicker3').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker4").on("change", function (e) {
    var endVal = $('#txt_cre_fpagimp').val();
    $('#datetimepicker4').data('datepicker').setEndDate(endVal);
  });
    $("#datetimepicker5").on("change", function (e) {
    var startVal = $('#txt_cre_feccre').val();
    $('#datetimepicker5').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker6").on("change", function (e) {
    var endVal = $('#txt_cre_fecdes').val();
    $('#datetimepicker6').data('datepicker').setEndDate(endVal);
  });
    $("#datetimepicker7").on("change", function (e) {
    var startVal = $('#txt_cre_fecfac').val();
    $('#datetimepicker7').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker8").on("change", function (e) {
    var endVal = $('#txt_cre_fecpro').val();
    $('#datetimepicker8').data('datepicker').setEndDate(endVal);
  });
  $("#datetimepicker9").on("change", function (e) {
    var endVal = $('#txt_cre_fecent').val();
    $('#datetimepicker9').data('datepicker').setEndDate(endVal);
  });
  
  $('.moneda2').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '999999.99'
  });

  $('#txt_cre_linapr').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '99999999.99'
  });
  $('.porcentaje').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '99.99'
  });
  
  $('#cmb_cuo_tip').change(function(event) {
      var numcuo = Number($('#txt_cre_numcuo').autoNumeric('get'));
      var int = Number($('#txt_cre_int').autoNumeric('get'));
      var cuotip = $(this).val(); //cmb_cuo_tip
      if(cuotip ==""){
          return false;
      }
      else{
          if(cuotip ==3){
            credito_cronograma();          
          }
          else{
              if(numcuo ==0){
                    alerta_warning("ALERT","Ingrese cuotas");
              }
              if(int==0){
                  alerta_warning("ALERT","Ingrese interes");
              }
                credito_cronograma();
          }  
      }

    });
  
  $('#txt_cre_preaco, #txt_cre_int, #txt_cre_numcuo, #cmb_cre_subper, #cmb_mon_id').change(function(event) {
    credito_calculo();
    credito_cronograma();
  });
  $('#txt_cre_int, #txt_cre_numcuo').change(function(event) {
      if($('#cmb_cuo_tip').val() == ""){
            return false;
      }
    credito_calculo();
    credito_cronograma();
  });

  $('#txt_cre_feccre, #txt_cre_fecfac, #txt_cre_fecdes, #txt_cre_fecpro').change(function(event) {
    cambio_moneda($('#cmb_mon_id').val());
    credito_calculo();
    credito_cronograma();
  });
  
  $('#txt_cre_amor').change(function(event) {
      var amor = $(this).autoNumeric('get');
      var acor = $('#txt_cre_preaco').autoNumeric('get');
      var res = 0;
      res = acor - amor;
      $('#txt_cre_preaco').autoNumeric('set',res);
      credito_calculo();
      credito_cronograma();
    });
});

function cambio_moneda(monid) {

  $.ajax({
    type: "POST",
    url: VISTA_URL + "/monedacambio/monedacambio_reg.php",
    async: true,
    dataType: "json",
    data: {
        mon_id: monid, 
        action: "obtener_dato",
        fecha:  $('#txt_cre_fecdes').val()
    },
    beforeSend: function () {
      //$("#cbo_cancha_id2").val("<option>' cargando '</option>");
    },
    success: function (data) {

      $('#txt_cre_tipcam').val(data.moncam_val);
    },
    complete: function (data) {

    }
  });
}

function cambiar_amortizacion(cre_id,cre_tip) {
    //console.log("VER");
  $.ajax({
    type: "POST",
    url: VISTA_URL + "/ingreso/ingreso_amor_reg.php",
    async: true,
    dataType: "json",
    data: {
        cre_id: cre_id, 
        cre_tip: cre_tip,
        mon_id: $("#hdd_mon_id").val(),
        action: "obtener_dato"
    },
    beforeSend: function () {
      //$("#cbo_cancha_id2").val("<option>' cargando '</option>");
    },
    success: function (data) {
        //console.log(data);
      $('#txt_cre_amor').val(data.amor_previa);
      var amor = data.amor_previa;
      var acor = $('#hdd_capital').val();
      console.log("acor = "+acor+'amor = '+amor);
      var res = 0;
      res = acor - amor;
      console.log("res = "+res);
      $('#txt_cre_preaco').autoNumeric('set',res);
      credito_calculo();
      credito_cronograma();
    },
    complete: function (data) {
        //console.log(data);
    }
  });
}

function amortizar_form(act){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "refinanciar/amortizar_form.php",
        async:true,
        dataType: "html",                      
        data: ({
            action: act,
            cre_id: $('#hdd_cre_id').val(),
            cre_tip: $('#hdd_cre_tip').val(),
            mon_id: $('#hdd_mon_id').val(),
            prorrateo: $('#hdd_prorrateo').val(),
            capital: $('#hdd_capital').val(),
            detalle_refinanciar: $('#hdd_detalle_refinanciar').val()
        }),
        beforeSend: function() {
                //$('#msj_credito').hide();
                //$('#div_refinanciar_form').dialog("open");
                //$('#div_refinanciar_form').html('Cargando <img src="../../images/loadingf11.gif" align="absmiddle"/>');
        },
        success: function(html){
            //console.log(html);
            if(html != 'sin_datos'){
                $('#div_amortizar_form').html(html);
                //modal_width_auto('modal_credito_amortizar',75);
                modal_height_auto('modal_credito_amortizar');
                $('#modal_credito_amortizar').modal('show');
            }

        }
    });
}

//esta variable captura el monto del prorrateo
monto_pro = 0;
function credito_calculo(){
    var v_pre = 0;
    var v_preaco = $('#txt_cre_preaco').val();
    //console.log("v_preaco = "+v_preaco);
    $.ajax({
      type: "POST",
      url: VISTA_URL + "creditoasiveh/credito_calculo.php",
      async:true,
      dataType: "json",
      data: ({
        action: "cuotas",
        veh_pre:  v_pre,
        cre_ini:  0,
        cre_preaco: v_preaco,
        cre_int:  $('#txt_cre_int').val(),
        cre_numcuo: $('#txt_cre_numcuo').val()
      }),
      beforeSend: function() {
        $('#txt_cre_linapr').val("0");
      },
      success: function(data){
        $('#txt_cre_linapr').val(data.credito_resultado);

        var linapr = Number($('#txt_cre_linapr').val().replace(/[^0-9\.]+/g,""));
        var suma = parseFloat(monto_pro + linapr).toFixed(2);

        $('#txt_cre_linapr').autoNumeric('set',suma);
        //console.log('prorra: '+monto_pro+' / linapr: '+linapr+' / suma: '+suma);
        obtener_valor_seguro_gps();
      },
      complete: function(data){
          //console.log(data);
      }
    });
}
  
function credito_cronograma(){
  var v_pre = 0;
  var v_preaco = $('#txt_cre_preaco').val();
  var linapr = Number($('#txt_cre_linapr').autoNumeric('get'));
  var numcuo = Number($('#txt_cre_numcuo').autoNumeric('get'));

  var tipo_cambio = parseFloat($('#hdd_tipo_cambio_venta').val());
  var valor_seguro = parseFloat($('#hdd_seguro_porcentaje').val());
  var cre_interes = parseFloat($("#txt_cre_int").val());
  var tecm = valor_seguro + cre_interes;
  $('#txt_cre_tecm').autoNumeric("set", tecm);

  //console.log('v_preaco = '+v_preaco+' linapr = '+linapr);
  if($('#txt_cre_fecfac').val() == '' || $('#txt_cre_fecpro').val() == ''){
    alert('Ingrese fecha de desembolso y prorrateo');
    return false;
  }

  if(tipo_cambio <= 0){
    alerta_warning('IMPORTANTE', 'Registre el tipo de cambio para poder generar el cronograma');
    return false;
  }

  var estado_gps = 0;

  if ($('#che_gps').is(':checked'))
    estado_gps = $('#che_gps').val();
  
  console.log('el estado del gps es: ' + estado_gps)
      
  
  $.ajax({
    type: "POST",
    url: VISTA_URL + "refinanciar/refinanciar_cronograma.php",
    async:true,
    dataType: "html",
    data: ({
      veh_pre:  v_pre,
      cre_ini:  0,
      cre_id: $('#hdd_cre_id').val(),
      fec_desem: $('#txt_cre_fecdes').val(),
      fec_prorra: $('#txt_cre_fecpro').val(),
      cre_preaco: v_preaco,
      cre_int: $('#txt_cre_int').val(),
      cre_numcuo: $('#txt_cre_numcuo').val(),
      cre_linapr: $('#hdd_cre_linapr').val(),
      mon_id: $('#cmb_mon_id').val(),
      cre_fecfac: $('#txt_cre_fecfac').val(),
      cre_fecdes: $('#txt_cre_fecdes').val(),
      cre_fecpro: $('#txt_cre_fecpro').val(),
      cre_per: $('#cmb_cre_subper').val(),
      tip_ade: $('#cmb_cre_tipade').val(),
      cuotip_id: $('#cmb_cuo_tip').val(),
      valor_seguro: $('#hdd_seguro_porcentaje').val(),
      valor_gps: $('#hdd_gps_precio').val(),
      estado_gps: estado_gps,
      tipo_cambio: tipo_cambio
    }),
    beforeSend: function(){
      $('#div_credito_cronograma').html('<span>Cargando...</span>');
    },
    success: function(html){
      $('#div_credito_cronograma').html(html);

      monto_pro = Number($('#hdd_mon_pro_1').val());
      
      /*var linapr = Number($('#txt_cre_linapr').val().replace(/[^0-9\.]+/g,""));

      suma_lin_pro = parseFloat(mon_pro + linapr).toFixed(2);
      console.log('prorra: '+mon_pro+' / linapr: '+linapr+' / suma: '+suma_lin_pro);*/
    },
    complete: function(){
      //credito_calculo();
    }
  });
}

function obtener_valor_seguro_gps(){
  var numero_cuotas = parseInt($('#txt_cre_numcuo').val());
  if(numero_cuotas <= 0)
    return false;

  $.ajax({
    type: "POST",
    url: VISTA_URL + "segurogps/segurogps_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "valores",
      numero_cuotas: numero_cuotas
    },
    beforeSend: function () {
    },
    success: function (data) {
      console.log(data);
      $('#hdd_seguro_porcentaje').val(data.valor_seguro);
      $('#hdd_gps_precio').val(data.valor_gps);
      $('#span_seguro_gps').text(data.texto);
      credito_cronograma();
    },
    complete: function (data) {},
  });
}

// JUAN 22-11-2023
function gps_form_simple() {
  var cliente_id = parseInt($('#hdd_cre_cli_id').val());
  var gps_precio = parseInt($('#hdd_gps_precio').val());
  var num_cuotas = parseInt($('#txt_cre_numcuo').val());

  if(cliente_id > 0 && gps_precio > 0 && num_cuotas > 0){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "controlseguro/gps_form_simple.php",
      async: true,
      dataType: "html",
      data: {
        cliente_id: cliente_id,
        gps_precio: gps_precio,
        num_cuotas: num_cuotas,
        poliza_id: $('#hdd_gps_simple_id').val()
      },
      beforeSend: function () {},
      success: function (data) {
        if (data != "sin_datos") {
          $("#div_modal_gps_simple").html(data);
          $("#modal_gps_simple").modal("show");
          //funcion js para limbiar el modal al cerrarlo
          modal_hidden_bs_modal("modal_gps_simple", "limpiar"); //funcion encontrada en public/js/generales.js
        }
      },
    });
  }
  else
    alerta_warning('IMPORTANTE', 'Hay algunos datos que debes llenar en el crédito antes de registrar el GPS: cliente ID: ' + cliente_id + ', Precio GPS: ' + gps_precio + ', Cuotas: ' + num_cuotas)
}

// JUAN 22-11-2023 -> FUNCION GENERAL PARA TODOS LOS FORMULARIOS DONDE SE USE EL REGISTRO DE GPS
function get_poliza_id(data) {
  var poliza_id = parseInt(data.poliza_id); //viene desde el registro del formulario de gps simple
  $('#hdd_gps_simple_id').val(poliza_id);
  console.log(data)
}