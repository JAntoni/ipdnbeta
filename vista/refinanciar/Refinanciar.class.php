<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Refinanciar extends Conexion{

    function listar_cuotasdetalle_impagas_entre_rango($credito_id, $creditotipo_id, $fecha_inicio, $fecha_fin){
      try {
        $sql = "SELECT * 
          FROM tb_cuota c 
          INNER JOIN tb_cuotadetalle cd ON cd.tb_cuota_id = c.tb_cuota_id 
          WHERE c.tb_credito_id = :credito_id 
          AND c.tb_creditotipo_id = :creditotipo_id
          AND tb_cuotadetalle_fec > :fecha_inicio AND tb_cuotadetalle_fec <= :fecha_fin AND tb_cuotadetalle_est != 2"; //cuotas impagas entre dos fechas

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha_inicio", $fecha_inicio, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle vencidas impagas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cuotasdetalle_menores_condicion_fecha($credito_id, $creditotipo_id, $fecha, $condicion){
      try {
        $sql = "SELECT * 
          FROM tb_cuota c 
          INNER JOIN tb_cuotadetalle cd ON cd.tb_cuota_id = c.tb_cuota_id 
          WHERE tb_cuota_xac = 1  
          AND c.tb_credito_id = :credito_id 
          AND c.tb_creditotipo_id = :creditotipo_id
          AND tb_cuotadetalle_fec ".$condicion." :fecha";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle vencidas impagas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function listar_cuotasdetalle_vencidas_impagas($cre_id, $cre_tip){
      try {
        $sql = "SELECT *
               FROM tb_cuota cu inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id 
               where tb_cuota_est !=2 
               and tb_credito_id =:cre_id 
               and tb_creditotipo_id =:cre_tip 
               and tb_cuotadetalle_est !=2 and tb_cuotadetalle_fec < NOW()";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle vencidas impagas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function cuotas_ateriores_cuotadetalle_id($cre_id, $cre_tip, $cuodet_id, $periodo){
      try {
        $sql = "SELECT c.tb_cuota_id,
                    tb_cuota_cuo, 
                    tb_cuota_amo, 
                    (tb_cuota_int / :periodo) AS interes_detalle,
                    tb_cuota_int,
                    tb_cuotadetalle_id, 
                    tb_cuotadetalle_cuo, 
                    tb_cuotadetalle_fec 
                from tb_cuota c inner join tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id 
                where c.tb_credito_id = :cre_id 
                and c.tb_creditotipo_id =:cre_tip 
                and tb_cuotadetalle_id <= :cuodet_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":periodo", $periodo, PDO::PARAM_INT);
        $sentencia->bindParam(":cuodet_id", $cuodet_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle anteriores";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function obtener_primera_cuota_numero_cuotasdetalle_impagas($cre_id, $cre_tip){
      try {
        $sql = "SELECT count(tb_cuotadetalle_id) as num_det, cu.* 
                FROM tb_cuota cu 
                inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id 
                where tb_cuota_est !=2 
                and tb_credito_id =:cre_id 
                and tb_creditotipo_id =:cre_tip 
                and tb_cuotadetalle_est !=2 
                group by tb_cuota_id 
                ORDER BY tb_cuota_fec limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle primera impaga";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function ultima_cuotadetalle_facturada($cre_id, $cre_tip){
      try {
        $sql = "SELECT * 
                from tb_cuota c 
                inner join tb_cuotadetalle cd on cd.tb_cuota_id = c.tb_cuota_id 
                where tb_cuota_xac = 1 
                AND c.tb_credito_id =:cre_id 
                and c.tb_creditotipo_id =:cre_tip 
                and tb_cuotadetalle_fec <= DATE(NOW()) 
                order by tb_cuotadetalle_fec desc limit 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cre_id", $cre_id, PDO::PARAM_INT);
        $sentencia->bindParam(":cre_tip", $cre_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle facturada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function ultima_cuotadetalle_facturada_pagada($credito_id, $creditotipo_id){
      try {
        /*
        ESTO LISTABA CUOTA FACTURADA PAGADA, PERO HAY CASOS DONDE EL CLIENTE PAGA POR ADELANTADO, ENTONCES TOMAMOS ESA CUOTA COMO REFERENCIA PARA TODOS LOS CALCULOS 
        SIGUIENTES, QUITAMOS LA CONFICION DE <= NOW()
        $sql = "SELECT * 
                FROM tb_cuota c 
                INNER JOIN tb_cuotadetalle cd ON cd.tb_cuota_id = c.tb_cuota_id 
                WHERE c.tb_credito_id =:credito_id 
                AND c.tb_creditotipo_id =:creditotipo_id 
                AND tb_cuotadetalle_fec <= DATE(NOW()) 
                AND tb_cuota_est = 2
                ORDER BY tb_cuotadetalle_fec DESC LIMIT 1";*/
        $sql = "SELECT * 
            FROM tb_cuota c 
            INNER JOIN tb_cuotadetalle cd ON cd.tb_cuota_id = c.tb_cuota_id 
            WHERE c.tb_credito_id =:credito_id 
            AND c.tb_creditotipo_id =:creditotipo_id 
            AND tb_cuota_est = 2
            ORDER BY tb_cuotadetalle_fec DESC LIMIT 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle facturada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function obtener_primera_cuotadetalle($credito_id, $creditotipo_id){
      try {
        $sql = "SELECT * 
                FROM tb_cuota c 
                INNER JOIN tb_cuotadetalle cd ON cd.tb_cuota_id = c.tb_cuota_id 
                WHERE c.tb_credito_id =:credito_id 
                AND c.tb_creditotipo_id =:creditotipo_id 
                AND tb_cuota_xac = 1 
                ORDER BY tb_cuota_fec DESC LIMIT 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay cuotasdetalle facturada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    // function obtener_numero_cuodasdetalle_impagas_cuodaid($cuo_id){
    //   $sql = "SELECT count(tb_cuotadetalle_id) as num_det, cu.* FROM tb_cuota cu inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id where tb_cuotadetalle_est !=2 and cu.tb_cuota_id =$cuo_id";
    //   $oCado = new Cado();
    //   $rst=$oCado->ejecute_sql($sql);
    //   return $rst;
    // }
    // function obtener_numero_cuodasdetalle_mayorigual_cuotadetallefecha($cuo_id, $cuode_fec){
    //   $sql = "SELECT count(tb_cuotadetalle_id) as num_det, cu.* FROM tb_cuota cu inner join tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id where tb_cuotadetalle_est !=2 and tb_cuotadetalle_fec > '$cuode_fec' and cu.tb_cuota_id =$cuo_id";
    //   $oCado = new Cado();
    //   $rst=$oCado->ejecute_sql($sql);
    //   return $rst; //3380
    // }
  }

?>
