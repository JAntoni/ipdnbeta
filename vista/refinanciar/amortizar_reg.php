<?php
require_once ("../../core/usuario_sesion.php");
require_once("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once ("../cajacambio/Cajacambio.class.php");
$oCajacambio = new Cajacambio();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$usuario_id = intval($_SESSION['usuario_id']);

$action = $_POST['hdd_action'];

if($action == "amortizar" || $action == "liquidar"){
	$cre_id = $_POST['hdd_cre_id'];
	$cre_tip = $_POST['hdd_cre_tip'];
  $data['cre_id'] = $cre_id;
	$forma_pago = intval($_POST['cmb_forma_pag']); // 1 en oficinas, 2 en banco
	$pago_banco = '';

	$credito_moneda_id = intval($_POST['hdd_amor_mon_id']); //? moneda original del crédito
	$moneda_seleccionada_id = intval($_POST['cmb_amor_mon_id']); //? moneda seleccionada por el usuario para el ingreso de dinero

	$razon_ingreso = 'INGRESO POR AMORTIZACIÓN';
	if($action == 'liquidar')
		$razon_ingreso == "INGRESO POR LIQUIDACION";

	$lugar_pago = '. EN EFECTIVO CON ASISTENCIA A OFICINA';

	//* SI EL PAGO FUE MEDIANTE BANCO, POR DEPOSITO
	if($forma_pago == 2){
		$lugar_pago = '';
		$numope = trim($_POST['txt_cuopag_numope']);
		$monto_validar = moneda_mysql($_POST['txt_cuopag_monval']);
		$monto_validar = formato_moneda($monto_validar);
		$moneda_cuenta_bancaria = intval($_POST['moneda_deposito']);

		//? LA CUENTA BANCARIA DEBE SER OBLIGATORIA YA QUE DE AQUÍ OBTENEMOS LA MONEDA PARA EL EGRESO POR BANCO
		if($moneda_cuenta_bancaria <= 0){
			$data['estado'] = 0;
			$data['msj'] = 'El pago lo estás realizando por banco, por ello debes seleccionar una Cuenta Bancaria, la moneda de la cuenta bancaria debe ser igual a la moneda seleccionada para el pago';
			echo json_encode($data);
			exit();
		}
		//? LA MONEDA SELECCIONADA PARA EL PAGO DEBE SER IGUAL A LA MONEDA DE LA CUNETA BANCARIA
		if($moneda_cuenta_bancaria != $moneda_seleccionada_id){
			$data['estado'] = 0;
			$data['msj'] = 'La moneda seleccionada de la izquierda debe ser igual a la moneda de la cuenta bancaria que has seleccionado';
			echo json_encode($data);
			exit();
		}

		$deposito_mon = 0;
		$dts = $oDeposito->validar_num_operacion($numope);
			if($dts['estado']==1){
				foreach ($dts['data'] as $key => $value) {
					$deposito_mon += floatval($value['tb_deposito_mon']);
				}
			}
		$dts = null;

		if($deposito_mon == 0){
			$data['estado'] = 0;
			$data['msj'] = 'El número de operación que ingresaste no se encuentra registrado, consulta con ADMINISTRACIÓN por favor.';
			echo json_encode($data);
			exit();
		}

		$ingreso_importe = 0;
    $tipocambio_dia = moneda_mysql($_POST['txt_amor_cuopag_tipcam']);
		$dts = $oIngreso->lista_ingresos_num_operacion($numope);
			if($dts['estado']==1){
				foreach ($dts['data'] as $key => $value) {
					$tb_ingreso_imp = floatval($value['tb_ingreso_imp']); //? si la moneda seleccionada es igual a la moneda de ingreso
					$ingreso_mon_id = intval($value['tb_moneda_id']);
					if($ingreso_mon_id == 1 && $moneda_seleccionada_id == 2) // el ingreso se hizo en soles, y se está seleccionado pagar en dolares
						$tb_ingreso_imp = formato_moneda($tb_ingreso_imp / $tipocambio_dia);
					if($ingreso_mon_id == 2 && $moneda_seleccionada_id == 1) // el ingreso se hizo en dolares, y se está seleccionado pagar en soles
						$tb_ingreso_imp = formato_moneda($tb_ingreso_imp * $tipocambio_dia);

					$ingreso_importe += $tb_ingreso_imp;
				}
			}
		$dts = null;

		$monto_usar = formato_moneda($deposito_mon - $ingreso_importe);

		if($monto_validar > $monto_usar){
			$data['estado'] = 0;
			$data['msj'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar más de lo depositado. Monto ingresado: '. formato_moneda($monto_validar).', monto disponible: '.formato_moneda($monto_usar).', TOTAL DEL DEPÓSITO: '.formato_moneda($deposito_mon);
			echo json_encode($data);
			exit();
		}

		$moneda_egre_id = intval($_POST['moneda_deposito']);
 		$moneda_egre = 'S/.';
 		if($moneda_egre_id == 2)
 			$moneda_egre = 'US$';
 		$pago_banco = 'Abonado en el banco, la fecha de depósito fue: '.$_POST['txt_cuopag_fecdep'].', la fecha de validación fue: '.$_POST['txt_cuopag_fec'].', el N° de operación fue: '.$_POST['txt_cuopag_numope'].', el monto depositado fue: '.$moneda_egre.' '.$deposito_mon.', la comisión del banco fue: '.$moneda_egre.' '.$_POST['txt_cuopag_comi'].', el monto validado fue: '.$moneda_egre.' '.$_POST['txt_cuopag_monval'];
	}

	$razon_ingreso = $razon_ingreso.$lugar_pago;

	$subcuenta_amor = 0; //cuentas y subcuentas encontradas en Mantenimiento->Caja->Cuentas - Sub cuentas
	$subcuenta_liqui = 0; //cuentas y subcuentas encontradas en Mantenimiento->Caja->Cuentas - Sub cuentas
	$credito_nom = ''; $cre_cod = '';

	if($cre_tip == 2){
	  require_once ("../creditoasiveh/Creditoasiveh.class.php");
	  $oCredito = new creditoasiveh();
	  $subcuenta_amor = 106; //sub cuenta de AMORTIZACION ASIVEH
		$subcuenta_liqui = 110; //sub cuenta de LIQUIDACION ASIVEH
		$credito_nom = 'Crédito ASIVEH';
		$cre_cod = 'CAV';
		$creditotipo_id = 2; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
	}
	if($cre_tip == 3){
	  require_once ("../creditogarveh/Creditogarveh.class.php");
	  $oCredito = new creditogarveh();
	  $subcuenta_amor = 107; //sub cuenta de AMORTIZACION GARVEH
		$subcuenta_liqui = 111; // sub cuenta de LIQUIDACION GARVEH
		$credito_nom = 'Crédito GARVEH';
		$cre_cod = 'CGV';
		$creditotipo_id = 3; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
	}
	if($cre_tip == 4){
	  require_once ("../creditohipo/Creditohipo.class.php");
	  $oCredito = new Creditohipo();
	  $subcuenta_amor = 108; //sub cuenta de AMORTIZACION DE HIPOTECARIO
		$subcuenta_liqui = 112; //sub cuenta de LIQUIDACION DE HIPOTECARIO
		$credito_nom = 'Crédito HIPOTECARIO';
		$cre_cod = 'CH';
		$creditotipo_id = 4; // 1 cmenor, 2 asiveh, 3 garveh, 4 hipo
	}

	if(!empty($_POST['txt_cuopag_mon']) && !empty($cre_id) && !empty($cre_tip)){
		
		$capital = moneda_mysql($_POST['txt_capital']);
		$monto_pagado = moneda_mysql($_POST['txt_cuopag_mon']);
		$amor_prev = moneda_mysql($_POST['txt_amort_pre']);
		$paga_con = moneda_mysql($_POST['txt_paga_con']);
		$opc_vuelto = $_POST['cmb_cuopag_vuel']; //opcion de dar vuelto

		if($action == "liquidar" && $monto_pagado < $capital){
			$data['estado'] = 0;
			$data['msj'] = 'Para liquidar totalmente el crédito el Monto Pago debe ser igual al Capital';

			echo json_encode($data);
			exit();
		}
		$cue_id = 0; //CANCEL CUOTAS
		$subcue_id = 0;

		if($monto_pagado < $capital){
			$cue_id = 35; // cuenta de AMORTIZACION
			$subcue_id = $subcuenta_amor; //sub cuenta de AMORTIZACION 
		}
		if($monto_pagado >= $capital){
			$cue_id = 36; // cuenta de LIQUIDACION
			$subcue_id = $subcuenta_liqui; //sub cuenta de LIQUIDACION 
		}

		if($cue_id == 0 || $subcue_id == 0){
			$data['estado'] = 0;
			$data['msj'] = 'No se ha especificado las cuentas y subcuentas para la liquidación, reporte con sistemas esete error, tomando un pantallazo';

			echo json_encode($data);
			exit();
		}
                
		$fecha = fecha_mysql(date('d-m-Y'));
		$mod_id = $cre_tip; //1 credito menor, 2 asiveh, 3 garveh, 4 hipotecario
		$ing_det = $_POST['hdd_amor_det'].'. '.$razon_ingreso.'. '.$pago_banco;

		$numdoc = "";

		$oIngreso->ingreso_usureg = $_SESSION['usuario_id'];
		$oIngreso->ingreso_usumod = $_SESSION['usuario_id'];
		$oIngreso->ingreso_fec = $fecha;
		$oIngreso->documento_id = 8; 
		$oIngreso->ingreso_numdoc = $numdoc;
		$oIngreso->ingreso_det = $ing_det; 
		$oIngreso->ingreso_imp = $monto_pagado;  
		$oIngreso->cuenta_id = $cue_id;
		$oIngreso->subcuenta_id = $subcue_id; 
		$oIngreso->cliente_id = $_POST['hdd_cli_id']; 
		$oIngreso->caja_id = 1; 
		$oIngreso->moneda_id = $credito_moneda_id;
		//valores que pueden ser cambiantes según requerimiento de ingreso
		$oIngreso->modulo_id = $mod_id; 
		$oIngreso->ingreso_modide = $cre_id;
		$oIngreso->empresa_id = $_SESSION['empresa_id'];
		$oIngreso->ingreso_fecdep = ''; 
		$oIngreso->ingreso_numope = ''; 
		$oIngreso->ingreso_mondep = 0; 
		$oIngreso->ingreso_comi = 0; 
		$oIngreso->cuentadeposito_id = 0; 
		$oIngreso->banco_id = 0; 
		$oIngreso->ingreso_ap = 0; 
		$oIngreso->ingreso_detex = '';
		
		$result2 = $oIngreso->insertar();
			if(intval($result2['estado']) == 1){
				$ing_id = $result2['ingreso_id'];
			}
		$result2 = null;
                
		//REGISTRAMOS EL PRORRATEO DE LA LIQUIDACIÓN O AMORTIZACION DEL CRÉDITO
		$prorrateo = moneda_mysql($_POST['hdd_prorrateo']);
		$oIngreso->modificar_campo($ing_id,'tb_ingreso_pro', $prorrateo,'STR');
                
		if($numdoc==""){
			$numdoc = $ing_id.'-'.$subcue_id.'-'.$cre_id;
			$oIngreso->modificar_campo($ing_id,'tb_ingreso_numdoc',$numdoc,'STR');
		}

		//paga con diferente moneda
		if($credito_moneda_id != $moneda_seleccionada_id and $_POST['txt_cuopag_moncam'] > 0){
			//cambio
			$xac = 1;
			if($credito_moneda_id == 2 and $moneda_seleccionada_id == 1){
				$des = "DOLARES A SOLES | ".$_POST['hdd_amor_det'].". REGISTRO DE INGRESO EN CUENTA BANCARIA DE FECHA ".$_POST['txt_cuopag_fecdep']." Y N° OPE: ".$_POST['txt_cuopag_numope'];
			}

			if($credito_moneda_id == 1 and $moneda_seleccionada_id == 2){
				$des = "SOLES A DOLARES | ".$_POST['hdd_amor_det'].". REGISTRO DE INGRESO EN CUENTA BANCARIA DE FECHA ".$_POST['txt_cuopag_fecdep']." Y N° OPE: ".$_POST['txt_cuopag_numope'];
			}

			$caj_id = 1;
			$mod_id = $cre_tip;
                        
			$oCajacambio->tb_cajacambio_usureg= $_SESSION['usuario_id'];
			$oCajacambio->tb_cajacambio_usumod= $_SESSION['usuario_id'];
			$oCajacambio->tb_cajacambio_xac= $xac;
			$oCajacambio->tb_caja_id= $caj_id;
			$oCajacambio->tb_moneda_id= $credito_moneda_id;
			$oCajacambio->tb_cajacambio_fec= $fecha;
			$oCajacambio->tb_cajacambio_imp= moneda_mysql($monto_pagado);
			$oCajacambio->tb_cajacambio_tipcam= moneda_mysql($_POST['txt_amor_cuopag_tipcam']);
			$oCajacambio->tb_cajacambio_cam= moneda_mysql($_POST['txt_cuopag_moncam']);
			$oCajacambio->tb_cajacambio_des= $des;
			$oCajacambio->tb_modulo_id= $mod_id;
			$oCajacambio->tb_cajacambio_modid= $cre_id;
			$oCajacambio->tb_empresa_id= $_SESSION['empresa_id'];

			$result2 = $oCajacambio->insertar();
				if(intval($result2['estado']) == 1){
					$cajcam_id = $result2['cajacambio_id'];
				}
			$result2 = null;

			$egr_det ='CAMBIO DE MONEDA: ' . $des;
			$cue_id	=23;
			$subcue_id=0;
			$doc_id=9;
			$mod_id=60;
			$numdoc=$mod_id.'-'.$cajcam_id;
			$egr_est='1';
			$pro_id='1';

			$oEgreso->egreso_usureg = $_SESSION['usuario_id'];
			$oEgreso->egreso_usumod= $_SESSION['usuario_id'];
			$oEgreso->egreso_fec = $fecha;
			$oEgreso->documento_id = $doc_id;
			$oEgreso->egreso_numdoc = $numdoc;
			$oEgreso->egreso_det = $egr_det;
			$oEgreso->egreso_imp = moneda_mysql($monto_pagado);
			$oEgreso->egreso_tipcam = $_POST['txt_amor_cuopag_tipcam'];
			$oEgreso->egreso_est = $egr_est;
			$oEgreso->cuenta_id = $cue_id;
			$oEgreso->subcuenta_id = $subcue_id;
			$oEgreso->proveedor_id = $pro_id;
			$oEgreso->cliente_id = 0;
			$oEgreso->usuario_id = 0;
			$oEgreso->caja_id = $caj_id;
			$oEgreso->moneda_id = $credito_moneda_id;
			$oEgreso->modulo_id = $mod_id;
			$oEgreso->egreso_modide = $cajcam_id;
			$oEgreso->empresa_id = $_SESSION['empresa_id'];
			$oEgreso->egreso_ap = 0;

      $oEgreso->insertar();

			$ing_det ='CAMBIO DE MONEDA: ' . $des;
			$doc_id= 8;
			$mod_id=60;
			$numdoc=$mod_id.'-'.$cajcam_id;
			$cue_id	=24;
			$subcue_id=0;
			$ing_est='1';
			//$cli_id=1;
			$cli_id=$_POST['hdd_cli_id'];

			if($credito_moneda_id == 1) $mon_id = '2';
			if($credito_moneda_id == 2) $mon_id = '1';

			$oIngreso->ingreso_usureg = $_SESSION['usuario_id'];
			$oIngreso->ingreso_usumod = $_SESSION['usuario_id'];
			$oIngreso->ingreso_fec = $fecha;
			$oIngreso->documento_id = $doc_id; 
			$oIngreso->ingreso_numdoc = $numdoc;
			$oIngreso->ingreso_det = $ing_det; 
			$oIngreso->ingreso_imp = moneda_mysql($_POST['txt_cuopag_moncam']);  
			$oIngreso->cuenta_id = $cue_id;
			$oIngreso->subcuenta_id = $subcue_id; 
			$oIngreso->cliente_id = $cli_id; 
			$oIngreso->caja_id = $caj_id; 
			$oIngreso->moneda_id = $mon_id;
			//valores que pueden ser cambiantes según requerimiento de ingreso
			$oIngreso->modulo_id = $mod_id; 
			$oIngreso->ingreso_modide = $cajcam_id;
			$oIngreso->empresa_id = $_SESSION['empresa_id'];
			$oIngreso->ingreso_fecdep = ''; 
			$oIngreso->ingreso_numope = ''; 
			$oIngreso->ingreso_mondep = 0; 
			$oIngreso->ingreso_comi = 0; 
			$oIngreso->cuentadeposito_id = 0; 
			$oIngreso->banco_id = 0; 
			$oIngreso->ingreso_ap = 0; 
			$oIngreso->ingreso_detex = '';

      $oIngreso->insertar();

		}

   	//cuando existe vuelto
   	$tot_vuelto = $_POST['txt_vuelto'];
   	$res_vuel = $paga_con - $monto_pagado;
   	//solo se dará vuelto cuando se paga en dolares y desea vuelto en soles
   	if ($res_vuel > 0 && $moneda_seleccionada_id == 2 && $opc_vuelto == 1) {
   		
   		$dio_vuelto = 'Amortización con vuelto solicitado';
   		$prec_dolar = $_POST['txt_amor_cuopag_tipcam'];

 			//es deir pago con dolares y quiero mi vuelto en soles
 			$des = "DOLARES A SOLES | ".$_POST['hdd_amor_det'];
 			$vuelto = $res_vuel * $prec_dolar;

   		//cambio
			$xac=1;
			$caj_id=1;
			$mod_id=30;

			$oCajacambio->tb_cajacambio_usureg= $_SESSION['usuario_id'];
			$oCajacambio->tb_cajacambio_usumod= $_SESSION['usuario_id'];
			$oCajacambio->tb_cajacambio_xac= $xac;
			$oCajacambio->tb_caja_id= $caj_id;
			$oCajacambio->tb_moneda_id= $opc_vuelto;
			$oCajacambio->tb_cajacambio_fec= $fecha;
			$oCajacambio->tb_cajacambio_imp= moneda_mysql($vuelto);
			$oCajacambio->tb_cajacambio_tipcam= moneda_mysql($_POST['txt_amor_cuopag_tipcam']);
			$oCajacambio->tb_cajacambio_cam= moneda_mysql($res_vuel);
			$oCajacambio->tb_cajacambio_des= $des;
			$oCajacambio->tb_modulo_id= $mod_id;
			$oCajacambio->tb_cajacambio_modid= $cre_id;
			$oCajacambio->tb_empresa_id= $_SESSION['empresa_id'];

			$result2 = $oCajacambio->insertar();
				if(intval($result2['estado']) == 1){
						$cajcam_id = $result2['cajacambio_id'];
				}
			$result2 = null;

			$egr_det ='CAMBIO DE MONEDA: ' . $des;
			$cue_id	=23;
			$subcue_id=0;
			$doc_id=9;
			$mod_id=60;
			$numdoc=$mod_id.'-'.$cajcam_id;
			$egr_est='1';
			$pro_id='1';

			$oEgreso->egreso_usureg = $_SESSION['usuario_id'];
			$oEgreso->egreso_usumod= $_SESSION['usuario_id'];
			$oEgreso->egreso_fec = $fecha;
			$oEgreso->documento_id = $doc_id;
			$oEgreso->egreso_numdoc = $numdoc;
			$oEgreso->egreso_det = $egr_det;
			$oEgreso->egreso_imp = moneda_mysql($vuelto);
			$oEgreso->egreso_tipcam = $_POST['txt_amor_cuopag_tipcam'];
			$oEgreso->egreso_est = $egr_est;
			$oEgreso->cuenta_id = $cue_id;
			$oEgreso->subcuenta_id = $subcue_id;
			$oEgreso->proveedor_id = $pro_id;
			$oEgreso->cliente_id = 0;
			$oEgreso->usuario_id = 0;
			$oEgreso->caja_id = $caj_id;
			$oEgreso->moneda_id = $opc_vuelto;
			$oEgreso->modulo_id = $mod_id;
			$oEgreso->egreso_modide = $cajcam_id;
			$oEgreso->empresa_id = $_SESSION['empresa_id'];
			$oEgreso->egreso_ap = 0;
			
			$oEgreso->insertar();
                               
			$ing_det ='CAMBIO DE MONEDA: ' . $des;
			$doc_id= 8;
			$mod_id=60;
			$numdoc=$mod_id.'-'.$cajcam_id;
			$cue_id	=24;
			$subcue_id=0;
			$ing_est='1';
			//$cli_id=1;
			$cli_id=$_POST['hdd_cli_id'];

			if($opc_vuelto=='1')$mon_id = '2';
			if($opc_vuelto=='2')$mon_id = '1';

			$oIngreso->ingreso_usureg = $_SESSION['usuario_id'];
			$oIngreso->ingreso_usumod = $_SESSION['usuario_id'];
			$oIngreso->ingreso_fec = $fecha;
			$oIngreso->documento_id = $doc_id; 
			$oIngreso->ingreso_numdoc = $numdoc;
			$oIngreso->ingreso_det = $ing_det; 
			$oIngreso->ingreso_imp = moneda_mysql($res_vuel);  
			$oIngreso->cuenta_id = $cue_id;
			$oIngreso->subcuenta_id = $subcue_id; 
			$oIngreso->cliente_id = $cli_id; 
			$oIngreso->caja_id = $caj_id; 
			$oIngreso->moneda_id = $mon_id;
			//valores que pueden ser cambiantes según requerimiento de ingreso
			$oIngreso->modulo_id = $mod_id; 
			$oIngreso->ingreso_modide = $cajcam_id;
			$oIngreso->empresa_id = $_SESSION['empresa_id'];
			$oIngreso->ingreso_fecdep = ''; 
			$oIngreso->ingreso_numope = ''; 
			$oIngreso->ingreso_mondep = 0; 
			$oIngreso->ingreso_comi = 0; 
			$oIngreso->cuentadeposito_id = 0; 
			$oIngreso->banco_id = 0; 
			$oIngreso->ingreso_ap = 0; 
			$oIngreso->ingreso_detex = '';

    	$oIngreso->insertar();
   	}

   	//SI HA LIQUIDADO O AMORTIZADO POR DEPOSITO
   	$forma_pago = intval($_POST['cmb_forma_pag']); // 1 en oficinas, 2 en banco

   	if($forma_pago == 2){
			$oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_cuopag_fecdep']);
			$oIngreso->ingreso_numope = $_POST['txt_cuopag_numope'];
			$oIngreso->ingreso_mondep =  moneda_mysql($_POST['txt_cuopag_monval']);
			$oIngreso->ingreso_comi = moneda_mysql($_POST['txt_cuopag_comi']);
			$oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id_banco'];
			$oIngreso->banco_id = $_POST['cmb_banco_id'];
			$oIngreso->ingreso_id = $ing_id;
			$oIngreso->registrar_datos_deposito_banco(); //modificamos el ingreso ya realizado líneas arriba

			$moneda_egre_id = intval($_POST['moneda_deposito']); //? esta moneda corresponde a la cuenta bancaria si la cuenta es en soles o dolares 1 soles, 2 dolares
	 		$moneda_egre = 'S/.';
	 		if($moneda_egre_id == 2)
	 			$moneda_egre = 'US$';

      $xac = 1;
			$cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO
			$subcue_egre_id = 71; // 71 es cuenta en soles
			$pro_id = 1;//sistema
			$mod_id = 30;//30 es el modulo_id en ingreso, representa al CUOTAPAGO, será igual para EGRESO servira para cuando se anule el pago
			$modide = $ing_id; //el modide de egreso guardará el id del Ingreso de amor o liqui para poder anularlo mediante este id
			$egr_det ='EGRESO POR: '.$pago_banco;
			$caj_id = 1; //caja id 14 ya que es CAJA AP
			$numdoc = $mod_id.'-'.$ing_id.'-'.$subcue_id;
			$monto_valido_banco = formato_numero($_POST['txt_cuopag_monval']);
			
			$oEgreso->egreso_usureg = $_SESSION['usuario_id'];
			$oEgreso->egreso_usumod= $_SESSION['usuario_id'];
			$oEgreso->egreso_fec = $fecha;
			$oEgreso->documento_id = 9;
			$oEgreso->egreso_numdoc = $numdoc;
			$oEgreso->egreso_det = $egr_det;
			$oEgreso->egreso_imp = $monto_valido_banco;
			$oEgreso->egreso_tipcam = moneda_mysql($_POST['txt_amor_cuopag_tipcam']);
			$oEgreso->egreso_est = 1;
			$oEgreso->cuenta_id = $cue_id;
			$oEgreso->subcuenta_id = $subcue_id;
			$oEgreso->proveedor_id = $pro_id;
			$oEgreso->cliente_id = 0;
			$oEgreso->usuario_id = 0;
			$oEgreso->caja_id = $caj_id;
			$oEgreso->moneda_id = $moneda_egre_id;
			$oEgreso->modulo_id = $mod_id;
			$oEgreso->egreso_modide = $modide;
			$oEgreso->empresa_id = $_SESSION['empresa_id'];
			$oEgreso->egreso_ap = 0;
			
			$oEgreso->insertar();
   	}

   	//si se ha pagado todo el credito, cambia a estado LIQUIDADO
   	$dts = $oCredito->mostrarUno($_POST['hdd_cre_id']);
			if($dts['estado']==1){
				$num_cuo = $dts['data']['tb_credito_numcuo'];
			}
   	$dts = null;

   	$moneda = 'S/.';
   	if($credito_moneda_id == 2)
   		$moneda = 'US$.';

   	if($monto_pagado < $capital){
    	$his = '<span style="color: green;">El crédito fue amortizado con un total de '.$moneda.' '.formato_moneda($monto_pagado).'. Amortizado por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';

    	//$oCredito->registro_historial($_POST['hdd_cre_id'], $his);
			$oCreditolinea->insertar($creditotipo_id, $_POST['hdd_cre_id'], $usuario_id, $his);

    	$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha amortizado el '.$credito_nom.' de número: '.$cre_cod.'-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';

  		$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
  		$oLineaTiempo->tb_lineatiempo_det = $line_det;
      $oLineaTiempo->insertar();
    	
    	$data['estado'] = 1;
    	$data['amor'] = formato_moneda($monto_pagado + $amor_prev);
   	}

    if($monto_pagado >= $capital){
    	$his = '<span style="color: green;">El crédito pasó a liquidado ya que se pagó el capital total de: '.$moneda.' '.formato_moneda($monto_pagado + $amor_prev).'. Liquidado por: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.date('d-m-Y h:i a').'</b></span><br>';

    	//$oCredito->registro_historial($_POST['hdd_cre_id'], $his);
			$oCreditolinea->insertar($creditotipo_id, $_POST['hdd_cre_id'], $usuario_id, $his);

			//? SE LIQUIDÓ Y GUARDAMOS TODO EL DETALLE DE LA LIQUIDACION:
			$detalle_refinanciar = $_POST['hdd_detalle_refinanciar'].'<br>';
  		//$oCredito->registro_historial($_POST['hdd_cre_id'], $detalle_refinanciar); //guardamos en el crédito ORIGINAL
			$oCreditolinea->insertar($creditotipo_id, $_POST['hdd_cre_id'], $usuario_id, $detalle_refinanciar);

    	$line_det = '<b>'.$_SESSION['usuario_primernom'].'</b> ha liquidado el '.$credito_nom.' de número: '.$cre_cod.'-'.str_pad($_POST['hdd_cre_id'], 4, "0", STR_PAD_LEFT).'.  && <b>'.date('d-m-Y h:i a').'</b>';

  		$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
  		$oLineaTiempo->tb_lineatiempo_det = $line_det;
      $oLineaTiempo->insertar();

    	$oCredito->modificar_campo($cre_id,'tb_credito_est', 7,'INT'); // 7 es el estado liquidado
    	$oCredito->modificar_campo($cre_id,'tb_credito_fecliq', date('Y-m-d'), 'STR'); // 7 es el estado liquidado

    	//listamos las adendas del crédipo para LIQUIDARLAS
      $dts = $oCredito->listar_adendas_credito($cre_id);
				if($dts['estado']==1){
					foreach ($dts['data'] as $key => $value) {
						$aden_id = $value['tb_credito_id'];
						$est=8;
						$oCredito->aprobar($aden_id, $_SESSION['usuario_id'], 7);
						$oCredito->modificar_campo($aden_id,'tb_credito_fecliq', date('Y-m-d'), 'STR'); // 7 es el estado liquidado

						//registramos el historial para el credito
						$his = '<span style="color: green;">Adenda LUIQUIDADA por: <b>'.$_SESSION['usuario_nom'].'</b>. Detalle: ha sido LIQUIDADA ya que el crédito matríz fue liquidado en su totalidad, crédito ID. CAV-'.$cre_id.' | <b>'.date('d-m-Y h:i a').'</b></span><br>';
						//$oCredito->registro_historial($aden_id, $his);
						$oCreditolinea->insertar($creditotipo_id, $aden_id, $usuario_id, $his);
					}
				}
      $dts = null;

      $data['estado'] = 2;
    }

		$data['msj'] = 'Se registró el pago correctamente.'.$dio_vuelto.' // form: '.$forma_pago;
	}
	else
	{
		$data['estado'] = 0;
		$data['msj'] = 'Intentelo nuevamente.';
                //echo json_encode($data);
	}
	echo json_encode($data);
}

?>