<?php

function simbolo_moneda($moneda_id){
  $simbolo = 'SIN SIMBOLO';
  if(intval($moneda_id) > 0 && intval($moneda_id) == 1){
    $simbolo = 'S/.';
  }
  if(intval($moneda_id) > 0 && intval($moneda_id) == 2){
    $simbolo = '$USD';
  }
  return $simbolo;
}
?>