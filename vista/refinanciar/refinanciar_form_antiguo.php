<?php
//session_start();

require_once ("../../core/usuario_sesion.php");
require_once ("../usuario/Usuario.class.php");

$oUsuario = new Usuario();
require_once ("../acuerdopago/Acuerdopago.class.php");
$oAcuerdopago = new Acuerdopago();
require_once ("../refinanciar/Refinanciar.class.php");
$oRefinanciar = new Refinanciar();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
require_once('../cuota/Cuotadetalle.class.php');
$oCuotadetalle = new Cuotadetalle();
require_once('../cuota/Cuota.class.php');
$oCuota = new Cuota();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");


$cre_tip = $_POST['cre_tip']; //tipo de crédito 1 menor, 2 asiveh, 3 garveh, 4 hipo
if($cre_tip == 2){
  require_once ("../creditoasiveh/Creditoasiveh.class.php");
  $oCredito = new Creditoasiveh();
}
if($cre_tip == 3){
  require_once ("../creditogarveh/Creditogarveh.class.php");
  $oCredito = new Creditogarveh();
}
if($cre_tip == 4){
  require_once ("../creditohipo/Creditohipo.class.php");
  $oCredito = new Creditohipo();
}

$action = $_POST['action'];
$cre_id = $_POST['cre_id'];
$ref_tip = $_POST['ref_tip'];
$fecha_ref = trim($_POST['fecha_ref']);

$validar_fecha = validar_fecha_espanol($fecha_ref);

if(!$validar_fecha){
  echo '<h2 style="color: red;">POR FAVOR INGRESE UNA FECHA VÁLIDA PARA PODER CALCULAR, RECUERDE QUE EL FORMATO ES COMO EL SIGUIENTE: 01-10-2018 / '.$fecha_ref.'</h2>';
  exit();
}

$cre_feccre = date('d-m-Y');
$cre_fecdes = date('d-m-Y');
$cre_fecfac = date('d-m-Y');
$cre_fecent = date('d-m-Y');
$cre_fecpro = date('d-m-Y');

$result=$oCredito->mostrarUno($cre_id);
if($result['estado']==1){
  $cre_matriz = $result['data']['tb_credito_idori'];
  $cre_mon_id= $result['data']['tb_moneda_id'];
  $moneda_id = $cre_mon_id;
  $cli_id = $result['data']['tb_cliente_id'];
  $cre_preaco = floatval($result['data']['tb_credito_preaco']);
  $cre_cuotip = intval($result['data']['tb_cuotatipo_id']);
  $cre_subper_id = $result['data']['tb_cuotasubperiodo_id']; //1 mensual, 2 quincenal, 3 semanal
  $cre_fecren= mostrar_fecha($result['data']['tb_credito_fecren']); //fecha de vencimiento del SOAT VER: tb_credito_fecsoat  // ESTABA: tb_credito_fecren
  $cre_fecseg= mostrar_fecha($result['data']['tb_credito_fecseg']); //fecha de vencimiento del STR VER: tb_credito_fecstr // ESTABA: tb_credito_fecseg
  $cre_fecgps= mostrar_fecha($result['data']['tb_credito_fecgps']); //fecha de vencimiento del GPS
  $cre_fpagimp = mostrar_fecha($result['data']['tb_credito_pagimp']); //fecha del pago del inpuesto vehicular
  $credito_int = floatval($result['data']['tb_credito_int']);
  $cre_diaspa = intval($result['data']['tb_credito_diapar']);
  $credito_fecfac = mostrar_fecha($result['data']['tb_credito_fecfac']);
}
$result = null;
if(intval($cre_matriz)>0){
    $result=$oCredito->mostrarUno($cre_matriz);
    if($result['estado']==1){
        $cre_fecren= mostrar_fecha($result['data']['tb_credito_fecsoat']); //fecha de vencimiento del SOAT VER: tb_credito_fecsoat  // ESTABA: tb_credito_fecren
        $cre_fecseg= mostrar_fecha($result['data']['tb_credito_fecstr']); //fecha de vencimiento del STR VER: tb_credito_fecstr // ESTABA: tb_credito_fecseg
        $cre_fecgps= mostrar_fecha($result['data']['tb_credito_fecgps']); //fecha de vencimiento del GPS
    }
}
  if($cre_fecgps == "31-12-1969"){
   $cre_fecgps = "";   
  }
  if($cre_fecseg == "31-12-1969"){
   $cre_fecseg = "";   
  }
  if($cre_fecren == "31-12-1969"){
   $cre_fecren = "";   
  }

//en primer lugar verificamos si el credito tiene acuerdos de pago activos, si los tiene entonces no se podrá refinanciar hasta que pague su acuerdo
$result1 = $oAcuerdopago->listar_acuerdopago_credito_xac($cre_id, $cre_tip, 0, $cli_id); //si xac = 0 el acuerdo está activo
if($result1['estado']==1){
    $rows = count($result1['data']);
}
$result1 = null;

if($rows > 0){
  echo '<h2 style="color: red;">NO SE PUEDE REFINANCIAR YA QUE TIENE '.$rows.' ACUERDO(S) ACTIVOS, EL CLIENTE TENDRÁ QUE TERMINAR DE PAGAR PRIMERO SUS ACUERDOS. '.$cre_id.' '.$cre_tip.' '.$cli_id.'</h2>';
  exit();
}
//verificamos que si el cliente activó el monto de la caja AP para pagar cuotas vencidas, haya aplicado todo el monto

//consultamos si de este crédito hay acuerdos de pago activos para que sus montos pagados sean aplicados totalmente
$result1 = $oAcuerdopago->listar_por_credito_aplicar($cre_id, $cre_tip, $cli_id); //devuelve todos los AP que su columna tb_acuerdopago_apl = 1: disponibles para aplicar
if($result1['estado']==1){
    $rows_apl = count($result1['data']);
}
$result1 = null;
//si hay AP disponibles para aplicar obtenemos todos sus ingresos guardados en la CAJA AP restanto egresos de la misma caja, para saber cuanto hay disponible para aplicar
$monto_aplicar = 0;

if($rows_apl > 0){
  $monto_ingre_ap = 0;
  $monto_egre_ap = 0;
  $caja_id = 14; //caja ap

  $result2 = $oAcuerdopago->listar_ap_aplicar_por_cliente(2, $cli_id);
  if($result2['estado']==1){
      foreach ($result2['data'] as $key => $value) {
          $acuerdo_id = $value['tb_credito_nuevo']; //tomará el último ACUERDO DE PAGO registrado, ya que ese debe ser
          $result3 = $oIngreso->ingreso_total_por_credito_cuotadetalle($dt2['tb_credito_nuevo'], 2, $cre_mon_id); //parametro 2: CREDITO ASIVEH
          if($result3['estado']==1){
            foreach ($result3['data'] as $key => $value1) {
                $monto_ingre_ap += formato_moneda($value1['importe_total']);
            }
          }
          $result3 = null;
      }
  }
  $result2 = null;

  $modulo_id = 100; //ya que egreso no tiene cliente_id, el egreso para CAJA AP tendra como modulo = 100 e egreso_modide = cliente_id
  $result2 = $oEgreso->egresos_caja_cliente($caja_id, $cre_mon_id, $modulo_id, $cli_id);
  if($result2['estado']==1){
      foreach ($result2['data'] as $key => $value) {
          $monto_egre_ap += $value['tb_egreso_imp'];
      }
  }
  $result2 = null;

  $monto_aplicar = $monto_ingre_ap - $monto_egre_ap;
  $monto_aplicar = number_format($monto_aplicar, 2, '.', ''); //REVISAR DESPUES LA FUNCION
}

if($monto_aplicar > 0){
  echo '<h2 style="color: red;">NO SE PUEDE REFINANCIAR YA QUE HAY MONTO DISPONIBLE EN CAJA AP PARA SER APLICACO A LAS CUOTAS VENCIDAS. HAY UN TOTAL DE: '.$monto_aplicar.', INGRESO: '.$monto_ingre_ap.', EGRESO: '.$monto_egre_ap.'</h2>';
  exit();
}

/*listamos las adendas si lo tuviera el crédito
$num_impagas = 0; $msj = '';
$dts = $oCredito->listar_adendas_credito($cre_id); //lista adendas que esten en estado 3: desembolsado
  $rows = mysql_num_rows($dts);
  //listamos las cuotasdetalle vencidas impagas, si hay impagas entonces no se puede refinanciar
  while ($dt = mysql_fetch_array($dts)) {
    $aden_id = $dt['tb_credito_id'];
    $refs = $oRefinanciar->listar_cuotasdetalle_vencidas_impagas($aden_id, $cre_tip); //parametro 2: credito asiveh
      $rowsRef = mysql_num_rows($refs);
      $num_impagas += $rowsRef;
      while ($ref = mysql_fetch_array($refs)) {
        $msj .='<strong>Falta pagar la fecha: <span style="color: red;">'.$ref['tb_cuotadetalle_fec'].'</span> de la adenda ID: CAV-'.$aden_id.'</strong><br>';
      }
    mysql_free_result($refs);
  }
mysql_free_result($dts);

if($num_impagas > 0){
  echo '<strong style="color: red;">NO SE PUEDE REFINANCIAR EL CRÉDITO POR LO SIGUIENTE:</strong><br><br>';
  echo $msj;
  exit();
}*/

//verificamos si el crédito tiene fechas vencidas impagas ya que debe estar al día para poder refinanciar
/*$refs = $oRefinanciar->listar_cuotasdetalle_vencidas_impagas($cre_id, $cre_tip);
  $rowsRef = mysql_num_rows($refs);
  $num_impagas += $rowsRef;
  while ($ref = mysql_fetch_array($refs)) {
    $msj .='<strong>Falta pagar la fecha: <span style="color: red;">'.$ref['tb_cuotadetalle_fec'].'</span> del crédito</strong><br>';
  }
mysql_free_result($refs);

if($num_impagas > 0){
  echo '<strong style="color: red;">NO SE PUEDE REFINANCIAR EL CRÉDITO POR LO SIGUIENTE:</strong><br><br>';
  echo $msj;
  exit();
}*/

//calculamos SALDOS CAPITALES de las adendas y del crédito para sumar todo y ese será el precio acordado, para ello consultamos tipo de cambio del día
$result = $oMonedacambio->consultar(fecha_mysql($fecha_ref));
if($result['estado']==1){
    $cambio = $result['data']['tb_monedacambio_val'];
}
$result = null;

if(empty($cambio)){
  $result = $oMonedacambio->consultar(fecha_mysql(date('d-m-Y')));
  if($result['estado']==1){
    $cambio = $result['data']['tb_monedacambio_val'];
  }
  $result = null;
}
if(empty($cambio)){
  echo '<strong style="color: red;">INGRESE EL TIPO DE CAMBIO DEL DÍA POR FAVOR</strong><br><br>';
  exit();
}

$monto_aden_cap = 0; $capitales = '';
$result1 = $oCredito->listar_adendas_credito($cre_id); //lista adendas que esten en estado 3: desembolsado o vigentes
$adendaNovencida = 0; //para las adendas que no tienen fechas facturadas en su cronograma
if($result1['estado']==1){
    foreach ($result1['data'] as $key => $value) {
        $aden_id = intval($value['tb_credito_id']); //value3
        $aden_tip = intval($value['tb_credito_tip2']); // 4 es cuota balon
        $result2=$oCredito->mostrarUno($aden_id);
        if($result2['estado']==1){
            foreach ($result2['data'] as $key => $value1) {
                $mon_id=$value1['tb_moneda_id'];
                $adenda_preaco = $value1['tb_credito_preaco'];
                $subper_id = $value1['tb_cuotasubperiodo_id']; //1 mensual, 2 quincenal, 3 semanal
                $adenda_fecdes = $value1['tb_credito_fecdes'];
                $adenda_diaspa = intval($value1['tb_credito_diapar']); //si estuvo paralizado, estos días se restan de la Prórroga
            }
        }
        $result2 = null;
        $result2 = $oRefinanciar->ultima_cuotadetalle_facturada($aden_id, $cre_tip); //devuelve la última cuota facturada
        if($result2['estado']==1){
            $rowsRef = count($result2['data']);
            foreach ($result2['data'] as $key => $value1) {
                $cuo_id = $value1['tb_cuota_id'];
                $cuodet_id = $value1['tb_cuotadetalle_id'];
                $cuodet_fec = $value1['tb_cuotadetalle_fec'];
                $cuo_adenda_int = formato_moneda($value1['tb_cuota_int']); //interes de la cuota matriz
                $adenda_fec_prorra = $cuodet_fec;
            }
        }
        $result2 = null;
        if(intval($rowsRef) <= 0){
            //esta función debido a que hay adendas que aun no vencen sus cuotas, así que también debemos sumar su capital, por ello mostramos la primera cuotadetalle
            $result2 = $oCuotadetalle->mostrar_una_cuotadetalle_por_creditoid($aden_id,$cre_tip); //devuelve la última cuota facturada
            if($result2['estado']==1){
                $rowsRef = count($result2['data']);
                foreach ($result2['data'] as $key => $value1) {
                    $cuo_id = $value1['tb_cuota_id'];
                    $cuodet_id = $value1['tb_cuotadetalle_id'];
                    $cuodet_fec = $value1['tb_cuotadetalle_fec'];
                    $cuo_adenda_int = formato_moneda($value1['tb_cuota_int']); //interes de la cuota matriz
                    $adenda_fec_prorra = $cuodet_fec;
                }
            }
            $result2 = null;
            $adendaNovencida = 1; //esta es una adenda que no tiene fechas facturadas
        }
        
        if($rowsRef > 0){
            /*esta funcion devuelve las cuotasdetalle con fechas mayores a la fecha de la cuotadetalle facturada, para que por formula esas fechas mayores sean las impagas, ya que luego se hará un calculo de una deuda anterior que tomará todas las cuotasdetalle impagas o en paga parcial
            $dts1 = $oRefinanciar->obtener_numero_cuodasdetalle_mayorigual_cuotadetallefecha($cuo_id, $cuodet_fec);
              $dt = mysql_fetch_array($dts1);
              $cuodet_num = $dt['num_det']; //numero de cuotas detalle que están por pagar de la cuota
              $cuo_amor = $dt['tb_cuota_amo']; //amortización hasta esa fecha
              $cuo_cap = $dt['tb_cuota_cap']; //monto del capital hasta ese momento
            mysql_free_result($dts1);*/

            $periodo = 1;
            if($subper_id == 2)
              $periodo = 2;
            if($subper_id == 3)
              $periodo = 4;
            
            //1. Listamos todas las cuotasdetalle que sean menores a la últica cuota facturada, sumamos todos los intereses de todas las cuotas facturadas
            $intereses_totales = 0;
            $result2 = $oRefinanciar->cuotas_ateriores_cuotadetalle_id($aden_id, $cre_tip, $cuodet_id, $periodo);
            if($result2['estado']==1){
                foreach ($result2['data'] as $key => $value1) {
                    $intereses_totales += floatval($value1['interes_detalle']);
                }
            }
            $result2 = null;
            
            //2. Consultamos todos los ingresos que se han hecho al crédito
            $ingreso_total_adenda = 0;
            $result2 = $oIngreso->ingreso_total_por_credito_cuotadetalle($aden_id, $cre_tip, $mon_id);
            if($result2['estado']==1){
                foreach ($result2['data'] as $key => $value1) {
                    $ingreso_total_adenda = floatval($value1['importe_total']);
                }
            }
            $result2 = null;

            $moneda = 'S/.';
            if($mon_id == 2)
              $moneda = 'US$';

            $monto_prestado = $adenda_preaco;

            //$res = $cuo_cap - (($cuo_amor / $periodo) * ($periodo - $cuodet_num));
            $res = ($monto_prestado + $intereses_totales) - $ingreso_total_adenda;

            if($adendaNovencida == 1)
              $res = $adenda_preaco; //si la adenda no tiene fechas facturadas, res simplemente es el capital

            $adendaNovencida = 0; //volvemos a cero la bandera que obtiene una adnda no vencida

            if($cre_mon_id == 1 && $mon_id == 1){
              $moneda2 = 'S/.';
              $total = $res;
            }
            if($cre_mon_id == 1 && $mon_id == 2){
              $moneda2 = 'S/.';
              $total = $res * $cambio;
            }
            if($cre_mon_id == 2 && $mon_id == 1){
              $moneda2 = 'US$.';
              $total = $res / $cambio;
            }
            if($cre_mon_id == 2 && $mon_id == 2){
              $moneda2 = 'US$.';
              $total = $res;
            }

            $monto_aden_cap += $total;
            $monto_aden_cap = number_format($monto_aden_cap, 2, '.','');
            $capitales .='
              <tr>
                <td><b>Adenda</b></td>
                <td>'.$aden_id.'</td>
                <td>'.$moneda.' '. formato_moneda($res).'</td>
                <td>'.$cambio.'</td>
                <td>'.$moneda2.' '.formato_moneda($total).'</td>
                <td></td>
              </tr>';

            //obtenemos el monto del prorrateo para la liquidación, para ello contamos los días pasados desde la ultima cuota facturada para determinar cuanto de interes se debe pagar por esos dias pasados
            $fecha_cuo = mostrar_fecha($adenda_fec_prorra);
            $fecha_hoy = $fecha_ref;
            list($day, $month, $year) = explode('-', $fecha_cuo);
            $dias_del_mes = date("d", mktime(0,0,0, intval($month) + 1,0, $year));

            $fecha_ven_time = new DateTime($fecha_cuo);
            $fecha_hoy_time = new DateTime($fecha_hoy);
            //resta entre fechas

            $diff = $fecha_ven_time->diff($fecha_hoy_time)->format("%a"); //días de prorrateo
            if($diff > $adenda_diaspa)
              $diff = $diff - $adenda_diaspa; // a los días de prorrateo le restamos los días de exoneración (paralizado)
            $interes_diario = floatval($cuo_adenda_int / intval($dias_del_mes)); //monto de interes por día
            $prorrateo_adenda = floatval($interes_diario * intval($diff));
            if($aden_tip == 4)
              $prorrateo_adenda = 0;
            
            //evaluar si la última cuota facturada también es la última del crédito, ya que si es la última del crédito ya no debe generar PRORRATEO
            $result2 = $oCuota->ultima_cuota_por_credito($aden_id, $cre_tip);
            if($result2['estado']==1){
                 foreach ($result2['data'] as $key => $value1) {
                     $ultima_cuota_id = $value1['tb_cuota_id']; // esta es la última cuota del crédito
                 }
            }
            $result2 = null;

            if($cuo_id == $ultima_cuota_id)
              $prorrateo_adenda = 0; //si la cuota facturada es la última, entonces ya no se cobra prorrateo

             $prorrateo_cambio = $prorrateo_adenda;

            if($cre_mon_id == 1 && $mon_id == 2){
              $prorrateo_cambio = $prorrateo_adenda * $cambio;
            }
            if($cre_mon_id == 2 && $mon_id == 1){
              $prorrateo_cambio = $prorrateo_adenda / $cambio;
            }

            $interes_prorrateado_adendas += $prorrateo_cambio;

            $capitales .='
              <tr>
                <td><b>PRORRATEO ANDENDA:</b></td>
                <td>'.$aden_id.'</td>
                <td>'.$moneda.' '.formato_moneda($prorrateo_adenda).'</td>
                <td>'.$cambio.'</td>
                <td>'.$moneda2.' '.formato_moneda($prorrateo_cambio).'</td>
                <td>'.$diff.' días pasados, interés diairio de <b>'.$interes_diario.'.</b> Días de Prórroga <b>('.$adenda_diaspa.')</b></td>
              </tr>';
            }

        }  
    }
    
    $result1 = null;

//capital del crédito
$monto_cre_cap = 0;
$result1 = $oRefinanciar->ultima_cuotadetalle_facturada($cre_id, $cre_tip); //devuelve la última cuota facturada
  if($result1['estado']==1){
    $rowsRef = count($result1['data']);
    $cuo_id = $result1['data']['tb_cuota_id'];
    $cuodet_id = $result1['data']['tb_cuotadetalle_id'];
    $cuodet_fec = $result1['data']['tb_cuotadetalle_fec']; //fecha de la ultima cuota facturada
    $cuo_interes = formato_moneda($result1['data']['tb_cuota_int']); //interes de la cuota matriz
    $cuo_amortizacion = formato_moneda($result1['data']['tb_cuota_amo']);
    $cuo_capital = formato_moneda($result1['data']['tb_cuota_cap']);
  }
  
 $result1 = null;

if($rowsRef > 0){
  //1. Listamos todas las cuotasdetalle que sean menores a la últica cuota facturada, sumamos todos los intereses de todas las cuotas facturadas
  $periodo = 1;
  if($cre_subper_id == 2)
    $periodo = 2;
  if($cre_subper_id == 3)
    $periodo = 4;

  $intereses_totales = 0;
  $result2 = $oRefinanciar->cuotas_ateriores_cuotadetalle_id($cre_id, $cre_tip, $cuodet_id, $periodo);
  if($result2['estado']==1){
      foreach ($result2['data'] as $key => $value) {
          $intereses_totales += floatval($value['interes_detalle']);
      }
  }
  $result2= null;

  //2. Consultamos todos los ingresos que se han hecho al crédito
  $ingreso_total_credito = 0;
  $result2 = $oIngreso->ingreso_total_por_credito_cuotadetalle($cre_id, $cre_tip, $cre_mon_id);
  if($result2['estado']==1){
      foreach ($result2['data'] as $key => $value) {
          $ingreso_total_credito = floatval($value['importe_total']);
      }
  }
  $result2= null;

  $monto_prestado = $cre_preaco;

  $res = ($monto_prestado + $intereses_totales) - $ingreso_total_credito;

  $moneda_cre = 'S/.';
  if($cre_mon_id == 2)
    $moneda_cre = 'US$';

  $monto_cre_cap += $res;

  $capitales .='
    <tr class="filaTabla">
      <td id="fila" style="border-left: 1px solid #135896;"><b>CRÉDITO</b></td>
      <td id="fila">'.$cre_id.'</td>
      <td id="fila">'.$moneda_cre.' '.formato_moneda($res).'</td>
      <td id="fila">'.$cambio.'</td>
      <td id="fila">'.$moneda_cre.' '.formato_moneda($res).'</td>
      <td id="fila"></td>
    </tr>';

  //obtenemos el monto del prorrateo para la liquidación, para ello contamos los días pasados desde la ultima cuota facturada para determinar cuanto de interes se debe pagar por esos dias pasados
  $fecha_cuo = mostrar_fecha($cuodet_fec);
  $fecha_hoy = $fecha_ref;
  list($day, $month, $year) = explode('-', $fecha_cuo);
  $dias_del_mes = date("d", mktime(0,0,0, intval($month) + 1,0, $year));

  $fecha_ven_time = new DateTime($fecha_cuo);
  $fecha_hoy_time = new DateTime($fecha_hoy);
  //resta entre fechas
  //para ello tenemos que calcular el interés el mes entrante, lo hacemos con los datos de la cuota facturada
  $interes_valido = ($cuo_capital - $cuo_amortizacion);
  $interes_valido = $interes_valido * $credito_int / 100;
  $interes_valido = formato_numero($interes_valido);

  $diff = $fecha_ven_time->diff($fecha_hoy_time)->format("%a"); //días de prorrateo
  $dias_prorra = $diff;
  if($diff > $cre_diaspa)
    $diff = $diff - $cre_diaspa; // a los días de prorrateo le restamos los días de exoneración (paralizado)
  else
    $diff = 0;
  $interes_diario = floatval($interes_valido / intval($dias_del_mes)); //monto de interes por día
  $interes_prorrateado = floatval($interes_diario * intval($diff));
  $interes_prorrateado = formato_numero($interes_prorrateado);

  $head = 'interes cuota '.$interes_valido.' / dias de del mes '.$dias_del_mes.' //  dias pasados '.$diff.' / int diario '.$interes_diario.' // '.$interes_prorrateado.' // ingresos: '.$ingreso_total_credito.' / '.$dias_prorra;
  $capitales .='
    <tr class="filaTabla">
      <td id="fila" style="border-left: 1px solid #135896;"><b>PRORRATEO</b></td>
      <td id="fila">'.$cre_id.'</td>
      <td id="fila">'.$moneda_cre.' '.formato_moneda($interes_prorrateado).'</td>
      <td id="fila">'.$cambio.'</td>
      <td id="fila">'.$moneda_cre.' '.formato_moneda($interes_prorrateado).'</td>
      <td id="fila">'.$diff.' días pasados, interés diario de <b>'.$interes_diario.'.</b> Días de Prórroga <b>('.$cre_diaspa.')</b></td>
    </tr>';
}
else{
  /*el credito no tiene ninguna cuota facturada, por ende solo se calculara si hizo pagos para restarle del monto de linea aprobada
  $dts = $oIngreso->ingreso_total_por_credito_cuotadetalle($cre_id, $cre_tip, $cre_mon_id); // parametro 1 ya que es el estado del ingreso 1 activo
    while ($dt = mysql_fetch_array($dts)) {
      $importe_total += floatval($dt['importe_total']);
    }
  mysql_free_result($dts);*/

  $res = floatval($cre_preaco);

  $moneda_cre = 'S/.';
  if($cre_mon_id == 2)
    $moneda_cre = 'US$';

  $monto_cre_cap +=$res;
  $capitales .='
    <tr class="filaTabla">
      <td id="fila" style="border-left: 1px solid #135896;"><b>CRÉDITO</b></td>
      <td id="fila">'.$cre_id.'</td>
      <td id="fila">'.$moneda_cre.' '.formato_moneda($res).'</td>
      <td id="fila">'.$cambio.'</td>
      <td id="fila">'.$moneda_cre.' '.formato_moneda($res).'</td>
      <td id="fila"></td>
    </tr>';


  $result2 = $oCuota->primera_cuota_por_credito($cre_tip, $cre_id);
  if($result2['estado']==1){
    foreach ($result2['data'] as $key => $value) {
        $cuo_id = $value['tb_cuota_id'];
        $cuodet_id = $value['tb_cuotadetalle_id'];
        $cuodet_fec = $value['tb_cuota_fec']; //fecha de la ultima cuota facturada
        $cuo_interes = formato_moneda($value['tb_cuota_int']); //interes de la cuota matriz
        $cuo_amortizacion = formato_moneda($value['tb_cuota_amo']);
        $cuo_capital = formato_moneda($value['tb_cuota_cap']);
    }
  }
$result2 = null;

  //obtenemos el monto del prorrateo para la liquidación, para ello contamos los días pasados desde la ultima cuota facturada para determinar cuanto de interes se debe pagar por esos dias pasados
  $fecha_cuo = $credito_fecfac; //como es cuota sin facturar, tomamos la fecha del crédito
  $fecha_hoy = $fecha_ref;
  list($day, $month, $year) = explode('-', $fecha_cuo);
  $dias_del_mes = date("d", mktime(0,0,0, intval($month) + 1,0, $year));

  $fecha_ven_time = new DateTime($fecha_cuo);
  $fecha_hoy_time = new DateTime($fecha_hoy);
  //resta entre fechas
  //para ello tenemos que calcular el interés el mes entrante, lo hacemos con los datos de la cuota facturada
  $interes_valido = ($cuo_capital - $cuo_amortizacion);
  $interes_valido = $interes_valido * $credito_int / 100;
  $interes_valido = formato_numero($interes_valido);

  $diff = $fecha_ven_time->diff($fecha_hoy_time)->format("%a"); //días de prorrateo
  $dias_prorra = $diff;
  if($diff > $cre_diaspa)
    $diff = $diff - $cre_diaspa; // a los días de prorrateo le restamos los días de exoneración (paralizado)
  else
    $diff = 0;
  $interes_diario = floatval($interes_valido / intval($dias_del_mes)); //monto de interes por día
  $interes_prorrateado = floatval($interes_diario * intval($diff));
  $interes_prorrateado = formato_numero($interes_prorrateado);

  $head = 'fecha cuo: '.$fecha_cuo.' / fec ref: '.$fecha_hoy.'/ interes cuota '.$interes_valido.' / dias de del mes '.$dias_del_mes.' //  dias pasados '.$diff.' / int diario '.$interes_diario.' // '.$interes_prorrateado.' // ingresos: '.$ingreso_total_credito.' / '.$dias_prorra;

  $capitales .='
    <tr class="filaTabla">
      <td id="fila" style="border-left: 1px solid #135896;"><b>PRORRATEO</b></td>
      <td id="fila">'.$cre_id.'</td>
      <td id="fila">'.$moneda_cre.' '. formato_moneda($interes_prorrateado).'</td>
      <td id="fila">'.$cambio.'</td>
      <td id="fila">'.$moneda_cre.' '.formato_moneda($interes_prorrateado).'</td>
      <td id="fila">'.$diff.' días pasados, interés diario de <b>'.$interes_diario.'.</b> Días de Prórroga <b>('.$cre_diaspa.')</b></td>
    </tr>';

}

//formato_moneda -> devuelve un flot con dos decimales sin comas
$amorti_prev = 0; //amortización previo
$mod_id = $cre_tip; //el modulo en INGRESO para amortizacion es 1 menor, 2 asve, 3 garve, 4 hipo
$modide = $cre_id; //el modide en ingreso para amortización es el ID  del crédito

$result2 = $oIngreso->mostrar_por_modulo($mod_id,$modide,$cre_mon_id, '1'); // parametro 1 ya que es el estado del ingreso 1 activo
if($result2['estado']==1){
    foreach ($result2['data'] as $key => $value) {
        $head = ' /// '.$value['tb_ingreso_id'].' // '.$value['tb_ingreso_imp'].' // '.$value['tb_moneda_id'].' --- ';
        $amorti_prev += floatval($value['tb_ingreso_imp']);
    }
}
$result2 = null;

$amorti_prev = formato_moneda($amorti_prev); //amortización previo a este crédito
$total_capital = formato_moneda($monto_cre_cap + $monto_aden_cap + $interes_prorrateado + $interes_prorrateado_adendas);
$saldo_capital = formato_moneda($total_capital - $amorti_prev);

$prorrateo_total = formato_moneda($interes_prorrateado + $interes_prorrateado_adendas)
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_refinanciar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Refinanciar Crédito</h4>
        </div>
        <form id="form_refi">
          <input name="action_credito" id="action_credito" type="hidden" value="insertar">
          <input type="hidden" name="hdd_cli_id" value="<?php echo $cli_id;?>">
          <input name="hdd_cre_id" id="hdd_cre_id" type="hidden" value="<?php echo $cre_id?>">
          <input name="hdd_cre_tip" id="hdd_cre_tip" type="hidden" value="<?php echo $cre_tip?>">
          <input name="hdd_mon_id" id="hdd_mon_id" type="hidden" value="<?php echo $cre_mon_id;?>">
          <input type="hidden" name="hdd_prorrateo" id="hdd_prorrateo" value="<?php echo $prorrateo_total; ?>">
          <input name="hdd_capital" id="hdd_capital" type="hidden" value="<?php echo $total_capital;?>">
          <input name="hdd_capital" id="hdd_matriz" type="hidden" value="<?php echo $cre_matriz;?>">
          <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                        <div class="row">
                              <div class="col-lg-12">
                                  <div><?php echo $head . ' --//-- '.$cre_matriz;?></div>
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                      <h3 class="box-title">Capital de Adendas y Crédito</h3>
                                      <div class="box-tools pull-right">
                                      <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                                      </div>
                                    </div>
                                    <div class="box-body no-padding">
                                     
                                      <table id="tbl_cap" class="table table-bordered table-hover">
                                        <thead>  
                                          <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
                                            <th id="filacabecera">TIPO</th>
                                            <th id="filacabecera">ID</th>
                                            <th id="filacabecera" width="12%">CAPITAL</th>
                                            <th id="filacabecera">CAMBIO</th>
                                            <th id="filacabecera" width="12%">TOTAL</th>
                                            <th id="filacabecera">DETALLE</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php echo $capitales;?>
                                          <tr class="filaTabla">
                                            <td colspan="4"><b>Total Capital:</b></td>
                                            <td><?php echo $moneda_cre.' '. formato_moneda($total_capital);?></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                </div>
                        <?php if($cre_tip == 2 || $cre_tip == 3):?>
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                      <h3 class="box-title">Datos del Vehículo</h3>
                                      <div class="box-tools pull-right">
                                      <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                                      </div>
                                    </div>
                                    <div class="box-body no-padding">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Vencimiento GPS :</label>
                                                    <div class="input-group">
                                                        <div class='input-group date' id='datetimepicker1'>
                                                            <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecgps" id="txt_cre_fecgps" value="<?php echo mostrar_fecha($cre_fecgps); ?>" readonly/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Vencimiento STR :</label>
                                                    <div class="input-group">
                                                        <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecseg" id="txt_cre_fecseg" value="<?php echo mostrar_fecha($cre_fecseg); ?>" readonly/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Vencimiento SOAT :</label>
                                                    <div class="input-group">
                                                        <div class='input-group date' id='datetimepicker3'>
                                                            <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecren" id="txt_cre_fecren" value="<?php echo mostrar_fecha($cre_fecren); ?>" readonly/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Vencimiento I.V :</label>
                                                    <div class="input-group">
                                                        <div class='input-group date' id='datetimepicker4'>
                                                            <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fpagimp" id="txt_cre_fpagimp" value="<?php echo mostrar_fecha($cre_fpagimp); ?>" readonly/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php endif;?>
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Datos del Crédito</h3>
                                    <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btn_creditomenor_minus"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body no-padding">
                                    <div class="row">
                                        <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="cmb_cuo_tip" class="control-label">Tipo de Cuota :</label>
                                                    <select class="form-control input-sm" name="cmb_cuo_tip" id="cmb_cuo_tip">
                                                        <option value="">- </option>
                                                        <option value="3">LIBRE</option>
                                                        <option value="4">FIJA</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="cmb_cre_tipref" class="control-label">Tipo Refinanciado :</label>
                                                <select class="form-control input-sm" name="cmb_cre_tipref" id="cmb_cre_tipref">
                                                    <option value="">- </option>
                                                    <option value="5">REFINANCIADO AMORTIZADO</option>
                                                    <option value="6">REFINANCIADO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for="cmb_cre_per" class="control-label">Periodo :</label>
                                                <select class="form-control input-sm" name="cmb_cre_per" id="cmb_cre_per">
                                                    <option value="4">MENSUAL</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                          <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="cmb_cre_subper" class="control-label">Sub Periodo :</label>
                                                    <select class="form-control input-sm" name="cmb_cre_subper" id="cmb_cre_subper">
                                                        <option value="1">MENSUAL</option>
                                                        <option value="2">QUINCENAL</option>
                                                        <option value="3">SEMANAL</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="row">
                                                  <div class="col-lg-10">
                                                      <div class="form-group">
                                                        <label for="cmb_mon_id" class="control-label">Moneda :</label>
                                                        <select class="form-control input-sm" name="cmb_mon_id" id="cmb_mon_id" disabled>
                                                            <?php require_once('../moneda/moneda_select.php');?>
                                                          </select>
                                                      </div>
                                                  </div>
                                                 <!-- <div class="col-lg-2">
                                                    <a class="btn btn-primary btn-xs" title="Agregar" onclick="monedacambio_form('insertar')"><i class="fa fa-plus"></i></a>
                                                  </div>-->
                                               </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="txt_cre_tipcam" class="control-label">Cambio :</label>
                                                    <input type="text" name="txt_cre_tipcam" id="txt_cre_tipcam" class="form-control input-sm moneda" value="" readonly>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for="txt_cre_preaco" class="control-label">Precio Acordado :</label>
                                                <input type="text" name="txt_cre_preaco" id="txt_cre_preaco" class="form-control input-sm moneda2" value="<?php echo formato_moneda($saldo_capital)?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="txt_cre_amor" class="control-label">Amortización :</label>
                                                    <input type="text" name="txt_cre_amor" id="txt_cre_amor" class="form-control input-sm moneda2" value="<?php echo formato_moneda($amorti_prev)?>" readonly>                   
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group" style="margin-top: 4px">
                                                    <label for="txt_cre_amor" class="control-label"> </label><br>
                                                    <a class="btn btn-primary btn-sm" title="Amortizar" onclick="amortizar_form('amortizar')"><i class="fa fa-plus"></i> Amortizar</a>
                                                    <a class="btn btn-primary btn-sm" title="Liquidar" onclick="amortizar_form('liquidar')"><i class="fa fa-plus"></i> Liquidar</a>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                          <div class="row">
                                              <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="txt_cre_int" class="control-label">Interés (%) :</label>
                                                    <input type="text" name="txt_cre_int" id="txt_cre_int" class="form-control input-sm porcentaje" value="">
                                                </div>
                                              </div>
                                              <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="txt_cre_numcuo" class="control-label">N° Cuotas :</label>
                                                    <input type="text" name="txt_cre_numcuo" id="txt_cre_numcuo" class="form-control input-sm porcentaje" value="">
                                                </div>
                                              </div>
                                          </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for="txt_cre_linapr" class="control-label">Línea Aprobada :</label>
                                                <input type="text" name="txt_cre_linapr" id="txt_cre_linapr" class="form-control input-sm moneda2" value="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <label for="txt_cre_obs" class="control-label">Observación :</label>
                                            <div class="form-group">
                                                
                                                <textarea class="form-control mayus" name="txt_cre_obs" id="txt_cre_obs" rows="2" cols="35"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="cmb_cuedep_id" class="control-label">Cuenta a Depositar :</label>
                                                    <select class="form-control input-sm" name="cmb_cuedep_id" id="cmb_cuedep_id">
                                                      <?php require_once('../cuentadeposito/cuentadeposito_select.php');?>
                                                    </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">

                                        </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Fecha de Credito :</label>
                                                <div class="input-group">
                                                    <div class='input-group date' id='datetimepicker5'>
                                                        <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_feccre" id="txt_cre_feccre" value="<?php echo mostrar_fecha($cre_feccre); ?>" readonly/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>   
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Fecha de Desembolso :</label>
                                                <div class="input-group">
                                                    <div class='input-group date' id='datetimepicker6'>
                                                        <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecdes" id="txt_cre_fecdes" value="<?php echo mostrar_fecha($cre_fecdes); ?>" readonly/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>   
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Fecha de Facturación :</label>
                                                <div class="input-group">
                                                    <div class='input-group date' id='datetimepicker7'>
                                                        <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecfac" id="txt_cre_fecfac" value="<?php echo mostrar_fecha($cre_fecfac); ?>" readonly/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>   
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Fecha de Prorrateo :</label>
                                                <div class="input-group">
                                                    <div class='input-group date' id='datetimepicker8'>
                                                        <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecpro" id="txt_cre_fecpro" value="<?php echo mostrar_fecha($cre_fecpro); ?>" readonly/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>   
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Fecha de Entrega :</label>
                                                    <div class="input-group">
                                                        <div class='input-group date' id='datetimepicker9'>
                                                            <input type='text' maxlength="10" class="form-control input-sm" name="txt_cre_fecent" id="txt_cre_fecent" value="<?php echo mostrar_fecha($cre_fecent); ?>" readonly/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                              <div id="msj_credito_ref" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px;"></div>
                              <div id="div_credito_cronograma"></div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-info" id="btn_guardar_creditogarveh">Guardar</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </form>

    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/refinanciar/refinanciar_form.js?ver=437433';?>"></script>