function buscaveh_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"buscaveh/buscaveh_tabla.php",
    async:false,
    dataType: "html",                      
    data: $('#form_buscaveh_filtro').serialize(),
    beforeSend: function() {
      $('#div_buscaveh_tabla').html('<option value="">Cargando...</option>');
    },
    success: function(html){
      $('#div_buscaveh_tabla').html(html);
    },
    complete: function(data){
      console.log(data);
    }
  });
}
$(document).ready(function() {
  console.log('Perfil menu');

  $( "#txt_cliente_nom" ).autocomplete({
    minLength: 1,
    source: VISTA_URL+"cliente/cliente_autocomplete.php",
    select: function(event, ui){
      $("#hdd_cliente_id").val(ui.item.cliente_id);
      //buscaveh_tabla();               
    }
  });
});