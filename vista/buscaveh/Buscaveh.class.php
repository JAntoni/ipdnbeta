<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Buscaveh extends Conexion{

    function mostrarUno($buscaveh_id){
      try {
        $sql = "SELECT * FROM tb_buscaveh WHERE tb_buscaveh_id =:buscaveh_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":buscaveh_id", $buscaveh_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_vehiculos($vehiculo_ser, $vehiculo_pla, $cliente_id){
      try {
        $sql = "SELECT 2 AS tipo,tb_credito_id, tb_cliente_nom, tb_cliente_tel, tb_cliente_cel, tb_cliente_telref, tb_credito_vehpla, tb_credito_vehsermot, tb_credito_est FROM tb_creditoasiveh asi INNER JOIN tb_cliente cli on cli.tb_cliente_id = asi.tb_cliente_id  where asi.tb_credito_xac =1 and asi.tb_credito_tip1 in(1,4)";

        if(!empty($vehiculo_ser))
          $sql.=" and tb_credito_vehsermot = '".$vehiculo_ser."'";
        if(!empty($vehiculo_pla))
          $sql.=" and tb_credito_vehpla = '".$vehiculo_pla."'";
        if(!empty($cliente_id))
          $sql.=" and cli.tb_cliente_id =".$cliente_id;

        $sql .=" UNION SELECT 3 as tipo,tb_credito_id, tb_cliente_nom, tb_cliente_tel, tb_cliente_cel, tb_cliente_telref, tb_credito_vehpla, tb_credito_vehsermot, tb_credito_est FROM tb_creditogarveh gar INNER JOIN tb_cliente cli on cli.tb_cliente_id = gar.tb_cliente_id  where gar.tb_credito_xac =1 and gar.tb_credito_tip != 2";

        if(!empty($vehiculo_ser))
          $sql.=" and tb_credito_vehsermot = '".$vehiculo_ser."'";
        if(!empty($vehiculo_pla))
          $sql.=" and tb_credito_vehpla = '".$vehiculo_pla."'";
        if(!empty($cliente_id))
          $sql.=" and cli.tb_cliente_id =".$cliente_id;

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    //buscamos el vehículo en Garveh y Asveh por placa y retornamos el detalle de dicho vehículo
    function buscar_placa_garveh_asiveh($vehiculo_pla){
      try {
        $sql = "SELECT 2 AS tipo, tb_credito_id, tb_credito_vehpla, tb_credito_vehsermot, tb_credito_vehano, tb_vehiculomarca_nom, tb_vehiculomodelo_nom 
          FROM tb_creditoasiveh asi
          INNER JOIN tb_vehiculomarca mar on mar.tb_vehiculomarca_id = asi.tb_vehiculomarca_id
          INNER JOIN tb_vehiculomodelo model on model.tb_vehiculomodelo_id = asi.tb_vehiculomodelo_id 
          where asi.tb_credito_xac =1 and asi.tb_credito_tip1 in(1,4) 
          AND (REPLACE(LOWER(tb_credito_vehpla), '-', '') = REPLACE(LOWER(:vehiculo_pla),'-','')) AND tb_credito_xac = 1";

        $sql .=" UNION
          SELECT 3 AS tipo, tb_credito_id, tb_credito_vehpla, tb_credito_vehsermot, tb_credito_vehano, tb_vehiculomarca_nom, tb_vehiculomodelo_nom 
          FROM tb_creditogarveh garv
          INNER JOIN tb_vehiculomarca mar on mar.tb_vehiculomarca_id = garv.tb_vehiculomarca_id
          INNER JOIN tb_vehiculomodelo model on model.tb_vehiculomodelo_id = garv.tb_vehiculomodelo_id
          WHERE garv.tb_credito_xac =1 and garv.tb_credito_tip != 2 
          and REPLACE(LOWER(garv.tb_credito_vehpla), '-', '') = REPLACE(LOWER(:vehiculo_pla),'-','') AND garv.tb_credito_xac = 1
          LIMIT 1
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculo_pla", $vehiculo_pla, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
