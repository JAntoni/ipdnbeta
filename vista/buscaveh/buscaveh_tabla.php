<?php
  session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  require_once('Buscaveh.class.php');
  $oBuscaveh = new Buscaveh();

  $vehiculo_ser = trim($_POST['txt_vehiculo_ser']);
  $vehiculo_pla = trim($_POST['txt_vehiculo_pla']);
  $cliente_id = trim($_POST['hdd_cliente_id']);

  if(empty($vehiculo_ser) && empty($vehiculo_pla) && empty($cliente_id))
    $result['estado'] =  0;
  else
    $result = $oBuscaveh->listar_vehiculos($vehiculo_ser, $vehiculo_pla, $cliente_id);

?>
<?php if($result['estado'] == 1): ?>
  <table id="tbl_buscavehs" class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>CREDITO ID</th>
        <th>CLIENTE</th>
        <th>ESTADO</th>
        <th>TELÉFONO 1</th>
        <th>TELÉFONO 2</th>
        <th>TELÉFONO 3</th>
        <th>PLACA VEHÍCULO</th>
        <th>SERIE VEHÍCULO</th>
      </tr>
    </thead>
    <tbody> <?php
      foreach ($result['data'] as $key => $value) {
          $id = 'CAV-'.$value['tb_credito_id'];
          if($value['tipo'] == 3)
            $id = 'CGV-'.$value['tb_credito_id'];
        ?>
        <tr>
          <td><?php echo $id;?></td>
          <td><?php echo $value['tb_cliente_nom'];?></td>
          <td>
            <?php
              if($value['tb_credito_est'] == 7 || $value['tb_credito_est'] == 8)
                echo 'NO VIGENTE';
              else
                echo '<strong style="color: green;">VIGENTE</strong>';
            ?>
          </td>
          <td><?php echo $value['tb_cliente_tel'];?></td>
          <td><?php echo $value['tb_cliente_cel'];?></td>
          <td><?php echo $value['tb_cliente_telref'];?></td>
          <td><?php echo $value['tb_credito_vehpla'];?></td>
          <td><?php echo $value['tb_credito_vehsermot'];?></td>
        </tr> <?php
      }
      $result = null; ?>
    </tbody>
  </table>
<?php endif; ?>

<?php if($result['estado'] != 1): ?>
  <table id="tbl_buscavehs" class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>CREDITO ID</th>
        <th>CLIENTE</th>
        <th>ESTADO</th>
        <th>TELÉFONO 1</th>
        <th>TELÉFONO 2</th>
        <th>TELÉFONO 3</th>
        <th>PLACA VEHÍCULO</th>
        <th>SERIE VEHÍCULO</th>
      </tr>
    </thead>
    <tbody>
      <?php echo $result['mensaje'];?>
    </tbody>
  </table>
<?php endif; ?>