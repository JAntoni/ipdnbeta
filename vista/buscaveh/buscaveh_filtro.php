<?php
	$filtro_fec1 = date('01-01-Y');
	$filtro_fec2 = date('d-m-Y');
?>
<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-info">
		  <div class="panel-body">
		    <form id="form_buscaveh_filtro" class="form-inline" role="form">
          <div class="form-group">
            <label for="txt_vehiculo_ser" class="control-label">Serie Mot:</label>
            <input type="text" name="txt_vehiculo_ser" id="txt_vehiculo_ser" class="form-control input-sm mayus" value="">
          </div>
          <div class="form-group">
            <label for="txt_vehiculo_pla" class="control-label">Placa:</label>
            <input type="text" name="txt_vehiculo_pla" id="txt_vehiculo_pla" class="form-control input-sm mayus" value="">
          </div>
          <div class="form-group">
            <label for="txt_cliente_nom" class="control-label">Cliente:</label>
            <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="form-control input-sm mayus" value="" size="40">
            <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" class="form-control input-sm mayus" value="">
          </div>
          <button type="button" class="btn btn-info btn-sm" onclick="buscaveh_tabla()"><i class="fa fa-search"></i> Buscar</button>
        </form>
		  </div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12" id="buscaveh_mensaje_opc" style="padding-top: 5px;"> 
	</div>
</div>