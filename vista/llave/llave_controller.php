<?php
require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../llave/Llave.class.php');
$ollave = new Llave();
require_once('../creditogarveh/Creditogarveh.class.php');
$ocredito = new Creditogarveh();
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../vehiculoclase/Vehiculoclase.class.php');
$oVclase = new Vehiculoclase();
require_once('../vehiculomarca/Vehiculomarca.class.php');
$oVmarca = new Vehiculomarca();
require_once('../vehiculomodelo/Vehiculomodelo.class.php');
$oVmodelo = new Vehiculomodelo();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();

//$_SESSION['usuario_id']
$action = $_POST['action'];
$action2 = $_POST['action2']; // se recibe con el nombre id del html
/* print_r($_POST); */
//print_r($_POST['txt_llave_tip']);
//print_r($_POST['cmb_numcasillero']);

$llave_usureg = $_SESSION['usuario_id']; //id del usuario que registra llave
$cliente_id = $_POST['hdd_llave_clienteid']; //id del usuario
$llave_creditoid = $_POST['cmb_credito']; //id del credito  
$vehiculoclase_id = $_POST['cmb_clasevehiculo'];
$vehiculomarca_id = $_POST['cmb_marcavehiculo'];
$vehiculomodelo_id = $_POST['cmb_modelovehiculo'];
$tipo_actual  = $_POST['hdd_llave_tipo_actual'];
$motor  = $_POST['hdd_llave_motor'];
$chasis  = $_POST['hdd_llave_chasis'];
$hiddenmotor = $_POST['hdd_llave_motor'];
$hiddenchasis = $_POST['hdd_llave_chasis'];
$vehpla = $_POST['hdd_llave_placa'];
$creditotipo_id = $_POST['hdd_llave_tipocredito'];
$garantia_id = $_POST['hdd_llave_garantiaid'];
$llave_id = $_POST['hdd_llave_id']; 

/* print_r($tipo_actual);
print_r($tipo_nuevo); */

$llave_obs = $_POST['txt_comentario'];
$llave_numcasillero = $_POST['cmb_numcasillero'];
$llave_tipollave = $_POST['txt_llave_tip'];





/* $data['estado'] = 0;
$data['mensaje']=$action;
echo json_encode($data);
exit(); */

/* $data['mensaje'] = error_get_last()['message']; */
//recogiendo los datos
$ollave->llave_usureg = $llave_usureg;
$ollave->cliente_id = $cliente_id;
$ollave->vehiculoclase_id = intval($vehiculoclase_id);
$ollave->vehiculomarca_id = $vehiculomarca_id;
$ollave->vehiculomodelo_id = $vehiculomodelo_id;
$ollave->llave_obs = $llave_obs;
$ollave->llave_numcasillero = $llave_numcasillero;
$ollave->llave_tipollave = $llave_tipollave;
$ollave->credito_id = $llave_creditoid;
$ollave->vehpla = $vehpla;
$ollave->creditotipo_id = $creditotipo_id;
$ollave->llave_id = $llave_id;

/* $ollave->nromotcha = $nromotcha; */


//print_r($ollave->llave_tipollave);
//print_r($ollave->llave_numcasillero);

if ($hiddenmotor == "") {
  $ollave->nromotcha = $hiddenchasis;
  $nromotcha = $hiddenchasis;
} else {
  $ollave->nromotcha = $hiddenmotor;
  $nromotcha = $hiddenmotor;
}

if ($action == 'insertar') {
  
  $tipo_nuevo = $_POST['txt_llave_tip'];

  // Verificar si ya existe una llave para el cliente y el tipo seleccionado
  $llave_existente = $ollave->consultarLlaveExistente($cliente_id, $nromotcha, $llave_tipollave, 0);

  if ($llave_existente['cantidad_resultados'] > 0) {
      $data['estado'] = 0;
      $data['mensaje'] = 'Ya existe una llave para este cliente y tipo seleccionado.';
      echo json_encode($data);
      return;
  } else {
      // Insertar la nueva llave
      $res = $ollave->insertar();
      if (intval($res['estado']) == 1) {
          // Registrar en el historial
          $oHist->setTbHistUsureg($_SESSION['usuario_id']);
          $oHist->setTbHistNomTabla('tb_llave');
          $oHist->setTbHistRegmodid($res['llave_id']); // Asumiendo que insertar() devuelve el ID de la nueva llave
          $mensaje = 'La llave fue registrada en el casillero número: ' . $llave_numcasillero . '.';
          $oHist->setTbHistDet($mensaje);
          $oHist->insertar();

          $data['estado'] = 1;
          $data['mensaje'] = 'Llave registrada correctamente.';
      }
  }

  echo json_encode($data);
}elseif ($action == 'modificar') {

  $tipo_nuevo = $_POST['txt_llave_tip'];
  $llave_existente = $ollave->consultarLlaveExistente($cliente_id, $nromotcha, $llave_tipollave, $_POST['hdd_llave_id']);

  if ($llave_existente['cantidad_resultados'] > 0) {
    $data['estado'] = 0;
    $data['mensaje'] = 'Ya existe una llave para este cliente y tipo seleccionado. ';
    echo json_encode($data);
    return;
  }

  if ($action2 != 'ModificarPropietario') {
    $oModifcar1 = intval($_POST['hdd_llave_id']);
    $oModifcar2 = intval($_POST['cmb_credito']);
    // Asigna al objeto
    $ollave->llave_id = $oModifcar1;
    $ollave->credito_id = $oModifcar2;

    try {
      if ($ollave->modificar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Llave modificada correctamente.';
      }
    } catch (Exception $e) {
      $data['mensaje'] = 'Existe un error al modificar la llave: ' . $e->getMessage();
    }
  } else {
    // Función para modificar el propietario
    $cliente_nuevo = $_POST['hdd_cliente_nuevo'];
    $ollave->cliente_id = $cliente_nuevo;

    $llaveid = $_POST['hdd_llave_id'];
    $ollave->llave_id = $llaveid;

    $creditonuevo = $_POST['cmb_credito'];


    try {
      if (intval($cliente_nuevo) > 0 && intval($creditonuevo) > 0) {

        $ollave->modificarPropietario();
        //$oCreditogarveh->modificar_campo($credito_id, 'tb_cliente_id', $cliente_nuevo, 'INT'); //SI LIQUIDO EL TOTAL O NO
        $oHist->setTbHistUsureg($_SESSION['usuario_id']);
        $oHist->setTbHistNomTabla('tb_llave');
        $oHist->setTbHistRegmodid(intval($llaveid));
        // Función para modificar el propietario
        // Función para modificar el propietario
        $cliente_nuevo = $_POST['hdd_cliente_nuevo'];
        $ollave->cliente_id = $cliente_nuevo;

        $llaveid = $_POST['hdd_llave_id'];
        $ollave->llave_id = $llaveid;

        // Obtener valores adicionales para el mensaje
        $cliente_antiguo = $_POST["txt_llave_clienteid"];

        // Modificar el mensaje con los valores obtenidos
        $mensaje = 'Modificó el propietario Antiguo ' . $cliente_antiguo . ' al nuevo propietario ' . $_POST["txt_cliente_nuevo"] . ' .';

        $oHist->setTbHistDet($mensaje);
        $oHist->insertar();

        $data['estado'] = 1;
        $data['mensaje'] = 'Llave modificada correctamente.';
      } else {
        $data['estado'] = 0;
        $data['mensaje'] = 'No existe cliente o credito ';
      }
    } catch (Exception $e) {
      $data['mensaje'] = 'Existe un error al modificar el propietario de la llave: ' . $e->getMessage();
    }
  }

  echo json_encode($data);
} elseif ($action == 'eliminar') {
  $llave_id = intval($_POST['hdd_llave_id']);

  $data['estado'] = 0;

  try {
    if ($ollave->eliminar($llave_id)) {
      $data['estado'] = 1;
      $data['mensaje'] = 'Llave eliminada correctamente.';
    }
  } catch (Exception $e) {
    $data['mensaje'] = 'Existe un error al eliminar la llave: ' . $e->getMessage();
  }

  echo json_encode($data);
} elseif ($action == 'llenardatos') {
  $creditotipo_id = $_POST['creditotipo_id'];
  $credito_id = $_POST['credito_id'];
  
  $data['estado'] = 0;

 /*  $data['mensaje']="$credito_id, $creditotipo_id";
  echo json_encode($data);
  exit(); */

  try {
      $res = $ollave->llenarDatosCreditos($credito_id, $creditotipo_id);
      if (intval($res['estado']) == 1) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Datos Obtenidos: ' . $res['data']['clase_vehiculo'] . $res['data']['marca_vehiculo'] . $res['data']['modelo_vehiculo'];
        $data['claseid'] = $res['data']['clase_vehiculo'];
        $data['marcaid'] = $res['data']['marca_vehiculo'];
        $data['modeloid'] = $res['data']['modelo_vehiculo'];
        $data['motor'] = $res['data']['vehiculo_motor'];
        $data['chasis'] = $res['data']['vehiculo_chasis'];
        $data['placa'] = $res['data']['vehiculo_placa'];
      }
    $res = NULL;
  } catch (Exception $e) {
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al obtener datos de la llave: ' . $e->getMessage();
  }
  echo json_encode($data);
}

