<?php
require_once('../../core/usuario_sesion.php');
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../comentario/Comentario.class.php');
$oComentario = new Comentario();
require_once ('./Llave.class.php');
$oEstadollave= new Llave();
require_once('../funciones/fechas.php');

$llave_id=$_POST['llave_id'];
$lista = '';

$resultado = $oHist->filtrar_historial_por('tb_llave', $llave_id); //arreglo que contiene estado, mensaje, data

if($resultado['estado'] == 1){
    foreach($resultado['data'] as $historiales => $hist){
        $lista .='
	  			<li>
				    <i class="fa fa-envelope bg-blue"></i>
				    <div class="timeline-item">';
            $lista .='
				      <span class="time"><i class="fa fa-clock-o"></i> '.mostrar_fecha_hora($hist['tb_hist_fecreg']).'</span>
				      <div class="user-block">
                      <img class="img-circle img-bordered-sm" src="'.$hist['tb_usuario_fot'].'" alt="user image">
                      <span class="username">
                        <a href="#">'.$hist['tb_usuario_nom'].' '.$hist['tb_usuario_ape'].'</a>
                      </span>
                      <span class="description">'.$hist['tb_hist_det'].'</span>
                    </div>
				    </div>
				  </li>
	  		';
    }
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_llave_historial_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
            </button>
            <h4 class="modal-title">Historial de Registro de llaves</h4>
        </div>
        <div class="modal-body">
            <ul class="timeline">
                <?php echo $lista;?>
            </ul>
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/historial/historial_form.js?ver=6444545300434'; ?>"></script>