<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Llave extends Conexion
{

    public $llave_id;
    public $llave_xac;
    public $llave_reg;
    public $llave_usureg;
    public $cliente_id;
    public $vehiculoclase_id;
    public $vehiculomarca_id;
    public $vehiculomodelo_id;
    public $llave_obs;
    public $llave_numcasillero;
    public $llave_tipollave;
    public $creditotipo_id;
    public $credito_id;
    public $nromotcha;
    public $vehpla;
    public $estadollave_tipo = 1;

    //datos de temp_llave
    public $casillero;
    public $nombreCliente;
    public $tipoCredito;

    function insertar()
    {
        $this->dblink->beginTransaction();

        try {


            $sql = "INSERT INTO tb_llave (
                tb_llave_usureg,
                tb_cliente_id,
                tb_vehiculoclase_id,
                tb_vehiculomarca_id,
                tb_vehiculomodelo_id,
                tb_llave_obs,
                tb_llave_numcasillero,
                tb_llave_tipollave,
                tb_creditotipo_id,
                tb_credito_id,
                tb_estadollave_tipo,
                tb_llave_vehsermot_chasis,
                tb_llave_vehpla
                )
         VALUES (
                :llave_usureg,
                :cliente_id,
                :vehiculoclase_id,
                :vehiculomarca_id,
                :vehiculomodelo_id,
                :llave_obs,
                :llave_numcasillero,
                :llave_tipollave,
                :creditotipo_id,
                :credito_id,
                :estadollave_tipo,
                :llave_vehsermot_chasis,
                :llave_vehpla);";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":llave_usureg", $this->llave_usureg, PDO::PARAM_STR);
            $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":vehiculoclase_id", $this->vehiculoclase_id, PDO::PARAM_INT);
            $sentencia->bindParam(":vehiculomarca_id", $this->vehiculomarca_id, PDO::PARAM_INT);
            $sentencia->bindParam(":vehiculomodelo_id", $this->vehiculomodelo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":llave_obs", $this->llave_obs, PDO::PARAM_STR);
            $sentencia->bindParam(":llave_numcasillero", $this->llave_numcasillero, PDO::PARAM_STR);
            $sentencia->bindParam(":llave_tipollave", $this->llave_tipollave, PDO::PARAM_STR);
            $sentencia->bindParam(":creditotipo_id", $this->creditotipo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":estadollave_tipo", $this->estadollave_tipo, PDO::PARAM_STR);
            $sentencia->bindParam(":llave_vehsermot_chasis", $this->nromotcha, PDO::PARAM_STR);
            $sentencia->bindParam(":llave_vehpla", $this->vehpla, PDO::PARAM_STR);


            $result = $sentencia->execute();
            $llave_id = $this->dblink->lastInsertId();

            $this->dblink->commit();
            $data['estado'] = $result;
            $data['llave_id'] = $llave_id;
            return $data;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }




    function modificar()
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_llave
            SET
             tb_llave_usureg = :llave_usureg,
             tb_cliente_id = :cliente_id,
             tb_vehiculoclase_id = :vehiculoclase_id,
             tb_vehiculomarca_id = :vehiculomarca_id,
             tb_vehiculomodelo_id = :vehiculomodelo_id,
             tb_llave_obs = :llave_obs,
             tb_llave_numcasillero = :llave_numcasillero,
             tb_llave_tipollave = :llave_tipollave,
             tb_creditotipo_id = :creditotipo_id,
             tb_credito_id = :credito_id
            WHERE
            tb_llave_id = :llave_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':llave_usureg', $this->llave_usureg, PDO::PARAM_STR);
            $sentencia->bindParam(':cliente_id', $this->cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':vehiculoclase_id', $this->vehiculoclase_id, PDO::PARAM_STR);
            $sentencia->bindParam(':vehiculomarca_id', $this->vehiculomarca_id, PDO::PARAM_STR);
            $sentencia->bindParam(':vehiculomodelo_id', $this->vehiculomodelo_id, PDO::PARAM_STR);
            $sentencia->bindParam(':llave_obs', $this->llave_obs, PDO::PARAM_STR);
            $sentencia->bindParam(':llave_numcasillero', $this->llave_numcasillero, PDO::PARAM_INT);
            $sentencia->bindParam(':llave_tipollave', $this->llave_tipollave, PDO::PARAM_STR);
            $sentencia->bindParam(':creditotipo_id', $this->creditotipo_id, PDO::PARAM_STR);
            $sentencia->bindParam(':credito_id', $this->credito_id, PDO::PARAM_STR);
            $sentencia->bindParam(':llave_id', $this->llave_id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();

            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado; //si es correcto retorna 1
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }


    function modificarPropietario()
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_llave
            SET tb_cliente_id = :paramcambia,
                tb_credito_id = :credito
            WHERE tb_llave_id = :llaveid;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':paramcambia', $this->cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(':credito', $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(':llaveid', $this->llave_id, PDO::PARAM_INT);
            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
                // Puedes agregar algún código adicional aquí si es necesario
            }

            return $resultado; // Si es correcto, retorna 1
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }


    function eliminar($llave_id)
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_llave SET tb_llave_xac=0 WHERE tb_llave_id =:llave_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":llave_id", $llave_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }


    function mostrarUno($llave_id, $creditotipo_id)
    {
        try {
            if (intval($creditotipo_id) > 1) {
                $nombre_tabla = 'tb_creditogarveh';
                if (intval($creditotipo_id) == 2)
                    $nombre_tabla = 'tb_creditoasiveh';

                $sql = "SELECT  llav.tb_llave_id as id,
                CONCAT(u.tb_usuario_nom, ' ', u.tb_usuario_ape) as Usuario_registra,
                c.tb_cliente_nom as Nombre_Cliente,
                c.tb_cliente_id as clienteid,
                llav.tb_credito_id as creditoid,
                llav.tb_vehiculoclase_id as clase_vehiculo,
                llav.tb_vehiculomarca_id as marca_vehiculo,
                llav.tb_vehiculomodelo_id as modelo_vehiculo,
                llav.tb_llave_vehpla as placa,
                llav.tb_llave_obs as observaciones,
                llav.tb_llave_numcasillero as numero_casillero,
                llav.tb_llave_tipollave AS tipo_llave,
                cgv.tb_credito_vehsercha AS numero_serie,
                cgv.tb_credito_vehsermot AS numero_motor,
                cgv.tb_credito_vehsercha AS numero_chasis
                FROM tb_llave llav
                LEFT JOIN tb_usuario u ON llav.tb_llave_usureg = u.tb_usuario_id
                LEFT JOIN tb_cliente c ON c.tb_cliente_id = llav.tb_cliente_id
                INNER JOIN $nombre_tabla cgv ON llav.tb_credito_id = cgv.tb_credito_id
                WHERE 
                llav.tb_llave_id = :llave_id";
            } else {
                $sql = "SELECT  llav.tb_llave_id as id,
                c.tb_cliente_nom as Nombre_Cliente,
                c.tb_cliente_empruc as ruc_empresa,
                c.tb_cliente_emprs as nombre_empresa,
                c.tb_cliente_nom as cliente_empresa_nombre,
                c.tb_cliente_doc as dni_cliente_empresa,
                c.tb_cliente_dir as direccion,
                c.tb_cliente_numpar as numparcliente,
                estadollav.tb_estadollave_obs as obs,
                c.tb_cliente_id as clienteid,
                c.tb_cliente_doc as dni,
                vc.tb_vehiculoclase_nom as clase_vehiculo,
                vm.tb_vehiculomarca_nom as marca_vehiculo,
                gar.tb_garantia_vehpla as placa,
                gar.tb_garantia_vehsercha numero_serie,
                gar.tb_garantia_vehsermot numero_motor
                FROM tb_estadollave estadollav
                INNER JOIN tb_llave llav ON estadollav.tb_llave_id = llav.tb_llave_id
                 INNER JOIN tb_creditomenor cgv ON llav.tb_credito_id = cgv.tb_credito_id
                 INNER JOIN tb_garantia gar ON gar.tb_credito_id = cgv.tb_credito_id
                 LEFT JOIN tb_cliente c ON c.tb_cliente_id = llav.tb_cliente_id
                 LEFT JOIN tb_vehiculoclase vc ON vc.tb_vehiculoclase_id = llav.tb_vehiculoclase_id
                 LEFT JOIN tb_vehiculomarca vm ON vm.tb_vehiculomarca_id = llav.tb_vehiculomarca_id
                 WHERE 
                 llav.tb_llave_id = :llave_id AND (gar.tb_garantiatipo_id = 3 and gar.tb_articulo_id=75)";
            }


            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":llave_id", $llave_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Estados de llave registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function llenarDatosCreditos($creditoid, $creditotipo_id)
    {

        try {
            if (intval($creditotipo_id) > 1) {
                $nombre_tabla = 'tb_creditogarveh';
                if (intval($creditotipo_id) == 2)
                    $nombre_tabla = 'tb_creditoasiveh';
                $sql = "SELECT  
                cgv.tb_credito_id as creditoid,
                cgv.tb_vehiculoclase_id as clase_vehiculo,
                cgv.tb_vehiculomarca_id as marca_vehiculo,
                cgv.tb_vehiculomodelo_id as modelo_vehiculo,
                cgv.tb_credito_vehsermot as vehiculo_motor,
                cgv.tb_credito_vehsercha as vehiculo_chasis,
                cgv.tb_credito_vehpla as vehiculo_placa


                from $nombre_tabla cgv
                WHERE 
                cgv.tb_credito_id = :credito_id";
            } else {
                $sql = "SELECT  
                    gar.tb_credito_id as creditoid,
                    gar.tb_vehiculomarca_id as marca_vehiculo,
                    gar.tb_vehiculoclase_id as clase_vehiculo,
                    gar.tb_vehiculomodelo_id as modelo_vehiculo,
                    gar.tb_garantia_vehcol as color,
                    gar.tb_garantia_vehsermot  as vehiculo_motor,
                    gar.tb_garantia_vehsercha as vehiculo_chasis,
                    gar.tb_garantia_vehpla as vehiculo_placa   
                FROM tb_garantia gar
                    WHERE gar.tb_credito_id = :credito_id AND gar.tb_articulo_id = 75";
            }


            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":credito_id", $creditoid, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Estados de llave registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }


    function listar_llave()
    {
        try {
            $sql = "SELECT  
            llav.tb_llave_id as id,
            c.tb_cliente_nom as Nombre_Cliente,
            c.tb_cliente_emprs,
            llav.tb_creditotipo_id,
            c.tb_cliente_empruc,
            llav.tb_credito_id as creditoid,
            CONCAT(vm.tb_vehiculomarca_nom, ' / ', vmod.tb_vehiculomodelo_nom) as marca_modelo,
            llav.tb_llave_vehpla as placa,
            llav.tb_llave_obs as observaciones,
            llav.tb_llave_numcasillero as numero_casillero,
            CASE
                WHEN llav.tb_estadollave_tipo = 1 THEN 'Almacenada' 
                WHEN llav.tb_estadollave_tipo= 2 THEN 'En manos de asesor' 
                WHEN llav.tb_estadollave_tipo = 3 THEN 'Pendiente de entrega' 
                WHEN llav.tb_estadollave_tipo = 4 THEN 'Entregado/Liquidado' 
            END as estado,
            CASE
                WHEN llav.tb_llave_tipollave = 1 THEN 'Copia'
                WHEN llav.tb_llave_tipollave = 2 THEN 'Original'
            END AS tipo_llave,
            llav.tb_credito_id,
            CASE
            WHEN llav.tb_creditotipo_id = '1' THEN 'CREDITO MENOR' 
            WHEN llav.tb_creditotipo_id = '2' THEN 'CREDITO ASCVEH' 
            WHEN llav.tb_creditotipo_id = '3' THEN 'CREDITO GARVEH' 
            END as tipo_credito
        FROM 
            tb_llave llav
            INNER JOIN tb_cliente c ON c.tb_cliente_id = llav.tb_cliente_id
            INNER JOIN tb_vehiculomarca vm ON vm.tb_vehiculomarca_id = llav.tb_vehiculomarca_id
            INNER JOIN tb_vehiculomodelo vmod ON vmod.tb_vehiculomodelo_id = llav.tb_vehiculomodelo_id
        WHERE 
            llav.tb_llave_xac = 1 AND llav.tb_credito_id>0
        GROUP BY 
            llav.tb_llave_id;
        
        ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["cantidad_resultados"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay estados de llave registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function ObtenerNunLlavesCasillero($codigo)
    {
        try {
            $sql = "SELECT tb_config_valor FROM tb_config WHERE tb_config_id=:codigo";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":codigo", $codigo, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $configuracion = $sentencia->fetch(PDO::FETCH_ASSOC);
                $numLlaves = $configuracion['tb_config_valor'];

                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $numLlaves;
                $sentencia->closeCursor(); // para liberar memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay configuración registrada para el casillero";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }


    function llenarCombo($cliente_id)
    {
        try {
            $sql = "SELECT 3 as tipocredito,
            tb_credito_id AS credito_id,
            0 as tb_garantia_id,
            tb_credito_vehpla AS placa,
            tb_credito_vehsermot AS numero_motor,
            tb_credito_vehsercha AS numero_chasis
            FROM tb_creditogarveh 
            WHERE tb_cliente_id = :cliente_id
            AND (tb_credito_est = 3 OR tb_credito_est = 4 OR tb_credito_est = 9 OR tb_credito_est = 10 OR tb_credito_est = 7 OR tb_credito_est = 2) 
            AND tb_credito_xac = 1
            -- GROUP BY tb_credito_vehsermot

            UNION

            SELECT 1 as tipocredito, cm.tb_credito_id as credito_id,
            g.tb_garantia_id,
            g.tb_garantia_vehpla AS placa,
            g.tb_garantia_vehsermot AS numero_motor,
            g.tb_garantia_vehsercha AS numero_chasis
            FROM tb_garantia g
            INNER JOIN tb_creditomenor cm ON cm.tb_credito_id = g.tb_credito_id
            WHERE cm.tb_cliente_id = :cliente_id

            AND (cm.tb_credito_est = 3 OR cm.tb_credito_est = 4 OR cm.tb_credito_est = 9 OR cm.tb_credito_est = 10 OR cm.tb_credito_est = 7) 
            
            AND cm.tb_credito_xac = 1
                AND (tb_garantiatipo_id = 3 and tb_articulo_id=75)
            GROUP BY g.tb_credito_id

            UNION 

            SELECT  2 as tipocredito,tb_credito_id as creditoid,
            0 as tb_garantia_id,
            tb_credito_vehpla AS placa,
            tb_credito_vehsermot AS numero_motor,
            tb_credito_vehsercha AS numero_chasis
            FROM tb_creditoasiveh asv
            WHERE tb_cliente_id =:cliente_id 
            AND (tb_credito_est = 3 OR tb_credito_est = 4 OR tb_credito_est = 9 OR tb_credito_est = 10)
            AND tb_credito_xac = 1
            GROUP BY tb_credito_vehsermot;
            
            ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["cantidad_resultados"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se puede listar combo";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // En el método consultarLlaveExistente() de la clase Llave

    public function consultarLlaveExistente($cliente_id, $nromotcha, $llave_tipollave, $llave_id)
    {
        // Construir la consulta SQL
        if (intval($llave_id) > 0) {
            $sql = "SELECT * FROM tb_llave WHERE tb_cliente_id = :cliente_id AND tb_llave_tipollave = :llave_tipollave AND tb_llave_id != :llave_id AND tb_llave_xac = 1 AND tb_llave_numcasillero != 0";
        } else {
            $sql = "SELECT * FROM tb_llave WHERE tb_cliente_id = :cliente_id AND tb_llave_tipollave = :llave_tipollave AND tb_llave_xac = 1";
        }

        if (!empty($nromotcha)) {
            $sql .= " AND tb_llave_vehsermot_chasis = :llave_vehsermot_chasis";
        }

        // Preparar la consulta
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(':cliente_id', $cliente_id);
        $sentencia->bindParam(':llave_tipollave', $llave_tipollave);
        if (!empty($nromotcha)) {
            $sentencia->bindParam(':llave_vehsermot_chasis', $nromotcha);
        }
        if (intval($llave_id) > 0) {
            $sentencia->bindParam(':llave_id', $llave_id);
        }

        // Ejecutar la consulta
        if ($sentencia->execute()) {
            // Devolver los resultados
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["cantidad_resultados"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para liberar memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontraron registros";
                $retorno["data"] = "";
            }
        } else {
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "Error al ejecutar la consulta";
            $retorno["data"] = "";
        }

        return $retorno;
    }



    function modificarestado($llave_id, $tipollave_id)
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_llave SET tb_estadollave_tipo =:tipollave WHERE tb_llave_id =:llave_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tipollave", $tipollave_id, PDO::PARAM_INT);
            $sentencia->bindParam(":llave_id", $llave_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }



    function modificar_campo($llave_id, $llave_columna, $llave_valor, $param_tip)
    {
        $this->dblink->beginTransaction();
        try {
            if (!empty($llave_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_llave SET " . $llave_columna . " =:llave_valor WHERE tb_llave_id =:llave_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":llave_id", $llave_id, PDO::PARAM_INT);



                if ($param_tip == 'INT')
                    $sentencia->bindParam(":llave_valor", $llave_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":llave_valor", $llave_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el ingreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
