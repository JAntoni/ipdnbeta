<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'llave/Llave.class.php');
    require_once(VISTA_URL . 'gestioncochera/GestionCochera.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../llave/Llave.class.php');
    require_once('../gestioncochera/GestionCochera.class.php');
    require_once('../funciones/fechas.php');
}
$oLlave = new Llave();
$oGestioncochera = new Gestioncochera();
?>
<table id="tbl_llave" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">CREDITO ID</th>
            <th id="tabla_cabecera_fila">TIPO DE CREDITO</th>
            <th id="tabla_cabecera_fila">MARCA/MODELO</th>
            <th id="tabla_cabecera_fila">PLACA DE VEHICULO</th>
            <th id="tabla_cabecera_fila">COCHERA</th>
            <th id="tabla_cabecera_fila">OBSERVACIONES</th>
            <th id="tabla_cabecera_fila">NUMERO DE CASILLERO</th>
            <th id="tabla_cabecera_fila">TIPO DE LLAVE</th>
            <th id="tabla_cabecera_fila">ESTADO</th>

            <th id="tabla_cabecera_fila" width="6%" class="no-sort">ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //PRIMER NIVEL
        $result = $oLlave->listar_llave();
        if ($result['estado'] == 1) {
            foreach ($result['data'] as $key => $value) :
                $cocheradesc = 'VEHÍCULO SIN CUSTODIA';
                $result2 = $oGestioncochera->mostrarPorPlaca($value['placa'],1);
                if ($result2['estado'] == 1) {
                    $cocheradesc = $result2['data']['tb_cochera_nom'];
                }
                $result2 = null;

                $nombre_cliente = $value['Nombre_Cliente'];
                if ($value['tb_cliente_emprs'] != '')
                    $nombre_cliente = $value['tb_cliente_emprs'] . ' | ' . $value['Nombre_Cliente'];
        ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $value['id']; ?></td>
                    <td id="tabla_fila"><?php echo $nombre_cliente; ?></td>
                    <td id="tabla_fila"><?php echo $value['creditoid']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tipo_credito']; ?></td>
                    <td id="tabla_fila"><?php echo $value['marca_modelo']; ?></td>
                    <td id="tabla_fila"><?php echo $value['placa']; ?></td>
                    <td id="tabla_fila"><?php echo $cocheradesc; ?></td>
                    <td id="tabla_fila"><?php echo $value['observaciones']; ?></td>
                    <td id="tabla_fila"><?php echo $value['numero_casillero']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tipo_llave']; ?></td>
                    <td id="tabla_fila"><?php echo $value['estado']; ?></td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-warning btn-xs" title="Editar" onclick="seleccionarModal(<?php echo  $value['id'] . ' , ' . $value['tb_credito_id']; ?>)"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-xs" title="Eliminar" onclick="llave_form(<?php echo "'E' ,  " .  $value['id'] . ' , ' . $value['tb_credito_id']; ?>)"><i class="fa fa-trash"></i></a>
                        <a class="btn btn-github btn-xs" title="Historial" onclick="llave_historial_form(<?php echo $value['id']; ?>)"><i class="fa fa-history"></i></a>
                    </td>
                </tr>
        <?php
            endforeach;
        }
        $result = NULL;
        ?>
    </tbody>
</table>