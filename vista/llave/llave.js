var datatable_global;

function llave_form(usuario_act, llave_id,tb_credito_id){ 
  
  $.ajax({
		type: "POST",
		url: VISTA_URL+"llave/llave_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      llave_id: llave_id,
      credito_id:tb_credito_id // nombre:valor
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      //console.log(data);
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_llave_form').html(data);
      	$('#modal_registro_llave').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
        form_desabilitar_elementos('form_llave'); //funcion encontrada en public/js/generales.js
        modal_width_auto('modal_registro_llave', 40);
      	modal_hidden_bs_modal('modal_registro_llave', 'limpiar'); //funcion encontrada en public/js/generales.js
        
        
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'llave';
      	var div = 'div_modal_llave_form';
      	permiso_solicitud(usuario_act, llave_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			//console.log(data.responseText);
		}
	});
}


function llave_formPropietario(usuario_act, llave_id,tb_credito_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"llave/llave_formpropietario.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      llave_id: llave_id,
      credito_id:tb_credito_id,
      /* accion2: "propi" */
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_llave_propietario').html(data);
      	$('#modal_registro_nuevopropietario').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_llave'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_nuevopropietario', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'llave';
      	var div = 'div_modal_llave_propietario';
      	permiso_solicitud(usuario_act, llave_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			//console.log(data.responseText);
		}
	});
}


function llave_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"llave/llave_tabla.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function() {
      $('#llave_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_llave_tabla').html(data);
      $('#llave_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#llave_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_llave').DataTable({
    "pageLength": 50,
    "responsive": true,
    "bScrollCollapse": true,
    "bPaginate": false,
    "bJQueryUI": true,
    "scrollX": true,
    scrollY: 700,
    //"scrollY": "80vh",
    //dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
    dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
    buttons: [
      { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
      { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
      { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
      { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
      { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
    ],
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsquedad",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [],
    columnDefs: [
      { targets: "no-sort", orderable: false}
    ]
    /*drawCallback: function () {
      var sum_desembolsado = $('#tbl_infocorp').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
      var sum_capital_rest_sol = $('#tbl_infocorp').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles

      $('#total').html(sum);
    }*/
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}
$(document).ready(function() {
  estilos_datatable();

  console.log('CAMBIOS 20-02-2024')
});



//redireccionar modal
function seleccionarModal(id,tb_credito_id){
  Swal.fire({
    title: 'SELECCIONE QUE PROCESO QUIERE REALIZAR',
    icon: 'info',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: "MODIFICAR DATOS",
    showDenyButton: true,
    denyButtonText: "CAMBIAR DE PROPIETARIO",
    confirmButtonColor: '#3b5998',
    denyButtonColor: '#21ba45',
}).then((result) => {

    if (result.isConfirmed) {
       
       //MODAL PARA MODIFICAR DATOS
       llave_form('M',id,tb_credito_id);
      

    } else if (result.isDenied) {
        //MODAL DE CAMBIO DE PROPIETARIO
        llave_formPropietario('M',id,tb_credito_id);// depende de la encesidad del form

    }
});
}

function llave_historial_form(llave_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "llave/llave_historial_form.php",
      async: true,
      dataType: "html",
      data: ({
        llave_id: llave_id
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (html) {
          $('#div_llave_historial_form').html(html);//se carga el html en la vista del php
          $('#div_modal_llave_historial_form').modal('show'); //es para el archivo de destino(URL)
          $('#modal_mensaje').modal('hide');

          //funcion js para limbiar el modal al cerrarlo
          modal_hidden_bs_modal('div_modal_llave_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
          //funcion js para agregar un largo automatico al modal, al abrirlo
          modal_height_auto('div_modal_llave_historial_form'); //funcion encontrada en public/js/generales.js

      },
      complete: function (html) {
      }
  });
}



