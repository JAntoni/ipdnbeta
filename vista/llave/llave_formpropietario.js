$(document).ready(function () {
  $("#cmb_credito").change(function (event) {
    var creditoid = $("#cmb_credito").val();

    if (parseInt(creditoid) == 0) {
      $("#hdd_clasevehiculo").val(0);
      $("#hdd_marcavehiculo").val(0);
      $("#hdd_modelovehiculo").val(0);
    } else confDatosVeh(creditoid);
  });
  //autocomplete

  $("#txt_cliente_nuevo")
    .autocomplete({
      minLength: 1,
      source: function (request, response) {
        $.getJSON(
          VISTA_URL + "cliente/cliente_autocomplete.php",
          { term: request.term }, //1 solo clientes de la empresa
          response
        );
      },
      select: function (event, ui) {
        credito_cliente_select(ui.item.cliente_id, $("#hdd_credito_id").val());
        $("#txt_cliente_nuevo").val(ui.item.cliente_nom);
        $("#hdd_cliente_nuevo").val(ui.item.cliente_id);

        event.preventDefault();
      },
    })
    .keyup(function () {
      console.log(VISTA_URL + "cliente/cliente_autocomplete.php");
      if ($("#txt_cliente_nuevo").val() === "") {
        $("#hdd_cliente_nuevo").val("");
      }
    });

  $("#form_llave_nuevopropietario").validate({
    submitHandler: function () {
      var elementos_disabled = $("#form_llave_nuevopropietario")
        .find("input:disabled")
        .removeAttr("disabled");
      var form_serializado = $("#form_llave_nuevopropietario").serialize();
      elementos_disabled.attr("disabled", "disabled");
      $.ajax({
        type: "POST",
        url: VISTA_URL + "llave/llave_controller.php",
        async: true,
        dataType: "json",
        data: form_serializado,
        beforeSend: function () {
          $("#llave_mensaje").show(400);
          $("#btn_guardar_llave").prop("disabled", true);
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {
            $("#llave_mensaje")
              .removeClass("callout-info")
              .addClass("callout-success");
            $("#llave_mensaje").html(data.mensaje);

            setTimeout(function () {
              llave_tabla();
              $("#modal_registro_nuevopropietario").modal("hide");
            }, 1000);
          } else {
            $("#llave_mensaje")
              .removeClass("callout-info")
              .addClass("callout-warning");
            $("#llave_mensaje").html("Alerta: " + data.mensaje);
            $("#btn_guardar_llave").prop("disabled", false);
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function (data) {
          $("#llave_mensaje")
            .removeClass("callout-info")
            .addClass("callout-danger");
          $("#llave_mensaje").html("ALERTA DE ERROR: " + data.responseText);
        },
      });
    },
    rules: {
      txt_cliente_nuevo: {
        required: true,
        minlength: 1,
      },
      cmb_credito: {
        required: true,
        min: 1,
      },
    },
    messages: {
      txt_cliente_nuevo: {
        required: "Ingrese el nuevo propietario",
        minlength: "",
      },
      cmb_credito: {
        required: "Ingrese un crédito",
        min: "Ingresa un credito ",
      },
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      // Add the `help-block` class to the error element
      error.addClass("help-block");
      if (element.prop("type") === "radio") {
        error.insertAfter($("#radio_persjur"));
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });

  var action = $("#action").val();

  /*Funcion Para seleccionar credito de cliente*/

  function credito_cliente_select(cliente_id, creditoid) {
    console.log(cliente_id, creditoid);
    $.ajax({
      type: "POST",
      url: VISTA_URL + "llave/llave_credito_select.php",
      async: true,
      dataType: "html",
      data: {
        cliente_id: cliente_id, //estos datos se envian con el metodo post
        creditoid: creditoid,
      },
      beforeSend: function () {
        $("#cmb_llave").html("<option>Cargando...</option>");
      },
      success: function (data) {
        $("#cmb_credito").html(data);
        $("#cmb_credito").change();
      },
      complete: function (data) {
        //console.log(data);
      },
      error: function (data) {
        alert(data.responseText);
      },
    });
  }
});

function confDatosVeh(creditoid) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "llave/llave_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "llenardatos",
      creditoid: creditoid,
    },
    beforeSend: function () {},
    success: function (data) {
      console.log(data);
      $("#hdd_clasevehiculo").val(parseInt(data.claseid));
      $("#hdd_marcavehiculo").val(parseInt(data.marcaid));
      $("#hdd_modelovehiculo").val(parseInt(data.modeloid));
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alert(data.responseText);
    },
  });
}
