<?php
require_once('Llave.class.php');
$oLlave = new Llave();

$numcasillero = (empty($numcasillero))? 0 : $numcasillero; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';
$codigo =12;

//buscar casilleros ocupados
$casilleros_ocupados=array();
$casOcupado=$oLlave->listar_llave();
if($casOcupado['estado'] == 1){
	foreach($casOcupado['data'] as $key => $casillero_ocupado){
		$casilleros_ocupados[]= intval($casillero_ocupado['numero_casillero']);
	}
}

$option = var_export($casilleros_ocupados);

//PRIMER NIVEL
$result = $oLlave->ObtenerNunLlavesCasillero($codigo);
if($result['estado'] == 1)
{
    for($i = 1; $i <= intval($result['data']); $i++) {
        $selected = '';
        $disabled = '';

        if(!in_array($i, $casilleros_ocupados)) {
            $option .= '<option value="'.$i.'">'.$i.'</option>';
        } else {
            if($i == $numcasillero) {
				$option .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                $selected = 'selected';
				$disabled = 'disabled';
            }
        }
    }
}

$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>