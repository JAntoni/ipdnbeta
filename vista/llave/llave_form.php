<?php
require_once('../../core/usuario_sesion.php');
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../llave/Llave.class.php');
$ollave = new Llave();
require_once('../funciones/funciones.php');
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();


$direc = 'llave';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$llave_id = $_POST['llave_id'];
$credito_id = $_POST['credito_id'];


$titulo = '';
if ($usuario_action == 'L')
  $titulo = 'Registro de lave Registrado';
elseif ($usuario_action == 'I')
  $titulo = 'Registrar  llave';
elseif ($usuario_action == 'M')
  $titulo = 'Editar registro de llave';
elseif ($usuario_action == 'E')
  $titulo = 'Eliminar  llave';
else
  $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en llave
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id'];
    $modulo = 'llave';
    $modulo_id = $llave_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $result = NULL;
      echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
      exit();
    }
    $result = NULL;
  }


  //si la accion es modificar, mostramos los datos del llave por su ID
  if (intval($llave_id) > 0) {
    $result = $ollave->mostrarUno($llave_id, $credito_id);
    if ($result['estado'] != 1) {
      $mensaje =  'No se ha encontrado ningún registro de llave.';
      $bandera = 4;
    } else {
      //se reciben los datos de la consulta y se asignan a  variables y se usa en el formulario
      $cliente = $result['data']['Nombre_Cliente'];
      $clienteid = $result['data']['clienteid'];
      $creditoid = $result['data']['creditoid'];
      $clasevehiculo = $result['data']['clase_vehiculo'];
      $marcavehiculo = $result['data']['marca_vehiculo'];
      $modelovehiculo = $result['data']['tb_vehicuilomodelo'];
      $estadollave = $result['data']['tipo_estado'];
      $observaciones = $result['data']['observaciones'];
      $numcasillero = $result['data']['numero_casillero'];
      $tipollave = $result['data']['tipo_llave'];
      $credito_vehsermot = $result['data']['numero_motor'];
      $credito_vehsercha = $result['data']['numero_chasis'];
      $vehpla = $result['data']['placa'];
      

    }
    $result = NULL;
  }
} else {
  $mensaje =  $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}

?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_llave" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title"><?php echo $titulo; ?></h4>
        </div>
        <form id="form_llave" name="form_llave" method="post">

          <input type="hidden" name="action" id="action" value="<?php echo $action; ?>">
          <input type="hidden" name="hdd_llave_id" id="hdd_llave_id" value="<?php echo $llave_id; ?>">
          <input type="hidden" name="hdd_llave_tipo_actual" id="hdd_llave_tipo_actual" value="<?php echo $tipollave; ?>">
          <input type="hidden" name="hdd_llave_motor" id="hdd_llave_motor" value="<?php echo $credito_vehsermot ; ?>">
          <input type="hidden" name="hdd_llave_chasis" id="hdd_llave_chasis" value="<?php echo $credito_vehsercha ; ?>">
          <input type="hidden" name="hdd_llave_placa" id="hdd_llave_placa" value="<?php echo $vehpla ; ?>">
          <input type="hidden" name="hdd_llave_tipocredito" id="hdd_llave_tipocredito" value="">



          <div class="modal-body">
            <!-- usuario que registra llave -->
            <div class="form-group">
              <label for="txt_llave_usuentrega">Usuario que Registra llave: </label>
              <input type="text" name="txt_llave_usuregistra" id="txt_llave_usuregistra" class="form-control input-sm" value="<?php echo $_SESSION['usuario_nombre'] . " " . $_SESSION['usuario_ape'];  ?>" disabled>
            </div>

            <!-- Cliente -->
            <div class="form-group">
              <label for="txt_llave_clienteid" class="control-label">Cliente:</label>
              <input type="text" name="txt_llave_clienteid" id="txt_llave_clienteid" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48" value="<?php echo $cliente; ?>">
              <input type="hidden" name="hdd_llave_clienteid" id="hdd_llave_clienteid" value="<?php echo $clienteid ?>">
            </div>

            <!-- Información de crédito -->
            <div class="form-group">
            <input type="hidden" name="hdd_llave_creditoid" id="hdd_llave_creditoid" value="<?php echo $creditoid ?>">

              <label>Información de crédito :</label>
              <select id="cmb_credito" name="cmb_credito" class="form-control">
              </select>
            </div>
            <!-- clase,marca,modelo -->
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <input type="hidden" name="hdd_clasevehiculo" id="hdd_clasevehiculo">
                  <label for="cmb_clasevehiculo" class="control-label">Carrocería de Vehiculo:</label>
                  <select id="cmb_clasevehiculo" name="cmb_clasevehiculo" class="form-control">
                    <?php require_once('../vehiculoclase/vehiculoclase_select.php') ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="cmb_marcavehiculo" class="control-label">Marca de Vehículo:</label>
                  <select id="cmb_marcavehiculo" name="cmb_marcavehiculo" class="form-control">
                    <?php require_once('../vehiculomarca/vehiculomarca_select.php') ?>
                  </select>
                </div>


                <div class="col-md-4">
                  <label for="cmb_modelovehiculo" class="control-label">Modelo de Vehículo:</label>
                  <select id="cmb_modelovehiculo" name="cmb_modelovehiculo" class="form-control">
                    <?php require_once('../vehiculomodelo/vehiculomodelo_select.php') ?>
                  </select>
                </div>

                
              </div>
            </div>


            <!-- Observaciones -->
            <div class="form-group">
              <label for="txt_comentario">Observaciones:</label>
              <textarea class="form-control mayus" name="txt_comentario" id="txt_comentario"><?php echo $observaciones ?></textarea>
            </div>


            <!-- Numero de casillero (Combo)-->
            <div class="form-group">
              <label>Númerode casillero</label>
              <select id="cmb_numcasillero" name="cmb_numcasillero" class="form-control">
                <?php require_once('../llave/llave_select.php') ?>
              </select>
            </div>

            <!-- tipo de llave -->
            <div class="form-group" id="radio_grupo">
              <label for="txt_llave_tip" class="control-label">Tipo de Llave:</label>
              <br>
              <label id="tipo_llave" class="radio-inline" style="padding-left: 0px;">
                <input type="radio" name="txt_llave_tip" id="txt_llave_tip" class="flat-green" value="1" <?php if ($tipollave == 1) echo 'checked'; ?>> Copia
              </label>
              <label id="tipo_llave" class="radio-inline">
                <input type="radio" name="txt_llave_tip" id="txt_llave_tip" class="flat-green" value="2" <?php if ($tipollave == 2) echo 'checked'; ?>> Original
              </label>
            </div>
            

            <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
            <?php if ($action == 'eliminar') : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta llave?</h4>
              </div>
            <?php endif; ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="llave_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_llave">Guardar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_llave">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_llave">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/llave/llave_form.js?ver=11'; ?>"></script>