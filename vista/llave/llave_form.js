$(document).ready(function () {
  $("#cmb_credito").change(function (event) {
    $("#hdd_llave_tipocredito").val(parseInt($('#cmb_credito>option:selected').attr("data-tipocredito")));
    $("#hdd_llave_garantiaid").val(parseInt($('#cmb_credito>option:selected').attr("data-tb_garantia_id")));
    var llave_id = $("#hdd_llave_id").val();
    var creditotipo_id =$("#hdd_llave_tipocredito").val();
    var creditoid =$("#cmb_credito").val();

    
    
    

    if (parseInt(creditoid) == 0) {
      $("#cmb_clasevehiculo").val(0);
      $("#cmb_marcavehiculo").val(0);
      $("#cmb_modelovehiculo").val(0);
      $("#hdd_llave_motor").val('');
      $("#hdd_llave_chasis").val('');

    } else confDatosVeh(creditoid, creditotipo_id);
  });

  $("#cmb_numcasillero").selectpicker({
    liveSearch: true,
    maxOptions: 1,
    language: "ES",
  });
  //autocomplete

  $("#txt_llave_clienteid")
    .autocomplete({
      minLength: 1,
      source: function (request, response) {
        $.getJSON(
          VISTA_URL + "cliente/cliente_autocomplete.php",
          { term: request.term }, //1 solo clientes de la empresa
          response
        );
      },
      select: function (event, ui) {
        $("#txt_llave_clienteid").val(ui.item.cliente_nom);
        $("#hdd_llave_clienteid").val(ui.item.cliente_id);
        credito_cliente_select(ui.item.cliente_id,0);// cuando el cliente esta insertando, no hay datos preseleccionados por eso el valor es 0, en las otras acciones si hay valores preseleccionados por eso el valor es diferente de 0
        event.preventDefault();
      },
    })
    .keyup(function () {
      console.log(VISTA_URL + "cliente/cliente_autocomplete.php");
      if ($("#txt_llave_clienteid").val() === "") {
        $("#hdd_llave_clienteid").val("");
        $("#cmb_credito").html("");
        $("#cmb_clasevehiculo").val(0);
        $("#cmb_marcavehiculo").val(0);
        $("#cmb_modelovehiculo").val(0);
      }
    });

  $("#cmb_tipocredito option:eq(5)").hide();

  $('input[type="radio"].flat-green').iCheck({
    checkboxClass: "icheckbox_flat-green",
    radioClass: "iradio_flat-green",
  });

  /*$("#radio_persnat, #radio_persjur, ins").click(function () {
    $("#txt_llave_tip-error").css("display", "none");
  });*/

  /*$("form[name='form_llave']").submit(function(e) {
    e.preventDefault();
    console.log($("#form_llave").serialize());
  });*/

  $("#form_llave").validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "llave/llave_controller.php",
        async: true,
        dataType: "json",
        data: serializar_form_llave(),
        beforeSend: function () {
          $("#llave_mensaje").show(400);
          $("#btn_guardar_llave").prop("disabled", true);
        },
        success: function (data) {
          if (parseInt(data.estado) > 0) {
            $("#llave_mensaje")
              .removeClass("callout-info")
              .addClass("callout-success");
            $("#llave_mensaje").html(data.mensaje);

            setTimeout(function () {
              llave_tabla();
              $("#modal_registro_llave").modal("hide");
            }, 1000);
          } else {
            $("#llave_mensaje")
              .removeClass("callout-info")
              .addClass("callout-warning");
            $("#llave_mensaje").html("Alerta: " + data.mensaje);
            $("#btn_guardar_llave").prop("disabled", false);
          }
        },
        complete: function (data) {
          //console.log(data);
        },
        error: function (data) {
          $("#llave_mensaje")
            .removeClass("callout-info")
            .addClass("callout-danger");
          $("#llave_mensaje").html("ALERTA DE ERROR: " + data.responseText);
        },
      });
    },
    rules: {

      txt_llave_tip:{
        required: true,
        minlength: 1
      },

      txt_llave_clienteid: {
        required: true,
      },
      cmb_credito: {
        min:1
      },
    },
    messages: {
      txt_llave_clienteid: {
        required: "Seleccione un Cliente",
      },
      cmb_credito: {
        required: "Ingrese un crédito valido",
      },
      txt_llave_ema: {
        required: "Ingrese el Documento del llave",
        digits: "Solo números por favor",
        minlength: "El Documento como mínimo debe tener 8 números",
        maxlength: "El Documento como máximo debe tener 11 números",
      },
      txt_llave_tip: {
        required: "Este campo es requerido",
				minlength: "Seleccione un tipo de llave"
      },

      txt_llave_ema: {
        email: "Ingrese un Email válido",
      },
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
      // Add the `help-block` class to the error element
      error.addClass("help-block");
      if (element.prop("type") === "radio") {
        error.insertAfter($("#radio_persjur"));
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-error")
        .removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".col-sm-5")
        .addClass("has-success")
        .removeClass("has-error");
    },
  });

  function serializar_form_llave() {
    var extraclase = `&clasevehiculotxt=`+ $('#cmb_clasevehiculo option:selected').text();
    var extramarca = `&marcavehiculotxt=`+ $('#cmb_marcavehiculo option:selected').text();
    var extramodelo = `&modelovehiculotxt=`+ $('#cmb_modelovehiculo option:selected').text();
  
      //var elementos_disabled = $('#form_estadollave').find('input:disabled, select:disabled, textarea:disabled').removeAttr('disabled');
      var form_serializado = $('#form_llave').serialize() + extraclase + extramarca + extramodelo;
      
      //elementos_disabled.attr('disabled', 'disabled');
      return form_serializado;
  }

  /*Funcion Para seleccionar credito de cliente*/
  function credito_cliente_select(cliente_id,creditoid) {
    $.ajax({
      type: "POST",
      url: VISTA_URL + "llave/llave_credito_select.php",
      async: true,
      dataType: "html",
      data: {
        cliente_id: cliente_id, //estos datos se envian con el metodo post
        creditoid: creditoid
      },
      beforeSend: function () {
        $("#cmb_credito").html("<option>Cargando...</option>");
      },
      success: function (data) {
        console.log(data)
        $("#cmb_credito").html(data);
        $("#cmb_credito").change();

      },
      complete: function (data) {
        //console.log(data);
      },
      error: function (data) {
        alert(data.responseText);
      },
    });
  }

  var action= $("#action").val();

  

  if (action !== 'insertar') {
    credito_cliente_select($("#hdd_llave_clienteid").val(), $("#hdd_llave_creditoid").val());
  
    // Deshabilitar el campo si la acción es 'eliminar' o 'modificar'
    if (action === 'eliminar') {
      disabled($('#txt_llave_clienteid'));
      disabled($('#cmb_credito'));
      disabled($('#cmb_clasevehiculo'));
      disabled($('#cmb_marcavehiculo'));
      disabled($('#cmb_modelovehiculo'));
      disabled($('#cmb_numcasillero'));
      disabled($('#radio_persjur'));

    }
  }
});

function confDatosVeh(creditoid, creditotipo_id) {
  console.log('consulta de datos para llenar options: CREDITO ID: ' + creditoid + ' / tipo: ' + creditotipo_id)
  $.ajax({
    type: "POST",
    url: VISTA_URL + "llave/llave_controller.php",
    async: true,
    dataType: "json",
    data: {
      action: "llenardatos",
      //el primero va al controller,el segundo es el parametro
      creditotipo_id:creditotipo_id,
      credito_id:creditoid

    },
    beforeSend: function () {},
    success: function (data) {
     
      $("#cmb_clasevehiculo").val(parseInt(data.claseid));
      $("#cmb_marcavehiculo").val(parseInt(data.marcaid));
      $("#cmb_modelovehiculo").val(parseInt(data.modeloid));
      $("#hdd_llave_motor").val(data.motor);
      $("#hdd_llave_chasis").val(data.chasis);
      $("#hdd_llave_placa").val(data.placa);
      console.log(data)
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      alert(data.responseText);
    },
  });
}

/*function serializar_form_llave() {

    var elementos_disabled = $('#form_llave').find('input:disabled, select:disabled, textarea:disabled').removeAttr('disabled');
    var form_serializado = $('#form_llave').serialize();
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}*/