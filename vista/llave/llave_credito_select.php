<?php
require_once('Llave.class.php');
$oLlave = new Llave();

// esta variable puede ser definida en un archivo en donde se haga un require_once o un include
$llave_idcliente = (empty($llave_idcliente))? $_POST['cliente_id'] : $llave_idcliente; // aquí se recibe el post del js
$creditoid = (empty($creditoid))? $_POST['creditoid'] : $creditoid; // aquí se recibe el post del js

$option = "<option value='0'>Seleccione...</option>";

if (intval($llave_idcliente) > 0) {
    $result = $oLlave->llenarCombo($llave_idcliente);

    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            $selected = '';
            $tipo_credito = '';
            switch ($value['tipocredito']) {
                case 1:
                    $tipo_credito = 'C-MENOR';
                    break;
                case 2:
                    $tipo_credito = 'ASVEH';
                    break;
                case 3:
                    $tipo_credito = 'GARVEH';
                    break;
                default:
                    $tipo_credito = 'Desconocido';
                    break;
            }
            if ($creditoid == $value['credito_id'])//la consulta devuelve varios creditoid y el codigo lo quer hace
                $selected = 'selected';
            $option .= '<option data-tipocredito="'.$value['tipocredito'].'" value="'.$value['credito_id'].'" style="font-weight: bold;" '.$selected.'>' . $tipo_credito . ' - ID: '.$value['credito_id'].' - PLACA: ' .$value['placa'].' - N° MOTOR: '.$value['numero_motor'].' - N° CHASIS: '.$value['numero_chasis'].'</option>';
        }
    }
    $result = NULL;
}
// FIN PRIMER NIVEL
echo $option;
?>
