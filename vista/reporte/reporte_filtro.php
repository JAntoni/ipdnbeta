

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-body">
                <form id="form_controlsefuro_filtro" class="form-inline" role="form">
                    <!--<a class="btn btn-primary btn-sm" href="javascript:void(0)" onClick="controlseguro_form('insertar', 0)" title="Agregar un Nuevo Registro al Sistema"><i class="fa fa-plus" aria-hidden="true" title="Agregar Comentario"></i></a>-->

                    <div class="form-group">
                        <!--<label for="">Fecha </label>-->
                        <div class="input-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control input-sm" name="txt_reporte_fec1" type="text" id="txt_reporte_fec1" value="<?php echo $fec1?>" readonly>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <span class="input-group-addon">-</span>
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' class="form-control input-sm" name="txt_reporte_fec2" type="text" id="txt_reporte_fec2" value="<?php echo $fec2?>" readonly>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                   <div class="form-group">
                       <select class="form-control input-sm" id="cmb_tipo_cre" name="cmb_tipo_cre">
                            <option value="0">...Tipo de Credito...</option>
                            <option value="1">Crédito Menor</option>
                            <option value="2">Crédito ASIVEH</option>
                            <option value="3">Crédito GARVEH</option>
                            <option value="4">Crédito HIPO</option>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="text" class="form-control input-sm" name="txt_cambio_mon" id="txt_cambio_mon" value="<?php echo $tipo_cambio;?>" readonly title="Tipo de Cambio">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" name="txt_veh_pla" id="txt_veh_pla" title="Placa del Vehiculo" placeholder="Ingrese Placa del Vehiculo">
                    </div>
                    <div class="form-group">
                        <select class="form-control input-sm" name="cmb_anio" id="cmb_anio" title="Año">
	            <?php 
	              $anio = intval(date('Y'));
	              for ($i = 2016; $i < ($anio + 5); $i++) { 
	                if($anio == $i)
	                  echo '<option value="'.$i.'" selected="true">'.$i.'</option>';
	                else
	                  echo '<option value="'.$i.'">'.$i.'</option>';
	              }
	            ?>
	          </select>
                    </div>
                    <div class="form-group">
                       <select class="form-control input-sm" id="cmb_mes" name="cmb_mes">
                            <option value="01">ENERO</option>
                            <option value="02">FEBRERO</option>
                            <option value="03">MARZO</option>
                            <option value="04">ABRIL</option>
                            <option value="05">MAYO</option>
                            <option value="06">JUNIO</option>
                            <option value="07">JULIO</option>
                            <option value="08">AGOSTO</option>
                            <option value="09">SETIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                    </div>
                    
<!--                    <div class="form-group">
                        <a href="javascript:void(0)" onClick="controlseguro_tabla()" class="btn btn-primary btn-sm" title="Buscar"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div>-->
                </form>
            </div>
        </div>
    </div>
</div>