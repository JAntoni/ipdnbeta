
<?php

date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
    die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
//echo 'hasta aki estoy entrando normal ssssss'; exit();
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';

require_once("Reporte.class.php");
$oReporte = new Reporte();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("cristhian.cyga@gmail.com")
	->setLastModifiedBy("cristhian.cyga@gmail.com")
	->setTitle("Reporte")
	->setSubject("Reporte")
	->setDescription("Reporte generado por cristhian.cyga@gmail.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloColumnas = array(
    'font' => array(
        'name' => 'cambria',
        'bold' => true,
        'size' => 10,
        'color' => array(
            'rgb' => 'FFFFFF'
        )
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array(
            'rgb' => '135896')
    ),
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb' => '143860'
            )
        )
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => TRUE
    )
);


$nombre_archivo = "REPORTE IMPUESTOS GARMOB-" . $_GET['anio'];

//----------------------- INICIO DE UNA HOJA -------------------------------//

$meses = ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];
$num = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

//for ($i = 0; $i < count($meses); $i++) {
$i=0;
//    if($i > 0)
    $objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex($i);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

  $c = 2;
  $titulo = "Reporte de Intereses del mes de ".$meses[$i];
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:F$c");
  $objPHPExcel->getActiveSheet()->setCellValue("A$c",$titulo);

  $c = 3;
  $c = $c + 1;

    $titulosColumnas = array(
    'N° CRÉDIO',
    'CLIENTE',
    'CAPITAL',
    'CUOTA MÍNIMA',
    'AMORTIZACION',
    'INTERÉS TOTAL',
    'UTILIDAD',
    'IGV',
    'FECHA PAGO',
    'N° y FEC. CUOTA',
    'REPRESENTANTE',
    'TIPO DE PAGO',
    'MONEDA',
    'PAGOS TOTALES'
  );

    $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
    ->setCellValue('I'.$c,  $titulosColumnas[8])
    ->setCellValue('J'.$c,  $titulosColumnas[9])
    ->setCellValue('K'.$c,  $titulosColumnas[10])
    ->setCellValue('L'.$c,  $titulosColumnas[11])
    ->setCellValue('M'.$c,  $titulosColumnas[12])
    ->setCellValue('N'.$c,  $titulosColumnas[13])
    ->setCellValue('O'.$c,  $titulosColumnas[14])
  ;
    
    $objPHPExcel->getActiveSheet()->getStyle("A$c:O$c")->applyFromArray($estiloTituloColumnas);

    $c = $c + 1;
    //----------------CODIGO DE FOREACH O WHILE ----------------------- //
    $anio = $_GET['anio'];
    $cre_tip = $_GET['cre_tip'];

    $credito = 'tb_creditogarveh';
    if (intval($cre_tip) == 4) {
        $credito = 'tb_creditohipo';
        $nombre_archivo = "REPORTE IMPUESTOS HIPOTECA-" . $anio;
    }
    $mes = $num[$i];

    $arrayCredito = array();
    $dts = $oReporte->credito_pagos_interes_igv($anio, $mes, $credito, $cre_tip);

    
    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            
            $cuota = $dt['tb_cuota_cuo'];
            $igv = $dt['igv'];
            $utilidad = $dt['utilidad'];
            $representante = $dt['tb_representante_nom'];
            $credito_id = intval($dt['tb_credito_id']);
            $cliente_id = intval($dt['tb_cliente_id']);

            if (intval($dt['tb_cuotatipo_id']) == 3) {
                $cuota = $dt['tb_cuota_int']; // cuota LIBRE
            }
            if (intval($cre_tip) == 4) {
                $igv = '0.00';
                $utilidad = $dt['tb_cuota_int'];
                $representante = $dt['tb_representante_nom'];
            }

            $moneda = "S./";
            if (intval($dt['tb_moneda_id']) == 2)
                $moneda = "$$";

            if ($dt['tb_cuota_est'] == 1)
                $estado = 'POR COBRAR';
            if ($dt['tb_cuota_est'] == 2)
                $estado = 'CANCELADA';
            if ($dt['tb_cuota_est'] == 3)
                $estado = 'PAGO PARCIAL';

            $cliente_nombre = $dt['tb_cliente_nom'];
            if (intval($dt['tb_cliente_tip']) == 2)
                $cliente_nombre = $dt['tb_cliente_emprs'] . ' | ' . $dt['tb_cliente_empruc'];

            $numopera = 'PAGO EN OFICINA';
            if ($dt['tb_ingreso_numope'] != "")
                $numopera = 'PAGO EN BANCO';

            $objPHPExcel->getActiveSheet(0)
                    ->setCellValue('A' . $c, $dt['tb_credito_id'])
                    ->setCellValue('B' . $c, $cliente_nombre)
                    ->setCellValue('C' . $c, $dt['tb_cuota_cap'])
                    ->setCellValue('D' . $c, $cuota)
                    ->setCellValue('E' . $c, $dt['tb_cuota_amo'])
                    ->setCellValue('F' . $c, $dt['tb_cuota_int'])
                    ->setCellValue('G' . $c, $utilidad)
                    ->setCellValue('H' . $c, $igv)
                    ->setCellValue('I' . $c, mostrar_fecha($dt['tb_ingreso_fec']))
                    ->setCellValue('J' . $c, $dt['tb_cuota_num'] . '/' . $dt['tb_credito_numcuo'] . ' | ' . mostrar_fecha($dt['tb_cuota_fec']))
                    ->setCellValue('K' . $c, $representante)
                    ->setCellValue('L' . $c, $estado)//'PAGO DE CUOTA'
                    ->setCellValue('M' . $c, $moneda)
                    ->setCellValue('N' . $c, $dt['tb_ingreso_imp'])
                    ->setCellValue('O' . $c, $numopera)
            ;

            if (!in_array($credito_id, $arrayCredito)) {
                array_push($arrayCredito, $credito_id);

                $dts2 = $oIngreso->ingresos_amor_liq_cliente_credito($cliente_id, $credito_id, $cre_tip, $anio, $mes);
               
                if($dts2['estado']==1){
                    $prorrateo = $dts2['data']['prorrateo'];
                    $importe = $dts2['data']['importe'];
                    $tipo_pag = ($dts2['data']['tb_cuenta_id'] == 35) ? 'AMORTIZACION' : 'LIQUIDACION';
                    $fec_ingre = $dts2['data']['tb_ingreso_fec'];
                }
                $dts2 =NULL;

                if ($registros > 0) {
                    $c++;

                    $objPHPExcel->getActiveSheet()->mergeCells("D$c:E$c");
                    $objPHPExcel->getActiveSheet()->setCellValue("D$c", $importe);
                    $objPHPExcel->getActiveSheet(0)
                            ->setCellValue('A' . $c, $dt['tb_credito_id'])
                            ->setCellValue('B' . $c, $cliente_nombre)
                            ->setCellValue('C' . $c, $dt['tb_cuota_cap'])
                            ->setCellValue('F' . $c, $prorrateo)
                            ->setCellValue('G' . $c, '')
                            ->setCellValue('H' . $c, '')
                            ->setCellValue('I' . $c, mostrar_fecha($fec_ingre))
                            ->setCellValue('J' . $c, '')
                            ->setCellValue('K' . $c, $representante)
                            ->setCellValue('L' . $c, $tipo_pag)
                            ->setCellValue('M' . $c, $moneda)
                            ->setCellValue('N' . $c, $dt['tb_ingreso_imp'])
                    ;

                    cellColor("D$c", '30F908');
                    cellColor("K$c", '30F908');
                }
            }

            $objPHPExcel->getActiveSheet(0)->getStyle('C' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet(0)->getStyle('D' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet(0)->getStyle('E' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet(0)->getStyle('F' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet(0)->getStyle('G' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet(0)->getStyle('H' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet(0)->getStyle('N' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $c++;
        }
    }
    $dts = NULL;
//echo 'hasta aki entrando normal2';exit();
    $arrayCredito = NULL;

    $c = $c + 2;
    $objPHPExcel->getActiveSheet()->mergeCells("A$c:N$c");
    $objPHPExcel->getActiveSheet()->setCellValue("A$c", 'CLIENTES QUE NO PAGARON SU CUOTA FACTURADA ESTE MES');
    cellColor("A$c", 'F56A6A');
    $c = $c + 2;

    $dts = $oReporte->cuotas_facturadas_no_pagadas($anio, $mes, $credito, $cre_tip);

    if ($dts['estado'] == 1) {
        foreach ($dts['data']as $key => $dt) {
            $cuota = $dt['tb_cuota_cuo'];
            $igv = $dt['igv'];
            $utilidad = 0;
            $representante = $dt['tb_representante_nom'];
            $credito_id = intval($dt['tb_credito_id']);
            $cliente_id = intval($dt['tb_cliente_id']);

            if (intval($dt['tb_cuotatipo_id']) == 3) {
                $cuota = $dt['tb_cuota_int']; // cuota LIBRE
            }
            if (intval($cre_tip) == 4) {
                $igv = '0.00';
                $utilidad = 0;
                $representante = $dt['tb_representante_nom'];
            }

            $cliente_nombre = $dt['tb_cliente_nom'];
            if (intval($dt['tb_cliente_tip']) == 2) {
                $cliente_nombre = $dt['tb_cliente_emprs'] . ' | ' . $dt['tb_cliente_empruc'];
            }

            $objPHPExcel->getActiveSheet(0)
                    ->setCellValue('A' . $c, $dt['tb_credito_id'])
                    ->setCellValue('B' . $c, $cliente_nombre)
                    ->setCellValue('C' . $c, $dt['tb_cuota_cap'])
                    ->setCellValue('D' . $c, $cuota)
                    ->setCellValue('E' . $c, $dt['tb_cuota_amo'])
                    ->setCellValue('F' . $c, $dt['tb_cuota_int'])
                    ->setCellValue('G' . $c, 0)
                    ->setCellValue('H' . $c, 0)
                    ->setCellValue('I' . $c, '')
                    ->setCellValue('J' . $c, $dt['tb_cuota_num'] . '/' . $dt['tb_credito_numcuo'] . ' | ' . mostrar_fecha($dt['tb_cuota_fec']))
                    ->setCellValue('K' . $c, $representante)
                    ->setCellValue('L' . $c, 'SIN PAGO')
            ;

            $c++;
        }
    }
    $dts = NULL;

    //--------------------FIN DEL BUCLE ------------------------------//

    $c = $c + 1;
    for ($z = 'A'; $z <= 'N'; $z++) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
    }
 // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle($meses[$i]);
//}
//----------------------FIN DE HOJA ------------------------------------------//
    
function cellColor($cells, $color){
  global $objPHPExcel;

  $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
      'type' => PHPExcel_Style_Fill::FILL_SOLID,
      'startcolor' => array(
           'rgb' => $color
      )
  ));
}
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header("Content-type: text/csv");
header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.csv"');
header("Pragma: no-cache");
header("Expires: 0");

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");

//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;    
