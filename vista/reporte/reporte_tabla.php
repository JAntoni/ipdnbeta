<?php

?>

  <table cellspacing="1" id="tabla_lista_reportes" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
        <th id="tabla_cabecera_fila">ID</th>
        <th id="tabla_cabecera_fila">TIPO REPORTE</th>
        <th id="tabla_cabecera_fila">DESCRIPCIÓN</th>
        <th id="tabla_cabecera_fila">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">1</td>
        <td id="tabla_fila">Ingresos y Egresos</td>
        <td id="tabla_fila">Se genera un reporte de todos los ingresos y egresos diarios, filamente hace una sumatoria de todos los ingreos para poder restarlos de los egresos.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('ingreso_egreso')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">2</td>
        <td id="tabla_fila">Ingresos por Tipo de Crédito</td>
        <td id="tabla_fila">Se genera un reporte de todos los ingresos por un tipo de crédito, para ello elija un rango de fechas y un tipo de crédito del desplegable.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('ingreso_tipo_credito')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">3</td>
        <td id="tabla_fila">Ingresos y Egresos por Cuentas</td>
        <td id="tabla_fila">Se genera un reporte de todos los ingresos y egresos pors tipos de cuentas, para ello se mostrarán las cuentas disponibles para poder ser seleccionadas solo las que se desea mostrar en el reporte.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="lista_subcuentas()">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">4</td>
        <td id="tabla_fila">Caja Cliente</td>
        <td id="tabla_fila">Se genera un reporte del saldo disponible en caja cliente para cada uno de los clientes en un crédito determinado, para ello seleccione un tipo de crédito, no importa la fecha.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('caja_cliente')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">5</td>
        <td id="tabla_fila">Gastos de Vehículos</td>
        <td id="tabla_fila">Se genera un reporte de los gastos que genera un vehículo, tanto como esté vigente o los gastos de reparación para ser vendido. Debe elegir un rango de fechas, para filtrar los créditos resueltos entre esas fechas.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('gastos_vehiculos')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">6</td>
        <td id="tabla_fila">Facturado VS Cobrado</td>
        <td id="tabla_fila">Se genera un reporte por crédito, la suma total de cuotas facturadas vs la suma total de pagos. Para ello seleccione un rango de fechas.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="facturado_cobrado()">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">7</td>
        <td id="tabla_fila">Cronograma de Créditos</td>
        <td id="tabla_fila">Se genera un reporte con el cronograma de todos los créditos registrados vigentes o reprogramados en un determinado rango de fechas. También se puede agregar una PLACA para poder filtrar solamente por ese vehículo.</td>
        <td id="tabla_fila" align="center">
            <select id="cmb_cronograma" class="form-control input-sm">
            <option value="">--</option>
            <option value="creditos_nuevos">Créditos Nuevos</option>
            <option value="creditos_reprogramado">Créditos Reprogramados</option>
          </select>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">8</td>
        <td id="tabla_fila">Gestión de Cobranza por Usuario</td>
        <td id="tabla_fila">Se genera un reporte de montos facturados de los clientes asignados a cada colaborador, los montos facturados en moneda soles y dólares.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('gc_por_usuario')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">9</td>
        <td id="tabla_fila">Avance de Pagos por Crédito</td>
        <td id="tabla_fila">Se genera un reporte del avance de pagos por créditos, tiempo contratado, cuota actual del cliente y monto hasta esa cuota.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="avance_pagos()">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">10</td>
        <td id="tabla_fila">Cajas de Seguros Retenidas</td>
        <td id="tabla_fila">Se genera un reporte del total de dinero retenido para pago de seguros por cliente.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('caja_seguros')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">11</td>
        <td id="tabla_fila">Capital Restante</td>
        <td id="tabla_fila">Se genera un reporte mostrando el capital restante por recuperar del crédito seleccionado.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="capital_recuperar()">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">12</td>
        <td id="tabla_fila">Clientes Deudores</td>
        <td id="tabla_fila">Se genera un reporte con la lista de clientes deudores, su número de cuotas vencidas y las fechas de las cuotas.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('cliente_deudor')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">13</td>
        <td id="tabla_fila">Interes e IGV</td>
        <td id="tabla_fila">Se genera un reporte con el avance de pagos de los clientes de los créditos GARANTÍA MOBILIRIA, de ellos el interés total, la utilidad y el IGV del interés pagado. Reporte mensual y por año seleccionado.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('interes_igv')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">14</td>
        <td id="tabla_fila">Reporte Vehículos Comprados</td>
        <td id="tabla_fila">Se genera un reporte de todos los vehículos que han sido comprado por ASISTENCIA VEHICULAR.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('vehiculos_comprados')">Generar</a>
        </td>
      </tr>
      <tr id="tabla_cabecera_fila">
        <td id="tabla_fila">15</td>
        <td id="tabla_fila">Ranking de Pagadores</td>
        <td id="tabla_fila">Se genera un reporte de todos los Clientes que han solicitado un crédito y han liquidado, los meses totales de su liquidación. Selecciona el tipo de crédito primero.</td>
        <td id="tabla_fila" align="center">
          <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="generar_reporte('ranking_pagadores')">Generar</a>
        </td>
      </tr>
    </tbody>
  </table>
