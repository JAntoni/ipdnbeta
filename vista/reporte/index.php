<?php
require_once('core/usuario_sesion.php');
require_once ("vista/monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once ('vista/funciones/funciones.php');
require_once ('vista/funciones/fechas.php');

$fec1 = date('d-m-Y');
$fec2 = date('d-m-Y');

$fec_moneda = date('Y-m-d');
$tipo_cambio = 0;
$dts = $oMonedacambio->consultar($fec_moneda);
if ($dts['estado'] == 1) {
    $tipo_cambio = formato_moneda($dts['data']['tb_monedacambio_val']);
}
$dts = NULL;


?>
<!DOCTYPE html>
<html>
    <head>
        <!-- TODOS LOS ESTILOS-->
        <?php include(VISTA_URL . 'templates/head.php'); ?>
        <title><?php echo ucwords(mb_strtolower($menu_tit)); ?></title>
    </head>

    <body <?php echo 'class="' . CLASE_BODY . ' ' . $usuario_tem . '"'; ?>>

        <div class="wrapper">
            <!-- INCLUIR EL HEADER-->
            <?php include(VISTA_URL . 'templates/header.php'); ?>
            <!-- INCLUIR ASIDE, MENU LATERAL -->
            <?php include(VISTA_URL . 'templates/aside.php'); ?>
            <!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
            <?php include('reporte_vista.php');?>
            <!-- INCLUIR FOOTER-->
            <?php include(VISTA_URL . 'templates/footer.php'); ?>
            <div class="control-sidebar-bg"></div>
        </div>

        <!-- TODOS LOS SCRIPTS-->
            <?php include(VISTA_URL . 'templates/script.php'); ?>

        <script type="text/javascript" src="<?php echo VISTA_URL . 'reporte/reporte.js?ver=191224'; ?>"></script>
    </body>
</html>
