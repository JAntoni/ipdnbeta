

$(document).ready(function () {
    reportes_tabla();

});

function reportes_tabla()
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "reporte/reporte_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            //pro_est:	$('#cmb_fil_pro_est').val()
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_reporte_tabla').html(html);
        },
        complete: function () {

        }
    });
}

$('#cmb_cronograma').change(function (event) {
    var cro = $(this).val();
    //console.log(cro);
    generar_reporte(cro);
});


function generar_reporte(tipo) {
    if (tipo == 'ingreso_egreso') {
        var fecha1 = $('#txt_reporte_fec1').val();
        var fecha2 = $('#txt_reporte_fec2').val();
        var url = '../flujocaja/doc_flujocaja_diario_excel.php?fecha1=' + fecha1 + '&fecha2=' + fecha2;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
    if (tipo == 'caja_cliente') {
        var cre_tip = $('#cmb_tipo_cre').val();
        if (parseInt(cre_tip) == 0) {
            alert('Elija un tipo de crédito');
            return false;
        }
        var url = '../reportes/doc_clientecaja_creditos_excel.php?tipo=' + cre_tip;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
    if (tipo == 'ingreso_tipo_credito') {
        var cre_tip = $('#cmb_tipo_cre').val();
        var fecha1 = $('#txt_reporte_fec1').val();
        var fecha2 = $('#txt_reporte_fec2').val();
        if (parseInt(cre_tip) == 0 || !fecha1 || !fecha2) {
            alert('Elija un tipo de crédito y un rango de fechas');
            return false;
        }
        var url = '../reportes/doc_ingresos_cuentas_credito_excel.php?tipo=' + cre_tip + '&fecha1=' + fecha1 + '&fecha2=' + fecha2;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
    if (tipo == 'gastos_vehiculos') {
        var fecha1 = $('#txt_reporte_fec1').val();
        var fecha2 = $('#txt_reporte_fec2').val();
        var tip_cam = Number($('#txt_cambio_mon').val());
        if (tip_cam <= 0) {
            alert('Registre el tipo de cambio del día por favor.');
            return false;
        }
        var url = '../reportes/doc_gastos_vehiculos_excel.php?fecha1=' + fecha1 + '&fecha2=' + fecha2 + '&cambio=' + tip_cam;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
    if (tipo == 'creditos_nuevos') {
        var fecha1 = $('#txt_reporte_fec1').val();
        var fecha2 = $('#txt_reporte_fec2').val();
        var veh_pla = $('#txt_veh_pla').val();

        var url = '../reportes/reporte_cronograma_credito.php?fecha1=' + fecha1 + '&fecha2=' + fecha2 + '&tipo=' + tipo + '&placa=' + veh_pla;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
    if (tipo == 'creditos_reprogramado') {
        var fecha1 = $('#txt_reporte_fec1').val();
        var fecha2 = $('#txt_reporte_fec2').val();
        var veh_pla = $('#txt_veh_pla').val();

        var url = '../reportes/reporte_cronograma_credito.php?fecha1=' + fecha1 + '&fecha2=' + fecha2 + '&tipo=' + tipo + '&placa=' + veh_pla;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
    if (tipo == 'gc_por_usuario') {
        var fecha1 = $('#txt_reporte_fec1').val();
        var fecha2 = $('#txt_reporte_fec2').val();

        var url = '../reportes/reporte_gc_usuario_excel.php?fecha1=' + fecha1 + '&fecha2=' + fecha2;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
    if (tipo == 'caja_seguros') {
        var url = '../reportes/reporte_caja_seguros_retenido_excel.php';
        window.open(url, "_blank");
    }
    if (tipo == 'cliente_deudor') {
        var url = '../reportes/reporte_clientes_deudores_excel.php';
        window.open(url, "_blank");
    }
    if (tipo == 'interes_igv') {
        var anio = $('#cmb_anio').val();
        var cre_tip = $('#cmb_tipo_cre').val();
        var mes = $('#cmb_mes').val();
        if (parseInt(cre_tip) <= 0) {
            swal_warning('PRESTAMOS DEL NORTE', 'Elija el tipo de crédito y año por favor', 3000);
            return false;
        }
        if ( !($.inArray($('#cmb_mes').val(), ['01','02','03','04','05','06','07','08','09','10','11','12']) > -1) ) // -1
        {
            swal_error('PRESTAMOS DEL NORTE', 'El mes seleccionado es inválido', 3000);
            return false;
        }
        var url = 'vista/reporte/13_reporte_impuestos_creditos_excel.php?anio=' + anio + '&cre_tip=' + cre_tip + '&mes=' + mes;
        window.open(url, "_blank");
    }
    if (tipo == 'vehiculos_comprados') {
        var cre_tip = $('#cmb_tipo_cre').val();
        if (parseInt(cre_tip) <= 0) {
            alert('Elija el tipo de crédito');
            return false;
        }
        var url = '../reportes/reporte_vehiculos_comprados_excel.php?cre_tip=' + cre_tip;
        window.open(url, "_blank");
    }
    if (tipo == 'ranking_pagadores') {
        var cre_tip = $('#cmb_tipo_cre').val();

        if (parseInt(cre_tip) == 0) {
            alert('Elija un tipo de crédito');
            return false;
        }
        var url = '../reportes/reporte_ranking_pagadores_excel.php?cre_tip=' + cre_tip;
        window.open(url, "_blank");
        console.log('debe ir a: ' + url);
    }
}