<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Reporte extends Conexion {

    function ingresos_creditomenor_cuentas($fecha1, $fecha2, $mon_id) {
        try {
            //cuentas: PAGO CUOTAS (1), MORAS (26), AMORTIZACION (35), LIQUIDACION (36)
            //SUBCUENTAS: CUOTAS MENOR (2), MORAS MENOR (67), AMORTIZACION MENOR (105), LIQUIDACION MENOR (109)
            $sql = "SELECT 
                        tb_ingreso_fec,SUM(tb_ingreso_imp) as ingreso 
                    FROM 
                        tb_cuenta cu 
                        inner join tb_subcuenta sub on sub.tb_cuenta_id = cu.tb_cuenta_id 
                        inner join tb_ingreso ing on ing.tb_subcuenta_id = sub.tb_subcuenta_id 
                    where 
                        (ing.tb_cuenta_id = 1 OR ing.tb_cuenta_id = 26 OR ing.tb_cuenta_id = 35 OR ing.tb_cuenta_id = 36) 
                        and (ing.tb_subcuenta_id = 2 OR ing.tb_subcuenta_id = 67 OR ing.tb_subcuenta_id = 105 OR ing.tb_subcuenta_id = 109) 
                        and tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and tb_ingreso_fec BETWEEN :fecha1 and :fecha2 group by 1 order by 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function ingresos_creditoasiveh_cuentas($fecha1, $fecha2, $mon_id) {
        try {
            //cuentas: PAGO CUOTAS (1), MORAS (26), AMORTIZACION (35), LIQUIDACION (36)
            //SUBCUENTAS: CUOTAS MENOR (2), MORAS MENOR (67), AMORTIZACION MENOR (105), LIQUIDACION MENOR (109)
            //cuentas: PAGO CUOTAS (1), MORAS (26), RETENCIONES (31), AMORTIZACION (35), LIQUIDACION (36)
            //SUBCUENTAS: CUOTAS ASIVEH (1), MORAS ASIVEH (68), RETENCIONES ASIVEH (100), AMORTIZACION ASIVEH (106), LIQUIDACION ASIVEH (110)
            $sql = "SELECT 
                            tb_ingreso_fec,SUM(tb_ingreso_imp) as ingreso 
                        FROM 
                            tb_cuenta cu inner join tb_subcuenta sub on sub.tb_cuenta_id = cu.tb_cuenta_id 
                            inner join tb_ingreso ing on ing.tb_subcuenta_id = sub.tb_subcuenta_id 
                        where 
                            (ing.tb_cuenta_id = 1 OR ing.tb_cuenta_id = 26 OR ing.tb_cuenta_id = 31 OR ing.tb_cuenta_id = 35 OR ing.tb_cuenta_id = 36)
                            and (ing.tb_subcuenta_id = 1 OR ing.tb_subcuenta_id = 68 OR ing.tb_subcuenta_id = 100 OR ing.tb_subcuenta_id = 106 OR ing.tb_subcuenta_id = 110) 
                            and tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and tb_ingreso_fec BETWEEN :fecha1 and :fecha2 group by 1 order by 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function ingresos_creditogarveh_cuentas($fecha1, $fecha2, $mon_id) {
        try {
            //cuentas: PAGO CUOTAS (1), MORAS (26), AMORTIZACION (35), LIQUIDACION (36)
            //SUBCUENTAS: CUOTAS GARVEH (4), MORAS GARVEH (70), AMORTIZACION GARVEH (108), LIQUIDACION GARVEH (112)
            $sql = "SELECT 
                            tb_ingreso_fec,SUM(tb_ingreso_imp) as ingreso 
                        FROM tb_cuenta cu 
                            inner join tb_subcuenta sub on sub.tb_cuenta_id = cu.tb_cuenta_id 
                            inner join tb_ingreso ing on ing.tb_subcuenta_id = sub.tb_subcuenta_id 
                        where 
                            (ing.tb_cuenta_id = 1 OR ing.tb_cuenta_id = 26 OR ing.tb_cuenta_id = 35 OR ing.tb_cuenta_id = 36)
                            and (ing.tb_subcuenta_id = 4 OR ing.tb_subcuenta_id = 70 OR ing.tb_subcuenta_id = 108 OR ing.tb_subcuenta_id = 112) 
                            and tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and tb_ingreso_fec BETWEEN :fecha1 and :fecha2 group by 1 order by 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function ingresos_creditohipo_cuentas($fecha1, $fecha2, $mon_id) {
        try {
            //cuentas: PAGO CUOTAS (1), MORAS (26), RETENCIONES (31), AMORTIZACION (35), LIQUIDACION (36)
            //SUBCUENTAS: CUOTAS HIPO (3), MORAS HIPO (69), RETENCIONES HIPO (99), AMORTIZACION HIPO (107), LIQUIDACION HIPO (111)
            $sql = "SELECT 
                            tb_ingreso_fec,SUM(tb_ingreso_imp) as ingreso
                         FROM 
                         tb_cuenta cu 
                            inner join tb_subcuenta sub on sub.tb_cuenta_id = cu.tb_cuenta_id 
                            inner join tb_ingreso ing on ing.tb_subcuenta_id = sub.tb_subcuenta_id 
                         where 
                            (ing.tb_cuenta_id = 1 OR ing.tb_cuenta_id = 26 OR ing.tb_cuenta_id = 31 OR ing.tb_cuenta_id = 35 OR ing.tb_cuenta_id = 36) 
                            and (ing.tb_subcuenta_id = 3 OR ing.tb_subcuenta_id = 69 OR ing.tb_subcuenta_id = 99 OR ing.tb_subcuenta_id = 107 OR ing.tb_subcuenta_id = 111) 
                            and tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and tb_ingreso_fec BETWEEN :fecha1 and :fecha2 group by 1 order by 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function ingresos_subcuentas($fecha1, $fecha2, $mon_id, $subcuentas) {
        try {
            $sql = "SELECT 
                        tb_ingreso_fec,SUM(tb_ingreso_imp) as ingreso 
                    FROM 
                        tb_cuenta cu 
                        inner join tb_subcuenta sub on sub.tb_cuenta_id = cu.tb_cuenta_id 
                        inner join tb_ingreso ing on ing.tb_subcuenta_id = sub.tb_subcuenta_id 
                    where 
                        ing.tb_subcuenta_id IN ($subcuentas) and tb_ingreso_xac = 1 and ing.tb_moneda_id =:moneda_id and tb_ingreso_fec BETWEEN :fecha1 and :fecha2 group by 1 order by 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function egresos_subcuentas($fecha1, $fecha2, $mon_id, $subcuentas) {
        try {
            $sql = "SELECT 
                        tb_egreso_fec,SUM(tb_egreso_imp) as egreso 
                    FROM 
                        tb_cuenta cu 
                        inner join tb_subcuenta sub on sub.tb_cuenta_id = cu.tb_cuenta_id 
                        inner join tb_egreso eg on eg.tb_subcuenta_id = sub.tb_subcuenta_id 
                    where 
                        eg.tb_subcuenta_id IN ($subcuentas) and tb_egreso_xac = 1 and eg.tb_moneda_id =:moneda_id and tb_egreso_fec BETWEEN :fecha1 and :fecha2 group by 1 order by 1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function clientes_creditos($credito_tabla) {
        try {
            $sql = "SELECT * FROM tb_cliente cli INNER JOIN $credito_tabla cre on cre.tb_cliente_id = cli.tb_cliente_id where tb_credito_xac = 1 and tb_cliente_xac = 1 and tb_credito_est NOT IN(1,2,7,8) group BY cli.tb_cliente_id order by tb_cliente_nom";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function cierres_creditos_fecha($fec1, $fec2, $usu_id) {
        try {
            $usuario = "";
            if ($usu_id != 0) {
                $usuario = "and tb_credito_usureg =" . $usu_id;
            }
            $sql = "
			SELECT 1 AS credito_tip, tb_credito_preaco, tb_credito_tipcam FROM `tb_creditomenor` where tb_credito_fecdes BETWEEN :fecha1 and :fecha2 and tb_credito_xac = 1 $usuario
			UNION ALL
			SELECT 2 AS credito_tip, tb_credito_preaco, tb_credito_tipcam FROM `tb_creditoasiveh` where tb_credito_fecvig BETWEEN :fecha1 and :fecha2 and tb_credito_tip1 IN(1,4,2) and tb_credito_est IN(3,7) and tb_credito_xac = 1 $usuario
			UNION ALL
			SELECT 3 AS credito_tip, tb_credito_preaco, tb_credito_tipcam FROM `tb_creditogarveh` where tb_credito_fecvig BETWEEN :fecha1 and :fecha2 and tb_credito_tip IN(1,2,4) and tb_credito_est IN(3,7) and tb_credito_xac = 1 $usuario
			UNION ALL
			SELECT 4 AS credito_tip, tb_credito_preaco, tb_credito_tipcam FROM `tb_creditohipo` where tb_credito_fecvig BETWEEN :fecha1 and :fecha2 and tb_credito_tip IN(1,2,4) and tb_credito_est IN(3,7) and tb_credito_xac = 1 $usuario";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);
            if ($usu_id != 0) {
                $sentencia->bindParam(":usureg", $usu_id, PDO::PARAM_STR);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function cierres_adendas_fecha($fec1, $fec2, $usu_id) {
        try {
            $usuario = "";
            if ($usu_id != 0) {
                $usuario = "and tb_credito_usureg =:usureg";
            }
            $sql = "
			SELECT 2 AS credito_tip, tb_credito_preaco, tb_credito_tipcam FROM `tb_creditoasiveh` where tb_credito_fecvig BETWEEN :fecha1 and :fecha2 and tb_credito_tip1 = 2 and tb_credito_est = 3 and tb_credito_xac = 1 $usuario
			UNION ALL
			SELECT 3 AS credito_tip, tb_credito_preaco, tb_credito_tipcam FROM `tb_creditogarveh` where tb_credito_fecvig BETWEEN :fecha1 and :fecha2 and tb_credito_tip = 2 and tb_credito_est = 3 and tb_credito_xac = 1 $usuario
			UNION ALL
			SELECT 4 AS credito_tip, tb_credito_preaco, tb_credito_tipcam FROM `tb_creditohipo` where tb_credito_fecvig BETWEEN :fecha1 and :fecha2 and tb_credito_tip = 2 and tb_credito_est = 3 and tb_credito_xac = 1 $usuario";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);
            if ($usu_id != 0) {
                $sentencia->bindParam(":usureg", $usu_id, PDO::PARAM_STR);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function monto_cuotas_facturadas_tipocredito_moneda($tabla, $cre_tip, $mon_id, $fec1, $fec2) {
        try {
            $sql = "
			SELECT 
                                IFNULL(SUM(CASE WHEN tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END),0) AS facturado 
                        FROM 
                                tb_cuota cu
                                INNER JOIN tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id
                                INNER JOIN $tabla cre on cre.tb_credito_id = cu.tb_credito_id
			where 
                            cu.tb_creditotipo_id =:creditotipo_id and tb_cuotadetalle_fec between :fecha1 and :fecha2 and tb_credito_xac = 1 and tb_credito_est IN (3,4) and cre.tb_moneda_id =:moneda_id and tb_cuotadetalle_estap = 0";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function monto_cuotasdetalle_cobradas_credito_moneda($tabla, $cre_tip, $mon_id, $fec1, $fec2, $cre_id) {
        try {
            $sql = "
			SELECT 
                                IFNULL(SUM(ing.tb_ingreso_imp), 0) as importe_total 
                        FROM 
                                tb_ingreso ing
                                INNER JOIN tb_cuotapago cp on ing.tb_ingreso_modide = cp.tb_cuotapago_id
                                INNER JOIN tb_cuotadetalle cud on cud.tb_cuotadetalle_id = cp.tb_cuotapago_modid
                                INNER JOIN tb_cuota cu on cu.tb_cuota_id = cud.tb_cuota_id
                                INNER JOIN $tabla cre on cre.tb_credito_id = cu.tb_credito_id
			where 
                                cu.tb_creditotipo_id =:creditotipo_id and cp.tb_modulo_id = 2 
                                and ing.tb_modulo_id = 30 and tb_ingreso_xac = 1 and tb_cuotapago_xac = 1 
                                and ing.tb_moneda_id =:moneda_id and tb_ingreso_fec between :fecha1 and :fecha2 
                                and tb_credito_xac = 1 and tb_credito_est IN(3,4,5,6,7,8) and cre.tb_moneda_id =:moneda_id and tb_ingreso_ap = 0";
            if ($cre_id > 0) {
                $sql .= " and cre.tb_credito_id =:credito_id";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);
            if ($cre_id > 0) {
                $sentencia->bindParam(":credito_id", $cre_id, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function monto_cuotas_facturadas_creditomenor($fec1, $fec2) {
        try {
            $sql = "
			SELECT IFNULL(SUM(CASE WHEN tb_cuotatipo_id = 1 THEN tb_cuota_int ELSE tb_cuota_cuo END),0) AS facturado FROM tb_cuota cu
			INNER JOIN tb_creditomenor cre on cre.tb_credito_id = cu.tb_credito_id
			where cu.tb_creditotipo_id =1 and tb_cuota_fec between :fecha1 and :fecha2 and tb_credito_xac = 1 and tb_credito_est IN (3,5)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function monto_cuotas_cobradas_creditomenor($fec1, $fec2) {
        try {
            $sql = "
                    SELECT 
                            IFNULL(SUM(ing.tb_ingreso_imp), 0) as importe_total 
                    FROM 
                            tb_ingreso ing
                            INNER JOIN tb_cuotapago cp on ing.tb_ingreso_modide = cp.tb_cuotapago_id
                            INNER JOIN tb_cuota cu on cu.tb_cuota_id = cp.tb_cuotapago_modid
                            INNER JOIN tb_creditomenor cre on cre.tb_credito_id = cu.tb_credito_id
                    where 
                            cu.tb_creditotipo_id =1 and cp.tb_modulo_id = 1 and ing.tb_modulo_id = 30 and tb_ingreso_xac = 1 
                            and tb_cuotapago_xac = 1 and tb_cuota_fec between :fecha1 and :fecha2 and tb_credito_xac = 1 and tb_credito_est IN(3,5)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function monto_cuotas_facturadas_tipocredito_moneda_detallado($tabla, $cre_tip, $mon_id, $fec1, $fec2, $cretip_id) {
        try {
            $select = "SELECT 
                   cre.tb_credito_id, tb_cliente_nom,tb_credito_tip1 as credito_tip,tb_credito_est,tb_cuota_fec, 
                   tb_cuota_amo, tb_cuota_int, IFNULL(SUM(CASE WHEN tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END),0) as facturado";

            if ($cre_tip != 2) {
                $select = "SELECT cre.tb_credito_id, tb_cliente_nom,tb_credito_tip as credito_tip,tb_credito_est,tb_cuota_fec, tb_cuota_amo, tb_cuota_int, 
                                IFNULL(SUM(CASE WHEN tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END),0) as facturado";
            }

            $sql = $select . " FROM tb_cuota cu
			INNER JOIN tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id
			INNER JOIN $tabla cre on cre.tb_credito_id = cu.tb_credito_id
			INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id
			where cu.tb_creditotipo_id =:creditotipo_id and cud.tb_cuotadetalle_fec between :fecha1 and :fecha2 and tb_credito_xac = 1 
                        and tb_credito_est IN(3,4) and cre.tb_moneda_id =:moneda_id and tb_cuotadetalle_estap = 0
		";
            if ($cre_tip == 2) {
                $sql .= " and tb_credito_tip1 IN(" . $cretip_id . ") group by 1"; // sin acuerdos de pago en asiveh
            } else {
                $sql .= " and tb_credito_tip IN(" . $cretip_id . ") group by 1"; // sin acuerdos de pago en garveh e hipotecario
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function monto_cuotas_facturadas_acuerdopago_moneda_detallado($tabla, $cre_tip, $mon_id, $fec1, $fec2, $cretip_id) {
        try {
            $select = "SELECT cre.tb_credito_id, tb_cliente_nom,tb_credito_tip1 as credito_tip,tb_credito_est,tb_cuota_fec,IFNULL(SUM(CASE WHEN tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END),0) as facturado";
            if ($cre_tip != 2) {
                $select = "SELECT cre.tb_credito_id, tb_cliente_nom,tb_credito_tip as credito_tip,tb_credito_est,tb_cuota_fec,IFNULL(SUM(CASE WHEN tb_cuotatipo_id = 3 THEN tb_cuota_int ELSE tb_cuotadetalle_cuo END),0) as facturado";
            }
            $sql = $select . " FROM tb_cuota cu
			INNER JOIN tb_cuotadetalle cud on cud.tb_cuota_id = cu.tb_cuota_id
			INNER JOIN $tabla cre on cre.tb_credito_id = cu.tb_credito_id
			INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id
			where cu.tb_creditotipo_id =:creditotipo_id and cud.tb_cuotadetalle_fec between :fecha1 and :fecha2 and tb_credito_xac = 1 
                        and tb_credito_est IN(3,4) and cre.tb_moneda_id =:moneda_id ";
            if ($cre_tip == 2) {
                $sql .= " and tb_credito_tip1 IN(" . $cretip_id . ") group by 1"; // sin acuerdos de pago en asiveh
            } else {
                $sql .= " and tb_credito_tip IN(" . $cretip_id . ") group by 1"; // sin acuerdos de pago en garveh e hipotecario
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->bindParam(":fecha1", $fec1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec2, PDO::PARAM_STR);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function clientes_creditos_estado_subcredito($credito_tabla, $cre_tip, $cre_est, $sub_cre, $cuota_tipo) {
        try {
            $columna_tip = 'tb_credito_tip';
            $tipos = $sub_cre;
            if ($cre_tip == 2) {
                $columna_tip = 'tb_credito_tip1';
            }
            if ($sub_cre == 1) {
                $tipos = '1,4'; //1,4 para GARVEH E HIPO: 1 GENERAL Y 4 HIPOTECA
            }


            $sql = "SELECT * FROM tb_cliente cli INNER JOIN $credito_tabla cre on cre.tb_cliente_id = cli.tb_cliente_id where tb_credito_xac = 1 
                        and cre.tb_cuotatipo_id in($cuota_tipo) and tb_cliente_xac = 1 and tb_credito_est in($cre_est) and $columna_tip IN($tipos) 
                        group BY cre.tb_credito_id order by tb_cliente_nom";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function credito_moneda($tabla, $mon_id, $cre_tip, $cli_id) {
        try {
            $sql = "SELECT * FROM $tabla where tb_moneda_id =:moneda_id and tb_credito_tip !=3 and tb_credito_xac = 1 and tb_credito_est in(3,4) and tb_cliente_id =:cliente_id";
            if ($cre_tip == 2) {
                $sql = "SELECT * FROM $tabla where tb_moneda_id =:moneda_id and tb_credito_tip1 !=3 and tb_credito_xac = 1 and tb_credito_est in(3,4) and tb_cliente_id =:cliente_id";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function clientes_credito($tabla, $cre_tip) {
        try {
            $sql = "SELECT * FROM $tabla cre INNER JOIN tb_cliente cl on cl.tb_cliente_id = cre.tb_cliente_id where tb_credito_tip !=3 and tb_credito_xac = 1 and tb_credito_est in(3,4) GROUP BY cl.tb_cliente_id";
            if ($cre_tip == 2) {
                $sql = "SELECT * FROM $tabla cre INNER JOIN tb_cliente cl on cl.tb_cliente_id = cre.tb_cliente_id where tb_credito_tip1 !=3 and tb_credito_xac = 1 and tb_credito_est in(3,4) GROUP BY cl.tb_cliente_id";
            }

            $sentencia = $this->dblink->prepare($sql);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function credito_por_cliente($tabla, $cre_tip, $cli_id) {
        try {
            $sql = "SELECT * FROM $tabla where tb_credito_tip !=3 and tb_credito_xac = 1 and tb_credito_est in(3,4) and tb_cliente_id =:cliente_id";
            if ($cre_tip == 2) {
                $sql = "SELECT * FROM $tabla where tb_credito_tip1 !=3 and tb_credito_xac = 1 and tb_credito_est in(3,4) and tb_cliente_id =:cliente_id";
            }

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cli_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function clientes_deudores_asiveh() {
        try {
            $sql = "SELECT cre.tb_credito_id, tb_cliente_nom, (CASE 
                            WHEN tb_credito_tip1 = 1 THEN 'VENTA NUEVA'
                            WHEN tb_credito_tip1 = 2 THEN 'ADENDA'
                            WHEN tb_credito_tip1 = 3 THEN 'ACUERDO PAGO'
                            WHEN tb_credito_tip1 = 4 THEN 'RE-VENTA'
                            ELSE 'S/N' END) AS tipo_credito, COUNT(tb_cuotadetalle_cuo) AS numero_cuotas, GROUP_CONCAT(DATE_FORMAT(tb_cuotadetalle_fec,  '%d-%m-%Y') SEPARATOR ', ') AS fechas_vencidas 
                    FROM 
                            tb_creditoasiveh cre 
                            INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
                            INNER JOIN tb_cuota cuo ON cre.tb_credito_id = cuo.tb_credito_id
                            INNER JOIN tb_cuotadetalle cud ON cuo.tb_cuota_id = cud.tb_cuota_id
                    WHERE 
                            cuo.tb_creditotipo_id = 2 and tb_credito_est in (3) and tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_cuotadetalle_est in (1,3) and tb_cuotadetalle_estap = 0 and tb_cuotadetalle_fec <= NOW()
                            GROUP by 2,3
                            ORDER BY 2 ASC, 3 DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function clientes_deudores_garveh() {
        try {
            $sql = "SELECT cre.tb_credito_id, tb_cliente_nom, (CASE 
                            WHEN tb_credito_tip = 1 THEN 'GARVEH'
                            WHEN tb_credito_tip = 2 THEN 'ADENDA'
                            WHEN tb_credito_tip = 3 THEN 'ACUERDO PAGO'
                            WHEN tb_credito_tip = 4 THEN 'GAR MOBILIARIA'
                            ELSE 'S/N' END) AS tipo_credito, COUNT(tb_cuotadetalle_cuo) AS numero_cuotas, GROUP_CONCAT(DATE_FORMAT(tb_cuotadetalle_fec,  '%d-%m-%Y') SEPARATOR ', ') AS fechas_vencidas 
                    FROM 
                            tb_creditogarveh cre 
                            INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
                            INNER JOIN tb_cuota cuo ON cre.tb_credito_id = cuo.tb_credito_id
                            INNER JOIN tb_cuotadetalle cud ON cuo.tb_cuota_id = cud.tb_cuota_id
                    WHERE 
                            cuo.tb_creditotipo_id = 3 and tb_credito_est in (3) and tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_cuotadetalle_est in (1,3) and tb_cuotadetalle_estap = 0 and tb_cuotadetalle_fec <= NOW()
                            GROUP by 2,3
                            ORDER BY 2 ASC, 3 DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function clientes_deudores_hipo() {
        try {
            $sql = "SELECT cre.tb_credito_id, tb_cliente_nom, (CASE 
                            WHEN tb_credito_tip = 1 THEN 'GARANTIA INMUEBLE'
                            WHEN tb_credito_tip = 2 THEN 'ADENDA'
                            WHEN tb_credito_tip = 3 THEN 'ACUERDO PAGO'
                            WHEN tb_credito_tip = 4 THEN 'HIPOTECA'
                            ELSE 'S/N' END) AS tipo_credito, COUNT(tb_cuotadetalle_cuo) AS numero_cuotas, GROUP_CONCAT(DATE_FORMAT(tb_cuotadetalle_fec,  '%d-%m-%Y') SEPARATOR ', ') AS fechas_vencidas 
                    FROM 
                            tb_creditohipo cre 
                            INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
                            INNER JOIN tb_cuota cuo ON cre.tb_credito_id = cuo.tb_credito_id
                            INNER JOIN tb_cuotadetalle cud ON cuo.tb_cuota_id = cud.tb_cuota_id
                    WHERE 
                            cuo.tb_creditotipo_id = 4 and tb_credito_est in (3) and tb_credito_xac = 1 and tb_cuota_xac = 1 and tb_cuotadetalle_est in (1,3) and tb_cuotadetalle_estap = 0 and tb_cuotadetalle_fec <= NOW()
                            GROUP by 2,3
                            ORDER BY 2 ASC, 3 DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function credito_pagos_interes_igv($anio, $mes, $credito, $cre_tip) {
        try {
            $sql = "SELECT 
                            cre.tb_credito_id, tb_representante_nom, cre.tb_cliente_id, tb_cliente_nom, tb_cliente_doc,
                            tb_cliente_tip, tb_cliente_emprs, tb_cliente_empruc,cre.tb_credito_preaco, cre.tb_cuotatipo_id, 
                            cuo.tb_cuota_id, tb_cuota_cap, tb_cuota_cuo, tb_cuota_int, tb_cuota_amo, ROUND((tb_cuota_int / 1.18),2) AS utilidad,
                            ROUND(tb_cuota_int - ROUND(tb_cuota_int / 1.18, 2), 2) AS igv, tb_ingreso_fec,tb_cuotadetalle_cuo, tb_cuotapago_mon, 
                            SUM(tb_ingreso_imp) AS tb_ingreso_imp, tb_credito_numcuo,ing.*, 
                            tb_cuota_num, tb_cuota_fec,ing.tb_moneda_id,tb_cuota_est,cupag.tb_cuotapago_tipcam,ing.tb_ingreso_numope,ing.tb_cuentadeposito_id
			FROM 
                            $credito cre
                            INNER JOIN tb_representante re on re.tb_representante_id = cre.tb_representante_id
                            INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id
                            INNER JOIN tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id
                            INNER join tb_cuotadetalle cud on cud.tb_cuota_id = cuo.tb_cuota_id
                            INNER JOIN tb_cuotapago cupag on cupag.tb_cuotapago_modid = cud.tb_cuotadetalle_id
                            INNER JOIN tb_ingreso ing on ing.tb_ingreso_modide = cupag.tb_cuotapago_id
			WHERE 
                            tb_credito_tip IN(1,2,4) and tb_credito_xac = 1 and tb_credito_est in(3,4,7,8,9,10) and cuo.tb_creditotipo_id = :creditotipo_id and tb_cuota_est in(2) 
                            and cupag.tb_modulo_id = 2 and tb_cuotapago_xac = 1 and tb_ingreso_xac = 1 and ing.tb_modulo_id = 30 and YEAR(tb_ingreso_fec) = :anio and MONTH(tb_ingreso_fec) =:mes GROUP BY cuo.tb_cuota_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
            $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function credito_vehiculos_comprados($credito, $cre_tip) {
        try {
            $sql = "SELECT * FROM $credito cre 
                    INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
                    WHERE tb_credito_tip1 = 1 AND tb_credito_xac = 1 ORDER BY tb_credito_vehpla";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function cuotas_facturadas_no_pagadas($anio, $mes, $credito, $cre_tip) {
        try {
            $sql = "SELECT 
                            cre.tb_credito_id,tb_cliente_doc, tb_cliente_nom, tb_cliente_tip, tb_cliente_emprs, tb_cliente_empruc, tb_cuota_cap, tb_cuota_cuo, 
                            tb_cuota_amo,tb_cuota_int,tb_cuota_fec, tb_cuota_est, tb_credito_numcuo, tb_cuota_num, tb_representante_nom 
                    FROM $credito cre 
                            INNER JOIN tb_cliente cli on cli.tb_cliente_id = cre.tb_cliente_id 
                            INNER JOIN tb_representante re on re.tb_representante_id = cre.tb_representante_id 
                            INNER JOIN tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id
                    WHERE 
                    tb_credito_tip = 4 and tb_credito_xac = 1 and cuo.tb_creditotipo_id = :creditotipo_id AND YEAR(tb_cuota_fec) =:anio and MONTH(tb_cuota_fec) =:mes and tb_cuota_est in(1,3) AND tb_credito_est in (3,4)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
            $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
            $sentencia->bindParam(":creditotipo_id", $cre_tip, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function ranking_pagadores($tabla) {
        try {
            $tipo = 'tb_credito_tip1';
            if ($tabla != 'tb_creditoasiveh')
                $tipo = 'tb_credito_tip';
            $sql = "SELECT tb_credito_id, tb_cliente_nom, CONCAT(tb_cliente_tel,' | ', tb_cliente_cel, ' | ', tb_cliente_telref) as contactos, tb_credito_feccre, tb_credito_fecliq, tb_credito_preaco, tb_credito_numcuo, tb_credito_numcuomax  FROM $tabla cre
			INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id
			WHERE tb_credito_xac = 1 AND tb_credito_est = 7 AND $tipo in (1,4) and tb_credito_preaco > 1 order by 1 DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "Datos no encontrados en el Sistema";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}
