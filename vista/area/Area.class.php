<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Area extends Conexion{

    function insertar($area_nom, $area_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_area(tb_area_xac, tb_area_nom, tb_area_des)
          VALUES (1, :area_nom, :area_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":area_nom", $area_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":area_des", $area_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($area_id, $area_nom, $area_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_area SET tb_area_nom =:area_nom, tb_area_des =:area_des WHERE tb_area_id =:area_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":area_id", $area_id, PDO::PARAM_INT);
        $sentencia->bindParam(":area_nom", $area_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":area_des", $area_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($area_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_area WHERE tb_area_id =:area_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":area_id", $area_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($area_id){
      try {
        $sql = "SELECT * FROM tb_area WHERE tb_area_id =:area_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":area_id", $area_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_areas(){
      try {
        $sql = "SELECT * FROM tb_area";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
