<?php
require_once('Area.class.php');
$oArea = new Area();

$area_id = (empty($area_id))? 0 : $area_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$option = '<option value="0"></option>';

//PRIMER NIVEL
$result = $oArea->listar_areas();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($area_id == $value['tb_area_id'])
	  		$selected = 'selected';

	    $option .= '<option value="'.$value['tb_area_id'].'" style="font-weight: bold;" '.$selected.'>'.$value['tb_area_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $option;
?>