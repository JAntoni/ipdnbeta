
$(document).ready(function(){
  $('#form_area').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"area/area_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_area").serialize(),
				beforeSend: function() {
					$('#area_mensaje').show(400);
					$('#btn_guardar_area').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#area_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#area_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		area_tabla();
		      		$('#modal_registro_area').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#area_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#area_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_area').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#area_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#area_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_area_nom: {
				required: true,
				minlength: 2
			},
			txt_area_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_area_nom: {
				required: "Ingrese un nombre para el Area",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_area_des: {
				required: "Ingrese una descripción para el Area",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
