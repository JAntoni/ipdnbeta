<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Ventagarantia extends Conexion{

    function insertar($ventagarantia_nom, $ventagarantia_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_ventagarantia(tb_ventagarantia_xac, tb_ventagarantia_nom, tb_ventagarantia_des)
          VALUES (1, :ventagarantia_nom, :ventagarantia_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ventagarantia_nom", $ventagarantia_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":ventagarantia_des", $ventagarantia_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($ventagarantia_id, $ventagarantia_nom, $ventagarantia_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_ventagarantia SET tb_ventagarantia_nom =:ventagarantia_nom, tb_ventagarantia_des =:ventagarantia_des WHERE tb_ventagarantia_id =:ventagarantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ventagarantia_id", $ventagarantia_id, PDO::PARAM_INT);
        $sentencia->bindParam(":ventagarantia_nom", $ventagarantia_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":ventagarantia_des", $ventagarantia_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($ventagarantia_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_ventagarantia WHERE tb_ventagarantia_id =:ventagarantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ventagarantia_id", $ventagarantia_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($ventagarantia_id){
      try {
        $sql = "SELECT * FROM tb_ventagarantia WHERE tb_ventagarantia_id =:ventagarantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ventagarantia_id", $ventagarantia_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    //listamos las ventas de la garantías hechas en una fecha a un cliente particular, luego juntamos con las ventas del colaborador y jalamos el precio que debe pagar
    function listar_ventagarantias_fecha($ventagarantia_fec1, $ventagarantia_fec2){
      try {
        $sql = "
          SELECT tb_ventagarantia_id,tb_garantia_id,tb_ventagarantia_prec2 as venta_prec FROM tb_ventagarantia where tb_ventagarantia_xac = 1 and (tb_ventagarantia_fec between :ventagarantia_fec1 and :ventagarantia_fec2) and tb_cliente_id > 0
          UNION
          SELECT tb_ventainterna_id, tb_garantia_id,tb_ventainterna_mon as venta_prec FROM tb_ventainterna where  tb_ventainterna_xac = 1 and (tb_ventainterna_fecpag between :ventagarantia_fec1 and :ventagarantia_fec2)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":ventagarantia_fec1", $ventagarantia_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":ventagarantia_fec2", $ventagarantia_fec2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
