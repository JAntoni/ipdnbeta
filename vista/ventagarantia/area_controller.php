<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../area/Area.class.php');
  $oArea = new Area();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$area_nom = mb_strtoupper($_POST['txt_area_nom'], 'UTF-8');
 		$area_des = $_POST['txt_area_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Area.';
 		if($oArea->insertar($area_nom, $area_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Area registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$area_id = intval($_POST['hdd_area_id']);
 		$area_nom = mb_strtoupper($_POST['txt_area_nom'], 'UTF-8');
 		$area_des = $_POST['txt_area_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Area.';

 		if($oArea->modificar($area_id, $area_nom, $area_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Area modificado correctamente. '.$area_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$area_id = intval($_POST['hdd_area_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Area.';

 		if($oArea->eliminar($area_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Area eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>