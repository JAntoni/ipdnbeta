<?php
require_once('../../core/usuario_sesion.php');
require_once ("Creditogarvehfile.class.php");
$oCreditogarvehfile = new Creditogarvehfile();

?>
<table class="table table-bordered table-hover" cellspacing="1" id="tabla_catalogo_imagen">
    <thead>
        <tr>
            <th></th>              
        </tr>
    </thead>
<?php
?>  
    <tbody>
	
        <tr class="even">
            <?php
            $dts=$oCreditogarvehfile->filtrar($_POST['cre_id']);
            if($dts['estado']==1){            
            foreach ($dts['data'] as $key => $dt) {
                
                $servidor = $dt['tb_creditogarvehfile_url'];
            ?>
            <td align="center">
                <a class="group1"><img data-u="image" src="<?php echo $servidor;?>" alt="" width="80" height="70" onclick="carousel(<?php echo $_POST['cre_id']; ?>)"></a>
                <br>
                <br>
                <a class="btn btn-primary btn-sm" onClick="credito_file_eliminar(<?php echo $dt['tb_creditogarvehfile_id']?>)">Eliminar</a>
            </td>
            <?php
            }
            $dts=null;
            ?>
        </tr>

    </tbody>
	<?php
}
?>
</table>