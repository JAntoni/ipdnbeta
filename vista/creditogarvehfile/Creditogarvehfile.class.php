<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Creditogarvehfile extends Conexion {
    
    public $creditogarvehfile_xac = 1;
    public $credito_id;
    public $creditogarvehfile_url= '...';
    public $creditogarvehfile_his;
    public $creditogarvehfile_id;
    
    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_creditogarvehfile(tb_creditogarvehfile_xac, tb_credito_id, tb_creditogarvehfile_url, tb_creditogarvehfile_his)
                    VALUES (:creditogarvehfile_xac, :credito_id, :creditogarvehfile_url, :creditogarvehfile_his)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditogarvehfile_xac", $this->creditogarvehfile_xac, PDO::PARAM_INT);
            $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":creditogarvehfile_url", $this->creditogarvehfile_url, PDO::PARAM_STR);
            $sentencia->bindParam(":creditogarvehfile_his", $this->creditogarvehfile_his, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->creditogarvehfile_id = $this->dblink->lastInsertId();
            $this->dblink->commit();
            
            $nombre_imagen = 'creditogarveh_'.$this->credito_id.'_creditogarvehfile_'. $this->creditogarvehfile_id .'.'. $this->fileParts['extension'];
            $creditogarvehfile_url = $this->directorio . strtolower($nombre_imagen);
            
            if(move_uploaded_file($this->tempFile, '../../'. $creditogarvehfile_url)){
                //insertamos las imágenes
                $sql = "UPDATE tb_creditogarvehfile SET tb_creditogarvehfile_url =:creditogarvehfile_url WHERE tb_creditogarvehfile_id  =:creditogarvehfile_id";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":creditogarvehfile_id", $this->creditogarvehfile_id, PDO::PARAM_INT);
                $sentencia->bindParam(":creditogarvehfile_url", $creditogarvehfile_url, PDO::PARAM_STR);
                $result2 = $sentencia->execute();
              }
              else{
                $this->eliminar($this->creditogarvehfile_id);
              }

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_url() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_creditogarvehfile SET 
                                            tb_creditogarvehfile_url =:creditogarvehfile_url, 
                                            tb_creditogarvehfile_his= CONCAT(tb_creditogarvehfile_his,:creditogarvehfile_his)
                                    WHERE 
                                            tb_creditogarvehfile_id =:creditogarvehfile_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_creditogarvehfile_url", $this->tb_creditogarvehfile_url, PDO::PARAM_STR);
            $sentencia->bindParam(":creditogarvehfile_his", $this->creditogarvehfile_his, PDO::PARAM_STR);
            $sentencia->bindParam(":creditogarvehfile_id", $this->creditogarvehfile_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar($creditogarvehfile_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_creditogarvehfile SET tb_creditogarvehfile_xac=0 WHERE tb_creditogarvehfile_id =:creditogarvehfile_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditogarvehfile_id", $creditogarvehfile_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($creditogarvehfile_id) {
        try {
            $sql = "SELECT * FROM tb_creditogarvehfile WHERE tb_creditogarvehfile_id =:creditogarvehfile_id AND tb_creditogarvehfile_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditogarvehfile_id", $creditogarvehfile_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function filtrar($credito_id) {
        try {
            $sql = "SELECT * FROM tb_creditogarvehfile WHERE tb_credito_id =:tb_credito_id AND tb_creditogarvehfile_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar() {
        try {
            $sql = "SELECT * FROM tb_creditogarvehfile WHERE tb_creditogarvehfile_xac=1 ORDER BY tb_creditogarvehfile_id DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

   

}

?>
