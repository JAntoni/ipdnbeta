<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit)); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<button class="btn btn-primary btn-sm" onclick="alerta_form('I', 0)"><i class="fa fa-plus"></i> Nueva Alerta</button>
			</div>
			<div class="box-body">
				<!--- MESAJES DE CARGANDO -->
				<div class="callout callout-info" id="alerta_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<input type="hidden" id="hdd_datatable_alerta_fil">
						<div id="div_alerta_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
						</div>
					</div>
				</div>
			</div>
			<div id="div_modal_alerta_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once VISTA_URL . 'templates/modal_mensaje.php'; ?>
		</div>
	</section>
	<!-- /.content -->
</div>