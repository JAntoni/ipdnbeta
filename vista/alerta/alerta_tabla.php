<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/fechas.php';
require_once 'Alerta.class.php';
$oAlerta = new Alerta();

$result = $oAlerta->listar_todos('', '', '', '', '', '', '', '', '', 'ASC');

$tr = '';
if ($result['estado'] == 1) {
    $estado_nom = array('No leída', 'Leída');
    foreach ($result['data'] as $key => $value) {
        $fecha_lect = '';
        $color = "#FFCFCF";
        $prioridad_nom = 'ALTA';
        if ($value['tb_alerta_prioridad'] == 2) {
            $color = "#FDFDBC";
            $prioridad_nom = 'MEDIA';
        }
        elseif ($value['tb_alerta_prioridad'] == 3) {
            $color = "#CCFFEC";
            $prioridad_nom = 'BAJA';
        }
        if (!empty($value['tb_alerta_fecha_lectura'])) {
            $fecha_lect = ' (el '.mostrar_fecha_hora($value['tb_alerta_fecha_lectura']).')';
        }


        $tr .= '<tr>';
        $tr .= '
          <td id="tabla_fila">' . $value['tb_alerta_id'] . '</td>
          <td id="tabla_fila">' . $value['tb_alerta_titulo'] . '</td>
          <td id="tabla_fila">' . $value['usuario_nom'] . '</td>
          <td id="tabla_fila" style="background-color: '.$color.';">' . $prioridad_nom . '</td>
          <td id="tabla_fila">' . $estado_nom[$value['tb_alerta_estado']] . $fecha_lect . '</td>
          <td id="tabla_fila">' . mostrar_fecha_hora($value['tb_alerta_fecreg']) . '</td>
          <td id="tabla_fila">' . $value['usuario_reg_nom'] . '</td>
          <td id="tabla_fila" align="center">
              <a class="btn btn-info btn-xs" title="Ver" onclick="alerta_form(\'L\',' . $value['tb_alerta_id'] . ')"><i class="fa fa-eye"></i> Ver</a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="alerta_form(\'M\',' . $value['tb_alerta_id'] . ')"><i class="fa fa-edit"></i> Editar</a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="alerta_form(\'E\',' . $value['tb_alerta_id'] . ')"><i class="fa fa-trash"></i> Eliminar</a>
          </td>
        ';
        $tr .= '</tr>';
    }
    $result = null;
} else {
    echo $result['mensaje'];
    $result = null;
    exit();
}

?>
<table id="tbl_alertas" class="table table-hover" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">Titulo</th>
            <th id="tabla_cabecera_fila">Destinatario</th>
            <th id="tabla_cabecera_fila">Prioridad</th>
            <th id="tabla_cabecera_fila">Estado</th>
            <th id="tabla_cabecera_fila">Fec. Reg</th>
            <th id="tabla_cabecera_fila">Usu. Reg</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>