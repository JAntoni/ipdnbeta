
$(document).ready(function(){
	$('#txt_alerta_fecha_mostrar').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        startDate: new Date()
    });

	$('#cbo_alerta_destinatario').change(function(){
		var destinatario = $('#cbo_alerta_destinatario option:selected').val();
		if (parseInt(destinatario) == 0 || destinatario === null) {
			limpiar_cbo_destinatarioid();
		} else if (parseInt(destinatario) == 5) {
			disabled($('#cbo_alerta_destinatario_id').html(''));
		} else {
			$('#cbo_alerta_destinatario_id').removeClass('disabled').removeAttr('disabled');
			completar_cbo_destinatario_id(parseInt(destinatario));
		}

		if (parseInt(destinatario) != 5) { //5 valor para todos los usuarios activos
			//mas info en https://jqueryvalidation.org/rules/
			//jQuery.validator.format("Please, at least {0} characters are necessary")
			$("#cbo_alerta_destinatario_id").rules("add", {
				min: 1,
				messages: {
					min: jQuery.validator.format("Obligatorio*")
				}
			});
		} else {
			$("#cbo_alerta_destinatario_id").rules("remove", "min");
		}
	});

	disabled($("#form_alerta").find(".disabled"));

	$('#form_alerta').validate({
		submitHandler: function () {
			var destino_nom = '&destino_nom='+$('#cbo_alerta_destinatario option:selected').text();
			var destinatario_nom = '&destinatario_nom='+$('#cbo_alerta_destinatario_id option:selected').text();
			var datos_extra = destino_nom+destinatario_nom;
			$.ajax({
				type: "POST",
				url: VISTA_URL + "alerta/alerta_controller.php",
				async: true,
				dataType: "json",
				data: serialize_form('form_alerta', datos_extra),
				beforeSend: function () {
					$('#alerta_mensaje').show(400);
					$('#btn_guardar_alerta').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						disabled($('#form_alerta').find("button[class*='btn-sm'], select, input, textarea"));
						$('#alerta_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#alerta_mensaje').html(`<h4>${data.mensaje}</h4>`);

						setTimeout(function () {
							$('#modal_alerta_form').modal('hide');
							if (data.estado == 1) {
								alerta_tabla();
							}
						}, 2500);
					}
					else {
						$('#alerta_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#alerta_mensaje').html(`<h4>Alerta: ${data.mensaje}</h4>`);
						$('#btn_guardar_alerta').prop('disabled', false);
					}
				},
				error: function (data) {
					$('#alerta_mensaje').removeClass('callout-info').addClass('callout-danger');
					$('#alerta_mensaje').html(`<h4>ALERTA DE ERROR: ${data.responseText}</h4>`);
				}
			});
		},
		rules: {
			txt_alerta_fecha_mostrar: {
				required: true
			},
			txt_alerta_modulo_nom: {
				required: true
			},
			txt_alerta_titulo: {
				required: true
			},
			cbo_alerta_destinatario: {
				min: 1
			},
			cbo_alerta_prioridad: {
				min: 1
			},
		},
		messages: {
			txt_alerta_fecha_mostrar: {
				required: "Obligatorio*"
			},
			txt_alerta_modulo_nom: {
				required: "Obligatorio*"
			},
			txt_alerta_titulo: {
				required: "Obligatorio*"
			},
			cbo_alerta_destinatario: {
				min: "Obligatorio*"
			},
			cbo_alerta_prioridad: {
				min: "Obligatorio*"
			},
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else if (element.parent("div.input-group").length == 1) {
                error.insertAfter(element.parent("div.input-group")[0]);
            } else {
                error.insertAfter(element);
            }
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	//mensaje antes de eliminar
    if ($.inArray($('#action_alerta').val(), ['leer', 'eliminar']) > -1) {
		disabled($('#form_alerta').find("button[class*='btn-sm'], select, input, textarea, a").not('.disabled'));
        if ($('#action_alerta').val() == 'eliminar') {
            $('#alerta_mensaje').removeClass('callout-info').addClass('callout-warning');
            var mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este registro?</h4>';
            $('#alerta_mensaje').html(mens);
            $('#alerta_mensaje').show();
        }
    }
});


function completar_cbo_destinatario_id(destinatario) {
	$.ajax({
		type: 'POST',
		url: VISTA_URL + 'alerta/alerta_controller.php',
		async: true,
		dataType: 'json',
		data: ({
			action_alerta: 'completar_cbo_destinatario_id',
			valor: destinatario
		}),
		success: function (data) {
			if (data.estado == 1) {
				$('#cbo_alerta_destinatario_id').html(data.html);
			} else {
				alerta_error("SISTEMA", data.mensaje);
				limpiar_cbo_destinatarioid();
			}
		}
	});
}

function limpiar_cbo_destinatarioid(){
	$('#cbo_alerta_destinatario_id').html('');
	$('#cbo_alerta_destinatario_id').removeClass('disabled').removeAttr('disabled');
}

function serialize_form(id_form, extra) {
    var elementos_disabled = $(`#${id_form}`).find('input:disabled, select:disabled, textarea:disabled').removeAttr('disabled');
    var form_serializado = $(`#${id_form}`).serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}

/* $.ajax({
    type: 'POST',
    url: VISTA_URL + 'ejecucion/ejecucion_controller.php',
    async: true,
    dataType: 'json',
    data: ({
        
    }),
    success: function (data) {
        if (data.estado == 1) {
            alerta_success("SISTEMA", data.mensaje);
        } else {
            alerta_error("SISTEMA", data.mensaje);
        }
    }
}); */