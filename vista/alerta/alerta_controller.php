<?php
require_once '../../core/usuario_sesion.php';
require_once '../funciones/fechas.php';
require_once '../usuario/Usuario.class.php';
$oUsuario = new Usuario();
require_once '../alerta/Alerta.class.php';
$oAlerta = new Alerta();

$action = $_POST['action_alerta'];

$data['estado'] = 0;
$data['mensaje'] = "Error al $action";
$usuario_reg_id = $_SESSION['usuario_id'];
$alerta_id = $_POST['hdd_alerta_id'];
$alerta_reg_origin = (array) json_decode($_POST['hdd_alerta_reg']);

$fecha_mostrar	= $_POST['txt_alerta_fecha_mostrar'];
$modulo_nombre	= $_POST['txt_alerta_modulo_nom'];
$modulo_id		= $_POST['txt_alerta_modulo_id'];
$titulo_alerta	= $_POST['txt_alerta_titulo'];
$mensaje1		= $_POST['txt_alerta_mens1'];
$mensaje2		= $_POST['txt_alerta_mens2'];
$prioridad		= $_POST['cbo_alerta_prioridad'];
$comentario		= $_POST['txt_alerta_coment'];

$oAlerta->fecha_mostrar = fecha_mysql($fecha_mostrar);
$oAlerta->modulo_nom = $modulo_nombre;
$oAlerta->modulo_id = empty(trim($modulo_id)) ? null : $modulo_id;
$oAlerta->titulo = strtoupper(trim($titulo_alerta));
$oAlerta->mens1 = empty(trim($mensaje1)) ? null : trim($mensaje1);
$oAlerta->mens2 = empty(trim($mensaje2)) ? null : trim($mensaje2);
$oAlerta->prioridad = $prioridad;
$oAlerta->coment = empty($comentario) ? null : strtoupper(trim($comentario));

if ($action == 'completar_cbo_destinatario_id') {
	$destino = $_POST['valor'];
	
	switch ($destino) { //la noti irá a
		case 1:
			//usuario específico
			ob_start();
			require_once '../usuario/usuario_select.php';
			$data['estado'] = 1;
			break;
		case 2:
			//grupo de usuarios
			ob_start();
			require_once '../usuariogrupo/usuariogrupo_select.php';
			$data['estado'] = 1;
			break;
		case 3:
			//un perfil
			ob_start();
			require_once '../usuarioperfil/usuarioperfil_select.php';
			$data['estado'] = 1;
			break;
		case 4:
			//un cargo
			ob_start();
			require_once '../cargo/cargo_select.php';
			$data['mensaje'] = 'Stand-by para listar usuarios por cargo';
			break;
		default:
			$data['mensaje'] .= '. Bandera switch';
			break;
	}
	$data['html'] = ob_get_clean();
}
elseif ($action == 'insertar') {
	$destino		= $_POST['cbo_alerta_destinatario'];
	if ($destino == 4) {
		$data['mensaje'] = "No se puede $action. Stand-by para listar usuarios por cargo";
		echo json_encode($data); exit();
	}
	$destinatario_id = $_POST['cbo_alerta_destinatario_id'];
	$oAlerta->usureg = $usuario_reg_id;
	$oAlerta->correlativo = $oAlerta->nuevo_correlativo()['data']['new_correlativo'];

	//obtener id de los usuarios a quienes se les va a hacer el registro
		//destino es un usuario específico
		if ($destino < 2) {
			$oAlerta->usuario_id = $destinatario_id;
		}
		//se envia a un grupo
		elseif ($destino < 5) {
			switch ($destino) {
				case 2:
					$lista_users = $oUsuario->listar_usuarios_campo('usu.tb_usuariogrupo_id', $destinatario_id, 'INT', 0);//grupo usuarios
					break;
				case 3:
					$lista_users = $oUsuario->listar_usuarios_campo('usu.tb_usuarioperfil_id', $destinatario_id, 'INT', 0);//perfil
					break;
				case 4:
					//$lista_users = LISTAR USUARIOS POR CARGO;//cargo
					break;
			}
		}
		//se envía a todos los usuarios activos
		elseif ($destino == 5) {
			$lista_users = $oUsuario->listar_usuarios();
		}
	//

	if ($destino < 2) {
		$res_insert = $oAlerta->insertar();
		if ($res_insert['estado'] == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = "Se realizó con éxito";
		} else {
			$data['mensaje'] = "No se realizó con éxito";
		}
	} else {
		if ($lista_users['estado'] == 1) {
			$i = 0;
			foreach ($lista_users['data'] as $key => $usuario) {
				$oAlerta->usuario_id = $arr[$i]['usuario_id'] = $usuario['tb_usuario_id'];
				$arr[$i]['res_insert'] = $oAlerta->insertar()['estado'];
				$i++;
			}
			$todo_ok = true;
			$i = 0;
			while ($i < count($arr)) {
				if ($arr[$i]['res_insert'] != 1) {
					$todo_ok = false;
				}
				$i++;
			}

			if ($todo_ok) {
				$data['estado'] = 1;
				$data['mensaje'] = "Se realizó con éxito";
			} else {
				$data['mensaje'] = "❌ Hubo error al insertar con el usuario id: ";
				foreach ($arr as $key => $insercion) {
					if ($insercion['res_insert'] != 1) {
						$data['mensaje'] .= $insercion['usuario_id'].', ';
					}
				}
				$data['mensaje'] = substr($data['mensaje'], 0, -2)."<br>";

				$data['mensaje'] .= "✔ Se insertó correctamente con el usuario id: ";
				foreach ($arr as $key => $insercion) {
					if ($insercion['res_insert'] == 1) {
						$data['mensaje'] .= $insercion['usuario_id'].', ';
					}
				}
				$data['mensaje'] = substr($data['mensaje'], 0, -2);
			}
		} else {
			$data['mensaje'] = "No existen usuarios activos con {$_POST['destino_nom']} {$_POST['destinatario_nom']}";
			echo json_encode($data); exit();
		}
	}
}
elseif ($action == 'modificar') {
	$igual[0] = boolval($oAlerta->fecha_mostrar	== fecha_mysql($alerta_reg_origin['tb_alerta_fecha_mostrar']));
	$igual[1] = boolval($oAlerta->modulo_nom	== $alerta_reg_origin['tb_alerta_modulo_nom']);
	$igual[2] = boolval($oAlerta->modulo_id		== $alerta_reg_origin['tb_alerta_modulo_id']);
	$igual[3] = boolval($oAlerta->titulo		== $alerta_reg_origin['tb_alerta_titulo']);
	$igual[4] = boolval($oAlerta->mens1			== $alerta_reg_origin['tb_alerta_mens1']);
	$igual[5] = boolval($oAlerta->mens2			== $alerta_reg_origin['tb_alerta_mens2']);
	$igual[6] = boolval($oAlerta->prioridad		== $alerta_reg_origin['tb_alerta_prioridad']);
	$igual[7] = boolval($oAlerta->coment		== $alerta_reg_origin['tb_alerta_coment']);

	$son_iguales = true;
	$i = 0;
	while ($i < count($igual)) {
		if (!$igual[$i]) {
			$son_iguales = false;
		}
		$i++;
	}
	$i = 0;

	if ($son_iguales) {
		$data['estado'] = 2;
		$data['mensaje'] = "● No se realizó ninguna modificacion";
	}
	else {
		$oAlerta->id = $alerta_id;

		$res_modificar = $oAlerta->modificar();
		if ($res_modificar == 1) {
			$data['mensaje'] = '✔ Alerta modificada correctamente';
			$data['estado'] = 1;
		}
		unset($res_modificar);
	}
}
elseif ($action == 'cambiar_estado') {
	$estado_lectura = $_POST['estado']; //puede ser 1: leido, 0: no leida
	
	$res_mod_campo = $oAlerta->modificar_campo($alerta_id, 'tb_alerta_estado', $estado_lectura, 'INT');

	if ($res_mod_campo == 1) {
		$data['estado'] = 1;
		$data['mensaje'] = 'Éxito en la operación';

		if ($estado_lectura == 1) {
			$oAlerta->modificar_campo($alerta_id, 'tb_alerta_fecha_lectura', date('Y-m-d H:m:s'), 'INT');
		}
	}
}
elseif ($action == 'buscar_alerta') {
	$modulo_nom = $_POST['modulo_nom'];
	$modulo_id = $_POST['modulo_id'];

	$data['html'] = '';

	$res_busq = $oAlerta->listar_todos('', $modulo_nom, '', $modulo_id, '', '', '', '', 0, 'ASC');
	if ($res_busq['estado'] == 1) {
		$data['estado'] = 1;
		$data['mensaje'] = "éxito";
		$data['html'] = '<label style="font-size: 20px;">Alertas</label>';
		foreach ($res_busq['data'] as $key => $alerta) {
			$data['html'] .= '<br> ● <b>'.$alerta['tb_alerta_titulo'].'. Mensaje: '.$alerta['tb_alerta_mens1'] .' |</b> <button class="btn btn-xs bg-olive" onclick="cambiar_estado_alerta('.$alerta['tb_alerta_id'].', 1)">✔ ALERTA LEIDA</button>';
		}
	}
}
elseif ($action == 'eliminar') {
	if ($oAlerta->eliminar($alerta_id)) {
		$data['estado'] = 1;
		$data['mensaje'] = 'Alerta eliminada correctamente.';
	}
}

echo json_encode($data);
