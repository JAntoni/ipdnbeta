<?php
	require_once 'core/usuario_sesion.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php include_once VISTA_URL.'templates/head.php'; ?>
		<title><?php echo ucwords(mb_strtolower($menu_tit));?></title>
	</head>
	<body <?php echo 'class="'.CLASE_BODY.' '.$usuario_tem.'"'; ?>>
		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php include_once VISTA_URL.'templates/header.php'; ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php include_once VISTA_URL.'templates/aside.php'; ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
				<?php include_once 'alerta_vista.php'; ?>
			<!-- INCLUIR FOOTER-->
				<?php include_once VISTA_URL.'templates/footer.php'; ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php include_once VISTA_URL.'templates/script.php'; ?>

		<script type="text/javascript" src="<?php echo VISTA_URL.'alerta/alerta.js';?>"></script>
	</body>
</html>
