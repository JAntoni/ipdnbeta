<?php
//revision de sesion activa
    require_once '../../core/usuario_sesion.php';
//
//requires
    require_once '../funciones/fechas.php';
    require_once '../funciones/funciones.php';
    require_once '../alerta/Alerta.class.php';
    $oAlerta = new Alerta();
//

//datos post
$usuario_action = $_POST['action'];
$alerta_id     = intval($_POST['alerta_id']);

//datos estaticos
$listo          = 1;
$action         = devuelve_nombre_usuario_action($usuario_action);

$titulo = '';
//titulo del form
if ($usuario_action == 'I') {
    $titulo = 'Registrar alerta';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar alerta';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar alerta';
} elseif ($usuario_action == 'L') {
    $titulo = 'Alerta registrada';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
if ($usuario_action != 'I') {
    $titulo .= ": ID $alerta_id";
}
//

if ($listo == 1) {
    $style_destin = '';
    if (!empty($alerta_id)) {
        $style_destin = 'display: none';
        $res = $oAlerta->mostrar_uno_id($alerta_id);

        if ($res['estado'] == 1) {
            $alerta = $res['data'];
            $titulo .= ". Usuario destino: {$alerta['usuario_nom']}";
            $fecha_mostrar = mostrar_fecha($alerta['tb_alerta_fecha_mostrar']);
            $modulo_nombre = $alerta['tb_alerta_modulo_nom'];
            $modulo_id = $alerta['tb_alerta_modulo_id'];
            $titulo_alerta = $alerta['tb_alerta_titulo'];
            $mensaje1 = $alerta['tb_alerta_mens1'];
            $mensaje2 = $alerta['tb_alerta_mens2'];
            $prioridad = $alerta['tb_alerta_prioridad'];
            $comentario = $alerta['tb_alerta_coment'];
        }
        unset($res);
    }
} else {
    $mensaje = 'EN DESARROLLO. PRONTO ACTIVO';
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_alerta_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4><?php echo $mensaje; ?></h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo"; ?></h4>
                </div>
                <form id="form_alerta" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action_alerta" id="action_alerta" value="<?php echo $action; ?>">
                        <input type="hidden" name="hdd_alerta_id" id="hdd_alerta_id" value="<?php echo $alerta_id; ?>">
                        <input type="hidden" name="hdd_alerta_reg" id="hdd_alerta_reg" value='<?php echo json_encode($alerta); ?>'>

                        <div class="box box-primary shadow">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="txt_alerta_fecha_mostrar">Fecha desde cuando se mostrá:</label>
                                    <div class='input-group date'>
                                        <input type="text" name="txt_alerta_fecha_mostrar" id="txt_alerta_fecha_mostrar" class="form-control input-sm input-shadow mayus" placeholder="Fec. Mostrar" value="<?php echo $fecha_mostrar; ?>">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="txt_alerta_modulo_nom">Nombre del módulo:</label>
                                    <input type="text" name="txt_alerta_modulo_nom" id="txt_alerta_modulo_nom" placeholder="Nombre del módulo" class="form-control input-sm input-shadow" style="text-transform: lowercase;" value="<?php echo $modulo_nombre; ?>">
                                </div>
                                <div class="col-md-4">
                                    <label for="txt_alerta_modulo_id">ID del módulo (opcional):</label>
                                    <input type="number" name="txt_alerta_modulo_id" id="txt_alerta_modulo_id" placeholder="ID del módulo" class="form-control input-sm input-shadow" value="<?php echo $modulo_id; ?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_alerta_titulo">Título de la alerta:</label>
                                    <input type="text" name="txt_alerta_titulo" id="txt_alerta_titulo" placeholder="Título de la alerta" class="form-control mayus input-sm input-shadow" value="<?php echo $titulo_alerta; ?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_alerta_mens1">Mensaje 1 (opcional):</label>
                                    <textarea name="txt_alerta_mens1" id="txt_alerta_mens1" placeholder="Mensaje 1" class="form-control input-sm input-shadow"><?php echo $mensaje1; ?></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_alerta_mens2">Mensaje 2 (opcional):</label>
                                    <textarea name="txt_alerta_mens2" id="txt_alerta_mens2" placeholder="Mensaje 2" class="form-control input-sm input-shadow"><?php echo $mensaje2; ?></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4" style="<?php echo $style_destin;?>">
                                    <label for="cbo_alerta_destinatario">Destino:</label>
                                    <select name="cbo_alerta_destinatario" id="cbo_alerta_destinatario" class="form-control input-sm input-shadow mayus">
                                        <option value="0" style="font-weight: bold;">SELECCIONE</option>
                                        <option value="1" <?php echo $destinatario == '1' ? 'selected' : ''; ?> style="font-weight: bold;">USUARIO</option>
                                        <option value="2" <?php echo $destinatario == '2' ? 'selected' : ''; ?> style="font-weight: bold;">GRUPO DE USUARIOS</option>
                                        <option value="3" <?php echo $destinatario == '3' ? 'selected' : ''; ?> style="font-weight: bold;">PERFIL</option>
                                        <option value="4" <?php echo $destinatario == '4' ? 'selected' : ''; ?> style="font-weight: bold; display: none;">CARGO</option>
                                        <option value="5" <?php echo $destinatario == '5' ? 'selected' : ''; ?> style="font-weight: bold;">TODOS LOS USUARIOS</option>
                                    </select>
                                </div>
                                <div class="col-md-4" style="<?php echo $style_destin;?>">
                                    <label for="cbo_alerta_destinatario_id">Destinatario:</label>
                                    <select name="cbo_alerta_destinatario_id" id="cbo_alerta_destinatario_id" class="form-control mayus input-sm input-shadow">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="cbo_alerta_prioridad">Prioridad:</label>
                                    <select name="cbo_alerta_prioridad" id="cbo_alerta_prioridad" class="form-control input-sm input-shadow mayus">
                                        <option value="0" style="font-weight: bold;">SELECCIONE</option>
                                        <option value="1" <?php echo $prioridad == '1' ? 'selected' : ''; ?> style="font-weight: bold;">ALTA</option>
                                        <option value="2" <?php echo $prioridad == '2' ? 'selected' : ''; ?> style="font-weight: bold;">MEDIA</option>
                                        <option value="3" <?php echo $prioridad == '3' ? 'selected' : ''; ?> style="font-weight: bold;">BAJA</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_alerta_coment">Comentario (opcional):</label>
                                    <textarea name="txt_alerta_coment" id="txt_alerta_coment" placeholder="Comentario" class="form-control mayus input-sm input-shadow"><?php echo $comentario; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="alerta_mensaje" style="display: none;">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_alerta">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_alerta">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/alerta/alerta_form.js?ver=0000001'; ?>"></script>
