var datatable_alerta_global;
$(document).ready(function () {
    alerta_tabla();
});

function alerta_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "alerta/alerta_tabla.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#alerta_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_alerta_tabla').html(data);
            $('#alerta_mensaje_tbl').hide(300);
            estilos_datatable_alerta();
        },
        complete: function (data) {
        },
        error: function (data) {
            $('#alerta_mensaje_tbl').html('ERROR AL CARGAR DATOS: ' + data.responseText);
        }
    });
}

function estilos_datatable_alerta() {
    datatable_alerta_global = $('#tbl_alertas').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsqueda",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:"
        },
        order: [],
        columnDefs: [
            {targets: [7], orderable: false}
        ]
    });
    datatable_alerta_texto_filtrar();
}

function datatable_alerta_texto_filtrar() {
    $('input[aria-controls*="tbl_alertas"]')
    .attr('id', 'txt_datatable_alerta_fil')
    .attr('placeholder','escriba para buscar');

    $('#txt_datatable_alerta_fil').keyup(function (event) {
        $('#hdd_datatable_alerta_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_alerta_fil').val();
    if (text_fil) {
        $('#txt_datatable_alerta_fil').val(text_fil);
        datatable_alerta_global.search(text_fil).draw();
    }
}

function alerta_form(action, alerta_id){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "alerta/alerta_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            alerta_id: alerta_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_modal_alerta_form').html(html);
            $('#modal_alerta_form').modal('show');
            $('#modal_mensaje').modal('hide');

            modal_width_auto('modal_alerta_form', 40);
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('modal_alerta_form'); //funcion encontrada en public/js/generales.js
        }
    });
}

function alerta_historial_form(alerta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "alerta/alerta_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
            alerta_id: alerta_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_alerta_historial_form').html(html);
            $('#div_modal_alerta_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_alerta_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_alerta_historial_form'); //funcion encontrada en public/js/generales.js
        },
        complete: function (html) {
        }
    });
}
