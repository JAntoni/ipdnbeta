<?php
require_once '../../datos/conexion.php';

class Alerta extends Conexion {
    public $id;
    public $modulo_nom;
    public $modulo_id;
    public $titulo;
    public $mens1;
    public $mens2;
    public $usuario_id;
    public $prioridad;
    public $coment;
    public $usureg;
    public $fecreg;
    public $estado;
    public $fecha_mostrar;
    public $fecha_lectura;
    public $correlativo;
    
    public function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_alerta (
                        tb_alerta_modulo_nom,
                        tb_alerta_modulo_id,
                        tb_alerta_titulo,
                        tb_alerta_mens1,
                        tb_alerta_mens2,
                        tb_usuario_id,
                        tb_alerta_prioridad,
                        tb_alerta_coment,
                        tb_alerta_usureg,
                        tb_alerta_fecha_mostrar,
                        tb_alerta_correlativo)
                    VALUES (
                        :param0,
                        :param1,
                        :param2,
                        :param3,
                        :param4,
                        :param5,
                        :param6,
                        :param7,
                        :param8,
                        :param9,
                        :param10)";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->modulo_nom, PDO::PARAM_STR);
            $sentencia->bindParam(':param1', $this->modulo_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->titulo, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->mens1, PDO::PARAM_STR);
            $sentencia->bindParam(':param4', $this->mens2, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->usuario_id, PDO::PARAM_STR);
            $sentencia->bindParam(':param6', $this->prioridad, PDO::PARAM_INT);
            $sentencia->bindParam(':param7', $this->coment, PDO::PARAM_STR);
            $sentencia->bindParam(':param8', $this->usureg, PDO::PARAM_INT);
            $sentencia->bindParam(':param9', $this->fecha_mostrar, PDO::PARAM_STR);
            $sentencia->bindParam(':param10', $this->correlativo, PDO::PARAM_INT);

            $resultado['sql'] = $sql;
            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function listar_todos($alerta_id, $modulo_nom, $prioridad, $modulo_id, $titulo, $mens1, $mens2, $usuario_id, $estado, $orden) {
        $where_opt = '';
        if (!empty($alerta_id)) {
            $where_opt .= empty($where_opt) ? 'al.tb_alerta_id = :param_opc0' : ' AND al.tb_alerta_id = :param_opc0';
        }
        if (!empty($modulo_nom)) {
            $where_opt .= empty($where_opt) ? 'al.tb_alerta_modulo_nom = :param_opc1' : ' AND al.tb_alerta_modulo_nom = :param_opc1';
        }
        if (!empty($prioridad)) {
            $where_opt .= empty($where_opt) ? 'al.tb_alerta_prioridad = :param_opc2' : ' AND al.tb_alerta_prioridad = :param_opc2';
        }
        if (!empty($modulo_id)) {
            $where_opt .= empty($where_opt) ? 'al.tb_alerta_modulo_id = :param_opc3' : ' AND al.tb_alerta_modulo_id = :param_opc3';
        }
        if (!empty($titulo)) {
            $where_opt .= empty($where_opt) ? 'LOCATE(:param_opc4, al.tb_alerta_titulo) > 0' : ' AND LOCATE(:param_opc4, al.tb_alerta_titulo) > 0';
        }
        if (!empty($mens1)) {
            $where_opt .= empty($where_opt) ? 'LOCATE(:param_opc5, al.tb_alerta_mens1) > 0' : ' AND LOCATE(:param_opc5, al.tb_alerta_mens1) > 0';
        }
        if (!empty($mens2)) {
            $where_opt .= empty($where_opt) ? 'LOCATE(:param_opc6, al.tb_alerta_mens2) > 0' : ' AND LOCATE(:param_opc6, al.tb_alerta_mens2) > 0';
        }
        if (!empty($usuario_id)) {
            $where_opt .= empty($where_opt) ? 'al.tb_usuario_id = :param_opc7' : ' AND al.tb_usuario_id = :param_opc7';
        }
        if (in_array(strval($estado), array('0', '1'))) {
            $where_opt .= empty($where_opt) ? 'al.tb_alerta_estado = :param_opc8' : ' AND al.tb_alerta_estado = :param_opc8';
        }

        if (!empty($where_opt)) {
            $where_opt = "WHERE $where_opt";
        }
        try {
            $sql = "SELECT al.*,
                        CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as usuario_nom,
                        CONCAT(us1.tb_usuario_nom, ' ', us1.tb_usuario_ape) as usuario_reg_nom
                    FROM tb_alerta al
                    INNER JOIN tb_usuario us ON (al.tb_usuario_id = us.tb_usuario_id)
                    INNER JOIN tb_usuario us1 ON (al.tb_alerta_usureg = us1.tb_usuario_id)
                    $where_opt
                    ORDER BY al.tb_alerta_fecreg $orden;";
            //
            $retorno['sql'] = $sql;
            $sentencia = $this->dblink->prepare($sql);
            if (!empty($alerta_id)) {
                $sentencia->bindParam(':param_opc0', $alerta_id, PDO::PARAM_INT);
            }
            if (!empty($modulo_nom)) {
                $sentencia->bindParam(':param_opc1', $modulo_nom, PDO::PARAM_STR);
            }
            if (!empty($prioridad)) {
                $sentencia->bindParam(':param_opc2', $prioridad, PDO::PARAM_INT);
            }
            if (!empty($modulo_id)) {
                $sentencia->bindParam(':param_opc3', $modulo_id, PDO::PARAM_INT);
            }
            if (!empty($titulo)) {
                $sentencia->bindParam(':param_opc4', $titulo, PDO::PARAM_STR);
            }
            if (!empty($mens1)) {
                $sentencia->bindParam(':param_opc5', $mens1, PDO::PARAM_STR);
            }
            if (!empty($mens2)) {
                $sentencia->bindParam(':param_opc6', $mens2, PDO::PARAM_STR);
            }
            if (!empty($usuario_id)) {
                $sentencia->bindParam(':param_opc7', $usuario_id, PDO::PARAM_INT);
            }
            if (in_array(strval($estado), array('0', '1'))) {
                $sentencia->bindParam(':param_opc8', $estado, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function mostrar_uno_id($alerta_id) {
        try {
            $sql = "SELECT al.*,
                        CONCAT(us.tb_usuario_nom, ' ', us.tb_usuario_ape) as usuario_nom,
                        CONCAT(us1.tb_usuario_nom, ' ', us1.tb_usuario_ape) as usuario_reg_nom
                    FROM tb_alerta al
                    INNER JOIN tb_usuario us ON (al.tb_usuario_id = us.tb_usuario_id)
                    INNER JOIN tb_usuario us1 ON (al.tb_alerta_usureg = us1.tb_usuario_id)
                    WHERE al.tb_alerta_id = :param0;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $alerta_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function modificar_campo($alerta_id, $alerta_columna, $alerta_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($alerta_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_alerta SET " . $alerta_columna . " = :alerta_valor WHERE tb_alerta_id = :alerta_id;";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":alerta_id", $alerta_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":alerta_valor", $alerta_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":alerta_valor", $alerta_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function modificar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_alerta SET
                        tb_alerta_fecha_mostrar = :param0,
                        tb_alerta_modulo_nom = :param1,
                        tb_alerta_modulo_id = :param2,
                        tb_alerta_titulo = :param3,
                        tb_alerta_mens1 = :param4,
                        tb_alerta_mens2 = :param5,
                        tb_alerta_prioridad = :param6,
                        tb_alerta_coment = :param7
                    WHERE tb_alerta_id = :param_x;";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->fecha_mostrar, PDO::PARAM_STR);
            $sentencia->bindParam(":param1", $this->modulo_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":param2", $this->modulo_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param3", $this->titulo, PDO::PARAM_STR);
            $sentencia->bindParam(":param4", $this->mens1, PDO::PARAM_STR);
            $sentencia->bindParam(":param5", $this->mens2, PDO::PARAM_STR);
            $sentencia->bindParam(":param6", $this->prioridad, PDO::PARAM_INT);
            $sentencia->bindParam(":param7", $this->coment, PDO::PARAM_STR);
            $sentencia->bindParam(":param_x", $this->id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function nuevo_correlativo () {
        try {
            $sql = "SELECT IFNULL(MAX(tb_alerta_correlativo), 0) + 1 as new_correlativo
                    FROM tb_alerta;";
            //
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay resultado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function eliminar($alerta_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_alerta WHERE tb_alerta_id =:alerta_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":alerta_id", $alerta_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
