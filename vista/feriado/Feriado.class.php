<?php
if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Feriado extends Conexion{

    function listar_feriados(){
        try {
        $sql = "SELECT * FROM tb_feriado";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay feriados registrados";
            $retorno["data"] = "";
        }
        return $retorno;
        } catch (Exception $e) {
        throw $e;
        }

    }

    function listar_feriados_anio($anio){
        try {
        $sql = "SELECT * FROM tb_feriado WHERE tb_feriado_anio =:anio";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay feriados registrados";
            $retorno["data"] = "";
        }
        return $retorno;
        } catch (Exception $e) {
        throw $e;
        }

    }
}