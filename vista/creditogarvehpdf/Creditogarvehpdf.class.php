<?php

if (defined('APP_URL')) {
    require_once(APP_URL . 'datos/conexion.php');
} else {
    require_once('../../datos/conexion.php');
}

class Creditogarvehpdf extends Conexion {
    
    public $pdfgarv_id;
    public $tipogarvdocpersdetalle_id;
    public $cliente_id;
    public $credito_id;
    public $pdfgarv_ruta= '...';
    public $pdfgarv_xac = 1;
    public $pdfgatv_estado=1;
    public $pdfgarv_his;
    
    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    function insertar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO pdfgarv(tb_tipogarvdocpersdetalle_id, tb_tb_cliente_id, tb_tb_credito_id, tb_pdfgarv_ruta, tb_pdfgarv_estado, tb_pdfgarv_xac, tb_pdfgarv_his)
                    VALUES (:tipogarvdocpersdetalle_id, :cliente_id, :credito_id, :pdfgarv_ruta, :pdfgatv_estado, :pdfgarv_xac, :pdfgarv_his)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tipogarvdocpersdetalle_id", $this->tipogarvdocpersdetalle_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":pdfgarv_ruta", $this->pdfgarv_ruta, PDO::PARAM_STR);
            $sentencia->bindParam(":pdfgatv_estado", $this->pdfgatv_estado, PDO::PARAM_INT);
            $sentencia->bindParam(":pdfgarv_xac", $this->pdfgarv_xac, PDO::PARAM_INT);
            $sentencia->bindParam(":pdfgarv_his", $this->pdfgarv_his, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $this->pdfgarv_id = $this->dblink->lastInsertId();
            $this->dblink->commit();
            
            $nombre_imagen = 'creditogarveh_'.$this->credito_id.'_pdfgarv_'. $this->pdfgarv_id .'.'. $this->fileParts['extension'];
            $creditogarvehfile_url = $this->directorio . strtolower($nombre_imagen);
            
            if(move_uploaded_file($this->tempFile, '../../'. $creditogarvehfile_url)){
                //insertamos las imágenes
                $sql = "UPDATE pdfgarv SET tb_pdfgarv_ruta =:pdfgarv_ruta WHERE tb_pdfgarv_id  =:pdfgarv_id";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":pdfgarv_id", $this->pdfgarv_id, PDO::PARAM_INT);
                $sentencia->bindParam(":pdfgarv_ruta", $creditogarvehfile_url, PDO::PARAM_STR);
                $result2 = $sentencia->execute();
              }
              else{
                $this->eliminar($this->pdfgarv_id);
              }

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
    
    function exonerar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO pdfgarv(tb_tipogarvdocpersdetalle_id, tb_tb_cliente_id, tb_tb_credito_id, tb_pdfgarv_ruta, tb_pdfgarv_estado, tb_pdfgarv_xac, tb_pdfgarv_his)
                    VALUES (:tipogarvdocpersdetalle_id, :cliente_id, :credito_id, :pdfgarv_ruta, :pdfgatv_estado, :pdfgarv_xac, :pdfgarv_his)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tipogarvdocpersdetalle_id", $this->tipogarvdocpersdetalle_id, PDO::PARAM_INT);
            $sentencia->bindParam(":cliente_id", $this->cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":credito_id", $this->credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":pdfgarv_ruta", $this->pdfgarv_ruta, PDO::PARAM_STR);
            $sentencia->bindParam(":pdfgatv_estado", $this->pdfgatv_estado, PDO::PARAM_INT);
            $sentencia->bindParam(":pdfgarv_xac", $this->pdfgarv_xac, PDO::PARAM_INT);
            $sentencia->bindParam(":pdfgarv_his", $this->pdfgarv_his, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $data['registro_id']= $this->dblink->lastInsertId();
            $this->dblink->commit();
            
            $data['estado'] = $result; //si es correcto el ingreso retorna 1
            return $data;
            
            } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
            
        }

    function modificar($mod, $option) {
        $this->dblink->beginTransaction();
        try {
            if($option=='s'){
                $nombre_pdf = 'creditogarveh_'.$this->credito_id.'_pdfgarv_'. $this->pdfgarv_id . '_' . $mod . '.'. $this->fileParts['extension'];
                $creditogarvehfile_url = $this->directorio . strtolower($nombre_pdf);
            }
            else{
                $creditogarvehfile_url = '...';
            }
            
            $retorno['estado'] = 0;
            $retorno['mensaje'] = 'No se ha ejecutado el update';
            
            $sql = "UPDATE pdfgarv SET tb_pdfgarv_ruta= :ruta_mod , tb_pdfgarv_estado= :valor , tb_pdfgarv_xac=1, tb_pdfgarv_his = :hist  WHERE tb_pdfgarv_id = :pdfgarv_id ";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":ruta_mod", $creditogarvehfile_url, PDO::PARAM_STR);
            $sentencia->bindParam(":valor", $this->pdfgatv_estado, PDO::PARAM_INT);
            $sentencia->bindParam(":hist", $this->pdfgarv_his, PDO::PARAM_STR);
            $sentencia->bindParam(":pdfgarv_id", $this->pdfgarv_id, PDO::PARAM_INT);

            $result = $sentencia->execute();
            if($result == 1){
                $retorno['mensaje'] = 'SQL OK.';
                if($option=='s'){
                    $retorno['estado'] = 0;
                    $retorno['mensaje'] .= ' Falta mover el archivo';
                    if(move_uploaded_file($this->tempFile, '../../'. $creditogarvehfile_url)){
                        $retorno['estado'] = 1;
                        $retorno['mensaje'] = 'exito';
                        $this->dblink->commit();
                    }
                }
                else{
                    $retorno['estado'] = 1;
                    $retorno['mensaje'] = 'exito';
                    $this->dblink->commit();
                }
            }
            return $retorno; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar($pdfgarv_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE pdfgarv SET tb_pdfgarv_xac=0 WHERE tb_pdfgarv_id =:pdfgarv_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":pdfgarv_id", $pdfgarv_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($creditogarvehfile_id) {
        try {
            $sql = "SELECT * FROM tb_creditogarvehfile WHERE tb_creditogarvehfile_id =:creditogarvehfile_id AND tb_creditogarvehfile_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":creditogarvehfile_id", $creditogarvehfile_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrarUnoPorPdfgarvid($pdfgarv_id) {
        try {
            $sql = "SELECT * FROM pdfgarv WHERE tb_pdfgarv_id= :pdfgarv_id ";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":pdfgarv_id", $pdfgarv_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function filtrar($credito_id) {
        try {
            $sql = "SELECT * FROM tb_creditogarvehfile WHERE tb_credito_id =:tb_credito_id AND tb_creditogarvehfile_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_credito_id", $credito_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar() {
        try {
            $sql = "SELECT * FROM tb_creditogarvehfile WHERE tb_creditogarvehfile_xac=1 ORDER BY tb_creditogarvehfile_id DESC";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

   

}

?>
