$(document).ready(function() {
    
$("#frm_exonerarcreditogarceh").validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "creditogarvehpdf/credito_pdf_controller.php",
        async:true,
        dataType: "json",
        data: $("#frm_exonerarcreditogarceh").serialize(),
        beforeSend: function() {
          //$('#msj_credito').html("Guardando...");
          //$('#msj_credito').show(100);
        },
        success: function(data){
                if(parseInt(data.estado) > 0){
                    alerta_success("EXITO",data.mensaje);
                    $("#modal_creditogarveh_exonerar").modal('hide');
                    cargardocumentosnecesarios();
                }
                else
                  console.log(data);
        },
        complete: function(data){
          console.log(data);
        }
      });
    },
    rules: {
        txt_motivo_exonerar: {
                required: true
        }
    },
    messages: {
        txt_motivo_exonerar: {
                required: 'Ingresar motivo'
        }
    }
	  });
});

function eliminar(pdfgarv_id){
    $.ajax({
    type: "POST",
    url: VISTA_URL+"creditogarvehpdf/credito_pdf_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'eliminar',
      pdfgarv_id: pdfgarv_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
        console.log(data);
        if(parseInt(data.estado)>0){
          alerta_success("EXITO","Pdf eliminado correctamente");
          $("#modal_creditogarveh_exonerar").modal('hide');
          $("#modal_registro_creditogarveh").modal('hide');
          creditogarveh_form('M',$('#credito_id').val());
        }
      },
    complete: function(data){
      //$('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
};

