<?php
    require_once('../../core/usuario_sesion.php');
    session_start();
  $tipogarvdocpersdetalle_id = $_POST['tipogarvdocpersdetalle_id'];
  $credito_id = $_POST['credito_id'];
  $cliente_id = $_POST['cliente_id'];
  $user = $_SESSION['usuario_nombre'];

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarveh_exonerar" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
          <h4 class="modal-title">EXONERAR PDF <b>CREDITOGARVEH</b></h4>
      </div>
      <form id="frm_exonerarcreditogarceh">
        <input type="hidden" name="action" value="exonerar">
        <input type="hidden" name="credito_id" ID="credito_id" value="<?php echo $credito_id;?>">
        <input type="hidden" name="cliente_id" value="<?php echo $cliente_id;?>">
        <input type="hidden" name="usuario" value="<?php echo $user;?>">
        <input type="hidden" name="tipogarvdocpersdetalle_id" value="<?php echo $tipogarvdocpersdetalle_id;?>">
        <input type="hidden" name="hdd_cgarvdoc_nom" id="hdd_cgarvdoc_nom" value="<?php echo $_POST['cgarvdoc_nom'];?>">
        <?php if($_POST['pdf_is'] != 'new'){
                  $id_reg_mod = explode('-', $_POST['pdf_is']);
                  $mod_or_new = $id_reg_mod[0];
                  $id_reg_mod = $id_reg_mod[1];
                  ?>
                  <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $mod_or_new;?>">
                  <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="<?php echo intval($id_reg_mod);?>">
                <?php
                }
                else{
                  ?>
                  <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $_POST['pdf_is'];?>">
                  <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="<?php echo 0;?>">
                <?php
                }
                 ?>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="txt_motivo_exonerar" class="control-label" >Motivo :</label>
                            <div class="form-group">
                              <textarea name="txt_motivo_exonerar" id="txt_motivo_exonerar" rows="4" cols="70"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info" id="btn_guardar_exonerar">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
      <div id="msj_creditoexonerarpdf" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/creditogarvehpdf/exonerar_creditogarvehpdf.js?ver=436436643643634';?>"></script>
