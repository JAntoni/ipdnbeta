<?php ?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditogarvehpdf_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Subir <b>Archivo</b></h4>
            </div>
            <form id="form_garvehpdf" method="post">
                <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $_POST['cre_id'];?>">
                <input type="hidden" name="hdd_credito_id" id="hdd_cliente_id" value="<?php echo $_POST['cli_id'];?>">
                <input type="hidden" name="hdd_credito_id" id="hdd_docdetalle_id" value="<?php echo $_POST['documentodetalle_id'];?>">
                <input type="hidden" name="hdd_cgarvdoc_nom" id="hdd_cgarvdoc_nom" value="<?php echo $_POST['cgarvdoc_nom'];?>">
                <?php if($_POST['pdf_is'] != 'new'){
                  $id_reg_mod = explode('-', $_POST['pdf_is']);
                  $mod_or_new = $id_reg_mod[0];
                  $id_reg_mod = $id_reg_mod[1];
                  ?>
                  <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $mod_or_new;?>">
                  <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="<?php echo intval($id_reg_mod);?>">
                <?php
                }
                else{
                  ?>
                  <input type="hidden" name="hdd_pdf_is" id="hdd_pdf_is" value="<?php echo $_POST['pdf_is'];?>">
                  <input type="hidden" name="hdd_id_reg_mod" id="hdd_id_reg_mod" value="<?php echo 0;?>">
                <?php
                }
                 ?>
                
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                          <input id="file_upload" name="file_upload" type="file" multiple="true">
                        </div>
                        <div class="col-md-12">
                          <div id="queue"></div>
                        </div>
                        <div class="col-md-12">
                          <div class="alert alert-danger alert-dismissible" id="alert_img" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <h4>
                              <i class="icon fa fa-ban"></i> Alerta
                            </h4>
                          </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-info" id="btn_guardar_creditogarvehpdf" onclick="javascript:$('#file_upload').uploadifive('upload')">Guardar Pdf</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_cerrar_creditogarvehfile">Listo</button>
                    </div>
                </div>
            </form>
            <div id="msj_creditoverificacion" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo 'static/css/uploadifive/uploadifive.css';?>">
<script type="text/javascript" src="<?php echo 'static/js/uploadifive/jquery.uploadifive.js';?>"></script>
<script type="text/javascript" src="<?php echo 'vista/creditogarvehpdf/credito_pdf_form.js?ver=535643636364378563';?>"></script>
