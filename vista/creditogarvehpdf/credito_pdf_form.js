
$(document).ready(function(){
	$('#file_upload').uploadifive({
	  'auto'             : false,
	  //'checkScript'      : 'programacion_file_check.php',
	  'formData'         : {
	                         'action'    : 'insertar',
	                         'credito_id' : $('#hdd_credito_id').val(),
                                 'cliente_id' : $('#hdd_cliente_id').val(),
                                 'docdetalle' : $('#hdd_docdetalle_id').val(),
								 'cgarvdoc_nom' : $('#hdd_cgarvdoc_nom').val(),
								 'id_reg_mod' : $('#hdd_id_reg_mod').val(),
								 'pdf_is' : $('#hdd_pdf_is').val()
	                       },
	  'queueID'          : 'queue',
	  'uploadScript'     : VISTA_URL+'creditogarvehpdf/credito_pdf_controller.php',
	  'queueSizeLimit'   : 10,
	  'uploadLimit'      : 10,
	  'multi'            : true,
	  'buttonText'       : 'Seleccionar Archivo',
	  'height'           : 20,
	  'width'            : 180,
	  'fileSizeLimit'    : '50MB',
	  'fileType': ['pdf'],
	  'onUploadComplete' : function(file, data) {
              //console.log(data);
	   	if(data != 1 && data!=2){
	   		$('#alert_img').show(300);
	   		$('#alert_img').append('<strong>El archivo: '+file.name+', no se pudo subir -> '+data+'</strong><br>');
	   	}
		else{
			//credito_file_tabla();
			alerta_success("EXITO", "SE HA REGISTRADO CORRECTAMENTE EL DOCUMENTO");
			$("#modal_creditogarvehpdf_form").modal('hide');
			cargardocumentosnecesarios();
		}
	  },
	  'onError': function (errorType) {
	      var message = "Error desconocido.";
	      if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
	          message = "Tamaño de archivo debe ser menor a 3MB.";
	      } else if (errorType == 'FORBIDDEN_FILE_TYPE') {
	          message = "Tipo de archivo no válido.";
	          alert(message);
	      }
	  },
	  'onAddQueueItem' : function(file) {
	  },
	  /*'onCheck'      : function(file, exists) {
	      if (exists) {
	          alert('El archivo ' + file.name + ' ya existe.');
	      }
	  }*/
	});

	$('#form_garvehpdf').submit(function(event) {
		event.preventDefault();
		$.ajax({
				type: "POST",
				url: VISTA_URL+"creditogarvehpdf/credito_pdf_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_garvehpdf").serialize(),
				beforeSend: function() {
					$('#garantiafile_mensaje').show(400);
					$('#btn_guardar_creditogarvehpdf').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#garantiafile_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#garantiafile_mensaje').html(data.mensaje);
		      	//var garantiafile_id = $('#hdd_garantiafile_id').val();
		      	//$('#garantiafileid_'+garantiafile_id).hide(400);
		      	$('#modal_creditogarvehpdf_form').modal('hide');
					}
					else{
		      	$('#garantiafile_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#garantiafile_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_creditogarvehpdf').prop('disabled', false);
		      }
				},
				complete: function(data){
					////console.log(data);
				},
				error: function(data){
					$('#garantiafile_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#garantiafile_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	});
});

function eliminar(pdfgarv_id){
    $.ajax({
    type: "POST",
    url: VISTA_URL+"creditogarvehpdf/credito_pdf_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'eliminar',
	  cgarvdoc_nom: $('#hdd_credito_nomdoc').val(),
	  tipo_registro: $('#hdd_tipo_registro').val(),
	  credito_id: $('#hdd_credito_id').val(),
      pdfgarv_id: pdfgarv_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
        //console.log(data);
        if(parseInt(data.estado)>0){
          alerta_success("EXITO","Pdf eliminado correctamente");
          $("#modal_creditopdf_ver").modal('hide');
          cargardocumentosnecesarios();
        }
      },
    complete: function(data){
      //$('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      //console.log(data.responseText);
    }
  });
};

/* GERSON (09-02-24) */
function eliminarItemProceso(item_des,multimedia_id){
    $.ajax({
    type: "POST",
    url: VISTA_URL+"proceso/proceso_controller.php",
    async: true,
    dataType: "json",
    data: ({
      action: 'eliminar_documento',
	  proceso_fase_item_id: 0, // no es necesario, cuando se elimina doc desde checklist de credito garveh
	  item_des: item_des,
      multimedia_id: multimedia_id
    }),
    beforeSend: function() {
      //$('#h3_modal_title').text('Cargando Formulario');
      //$('#modal_mensaje').modal('show');
    },
    success: function(data){
        if(parseInt(data.estado)>0){
          alerta_success("EXITO","Documento eliminado correctamente");
          $("#modal_creditopdf_ver").modal('hide');
		  cargardocumentosnecesarios();
        }
      },
    complete: function(data){
      //$('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
    }
  });
};
/*  */
