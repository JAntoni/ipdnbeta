<?php

require_once('../../core/usuario_sesion.php');
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../creditogarvehpdf/Creditogarvehpdf.class.php');
$oCreditogarvehpdf = new Creditogarvehpdf();
require_once ('../historial/Historial.class.php');
$oHistorial = new Historial();
require_once('../comentario/Comentario.class.php');
$oComentario = new Comentario();
$fecha_hoy = date('d-m-Y h:i a');

$action = $_POST['action'];

/* if ($action == 'insertar') {
    if (!empty($_FILES)) {
        $creditogarveh_id = $_POST['credito_id'];
        $cliente_id = $_POST['cliente_id'];
        $docdetalle = $_POST['docdetalle'];
        $directorio = 'public/pdf/creditogarveh/';
        //$modulo_nom = $_POST['modulo_nom'];
        //$modulo_id = intval($_POST['modulo_id']);
        //$upload_uniq = $_POST['upload_uniq']; //id temporal

        //$directorio = 'public/pdf/' . $modulo_nom . '/';
        $fileTypes = array('pdf'); // Allowed file extensions

        $tempFile = $_FILES['Filedata']['tmp_name'];
        $targetFile = $directorio . $_FILES['Filedata']['name'];
        $fileParts = pathinfo($_FILES['Filedata']['name']);

        if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
            //move_uploaded_file($tempFile, $targetFile); insertamos las imágenes
            //$oUpload->modulo_nom = $modulo_nom;
            //$oUpload->modulo_id = $modulo_id;
            //$oUpload->upload_uniq = $upload_uniq;
            $oCreditogarvehpdf->credito_id = $creditogarveh_id;
            $oCreditogarvehpdf->cliente_id = $cliente_id;
            $oCreditogarvehpdf->tipogarvdocpersdetalle_id = $docdetalle;
            $oCreditogarvehpdf->pdfgarv_his = 'Archivo subido por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a');
            
            $oCreditogarvehpdf->tempFile = $tempFile; //temp servicio de PHP
            $oCreditogarvehpdf->directorio = $directorio; //nombre con el que irá guardado en la carpeta
            $oCreditogarvehpdf->fileParts = $fileParts; //extensiones de las imágenes
            //echo "VER"; exit();
            $return = $oCreditogarvehpdf->insertar();

            if($return == 1){
                $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                $oHistorial->setTbHistNomTabla("pdfgarv");
                $oHistorial->setTbHistRegmodid($oCreditogarvehpdf->pdfgarv_id);
                $oHistorial->setTbHistDet('Ha subido el archivo "'.$_POST['cgarvdoc_nom'].'" del crédito '.$oCreditogarvehpdf->credito_id.' | <b>'.$fecha_hoy.'</b>');

                $oHistorial->insertar();
            }

            echo trim($return);
        } else {
            // The file type wasn't allowed
            echo 'Tipo de archivo no válido.';
        }
    } else {
        echo 'No hay ningún archivo para subir';
    }
} */

//MOD DE DANIEL ODAR
if ($action == 'insertar') {
    if (!empty($_FILES)) {
        $creditogarveh_id = $_POST['credito_id'];
        $directorio = 'public/pdf/creditogarveh/';

        $tempFile = $_FILES['Filedata']['tmp_name'];
        $fileParts = pathinfo($_FILES['Filedata']['name']);

        $fileTypes = array('pdf'); // Allowed file extensions

        $pdf_is = $_POST['pdf_is'];

        $oCreditogarvehpdf->credito_id = $creditogarveh_id;
        
        $oCreditogarvehpdf->tempFile = $tempFile; //temp servicio de PHP
        $oCreditogarvehpdf->directorio = $directorio; //nombre con el que irá guardado en la carpeta
        $oCreditogarvehpdf->fileParts = $fileParts; //extensiones de las imágenes

        if($pdf_is == 'new'){ //si es exitosa la modificacion retorna 1
            $cliente_id = $_POST['cliente_id'];
            $docdetalle = $_POST['docdetalle'];

            if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
                $oCreditogarvehpdf->cliente_id = $cliente_id;
                $oCreditogarvehpdf->tipogarvdocpersdetalle_id = $docdetalle;
                $oCreditogarvehpdf->pdfgarv_his = 'Archivo subido por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a');
                
                //echo "VER"; exit();
                $return = $oCreditogarvehpdf->insertar();

                if($return == 1){
                    $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                    $oHistorial->setTbHistNomTabla("pdfgarv");
                    $oHistorial->setTbHistRegmodid($oCreditogarvehpdf->pdfgarv_id);
                    $oHistorial->setTbHistDet('Ha subido el archivo "'.$_POST['cgarvdoc_nom'].'" del crédito '.$oCreditogarvehpdf->credito_id.' | <b>'.$fecha_hoy.'</b>');

                    $oHistorial->insertar();
                }

                echo trim($return);
            } else {
                // The file type wasn't allowed
                echo 'Tipo de archivo no válido.';
            }
        }
        else{ //mod1... si es exitosa la modificacion retorna 2
            if (in_array(strtolower($fileParts['extension']), $fileTypes)){
                $oCreditogarvehpdf->pdfgarv_id = intval($_POST['id_reg_mod']);
                $oCreditogarvehpdf->pdfgarv_his = 'Archivo modificado. Subido por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a');
                $oCreditogarvehpdf->pdfgatv_estado = 1;

                $return = $oCreditogarvehpdf->modificar($pdf_is, 's');
                if($return['estado']==1){
                    echo 2;
                    $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
                    $oHistorial->setTbHistNomTabla("pdfgarv");
                    $oHistorial->setTbHistRegmodid($oCreditogarvehpdf->pdfgarv_id);
                    $oHistorial->setTbHistDet('Ha actualizado el archivo "'.$_POST['cgarvdoc_nom'].'" del crédito '.$oCreditogarvehpdf->credito_id.' | <b>'.$fecha_hoy.'</b>');

                    $oHistorial->insertar();
                }
                else{
                    echo trim($return['mensaje']);
                }
            }
            else {
                // The file type wasn't allowed
                echo 'Tipo de archivo no válido.';
            }
        }
        
    }
    else {
        echo 'No hay ningún archivo para subir';
    }
}
elseif ($action == 'modificar') {
    $upload_id = intval($_POST['hdd_upload_id']);
    $oUpload->upload_id = $upload_id;

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar.';

    if ($oUpload->modificar()) {
        $data['estado'] = 1;
        $data['mensaje'] = 'upload modificado correctamente.';
    }

    echo json_encode($data);
}
elseif ($action == 'eliminar') {
    $pdfgarv_id = intval($_POST['pdfgarv_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al eliminar el pdf.';

    if ($oCreditogarvehpdf->eliminar($pdfgarv_id)) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Pdf eliminado correctamente.';
        //Odar: añadir historial de eliminacion del pdf
        $ruta = $oCreditogarvehpdf->mostrarUnoPorPdfgarvid($pdfgarv_id)['data']['tb_pdfgarv_ruta'];
        if($ruta != '...'){
            $ruta = ' El archivo eliminado está <a href="'.$ruta.'" target="_blank">AQUI</a>';
        }
        else{
            $ruta='';
        }

        $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
        $oHistorial->setTbHistNomTabla("pdfgarv");
        $oHistorial->setTbHistRegmodid($pdfgarv_id);
        $oHistorial->setTbHistDet('Ha eliminado la '.$_POST['tipo_registro'].' del archivo "'.$_POST['cgarvdoc_nom'].'" del crédito '.$_POST['credito_id'].'. '.$ruta.' | <b>'.$fecha_hoy.'</b>');
        $oHistorial->insertar();
    }

    echo json_encode($data);
}
/* elseif ($action == 'exonerar') {
    $motivo = mb_strtoupper($_POST['txt_motivo_exonerar'], 'UTF-8');
    $credito_id = $_POST['credito_id'];
    $cliente_id = $_POST['cliente_id'];
    $tipogarvdocpersdetalle_id = $_POST['tipogarvdocpersdetalle_id'];
    $his = '<span style="color: green;">Documento exonerado por: <b>'.$_SESSION['usuario_nom'].'</b>, el día <b>'.date('d-m-Y h:i a').'</b> | el motivo ingresado es: <b>'. $motivo .'</b></span><br>';

    
    $oCreditogarvehpdf->tipogarvdocpersdetalle_id = $tipogarvdocpersdetalle_id;
    $oCreditogarvehpdf->credito_id = $credito_id;
    $oCreditogarvehpdf->cliente_id = $cliente_id;
    $oCreditogarvehpdf->pdfgarv_ruta = '...';
    $oCreditogarvehpdf->pdfgatv_estado = 2;
    $oCreditogarvehpdf->pdfgarv_xac = 1;
    $oCreditogarvehpdf->pdfgarv_his = $his;
    
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al exonerar el pdf.';
    
    $result = $oCreditogarvehpdf->exonerar();
    if($result['estado']==1){
        $data['estado'] = 1;
        $data['mensaje'] = 'Se ha logrado exonerar este pdf.';
    }
    
    echo json_encode($data);
    
} */
//mod de odar
elseif ($action == 'exonerar') {
    $motivo = mb_strtoupper($_POST['txt_motivo_exonerar'], 'UTF-8');
    $credito_id = intval($_POST['credito_id']);
    $cliente_id = intval($_POST['cliente_id']);
    $tipogarvdocpersdetalle_id = intval($_POST['tipogarvdocpersdetalle_id']);
    $his = '<span style="color: green;">Documento exonerado por: <b>'.$_SESSION['usuario_nom'].'</b>, el día <b>'.date('d-m-Y h:i a').'</b> | el motivo ingresado es: <b>'. $motivo .'</b></span><br>';

    
    $oCreditogarvehpdf->tipogarvdocpersdetalle_id = $tipogarvdocpersdetalle_id;
    $oCreditogarvehpdf->credito_id = $credito_id;
    $oCreditogarvehpdf->cliente_id = $cliente_id;
    $oCreditogarvehpdf->pdfgarv_ruta = '...';
    $oCreditogarvehpdf->pdfgatv_estado = 2;
    $oCreditogarvehpdf->pdfgarv_xac = 1;
    $oCreditogarvehpdf->pdfgarv_his = $his;
    
    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al exonerar el pdf.';
    
    if($_POST['hdd_pdf_is'] == 'new'){
        $result = $oCreditogarvehpdf->exonerar();
        $oCreditogarvehpdf->pdfgarv_id = intval($result['registro_id']);
    }
    else{
        $oCreditogarvehpdf->pdfgarv_id = intval($_POST['hdd_id_reg_mod']);
        $result = $oCreditogarvehpdf->modificar($_POST['hdd_pdf_is'], 'e');
    }
    if($result['estado']==1){
        $data['estado'] = 1;
        $data['mensaje'] = 'Se ha logrado exonerar este pdf.';
        //registrar historial
        $oHistorial->setTbHistUsureg($_SESSION['usuario_id']);
        $oHistorial->setTbHistNomTabla("pdfgarv");
        $oHistorial->setTbHistRegmodid($oCreditogarvehpdf->pdfgarv_id);
        $oHistorial->setTbHistDet('Ha exonerado el documento "'.$_POST['hdd_cgarvdoc_nom'].'" del crédito '.$oCreditogarvehpdf->credito_id.'. | <b>'.$fecha_hoy.'</b>');

        $histo_id = $oHistorial->insertar()['historial_id'];
        //registrar comentario de la exoneracion
        $oComentario->setTbComentUsureg($_SESSION['usuario_id']);
        $oComentario->setTbComentDet(explode(' ',$_SESSION['usuario_nom'])[0].' ingresó el motivo: '.$_POST['txt_motivo_exonerar'].'.');
        $oComentario->setTbComentNomTabla('tb_hist');
        $oComentario->setTbComentRegmodid($histo_id);
        $oComentario->insertar();
    }
    echo json_encode($data);
}
else {
    echo 'No se ha identificado ningún tipo de transacción para ' . $action;
}
?>
