<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

require_once('../tipogarvdocpersdetalle/Tipogarvdocpersdetalle.class.php');
$oTipogarvdocpersdetalle = new Tipogarvdocpersdetalle();
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();

$pdfgarv_id = intval($_POST['pdfgarv_id']);
$proceso_id = (isset($_POST['proceso_id']))? intval($_POST['proceso_id']) : 0;
$item_id = (isset($_POST['item_id']))? intval($_POST['item_id']) : 0;

if($pdfgarv_id>0){

    $result = $oTipogarvdocpersdetalle->mostrarUno($pdfgarv_id);
    if ($result['estado'] == 1) {
        $credito = $result['data']['tb_tb_credito_id'];
        $nombre_doc = $result['data']['cgarvdoc_nom'];
            if($result['data']['tb_pdfgarv_estado']==1){ 
            $ver = '<div class="item">
                    <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="hdd_tipo_registro" id="hdd_tipo_registro" value="subida">
                        <p class="message" style="font-family:cambria">
                            <a href="'.$result['data']['tb_pdfgarv_ruta'].'" class="name"  target="_blank">
                                '.$result['data']['cgarvdoc_nom'].' <i class="fa fa-file-pdf-o" title="VER"></i> 
                            </a>
                            <small class="text-muted pull-right"><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminar('.$pdfgarv_id.')">Eliminar <i class="fa fa-trash"></i></a></small>
                        </p>
                    </div>
                    
                    </div>
                </div>';
        }
        if ($result['data']['tb_pdfgarv_estado']==2) {
                $ver = '<div class="item">
                    <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="hdd_tipo_registro" id="hdd_tipo_registro" value="exoneración">
                        <p class="message name" style="font-family:cambria">
                            '.$result['data']['tb_pdfgarv_his'].'
                        </p>
                        <small class="text-muted pull-right"><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminar('.$pdfgarv_id.')">Eliminar <i class="fa fa-trash"></i></a></small>
                    </div>
                    
                    </div>
                </div>';
                
            }
    }
    if ($result['estado'] == 0) {
        $ver = '<div class="item">
                    <div class="row">
                    <div class="col-md-12">
                        <p class="message name" style="font-family:cambria">
                            <span style="color: red;"> ERROR EN EL CODIGO INGRESADO </span>
                        </p>
                    </div>
                    
                    </div>
                </div>';
    }

}else{

    /* GERSON (24-01-24) */
    $ver = '';
    if($proceso_id>0 && $item_id>0){
        $archivo = $oProceso->obtenerContenidoPorProcesoFaseItem($proceso_id, $item_id);
        if ($archivo['estado'] == 1) {

            $item = $oProceso->obtenerItemPorId($item_id);
            $item_des = '';
            if($item['estado'] == 1){
                if(intval($archivo['data']['tb_multimedia_exo']) > 0){
                    $ver = '<div class="item">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="message name" style="font-family:cambria">
                                    <span style="color: red;"> ARCHIVO EXONERADO </span>
                                </p>
                            </div>
                        </div>
                    </div>';
                }else{
                    $item_des = $item['data']['tb_item_des'];
                    $enlace_eliminar = "'".$item_des."',".$archivo['data']['tb_multimedia_id'];
                    $ver = '<div class="item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="hdd_tipo_registro" id="hdd_tipo_registro" value="subida">
                                        <p class="message" style="font-family:cambria">
                                            <a href="'.$archivo['data']['tb_multimedia_url'].'" class="name"  target="_blank">
                                                '.$item_des.' <i class="fa fa-file-pdf-o" title="VER"></i> 
                                            </a>
                                            <small class="text-muted pull-right"><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminarItemProceso('.$enlace_eliminar.')">Eliminar <i class="fa fa-trash"></i></a></small>

                                        </p>
                                    </div>
                                </div>
                            </div>';
                }
            }
            
        }
    }
    /*  */

}


?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_creditopdf_ver" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000099">ARCHIVO SUBIDO(s) EN PDF</h4>
            </div>
            <div class="modal-body">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        
                        <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                            <div class="btn-group" data-toggle="btn-toggle">

                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hdd_pdfgarv_id" id="hdd_pdfgarv_id" value="<?php echo $pdfgarv_id;?>">
                    <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito;?>">
                    <input type="hidden" name="hdd_credito_nomdoc" id="hdd_credito_nomdoc" value="<?php echo $nombre_doc;?>">
                    <div class="box-body" id="chat-box">
                        <?php echo $ver ?>
                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/creditogarvehpdf/credito_pdf_form.js?ver=09022024'; ?>"></script>

