<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Transferente extends Conexion{

//    function insertar($transferente_nom, $transferente_dni, $transferente_dir, $transferente_den){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "INSERT INTO tb_transferente(tb_transferente_xac, tb_transferente_nom, tb_transferente_dni, tb_transferente_dir, tb_transferente_den)
//          VALUES (1, :transferente_nom, :transferente_dni, :transferente_dir, :transferente_den)";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":transferente_nom", $transferente_nom, PDO::PARAM_STR);
//        $sentencia->bindParam(":transferente_dni", $transferente_dni, PDO::PARAM_STR);
//        $sentencia->bindParam(":transferente_dir", $transferente_dir, PDO::PARAM_STR);
//        $sentencia->bindParam(":transferente_den", $transferente_den, PDO::PARAM_STR);
//
//        $result = $sentencia->execute();
//
//        $this->dblink->commit();
//
//        return $result; //si es correcto el ingreso retorna 1
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }
//    function modificar($ttransferente, $transferente_nom, $transferente_dni, $transferente_dir, $transferente_den){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "UPDATE tb_transferente SET tb_transferente_nom =:transferente_nom, tb_transferente_dni =:transferente_dni, tb_transferente_dir =:transferente_dir, tb_transferente_den =:transferente_den WHERE tb_ttransferente =:ttransferente";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":ttransferente", $ttransferente, PDO::PARAM_INT);
//        $sentencia->bindParam(":transferente_nom", $transferente_nom, PDO::PARAM_STR);
//        $sentencia->bindParam(":transferente_dni", $transferente_dni, PDO::PARAM_STR);
//        $sentencia->bindParam(":transferente_dir", $transferente_dir, PDO::PARAM_STR);
//        $sentencia->bindParam(":transferente_den", $transferente_den, PDO::PARAM_STR);
//
//        $result = $sentencia->execute();
//
//        $this->dblink->commit();
//
//        return $result; //si es correcto retorna 1
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }
//    function eliminar($ttransferente){
//      $this->dblink->beginTransaction();
//      try {
//        $sql = "DELETE FROM tb_transferente WHERE tb_ttransferente =:ttransferente";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->bindParam(":ttransferente", $ttransferente, PDO::PARAM_INT);
//
//        $result = $sentencia->execute();
//
//        $this->dblink->commit();
//
//        return $result; //si es correcto retorna 1
//
//      } catch (Exception $e) {
//        $this->dblink->rollBack();
//        throw $e;
//      }
//    }
    function mostrarUno($transferente){
      try {
        $sql = "SELECT * FROM tb_transferente WHERE tb_transferente_id=:transferente";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":transferente", $transferente, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
//    function listar_transferentes(){
//      try {
//        $sql = "SELECT * FROM tb_transferente";
//
//        $sentencia = $this->dblink->prepare($sql);
//        $sentencia->execute();
//
//        if ($sentencia->rowCount() > 0) {
//          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
//          $retorno["estado"] = 1;
//          $retorno["mensaje"] = "exito";
//          $retorno["data"] = $resultado;
//          $sentencia->closeCursor(); //para libera memoria de la consulta
//        }
//        else{
//          $retorno["estado"] = 0;
//          $retorno["mensaje"] = "No hay tipos de crédito registrados";
//          $retorno["data"] = "";
//        }
//
//        return $retorno;
//      } catch (Exception $e) {
//        throw $e;
//      }
//
//    }
    
    function transferente_autocomplete($dato, $transferente_emp){
      try {

        $filtro = "%".$dato."%";
        $transferente_empresa = '';
        if(intval($transferente_emp) == 1)
          $transferente_empresa = 'AND tb_transferente_emp = 1';

        $sql = "SELECT * FROM tb_transferente WHERE (tb_transferente_nom LIKE :filtro OR tb_transferente_doc LIKE :filtro) GROUP BY CONCAT(tb_transferente_nom) LIMIT 0,12";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay transferentes registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
