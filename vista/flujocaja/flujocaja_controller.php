<?php

require_once('../../core/usuario_sesion.php');
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$action = $_POST['action'];

if ($action == 'resumen') {

    $empresa_id = intval($_POST['empresa_id']);
    $fecha1 = fecha_mysql($_POST['fecha1']);
    $fecha2 = fecha_mysql($_POST['fecha2']);
    $usuario_id = intval($_POST['usuario_id']);
    $moneda_id = intval($_POST['moneda_id']);

    $total_ingresos = 0;
    $result = $oIngreso->total_ingresos_fecha($fecha1, $fecha2, $usuario_id, $empresa_id, 1);
    if ($result['estado'] == 1)
        $total_ingresos = floatval($result['data']['total_ingresos']);
    $result = NULL;

    $total_egresos = 0;
    $result = $oEgreso->total_egresos_fecha($fecha1, $fecha2, $usuario_id, $empresa_id, 1);
    if ($result['estado'] == 1)
        $total_egresos = floatval($result['data']['total_egresos']);
    $result = NULL;

    $total_desembolso = 0;
    $result = $oEgreso->total_desembolsos_fecha($fecha1, $fecha2, $usuario_id, $empresa_id, $moneda_id, 19, 0);
    if ($result['estado'] == 1)
        $total_desembolso = floatval($result['data']['total_desembolso']);
    $result = NULL;

    //obtenemos la suma total de ingresos realizados por cheques en físico, sin cobrar
    //usamos la cuenta de ingreso: cuenta_id = 2 (INGRESO DE CAJA), subcuenta_id in (176,179) => REGISTRO DE CHEQUES
    $cuenta_id = 2; $subcuenta_id = '176,179'; $total_cheques_SOLES_FISICOS = 0;
    $result = $oIngreso->total_ingresos_cuenta_subcuenta_usuario($fecha1, $fecha2, $usuario_id, $empresa_id, 1, $cuenta_id, $subcuenta_id);
      if($result['estado'] == 1)
        $total_cheques_SOLES_FISICOS = floatval($result['data']['total_ingresos']);
    $result = NULL;

    // $cuenta_id = 2; $subcuenta_id = 179; $total_cheques_soles_GASTOS = 0;
    // $result = $oIngreso->total_ingresos_cuenta_subcuenta_usuario($fecha1, $fecha2, $usuario_id, $empresa_id, 1, $cuenta_id, $subcuenta_id);
    //   if($result['estado'] == 1)
    //     $total_cheques_soles_GASTOS = floatval($result['data']['total_ingresos']);
    // $result = NULL;

    //obtenemos los desembolsos totales que se han hecho al crédito vehicular para restar a los cheques almacenados registrados
    //para los egresos vamos a usar las cuentas: cuenta_id = 
    $cuenta_id = 19; $subcuenta_id = 178; $total_cheques_SOLES_DESEMBOLSADOS = 0;
    $result = $oEgreso->total_egresos_cuenta_subcuenta_usuario($fecha1, $fecha2, $usuario_id, $empresa_id, 1, $cuenta_id, $subcuenta_id);
      if($result['estado'] == 1)
        $total_cheques_SOLES_DESEMBOLSADOS = floatval($result['data']['total_egresos']);
    $result = NULL;

    //los cheques registados no deben afectar a los ingresos, por ello les restamos todos los cheques almanacenados
    $total_ingresos = $total_ingresos - $total_cheques_SOLES_FISICOS;
    //restamos los cheques desembolsados a los egresos para que no afecte
    $total_egresos = $total_egresos - $total_cheques_SOLES_DESEMBOLSADOS;

    $cheques_SOLES_FISICOS = $total_cheques_SOLES_FISICOS - $total_cheques_SOLES_DESEMBOLSADOS;

    $saldo_total = $total_ingresos - $total_egresos;

    $data['estado'] = 1;
    $data['mensaje'] = 'Cosulta exitosa, fec1: ' . $fecha1 . ' / fec2: ' . $fecha2 . ' / empre: ' . $empresa_id;
    $data['total_ingresos'] = mostrar_moneda($total_ingresos);
    $data['total_egresos'] = mostrar_moneda($total_egresos);
    $data['saldo'] = mostrar_moneda($saldo_total);
    $data['saldo_cheques_soles'] = mostrar_moneda($cheques_SOLES_FISICOS);
    $data['desembolso'] = mostrar_moneda($total_desembolso);

    $total_ingresos_dolares = 0;
    $result = $oIngreso->total_ingresos_fecha($fecha1, $fecha2, $usuario_id, $empresa_id, 2);
    if ($result['estado'] == 1)
        $total_ingresos_dolares = floatval($result['data']['total_ingresos']);
    $result = NULL;

    $total_egresos_dolares = 0;
    $result = $oEgreso->total_egresos_fecha($fecha1, $fecha2, $usuario_id, $empresa_id, 2);
    if ($result['estado'] == 1)
        $total_egresos_dolares = floatval($result['data']['total_egresos']);
    $result = NULL;
    
    //obtenemos la suma total de ingresos realizados por cheques en físico, sin cobrar
    //usamos la cuenta de ingreso: cuenta_id = 2 (INGRESO DE CAJA), subcuenta_id in (176,179) => REGISTRO DE CHEQUES
    $cuenta_id = 2; $subcuenta_id = '176,179'; $total_cheques_DOLARES_FISICOS = 0;
    $result = $oIngreso->total_ingresos_cuenta_subcuenta_usuario($fecha1, $fecha2, $usuario_id, $empresa_id, 2, $cuenta_id, $subcuenta_id);
      if($result['estado'] == 1)
        $total_cheques_DOLARES_FISICOS = floatval($result['data']['total_ingresos']);
    $result = NULL;

    // $cuenta_id = 2; $subcuenta_id = 179; $total_cheques_DOLARES_GASTOS = 0;
    // $result = $oIngreso->total_ingresos_cuenta_subcuenta_usuario($fecha1, $fecha2, $usuario_id, $empresa_id, 2, $cuenta_id, $subcuenta_id);
    //   if($result['estado'] == 1)
    //     $total_cheques_DOLARES_GASTOS = floatval($result['data']['total_ingresos']);
    // $result = NULL;

    //obtenemos los desembolsos totales que se han hecho al crédito vehicular para restar a los cheques almacenados registrados
    //para los egresos vamos a usar las cuentas: cuenta_id = 
    $cuenta_id = 19; $subcuenta_id = 178; $total_cheques_DOLARES_desembolsados = 0;
    $result = $oEgreso->total_egresos_cuenta_subcuenta_usuario($fecha1, $fecha2, $usuario_id, $empresa_id, 2, $cuenta_id, $subcuenta_id);
      if($result['estado'] == 1)
        $total_cheques_DOLARES_desembolsados = floatval($result['data']['total_egresos']);
    $result = NULL;

    $total_ingresos_dolares = $total_ingresos_dolares - $total_cheques_DOLARES_FISICOS;
    $total_egresos_dolares = $total_egresos_dolares - $total_cheques_DOLARES_desembolsados;

    $cheques_DOLARES_FISICOS = $total_cheques_DOLARES_FISICOS - $total_cheques_DOLARES_desembolsados;

    $saldo_total_dolares = $total_ingresos_dolares - $total_egresos_dolares;
    
    $data['saldo_cheques_dolares'] = mostrar_moneda($cheques_DOLARES_FISICOS);
    $data['total_ingresos_dolares'] = mostrar_moneda($total_ingresos_dolares);
    $data['total_egresos_dolares'] = mostrar_moneda($total_egresos_dolares);
    $data['saldo_dolares'] = mostrar_moneda($saldo_total_dolares);
    

    echo json_encode($data);
} 
elseif ($action == 'eliminar') {
    
}
elseif ($action == 'cambio_sede') {
  $tipo_operacion = $_POST['tipo_operacion'];
  $operacion_id = $_POST['operacion_id'];
  $empresa_id = $_POST['empresa_id'];
  $empresa_cambio = 0;
  $empresa_nom = 'SEDE BOULEVARD';
  $empresa_cambio_mom = 'SEDE MALL AVENTURA';

  if($empresa_id == 1){
    $empresa_cambio = 2;
  }
  if($empresa_id == 2){
    $empresa_cambio = 1;
    $empresa_nom = 'SEDE MALL AVENTURA';
    $empresa_cambio_mom = 'SEDE BOULEVARD';
  }

  $data['estado'] = 0;
  $data['mensaje'] = 'Existe un error al intentar cambiar de sede para: '.$tipo_operacion.', ID: '.$operacion_id;

  if($tipo_operacion == 'ingreso'){
    if($empresa_cambio > 0){
      $oIngreso->modificar_campo($operacion_id, 'tb_empresa_id', $empresa_cambio, 'INT');

      $data['estado'] = 1;
      $data['mensaje'] = 'INGRESO cambiado desde la '.$empresa_nom.' hacia la '.$empresa_cambio_mom;
    }
  }
  if($tipo_operacion == 'egreso'){
    if($empresa_cambio > 0){
      $oEgreso->modificar_campo($operacion_id, 'tb_empresa_id', $empresa_cambio, 'INT');

      $data['estado'] = 1;
      $data['mensaje'] = 'EGRESO cambiado desde la '.$empresa_nom.' hacia la '.$empresa_cambio_mom;
    }
  }

  echo json_encode($data);
} 
else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>
