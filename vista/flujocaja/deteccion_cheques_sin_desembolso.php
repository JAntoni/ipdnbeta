<?php
date_default_timezone_set("America/Lima");
require_once('../proceso/Proceso.class.php');
$oProceso = new Proceso();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();

$fecha_hoy = date('2024-06-07');
$primer_dia = date('01-m-Y');
$mes = date('m');
$anio = date('Y');
$numDays = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);

$lista_cheques = $oProceso->mostrarChequesDelDia(); // Obtener los cheques registrados del día desde INGRESOS

// detectar domingo o feriado 
$dia=intval(date("w", strtotime($fecha_hoy))); // domingos
$feriado = $oAsistencia->detectar_feriado($fecha_hoy, $anio); // feriados

// detectar responsable
$user_id = 11; // usuario de sistema

if($feriado['estado']==0){ // Identifica si no se trata de un FERIADO
  if($dia>0){ // Identifica si el día no es un DOMINGO

    $i = 0;
    if($lista_cheques['estado'] == 1){

      foreach ($lista_cheques['data'] as $key => $value) {
        
        $egresos = $oProceso->detectarDesembolsosXCheque($value['tb_credito_id']); // verificar si hay EGRESOS en el crédito detectado (desembolso cheques)
        if($egresos['estado'] == 0){ //Si no se encuentran egresos de los cheques del crédito se procede a registrarse

          $chequedetalle_id = 0;
          if(intval($value['tb_chequedetalle_id']) > 0){
            $chequedetalle_id = intval($value['tb_chequedetalle_id']);

            $oIngreso->ingreso_usureg = 11;
            $oIngreso->ingreso_usumod = 11;
            $oIngreso->ingreso_fec = $value['tb_ingreso_fec'];
            $oIngreso->documento_id = 8; // otros ingresos
            $oIngreso->ingreso_numdoc = $value['tb_ingreso_numdoc'];
            $oIngreso->ingreso_det = $value['tb_ingreso_det'];
            $oIngreso->ingreso_imp = floatval($value['tb_ingreso_imp']);
            $oIngreso->cuenta_id = 2; // INGRESO DE CAJA
            $oIngreso->subcuenta_id = 176; // CHEQUE ALMACENADO
            $oIngreso->cliente_id = intval($value['tb_cliente_id']);
            $oIngreso->caja_id = 1;
            $oIngreso->moneda_id = intval($value['tb_moneda_id']);
            //valores que pueden ser cambiantes según requerimiento de ingreso
            $oIngreso->modulo_id = 0;
            $oIngreso->ingreso_modide = 0;
            $oIngreso->empresa_id = intval($value['tb_empresa_id']);
            $oIngreso->ingreso_fecdep = '';
            $oIngreso->ingreso_numope = '';
            $oIngreso->ingreso_mondep = 0;
            $oIngreso->ingreso_comi = 0;
            $oIngreso->cuentadeposito_id = 0;
            $oIngreso->banco_id = 0;
            $oIngreso->ingreso_ap = 0;
            $oIngreso->ingreso_detex = '';
            $oIngreso->chequedetalle_id = $chequedetalle_id;

            $registro_cheque=$oIngreso->insertarChequeProceso();

            if($registro_cheque['estado'] || intval($registro_cheque['estado'])==1){ // si es true
              if($oIngreso->modificar_campo_registro(intval($registro_cheque['ingreso_id']))){
                $tipo = 0; //tipos: 0 no se muestra como mensaje, 1 se muestra como mensaje al iniciar sesión
                $det = 'Nuevo cheque ingresado <strong>'.$value['tb_ingreso_numdoc'].'</strong> con detalle <strong>'.$value['tb_ingreso_det'].'</strong> por el monto de <strong>'.floatval($value['tb_ingreso_imp']).'</strong>.';
                $oProceso->insertar_cronjob($det, 1, 11);//se mostrará como mensaje a los admin
              }
            }

          $i++;
          
          }

        }

      }

      if($i > 0){
        echo "Se encontraron un total de: ".$i." cheques sin desembolso.";
      }else{
        echo "No se encontraron cheques sin desembolso.";
      }

    }else{
      echo "No se encontraron cheques registrados el día de hoy.";
    }

  }else{
    $tipo = 0; //tipos: 0 no se muestra como mensaje
    $det = 'Cheques no detectados por ser DOMINGO';
    $oProceso->insertar_cronjob($det, 1, $user_id);
  }
}else{
  $tipo = 0; //tipos: 0 no se muestra como mensaje
  $det = 'Cheques no detectados por ser FERIADO';
  $oProceso->insertar_cronjob($det, 1, $user_id);
}

