<?php
require_once ('../../core/usuario_sesion.php');

require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once('../cuenta/Cuenta.class.php');
$oCuenta = new Cuenta();
require_once('../subcuenta/Subcuenta.php');
$oSucuenta = new Subcuenta();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$result = $oSucuenta->listar_subcuentas(19);

$empresa_id=intval($_POST['empresa_id']); 
    $fecha1 = fecha_mysql($_POST['fecha1']);
    $fecha2 = fecha_mysql($_POST['fecha2']);
    $usuario_id=intval($_POST['usuario_id']);
    $moneda_id=intval($_POST['moneda_id']);
    
    
    if($moneda_id==1){
        $moneda="S./ ";
    }
    if($moneda_id==2){
        $moneda="USS./ ";
    }
    if($empresa_id==1){
        $empresa="SEDE BOULEVARD";
    }
    if($empresa_id==2){
        $empresa="SEDE MALL AVENTURA";
    }

if ($result['estado'] == 1) {
    
    foreach ($result['data']as $key=>$value){
        $results = $oEgreso->total_desembolsos_fecha($fecha1, $fecha2, $usuario_id, $empresa_id,$moneda_id,19,$value['tb_subcuenta_id']);
            if($results['estado'] == 1)
              $total_desembolso = floatval($results['data']['total_desembolso']);
              
          $result = NULL;
        
        $tr.='<tr style="height: 25px">
                <td id="tabla_fila" style="font-family:cambria;font-weight: bold"> &nbsp;'.$value['tb_subcuenta_des'].'</td>
                <td id="tabla_fila" align="center">'.$moneda.' '.mostrar_moneda($total_desembolso).'</td>
            </tr>';
        $total_desembolsos+= $total_desembolso;
    }
    
     $tr.='<tr style="height: 25px">
                <td id="tabla_fila" align="center" style="font-family:cambria;font-weight: bold;"> &nbsp;TOTAL DE CRÉDITOS DESEMBOLSADOS</td>
                <td id="tabla_fila" align="center"><b>'.$moneda.' '.mostrar_moneda($total_desembolsos).'</b></td>
            </tr>';
}
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_flujo_desembolso" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria"><b>MONTO DESEMBOLSADO POR TIPO CRÉDITO <?php echo $empresa?><br> DE: <?php echo mostrar_fecha($fecha1)?> HASTA  <?php echo mostrar_fecha($fecha2)?></b></h4>
            </div>
            <form id="form_area" method="post">

                <div class="modal-body">

                    <div class="box box-primary">
                        <div class="box-body">
                             <table style="width: 100%" class="table-hover">
                                <?php                                 
                                    echo $tr;
                                ?>
                            </table>
                        </div>
                    </div>        

                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>