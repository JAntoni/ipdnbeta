<?php
session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../../libreriasphp/phpexcel/Classes/PHPExcel.php';

require_once ("../../config/Cado.php");
require_once ("../ingreso/cIngreso.php");
$oIngreso = new cIngreso();
require_once ("../egreso/cEgreso.php");
$oEgreso = new cEgreso();
require_once("../cuentas/cCuentas.php");
$oCuentas = new cCuentas();
require_once ("../formatos/formato.php");

//
$nombre_archivo="Reporte Consulta - Caja";

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("srjuan54@gmail.com")
							 ->setLastModifiedBy("srjuan54@gmail.com")
							 ->setTitle("Reporte")
							 ->setSubject("Reporte")
							 ->setDescription("Reporte generado por srjuan54@gmail.com")
							 ->setKeywords("")
							 ->setCategory("reporte excel");


$estiloTituloColumnas = array(
    'font' => array(
        'name'  => 'Arial',
        'bold'  => true,
        'size'  =>8,
        'color' => array(
            'rgb' => '000000'
        )
    ),
    'fill' => array(
        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array(
            'rgb' => 'FAFAFA')
    ),
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN ,
            'color' => array(
                'rgb' => '143860'
            )
        )
    ),
    'alignment' =>  array(
        'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'      => TRUE
    )
);

$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//establecer impresion a pagina completa
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

//establecer margenes
// $margin = 0.5 / 2.54; // 0.5 centimetros
// $marginBottom = 1.2 / 2.54; //1.2 centimetros
// $objPHPExcel->getActiveSheet()->getPageMargins()->setTop($margin);
// $objPHPExcel->getActiveSheet()->getPageMargins()->setBottom($marginBottom);
// $objPHPExcel->getActiveSheet()->getPageMargins()->setLeft($margin);
// $objPHPExcel->getActiveSheet()->getPageMargins()->setRight($margin);
//fin: establecer margenes
 
//incluir una imagen
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setPath('../../images/iconos/logopres.jpg'); //ruta
$objDrawing->setHeight(80); //altura
$objDrawing->setCoordinates('E1');
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet()); //incluir la imagen
//fin: incluir una imagen



$c=1;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","EMPRESA:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto = "PRESTAMOS DEL NORTE";
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=2;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","REPORTE:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto = "OPERACIONES DE CAJA";
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=3;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","MONEDA:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto = "SOLES";
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=4;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","FECHA:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto =$_POST['txt_fil_caj_fec1']." AL ".$_POST['txt_fil_caj_fec2'];
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=5;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","GENERACIÓN:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto=date("d-m-Y H:i:s");
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);



$c=6;
$objPHPExcel->getActiveSheet()->mergeCells("A$c:D$c");


$c=$c+1;
$titulo = "INGRESOS";
$objPHPExcel->getActiveSheet()->mergeCells("A$c:E$c");
 
$objPHPExcel->getActiveSheet()->setCellValue("A$c",$titulo);

$ven_est='CANCELADA';
$moneda_id=1;

$dts1=$oIngreso->mostrar_filtro($_SESSION['empresa_id'],$_POST['cmb_fil_caj_id'],fecha_mysql($_POST['txt_fil_caj_fec1']),fecha_mysql($_POST['txt_fil_caj_fec2']),$_POST['cmb_fil_cue_id'],$_POST['cmb_fil_subcue_id'],$_POST['cmb_fil_doc_id'],$_POST['txt_fil_ing_numdoc'],$_POST['hdd_fil_cli_id'],$_POST['cmb_fil_ing_est'],$moneda_id);

$num_rows= mysql_num_rows($dts1);

$c=$c+1;

$titulosColumnas = array(
    'CAJA',
    'FECHA',
    'DOCUMENTO',
    'CLIENTE',
    'DETALLE',
    'CUENTA',
    'SUB CUENTA',
    'IMPORTE',
    'ESTADO',
    'RESPONSABLE',
    'ID'
    );

$objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
    ->setCellValue('I'.$c,  $titulosColumnas[8])
    ->setCellValue('J'.$c,  $titulosColumnas[9])
    ->setCellValue('K'.$c,  $titulosColumnas[10])
    ;

$objPHPExcel->getActiveSheet()->getStyle("A$c:K$c")->applyFromArray($estiloTituloColumnas);

$sum_ing=0;
if($num_rows>0)
{
    $c=$c+1;
	$i=0;
	
	while($dt1 = mysql_fetch_array($dts1))
	{
	  $i++;

        $fecha=str_replace("-", "/", mostrarFecha($dt1['tb_ingreso_fec']));
        $documento=$dt1['tb_documento_abr'].' '.$dt1['tb_ingreso_numdoc'];
	  	$cliente=trim($dt1['tb_cliente_nom'])." (".trim($dt1['tb_cliente_doc'].")");
	    $importe    =formato_numero($dt1['tb_ingreso_imp'],2,'');
        $usuario=trim($dt1['tb_usuario_nom']);
        $caja_nom = $dt1['tb_caja_nom'];

        if($dt1['tb_ingreso_est']==1)$estado='CANCELADO';
        if($dt1['tb_ingreso_est']==2)$estado='EMITIDO';

        
        $sum_ing+=$dt1['tb_ingreso_imp'];

	    $objPHPExcel->getActiveSheet(0)
	        ->setCellValue('A'.$c, $caja_nom)
		    ->setCellValue('B'.$c, $fecha)
		    ->setCellValue('C'.$c, $documento)
            ->setCellValue('D'.$c, $cliente)
            ->setCellValue('E'.$c, $dt1['tb_ingreso_det'])
            ->setCellValue('F'.$c, $dt1['tb_cuenta_des'])
            ->setCellValue('G'.$c, $dt1['tb_subcuenta_des'])
            ->setCellValue('H'.$c, $importe)
            ->setCellValue('I'.$c, $estado)
            ->setCellValue('J'.$c, $usuario)
            ->setCellValue('K'.$c, $dt1['tb_ingreso_id'])
	         ;

	    $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

	    $c++;
	}
}  
mysql_free_result($dts1);

$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","OPERACIONES:");
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$num_rows);
$objPHPExcel->getActiveSheet()->setCellValue("G$c","TOTAL:");
$objPHPExcel->getActiveSheet()->setCellValue("H$c",$sum_ing);
$objPHPExcel->getActiveSheet()->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

$c=$c+1;//espacio

$c=$c+1;
$titulo = "EGRESOS";
$objPHPExcel->getActiveSheet()->mergeCells("A$c:E$c");
 
$objPHPExcel->getActiveSheet()->setCellValue("A$c",$titulo);

$ven_est='CANCELADA';

$dts1=$oEgreso->mostrar_filtro($_SESSION['empresa_id'],$_POST['cmb_fil_caj_id'],fecha_mysql($_POST['txt_fil_caj_fec1']),fecha_mysql($_POST['txt_fil_caj_fec2']),$_POST['cmb_fil_cue_id'],$_POST['cmb_fil_subcue_id'],$_POST['cmb_fil_doc_id'],$_POST['txt_fil_egr_numdoc'],$_POST['hdd_fil_pro_id'],$_POST['cmb_fil_egr_est'],$moneda_id);
$num_rows= mysql_num_rows($dts1);

$c=$c+1;

$titulosColumnas = array(
    'CAJA',
    'FECHA',
    'DOCUMENTO',
    'ANEXO',
    'DETALLE',
    'CUENTA',
    'SUB CUENTA',
    'IMPORTE',
    'ESTADO',
    'RESPONSABLE',
    'ID'
    );

$objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
    ->setCellValue('I'.$c,  $titulosColumnas[8])
    ->setCellValue('J'.$c,  $titulosColumnas[9])
    ->setCellValue('K'.$c,  $titulosColumnas[10])
    ;

$objPHPExcel->getActiveSheet()->getStyle("A$c:K$c")->applyFromArray($estiloTituloColumnas);

$sum_egr=0;
if($num_rows>0)
{
    $c=$c+1;
    $i=0;
    
    while($dt1 = mysql_fetch_array($dts1))
    {
      $i++;

        $fecha=str_replace("-", "/", mostrarFecha($dt1['tb_egreso_fec']));
        $documento=$dt1['tb_documento_abr'].' '.$dt1['tb_egreso_numdoc'];
        $cliente=trim($dt1['tb_proveedor_nom'])." (".trim($dt1['tb_proveedor_doc'].")");
        $importe    =formato_numero($dt1['tb_egreso_imp'],2,'');
        $usuario=trim($dt1['tb_usuario_nom']);
        $caja_nom = $dt1['tb_caja_nom'];

        if($dt1['tb_egreso_est']==1)$estado='CANCELADO';
        if($dt1['tb_egreso_est']==2)$estado='EMITIDO';

        
        $sum_egr+=$dt1['tb_egreso_imp'];

        $objPHPExcel->getActiveSheet(0)
            ->setCellValue('A'.$c, $caja_nom)
            ->setCellValue('B'.$c, $fecha)
            ->setCellValue('C'.$c, $documento)
            ->setCellValue('D'.$c, $cliente)
            ->setCellValue('E'.$c, $dt1['tb_egreso_det'])
            ->setCellValue('F'.$c, $dt1['tb_cuenta_des'])
            ->setCellValue('G'.$c, $dt1['tb_subcuenta_des'])
            ->setCellValue('H'.$c, $importe)
            ->setCellValue('I'.$c, $estado)
            ->setCellValue('J'.$c, $usuario)
            ->setCellValue('K'.$c, $dt1['tb_egreso_id'])
             ;

        $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

        $c++;
    }
}  
mysql_free_result($dts1);


$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","OPERACIONES:");
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$num_rows);
$objPHPExcel->getActiveSheet()->setCellValue("G$c","TOTAL:");
$objPHPExcel->getActiveSheet()->setCellValue("H$c",$sum_egr);
$objPHPExcel->getActiveSheet()->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);


//$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setAutoSize(10);

//$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setAutoSize(true); 
//$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setAutoSize(true); 
for($z = 'A'; $z <= 'K'; $z++){
    $objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
}

$c=$c+3;
$objPHPExcel->getActiveSheet()->setCellValue("C$c","TOTAL INGRESOS:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c",$sum_ing);
$objPHPExcel->getActiveSheet()->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("C$c","TOTAL EGRESOS:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c",$sum_egr);
$objPHPExcel->getActiveSheet()->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

$total=0;
$total=$sum_ing-$sum_egr;
$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("C$c","CAJA A REPORTAR:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c",$total);
$objPHPExcel->getActiveSheet()->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

$c=$c+3;
$objPHPExcel->getActiveSheet()->setCellValue("B$c","REPORTA:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c","RECIBE:");


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('CAJA - SOLES');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);


//NUEVA HOJA
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//establecer impresion a pagina completa
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

//incluir una imagen
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setPath('../../images/iconos/logopres.jpg'); //ruta
$objDrawing->setHeight(80); //altura
$objDrawing->setCoordinates('E1');
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet()); //incluir la imagen
//fin: incluir una imagen



$c=1;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","EMPRESA:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto = "PRESTAMOS DEL NORTE";
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=2;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","REPORTE:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto = "OPERACIONES DE CAJA";
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=3;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","MONEDA:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto = "DOLARES";
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=4;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","FECHA:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto =$_POST['txt_fil_caj_fec1']." AL ".$_POST['txt_fil_caj_fec2'];
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);

$c=5;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","GENERACIÓN:");
$objPHPExcel->getActiveSheet()->mergeCells("B$c:D$c");
$texto=date("d-m-Y H:i:s");
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$texto);



$c=6;
$objPHPExcel->getActiveSheet()->mergeCells("A$c:E$c");


$c=$c+1;
$titulo = "INGRESOS";
$objPHPExcel->getActiveSheet()->mergeCells("A$c:D$c");
 
$objPHPExcel->getActiveSheet()->setCellValue("A$c",$titulo);

$ven_est='CANCELADA';
$moneda_id=2;

$dts1=$oIngreso->mostrar_filtro($_SESSION['empresa_id'],$_POST['cmb_fil_caj_id'],fecha_mysql($_POST['txt_fil_caj_fec1']),fecha_mysql($_POST['txt_fil_caj_fec2']),$_POST['cmb_fil_cue_id'],$_POST['cmb_fil_subcue_id'],$_POST['cmb_fil_doc_id'],$_POST['txt_fil_ing_numdoc'],$_POST['hdd_fil_cli_id'],$_POST['cmb_fil_ing_est'],$moneda_id);

$num_rows= mysql_num_rows($dts1);

$c=$c+1;

$titulosColumnas = array(
    'CAJA',
    'FECHA',
    'DOCUMENTO',
    'CLIENTE',
    'DETALLE',
    'CUENTA',
    'SUB CUENTA',
    'IMPORTE',
    'ESTADO',
    'RESPONSABLE',
    'ID'
    );

$objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
    ->setCellValue('I'.$c,  $titulosColumnas[8])
    ->setCellValue('J'.$c,  $titulosColumnas[9])
    ->setCellValue('K'.$c,  $titulosColumnas[10])
    ;

$objPHPExcel->getActiveSheet()->getStyle("A$c:K$c")->applyFromArray($estiloTituloColumnas);

$sum_ing=0;
if($num_rows>0)
{
    $c=$c+1;
    $i=0;
    
    while($dt1 = mysql_fetch_array($dts1))
    {
      $i++;

        $fecha=str_replace("-", "/", mostrarFecha($dt1['tb_ingreso_fec']));
        $documento=$dt1['tb_documento_abr'].' '.$dt1['tb_ingreso_numdoc'];
        $cliente=trim($dt1['tb_cliente_nom'])." (".trim($dt1['tb_cliente_doc'].")");
        $importe    =formato_numero($dt1['tb_ingreso_imp'],2,'');
        $usuario=trim($dt1['tb_usuario_nom']);
        $caja_nom = $dt1['tb_caja_nom'];

        if($dt1['tb_ingreso_est']==1)$estado='CANCELADO';
        if($dt1['tb_ingreso_est']==2)$estado='EMITIDO';

        
        $sum_ing+=$dt1['tb_ingreso_imp'];

        $objPHPExcel->getActiveSheet(0)
            ->setCellValue('A'.$c, $caja_nom)
            ->setCellValue('B'.$c, $fecha)
            ->setCellValue('C'.$c, $documento)
            ->setCellValue('D'.$c, $cliente)
            ->setCellValue('E'.$c, $dt1['tb_ingreso_det'])
            ->setCellValue('F'.$c, $dt1['tb_cuenta_des'])
            ->setCellValue('G'.$c, $dt1['tb_subcuenta_des'])
            ->setCellValue('H'.$c, $importe)
            ->setCellValue('I'.$c, $estado)
            ->setCellValue('J'.$c, $usuario)
            ->setCellValue('K'.$c, $dt1['tb_ingreso_id'])
             ;

        $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

        $c++;
    }
}  
mysql_free_result($dts1);

$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","OPERACIONES:");
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$num_rows);
$objPHPExcel->getActiveSheet()->setCellValue("G$c","TOTAL:");
$objPHPExcel->getActiveSheet()->setCellValue("H$c",$sum_ing);
$objPHPExcel->getActiveSheet()->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

$c=$c+1;//espacio

$c=$c+1;
$titulo = "EGRESOS";
$objPHPExcel->getActiveSheet()->mergeCells("A$c:E$c");
 
$objPHPExcel->getActiveSheet()->setCellValue("A$c",$titulo);

$ven_est='CANCELADA';

$dts1=$oEgreso->mostrar_filtro($_SESSION['empresa_id'],$_POST['cmb_fil_caj_id'],fecha_mysql($_POST['txt_fil_caj_fec1']),fecha_mysql($_POST['txt_fil_caj_fec2']),$_POST['cmb_fil_cue_id'],$_POST['cmb_fil_subcue_id'],$_POST['cmb_fil_doc_id'],$_POST['txt_fil_egr_numdoc'],$_POST['hdd_fil_pro_id'],$_POST['cmb_fil_egr_est'],$moneda_id);
$num_rows= mysql_num_rows($dts1);

$c=$c+1;

$titulosColumnas = array(
    'CAJA',
    'FECHA',
    'DOCUMENTO',
    'ANEXO',
    'DETALLE',
    'CUENTA',
    'SUB CUENTA',
    'IMPORTE',
    'ESTADO',
    'RESPONSABLE',
    'ID'
    );

$objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
    ->setCellValue('I'.$c,  $titulosColumnas[8])
    ->setCellValue('J'.$c,  $titulosColumnas[9])
    ->setCellValue('K'.$c,  $titulosColumnas[10])
    ;

$objPHPExcel->getActiveSheet()->getStyle("A$c:K$c")->applyFromArray($estiloTituloColumnas);

$sum_egr=0;
if($num_rows>0)
{
    $c=$c+1;
    $i=0;
    
    while($dt1 = mysql_fetch_array($dts1))
    {
      $i++;

        $fecha=str_replace("-", "/", mostrarFecha($dt1['tb_egreso_fec']));
        $documento=$dt1['tb_documento_abr'].' '.$dt1['tb_egreso_numdoc'];
        $cliente=trim($dt1['tb_proveedor_nom'])." (".trim($dt1['tb_proveedor_doc'].")");
        $importe    =formato_numero($dt1['tb_egreso_imp'],2,'');
        $usuario=trim($dt1['tb_usuario_nom']);
        $caja_nom = $dt1['tb_caja_nom'];
        
        if($dt1['tb_egreso_est']==1)$estado='CANCELADO';
        if($dt1['tb_egreso_est']==2)$estado='EMITIDO';

        
        $sum_egr+=$dt1['tb_egreso_imp'];

        $objPHPExcel->getActiveSheet(0)
            ->setCellValue('A'.$c, $caja_nom)
            ->setCellValue('B'.$c, $fecha)
            ->setCellValue('C'.$c, $documento)
            ->setCellValue('D'.$c, $cliente)
            ->setCellValue('E'.$c, $dt1['tb_egreso_det'])
            ->setCellValue('F'.$c, $dt1['tb_cuenta_des'])
            ->setCellValue('G'.$c, $dt1['tb_subcuenta_des'])
            ->setCellValue('H'.$c, $importe)
            ->setCellValue('I'.$c, $estado)
            ->setCellValue('J'.$c, $usuario)
            ->setCellValue('K'.$c, $dt1['tb_egreso_id'])
             ;

        $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

        $c++;
    }
}  
mysql_free_result($dts1);


$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("A$c","OPERACIONES:");
$objPHPExcel->getActiveSheet()->setCellValue("B$c",$num_rows);
$objPHPExcel->getActiveSheet()->setCellValue("G$c","TOTAL:");
$objPHPExcel->getActiveSheet()->setCellValue("H$c",$sum_egr);
$objPHPExcel->getActiveSheet()->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);


//$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setAutoSize(10);

//$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setAutoSize(true); 
//$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setAutoSize(true); 
for($z = 'A'; $z <= 'K'; $z++){
    $objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
}

$c=$c+3;
$objPHPExcel->getActiveSheet()->setCellValue("C$c","TOTAL INGRESOS:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c",$sum_ing);
$objPHPExcel->getActiveSheet()->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("C$c","TOTAL EGRESOS:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c",$sum_egr);
$objPHPExcel->getActiveSheet()->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

$total=0;
$total=$sum_ing-$sum_egr;
$c=$c+1;
$objPHPExcel->getActiveSheet()->setCellValue("C$c","CAJA A REPORTAR:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c",$total);
$objPHPExcel->getActiveSheet()->getStyle('D'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

$c=$c+3;
$objPHPExcel->getActiveSheet()->setCellValue("B$c","REPORTA:");
$objPHPExcel->getActiveSheet()->setCellValue("D$c","RECIBE:");

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('CAJA - DOLARES');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);



// Redirect output to a client’s web browser (Excel5)
//header('Content-Type: application/vnd.ms-excel');
//header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.xls"');

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;