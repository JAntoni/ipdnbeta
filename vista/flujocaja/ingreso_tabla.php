<?php
if (defined('APP_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
  require_once(VISTA_URL . 'funciones/funciones.php');
  require_once(VISTA_URL . 'funciones/fechas.php');
} else {
  require_once('../../core/usuario_sesion.php');
  require_once('../funciones/funciones.php');
  require_once('../funciones/fechas.php');
}
require_once('../ingreso/Ingreso.class.php');
$oIngreso = new Ingreso();
require_once('../upload/Upload.class.php');
$oUpload = new Upload();

$fec1 = fecha_mysql($_POST['txt_fil_fec1']);
$fec2 = fecha_mysql($_POST['txt_fil_fec2']);

$emp_id = $_SESSION['empresa_id'];
$emp_id = intval($_POST['cmb_fil_empresa_id']);
$caj_id = intval($_POST['cmb_fil_caj_id']);
$mon_id = intval($_POST['cmb_moneda_id']);
$cli_id = intval($_POST['hdd_fil_cli_id']);
$usuario_id = intval($_POST['cmb_fil_usuario_id']);
$est = 0;
$doc_id = 0;
$numdoc = 0;
$cue_id = 0;
$subcue_id = 0;
$usuariogrupo_id = intval($_SESSION['usuariogrupo_id']);
$usuario_sesion_id = intval($_SESSION['usuario_id']);
$fecha_hoy = date('Y-m-d');

if (empty($emp_id)) {
  $emp_id = intval($_SESSION['empresa_id']);
}

if ($mon_id == 1) {
  $moneda = "S./ ";
}
if ($mon_id == 2) {
  $moneda = "US$./ ";
}

$tipo = "";
$estado = "";
$total_ingresos = 0;

if(($usuariogrupo_id != 2 && $usuariogrupo_id != 6) && $fec1 != $fecha_hoy){
  $result['estado'] = 0;
  $result['mensaje'] = 'No hay Ingresos | no puedes consultar ingresos de fechas anteriores, anterior: '.$fec1.', fecha hoy: '.$fecha_hoy.', USUARIO GRUPO ID: '.$usuariogrupo_id;
}
else
  $result = $oIngreso->listar_ingresos($fec1, $fec2, $emp_id, $caj_id, $mon_id, $cli_id, $est, $doc_id, $numdoc, $cue_id, $subcue_id, $usuario_id);

$tr = '';
if ($result['estado'] == 1) {

  foreach ($result['data'] as $key => $value) {
    $total_ingresos += floatval($value['tb_ingreso_imp']);
    if ($value['tb_ingreso_est'] == 1)
      $estado = "CANCELADO";
    if ($value['tb_ingreso_est'] == 2)
      $estado = "EMITIDO";
    if ($value['tb_ingreso_est'] == 0 || $value['tb_ingreso_est'] > 2)
      $estado = "";

    $subcuenta_alert = '';
    if (intval($value['tb_subcuenta_id']) == 17) { //solo deseo saber el N° 17, que es la sub de remate c-menor
      $result2 = $oUpload->mostrarUno_modulo($value['tb_ingreso_id'], 'ingreso');
      if ($result2['estado'] <= 0) //significa que no tiene ninguna foto subida
        $subcuenta_alert = '<span class="badge bg-red">Debes subir foto de DNI y Voucher de Venta</span>';
      $result2 = NULL;
    }

    $tr .= '<tr  style="font-family: cambria;font-size: 12px;border-color:#135896;">';
    $tr .= '
            <td align="center" style="border: 1px solid #135896;">' . mostrar_fecha($value['tb_ingreso_fec']) . '</td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_ingreso_id'] . '</td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_documento_abr'] . ' ' . $value['tb_ingreso_numdoc'] . '</a></td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_cliente_nom'] . '</td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_ingreso_det'] . $subcuenta_alert . '</td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_cuenta_des'] . '</td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_subcuenta_des'] . '</td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_ingreso_imp'] . '</td>
            <td align="center" style="border: 1px solid #135896;">' . $value['tb_caja_nom'] . '</td>
            <td align="center" style="border: 1px solid #135896;font-size: 10px">' . $value['tb_usuario_nom'] . '<br>' . mostrar_fecha_hora($value['tb_ingreso_fecreg']) . '</td>
            
            <td align="center" style="border: 1px solid #135896;">';
    if ($value['tb_modulo_id'] == 0 && $value['tb_ingreso_fec'] == $fecha_hoy) {
      $tr .= '
            <a class="btn btn-warning btn-xs" title="Editar" onclick="ingreso_form(\'M\',' . $value['tb_ingreso_id'] . ')"><i class="fa fa-edit"></i></a>';
            //<a class="btn btn-danger btn-xs" title="Eliminar" onclick="ingreso_form(\'E\',' . $value['tb_ingreso_id'] . ')"><i class="fa fa-trash"></i></a>';
    }
    if (($usuario_sesion_id == 2 || $usuario_sesion_id == 11) && $value['tb_ingreso_fec'] == $fecha_hoy) {
      $tr .= '
            <span class="badge bg-red">Solo Gerencia</span>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="ingreso_form(\'E\',' . $value['tb_ingreso_id'] . ')"><i class="fa fa-trash"></i></a>';
    }

    if ($usuariogrupo_id == 2 && $value['tb_ingreso_fec'] == $fecha_hoy) {
      $tr .= '
        <a class="btn btn-info btn-xs" title="Cambiar" onclick="cambio_sede(\'ingreso\',' . $value['tb_ingreso_id'] . ', ' . $value['tb_empresa_id'] . ')"><i class="fa fa-exchange"></i></a>';
    }

    $tr .= ' <a class="btn btn-success btn-xs" title="Subir Imagen" onclick="upload_form(\'I\', ' . $value['tb_ingreso_id'] . ')"><i class="fa fa-cloud-upload"></i></a>
            <a class="btn btn-facebook btn-xs" title="Ver Imagen" onclick="upload_galeria_simple(' . $value['tb_ingreso_id'] . ')"><i class="fa fa-picture-o"></i></a>';
    $tr .= '</td>';

    $tr .= '</tr>';
  }
  $result = null;
} else {
  $tr = '<td colspan="4">' . $result['mensaje'] . '</td>';
  $result = null;
}
?>
<table id="tbl_ingresos" class="table table-hover">
  <thead>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <th style="text-align: center;border: 1px solid #FFFFFF;">FECHA</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;">ID</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;" class="no-sort">DOCUMENTO</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;">CLIENTE</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;" class="no-sort">DETALLE</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;" class="no-sort">CUENTA</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;" class="no-sort">SUBCUENTA</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;">IMPORTE</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;" class="no-sort">CAJA</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;" class="no-sort">USUARIO</th>
      <th style="text-align: center;border: 1px solid #FFFFFF;" class="no-sort"></th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr; ?>
  </tbody>
  <tfoot>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <td colspan="7" style="text-align: center;border: 1px solid #FFFFFF;">TOTAL DE INGRESOS </td>
      <td style="text-align: center;border: 1px solid #FFFFFF;"><?php echo $moneda . ' ' . mostrar_moneda($total_ingresos); ?></td>
      <td colspan="3" style="text-align: center;border: 1px solid #FFFFFF;"></td>
    </tr>
  </tfoot>
</table>