var datatable_global_ingreso;
var datatable_global_egreso;

function ingreso_form(usuario_act, ingreso_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "ingreso/ingreso_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      ingreso_id: ingreso_id,
      vista: "ingreso",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_ingreso_form").html(data);
        $("#modal_registro_ingreso").modal("show");
        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_ingreso"); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_ingreso"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_ingreso", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "ingreso";
        var div = "div_modal_ingreso_form";
        permiso_solicitud(usuario_act, ingreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
    },
  });
}

function egreso_form(usuario_act, egreso_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "egreso/egreso_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      egreso_id: egreso_id,
      vista: "egreso",
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_egreso_form").html(data);
        $("#modal_registro_egreso").modal("show");

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == "L" || usuario_act == "E")
          form_desabilitar_elementos("form_egreso"); //funcion encontrada en public/js/generales.js

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_egreso"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_egreso", "limpiar"); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = "egreso";
        var div = "div_modal_egreso_form";
        permiso_solicitud(usuario_act, egreso_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
    },
  });
}

function ingreso_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "flujocaja/ingreso_tabla.php",
    async: true,
    dataType: "html",
    data: $("#flujocaja_filtro").serialize(),
    beforeSend: function () {
      $("#ingreso_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_ingreso_tabla").html(data);
      $("#ingreso_mensaje_tbl").hide(300);

      let registros = data.includes("No hay");
      console.log(registros);
      if (!registros) estilos_datatable_ingreso();
    },
    complete: function (data) {},
    error: function (data) {
      $("#ingreso_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CRÉDITO: " + data.responseText
      );
      console.log("Error al ejecutar la tabla de Ingreso");
      console.log(data);
    },
  });
}

function estilos_datatable_ingreso() {
  datatable_global_ingreso = $("#tbl_ingresos").DataTable({
    pageLength: 100,
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsquedad",
      info: "Mostrado _END_ registros",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar",
    },
    order: [],
    columnDefs: [{ targets: "no-sort", orderable: false }],
  });
  datatable_texto_filtrar_ingreso();
}
function datatable_texto_filtrar_ingreso() {
  $(".dataTables_filter input").attr("id", "txt_datatable_fil");

  $("#txt_datatable_fil").keyup(function (event) {
    $("#hdd_datatable_fil").val(this.value);
  });

  var text_fil = $("#hdd_datatable_fil").val();
  if (text_fil) {
    $("#txt_datatable_fil").val(text_fil);
    datatable_global_ingreso.search(text_fil).draw();
  }
}

//--------------------------- TABLA EGRESOOO --------------------//
function egreso_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "flujocaja/egreso_tabla.php",
    async: true,
    dataType: "html",
    data: $("#flujocaja_filtro").serialize(),
    beforeSend: function () {
      $("#egreso_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_egreso_tabla").html(data);
      $("#egreso_mensaje_tbl").hide(300);

      let registros = data.includes("No hay");
      if (!registros) estilos_datatable_egreso();
    },
    complete: function (data) {},
    error: function (data) {
      $("#egreso_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CRÉDITO: " + data.responseText
      );
      console.log("Error al ejecutar la tabla de Egreso");
      console.log(data);
    },
  });
}
function estilos_datatable_egreso() {
  datatable_global_egreso = $("#tbl_egresos").DataTable({
    pageLength: 100,
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsquedad",
      info: "Mostrado _END_ registros",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar",
    },
    order: [],
    columnDefs: [{ targets: "no-sort", orderable: false }],
  });
  datatable_texto_filtrar_egreso();
}
function datatable_texto_filtrar_egreso() {
  $(".dataTables_filter input").attr("id", "txt_datatable_fil");

  $("#txt_datatable_fil").keyup(function (event) {
    $("#hdd_datatable_fil").val(this.value);
  });

  var text_fil = $("#hdd_datatable_fil").val();
  if (text_fil) {
    $("#txt_datatable_fil").val(text_fil);
    datatable_global_egreso.search(text_fil).draw();
  }
}

function resumen_caja() {
  //SE HIZO UN CAMBIO, AHORA LOS MONTOS A REPORTAR EN FLUJO CAJA SERÁN DESDE EL 1 DE AGOSTO HASTA HOY
  //YA NO SERÁ CON LAS FECHAS SELECCIONADAS

  $.ajax({
    type: "POST",
    url: VISTA_URL + "flujocaja/flujocaja_controller.php",
    async: true,
    dataType: "JSON",
    data: {
      fecha1: $("#txt_fil_fec1").val(),
      fecha2: $("#txt_fil_fec2").val(),
      empresa_id: $("#cmb_fil_empresa_id").val(),
      usuario_id: $("#cmb_fil_usuario_id").val(),
      moneda_id: $("#cmb_moneda_id").val(),
      action: "resumen",
    },
    beforeSend: function () {},
    success: function (data) {
      console.log(data)
      if (parseInt(data.estado) > 0) {
        $("#h5_ingresos").text("S/. " + data.total_ingresos);
        $("#h5_egresos").text("S/. " + data.total_egresos);
        $("#h5_saldo").text("S/. " + data.saldo);
        $("#h5_cheque_soles").text("S/. " + data.saldo_cheques_soles);
        $("#h5_ingresos_dolares").text("US$. " + data.total_ingresos_dolares);
        $("#h5_egresos_dolares").text("US$. " + data.total_egresos_dolares);
        $("#h5_saldo_dolares").text("US$. " + data.saldo_dolares);
        $("#h5_cheque_dolares").text("US$. " + data.saldo_cheques_dolares);
        $("#h5_desmbolsos").text("S/. " + data.desembolso);
      }
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("ERROR", data.responseText);
    },
  });
}

function ejecutar_tablas() {
  console.log("Se están ejecutando las tablas");
  ingreso_tabla();
  egreso_tabla();
  resumen_caja();
}

function upload_form(usuario_act, upload_id) {
  var ramdom = Math.random() * (90000 - 2000) + 2000;
  $.ajax({
    type: "POST",
    url: VISTA_URL + "upload/upload_form.php",
    async: true,
    dataType: "html",
    data: {
      action: usuario_act, // PUEDE SER: L, I, M , E
      upload_id: 0,
      modulo_nom: "ingreso", //nombre de la tabla a relacionar
      modulo_id: upload_id, //aun no se guarda este modulo
      upload_uniq: ramdom, //ID temporal
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");
      if (data != "sin_datos") {
        $("#div_modal_upload_form").html(data);
        $("#modal_registro_upload").modal("show");

        //funcion js para agregar un largo automatico al modal, al abrirlo
        modal_height_auto("modal_registro_upload"); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal("modal_registro_upload", "limpiar"); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      swal_error("ERRROR", data.responseText, 5000);
    },
  });
}

function upload_galeria_simple(ingreso_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "upload/upload_galeria_simple.php",
    async: true,
    dataType: "html",
    data: {
      modulo_nom: "ingreso", //nombre de la tabla a relacionar
      modulo_id: ingreso_id,
    },
    beforeSend: function () {},
    success: function (html) {
      $("#div_modal_upload_galeria_simple").html(html);
      $("#modal_upload_simple").modal("show");
      modal_hidden_bs_modal("modal_upload_simple", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {},
    error: function (data) {
      alerta_error("Error", "ERRROR!:" + data.responseText); //en generales.js
      swal_error("ERRROR", data.responseText, 5000);
    },
  });
}

function caja_reporte(url) {
  var url = VISTA_URL + "flujocaja/caja_reporte_exc.php";
  $("#flujocaja_filtro").attr("action", url);
  $("#flujocaja_filtro").submit();
}

function cuotapago_banco() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "ingreso/ingreso_banco.php",
    async: true,
    dataType: "html",
    data: {
      credito: "cred",
      cuota_id: 0,
      vista: "vencimiento_tabla",
    },
    beforeSend: function () {},
    success: function (html) {
      //$('#modal_mensaje').modal('hide');
      $("#div_modal_vencimiento_menor_banco_form").html(html);
      $("#modal_registro_vencimientomenorbanco").modal("show");
      modal_height_auto("modal_registro_vencimientomenorbanco");
      modal_hidden_bs_modal("modal_registro_vencimientomenorbanco", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {
      //console.log(data);
    },
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
    },
  });
}

function usuario_select(empresa_id) {

  var usuariogrupo_id = parseInt($('#usuariogrupo_id').val()) || 0;

  if(usuariogrupo_id == 2 || usuariogrupo_id == 6){
    $.ajax({
      type: "POST",
      url: VISTA_URL + "usuario/usuario_select.php",
      async: false,
      dataType: "html",
      data: {
        usuario_columna: 'tb_usuario_ofi',
	      usuario_valor: 1, //para que solo muestre usuarios de oficina o que trabajaron en oficina
	      param_tip: 'INT',
        empresa_id: empresa_id
      },
      beforeSend: function () {
        $("#cmb_fil_usuario_id").html('<option value="">Cargando...</option>');
      },
      success: function (html) {
        $("#cmb_fil_usuario_id").html(html);
        $("#cmb_fil_usuario_id").selectpicker('refresh');
      },
      complete: function (html) {},
    });
  }
  else
    console.log('No tienes acceso a todos los usuarios, solo puedes ver el tuyo, grupo usuario: ' + usuariogrupo_id)
}

function caja_reporte() {
  var fecha1 = $("#txt_fil_fec1").val();
  var fecha2 = $("#txt_fil_fec2").val();
  var emp_id = $("#cmb_fil_empresa_id").val();
  var caj_id = $("#cmb_fil_caj_id").val();
  var mon_id = $("#cmb_moneda_id").val();
  var cli_id = $("#hdd_fil_cli_id").val();
  var usuario_id = $("#cmb_fil_usuario_id").val();
  //var est = $("#").val();
  //var doc_id = $("#").val();
  //var numdoc = $("#").val();
  //var cue_id = $("#").val();
  //var subcue_id = $("#").val();
  //  window.open("https://ipdsac.ipdnsac/ipdnsac/vista/flujocaja/reporte1_excel.php");
  window.open(
    "http://www.ipdnsac.com/ipdnsac/vista/flujocaja/reporte1_excel.php?fecha1=" +
      fecha1 +
      "&fecha2=" +
      fecha2 +
      "&emp_id=" +
      emp_id +
      "&caj_id=" +
      caj_id +
      "&mon_id=" +
      mon_id +
      "&cli_id=" +
      cli_id +
      "&usuario_id=" +
      usuario_id
  );
  //window.open("https://localhost/ipdnsac/vista/flujocaja/reporte1_excel.php?fecha1="+fecha1+"&fecha2="+fecha2+"&empre_id="+emp_id+"&caj_id="+caj_id+"&mon_id="+mon_id+"&cli_id="+cli_id+"&usuario_id="+usuario_id);
}

function flujodesembolso_form() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "flujocaja/flujoDesembolsos.php",
    async: true,
    dataType: "html",
    data: {
      fecha1: $("#txt_fil_fec1").val(),
      fecha2: $("#txt_fil_fec2").val(),
      empresa_id: $("#cmb_fil_empresa_id").val(),
      usuario_id: $("#cmb_fil_usuario_id").val(),
      moneda_id: $("#cmb_moneda_id").val(),
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (data) {
      $("#modal_mensaje").modal("hide");

      $("#div_modal_flujo_desembolso").html(data);
      $("#modal_flujo_desembolso").modal("show");
      modal_hidden_bs_modal("modal_flujo_desembolso", "limpiar"); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {},
    error: function (data) {
      $("#box_modal_mensaje").addClass("box-danger").removeClass("box-info");
      $("#overlay_modal_mensaje").removeClass("overlay").empty();
      $("#body_modal_mensaje").html("ERRROR!:" + data.responseText);
      console.log(data.responseText);
    },
  });
}

// ANTONIO 20-09-2023
function cambio_sede(tipo_operacion, operacion_id, empresa_id) {
  var empresa_nom = "Sede Boulevard";
  var empresa_cambio = "Sede Mall Aventura";
  

  if (empresa_id == 2) {
    empresa_nom = "Sede Mall Aventura";
    empresa_cambio = "Sede Boulevard";
  }

  var mensaje =
    "Está seguro de cambiar este INGRESO desde la " +
    empresa_nom +
    " hacia la " +
    empresa_cambio +
    "?";

  if (tipo_operacion == "egreso")
    mensaje =
      "Está seguro de cambiar este EGRESO desde la " +
      empresa_nom +
      " hacia la " +
      empresa_cambio +
      "?";

  Swal.fire({
    title: mensaje,
    icon: "info",
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText: '<i class="fa fa-exchange"></i> Acepto el Cambio',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-times"></i> Cancelar',
    confirmButtonColor: "#21ba45",
    denyButtonColor: "#dd4b39",
  }).then((result) => {
    if (result.isConfirmed) {
      
      $.ajax({
        type: "POST",
        url: VISTA_URL + "flujocaja/flujocaja_controller.php",
        async: true,
        dataType: "json",
        data: {
          action: 'cambio_sede',
          tipo_operacion: tipo_operacion,
          operacion_id: operacion_id,
          empresa_id: empresa_id
        },
        beforeSend: function () {
          
        },
        success: function (data) {
          if(parseInt(data.estado) > 0){
            notificacion_success(data.mensaje, 6000);
            ejecutar_tablas();
          }
          else{
            alerta_error('ERROR', 'REVISAR LA CONSOLA CON SISTEMAS');
            console.log(data)
          }
        },
        complete: function (data) {},
        error: function (data) {
          alerta_error('ERROR', 'REVISAR LA CONSOLA CON SISTEMAS');
          console.log(data)
        },
      });

    } else if (result.isDenied) {
      
    }
  });
}
$(document).ready(function () {
  console.log("Flujo caja al 09-08-2024 --");
  var empresa_id = $("#cmb_fil_empresa_id").val();
  usuario_select(empresa_id);

  $("#datetimepicker1").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    endDate: new Date(),
  });

  $("#datetimepicker2").datepicker({
    language: "es",
    autoclose: true,
    format: "dd-mm-yyyy",
    startDate: "-0d",
    endDate: new Date(),
  });

  $("#datetimepicker1").on("changeDate", function (e) {
    var startVal = $("#txt_fil_fec1").val();
    $("#datetimepicker2").data("datepicker").setStartDate(startVal);
    ejecutar_tablas();
  });

  $("#datetimepicker2").on("changeDate", function (e) {
    var endVal = $("#txt_fil_fec2").val();
    $("#datetimepicker1").data("datepicker").setEndDate(endVal);
    ejecutar_tablas();
  });

  $("#txt_fil_cli").autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "cliente/cliente_autocomplete.php",
        { term: request.term }, //
        response
      );
    },
    select: function (event, ui) {
      //console.log(ui);
      $("#hdd_fil_cli_id").val(ui.item.cliente_id);
      $("#txt_fil_cli").val(ui.item.cliente_nom);
      ejecutar_tablas();
      event.preventDefault();
      $("#txt_fil_cli").focus();
    },
  });

  $("#cmb_fil_caj_id").change(function () {
    ejecutar_tablas();
  });

  $("#cmb_moneda_id").change(function () {
    ejecutar_tablas();
  });

  $("#cmb_fil_usuario_id").change(function () {
    ejecutar_tablas();
  });

  $("#cmb_fil_empresa_id").change(function () {
    var empresa_id = $(this).val();
    usuario_select(empresa_id);
    ejecutar_tablas();
  });

  ejecutar_tablas();
});
