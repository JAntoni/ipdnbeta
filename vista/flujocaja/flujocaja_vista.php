<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
            <h1>
                <?php echo $menu_tit; ?>
                <small><?php echo $menu_des; ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Caja</a></li>
                <li class="active"><?php echo ucwords(mb_strtolower($menu_tit));?></li>
            </ol>
	</section>

	<!-- Main content -->
	<section class="content">

            <?php
                    //vamos a validar que la caja est茅 aperturada para poder hacer ingresos o egresos de todo tipo
                    require_once ("vista/funciones/funciones.php");
                    validar_apertura_caja();
                  ?>
		<div class="box box-primary">
                    <div class="box-header">
                        <button class="btn btn-primary btn-sm" onclick="ingreso_form('I',0)"><i class="fa fa-plus"></i> Nuevo Ingreso</button>
                        <button class="btn btn-success btn-sm" onclick="egreso_form('I',0)"><i class="fa fa-plus"></i> Nuevo Egreso</button>
                        <button class="btn btn-primary btn-sm" onclick=""><i class="fa fa-plus"></i> Cambio de Moneda</button>
                        <button class="btn btn-danger btn-sm" onclick="cuotapago_banco()"><i class="fa fa-plus"></i> Caja Banco</button>
                        <button class="btn btn-primary btn-sm" onclick=""><i class="fa fa-refresh"></i> Actualizar</button>
                        <button class="btn btn-danger btn-sm" onclick="caja_reporte()"><i class="fa fa-refresh"></i> Excel</button>
                        <button class="btn btn-primary btn-sm" onclick=""><i class="fa fa-refresh"></i> Imprimir</button>
                    </div>

			<div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="txt_filtro" class="control-label">Filtro de Caja</label>
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                          <?php include 'flujocaja_filtro.php';?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>
				<!--- MESAJES DE GUARDADO -->
                            <div class="callout callout-info" id="flujocaja_mensaje_tbl" style="display: none;">
                              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="progress-group" id="div_resumen_cobranza">
                                    </div>

                                    <!-- Input para guardar el valor ingresado en el search de la tabla-->
                                    <input type="hidden" id="hdd_datatable_fil">

                                      <!--- MESAJES DE GUARDADO -->
                                    <div class="callout callout-info" id="ingreso_mensaje_tbl" style="display: none;">
                                      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Ingresos...</h4>
                                    </div>
                                    <!-- TABLA DE INGRESOS -->
                                    <div class="box box-success">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Registro de Ingresos</h3>
                                                <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                            <div class="box-body no-padding">
                                                <div id="div_ingreso_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                                </div>
                                            </div>
                                    </div>

                                        <!--- MESAJES DE GUARDADO -->
                                    <div class="callout callout-info" id="egreso_mensaje_tbl" style="display: none;">
                                      <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos de Egresos...</h4>
                                    </div>
                                    
                                    <!-- TABLA DE EGRESOS -->
                                    <div class="box box-danger">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Registro de Egresos</h3>
                                            <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body no-padding">
                                            <div id="div_egreso_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- RESUMEN -->
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_ingresos"></h5>
                                                <span class="description-text">TOTAL INGRESOS SOLES</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-red"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_egresos"></h5>
                                                <span class="description-text">TOTAL EGRESOS SOLES</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_saldo"></h5>
                                                <span class="description-text">SALDO A REPORTAR SOLES </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_cheque_soles"></h5>
                                                <span class="description-text">CHEQUES SOLES ALMACENADOS</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_desmbolsos">0.00</h5>
                                                <span class="description-text" onclick="flujodesembolso_form()">CRÉDITOS/DESEMBOLSOS</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_gastos">0.00</h5>
                                                <span class="description-text">TOTAL GASTOS</span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <!-- RESUMEN  DOLARES -->
                                    <div class="row">
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_ingresos_dolares"></h5>
                                                <span class="description-text">TOTAL INGRESOS DÓLARES</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-red"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_egresos_dolares"></h5>
                                                <span class="description-text">TOTAL EGRESOS DÓLARES</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_saldo_dolares"></h5>
                                                <span class="description-text">SALDO A REPORTAR DÓLARES</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 col-md-2">
                                            <div class="description-block border-right">
                                                <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i></span>
                                                <h5 class="description-header" id="h5_cheque_dolares"></h5>
                                                <span class="description-text">CHEQUES DOLARES ALMACENADOS</span>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
			</div>
			<div id="div_modal_flujocaja_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE LA flujocaja-->
			</div>
			<div id="div_modal_flujo_desembolso">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE LA flujocaja-->
			</div>
			<div id="div_modal_prestamo_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE LA flujocaja-->
			</div>
			<div id="div_modal_comentario_timeline_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE LA flujocaja-->
			</div>
			<div id="div_modal_ingreso_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE LA ingreso-->
			</div>
			<div id="div_modal_egreso_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE LA egreso-->
			</div>
      <div id="div_modal_vencimiento_menor_banco_form">
          <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
      </div>
      <div id="div_modal_upload_form">
          <!-- PARA ABRIR EL MODAL SE SUBIR IMAGENES-->
      </div>
      <div id="div_modal_upload_galeria_simple">
          <!-- PARA PODER VER LAS IMAGENES SUBIDAS SIMPLE, UNA DE BAJO DE LA OTRA-->
      </div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL.'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>
