<?php
//echo 'hasta aki estoy entrando normal '; exit();
//require_once('../../core/usuario_sesion.php');
//error_reporting(0);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
	die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
//echo 'hasta aki estoy entrando normal ssssss'; exit();
require_once '../../static/libreriasphp/phpexcel/Classes/PHPExcel.php';


require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");



//$fecha1=(empty($_POST['txt_filtro_fec1']))? fecha_mysql($fecha1) : fecha_mysql($_POST['txt_filtro_fec1']);
//$fecha2=(empty($_POST['txt_filtro_fec2']))? fecha_mysql($fecha2) : fecha_mysql($_POST['txt_filtro_fec2']);
$fecha1=fecha_mysql($_GET['fecha1']);
$fecha2=fecha_mysql($_GET['fecha2']);
$emp_id = intval($_GET['emp_id']);
$caj_id =  intval($_GET['caj_id']);
$mon_id =  intval($_GET['mon_id']);
$cli_id = intval($_GET['cli_id']);
$usuario_id =  intval($_GET['usuario_id']);

//echo 'fecha 1= '.$fecha1.' fecha 2= '.$fecha2.' empresa 1='.$emp_id.' caja ='.$caj_id; exit();


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("cristhian.cyga@gmail.com")
	->setLastModifiedBy("cristhian.cyga@gmail.com")
	->setTitle("Reporte De Cierre de Caja")
	->setSubject("Flujo de Caja")
	->setDescription("Reporte generado por cristhian.cyga@gmail.com")
	->setKeywords("")
	->setCategory("reporte excel");

$estiloTituloColumnas = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => true,
    'size'  =>10,
    'color' => array(
      'rgb' => 'FFFFFF'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => '135896')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

$estiloTituloFilas = array(
  'font' => array(
    'name'  => 'cambria',
    'bold'  => false,
    'size'  =>10,
    'color' => array(
      'rgb' => '000000'
    )
  ),
  'fill' => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array(
      'rgb' => 'FAFAFA')
  ),
  'borders' => array(
    'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN ,
        'color' => array(
          'rgb' => '143860'
        )
      )
  ),
  'alignment' =>  array(
    'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    'wrap'      => TRUE
  )
);

  //$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
  $objPHPExcel->setActiveSheetIndex(0);

  $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
  //establecer impresion a pagina completa
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
  $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

  $nombre_archivo = "IPDNSAC - ".mostrar_fecha($fecha1);

  $c = 1;
  $titulo = "FORMATO DE APERTURA Y CIERRE DE CAJA";
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:D$c");
  $objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
  $objPHPExcel->getActiveSheet()->getStyle("A$c:D$c")->applyFromArray($estiloTituloColumnas);
  $titulo2 = mostrar_fecha($fecha1);
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:J$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", $titulo2);
  $objPHPExcel->getActiveSheet()->getStyle("E$c:J$c")->applyFromArray($estiloTituloColumnas);
//  $objPHPExcel->getActiveSheet()->setCellValue("D1", mostrar_fecha($fecha1));

  $c = 2;
//  $nombre_fecha = "FORMATO DE APERTURA Y CIERRE DE CAJA DE LA FECHA: ".mostrar_fecha($fecha1)." HASTA ".mostrar_fecha($fecha2);
//  $objPHPExcel->getActiveSheet()->mergeCells("A$c:J$c");
//  $objPHPExcel->getActiveSheet()->setCellValue("A$c", $nombre_fecha);

  $c = $c + 1;

  $titulosColumnas = array(
    'FECHA',
    'ID',
    'DOCUMENTO',
    'CLIENTE',
    'DETALLE',
    'CUENTA',
    'SUB CUENTA',
    'IMPORTE',
    'CAJA',
    'USUARIO'
  );

  $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
    ->setCellValue('I'.$c,  $titulosColumnas[8])
    ->setCellValue('J'.$c,  $titulosColumnas[9])
  ;

  $objPHPExcel->getActiveSheet()->getStyle("A$c:J$c")->applyFromArray($estiloTituloColumnas);

  $c = $c+1;
  
  $tipo = "";
$estado = "";
$total_ingresos = 0;
//echo 'sfahsahfsa == '.$result['estado'];          exit();
$result = $oIngreso->listar_ingresos($fecha1, $fecha2, $emp_id, $caj_id, $mon_id, $cli_id, $est, $doc_id, $numdoc, $cue_id, $subcue_id, $usuario_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
          
        if ($value['tb_ingreso_est'] == 1)
            $estado = "CANCELADO";
        if ($value['tb_ingreso_est'] == 2)
            $estado = "EMITIDO";
        if ($value['tb_ingreso_est'] == 0 || $value['tb_ingreso_est'] > 2)
            $estado = "";
        $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("F$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("I$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("J$c")->applyFromArray($estiloTituloFilas);
        
        $objPHPExcel->getActiveSheet()
          ->setCellValue('A'.$c, mostrar_fecha($value['tb_ingreso_fec']))
          ->setCellValue('B'.$c, $value['tb_ingreso_id'])
          ->setCellValue('C'.$c, $value['tb_documento_abr'] . ' ' . $value['tb_ingreso_numdoc'])
          ->setCellValue('D'.$c, $value['tb_cliente_nom'])
          ->setCellValue('E'.$c, $value['tb_ingreso_det'])
          ->setCellValue('F'.$c, $value['tb_cuenta_des'])
          ->setCellValue('G'.$c, $value['tb_subcuenta_des'])
          ->setCellValue('H'.$c, mostrar_moneda($value['tb_ingreso_imp']))
          ->setCellValue('I'.$c, $value['tb_caja_nom'])
          ->setCellValue('J'.$c, $value['tb_usuario_nom'] . '  '. mostrar_fecha_hora($value['tb_ingreso_fecreg']))
        ;
//        $objPHPExcel->getActiveSheet()->mergeCells("A$c:J$c");
//        $objPHPExcel->getActiveSheet()->getStyle('I'.$c)->getNumberFormat()->setFormatCode('00000000');
        $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        
        $SUMA_VENTAS += $value['tb_ingreso_imp'];
        $c++;
      }
    }
  $result = NULL;

//  $c=$c+1;
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:D$c");
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:G$c");
  $objPHPExcel->getActiveSheet()->mergeCells("I$c:J$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", 'TOTAL DE INGRESOS');
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", mostrar_moneda($SUMA_VENTAS));
  $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
  $objPHPExcel->getActiveSheet()->getStyle("A$c:D$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("E$c:G$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("I$c:J$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($estiloTituloColumnas);
  //FUNCIONA CUADO QUIERES MOSTRAR CEROS A LA IZQUIERDA: EJMP: 000001. 0000003
  //$objPHPExcel->getActiveSheet()->getStyle('K'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

  //for($z = 'A'; $z <= 'O'; $z++){
    //$objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
  //}
  
  
  $c=$c+4;
  
    $titulosColumnas = array(
    'FECHA',
    'ID',
    'DOCUMENTO',
    'ANEXO',
    'DETALLE',
    'CUENTA',
    'SUB CUENTA',
    'IMPORTE',
    'CAJA',
    'USUARIO'
  );

  $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$c,  $titulosColumnas[0])
    ->setCellValue('B'.$c,  $titulosColumnas[1])
    ->setCellValue('C'.$c,  $titulosColumnas[2])
    ->setCellValue('D'.$c,  $titulosColumnas[3])
    ->setCellValue('E'.$c,  $titulosColumnas[4])
    ->setCellValue('F'.$c,  $titulosColumnas[5])
    ->setCellValue('G'.$c,  $titulosColumnas[6])
    ->setCellValue('H'.$c,  $titulosColumnas[7])
    ->setCellValue('I'.$c,  $titulosColumnas[8])
    ->setCellValue('J'.$c,  $titulosColumnas[9])
  ;

  $objPHPExcel->getActiveSheet()->getStyle("A$c:J$c")->applyFromArray($estiloTituloColumnas);
  
  
  $c = $c+1;
  
  $tipo = "";
$estado = "";
$result = $oEgreso->listar_egresos($fecha1, $fecha2, $emp_id, $caj_id, $mon_id, $cli_id, $est, $doc_id, $numdoc, $cue_id, $subcue_id, $usuario_id);
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
          
        if($value['tb_egreso_est']==1)
                $estado="CANCELADO";
            if($value['tb_egreso_est']==2)
                $estado="EMITIDO";
            if($value['tb_egreso_est']==0 ||$value['tb_egreso_est']>2)
                $estado="";
            
        $objPHPExcel->getActiveSheet()->getStyle("A$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("D$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("E$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("F$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("G$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("I$c")->applyFromArray($estiloTituloFilas);
        $objPHPExcel->getActiveSheet()->getStyle("J$c")->applyFromArray($estiloTituloFilas);
        
        $objPHPExcel->getActiveSheet()
          ->setCellValue('A'.$c, mostrar_fecha($value['tb_egreso_fec']))
          ->setCellValue('B'.$c, $value['tb_egreso_id'])
          ->setCellValue('C'.$c, $value['tb_documento_abr'].' '.$value['tb_egreso_numdoc'])
          ->setCellValue('D'.$c, $value['tb_proveedor_nom'])
          ->setCellValue('E'.$c, $value['tb_egreso_det'])
          ->setCellValue('F'.$c, $value['tb_cuenta_des'])
          ->setCellValue('G'.$c, $value['tb_subcuenta_des'])
          ->setCellValue('H'.$c, mostrar_moneda($value['tb_egreso_imp']))
          ->setCellValue('I'.$c, $value['tb_caja_nom'])
          ->setCellValue('J'.$c, $value['tb_usuario_nom'] . ' '. mostrar_fecha_hora($value['tb_egreso_fecreg']))
        ;
//        $objPHPExcel->getActiveSheet()->mergeCells("A$c:J$c");
//        $objPHPExcel->getActiveSheet()->getStyle('I'.$c)->getNumberFormat()->setFormatCode('00000000');
        $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        
        $SUMA += $value['tb_egreso_imp'];
        $c++;
      }
    }
  $result = NULL;

//  $c=$c+1;
  $objPHPExcel->getActiveSheet()->mergeCells("A$c:D$c");
  $objPHPExcel->getActiveSheet()->mergeCells("E$c:G$c");
  $objPHPExcel->getActiveSheet()->mergeCells("I$c:J$c");
  $objPHPExcel->getActiveSheet()->setCellValue("E$c", 'TOTAL DE INGRESOS');
  $objPHPExcel->getActiveSheet()->setCellValue("H$c", mostrar_moneda($SUMA));
  $objPHPExcel->getActiveSheet(0)->getStyle('H'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
  $objPHPExcel->getActiveSheet()->getStyle("A$c:D$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("E$c:G$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("I$c:J$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($estiloTituloColumnas);
  
  
    

  
  
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(TRUE);
  $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(TRUE);
  
  
  $c=$c+4;
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'INGRESOS');
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", mostrar_moneda($SUMA_VENTAS));
  $objPHPExcel->getActiveSheet(0)->getStyle('C'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas);
  $c=$c+1;
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'EGRESOS');
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", mostrar_moneda($SUMA));
  $objPHPExcel->getActiveSheet(0)->getStyle('C'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas);
  $c=$c+1;
  $objPHPExcel->getActiveSheet()->setCellValue("B$c", 'SALDO A REPORTAR');
  $objPHPExcel->getActiveSheet()->setCellValue("C$c", mostrar_moneda($SUMA_VENTAS-$SUMA));
  $objPHPExcel->getActiveSheet(0)->getStyle('C'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
  $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($estiloTituloColumnas);
  $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($estiloTituloFilas);

  // Rename worksheet
  $objPHPExcel->getActiveSheet()->setTitle('CIERRE DE CAJA');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombre_archivo.'.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0*/
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=".$nombre_archivo.".csv");
header("Pragma: no-cache");
header("Expires: 0");
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
