
$(document).ready(function(){
  $('#form_motivollave').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"motivollave/motivollave_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_motivollave").serialize(),
				beforeSend: function() {
					$('#motivollave_mensaje').show(400);
					$('#btn_guardar_motivollave').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#motivollave_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#motivollave_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		motivollave_tabla();
		      		$('#modal_registro_motivollave').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#motivollave_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#motivollave_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_motivollave').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#motivollave_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#motivollave_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_motivollave_nom: {
				required: true,
				minlength: 2
			},
			txt_motivollave_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_motivollave_nom: {
				required: "Ingrese un motivo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_motivollave_des: {
				required: "Ingrese una descripción ",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
