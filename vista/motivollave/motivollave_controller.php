<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../motivollave/MotivoLlave.class.php');
  $oMotivollave = new MotivoLlave();

 	$action = $_POST['action'];

	//recogiendo los datos que se necesitan
	$motivollave_id = $_POST['hdd_motivollave_id'];


 	if($action == 'insertar'){
 		$motivollave_nom = mb_strtoupper($_POST['txt_motivollave_nom'], 'UTF-8');
 		$motivollave_des = $_POST['txt_motivollave_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Usuario Grupo.';
 		if($oMotivollave->insertar($motivollave_nom, $motivollave_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$motivollave_id = intval($_POST['hdd_motivollave_id']);
 		$motivollave_nom = mb_strtoupper($_POST['txt_motivollave_nom'], 'UTF-8');
 		$motivollave_des = $_POST['txt_motivollave_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Usuario Grupo.';

 		if($oMotivollave->modificar($motivollave_id, $motivollave_nom, $motivollave_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo modificado correctamente. '.$motivollave_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$motivollave_id = intval($_POST['hdd_motivollave_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el motivo de llave';

 		if($oMotivollave->eliminar($motivollave_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Motivo de llave registrada correctamente. '.$motivollave_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>