<?php
  if (defined('APP_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL .'motivollave/MotivoLlave.class.php');
  } else {
    require_once('../../core/usuario_sesion.php');
    require_once('../motivollave/MotivoLlave.class.php');
  }

  $oMotivollave = new MotivoLlave();

  $result = $oMotivollave->listar_motivollaves();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr>';
        $tr.='
          <td>'.$value['tb_motivollave_id'].'</td>
          <td>'.$value['tb_motivollave_nom'].'</td>
          <td>'.$value['tb_motivollave_des'].'</td>
          <td align="center">
            
              <a class="btn btn-info btn-xs" title="Ver" onclick="motivollave_form(\'L\','.$value['tb_motivollave_id'].')"><i class="fa fa-eye"></i> Ver</a>
              <a class="btn btn-warning btn-xs" title="Editar" onclick="motivollave_form(\'M\','.$value['tb_motivollave_id'].')"><i class="fa fa-edit"></i> Editar</a>
              <a class="btn btn-danger btn-xs" title="Eliminar" onclick="motivollave_form(\'E\','.$value['tb_motivollave_id'].')"><i class="fa fa-trash"></i> Eliminar</a>
              <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
            
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
  }

?>
<table id="tbl_motivollaves" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
