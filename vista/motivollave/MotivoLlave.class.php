<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class MotivoLlave extends Conexion{

    function insertar($motivollave_nom, $motivollave_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_motivollave(tb_motivollave_xac, tb_motivollave_nom, tb_motivollave_des)
          VALUES (1, :motivollave_nom, :motivollave_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivollave_nom", $motivollave_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":motivollave_des", $motivollave_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function modificar($motivollave_id, $motivollave_nom, $motivollave_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_motivollave SET tb_motivollave_nom =:motivollave_nom, tb_motivollave_des =:motivollave_des WHERE tb_motivollave_id =:motivollave_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivollave_id", $motivollave_id, PDO::PARAM_INT);
        $sentencia->bindParam(":motivollave_nom", $motivollave_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":motivollave_des", $motivollave_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }

    function eliminar($motivollave_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_motivollave WHERE tb_motivollave_id =:motivollave_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivollave_id", $motivollave_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function mostrarUno($motivollave_id){
      try {
        $sql = "SELECT * FROM tb_motivollave WHERE tb_motivollave_id =:motivollave_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":motivollave_id", $motivollave_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }
    function listar_motivollaves(){
      try {
        $sql = "SELECT * FROM tb_motivollave";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
  }

?>
