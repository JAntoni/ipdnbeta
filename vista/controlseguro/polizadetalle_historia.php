<?php
require_once('../../core/usuario_sesion.php');

require_once ("Poliza.class.php");
$oPoliza = new Poliza();
require_once ("Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();

$action = $_POST['action'];
$poliza_id = intval($_POST['poliza_id']);
$polizadetalle_id = intval($_POST['polizadetalle_id']);

$result = $oPoliza->mostarUno($poliza_id);
if ($result['estado'] == 1) {
    
}
$result =NULL;

$result1 = $oPolizadetalle->mostarUno($polizadetalle_id);
if ($result1['estado'] == 1) {
    $polizadetalle_historia=$result1['data']['tb_polizadetalle_historia'];
}
$result1 =NULL;

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_comentario_polizadetalle" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">INFORMACIÓN DE POLIZA DETALLE</h4>
            </div>
            <form id="for_cob" method="post">
                <input type="hidden" name="action" id="action"  value="<?php echo $action ?>">
                <input type="hidden" name="hdd_poliza_id" id="hdd_poliza_id" value="<?php echo $poliza_id ?>">
                <input type="hidden" name="hdd_polizadetalle_id" id="hdd_polizadetalle_id" value="<?php echo $polizadetalle_id ?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-success">
                                <div class="box-header bg-success">
                                    <h3 class="box-title text-green">Registrar </h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-original-title="Minimizar">
                                            <i class="fa fa-minus text-white"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="container-fluid">
                                        <div class="row mb-4">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_cuo_fecref" class="control-label">Fecha Referencial: </label>
                                                    <div class="input-group date" id="menorgarantia_picker1">
                                                        <input type="text" name="txt_det_fecref" id="txt_det_fecref" class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="col-md-8">
                                                <label>Notas:</label>
                                                <textarea id="txt_polizadetalle_historia" name="txt_polizadetalle_historia" class="form-control input-sm mayus" placeholder="Registre un comentario para la cuota del STR o GPS, regítrela sino déjela en blanco" style="resize: vertical;" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header bg-info">
                                    <h3 class="box-title text-blue">Historial de notas o comentarios </h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php echo $polizadetalle_historia; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_menorgarantia">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/controlseguro/polizadetalle_historia.js?ver==021124'; ?>"></script>