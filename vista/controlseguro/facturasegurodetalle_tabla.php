<?php
require_once("Poliza.class.php");
$oPoliza = new Poliza();
require_once("Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once ('../creditoasiveh/Creditoasiveh.class.php');
$oCreditoasiveh = new Creditoasiveh();
require_once ('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

require_once ('../uploadpdf/Upload.class.php');
$oUpload = new Upload();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

//echo 'fecha 1= '.$fecha1.' fecha2 ='.$fecha2;exit();

$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$cliente_id = $_POST['cliente_id'];
$poliza_tipo = $_POST['poliza_tipo'];
$poliza_categoria = $_POST['poliza_categoria'];
$tipo_credito = $_POST['tipo_credito'];
//$factura_estado = $_POST['factura_estado'];
// echo '<center><h2>NO SE HA ENCONTRADO POLIZAS VIGENTES POR PAGAR</h2></center>';
//    exit();

$dts = $oPolizadetalle->listar_facturasdetalle_poliza(fecha_mysql($fecha1), fecha_mysql($fecha2), $cliente_id, $poliza_tipo, $poliza_categoria, $tipo_credito);
//$dts = $oPolizadetalle->listar_facturasdetalle_poliza(fecha_mysql($fecha1), fecha_mysql($fecha2), $cliente_id, $poliza_tipo, $poliza_categoria, $tipo_credito,$factura_estado);

if ($dts['estado'] == 0) {
    echo '<center><h2>NO SE HA ENCONTRADO POLIZAS VIGENTES POR PAGAR</h2></center>';
    exit();
}
?>


<table cellspacing="1" id="tabla_controlsegurodetallereporte" class="table">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" width="5%">IDS</th>
            <th id="tabla_cabecera_fila" width="5%">TIPO</th>
            <th id="tabla_cabecera_fila" width="5%">CATEGORIAS</th>
            <th id="tabla_cabecera_fila" width="10%">N° POLIZA</th>
            <th id="tabla_cabecera_fila" width="20%">CLIENTE</th>
            <th id="tabla_cabecera_fila" width="20%">RAZON SOCIAL</th>
            <th id="tabla_cabecera_fila" width="20%">FECHA VENCIMIENTO</th>
            <th id="tabla_cabecera_fila" width="5%">FACTURA</th>
            <th id="tabla_cabecera_fila" width="5%">ID CREDITO</th>
            <th id="tabla_cabecera_fila" width="5%">TIPO CREDITO</th>
            <th id="tabla_cabecera_fila" width="5%">PRECIO POLIZA</th>
            <th id="tabla_cabecera_fila" width="5%">OPCIONES</th>
        </tr>
    </thead> 
    <tbody>
        <?php
        if ($dts['estado'] == 1) {
            foreach ($dts['data']as $key => $dt) {
                $btn = '';
                $vencida = '';
                 if ($dt['tb_poliza_respag2'] == 2) {
                
                
                $result=$oUpload->mostrarUno_modulo($dt['tb_polizadetalle_id'], 'segurosdetalle');
                if($result['estado']==0){
                     $btn = '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="abrirfactura_form2(\'I\',0,' . $dt['tb_polizadetalle_id'] . ')" title="Subir Pdf de la Poliza SRT o GPS"><i class="fa fa-file-pdf-o"></i></a>';
                
                     $vencida = '<strong style="color: red;">NO TIENE</strong>';
                }
                if($result['estado']==1){
                    $btn = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="contratos_SegurosDetalle_form2('.$dt['tb_polizadetalle_id'].')"><i class="fa fa-file-pdf-o" title="Ver Contratos"></i></a>';            
                    $vencida = '<strong style="color: green;">TIENE</strong>';
                    
                }
                
//                //$btn = '<a class="btn btn-danger btn-xs" onClick="controlseguro_anular_reporte('.$dt['tb_poliza_id'].')" title="Anular Poliza de STR"><i class="fa fa-eye-slash"></i></a> ';
//                        if (intval($dt['tb_factura_id'])==0&&intval($dt['tb_poliza_respag2'])==2) {
//                $btn = '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="abrirfactura_form2(\'I\',0,' . $dt['tb_polizadetalle_id'] . ')" title="Subir Pdf de la Poliza SRT o GPS"><i class="fa fa-file-pdf-o"></i></a>';
//                        }
////                        else{
//                            if (intval($dt['tb_factura_id'])!=0&&intval($dt['tb_poliza_respag2'])==2) {
//                $btn = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="contratos_SegurosDetalle_form2('.$dt['tb_polizadetalle_id'].')"><i class="fa fa-file-pdf-o" title="Ver Contratos"></i></a>';            
//                        }
//                $vencida = '<strong style="color: green;">TIENE</strong>';
//                if (intval($dt['tb_factura_id'])==0) {
//                    $vencida = '<strong style="color: red;">NO TIENE</strong>';
//                }
                
                
                
                
                
                

                $tipo_cre = '';
                $placa = '';
                $tipo = '';
                $categoria = '';

                if ($dt['tb_creditotipo_id'] == 2) {
                    $tipo_cre = 'ASI. VEH';
                    $resultasi = $oCreditoasiveh->mostrarUno($dt['tb_credito_id']);
                    if ($resultasi['estado'] == 1) {
                        $placa = $resultasi['data']['tb_credito_vehpla'];
                    }
                }
                if ($dt['tb_creditotipo_id'] == 3) {
                    $tipo_cre = 'GAR. MOB';
                    $resultgar = $oCreditogarveh->mostrarUno($dt['tb_credito_id']);
                    if ($resultgar['estado'] == 1) {
                        $placa = $resultgar['data']['tb_credito_vehpla'];
                    }
                }

                if ($dt['tb_poliza_tipo'] == 1){
                    $tipo = '<strong style="color: green;">STR</strong>';
                }
                if ($dt['tb_poliza_tipo'] == 2){
                    $tipo = '<strong style="color: blue;">GPS</strong>';
                }

                if ($dt['tb_poliza_categoria'] == 1){
                    $categoria = '<strong style="color: green;">NUEVO</strong>';
                }
                if ($dt['tb_poliza_categoria'] == 2){
                    $categoria = '<strong style="color: blue;">RENOVACIÓN</strong>';
                }


                $fecha_inicial = date("d-m-Y", strtotime($dt['tb_polizadetalle_fec']));
                $fecha_final = date('d-m-Y');
                $dias = (strtotime($fecha_inicial) - strtotime($fecha_final)) / 86400;

                $vencio = '<strong style="color: black;">Esta Cuota vence el dia de hoy </strong>';
                if ($dias > 0) {
                    $vencio = '<strong style="color: green;">Esta Cuota vence dentro de' . abs($dias) . ' dia(s)</strong>';
                }
                if ($dias < 0) {
                    $vencio = '<strong style="color: red;">Esta Cuota venció hace ' . abs($dias) . ' dia(s)</strong>';
                }
                ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $dt['tb_poliza_id'] . ' - ' . $dt['tb_polizadetalle_id'] ?></td>
                    <td id="tabla_fila"><?php echo $tipo; ?></td>
                    <td id="tabla_fila"><?php echo $categoria; ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_poliza_num']; ?></td>
                    <td id="tabla_fila" style="text-align: right"><?php echo $dt['tb_cliente_nom']; ?></td>
                    <?php
                    $razon = "";
                    if ($dt['tb_cliente_tip'] == 2)
                        $razon = $dt['tb_cliente_emprs'];
                    ?>

                    <td id="tabla_fila" style="text-align: right"><?php echo $razon; ?></td>
                    <td id="tabla_fila" style="text-align: right"><?php echo '<b>' . mostrar_fecha($dt['tb_polizadetalle_fec']) . '</b> - ' . $vencio ?></td>
                    <td id="tabla_fila"><?php echo $vencida ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_credito_id']; ?></td>
                    <td id="tabla_fila"><?php echo $tipo_cre; ?></td>
                    <td id="tabla_fila"><?php echo 'US$ ' . mostrar_moneda($dt['tb_polizadetalle_imp']); ?></td>
                    <td id="tabla_fila"><?php echo $btn; ?></td>
                </tr> 
            <?php 
            }
                }
            ?>
    </table>
<?php }
?>