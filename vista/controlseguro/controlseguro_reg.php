<?php
require_once ("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../proveedorpoliza/Proveedorpoliza.class.php");
$oProveedorpoliza = new Proveedorpoliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once ('../cliente/Cliente.class.php');
$oCliente= new cliente();
require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();
require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();

require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];

$poliza_id = intval($_POST['hdd_poliza_id']);
$poliza_num = $_POST['txt_poliza_num']; 
$cliente_id = intval($_POST['hdd_cliente_id']); 
$proveedorpoliza_id = intval($_POST['txt_cliente_aseg']); 
//$poliza_aseg = mb_strtoupper($_POST['txt_cliente_aseg'], 'UTF-8'); 
$poliza_tip = $_POST['cmb_poliza_tip']; 
$poliza_fecini = fecha_mysql($_POST['txt_poliza_fecini']); 
$poliza_fecfin = fecha_mysql($_POST['txt_poliza_fecfin']); 
$credito_id = $_POST['txt_credito_id'];
$creditotipo_id = $_POST['cmb_creditotipo_id']; 
$poliza_respag = $_POST['cmb_poliza_respag']; 
$poliza_respag2 = $_POST['cmb_poliza_respag2']; 
$poliza_pre = moneda_mysql($_POST['txt_poliza_pre']);
$poliza_cuo = $_POST['txt_poliza_cuo'];
$poliza_comi = moneda_mysql($_POST['txt_poliza_comi']);
$poliza_tipo = intval($_POST['txt_poliza_tip']);
$poliza_categoria = intval($_POST['txt_poliza_cate']);
$usuario_id=$_SESSION['usuario_id'];
$vehiculomarca_id = intval($_POST['txt_vehiculomarca_id']);
$vehiculomodelo_id = intval($_POST['txt_vehiculomodelo_id']);
$vehiculo_tasacion = moneda_mysql($_POST['txt_pagocomisiones_tasa']);
$moneda_id = intval($_POST['cmb_moneda_id']);
$poliza_formapago = mb_strtoupper($_POST['txt_pagocomisiones_forma'], 'UTF-8');
$asesor_id = intval($_POST['cbo_usuario_id']);
$vendedor_id = intval($_POST['hdd_vendedor_id']);


$result2=$oProveedorpoliza->mostrarUno($proveedorpoliza_id);
if($result2['estado']==1){
    $poliza_aseg=$result2['data']['tb_proveedor_poliza_nom'];
}


$oPoliza->setPoliza_id($poliza_id);
$oPoliza->setPoliza_num($poliza_num);
$oPoliza->setCliente_id($cliente_id);
$oPoliza->setProveedor_id($proveedorpoliza_id);
$oPoliza->setPoliza_aseg($poliza_aseg);
$oPoliza->setPoliza_tip($poliza_tip);
$oPoliza->setPoliza_fecini($poliza_fecini);
$oPoliza->setPoliza_fecfin($poliza_fecfin);
$oPoliza->setCredito_id($credito_id);
$oPoliza->setCreditotipo_id($creditotipo_id);
$oPoliza->setPoliza_respag($poliza_respag);
$oPoliza->setPoliza_respag2($poliza_respag2);
$oPoliza->setPoliza_pre($poliza_pre);
$oPoliza->setPoliza_cuo($poliza_cuo);
$oPoliza->setPoliza_comi($poliza_comi);
$oPoliza->setPoliza_tipo($poliza_tipo);
$oPoliza->setPoliza_categoria($poliza_categoria);
$oPoliza->setUsuario_id($usuario_id);
$oPoliza->setVehiculomarca_id($vehiculomarca_id);
$oPoliza->setVehiculomodelo_id($vehiculomodelo_id);
$oPoliza->setVehiculo_tasacion($vehiculo_tasacion);
$oPoliza->setMoneda_id($moneda_id);
$oPoliza->setPoliza_formapago($poliza_formapago);
$oPoliza->setAsesor_id($asesor_id);
$oPoliza->setVendedor_id($vendedor_id);

//echo '$poliza_categoria 3333= '.$poliza_categoria;exit();
//$data['estado'] = 0;
//$data['msj'] = 'entrando al editar 3553355353';
//echo json_encode($data);
//exit();


$result1=$oCliente->mostrarUno($cliente_id);
if($result1['estado']==1){
    $cliente=$result1['data']['tb_cliente_nom'];
    $dni=$result1['data']['tb_cliente_doc'];
}

if($creditotipo_id==2){
    $creditotipo="Asistencia Vehicular";
}
else if($creditotipo_id==3){
    $creditotipo="Garantia Vehicular";
}

$pol_tipo="SEGURO";
$proveedor="Aseguradora";
$estado="";
if($poliza_tipo==2){
    $pol_tipo="GPS";
    $proveedor="Proveedor";
    if($poliza_categoria==1){
     $estado=' de categoria <b style="color: #0000cc"> Nuevo </b>';   
    }
    if($poliza_categoria==2){
     $estado=' de categoria <b style="color: #0000cc"> Renovación </b>';   
    }
    
    
}

$tip="Particular";
if($poliza_tip==2){
   $tip="Taxi";
}

if($action == 'insertar'){
    $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha registrado una Poliza con el número'
            . ' <b style="color: #0000cc">'.$poliza_num.'</b> de tipo <b style="color: #0000cc">'.$pol_tipo.'</b> '.$estado.' para el cliente <b style="color: #0000cc">'.$cliente."</b>"
            . ' con DNI <b style="color: #0000cc">'.$dni.'</b> para el tipo de crédito <b style="color: #0000cc">'.$creditotipo.'</b> con la/el '.$proveedor.' <b style="color: #0000cc">'.$poliza_aseg.'</b>'
            . ' para uso del Vehiculo tipo <b style="color: #0000cc">'.$tip.'</b> con fecha de inicio <b style="color: #0000cc">'. mostrar_fecha($poliza_fecini).'</b> '
            . ' y cuya fecha final es el  <b style="color: #0000cc">'. mostrar_fecha($poliza_fecfin).'</b> y el precio es de <b style="color: #0000cc">'. mostrar_moneda($poliza_pre).'</b> '
            . ' cuya comision es de <b style="color: #0000cc">'. mostrar_moneda($poliza_comi).'</b>, la cuales se han generado en un total de <b style="color: #0000cc">'.$poliza_cuo.'</b> cuotas - dia '.date('d-m-Y h:i:s') .'<br><br>';
    
    $oPoliza->setPoliza_his($poliza_his);
    $result = $oPoliza->mostrarPoliza($poliza_num, $poliza_id);
//||$poliza_categoria>0||$poliza_categoria>0
//    if ($result['estado'] == 0) {
	
	$array_detalle_fec = $_POST['txt_polizadetalle_fec'];
	$array_detalle_imp = $_POST['txt_polizadetalle_imp'];
	$array_detalle_cupon = $_POST['txt_polizadetalle_cup'];

	if(!is_array($array_detalle_fec)){
		$data['estado'] = 0;
		$data['msj'] = 'Falta el cronograma de pagos';

		echo json_encode($data); exit();
	}
        
         if(is_array($array_detalle_fec)){
		for($i=0; $i < count($array_detalle_fec); $i++) {
                    $polizadetalle_fec = fecha_mysql($array_detalle_fec[$i]);
                    $polizadetalle_imp = moneda_mysql($array_detalle_imp[$i]);
                    $polizadetalle_cupon = $array_detalle_cupon[$i];

                    if($polizadetalle_fec==""||$polizadetalle_imp==""||$polizadetalle_cupon==""){
                        $data['estado'] =0;
                        $data['msj'] = 'Revise que todos los campos tengas datos a Registrar';
                        echo json_encode($data);
                        exit();
                    }
		}
	}
        

	$result=$oPoliza->insertar();
        
        if(intval($result['estado']) == 1){
            $poliza_id = $result['poliza_id'];
        }

       
        
        
        
	if(is_array($array_detalle_fec)){
		for($i=0; $i < count($array_detalle_fec); $i++) {
                    $polizadetalle_fec = fecha_mysql($array_detalle_fec[$i]);
                    $polizadetalle_imp = moneda_mysql($array_detalle_imp[$i]);
                    $polizadetalle_cupon = moneda_mysql($array_detalle_cupon[$i]);

                    $oPolizadetalle->setPoliza_id($poliza_id);
                    $oPolizadetalle->setPolizadetalle_fec($polizadetalle_fec);
                    $oPolizadetalle->setPolizadetalle_imp($polizadetalle_imp);
                    $oPolizadetalle->setPolizadetalle_cupon($polizadetalle_cupon);

                    $oPolizadetalle->insertar_detalle();
		}
	}

	$data['estado'] = 1;
	$data['tipo'] = $poliza_tipo;
	$data['credito_id'] = $credito_id;
	$data['msj'] = 'Registro de Póliza Correctamente';

	echo json_encode($data);
//    }
//    else {
//        $data['estado'] = 0;
//        $data['msj'] = 'El Número de Poliza ' . $poliza_num . ' ya se encuentra Registrado en el Sistema por favor Verifique';
//        echo json_encode($data);
//    }
}

if($action == 'editar'){
    
        
    
	$poliza_id = intval($_POST['hdd_poliza_id']);
        $oPoliza->setPoliza_id($poliza_id);
        
            $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha Editado la Poliza de ID <b style="color: #0000cc">'.$poliza_id.'</b> con el número de Poliza'
            . ' <b style="color: #0000cc">'.$poliza_num.'</b> de tipo <b style="color: #0000cc">'.$pol_tipo.'</b> '.$estado.' para el cliente <b style="color: #0000cc">'.$cliente."</b>"
            . ' con DNI <b style="color: #0000cc">'.$dni.'</b> del credito con el ID <b style="color: #0000cc">'.$credito_id.'</b> para el tipo de crédito <b style="color: #0000cc">'.$creditotipo.'</b> con la/el '.$proveedor.' <b style="color: #0000cc">'.$poliza_aseg.'</b>'
            . ' para uso del Vehiculo tipo <b style="color: #0000cc">'.$tip.'</b> con fecha de inicio <b style="color: #0000cc">'. mostrar_fecha($poliza_fecini).'</b> '
            . ' y cuya fecha final es el  <b style="color: #0000cc">'. mostrar_fecha($poliza_fecfin).'</b> y el precio es de <b style="color: #0000cc">'. mostrar_moneda($poliza_pre).'</b> '
            . ' cuya comision es de <b style="color: #0000cc">'. mostrar_moneda($poliza_comi).'</b>, la cuales se han generado en un total de <b style="color: #0000cc">'.$poliza_cuo.'</b> cuotas - dia '.date('d-m-Y h:i:s') .' <br><br>';
        
        $oPoliza->setPoliza_his($poliza_his);    
	$oPoliza->editar();
//	$oPoliza->Actualizar_NCuotas();
	$oPoliza->modificar_his();

	$array_detalle_fec = $_POST['txt_polizadetalle_fec'];
	$array_detalle_imp = $_POST['txt_polizadetalle_imp'];

    //? YA NO SE CREARÁ MÁS CUOTAS DE PAGO DE POLIZA, YA QUE LOS GPS SE PAGAN AL CONTADO
	/*if(is_array($array_detalle_fec) && count($array_detalle_fec) > 0){
                $oPolizadetalle->setPoliza_id($poliza_id);
                
		for($i=0; $i < count($array_detalle_fec); $i++) {
            $polizadetalle_fec = fecha_mysql($array_detalle_fec[$i]);
            $polizadetalle_imp = moneda_mysql($array_detalle_imp[$i]);

            $oPolizadetalle->setPoliza_id($poliza_id);
            $oPolizadetalle->setPolizadetalle_fec($polizadetalle_fec);
            $oPolizadetalle->setPolizadetalle_imp($polizadetalle_imp);

            $oPolizadetalle->insertar_detalle();

		}
	}*/

	$data['estado'] = 1;
	$data['msj'] = 'Detalle de Unidad Disponible Editado';

	echo json_encode($data);
}

if($action == 'pago'){
    
        $poliza_id = intval($_POST['poliza_id']);
        $oPoliza->setPoliza_id($poliza_id);
	$polizadetalle_id = intval($_POST['polizadetalle_id']);
	$polizadetalle_det = trim($_POST['detalle']);
        
         $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha Realizado el pago del detalle de la Poliza N° <b style="color: #0000cc">'.$poliza_id.'-'.$polizadetalle_id.'</b>  
                 con el siguiente detalle <b style="color: #0000cc">'.$polizadetalle_det.' - dia '.date('d-m-Y h:i:s') .'</b><br><br>';
        
        $oPoliza->setPoliza_his($poliza_his);    
	$oPoliza->modificar_his();

	//actualizamos el estado de la póliza a pagado
	$oPolizadetalle->modificar_campo_detalle($polizadetalle_id, 'tb_polizadetalle_est', 1,'INT'); // 1 ya pagado
	$oPolizadetalle->modificar_campo_detalle($polizadetalle_id, 'tb_polizadetalle_det', $polizadetalle_det,'STR');

	$data['estado'] = 1;
	$data['msj'] = 'Pago de Letra actualizada';

	echo json_encode($data);
}

if($action == 'pagar_comision'){
	$poliza_id = intval($_POST['poliza_id']);
        $oPoliza->setPoliza_id($poliza_id);
        
         $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha Realizado el pago de la Comisión de la Poliza N° <b style="color: #0000cc"> '.$poliza_id.' </b> el dia '.date('d-m-Y h:i:s') .'<br><br>';
        
        $oPoliza->setPoliza_his($poliza_his);    
	$oPoliza->modificar_his();

	//actualizamos el estado de la póliza a pagado
	$oPoliza->modificar_campo($poliza_id,'tb_poliza_estcomi', 1,'INT'); // 1 ya pagado

	$data['estado'] = 1;
	$data['msj'] = 'Pago de Letra actualizada';

	echo json_encode($data);
}

if($action == 'anular'){
	$poliza_id = intval($_POST['poliza_id']);
        
        $oPoliza->setPoliza_id($poliza_id);
        
         $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha hecho la anulación de la Poliza N° <b style="color: #0000cc"> '.$poliza_id.' </b> el dia '.date('d-m-Y h:i:s') .'<br><br>';
        
        $oPoliza->setPoliza_his($poliza_his);    
	$oPoliza->modificar_his();

	//actualizamos el estado de la póliza a pagado
	$oPoliza->modificar_campo($poliza_id,'tb_poliza_anulada',2,'INT'); // 1. activo 2. anulada

	$data['estado'] = 1;
	$data['msj'] = 'Poliza de Seguro Anulada';

	echo json_encode($data);
}

if($action == 'exonerar'){
	$poliza_id = intval($_POST['poliza_id']);
        $oPoliza->setPoliza_id($poliza_id);
        
        $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha hecho la exoneración de la Poliza N° <b style="color: #0000cc"> '.$poliza_id.' </b> el dia '.date('d-m-Y h:i:s') .'<br><br>'; 
        $oPoliza->setPoliza_his($poliza_his);    
        
        $result=$oPoliza->mostarUnoHistorial($poliza_id);
        if($result['estado']==1){
            $historial=$result['data']['tb_poliza_his'];
            if($historial==""){
                $oPoliza->modificar_his1();
            }
            else{
                $oPoliza->modificar_his();
            }
        }

	//actualizamos el estado de la póliza a pagado
	$oPoliza->modificar_campo($poliza_id,'tb_poliza_exonerar',2,'INT'); // 1. no exonerado 2. exonerado

	$data['estado'] = 1;
	$data['msj'] = 'Poliza de Seguro Anulada';

	echo json_encode($data);
}

if($action == 'verificar'){
	$poliza_id = intval($_POST['poliza_id']);
        
        $oPoliza->setPoliza_id($poliza_id);
        $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha Finalizado la Edicion de los datos de la Poliza N° <b style="color: #0000cc"> '.$poliza_id.' </b> el dia '.date('d-m-Y h:i:s') .'<br><br>';
        $oPoliza->setPoliza_his($poliza_his);
        
	$result=$oPoliza->mostarUnoHistorial($poliza_id);
        if($result['estado']==1){
            $historial=$result['data']['tb_poliza_his'];
            if($historial==""){
                $oPoliza->modificar_his1();
            }
            else{
                $oPoliza->modificar_his();
            }
        }

	//actualizamos el estado de la póliza a verificado
	$oPoliza->modificar_campo($poliza_id,'tb_poliza_ver',1,'INT'); // 1 ya pagado

	$data['estado'] = 1;
	$data['msj'] = 'La Poliza ha sido Cancelada de toda Edición por el Usuario '.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'];

	echo json_encode($data);
}

if($action == 'modificar_cupon'){
	$poliza_id = intval($_POST['poliza_id']);
	$polizadetalle_id = intval($_POST['polizadetalle_id']);
	$cupon = $_POST['cupon'];
	$monto = moneda_mysql($_POST['monto']);
	$fecha = fecha_mysql($_POST['fecha']);
	$detalle = strtoupper($_POST['detalle']);
        
        
        $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha Editado el cupon <b style="color: #0000cc">'.$cupon.' </b> de la Poliza N° <b style="color: #0000cc"> '.$poliza_id.' </b> el dia '.date('d-m-Y h:i:s') .'<br><br>';
        $oPoliza->setPoliza_his($poliza_his);  
        
	$result=$oPoliza->mostarUnoHistorial($poliza_id);
        if($result['estado']==1){
            $historial=$result['data']['tb_poliza_his'];
            if($historial==""){
                $oPoliza->modificar_his1();
            }
            else{
                $oPoliza->modificar_his();
            }
        }

	//actualizamos el estado de la póliza a verificado
	$oPolizadetalle->modificar_campo_detalle($polizadetalle_id,'tb_polizadetalle_cupon',$cupon,'STR'); // cupon
	$oPolizadetalle->modificar_campo_detalle($polizadetalle_id,'tb_polizadetalle_fec',$fecha,'STR'); // fecha
	$oPolizadetalle->modificar_campo_detalle($polizadetalle_id,'tb_polizadetalle_imp',$monto,'STR'); // Monto
	$oPolizadetalle->modificar_campo_detalle($polizadetalle_id,'tb_polizadetalle_det',$detalle,'STR'); // detalle

	$data['estado'] = 1;
	$data['poliza_id'] = $poliza_id;
	$data['msj'] = 'El Cupon del detalle de la Poliza ha sido Editada Correctamente'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'];

	echo json_encode($data);
}

if($action == 'agregar_nota'){
    
$polizadetalle_id= intval($_POST['hdd_polizadetalle_id']);
$comentario=mb_strtoupper($_POST['txt_polizadetalle_historia'], 'UTF-8');

$nota= '&FilledSmallSquare;<b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> - <b style="color: black">'. date('d-m-Y h:i:s').'</b> : '.$comentario.'. <br><br>';


$oPolizadetalle->modificar_campo_nota($polizadetalle_id,'tb_polizadetalle_historia', $nota);


$data['estado'] = 1;
$data['msj'] = 'Nota Agregada Correctamente';
echo json_encode($data);

}


if ($action == 'pagar_vendedor') {

    $poliza_id = $_POST['hdd_poliza_id'];
    $fecha = fecha_mysql($_POST['txt_poliza_fec']);
    $monto_pagado = $_POST['txt_pagar'];
    $tipo = $_POST['hdd_pago'];
    $vendedor = $_POST['txt_vendedor_nom'];
    $tipo_moneda = $_POST['cmb_moneda_id1'];
    
    if($tipo_moneda==1){
        $moneda="S./ ";
    }
    if($tipo_moneda==2){
        $moneda="US$./ ";
    }

    $result = $oPoliza->mostarUno($poliza_id);
    if ($result['estado'] == 1) {
        $vendedor_id = $result['data']['tb_vendedor_id'];
        $pagocomisiones_comision = $result['data']['tb_poliza_estcomi'];
        $moneda_id = $result['data']['tb_moneda_id'];
        $usuario_id = $result['data']['tb_asesor_id'];
        $cliente_id = $result['data']['tb_cliente_id'];
        $pagocomisiones_poliza = $result['data']['tb_poliza_num'];
    }
    $result = NULL;

    $result = $oCliente->mostrarUno($cliente_id);
    if ($result['estado'] == 1) {
        $cliente_nombre = $result['data']['tb_cliente_nom'];
    }
    $result = NULL;

    if ($tipo == 1) {
        $poliza_his = '&FilledSmallSquare; EL USUARIO: <b style="color: #0000cc">' . $_SESSION['usuario_nom'] . '</b> HA REALIZADO EL PAGO EL PAGO DE LA COMISIÓN AL  VENDEDOR  <b style="color: #0000cc">' . $vendedor . '</b>'
                . ' el dia <b style="color: #0000cc">' . date('d-m-Y h:i:s') . '</b> POR UN MONTO DE <b style="color: #0000cc">' . $moneda . ' ' . $monto_pagado . '</b> POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° ' . $pagocomisiones_poliza . '</b>  - dia '.date('d-m-Y h:i:s') .'<br><br>';

        
        $oPoliza->setPoliza_his($poliza_his);    
	$oPoliza->modificar_his();

        $pro_id = 18;

        $egr_det = 'EGRESO: PAGO A COLABORADORES EXTERNOS (VENDEDORES DE CONCESIONARIO), COMISIONES DE SEGUROS, CLIENTE: <b style="color: #0000cc"> ' . $cliente_nombre . '</b> - VENDEDOR: <b style="color: #0000cc">' . $vendedor . '</b>  POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° ' . $pagocomisiones_poliza . '</b> ';
        $caj_id = 1;
        $cue_id = 16; //Gastos operativos
        $subcue_id = 151; //Incentivos a colaboradores Externos
        $mod_id = 63; //pago de comisiones a vendedores de concesionarias
        $modide = 0;

        $oEgreso->egreso_usureg = $_SESSION['usuario_id'];
        $oEgreso->egreso_usumod = $_SESSION['usuario_id'];
        $oEgreso->egreso_fec = $fecha;
        $oEgreso->documento_id = 9; //otros egresos
        $oEgreso->egreso_numdoc = 'OE' . '-';
        $oEgreso->egreso_det = $egr_det;
        $oEgreso->egreso_imp = moneda_mysql($monto_pagado);
        $oEgreso->egreso_tipcam = 0;
        $oEgreso->egreso_est = 1;
        $oEgreso->cuenta_id = $cue_id;
        $oEgreso->subcuenta_id = $subcue_id;
        $oEgreso->proveedor_id = $pro_id;
        $oEgreso->cliente_id = 0;
        $oEgreso->usuario_id = 0;
        $oEgreso->caja_id = $caj_id;
        $oEgreso->moneda_id = $tipo_moneda;
        $oEgreso->modulo_id = $mod_id;
        $oEgreso->egreso_modide = $modide;
        $oEgreso->empresa_id = $_SESSION['empresa_id'];
        $oEgreso->egreso_ap = 0; //si

        $result = $oEgreso->insertar();
        if (intval($result['estado']) == 1) {
            $egreso_id = $result['egreso_id'];
        }
//            $oEgreso->modificar_campo($egreso_id,'tb_egreso_numdoc','OE-'.$egreso_id, 'STR');
        $oPoliza->modificar_campo($poliza_id, 'tb_egreso_id', $egreso_id, 'INT');
        $oPoliza->modificar_campo($poliza_id, 'tb_poliza_estcomi', 1, 'INT');
        $oPoliza->modificar_pago($poliza_id, 'tb_poliza_monven', $monto_pagado, 'INT');
    }
    if ($tipo == 2) {
        $poliza_his = '&FilledSmallSquare; EL USUARIO: <b style="color: #0000cc">' . $_SESSION['usuario_nom'] . '</b> HA REALIZADO UN INGRESO A CAJA POR CONCEPTO DE COMISIÓN DEL CLIENTE: <b style="color: #0000cc">' . $cliente_nombre . '</b>'
                . ' el dia <b style="color: #0000cc">' . date('d-m-Y h:i:s') . '</b> POR UN MONTO DE <b style="color: #0000cc">' . $moneda . ' ' .$monto_pagado . '</b>  POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° '.$pagocomisiones_poliza.'</b> <br><br>';
        $oPoliza->setPoliza_his($poliza_his);    
	$oPoliza->modificar_his();

        $cue_id = 3;
        $subcue_id = 10;
        $cliente = 1144;
        $mod_id = 63; //pago de comisiones a vendedores de concesionarias
        $modide = 0;
        $ing_det = 'INGRESO A ACAJA POR CONCEPTO DE PAGO DE  COMISIONES DE SEGUROS TODO RIESGO, CLIENTE: <b style="color: #0000cc"> ' . $cliente_nombre . '  POR LA POLIZA DE SEGURO <b style="color: #0000cc"> N° '.$pagocomisiones_poliza.'</b><br>';

        $oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
        $oIngreso->ingreso_fec = $fecha;
        $oIngreso->documento_id = 8;
        $oIngreso->ingreso_numdoc = 'OI';
        $oIngreso->ingreso_det = $ing_det;
        $oIngreso->ingreso_imp = moneda_mysql($monto_pagado);
        $oIngreso->cuenta_id = $cue_id;
        $oIngreso->subcuenta_id = $subcue_id;
        $oIngreso->cliente_id = $cliente;
        $oIngreso->caja_id = 1;
        $oIngreso->moneda_id = $tipo_moneda;
        //valores que pueden ser cambiantes según requerimiento de ingreso
        $oIngreso->modulo_id = $mod_id;
        $oIngreso->ingreso_modide = $modide;
        $oIngreso->empresa_id = $_SESSION['empresa_id'];

        $result = $oIngreso->insertar();
        if (intval($result['estado']) == 1) {
            $ingreso_id = $result['ingreso_id'];
        }


//        echo 'hasta ki estoy entrando normal al sistema';exit();
        $cuopag_his .= 'El ingreso generado en caja tuvo el siguiente detalle: ' . $ing_det . ' ||&& ';
        $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> ha hecho la transacción: ' . $ing_det . '. El monto válido fue: S/. ' . mostrar_moneda($monto) . ' && <b>' . date('d-m-Y h:i a') . '</b>';
        $oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
        $oLineaTiempo->tb_lineatiempo_det = $line_det;
        $oLineaTiempo->insertar();

        
        $oPoliza->modificar_campo($poliza_id, 'tb_ingreso_id', $ingreso_id, 'INT');
        $oPoliza->modificar_campo($poliza_id, 'tb_poliza_estcomi', 2, 'INT');
        $oPoliza->modificar_pago($poliza_id, 'tb_poliza_monipdn', $monto_pagado, 'INT');
    }


    $data['estado'] = 1;
    $data['msj'] = 'Pago de Comisión Realizado correctamente.';
    echo json_encode($data);
}


if ($action == 'controlcomision_his') {
    $controlcomision_id = $_POST['poliza_id'];

    $dts = $oPoliza->mostarUno($controlcomision_id);

    if ($dts['estado'] == 1) {
        $his = $dts['data']['tb_poliza_his'];
    }

    echo'<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_controlseguros_historial">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                      </button>
                      <h4 class="modal-title" style="font-family: cambria;"><b>Historial de Control de Seguros</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12" style="font-family: cambria">
                                            ' . $his . '
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                </div>
            </div>';

//	echo $his;
} 

if($action == 'finalizar'){
	$poliza_id = intval($_POST['poliza_id']);
        
        $oPoliza->setPoliza_id($poliza_id);
        
         $poliza_his='&FilledSmallSquare; El Usuario <b style="color: #0000cc">'.$_SESSION['usuario_nom'].' '.$_SESSION['usuario_ape'].'</b> ha Finalizado la Poliza N° <b style="color: #0000cc"> '.$poliza_id.' </b> el dia '.date('d-m-Y h:i:s') .'<br><br>';
        
        $oPoliza->setPoliza_his($poliza_his);    
	$oPoliza->modificar_his();

	//actualizamos el estado de la póliza a pagado
	$oPoliza->modificar_campo($poliza_id,'tb_poliza_visible',2,'INT'); // 1. visible 2.no visible

	$data['estado'] = 1;
	$data['msj'] = 'Poliza de Seguro Finalizada Correctamente';

	echo json_encode($data);
}
?>