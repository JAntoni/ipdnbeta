<?php

if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Poliza extends Conexion
{

  private $poliza_id;
  private $poliza_xac;
  private $poliza_reg;
  private $poliza_num;
  private $cliente_id;
  private $poliza_aseg;
  private $poliza_tip;
  private $poliza_fecini;
  private $poliza_fecfin;
  private $credito_id;
  private $creditotipo_id;
  private $poliza_respag;
  private $poliza_respag2;
  private $poliza_pre;
  private $poliza_cuo;
  private $poliza_comi;
  private $poliza_estcomi;
  private $poliza_tipo;
  private $poliza_categoria;
  private $poliza_his;
  private $usuario_id;
  private $poliza_ver;
  private $vehiculomarca_id;
  private $vehiculomodelo_id;
  private $vehiculo_tasacion;
  private $moneda_id;
  private $poliza_formapago;
  private $asesor_id;
  private $vendedor_id;
  private $proveedor_id;

  public function getPoliza_id()
  {
    return $this->poliza_id;
  }

  public function getPoliza_xac()
  {
    return $this->poliza_xac;
  }

  public function getPoliza_reg()
  {
    return $this->poliza_reg;
  }

  public function getPoliza_num()
  {
    return $this->poliza_num;
  }

  public function getCliente_id()
  {
    return $this->cliente_id;
  }

  public function getPoliza_aseg()
  {
    return $this->poliza_aseg;
  }

  public function getPoliza_tip()
  {
    return $this->poliza_tip;
  }

  public function getPoliza_fecini()
  {
    return $this->poliza_fecini;
  }

  public function getPoliza_fecfin()
  {
    return $this->poliza_fecfin;
  }

  public function getCredito_id()
  {
    return $this->credito_id;
  }

  public function getCreditotipo_id()
  {
    return $this->creditotipo_id;
  }

  public function getPoliza_respag()
  {
    return $this->poliza_respag;
  }

  public function getPoliza_respag2()
  {
    return $this->poliza_respag2;
  }

  public function getPoliza_pre()
  {
    return $this->poliza_pre;
  }

  public function getPoliza_cuo()
  {
    return $this->poliza_cuo;
  }

  public function getPoliza_comi()
  {
    return $this->poliza_comi;
  }

  public function getPoliza_estcomi()
  {
    return $this->poliza_estcomi;
  }

  public function getPoliza_tipo()
  {
    return $this->poliza_tipo;
  }

  public function getPoliza_categoria()
  {
    return $this->poliza_categoria;
  }

  public function getPoliza_his()
  {
    return $this->poliza_his;
  }

  public function getUsuario_id()
  {
    return $this->usuario_id;
  }

  public function getPoliza_ver()
  {
    return $this->poliza_ver;
  }

  public function getVehiculomarca_id()
  {
    return $this->vehiculomarca_id;
  }

  public function getVehiculomodelo_id()
  {
    return $this->vehiculomodelo_id;
  }

  public function getVehiculo_tasacion()
  {
    return $this->vehiculo_tasacion;
  }

  public function getMoneda_id()
  {
    return $this->moneda_id;
  }

  public function getPoliza_formapago()
  {
    return $this->poliza_formapago;
  }

  public function getAsesor_id()
  {
    return $this->asesor_id;
  }

  public function getVendedor_id()
  {
    return $this->vendedor_id;
  }

  public function getProveedor_id()
  {
    return $this->proveedor_id;
  }

  public function setPoliza_id($poliza_id)
  {
    $this->poliza_id = $poliza_id;
  }

  public function setPoliza_xac($poliza_xac)
  {
    $this->poliza_xac = $poliza_xac;
  }

  public function setPoliza_reg($poliza_reg)
  {
    $this->poliza_reg = $poliza_reg;
  }

  public function setPoliza_num($poliza_num)
  {
    $this->poliza_num = $poliza_num;
  }

  public function setCliente_id($cliente_id)
  {
    $this->cliente_id = $cliente_id;
  }

  public function setPoliza_aseg($poliza_aseg)
  {
    $this->poliza_aseg = $poliza_aseg;
  }

  public function setPoliza_tip($poliza_tip)
  {
    $this->poliza_tip = $poliza_tip;
  }

  public function setPoliza_fecini($poliza_fecini)
  {
    $this->poliza_fecini = $poliza_fecini;
  }

  public function setPoliza_fecfin($poliza_fecfin)
  {
    $this->poliza_fecfin = $poliza_fecfin;
  }

  public function setCredito_id($credito_id)
  {
    $this->credito_id = $credito_id;
  }

  public function setCreditotipo_id($creditotipo_id)
  {
    $this->creditotipo_id = $creditotipo_id;
  }

  public function setPoliza_respag($poliza_respag)
  {
    $this->poliza_respag = $poliza_respag;
  }

  public function setPoliza_respag2($poliza_respag2)
  {
    $this->poliza_respag2 = $poliza_respag2;
  }

  public function setPoliza_pre($poliza_pre)
  {
    $this->poliza_pre = $poliza_pre;
  }

  public function setPoliza_cuo($poliza_cuo)
  {
    $this->poliza_cuo = $poliza_cuo;
  }

  public function setPoliza_comi($poliza_comi)
  {
    $this->poliza_comi = $poliza_comi;
  }

  public function setPoliza_estcomi($poliza_estcomi)
  {
    $this->poliza_estcomi = $poliza_estcomi;
  }

  public function setPoliza_tipo($poliza_tipo)
  {
    $this->poliza_tipo = $poliza_tipo;
  }

  public function setPoliza_categoria($poliza_categoria)
  {
    $this->poliza_categoria = $poliza_categoria;
  }

  public function setPoliza_his($poliza_his)
  {
    $this->poliza_his = $poliza_his;
  }

  public function setUsuario_id($usuario_id)
  {
    $this->usuario_id = $usuario_id;
  }

  public function setPoliza_ver($poliza_ver)
  {
    $this->poliza_ver = $poliza_ver;
  }

  public function setVehiculomarca_id($vehiculomarca_id)
  {
    $this->vehiculomarca_id = $vehiculomarca_id;
  }

  public function setVehiculomodelo_id($vehiculomodelo_id)
  {
    $this->vehiculomodelo_id = $vehiculomodelo_id;
  }

  public function setVehiculo_tasacion($vehiculo_tasacion)
  {
    $this->vehiculo_tasacion = $vehiculo_tasacion;
  }

  public function setMoneda_id($moneda_id)
  {
    $this->moneda_id = $moneda_id;
  }

  public function setPoliza_formapago($poliza_formapago)
  {
    $this->poliza_formapago = $poliza_formapago;
  }

  public function setAsesor_id($asesor_id)
  {
    $this->asesor_id = $asesor_id;
  }

  public function setVendedor_id($vendedor_id)
  {
    $this->vendedor_id = $vendedor_id;
  }

  public function setProveedor_id($proveedor_id)
  {
    $this->proveedor_id = $proveedor_id;
  }

  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_poliza(
				tb_poliza_num, 
                tb_cliente_id, 
                tb_proveedor_id, 
                tb_poliza_aseg, 
				tb_poliza_tip, 
                tb_poliza_fecini, 
                tb_poliza_fecfin, 
				tb_credito_id, 
                tb_creditotipo_id, 
                tb_poliza_respag, 
                tb_poliza_respag2, 
                tb_poliza_pre,
				tb_poliza_cuo, 
                tb_poliza_comi,
                tb_poliza_tipo,
                tb_poliza_categoria,
                tb_poliza_his,
                tb_usuario_id) 
			VALUES (
				:poliza_num, 
                :cliente_id, 
                :proveedor_id, 
                :poliza_aseg, 
				:poliza_tip, 
                :poliza_fecini, 
                :poliza_fecfin, 
				:credito_id, 
                :creditotipo_id, 
                :poliza_respag, 
                :poliza_respag2, 
                :poliza_pre,
				:poliza_cuo, 
                :poliza_comi,
                :poliza_tipo,
                :poliza_categoria,
                :poliza_his,
                :usuario_id)";

      $getPoliza_num = $this->getPoliza_num();
      $getCliente_id = $this->getCliente_id();
      $getProveedor_id = $this->getProveedor_id();
      $getPoliza_aseg = $this->getPoliza_aseg();
      $getPoliza_tip = $this->getPoliza_tip();
      $getPoliza_fecini = $this->getPoliza_fecini();
      $getPoliza_fecfin = $this->getPoliza_fecfin();
      $getCredito_id = $this->getCredito_id();
      $getCreditotipo_id = $this->getCreditotipo_id();
      $getPoliza_respag = $this->getPoliza_respag();
      $getPoliza_respag2 = $this->getPoliza_respag2();
      $getPoliza_pre = $this->getPoliza_pre();
      $getPoliza_cuo = $this->getPoliza_cuo();
      $getPoliza_comi = $this->getPoliza_comi();
      $getPoliza_tipo = $this->getPoliza_tipo();
      $getPoliza_categoria = $this->getPoliza_categoria();
      $getPoliza_his = $this->getPoliza_his();
      $getUsuario_id = $this->getUsuario_id();

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":poliza_num", $getPoliza_num, PDO::PARAM_STR);
      $sentencia->bindParam(":cliente_id", $getCliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":proveedor_id", $getProveedor_id, PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_aseg", $getPoliza_aseg, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_tip", $getPoliza_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_fecini", $getPoliza_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_fecfin", $getPoliza_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":credito_id", $getCredito_id, PDO::PARAM_INT);
      $sentencia->bindParam(":creditotipo_id", $getCreditotipo_id, PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_respag", $getPoliza_respag, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_respag2", $getPoliza_respag2, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_pre", $getPoliza_pre, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_cuo", $getPoliza_cuo, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_comi", $getPoliza_comi, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_tipo", $getPoliza_tipo, PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_categoria", $getPoliza_categoria, PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_his", $getPoliza_his, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $getUsuario_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $poliza_id = $this->dblink->lastInsertId();
      $this->dblink->commit();
      $data['estado'] = $result;
      $data['poliza_id'] = $poliza_id;
      return $data; //si es correcto el poliza retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function insertarautomatico()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_poliza(
                                tb_poliza_num,
                                tb_cliente_id, 
                                tb_poliza_aseg, 
				tb_poliza_tip,  
				tb_credito_id, 
                                tb_creditotipo_id, 
                                tb_poliza_tipo,
                                tb_poliza_categoria,
                                tb_poliza_his,
                                tb_vehiculomarca_id,
                                tb_vehiculomodelo_id,
                                tb_vehiculo_tasacion,
                                tb_moneda_id,
                                tb_asesor_id,
                                tb_usuario_id,
                                tb_poliza_fecini,
                                tb_poliza_fecfin) 
			VALUES (
                                :poliza_num,
				:cliente_id, 
                                :poliza_aseg, 
				:poliza_tip,  
				:credito_id, 
                                :creditotipo_id, 
                                :poliza_tipo,
                                :poliza_categoria,
                                :poliza_his,
                                :vehiculomarca_id,
                                :vehiculomodelo_id,
                                :vehiculo_tasacion,
                                :moneda_id,
                                :asesor_id,
                                :usuario_id,
                                :poliza_fecini,
                                :poliza_fecfin)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":poliza_num", $this->getPoliza_num(), PDO::PARAM_STR);
      $sentencia->bindParam(":cliente_id", $this->getCliente_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_aseg", $this->getPoliza_aseg(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_tip", $this->getPoliza_tip(), PDO::PARAM_INT);
      $sentencia->bindParam(":credito_id", $this->getCredito_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":creditotipo_id", $this->getCreditotipo_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_tipo", $this->getPoliza_tipo(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_categoria", $this->getPoliza_categoria(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_his", $this->getPoliza_his(), PDO::PARAM_STR);
      $sentencia->bindParam(":vehiculomarca_id", $this->getVehiculomarca_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomodelo_id", $this->getVehiculomodelo_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculo_tasacion", $this->getVehiculo_tasacion(), PDO::PARAM_STR);
      $sentencia->bindParam(":moneda_id", $this->getMoneda_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":asesor_id", $this->getAsesor_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":usuario_id", $this->getUsuario_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_fecini", $this->getPoliza_fecini(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_fecfin", $this->getPoliza_fecfin(), PDO::PARAM_STR);

      $result = $sentencia->execute();
      $poliza_id = $this->dblink->lastInsertId();
      $this->dblink->commit();
      $data['estado'] = $result;
      $data['poliza_id'] = $poliza_id;
      return $data; //si es correcto el poliza retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function editar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = $sql = "UPDATE tb_poliza SET 
                                            tb_poliza_num   =:poliza_num,
                                            tb_cliente_id   = :cliente_id, 
                                            tb_proveedor_id   = :proveedor_id, 
                                            tb_poliza_aseg  = :poliza_aseg,
                                            tb_poliza_tip   = :poliza_tip, 
                                            tb_poliza_fecini =:poliza_fecini,
                                            tb_poliza_fecfin =:poliza_fecfin, 
                                            tb_credito_id    = :credito_id,
                                            tb_creditotipo_id = :creditotipo_id,
                                            tb_poliza_respag =:poliza_respag, 
                                            tb_poliza_respag2 =:poliza_respag2, 
                                            tb_poliza_pre   =:poliza_pre,
                                            tb_poliza_comi  = :poliza_comi,
                                            tb_poliza_tipo  =:poliza_tipo,
                                            tb_poliza_categoria=:poliza_categoria,
                                            tb_vehiculomarca_id=:vehiculomarca_id,
                                            tb_vehiculomodelo_id=:vehiculomodelo_id,
                                            tb_vehiculo_tasacion=:vehiculo_tasacion,
                                            tb_moneda_id=:moneda_id,
                                            tb_poliza_formapago=:poliza_formapago,
                                            tb_asesor_id=:asesor_id,
                                            tb_vendedor_id=:vendedor_id
                                            
                                        WHERE 
                                            tb_poliza_id = :poliza_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":poliza_num", $this->getPoliza_num(), PDO::PARAM_STR);
      $sentencia->bindParam(":cliente_id", $this->getCliente_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":proveedor_id", $this->getProveedor_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_aseg", $this->getPoliza_aseg(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_tip", $this->getPoliza_tip(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_fecini", $this->getPoliza_fecini(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_fecfin", $this->getPoliza_fecfin(), PDO::PARAM_STR);
      $sentencia->bindParam(":credito_id", $this->getCredito_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":creditotipo_id", $this->getCreditotipo_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_respag", $this->getPoliza_respag(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_respag2", $this->getPoliza_respag2(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_pre", $this->getPoliza_pre(), PDO::PARAM_STR);
      //            $sentencia->bindParam(":poliza_cuo", $this->getPoliza_cuo(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_comi", $this->getPoliza_comi(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_tipo", $this->getPoliza_tipo(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_categoria", $this->getPoliza_categoria(), PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomarca_id", $this->getVehiculomarca_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculomodelo_id", $this->getVehiculomodelo_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":vehiculo_tasacion", $this->getVehiculo_tasacion(), PDO::PARAM_STR);
      $sentencia->bindParam(":moneda_id", $this->getMoneda_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_formapago", $this->getPoliza_formapago(), PDO::PARAM_STR);
      $sentencia->bindParam(":asesor_id", $this->getAsesor_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":vendedor_id", $this->getVendedor_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_id", $this->getPoliza_id(), PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  // JUAN 24-11-2023
  function editar_gps_simple($cliente_id, $proveedor_id, $poliza_aseg, $poliza_tip, $poliza_fecini, $poliza_fecfin, $poliza_pre, $poliza_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = $sql = "UPDATE tb_poliza SET 
        tb_cliente_id   = :cliente_id, 
        tb_proveedor_id   = :proveedor_id, 
        tb_poliza_aseg  = :poliza_aseg,
        tb_poliza_tip   = :poliza_tip, 
        tb_poliza_fecini =:poliza_fecini,
        tb_poliza_fecfin =:poliza_fecfin, 
        tb_poliza_pre   =:poliza_pre
    WHERE 
        tb_poliza_id = :poliza_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_aseg", $poliza_aseg, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_tip", $poliza_tip, PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_fecini", $poliza_fecini, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_fecfin", $poliza_fecfin, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_pre", $poliza_pre, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function listar_gestion_cobranza($cliente_id, $fecha1, $fecha2)
  {

    $cliente = '';
    if (intval($cliente_id) > 0) {
      $cliente = ' AND po.tb_cliente_id =:cliente_id';
    }

    try {
      $sql = "SELECT po.*, det.*, tb_cliente_nom, tb_cliente_doc, tb_cliente_tip, tb_cliente_emprs, tb_cliente_empruc, DATEDIFF(tb_polizadetalle_fec, NOW()) AS resta_fecha FROM tb_poliza po 
			INNER JOIN tb_cliente cli on cli.tb_cliente_id = po.tb_cliente_id 
			INNER JOIN tb_polizadetalle det on det.tb_poliza_id = po.tb_poliza_id
			WHERE tb_poliza_xac = 1 AND tb_polizadetalle_fec between :fecha1 AND :fecha2 AND tb_polizadetalle_est = 0" . $cliente;

      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
      if (intval($cliente_id) > 0) {
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Cobros por Realizar";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function  mostrar_poliza_placa($veh_pla)
  {

    try {
      $sql = "SELECT * FROM tb_poliza where UPPER(tb_credito_vehpla) = UPPER(:credito_vehpla)";

      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":credito_vehpla", $veh_pla, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Cobros por Realizar";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  function  mostrarUno($poliza_id)
  {

    try {
      $sql = "SELECT * FROM tb_poliza where tb_poliza_id =:poliza_id;";

      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Cobros por Realizar";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_polizas($cliente_id, $fecha1, $fecha2, $tipo, $categoria)
  {
    try {
      $cliente = '';
      $tipos = '';
      $categorias = '';
      if (intval($cliente_id) > 0) {
        $cliente = ' AND po.tb_cliente_id =:cliente_id';
      }
      if (intval($tipo) > 0) {
        $tipos = ' AND po.tb_poliza_tipo =:tipo';
      }
      if (intval($categoria) > 0) {
        $categorias = ' AND po.tb_poliza_categoria =:categoria';
      }
      $sql = "SELECT 
                                po.*, cli.tb_cliente_nom,cli.tb_cliente_emprs,cli.tb_cliente_tip
                        FROM 
                                tb_poliza po 
                                INNER JOIN tb_cliente cli on cli.tb_cliente_id = po.tb_cliente_id 
                        WHERE  
                                DATE(tb_poliza_reg) BETWEEN :fec1 AND :fec2 AND tb_poliza_xac = 1 " . $cliente . " " . $tipos . " " . $categorias . " Order by 1 desc";

      $sentencia = $this->dblink->prepare($sql);
      if (intval($cliente_id) > 0) {
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      }
      if (intval($tipo) > 0) {
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
      }
      if (intval($categoria) > 0) {
        $sentencia->bindParam(":categoria", $categoria, PDO::PARAM_INT);
      }
      $sentencia->bindParam(":fec1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fec2", $fecha2, PDO::PARAM_STR);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Cobros por Realizar";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostarUno($poliza_id)
  {
    try {
      $sql = "SELECT 
                        po.*, tb_cliente_nom, tb_cliente_doc 
                FROM 
                        tb_poliza po 
			INNER JOIN tb_cliente cli ON cli.tb_cliente_id = po.tb_cliente_id where tb_poliza_id =:poliza_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de Polizas registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostarPorCredito($credito_id)
  {
    try {
      $sql = "SELECT 
                        *
                FROM 
                        tb_poliza
                WHERE tb_credito_id =:credito_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de Polizas registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostarUnoHistorial($poliza_id)
  {
    try {
      $sql = "select tb_poliza_his from tb_poliza WHERE tb_poliza_id =:poliza_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de Polizas registradas";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_campo($poliza_id, $poliza_columna, $poliza_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($poliza_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_poliza SET " . $poliza_columna . " =:poliza_valor WHERE tb_poliza_id =:poliza_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);
        if ($param_tip == 'INT') {
          $sentencia->bindParam(":poliza_valor", $poliza_valor, PDO::PARAM_INT);
        } else {
          $sentencia->bindParam(":poliza_valor", $poliza_valor, PDO::PARAM_STR);
        }

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el poliza retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar_pago($tb_poliza_id, $tb_poliza_columna, $tb_poliza_valor, $param_tip)
  {
    $this->dblink->beginTransaction();
    try {
      if (!empty($tb_poliza_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

        $sql = "UPDATE tb_poliza SET " . $tb_poliza_columna . " = " . $tb_poliza_columna . " + :tb_poliza_valor WHERE tb_poliza_id =:tb_poliza_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_poliza_id", $tb_poliza_id, PDO::PARAM_INT);
        if ($param_tip == 'INT')
          $sentencia->bindParam(":tb_poliza_valor", $tb_poliza_valor, PDO::PARAM_INT);
        else
          $sentencia->bindParam(":tb_poliza_valor", $tb_poliza_valor, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1
      } else
        return 0;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function Actualizar_NCuotas()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_poliza SET 
                                        tb_poliza_cuo = (tb_poliza_cuo +(:poliza_cuo))
                                    WHERE 
                                        tb_poliza_id = :poliza_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":poliza_id", $this->getPoliza_id(), PDO::PARAM_INT);
      $sentencia->bindParam(":poliza_cuo", $this->getPoliza_cuo(), PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1

    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function mostrarPoliza($tb_poliza_num, $tb_poliza__id)
  {
    $poliza = '';

    if ($tb_poliza__id > 0) {
      $poliza = ' AND tb_poliza__id=:tb_poliza__id';
    }
    try {
      $sql = "SELECT * FROM tb_poliza WHERE tb_poliza_num =:tb_poliza_num " . $poliza;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_poliza_num", $tb_poliza_num, PDO::PARAM_STR);
      if ($tb_poliza__id > 0) {
        $sentencia->bindParam(":tb_poliza__id", $tb_poliza__id, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Polizas Registradas en el Sistema";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function complete_det($det)
  {
    $filtro = "%" . $det . "%";
    try {
      $sql = "SELECT 
                        DISTINCT(tb_poliza_aseg) 
                    FROM 
                        tb_poliza
                    WHERE 
                        tb_poliza_aseg like :filtro ORDER BY tb_poliza_aseg LIMIT 0 , 10";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function modificar_his()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_poliza SET 
                                tb_poliza_his=CONCAT(tb_poliza_his,:poliza_his)
                            WHERE 
                                tb_poliza_id =:poliza_id";
      $sentencia = $this->dblink->prepare($sql);

      $poliza_his = $this->getPoliza_his();
      $poliza_id = $this->getPoliza_id();

      $sentencia->bindParam(":poliza_his", $poliza_his, PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);


      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function modificar_his1()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_poliza SET 
                                tb_poliza_his=:poliza_his
                            WHERE 
                                tb_poliza_id =:poliza_id";
      $sentencia = $this->dblink->prepare($sql);

      $sentencia->bindParam(":poliza_his", $this->getPoliza_his(), PDO::PARAM_STR);
      $sentencia->bindParam(":poliza_id", $this->getPoliza_id(), PDO::PARAM_INT);


      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function listar_todas_polizas($fecha1, $fecha2, $cliente_id, $poliza_tipo, $poliza_categoria)
  {

    if ($cliente_id > 0) {
      $concatenar1 = " AND P.tb_cliente_id=:cliente_id ";
    }
    if ($poliza_tipo > 0) {
      $concatenar2 = " AND P.tb_poliza_tipo=:poliza_tipo ";
    }
    if ($poliza_categoria > 0) {
      $concatenar3 = " AND P.tb_poliza_categoria=:poliza_categoria ";
    }


    try {
      $sql = "SELECT
                           P.tb_poliza_id, P.tb_poliza_xac, P.tb_poliza_reg, P.tb_poliza_num, P.tb_cliente_id, 
                           P.tb_poliza_aseg, P.tb_poliza_tip, P.tb_poliza_fecini, P.tb_poliza_fecfin, P.tb_credito_id, 
                           P.tb_creditotipo_id as credtipo, P.tb_poliza_respag, P.tb_poliza_pre, P.tb_poliza_cuo, P.tb_poliza_comi, 
                           P.tb_poliza_estcomi, P.tb_poliza_tipo, P.tb_poliza_categoria, P.tb_poliza_his, P.tb_usuario_id, 
                           P.tb_poliza_ver, P.tb_vehiculomarca_id, P.tb_vehiculomodelo_id, P.tb_vehiculo_tasacion, P.tb_moneda_id, 
                           P.tb_poliza_formapago, P.tb_asesor_id, P.tb_vendedor_id, P.tb_poliza_tipocambio, P.tb_poliza_monven, 
                           P.tb_poliza_monipdn, P.tb_poliza_anulada, P.tb_poliza_respag2, P.tb_egreso_id, P.tb_ingreso_id, 
                           P.tb_poliza_exonerar, C.*
                    FROM
                            tb_poliza P
                            INNER JOIN tb_cliente C on C.tb_cliente_id=P.tb_cliente_id
                    WHERE
                            P.tb_poliza_fecfin  BETWEEN :fecha1 AND :fecha2
                            AND P.tb_poliza_xac =1 AND P.tb_poliza_anulada=1 AND tb_poliza_exonerar=1  AND tb_poliza_visible=1 
                            AND P.tb_poliza_estcomi!=2 " . $concatenar1 . " " . $concatenar2 . ' ' . $concatenar3;

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
      if ($cliente_id > 0) {
        $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      }
      if ($poliza_tipo > 0) {
        $sentencia->bindParam(":poliza_tipo", $poliza_tipo, PDO::PARAM_INT);
      }
      if ($poliza_categoria > 0) {
        $sentencia->bindParam(":poliza_categoria", $concatenar3, PDO::PARAM_INT);
      }
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Cobros por Realizar";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostarCredito_Placa($tabla, $num_placa)
  {
    //m3w193
    try {
      $sql = "SELECT * from " . $tabla . " WHERE tb_credito_vehpla=:credito_pla  AND tb_credito_xac=1 AND tb_credito_est IN(3,4)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":credito_pla", $num_placa, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de Creditos Registrados con ese número de placa";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function mostrar_gps_cliente_tipo_credito($cliente_id, $creditotipo_id)
  {
    try {
      $tabla = "tb_creditogarveh";
      if(intval($creditotipo_id) == 2)
        $tabla = "tb_creditoasiveh";

      $sql = "SELECT tb_poliza_fecini, tb_poliza_fecfin, tb_poliza_aseg, tb_credito_vehpla, po.tb_credito_id FROM tb_poliza po INNER JOIN $tabla tb ON (po.tb_credito_id = tb.tb_credito_id AND po.tb_creditotipo_id = :creditotipo_id) 
        WHERE po.tb_poliza_tipo = 2 AND po.tb_cliente_id =:cliente_id AND tb_poliza_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
      $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de Creditos Registrados con ese número de placa";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
}
