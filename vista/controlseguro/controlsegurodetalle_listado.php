<?php
require_once ("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$poliza_id = intval($_POST['poliza_id']);
$vista = $_POST['vista'];
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_controlsegurodetalle_listado" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold">LISTADO CUOTAS VENCIDAS O POR VENCER</h4>
            </div>

            <form id="form_controlseguro">
                <input type="hidden" name="action" value="<?php echo $action; ?>">
                <input type="hidden" name="hdd_poliza_id" value="<?php echo $poliza_id; ?>">
                <input type="hidden" id="hdd_vista" value="<?php echo $vista; ?>">

                <div class="modal-body">

                    <div class="box box-primary">
                        <div class="box-header">
                                <?php include_once 'controlsegurodetalle_filtro.php'; ?>
                        </div>
                    </div>
                    
                        <!--- MESAJES DE GUARDADO -->
                       <div class="callout callout-info" id="controlsegurodetallereporte_mensaje" style="display: none;">
                           <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                       </div>

                    <div class="box box-danger">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="div_controlsegurodetallereporte_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                                <?php // require_once('controlseguro_tabla.php');    ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                   
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <!--<button type="submit" class="btn btn-info" id="btn_guardar_controlseguro">Guardar</button>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>



        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo 'vista/controlseguro/controlsegurodetalle_listado.js?ver=3302001202202'; ?>"></script>