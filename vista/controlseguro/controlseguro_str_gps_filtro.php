<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="container-fluid">
                    <form id="form_controlsegurodetalle_str_gps_filtro" role="form">
                        <div class="row mt-4">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Fecha:</label>
                                    <div class="input-group">
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control input-sm" name="txt_fil_fec1" id="txt_fil_fec1" value="<?php echo date('01-01-Y'); ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <span class="input-group-addon">-</span>
                                        <div class='input-group date' id='datetimepicker2'>
                                            <input type='text' class="form-control input-sm" name="txt_fil_fec2" id="txt_fil_fec2" value="<?php echo date('d-m-Y'); ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="txt_detalleseguro_fil_cli_nom">Cliente:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-user"></i></span>
                                        </div>
                                        <input type="text" id="txt_detalleseguro_fil_cli_nom" name="txt_fil_cli_nombre" class="form-control input-sm mayus" placeholder="ingrese cliente a buscar" size=50>
                                        <input type="hidden" id="hdd_detalleseguro_fil_cli_id" name="hdd_fil_cli_id">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="cmb_tipo">Tipo de Crédito:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-filter"></i></span>
                                        </div>
                                        <select id="cmb_creditotipodetalle_id" name="cmb_creditotipodetalle_id" class="form-control input-sm">
                                            <option value="" <?php if ($creditotipo_id == "") echo 'selected'; ?>>Selecciona...</option>
                                            <option value="2" <?php if ($creditotipo_id == 2) echo 'selected'; ?>>Asistencia Vehicular</option>
                                            <option value="3" <?php if ($creditotipo_id == 3) echo 'selected'; ?>>Garantía Vehicular</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="cmb_tipo">Control:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-shield"></i></span>
                                        </div>
                                        <select class="form-control input-sm" name="cbo_tipodetalle" id="cbo_tipodetalle">
                                            <option value="1">Seguro Todo Riezgo</option>
                                            <option value="2" selected>GPS</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="cmb_tipo">Categorias:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span><i class="fa fa-tags"></i></span>
                                        </div>
                                        <select class="form-control input-sm" name="cbo_categoriadetalle" id="cbo_categoriadetalle">
                                            <option value="0">Todas las Categorias</option>
                                            <option value="1">Nuevo</option>
                                            <option value="2">Renovación</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="cmb_tipo">&nbsp;</label> <br>
                                    <a href="javascript:void(0)" onClick="controlsegurodetalle_str_gps_tabla()" class="btn btn-primary btn-sm" title="Buscar STR o GPS por Vencer"><i class="fa fa-search fa-fw" aria-hidden="true"></i> Buscar</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>