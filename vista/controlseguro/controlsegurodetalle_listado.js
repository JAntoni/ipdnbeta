/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $vista = $("#hdd_vista").val();
$(document).ready(function () {
    controlsegurodetallereporte_tabla();
    
    
    $('#datetimepicker11, #datetimepicker22').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_ing_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
            controlsegurodetallereporte_tabla();
    });
    
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_ing_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
            controlsegurodetallereporte_tabla();
    });

});

$("#txt_fec1detalle,#txt_fec2detalle,#cbo_tipodetalle,#cbo_categoriadetalle,#cmb_creditotipodetalle_id").change(function (){
            controlsegurodetallereporte_tabla();
});
    $("#txt_detalleseguro_fil_cli_nom").autocomplete({
        minLength: 1,
        source: VISTA_URL + "cliente/cliente_autocomplete.php",
        select: function (event, ui) {
            $("#hdd_detalleseguro_fil_cli_id").val(ui.item.cliente_id);
            //$("#txt_seguro_fil_cli_nom").val(ui.item.nombre);
            //$("#txt_cliente_doc").val(ui.item.documento);
                controlsegurodetallereporte_tabla();
        }
    });



function abrir_pago(polizadetalle_id,poliza_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlsegurodetalle_form.php",
        async: true,
        dataType: "html",
        data: ({
            vista:'controldetallereporte',
            polizadetalle_id: polizadetalle_id,
            poliza_id:poliza_id

        }),
        beforeSend: function (data) {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_agregar_pago').html(data);
            $('#modal_registrodetalle_controlseguro').modal('show');
            modal_height_auto('modal_registrodetalle_controlseguro');
            modal_hidden_bs_modal('modal_registrodetalle_controlseguro', 'limpiar');
            $('#modal_mensaje').modal('hide');
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}



function controlsegurodetallereporte_tabla() {
    var cliente_fil_id = $('#hdd_detalleseguro_fil_cli_id').val();
    var cliente_fil_nom = $('#txt_detalleseguro_fil_cli_nom').val();
    
    if (cliente_fil_nom == "") {
        cliente_fil_id = 0;
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlsegurodetalle_tabla.php",
        async: true,
        dataType: "html",
        data:
                ({
                    fecha1: $("#txt_fec1detalle").val(),
                    fecha2: $("#txt_fec2detalle").val(),
                    cliente_id: cliente_fil_id,
                    poliza_tipo: $("#cbo_tipodetalle").val(),
                    poliza_categoria: $("#cbo_categoriadetalle").val(),
                    tipo_credito: $("#cmb_creditotipodetalle_id").val()

                }),
        beforeSend: function () {
            $('#controlsegurodetallereporte_mensaje').show(400);
        },
        success: function (html) {
            $('#controlsegurodetallereporte_mensaje').hide(400);
            $('#div_controlsegurodetallereporte_tabla').html(html);
            
        },
        complete: function () {
            estilos_datatable();
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tabla_controlsegurodetallereporte').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}


function controlseguro_anular_reporte(poliza_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'PRESTAMOS DEL NORTE',
        content: '¿Desea Anular la Poliza de seguros desde este modulo?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        poliza_id: poliza_id,
                        action: 'anular'
                    }),
                    beforeSend: function (data) {

                    },
                    success: function (data) {
                        if (parseInt(data.estado) > 0) {
                            swal_success("CORRECTO", data.msj, 2500);
//                            controlseguro_tabla();
                            $vista = $("#hdd_vista").val();
                                controlsegurodetallereporte_tabla();
                        } else
                            swal_warning("AVISO", data, 8000);
                    },
                    complete: function (data) {
                        //console.log(data);
                    }
                });
            },
            no: function () {}
        }
    });
}
