


<div class="panel panel-info">
    <div class="panel-body">
        <form id="form_controlsegurodetallereporte_filtro" class="form-inline" role="form">
            <div class="row">
                <div class="col-md-3">
                    <div class="input-group">
                        <div class='input-group date' id='datetimepicker11'>
                            <input type='text' class="form-control input-sm" name="txt_fec1detalle" id="txt_fec1detalle" value="<?php echo date('01-m-Y'); ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <span class="input-group-addon">-</span>
                        <div class='input-group date' id='datetimepicker22'>
                            <input type='text' class="form-control input-sm" name="txt_fec2detalle" id="txt_fec2detalle" value="<?php echo date('d-m-Y'); ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <input type="text" id="txt_detalleseguro_fil_cli_nom" name="txt_detalleseguro_fil_cli_nom"  class="form-control input-sm mayus" placeholder="ingrese cliente a buscar" size=50>
                    <input type="hidden" id="hdd_detalleseguro_fil_cli_id" name="hdd_detalleseguro_fil_cli_id">
                </div>
                <div class="col-sm-2">
                    <select id="cmb_creditotipodetalle_id" name="cmb_creditotipodetalle_id"  class="form-control input-sm">
                        <option value=""<?php if ($creditotipo_id == "") echo 'selected'; ?> >Selecciona...</option>
                        <option value="2" <?php if ($creditotipo_id == 2) echo 'selected'; ?> >Asistencia Vehicular</option>
                        <option value="3" <?php if ($creditotipo_id == 3) echo 'selected'; ?> >Garantía Vehicular</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control input-sm" name="cbo_tipodetalle" id="cbo_tipodetalle">
                        <option value="0">Todos los Tipos</option>
                        <option value="1">Seguro Todo Riezgo</option>
                        <option value="2">GPS</option>
                    </select>
                </div>
                <div class="col-md-1 col-sm-1">
                    <select class="form-control input-sm" name="cbo_categoriadetalle" id="cbo_categoriadetalle">
                        <option value="0">Categorias</option>
                        <option value="1">Nuevo</option>
                        <option value="2">Renovación</option>
                    </select>
                </div>
                
                <div class="col-md-1 col-sm-1">
                       <select class="form-control input-sm" name="cbo_factura_estado" id="cbo_factura_estado">
                           <option value="0">Facturas</option>
                           <option value="1">TIENE</option>
                           <option value="2">NO TIENE</option>
                       </select>
                </div>
                
                <div class="col-md-1">
                    <a href="javascript:void(0)" onClick="facturasegurodetallereporte_tabla()" class="btn btn-warning btn-sm">Buscar11</a>
                </div>
            </div>

        </form>
    </div>
</div>