<?php
$y = date('Y');
$fec1 = '01-01-' . $y;
$fec2 = date('d-m-Y');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="container-lg mb-4">
            <div class="pull-right">
                <!--<button type="button" class="btn btn-primary btn-sm" onclick="creditomenor_form('I', 0)"><i class="fa fa-plus"></i> Nuevo Crédito</button>-->
                <button type="button" class="btn btn-success btn-sm" href="javascript:void(0)" onClick="controlseguro_form('insertar', 0)" title="Agregar un Nuevo Registro al Sistema"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Registro</button>
                <button type="button" class="btn btn-github btn-sm" onClick="STR_GPS_renovacion()" title="Reporte de STR y GPS que estan por vencer y requieren renovación"><i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i> STR_GPS_renovacion</button>
            </div>
        </div> <br>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title text-primary"> <i class="fa fa-filter fa-fw"></i> Filtros </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" data-original-title="Minimizar">
                        <i class="fa fa-minus text-white"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                
                <div class="container-fluid">
                    <?php include_once 'controlseguro_filtro.php'; ?>
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="controlseguro_mensaje_tbl" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                    </div>

                    <div class="row mt-4 mb-4">
                        <div class="col-sm-12">
                            <div id="div_controlseguro_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                <?php // require_once('controlseguro_tabla.php');   
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div id="div_controlsegurodetalle_str_gps"></div>
                <div id="div_controlseguro_form"></div>
                <div id="div_modal_vehiculouso_form"></div>
                <div id="div_modal_proveedorpoliza_form"></div>
                <div id="div_controlseguro_separar"></div>
                <div id="div_vehiculoventa_form"></div>
                <div id="div_controlseguro_detalle"></div>
                <div id="div_controlsegurodetalle_listado"></div>
                <div id="div_controlseguros_his"></div>
                <div id="div_modal_upload_form"></div>
                <div id="div_modal_imagen_form"></div>
                <div id="div_controlsegurodetalle_cupon"></div>
                <div id="div_modal_factura_form"></div>
                <div id="div_modal_uploadpdf_form"></div>
                <div id="div_modal_contratolinea_timeline"></div>
                <div id="div_agregar_pago"></div>
                <div id="div_modal_pagocomisiones_pagar"></div>
                <div id="div_detallepolizahistoria_form"></div>

            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-success btn-sm" id="btn_generarReportExcel"><i class="fa fa-file-text fa-fw"></i> Exportar a Excel </button>
                </div>
            </div>

            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>