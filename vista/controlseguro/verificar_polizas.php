<?php
require_once ("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once ('../cliente/Cliente.class.php');
$oCliente= new cliente();
require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();
require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();
require_once '../vehiculoadicional/Vehiculoadicional.class.php';
$oVehiculoadicional = new Vehiculoadicional();
require_once '../creditogarveh/Creditogarveh.class.php';
$oCreditogarveh = new Creditogarveh();

require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$tipo = intval($_POST['tipo']);
$credito_id = intval($_POST['credito_id']);

$dts = $oPoliza->mostarPorCredito($credito_id);
$seguro = 0;
$gps = 0;
    $data['estado'] = 0;
    $data['msj'] = 'Error al registrar';
    
    if ($dts['estado'] == 1){
        foreach ($dts['data']as $key => $dt) {
            if (intval($dt['tb_poliza_tipo'])==1) {
                $seguro = 1;
            }
            if (intval($dt['tb_poliza_tipo'])==2) {
                $gps = 2;
            }
        }
        
        $data['estado'] = 1;
        $data['seguro'] = $seguro;
        $data['gps'] = $gps;
        $data['credito_id'] = $credito_id;
        $data['msj'] = 'DEVOLUCION DE CONSULTA';
        
    }

    echo json_encode($data);

?>