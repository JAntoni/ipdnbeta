<?php
session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
    die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
require_once ('../../static/libreriasphp/phpexcel/Classes/PHPExcel.php');
// if (defined('APP_URL')) { require_once(APP_URL . 'core/usuario_sesion.php'); } else { require_once('../../core/usuario_sesion.php'); }
require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");
require_once("Poliza.class.php");
$oPoliza = new Poliza();
require_once("Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once('../creditoasiveh/Creditoasiveh.class.php');
$oCreditoasiveh = new Creditoasiveh();
require_once('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();
require_once('../uploadpdf/Upload.class.php');
$oUploapPdf = new Upload();

$cliente_fil_id = intval($_GET['p_cliente_fil_id']);
$fecha1         = fecha_mysql($_GET['p_fecha1']);
$fecha2         = fecha_mysql($_GET['p_fecha2']);
$tipo           = intval($_GET['p_tipo']);
$categoria      = intval($_GET['p_categoria']);

$nombre_archivo = "REPORTE DE CONTROL DE SEGUROS Y GPS - ".mostrar_fecha(date('d-m-Y'));

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("srjuan54@gmail.com")
        ->setLastModifiedBy("srjuan54@gmail.com")
        ->setTitle("Reporte")
        ->setSubject("Reporte")
        ->setDescription("Reporte generado por srjuan54@gmail.com")
        ->setKeywords("")
        ->setCategory("reporte excel");

$estiloTituloColumnas = array(
    'font' => array(
        'name' => 'Arial',
        'bold' => true,
        'size' => 8,
        'color' => array(
            'rgb' => 'FFFFFF'
        )
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array(
            'rgb' => '183959')
    ),
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb' => 'FAFAFA'
            )
        )
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => TRUE
    )
);

//$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//establecer impresion a pagina completa
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

$c = 0;
$c = $c + 1;

$titulosColumnas = array(
    'ID',
    'TIPO',
    'NRO. POLIZA',
    'CLIENTE',
    'RAZÓN SOCIAL',
    'FECHA DE INICIO',
    'FECHA FIN',
    'ID CREDITO',
    'PLACA',
    'TIPO DE CREDITO',
    'SUB GAR',
    'RESPONSABLE PAGO',
    'PRECIO POLIZA',
    'ESTADO COMISION',
    'ASEGURADORA'
);

$objPHPExcel->getActiveSheet()->mergeCells("B$c:C$c");
$objPHPExcel->getActiveSheet()->mergeCells("N$c:O$c");

$objPHPExcel->getActiveSheet()
    ->setCellValue('A' . $c, $titulosColumnas[0])
    ->setCellValue("B$c", $titulosColumnas[1])
    ->setCellValue('D' . $c, $titulosColumnas[2])
    ->setCellValue('E' . $c, $titulosColumnas[3])
    ->setCellValue('F' . $c, $titulosColumnas[4])
    ->setCellValue('G' . $c, $titulosColumnas[5])
    ->setCellValue('H' . $c, $titulosColumnas[6])
    ->setCellValue('I' . $c, $titulosColumnas[7])
    ->setCellValue('J' . $c, $titulosColumnas[8])
    ->setCellValue('K' . $c, $titulosColumnas[9])
    ->setCellValue('L' . $c, $titulosColumnas[10])
    ->setCellValue('M' . $c, $titulosColumnas[11])
    ->setCellValue("N$c", $titulosColumnas[12])
    ->setCellValue('P' . $c, $titulosColumnas[13])
    ->setCellValue('Q' . $c, $titulosColumnas[14])
    ->setCellValue('R' . $c, $titulosColumnas[15])
    ->setCellValue('S' . $c, $titulosColumnas[16])
;

$objPHPExcel->getActiveSheet()->getStyle("A$c:Q$c")->applyFromArray($estiloTituloColumnas);

$suma_poliza = 0;
$fecha = date('d-m-Y');

$dts = $oPoliza->listar_polizas($cliente_id, $fecha1, $fecha2, $tipo, $categoria);
    if ($dts['estado'] == 1) {

        $colorVerde = '10a117';
        $colorFondoVerde = 'ccffcf';
        $colorRojo  = 'ff0000';
        $colorFondoRojo  = 'ffd9d6';
        $colorAzul  = '0000ff';
        $colorFondoAzul  = 'a3e7ff';
        $colorNegro = '1a1a1a';
        $colorFondoNegro = 'FAFAFA';

        $tipo_cre = '';
        $placa = '';
        $credito_subgar = '';
        $razon = '';
        $tipo = '';
        $categoria = '';
        $estado = '';

        foreach ($dts['data'] as $key => $dt) {
            $c++;         
               
            $vencida = 'Vigente';
            $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorVerde)));
            $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($styleColor);
            // cellColor("H$c", $colorFondoVerde);

            if (date('Y-m-d') > $dt['tb_poliza_fecfin']) {
                $vencida = 'Vencida';
                $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorRojo)));
                $objPHPExcel->getActiveSheet()->getStyle("H$c")->applyFromArray($styleColor);
                // cellColor("H$c", $colorFondoRojo);
            }

            $suma_poliza += moneda_mysql($dt['tb_poliza_pre']);

            $tipocredito = intval($dt['tb_creditotipo_id']);

            if ($tipocredito == 2) {
                $tipo_cre = 'ASI. VEH';
                $resultasi = $oCreditoasiveh->mostrarUno($dt['tb_credito_id']);
                    if ($resultasi['estado'] == 1) { $placa = $resultasi['data']['tb_credito_vehpla']; $usureg = $resultasi['data']['tb_credito_usureg']; }
                $resultasi = NULL;
            } elseif ($tipocredito == 3) {
                $tipo_cre = 'GAR. MOB';
                $resultgar = $oCreditogarveh->mostrarUno($dt['tb_credito_id']);
                    if ($resultgar['estado'] == 1) { $placa = $resultgar['data']['tb_credito_vehpla']; $usureg = $resultgar['data']['tb_credito_usureg']; $credito_subgar = $resultgar['data']['tb_credito_subgar']; }
                $resultgar = NULL;
            }

            $responsable = 'IPDN';
            if ($dt['tb_poliza_respag'] == 1) { $responsable = 'El Cliente'; }
            
            if ($dt['tb_poliza_tipo'] == 1 && $credito_subgar == "PRE-CONSTITUCION" && $dt['tb_poliza_categoria'] == 1 && $dt['tb_poliza_estcomi'] == 0) {
                $estado = 'Por Pagar a Vendedor';
                $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorVerde)));
                $objPHPExcel->getActiveSheet()->getStyle("P$c")->applyFromArray($styleColor);
            } else {
                if ($dt['tb_poliza_tipo'] != 2) {
                    $estado = 'Dinero por Ingresar a Caja IPDN';
                    $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorAzul)));
                    $objPHPExcel->getActiveSheet()->getStyle("P$c")->applyFromArray($styleColor);
                }
            }
            if ($dt['tb_poliza_tipo'] == 2) {
                $estado = '';
                $tipo = 'GPS';
                $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorAzul)));
                $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($styleColor);
                // cellColor("B$c", $colorFondoAzul);
            }

            if ($dt['tb_poliza_tipo'] == 1) {
                $tipo = 'STR';
                $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorVerde)));
                $objPHPExcel->getActiveSheet()->getStyle("B$c")->applyFromArray($styleColor);
                // cellColor("B$c", $colorFondoVerde);
            }            

            if ($dt['tb_poliza_categoria'] == 1) {
                $categoria = 'NUEVO';
                $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorRojo)));
                $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($styleColor);
                // cellColor("C$c", $colorFondoRojo);
            }
            if ($dt['tb_poliza_categoria'] == 2) {
                $categoria = 'RENOVACIÓN';
                $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorNegro)));
                $objPHPExcel->getActiveSheet()->getStyle("C$c")->applyFromArray($styleColor);
                // cellColor("C$c", $colorFondoNegro);
            }

            if ($dt['tb_cliente_tip'] == 2) { $razon = $dt['tb_cliente_emprs']; } else { $razon = ''; }

            $objPHPExcel->getActiveSheet(0)
                ->setCellValue('A' . $c, $dt['tb_poliza_id'])
                ->setCellValue('B' . $c, $tipo)
                ->setCellValue('C' . $c, $categoria)
                ->setCellValue('D' . $c, intval($dt['tb_poliza_aseg']))
                ->setCellValue('E' . $c, $dt['tb_cliente_nom'])
                ->setCellValue('F' . $c, $razon)
                ->setCellValue('G' . $c, mostrar_fecha($dt['tb_poliza_fecini']))
                ->setCellValue('H' . $c, mostrar_fecha($dt['tb_poliza_fecfin']) .' '. $vencida)
                ->setCellValue('I' . $c, $dt['tb_credito_id'])
                ->setCellValue('J' . $c, $placa)
                ->setCellValue('K' . $c, $tipo_cre)
                ->setCellValue('L' . $c, $credito_subgar)
                ->setCellValue('M' . $c, $responsable)
                ->setCellValue('N' . $c, 'US$')
                ->setCellValue('O' . $c, mostrar_moneda($dt['tb_poliza_pre']))
                ->setCellValue('P' . $c, $estado)
                ->setCellValue('Q' . $c, $dt['tb_poliza_aseg'])
            ;

            $objPHPExcel->getActiveSheet(0)->getStyle('O'.$c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        }

        $styleColor = array('font' => array( 'color' => array( 'rgb' => $colorAzul)));
        $objPHPExcel->getActiveSheet()->getStyle("O".($c+1))->applyFromArray($styleColor);
        cellColor("O".($c+1), $colorFondoAzul);

        $objPHPExcel->getActiveSheet(0)
            ->setCellValue('A' . ($c+1), '')
            ->setCellValue('B' . ($c+1), '')
            ->setCellValue('C' . ($c+1), '')
            ->setCellValue('D' . ($c+1), '')
            ->setCellValue('E' . ($c+1), '')
            ->setCellValue('F' . ($c+1), '')
            ->setCellValue('G' . ($c+1), '')
            ->setCellValue('H' . ($c+1), '')
            ->setCellValue('I' . ($c+1), '')
            ->setCellValue('J' . ($c+1), '')
            ->setCellValue('K' . ($c+1), '')
            ->setCellValue('L' . ($c+1), '')
            ->setCellValue('M' . ($c+1), '')
            ->setCellValue('N' . ($c+1), 'US$')
            ->setCellValue('O' . ($c+1), moneda_mysql($suma_poliza))
            ->setCellValue('P' . ($c+1), '')
            ->setCellValue('Q' . ($c+1), '')
        ;

        $objPHPExcel->getActiveSheet(0)->getStyle('O'.($c+1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
    }
$dts = null;

for ($z = 'A'; $z <= 'Q'; $z++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
}

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);

// $objPHPExcel->getActiveSheet()->getStyle(
//         'A3:' .
//         $objPHPExcel->getActiveSheet()->getHighestColumn() .
//         $objPHPExcel->getActiveSheet()->getHighestRow()
// )->applyFromArray($styleArray);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('REPORTE DE SEGUROS Y GPS');

//-------------------------------------------------------------------------------------------------------------------------------------------
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

function cellColor($cells, $color) {
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $nombre_archivo . '.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
