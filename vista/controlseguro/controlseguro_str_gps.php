<?php
require_once ("../../core/usuario_sesion.php");
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_controlsegurodetalle_str_gps" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold">LISTADO STR y GPS PRÓXIMOS A VENCER</h4>
            </div>

            <form id="form_controlseguro">
                <div class="modal-body">


                    <div class="box-header">
                        <?php include_once 'controlseguro_str_gps_filtro.php'; ?>
                    </div>

                    <div class="box-body">

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="controlsegurodetalle_str_gps_mensaje" style="display: none;">
                            <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                        </div>

                        <!-- <div class="box box-danger"> -->

                            <div class="row">
                                <div class="col-sm-12">
                                    <p>
                                    <div id="div_controlsegurodetalle_str_gps_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                                        <?php // require_once('controlseguro_tabla.php');     ?>
                                    </div>
                                </div>
                            </div>

                        <!-- </div> -->
                    </div>


                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <!--<button type="submit" class="btn btn-info" id="btn_guardar_controlseguro">Guardar</button>-->
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>



        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo 'vista/controlseguro/controlseguro_str_gps.js?ver=021124'; ?>"></script>