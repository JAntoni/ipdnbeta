<?php
require_once ("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../proveedorpoliza/Proveedorpoliza.class.php");
$oProveedorpoliza = new Proveedorpoliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once ('../cliente/Cliente.class.php');
$oCliente= new cliente();
require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();
require_once '../ingreso/Ingreso.class.php';
$oIngreso = new Ingreso();

require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];

$poliza_num = 0; // este número es para la póliza, para gps no se utilizaría
$cliente_id = intval($_POST['hdd_poliza_cliente_id']); 
$proveedorpoliza_id = intval($_POST['txt_cliente_aseg']); 
$poliza_aseg = '';
$poliza_tip = intval($_POST['cmb_poliza_tip']);
$poliza_fecini = fecha_mysql($_POST['txt_poliza_fecini']); 
$poliza_fecfin = fecha_mysql($_POST['txt_poliza_fecfin']); 
$credito_id = 0; //? EL CREDITO ID AUN NO LO SABREMOS, POR LO QUE SE GUARDARÁ CON UN HASH PARA LUEGO MODIFICAR EL ID
$creditotipo_id = 3; //este módulo siempre será para GARVEH
$poliza_respag = 2; // la factura que se emite será para IPDN
$poliza_respag2 = 2; //el responsable de pago siempre será IPDN
$poliza_pre = moneda_mysql($_POST['txt_poliza_pre']);
$poliza_cuo = 1; //el pago del gps para IPDN siempre se debe pagar en 1 sola cuota
$poliza_comi = 0; // EL GPS no tiene comisión
$poliza_tipo = 2; // TIPO 1 ES POLIZA, 2 ES GPS, SIEMPRE SERÁ TIPO GPS
$poliza_categoria = 1; // 1 GPS nuevo, siempre se considerará GPS NUEVO
$usuario_id = $_SESSION['usuario_id'];
$vehiculomarca_id = '';
$vehiculomodelo_id = '';
$vehiculo_tasacion = '';
$moneda_id = 2; // EL VALOR DEL GPS SIEMPRE SERÁ EN DÓLARES
$poliza_formapago = ''; // solo sirve para POLIZAS
$asesor_id = 0; // no existe asesor en los GPS
$vendedor_id = 0; // tampoco es necesario saber el vendedor

$result2 = $oProveedorpoliza->mostrarUno($proveedorpoliza_id);
  if($result2['estado'] == 1){
    $poliza_aseg = $result2['data']['tb_proveedor_poliza_nom'];
  }
$result2 = NULL;

if($action == 'insertar'){
  $oPoliza->setPoliza_num($poliza_num);
  $oPoliza->setCliente_id($cliente_id);
  $oPoliza->setProveedor_id($proveedorpoliza_id);
  $oPoliza->setPoliza_aseg($poliza_aseg);
  $oPoliza->setPoliza_tip($poliza_tip);
  $oPoliza->setPoliza_fecini($poliza_fecini);
  $oPoliza->setPoliza_fecfin($poliza_fecfin);
  $oPoliza->setCredito_id($credito_id);
  $oPoliza->setCreditotipo_id($creditotipo_id);
  $oPoliza->setPoliza_respag($poliza_respag);
  $oPoliza->setPoliza_respag2($poliza_respag2);
  $oPoliza->setPoliza_pre($poliza_pre);
  $oPoliza->setPoliza_cuo($poliza_cuo);
  $oPoliza->setPoliza_comi($poliza_comi);
  $oPoliza->setPoliza_tipo($poliza_tipo);
  $oPoliza->setPoliza_categoria($poliza_categoria);
  $oPoliza->setUsuario_id($usuario_id);
  $oPoliza->setVehiculomarca_id($vehiculomarca_id);
  $oPoliza->setVehiculomodelo_id($vehiculomodelo_id);
  $oPoliza->setVehiculo_tasacion($vehiculo_tasacion);
  $oPoliza->setMoneda_id($moneda_id);
  $oPoliza->setPoliza_formapago($poliza_formapago);
  $oPoliza->setAsesor_id($asesor_id);
  $oPoliza->setVendedor_id($vendedor_id);

  $poliza_id = 0;
  $result = $oPoliza->insertar();
    if(intval($result['estado']) == 1){
      $poliza_id = $result['poliza_id'];
    }
  $result = NULL;
  
  //? LUEGO DE REGISTRAR EL GPS, CREAMOS UNA CUOTA DE PAGO PARA EL GPS, ES ES POR QUE LAS PÓLIZAS ESTÁN EN CUOTAS, EL GPS SERÍA 1 SOLA SIEMPRE

  $oPolizadetalle->setPoliza_id($poliza_id);
  $oPolizadetalle->setPolizadetalle_fec($poliza_fecini);
  $oPolizadetalle->setPolizadetalle_imp($poliza_pre);
  $oPolizadetalle->setPolizadetalle_cupon(0);

  $oPolizadetalle->insertar_detalle(); // SE GUARDA 1 SOLA CUOTA PARA EL GPS, AQUÍ SE SUBE UNA FOTO DEL PAGO O LA FACTURA EMITIDA

  $oPoliza->modificar_campo($poliza_id, 'tb_poliza_xac', 0, 'INT'); // el GPS se crea en xac = 0 para luego actulizarla con el ID del crédito resultante

  $data['mensaje'] = 'Datos de GPS registrados con éxito, finaliza el crédito para culminar la operación.';
  $data['estado'] = 1;
  $data['poliza_id'] = $poliza_id;

  echo json_encode($data);
}
elseif($action == 'editar'){
  $poliza_id = $_POST['hdd_poliza_id'];

  $oPoliza->editar_gps_simple($cliente_id, $proveedorpoliza_id, $poliza_aseg, $poliza_tip, $poliza_fecini, $poliza_fecfin, $poliza_pre, $poliza_id);
  $oPolizadetalle->modificar_detalle_gps_simpe($poliza_fecini, $poliza_pre, $poliza_id);

  $data['mensaje'] = 'Datos de GPS EDITADOS con éxito, finaliza el crédito para culminar la operación.';
  $data['estado'] = 1;
  $data['poliza_id'] = $poliza_id;
}
?>