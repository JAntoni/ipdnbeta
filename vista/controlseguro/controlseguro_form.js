/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function agregar_pago() {
    var polizadetalle_id = $('#hdd_polizadetalle_id').val();
    var detalle = $('#txt_pago_seguro_det').val();

    $.ajax({
        type: "POST",
        url: "../controlseguro/controlseguro_reg.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'pago',
            polizadetalle_id: polizadetalle_id,
            detalle: detalle
        }),
        beforeSend: function (data) {
            $('#msj_controlseguro_form').show(100);
            $('#msj_controlseguro_form').html("Actualizando...");
        },
        success: function (data) {
            if (parseInt(data.estado) > 0) {
                $('#msj_controlseguro_form').html(data.msj);
                $('#btn_det_' + polizadetalle_id).remove();

                $("#div_agregar_pago").dialog('close');
            } else
                $('#msj_controlseguro_form').html(data.msj);
        },
        complete: function (data) {
            //console.log(data);
        }
    });

}


function vehiculo_modelo_select(marca_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vehiculomodelo/vehiculomodelo_select.php",
        async: true,
        dataType: "html",
        data: ({
            vehiculomarca_id: marca_id
        }),
        beforeSend: function () {
            $('#txt_vehiculomodelo_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#txt_vehiculomodelo_id').html(data);
        },
        complete: function (data) {
            //console.log(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}


$('#txt_vehiculomarca_id').change(function () {
    var marca_id = $(this).val();
    vehiculo_modelo_select(marca_id);
});




$("#txt_vendedor_nombre").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $.getJSON(
                VISTA_URL + "vendedor/vendedor_autocomplete.php",
                {term: request.term}, //1 solo clientes de la empresa
                response
                );
    },
    select: function (event, ui) {
        $('#hdd_vendedor_id').val(ui.item.vendedor_id);
        $('#txt_vendedor_dni').val(ui.item.vendedor_dni);
        $('#txt_vendedor_nombre').val(ui.item.value);
        $('#txt_vendedor_telefono').val(ui.item.vendedor_tel);
        $('#txt_vendedor_direccion').val(ui.item.vendedor_dir);
        $('#txt_vendedor_tienda').val(ui.item.vendedor_tie);
    }
});
$("#txt_vendedor_dni").autocomplete({
    minLength: 3,
    source: function (request, response) {
        $.getJSON(
                VISTA_URL + "vendedor/vendedor_autocomplete.php",
                {term: request.term}, //1 solo clientes de la empresa
                response
                );
    },
    select: function (event, ui) {
        event.preventDefault();
        $('#hdd_vendedor_id').val(ui.item.vendedor_id);
        $('#txt_vendedor_dni').val(ui.item.vendedor_dni);
        $('#txt_vendedor_nombre').val(ui.item.value);
        $('#txt_vendedor_telefono').val(ui.item.vendedor_tel);
        $('#txt_vendedor_direccion').val(ui.item.vendedor_dir);
        $('#txt_vendedor_tienda').val(ui.item.vendedor_tie);
    }
});



function Validar(poliza_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'PRESTAMOS DEL NORTE',
        content: '¿Esta Seguro que desea Finalizar la Edicion de los datos de la Poliza?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        poliza_id: poliza_id,
                        action: 'verificar'
                    }),
                    beforeSend: function (data) {

                    },
                    success: function (data) {
                        if (parseInt(data.estado) > 0) {
                            swal_success("CORRECTO", data.msj, 2500);
                            $('#modal_registro_controlseguro').modal('hide');
                            controlseguro_tabla();
                        } else
                            swal_warning("AVISO", data, 8000);
                    },
                    complete: function (data) {
                        //console.log(data);
                    }
                });
            },
            no: function () {}
        }
    });
}



//function abrir_pago(polizadetalle_id) {
//    $('#hdd_polizadetalle_id').val(polizadetalle_id);
//    $("#div_agregar_pago").dialog('open');
//      
//}

function abrir_pago(polizadetalle_id,poliza_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlsegurodetalle_form.php",
        async: true,
        dataType: "html",
        data: ({
            polizadetalle_id: polizadetalle_id,
            poliza_id:poliza_id

        }),
        beforeSend: function (data) {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_agregar_pago').html(data);
            $('#modal_registrodetalle_controlseguro').modal('show');
            modal_height_auto('modal_registrodetalle_controlseguro');
            modal_hidden_bs_modal('modal_registrodetalle_controlseguro', 'limpiar');
            $('#modal_mensaje').modal('hide');
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}

function modificar(polizadetalle_id,poliza_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlsegurodetalle_form_edit.php",
        async: true,
        dataType: "html",
        data: ({
            polizadetalle_id: polizadetalle_id,
            poliza_id:poliza_id

        }),
        beforeSend: function (data) {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_controlsegurodetalle_cupon').html(data);
            $('#modal_controlsegurodetalle').modal('show');
            modal_height_auto('modal_controlsegurodetalle');
            modal_hidden_bs_modal('modal_controlsegurodetalle', 'limpiar');
            $('#modal_mensaje').modal('hide');
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}

function agregar_detalle() {
    var cuotas = $('#txt_poliza_cuo').val();
    var tabla = '';
    var precio = Number($('#txt_poliza_pre').autoNumeric('get'));
    var total = precio;
    var preccuota = total/cuotas;
    var fechinicio = $('#txt_poliza_fecini').val();
    //fechinicio = fechinicio.toISOString().split('T')[0];
    var arrinicio = fechinicio.split('-');
    $('#tabla_detalle').html(tabla);
    var rowCount = $("#tabla_detalle tr").length;
        
    if (rowCount == 0) {
        if (parseInt(cuotas) > 0) {
            var mes = parseInt(arrinicio[1]);
            var año = parseInt(arrinicio[2]);
            var mess = mes;
            for (var i = 1; i <= parseInt(cuotas); i++) {
                  mess = parseInt(mess) + 1;
                 if(mess > 12){
                     año = año +1;
                     mess = 1;
                 }
                 if(mess < 10){
                     mess = '0' + mess;
                 }
                var diainicio = arrinicio[0]+'-'+mess+'-'+año;
                
                tabla += '<tr id="tabla_cabecera_fila">';
                tabla += '<td id="tabla_fila">' + i + '</td>';
                tabla += '<td id="tabla_fila"><input class="form-control input-sm cupon_poliza" type="text" name="txt_polizadetalle_cup[]" value="0"></td>';
                tabla += '<td id="tabla_fila"><input class="form-control input-sm fecha_detalle" type="text" name="txt_polizadetalle_fec[]" value='+diainicio+'></td>';
                tabla += '<td id="tabla_fila"><input class="form-control input-sm cuota_poliza" type="text" name="txt_polizadetalle_imp[]" value='+preccuota+'></td>';
                tabla += '<td id="tabla_fila" style="color: red;">Pendiente</td>';
                tabla += '<td id="tabla_fila"></td>';
                tabla += '<td id="tabla_fila"></td>';
                tabla += '</tr>';
            }
            $('#tabla_detalle').html(tabla);

        } else {
            swal_warning("AVISO", "DEBE INGRESAR NÚMERO MAYOR A 0");
            return false;
        }
    } else {
        if (parseInt(cuotas) > 0) {
            for (var i = 1; i <= parseInt(cuotas); i++) {
                tabla += '<tr id="tabla_cabecera_fila">';
                tabla += '<td id="tabla_fila">' + (parseInt(rowCount) + parseFloat(i)) + '</td>';
                tabla += '<td id="tabla_fila"><input class="form-control input-sm cupon_poliza" type="text" name="txt_polizadetalle_cup[]" ></td>';
                tabla += '<td id="tabla_fila"><input class="form-control input-sm fecha_detalle" type="text" name="txt_polizadetalle_fec[]" ></td>';
                tabla += '<td id="tabla_fila"><input class="form-control input-sm cuota_poliza" type="text" name="txt_polizadetalle_imp[]" ></td>';
                tabla += '<td id="tabla_fila" style="color: red;">Pendiente</td>';
                tabla += '<td id="tabla_fila"></td>';
                tabla += '<td id="tabla_fila"></td>';
                tabla += '</tr>';
            }
            $('#tabla_detalle').append(tabla);
        } else {
            swal_warning("AVISO", "DEBE INGRESAR NÚMERO MAYOR A 0");
            return false;
        }
    }


    $('#tabla_detalle').find('.fecha_detalle').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $('#tabla_detalle').find('.cuota_poliza').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });
}

function abrirfactura_form(usuario_act, factura_id,polizadetalle_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "factura/factura_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            factura_id: factura_id,
            polizadetalle_id:polizadetalle_id,
            vista:'control_seguro_detalle'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_factura_form').html(data);
                $('#modal_registro_factura').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_factura'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_factura', 'limpiar'); //funcion encontrada en public/js/generales.js
            } 
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}


$(document).ready(function () {

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
    
    $('input[type="radio"].flat-green').on('ifChecked', function(event){
        var valor = $(this).val();
        
        if(valor == 2){
            $('#txt_poliza_num').prop( "readonly", true );
            $('#cate').css("display", "block");
            $('#cliente_aseg').text("Proveedor:");
            
//            $('#txt_poliza_cate2').attr('readonly', true);
        }
        else{
            $('#txt_poliza_num').prop( "disabled", false );
//            $('#cate').css("display", "none");
            $('#cliente_aseg').text("Aseguradora:");
        }
      });
      
      
      $('#txt_cliente_aseg').autocomplete({
		minLength: 1,
		source: VISTA_URL+"controlseguro/controlseguro_complete_det.php"
	});

    $('#txt_poliza_pre, #txt_poliza_comi').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '99999.00'
    });

    $('.numero').autoNumeric({
        aSep: '',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0',
        vMax: '99999999'
    });
    
    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });

    $('#datetimepicker1,#datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

//    $("#txt_cliente_doc").autocomplete({
//        minLength: 1,
//        source: VISTA_URL + "cliente/cliente_autocomplete.php",
//        select: function (event, ui) {
//            $("#hdd_cliente_id").val(ui.item.cliente_id);
//            $("#txt_cliente_nom").val(ui.item.cliente_nom);
//            $("#txt_cliente_doc").val(ui.item.cliente_doc);
//        }
//    });
//    $("#txt_cliente_nom").autocomplete({
//        minLength: 1,
//        source: VISTA_URL + "cliente/cliente_autocomplete.php",
//        select: function (event, ui) {
//            $("#hdd_cliente_id").val(ui.item.cliente_id);
//            $("#txt_cliente_nom").val(ui.item.cliente_nom+" /// "+ui.item.cliente_id);
//            $("#txt_cliente_doc").val(ui.item.cliente_doc);
//        }
//    });
    /* ANTHONY  */
    $('#txt_poliza_fecini').change(function (e) {
        var fechinicio = $(this).val();
        var arrinicio = fechinicio.split('-');
        var anio = parseInt(arrinicio[2]);
        anio = anio +1 ;

        var diainicio = arrinicio[0]+'-'+arrinicio[1]+'-'+anio;
        $("#txt_poliza_fecfin").val(diainicio);
        //revisar_fechas(fechinicio,fechfin,'inicio');
    });
    
    
    /* ANTHONY  */
    $('#txt_poliza_fecini,#txt_poliza_fecfin,#txt_poliza_pre,#txt_poliza_cuo').change(function (e) {
        var fechinicio = $('#txt_poliza_fecini').val();
        var fechfin = $('#txt_poliza_fecfin').val();
        var cuotas = Number($('#txt_poliza_cuo').val());
        var precio = Number($('#txt_poliza_pre').autoNumeric('get'));
        
        if(fechinicio!='' && fechfin!='' && cuotas!=0 && precio!=0){
           agregar_detalle();
        }
    });
    
    $("#txt_cliente_doc,#txt_cliente_nom").autocomplete({
        minLength: 1,
        source: function(request, response){
            $.getJSON(
                    VISTA_URL+"cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
            );
        },
        select: function(event, ui){
            $('#hdd_cliente_id').val(ui.item.cliente_id);
            $('#txt_cliente_doc').val(ui.item.cliente_doc);
            $('#txt_cliente_nom').val(ui.item.cliente_nom);
          event.preventDefault();
            $('#txt_cliente_nom').focus();
        }
    });
    
    
    

    $("#form_controlseguro").validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                async: true,
                dataType: "json",
                data: $('#form_controlseguro').serialize(),
                beforeSend: function () {

                },
                success: function (data) {
                    if (parseInt(data.estado) == 1) {
                        swal_success("CORRECTO", data.msj, 3000);
                        $('#modal_registro_controlseguro').modal('hide');
                        var vista = $('#hdd_vista').val();
                        if (vista == 'control')
                            controlseguro_tabla();
                        if (vista == 'gcseguro')
                            gcseguro_tabla();
                        if (vista == 'creditogarveh')
                            verificar(data.tipo, data.credito_id);
                        
                    } else {
                        swal_warning("AVISO", data.msj, 3000);
                    }
                },
                complete: function (data) {
                    // console.log(data.mensaje);
//                    if (data.statusText != "success") {
//                        swal_warning("AVISO",data.responseText,5000);
//                    }
                }
            });
        },
        rules: {
            txt_poliza_num: {
                required: true
            },
            txt_cliente_nom: {
                required: true
            },
            txt_cliente_doc: {
                required: true
            },
            txt_cliente_aseg: {
                required: true
            },
            cmb_poliza_tip: {
                required: true
            },
            txt_poliza_fecini: {
                required: true
            },
            txt_poliza_fecfin: {
                required: true
            },
            txt_credito_id: {
                required: true
            },
            cmb_creditotipo_id: {
                required: true
            },
            cmb_poliza_respag: {
                required: true
            },
            txt_poliza_pre: {
                required: true
            },
            txt_poliza_cuo: {
                required: true
            },
        },
        messages: {
            txt_poliza_num: {
                required: 'N° de Póliza'
            },
            txt_cliente_nom: {
                required: 'Busca un cliente'
            },
            txt_cliente_doc: {
                required: 'Busca un cliente'
            },
            txt_cliente_aseg: {
                required: 'Aseguradora'
            },
            cmb_poliza_tip: {
                required: 'Tipo de Uso del Vehículo'
            },
            txt_poliza_fecini: {
                required: 'Fecha de Inicio'
            },
            txt_poliza_fecfin: {
                required: 'Fecha Fin'
            },
            txt_credito_id: {
                required: 'Ingresa el ID del Crédito'
            },
            cmb_creditotipo_id: {
                required: 'Tipo de Crédito'
            },
            cmb_poliza_respag: {
                required: 'Responsable del Pago'
            },
            txt_poliza_pre: {
                required: 'Precio de Póliza'
            },
            txt_poliza_cuo: {
                required: 'Número de Cuotas'
            },
        }
    });


});






function upload_form(upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'I', // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'controlsegurodetalle', //nombre de la tabla a relacionar
            modulo_id: upload_id, //aun no se guarda este modulo
//                modulo_id: 0, //aun no se guarda este modulo
            upload_uniq: $('#upload_uniq').val() //ID temporal
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_upload_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function upload_galeria(controlsegurodetalle_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "upload/upload_galeria.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'controlsegurodetalle', //nombre de la tabla a relacionar
            modulo_id: controlsegurodetalle_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.galeria').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
            swal_error('ERRROR', data.responseText, 5000);
        }
    });
}

function Abrir_imagen(controlsegurodetalle_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlsegurodetalle_img.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cerrar',
            empresa_id: $("#cmb_fil_empresa_id").val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_modal_imagen_form').html(html);
            $('#modal_controlsegurodetalle_img').modal('show');
            modal_hidden_bs_modal('modal_controlsegurodetalle_img', 'limpiar'); //funcion encontrada en public/js/generales.js
            upload_galeria(controlsegurodetalle_id);
        },
        complete: function (html) {
//            console.log(html);
        }
    });
}

function contratos_SegurosDetalle_form(controlsegurosdetalle_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"controlseguro/controlseguros_contratos.php",
    async: true,
    dataType: "html",
    data: ({
      controlsegurosdetalle_id: controlsegurosdetalle_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#div_modal_contratolinea_timeline').html(data);
      $('#modal_contratoseguguro_pdf').modal('show');
      modal_height_auto('modal_contratoseguguro_pdf'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_contratoseguguro_pdf', 'limpiar'); //funcion encontrada en public/js/generales.js
      uploadd_pdf(controlsegurosdetalle_id);
    },
    complete: function(data){
      $('#modal_mensaje').modal('hide');
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}



function uploadd_pdf(controlsegurosdetalle_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "uploadpdf/upload_contratos_seguros_gps.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'segurosdetalle', //nombre de la tabla a relacionar
            modulo_id: controlsegurosdetalle_id,
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.pdf').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
        }
    });
}


function his_poliza(poliza_id,polizadetalle_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"controlseguro/polizadetalle_historia.php",
		async:true,
		dataType: "html",                      
		data: ({
                        action:'agregar_nota',
			poliza_id:poliza_id,
			polizadetalle_id:polizadetalle_id

		}),
		beforeSend: function() {
                    $('#h3_modal_title').text('Cargando Formulario');
                    $('#modal_mensaje').modal('show');
                },
		success: function(html){
                    $('#div_detallepolizahistoria_form').html(html);		
                    $('#modal_registro_comentario_polizadetalle').modal('show');
                    $('#modal_mensaje').modal('hide');
                        //funcion js para limbiar el modal al cerrarlo
                    modal_hidden_bs_modal('modal_registro_comentario_polizadetalle', 'limpiar'); //funcion encontrada en public/js/generales.js
                    //funcion js para agregar un largo automatico al modal, al abrirlo
                    modal_height_auto('modal_registro_comentario_polizadetalle'); //funcion encontrada en public/js/generales.js

		},
                complete: function (html) {

                }
	});
}

function vehiculouso_form3(usuario_act, vehiculouso_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"vehiculouso/vehiculouso_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: 'controlseguro',
      vehiculouso_id: vehiculouso_id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_vehiculouso_form').html(data);
      	$('#modal_registro_vehiculouso').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_vehiculouso'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_vehiculouso', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'vehiculouso';
      	var div = 'div_modal_vehiculouso_form';
      	permiso_solicitud(usuario_act, vehiculouso_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}

function cargar_vehiculousoo(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "vehiculouso/vehiculouso_select.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#cmb_poliza_tip').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_poliza_tip').html(html);
        }
    });
}

function cargar_proveedorpoliza(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "proveedorpoliza/proveedorpoliza_select.php",
        async: true,
        dataType: "html",
        data: ({}),
        beforeSend: function () {
            $('#txt_cliente_aseg').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#txt_cliente_aseg').html(html);
        }
    });
}

function proveedorpoliza_form3(usuario_act, proveedorpoliza_id){ 
  $.ajax({
		type: "POST",
		url: VISTA_URL+"proveedorpoliza/proveedorpoliza_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vista: 'controlseguro',
      proveedorpoliza_id: proveedorpoliza_id,
      vista: 'proveedorpoliza'
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_proveedorpoliza_form').html(data);
      	$('#modal_registro_proveedorpoliza').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_proveedorpoliza'); //funcion encontrada en public/js/generales.js
      	modal_hidden_bs_modal('modal_registro_proveedorpoliza', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'proveedorpoliza';
      	var div = 'div_modal_proveedorpoliza_form';
      	permiso_solicitud(usuario_act, proveedorpoliza_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
		},
		complete: function(data){
			
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}