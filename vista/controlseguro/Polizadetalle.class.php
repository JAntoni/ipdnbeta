<?php

if (defined('APP_URL')){
    require_once(APP_URL . 'datos/conexion.php');
}
else{
    require_once('../../datos/conexion.php');
}

class Polizadetalle extends Conexion {

    private $polizadetalle_id;
    private $poliza_id;
    private $polizadetalle_fec;
    private $polizadetalle_imp;
    private $polizadetalle_est;
    private $polizadetalle_det;
    private $polizadetalle_cupon;

    public function getPolizadetalle_id() {
        return $this->polizadetalle_id;
    }

    public function getPoliza_id() {
        return $this->poliza_id;
    }

    public function getPolizadetalle_fec() {
        return $this->polizadetalle_fec;
    }

    public function getPolizadetalle_imp() {
        return $this->polizadetalle_imp;
    }

    public function getPolizadetalle_est() {
        return $this->polizadetalle_est;
    }

    public function getPolizadetalle_det() {
        return $this->polizadetalle_det;
    }

    public function getPolizadetalle_cupon() {
        return $this->polizadetalle_cupon;
    }

    public function setPolizadetalle_id($polizadetalle_id) {
        $this->polizadetalle_id = $polizadetalle_id;
    }

    public function setPoliza_id($poliza_id) {
        $this->poliza_id = $poliza_id;
    }

    public function setPolizadetalle_fec($polizadetalle_fec) {
        $this->polizadetalle_fec = $polizadetalle_fec;
    }

    public function setPolizadetalle_imp($polizadetalle_imp) {
        $this->polizadetalle_imp = $polizadetalle_imp;
    }

    public function setPolizadetalle_est($polizadetalle_est) {
        $this->polizadetalle_est = $polizadetalle_est;
    }

    public function setPolizadetalle_det($polizadetalle_det) {
        $this->polizadetalle_det = $polizadetalle_det;
    }

    public function setPolizadetalle_cupon($polizadetalle_cupon) {
        $this->polizadetalle_cupon = $polizadetalle_cupon;
    }

    function insertar_detalle() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_polizadetalle (
                                                tb_poliza_id, 
                                                tb_polizadetalle_fec, 
                                                tb_polizadetalle_imp,
                                                tb_polizadetalle_cupon) 
                                        VALUES(
                                                :poliza_id, 
                                                :polizadetalle_fec, 
                                                :polizadetalle_imp,
                                                :polizadetalle_cupon)";

            $sentencia = $this->dblink->prepare($sql);
            $polizadetalle_fec = $this->getPolizadetalle_fec();
            $polizadetalle_imp = $this->getPolizadetalle_imp();
            $polizadetalle_cupon = $this->getPolizadetalle_cupon();

            $sentencia->bindParam(":poliza_id", $this->getPoliza_id(), PDO::PARAM_INT);
            $sentencia->bindParam(":polizadetalle_fec", $polizadetalle_fec, PDO::PARAM_STR);
            $sentencia->bindParam(":polizadetalle_imp", $polizadetalle_imp, PDO::PARAM_STR);
            $sentencia->bindParam(":polizadetalle_cupon", $polizadetalle_cupon, PDO::PARAM_STR);

            $result = $sentencia->execute();
            $polizadetalle_id = $this->dblink->lastInsertId();
            $this->dblink->commit();
            $data['estado'] = $result;
            $data['polizadetalle_id'] = $polizadetalle_id;
            return $data; //si es correcto el mejoragc retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar_detalle() {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_polizadetalle WHERE tb_poliza_id =:poliza_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":poliza_id", $this->getPoliza_id(), PDO::PARAM_INT);

            $result = $sentencia->execute();
            $this->dblink->commit();
            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function listar_detalle_poliza($poliza_id) {

        try {
            $sql = "SELECT  DISTINCT * FROM tb_poliza po INNER JOIN tb_polizadetalle det on po.tb_poliza_id = det.tb_poliza_id WHERE po.tb_poliza_id =:poliza_id AND det.tb_polizadetalle_xac=1";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Cobros por Realizar";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_todosdetalle_poliza($fecha1, $fecha2, $cliente_id, $poliza_tipo, $poliza_categoria, $tipo_credito) {
        $estadopago = ' AND PD.tb_polizadetalle_est=0';
        if ($cliente_id > 0) {
            $concatenar1 = " AND P.tb_cliente_id=:cliente_id ";
        }
        if ($poliza_tipo > 0) {

            $concatenar2 = " AND P.tb_poliza_tipo=:poliza_tipo ";

            // if ($poliza_tipo == 2) {
            //     $estadopago = '';
            // }
        }
        if ($poliza_categoria == 0) {
            $concatenar3 = " AND P.tb_poliza_exonerar=1 ";
        }
        if ($poliza_categoria == 1 || $poliza_categoria == 2) {
            $concatenar3 = " AND P.tb_poliza_categoria=:poliza_categoria AND P.tb_poliza_exonerar=1 ";
        }
        if ($poliza_categoria == 3) {
            $concatenar3 = " AND P.tb_poliza_exonerar=2 ";
        }
        if ($tipo_credito > 0) {
            $concatenar4 = " AND P.tb_creditotipo_id=:creditotipo_id ";
        }

        try {
            $sql = "SELECT
                            * 
                    FROM
                            tb_polizadetalle PD
                            INNER JOIN tb_poliza P ON P.tb_poliza_id = PD.tb_poliza_id 
                            INNER JOIN tb_cliente C on C.tb_cliente_id=P.tb_cliente_id
                    WHERE
                            PD.tb_polizadetalle_fec BETWEEN :fecha1 AND :fecha2  
                            AND P.tb_poliza_xac =1 AND P.tb_poliza_anulada=1 AND PD.tb_polizadetalle_xac=1 
                            AND P.tb_poliza_estcomi!=2 " . $estadopago . $concatenar1 . " " . $concatenar2 . " " . $concatenar3 . " " . $concatenar4 . ' order by tb_polizadetalle_fec asc';

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            if ($cliente_id > 0) {
                $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            }
            if ($poliza_tipo > 0) {
                $sentencia->bindParam(":poliza_tipo", $poliza_tipo, PDO::PARAM_INT);
            }
            if ($poliza_categoria == 1 || $poliza_categoria == 2) {
                $sentencia->bindParam(":poliza_categoria", $poliza_categoria, PDO::PARAM_INT);
            }
            if ($tipo_credito > 0) {
                $sentencia->bindParam(":creditotipo_id", $tipo_credito, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Cobros por Realizar";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

//    function listar_facturasdetalle_poliza($fecha1, $fecha2, $cliente_id, $poliza_tipo, $poliza_categoria, $tipo_credito, $factura_estado) {
    function listar_facturasdetalle_poliza($fecha1, $fecha2, $cliente_id, $poliza_tipo, $poliza_categoria, $tipo_credito) {
        $concatenar5=" ";
        if ($cliente_id > 0) {
            $concatenar1 = " AND C.tb_cliente_id=:cliente_id ";
        }
        if ($poliza_tipo > 0) {
            $concatenar2 = " AND P.tb_poliza_tipo=:poliza_tipo ";
        }
        if ($poliza_categoria > 0) {
            $concatenar3 = " AND P.tb_poliza_categoria=:poliza_categoria ";
        }
        if ($tipo_credito > 0) {
            $concatenar4 = " AND P.tb_creditotipo_id=:creditotipo_id ";
        }

//        if ($factura_estado > 0) {
//            if ($factura_estado == 1) {
//                $concatenar5 = " AND tb_factura_id IS NOT NULL ";
//            } else {
//                if ($factura_estado == 2) {
//                    $concatenar5 = " AND tb_factura_id IS NULL";
//                }
//            }
//        }
//         INNER JOIN upload U ON PD.tb_polizadetalle_id=U.modulo_id 
//                            LEFT JOIN tb_factura F ON F.tb_uploap_id=U.upload_id

        try {
            $sql = "SELECT
                            DISTINCT  *
                    FROM
                            tb_poliza P 
                            INNER JOIN tb_polizadetalle PD ON PD.tb_poliza_id = P.tb_poliza_id 
                            INNER JOIN tb_cliente C ON C.tb_cliente_id=P.tb_cliente_id                           
                    WHERE
                            PD.tb_polizadetalle_fec BETWEEN :fecha1 AND :fecha2  
                            AND P.tb_poliza_xac =1 AND PD.tb_polizadetalle_xac =1 
                            AND PD.tb_polizadetalle_est = 1  " . $concatenar1 . " " . $concatenar2 . " " . $concatenar3 . " " . $concatenar4 . " " . ' order by PD.tb_polizadetalle_fec asc';

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            if ($cliente_id > 0) {
                $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            }
            if ($poliza_tipo > 0) {
                $sentencia->bindParam(":poliza_tipo", $poliza_tipo, PDO::PARAM_INT);
            }
            if ($poliza_categoria > 0) {
                $sentencia->bindParam(":poliza_categoria", $poliza_categoria, PDO::PARAM_INT);
            }
            if ($tipo_credito > 0) {
                $sentencia->bindParam(":creditotipo_id", $tipo_credito, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Cobros por Realizar";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function modificar_campo_detalle($polizadetalle_id, $polizadetalle_columna, $polizadetalle_valor, $param_tip) {
        $this->dblink->beginTransaction();
        try {
            if (!empty($polizadetalle_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {

                $sql = "UPDATE tb_polizadetalle SET " . $polizadetalle_columna . " =:polizadetalle_valor WHERE tb_polizadetalle_id =:polizadetalle_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":polizadetalle_id", $polizadetalle_id, PDO::PARAM_INT);
                if ($param_tip == 'INT') {
                    $sentencia->bindParam(":polizadetalle_valor", $polizadetalle_valor, PDO::PARAM_INT);
                } else {
                    $sentencia->bindParam(":polizadetalle_valor", $polizadetalle_valor, PDO::PARAM_STR);
                }

                $result = $sentencia->execute();

                $this->dblink->commit();

                return $result; //si es correcto el poliza retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar_campo_nota($polizadetalle_id, $polizadetalle_columna, $polizadetalle_valor) {
        $this->dblink->beginTransaction();
        try {
                $sql = "UPDATE tb_polizadetalle SET $polizadetalle_columna = CONCAT(COALESCE($polizadetalle_columna, ''), :polizadetalle_valor) WHERE tb_polizadetalle_id = :polizadetalle_id";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":polizadetalle_valor", $polizadetalle_valor, PDO::PARAM_STR);
                $sentencia->bindParam(":polizadetalle_id", $polizadetalle_id, PDO::PARAM_INT);

                $result = $sentencia->execute();
                $this->dblink->commit();

                return $result; //si es correcto el poliza retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostarUno($polizadetalle_id) {
        try {
            $sql = "SELECT 
                        * 
                FROM 
                        tb_polizadetalle  where tb_polizadetalle_id =:polizadetalle_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":polizadetalle_id", $polizadetalle_id, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay detalle de Polizas registradas";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    //JUAN 24-11-2023
    function modificar_detalle_gps_simpe($polizadetalle_fec, $polizadetalle_imp, $poliza_id) {
        $this->dblink->beginTransaction();
        try {
                $sql = "UPDATE tb_polizadetalle SET
                    tb_polizadetalle_fec =:polizadetalle_fec,
                    tb_polizadetalle_imp =:polizadetalle_imp 
                    WHERE tb_poliza_id =:poliza_id";
                
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":polizadetalle_fec", $polizadetalle_fec, PDO::PARAM_STR);
                $sentencia->bindParam(":polizadetalle_imp", $polizadetalle_imp, PDO::PARAM_STR);
                $sentencia->bindParam(":poliza_id", $poliza_id, PDO::PARAM_INT);

                $result = $sentencia->execute();
                $this->dblink->commit();

                return $result; //si es correcto el poliza retorna 1

        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }
}
