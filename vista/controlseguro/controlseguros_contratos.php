<?php

?>

<script>
function Eliminar_pdf(upload_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'PRESTAMOS DEL NORTE',
        content: '¿Desea Eliminar este PDF?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "uploadpdf/upload_controller.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        upload_id: upload_id,
//                        action: 'anular'
                        action: 'eliminar'
                    }),
                    beforeSend: function (data) {

                    },
                    success: function (data) {
                        if (parseInt(data.estado) > 0) {
                            swal_success("CORRECTO", data.msj, 2500);
                            $('#modal_contratoseguguro_pdf').modal('hide');
                            controlseguro_tabla();
                        } else
                            swal_warning("AVISO", data, 8000);
                    },
                    complete: function (data) {
                        //console.log(data);
                    }
                });
            },
            no: function () {}
        }
    });
}
</script>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_contratoseguguro_pdf" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000099">HISTORIAL DE CONTRATO(s) EN PDF</h4>
            </div>
            <div class="modal-body">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">

                        <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
                            <div class="btn-group" data-toggle="btn-toggle">

                            </div>
                        </div>
                    </div>

                    <div class="box-body chat pdf" id="chat-box">

                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>



