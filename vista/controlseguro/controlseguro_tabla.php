<?php
    if (defined('APP_URL')) { require_once(APP_URL . 'core/usuario_sesion.php'); } else { require_once('../../core/usuario_sesion.php'); }
    require_once("../funciones/funciones.php");
    require_once("../funciones/fechas.php");
    require_once("Poliza.class.php");
    $oPoliza = new Poliza();
    require_once("Polizadetalle.class.php");
    $oPolizadetalle = new Polizadetalle();
    require_once('../creditoasiveh/Creditoasiveh.class.php');
    $oCreditoasiveh = new Creditoasiveh();
    require_once('../creditogarveh/Creditogarveh.class.php');
    $oCreditogarveh = new Creditogarveh();
    require_once('../uploadpdf/Upload.class.php');
    $oUploapPdf = new Upload();

    $cliente_id = intval($_POST['cliente_id']);
    $fecha1 = fecha_mysql($_POST['fecha1']);
    $fecha2 = fecha_mysql($_POST['fecha2']);
    $tipo = intval($_POST['tipo']);
    $categoria = intval($_POST['categoria']);
    $dts = $oPoliza->listar_polizas($cliente_id, $fecha1, $fecha2, $tipo, $categoria);
    if ($dts['estado'] == 0) { echo '<center><h2>NO SE HA ENCONTRADO REGISTROS DISPONIBLES PARA CONTROL DE SEGUROS</h2></center>'; exit(); }
?>
<table cellspacing="1" id="tabla_controlseguro" class="table table-hover table-borderless table-responsive">
    <thead> <tr class="bg-primary text-white"> <th class="text-center" width="3%">ID</th> <th class="text-center" width="3%">TIPO</th> <th class="text-center" width="3%">N° POLIZA</th> <th class="text-center" width="15%">CLIENTE</th> <th class="text-center" width="15%">RAZON SOCIAL</th> <th class="text-center" width="5%">FECHA INICIO</th> <th class="text-center" width="7%">FECHA FIN</th> <th class="text-center" width="1%">ID CREDITO</th> <th class="text-center" width="1%">PLACA</th> <th class="text-center" width="1%">TIPO CREDITO</th> <th class="text-center" width="10%">SUB GAR</th> <th class="text-center" width="2%">RESPONSABLE PAGO</th> <th class="text-center" width="5%">PRECIO POLIZA</th> <th class="text-center" width="8%">ESTADO COMISION</th> <th class="text-center">ASEGURADORA</th> <th class="text-center" width="17%">OPCIONES</th> </tr> </thead>
    <tbody>
        <?php
            $cc = 1;
            $suma_poliza = 0;
            $suma_comision = 0;
            $tipo = '';
            $categoria = '';
            $razon = '';
            $tipo_cre = '';
            $placa = '';
            $credito_subgar = '';
            $tr = '';
            foreach ($dts['data'] as $key => $dt) {
                $tr = '<tr> </tr>';

                $vencida = '<strong style="color: green;">Vigente</strong>';
                if(date('Y-m-d') > $dt['tb_poliza_fecfin']) { $vencida = '<strong style="color: red;">Vencida</strong>'; }
                $suma_poliza   += formato_moneda($dt['tb_poliza_pre']);
                $suma_comision += $dt['tb_poliza_comi'];
                $tipocredito = intval($dt['tb_creditotipo_id']);
                if ($tipocredito == 2) {
                    $tipo_cre = 'ASI. VEH';
                    $resultasi = $oCreditoasiveh->mostrarUno($dt['tb_credito_id']);
                        if ($resultasi['estado'] == 1) { $placa = $resultasi['data']['tb_credito_vehpla']; $usureg = $resultasi['data']['tb_credito_usureg']; }
                    $resultasi = NULL;
                } elseif ($tipocredito == 3) {
                    $tipo_cre = 'GAR. MOB';
                    $resultgar = $oCreditogarveh->mostrarUno($dt['tb_credito_id']);
                        if ($resultgar['estado'] == 1) { $placa = $resultgar['data']['tb_credito_vehpla']; $usureg = $resultgar['data']['tb_credito_usureg']; $credito_subgar = $resultgar['data']['tb_credito_subgar']; }
                    $resultgar = NULL;
                }
                else {
                    $tipo_cre = 'NO IDENTIFICADO - REPORTELO';
                    $placa = '-';
                }

                $responsable = 'IPDN';
                if ($dt['tb_poliza_respag'] == 1) { $responsable = 'El Cliente'; }

                $estados = '<strong style="color: black;"></strong> ';

                if ($dt['tb_poliza_tipo'] == 1 && $credito_subgar == "PRE-CONSTITUCION" && $dt['tb_poliza_categoria'] == 1 && $dt['tb_poliza_estcomi'] == 0) { $estado = '<strong style="color: green;">Por Pagar a Vendedor</strong> '; }
                else { if ($dt['tb_poliza_tipo'] != 2) { $estado = '<strong style="color: blue;">Dinero por Ingresar a Caja IPDN</strong> '; }
                }
                if ($dt['tb_poliza_tipo'] == 2) {
                    $estado = '';
                    $tipo = '<strong style="color: blue;">GPS</strong>';
                }
                if ($dt['tb_poliza_tipo'] == 1) {
                    $tipo = '<strong style="color: green;">STR</strong>';
                }
                if ($dt['tb_poliza_categoria'] == 1) {
                    $categoria = '<strong style="color: red;">NUEVO</strong>';
                }
                if ($dt['tb_poliza_categoria'] == 2) {
                    $categoria = '<strong style="color: black;">RENOVACIÓN</strong>';
                }
                if ($dt['tb_cliente_tip'] == 2) { $razon = $dt['tb_cliente_emprs']; } else { $razon = ''; }
            ?>
            <tr>
                <td style="vertical-align: middle;" class="text-center"><?php echo $dt['tb_poliza_id']; ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $tipo . '-' . $categoria; ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $dt['tb_poliza_num']; ?></td>
                <td style="vertical-align: middle;" class="text-left"><?php echo $dt['tb_cliente_nom']; ?></td>
                <td style="vertical-align: middle;" class="text-left"><?php echo $razon; ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo mostrar_fecha($dt['tb_poliza_fecini']); ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo mostrar_fecha($dt['tb_poliza_fecfin']) . '<br> (' . $vencida . ')'; ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $dt['tb_credito_id']; ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $placa ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $tipo_cre; ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $credito_subgar; ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $responsable; ?></td>
                <td style="vertical-align: middle;" class="text-right"><?php echo 'US$ ' . mostrar_moneda($dt['tb_poliza_pre']); ?></td>
                <td style="vertical-align: middle;" class="text-center"><?php echo $estado; ?> <!--<a href="javascript:void(0)" class="btn btn-danger btn-xs" title="Ingresar a Caja" onclick="pagar_comision(< ?php echo $dt['tb_poliza_id']; ?>)"><i class="fa fa-money"></i></a>--> </td>
                <td style="vertical-align: middle;" class="text-center"> <?php echo $dt['tb_poliza_aseg']; ?> </td>
                <td style="vertical-align: middle;" class="text-center">
                    <?php if ($dt['tb_poliza_anulada'] == 1) { //if ($dt['tb_poliza_ver'] ==0){ ?>
                        <a href="javascript:void(0)" class="btn btn-facebook btn-xs" onclick="controlseguro_his(<?php echo $dt['tb_poliza_id']; ?>)" title="HISTORIAL"><i class="fa fa-clock-o" aria-hidden="true"></i></a> &nbsp;
                        <a href="javascript:void(0)" class="btn btn-success btn-xs" onClick="controlseguro_form('editar', <?php echo $dt['tb_poliza_id']; ?>)" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp;
                        <a class="btn btn-info btn-xs" onClick="controlseguro_form('Leer', <?php echo $dt['tb_poliza_id']; ?>)" title="Ver"><i class="fa fa-eye" aria-hidden="true"></i></a> &nbsp;
                        <!-- <a href="javascript:void(0)" class="btn btn-success btn-xs" title="NOTA"><i class="fa fa-file"></i> <i class="fa fa-pencil fa-rotate-90"></i></a> &nbsp; -->
                        <?php if( $_SESSION['usuariogrupo_id'] == 2 ) { echo '<a class="btn btn-danger btn-xs" onClick="controlseguro_anular('.$dt['tb_poliza_id'].')" title="Anular Control de Seguro"><i class="fa fa-ban" aria-hidden="true"></i></a>'; }  ?>
                        <!--<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="upload_formpdf('I',< ?php echo $dt['tb_poliza_id']; ?>)" title="Subir Pdf de la Poliza SRT o GPS"><i class="fa fa-file-pdf-o"></i></a>-->
                        <?php if ($_SESSION['usuario_id'] == 35 || $_SESSION['usuario_id'] == 46) {
                            $PDF = $oUploapPdf->mostrarUno_modulo($dt['tb_poliza_id'], 'seguros');
                            if ($PDF['estado'] == 0) {
                                echo '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="factura_form(\'I\',0,' . $dt['tb_poliza_id'] . ')" title="Subir Pdf de la Poliza SRT o GPS"><i class="fa fa-file-pdf-o"></i></a> ';
                            } else {
                                echo '<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="contratos_Seguros_form(' . $dt['tb_poliza_id'] . ')"><i class="fa fa-file-pdf-o" title="Ver Contratos"></i></a> ';
                            }
                        }
                        if ($dt['tb_poliza_tipo'] == 1 && $credito_subgar == "PRE-CONSTITUCION" && $dt['tb_poliza_categoria'] == 1 && $dt['tb_poliza_estcomi'] == 0) {
                            $estados = '<a href="javascript:void(0)" class="btn btn-success btn-xs" title="Pagar a Vendedor" onclick="pagocomisiones_pagar(1,' . $dt['tb_poliza_id'] . ')"><i class="fa fa-money"></i></a> ';
                        } elseif ($dt['tb_poliza_tipo'] == 1) {
                            if ($dt['tb_poliza_tipo'] != 2) {
                                $estados = '<a href="javascript:void(0)" class="btn btn-danger btn-xs" title="Ingresar a Caja" onclick="pagocomisiones_pagar(2,' . $dt['tb_poliza_id'] . ')"><i class="fa fa-money"></i></a> ';
                            }
                        }
                        echo $estados;
                    } else { ?>
                        <strong style="color: red;">POLIZA ANULADA</strong>&nbsp;&nbsp;
                        <a class="btn btn-info btn-xs" onClick="controlseguro_form('Leer', <?php echo $dt['tb_poliza_id']; ?>)"><i class="fa fa-eye"></i></a>
                        <a href="javascript:void(0)" class="btn btn-facebook btn-xs" onclick="controlseguro_his(<?php echo $dt['tb_poliza_id']; ?>)" title="HISTORIAL"><i class="fa fa-clock-o"></i></a>
                    <?php } ?>
                    <!-- < ?php if ($dt['tb_poliza_exonerar'] == 1) { ?>
                        <a href="javascript:void(0)" class="btn btn-bitbucket btn-xs" onclick="controlseguro_exonerar(< ?php echo $dt['tb_poliza_id']; ?>)" title="Exonerar Poliza"><i class="fa fa-eraser"></i></a>
                    < ?php } ?> -->
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
    <tfoot> <tr> <td colspan="12"></td> <td class="bg-info text-blue text-right"><?php echo 'US$ '.mostrar_moneda($suma_poliza); ?></td> <td colspan="2"></td> </tr> </tfoot>
</table>