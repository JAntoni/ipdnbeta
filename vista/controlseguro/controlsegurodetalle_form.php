<?php
require_once ("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$polizadetalle_id = intval($_POST['polizadetalle_id']);
$poliza_id = intval($_POST['poliza_id']);
$vista=$_POST['vista'];


?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registrodetalle_controlseguro" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold">DETALLE DE PAGO DE CONTROL DE SEGUROS Y GPS</h4>
            </div>

            <form id="form_controlsegurodetalle">
                <input type="hidden" name="hdd_polizadetalle_id" id="hdd_polizadetalle_id" value="<?php echo $polizadetalle_id; ?>">
                <input type="hidden" name="hdd_poliza_id" id="hdd_poliza_id" value="<?php echo $poliza_id; ?>">
                <input type="hidden" name="hdd_vistas" id="hdd_vistas" value="<?php echo $vista; ?>">

                <div class="modal-body">

                    <!-- <div class="box box-primary"> -->
                        <!-- <div class="box-header"> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Detalle el pago:</label>
                                    <textarea id="txt_pago_seguro_det" name="txt_pago_seguro_det" class="form-control input-sm" rows="4" style="resize: vertical;"></textarea>
                                </div>
                            </div>
                        <!-- </div> -->
                    <!-- </div> -->
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_controlseguro">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>



        </div>
    </div>
</div>



<script type="text/javascript" src="<?php echo 'vista/controlseguro/controlsegurodetalle_form.js?ver=00022558100112500';?>"></script>