<?php
require_once("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once('../vendedor/Vendedor.class.php');
$oVendedor = new Vendedor();

require_once('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$poliza_id = intval($_POST['poliza_id']);
$vista = $_POST['vista'];
$poliza_num = 0;
$moneda_id = 2;
$credito_id = intval($_POST['credito_id']);
$poliza_reg = intval($_POST['poliza_tipo']);

if ($action == 'editar' || $action == 'Leer') {
    $dts = $oPoliza->mostarUno($poliza_id);
    if ($dts['estado'] == 1) {
        $poliza_num = $dts['data']['tb_poliza_num'];
        $cliente_id = $dts['data']['tb_cliente_id'];
        $cliente_nom = $dts['data']['tb_cliente_nom'];
        $cliente_doc = $dts['data']['tb_cliente_doc'];
        //$poliza_aseg = $dts['data']['tb_poliza_aseg'];
        $proveedorpoliza_id = $dts['data']['tb_proveedor_id'];
        $vehiculouso_id = $dts['data']['tb_poliza_tip'];
        $poliza_fecini = mostrar_fecha($dts['data']['tb_poliza_fecini']);
        $poliza_fecfin = mostrar_fecha($dts['data']['tb_poliza_fecfin']);
        $credito_id = $dts['data']['tb_credito_id'];
        $creditotipo_id = intval($dts['data']['tb_creditotipo_id']);
        $poliza_respag = $dts['data']['tb_poliza_respag'];
        $poliza_respag2 = $dts['data']['tb_poliza_respag2'];
        $poliza_pre = $dts['data']['tb_poliza_pre'];
        $poliza_cuo = $dts['data']['tb_poliza_cuo'];
        $poliza_estcomi = $dts['data']['tb_poliza_estcomi'];
        $poliza_ver = intval($dts['data']['tb_poliza_ver']);
        $poliza_comi = $dts['data']['tb_poliza_comi'];
        $poliza_tipo = $dts['data']['tb_poliza_tipo']; //si es Seguro todo riesgo o es GPS
        $poliza_cate = $dts['data']['tb_poliza_categoria'];
        $vehiculomarca_id = $dts['data']['tb_vehiculomarca_id'];
        $vehiculomodelo_id = $dts['data']['tb_vehiculomodelo_id'];
        $vehiculo_tasacion = $dts['data']['tb_vehiculo_tasacion'];
        $usuario_id = $dts['data']['tb_asesor_id'];
        $vendedor_id = $dts['data']['tb_vendedor_id'];
        $poliza_formapago = $dts['data']['tb_poliza_formapago'];
    }

    if ($vendedor_id > 0) {
        $result3 = $oVendedor->mostrarUno($vendedor_id);
        if ($result3['estado'] == 1) {
            $vendedor_dni = $result3['data']['tb_vendedor_dni'];
            $vendedor_nombre = $result3['data']['tb_vendedor_nombre'];
            $vendedor_tienda = $result3['data']['tb_vendedor_tienda'];
        }
    }
}

if ($credito_id > 0) {
    $result = $oCreditogarveh->mostrarUno($credito_id);
    if ($result['estado'] == 1) {
        $cliente_nom = $result['data']['tb_cliente_nom'] . ' ' . $result['data']['tb_cliente_ape'];
        //$cli_ape = $result['data']['tb_cliente_ape'];
        $cliente_id = $result['data']['tb_cliente_id'];
        $cliente_doc = $result['data']['tb_cliente_doc'];
        //$credito_id = $_POST['credito_id'];
        $creditotipo_id = 3;
        //$poliza_categoria = 1; // 1 es nuevo
        $poliza_tip = $result['data']['tb_credito_tipus']; //tipo de uso 1 taxi, 2 uso particular
        $vehiculomarca_id = $result['data']['tb_vehiculomarca_id'];
        $vehiculomodelo_id = $result['data']['tb_vehiculomodelo_id'];
        $vehiculo_tasacion = moneda_mysql($result['data']['tb_credito_valtas']);
        //        $data['tasacion'] = $vehiculo_tasacion;
        $asesor_id = intval($result['data']['tb_credito_usureg']);
        $husuario_id = $_SESSION['usuario_id']; //ANTHONY
    } else {
        $cliente_nom = 'NO HAY';
    }
}

if ($_SESSION['usuario_id'] == 48) {
    $disabled = 'disabled';
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_controlseguro" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold">CARACTERÍSTICAS DEL VEHICULO DISPONIBLE <?php //echo "creditotipo_id=".$creditotipo_id  ?></h4>
            </div>

            <form id="form_controlseguro">
                <input type="hidden" name="action" value="<?php echo $action; ?>">
                <input type="hidden" name="hdd_poliza_id" value="<?php echo $poliza_id; ?>">
                <input type="hidden" name="hdd_credito_id" value="<?php echo $credito_id; ?>">
                <input type="hidden" id="hdd_vista" value="<?php echo $vista; ?>">
                <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>">
                <input type="hidden" name="hdd_vendedor_id" id="hdd_vendedor_id" value="<?php echo $vendedor_id; ?>">
                <input type="hidden" name="hdd_usuario_id" id="hdd_usuario_id" value="<?php echo $husuario_id; ?>"> <!-- //ANTHONY -->

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="container-fluid">
                                        <div class="row mt-4 mb-4">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_tip" class="control-label">Tipo:</label>
                                                    <br>
                                                    <label class="radio-inline" style="padding-left: 0px;">
                                                        <input type="radio" name="txt_poliza_tip" id="txt_poliza_tip1" class="flat-green" value="1" <?php if ($poliza_tipo == 1 || $poliza_tipo == 0) echo 'checked'; ?> <?php if ($poliza_reg == 1) echo 'disabled'; ?><?php if ($poliza_reg == 2) echo 'checked'; ?>>&nbsp; SEGURO
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="txt_poliza_tip" id="txt_poliza_tip2" class="flat-green" value="2" <?php if ($poliza_tipo == 2) echo 'checked'; ?> <?php echo $disabled ?> <?php if ($poliza_reg == 2) echo 'disabled'; ?><?php if ($poliza_reg == 1) echo 'checked'; ?>>&nbsp; GPS
                                                    </label>
                                                </div>
                                            </div>

                                            <?php
                                            if ($poliza_tipo == 2 && $action == 'editar') {
                                                $display = "display: block";
                                                $readonly = "readonly";
                                            } else {
                                                //                                     $display="display: none";
                                                $display = "display: block";
                                                $readonly = "";
                                            }
                                            ?>
                                            <div class="col-md-2">
                                                <label>N° de Póliza:</label>
                                                <input type="text" class="form-control input-sm" name="txt_poliza_num" id="txt_poliza_num" value="<?php echo $poliza_num; ?>" autocomplete="off" <?php echo $readonly ?>>
                                            </div>

                                            <div class="col-md-5" id="cate" style="<?php echo $display; ?>">
                                                <div class="form-group">
                                                    <label>Categoria:</label>
                                                    <br>
                                                    <label class="radio-inline" style="padding-left: 0px;">
                                                        <input type="radio" name="txt_poliza_cate" id="txt_poliza_cat1" class="flat-blue" value="1" <?php if ($poliza_cate == 1 || $poliza_cate == 0) echo 'checked'; ?>>&nbsp; Nuevo
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="txt_poliza_cate" id="txt_poliza_cat2" class="flat-blue" value="2" <?php if ($poliza_cate == 2) echo 'checked'; ?> <?php if ($vista == 'creditogarveh') echo 'disabled'; ?>>&nbsp; Renovación
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row mb-4">
                                            <div class="col-md-6">
                                                <label>Cliente:</label>
                                                <input type="text" class="form-control input-sm" name="txt_cliente_nom" id="txt_cliente_nom" value="<?php echo $cliente_nom; ?>" autocomplete="off">
                                                <!--<input type="hidden" class="form-control input-sm" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id; ?>">-->
                                            </div>
                                            <div class="col-md-3">
                                                <label>DNI/RUC Cliente:</label>
                                                <input type="text" class="form-control input-sm" name="txt_cliente_doc" id="txt_cliente_doc" value="<?php echo $cliente_doc; ?>" autocomplete="off">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Facturas a Nombre de :</label>
                                                <select id="cmb_poliza_respag" name="cmb_poliza_respag" class="form-control input-sm">
                                                    <option value="">Selecciona...</option>
                                                    <option value="1" <?php if ($poliza_respag2 == 1) echo 'selected'; ?>>Cliente</option>
                                                    <option value="2" <?php if ($poliza_respag2 == 2) echo 'selected'; ?>>Empresa IPDN</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="row mb-4">
                                            <div class="col-md-2">
                                                <label>ID Crédito:</label>
                                                <input class="form-control input-sm" type="text" name="txt_credito_id" id="txt_credito_id" value="<?php echo $credito_id; ?>" autocomplete="off">
                                            </div>
                                            <div class="col-md-4">
                                                <label>Tipo de Crédito:</label>
                                                <select id="cmb_creditotipo_id" name="cmb_creditotipo_id" class="form-control input-sm">
                                                    <option value="" <?php if ($creditotipo_id == "") echo 'selected'; ?>>Selecciona...</option>
                                                    <option value="2" <?php if ($creditotipo_id == 2) echo 'selected'; ?>>Asistencia Vehicular</option>
                                                    <option value="3" <?php if ($creditotipo_id == 3) echo 'selected'; ?>>Garantía Vehicular</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Aseguradora:</label>
                                                <div class="input-group">
                                                    <select id="txt_cliente_aseg" name="txt_cliente_aseg" class="form-control input-sm">
                                                        <?php require_once '../proveedorpoliza/proveedorpoliza_select.php'; ?>
                                                    </select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary btn-sm" type="button" onclick="proveedorpoliza_form3('I', 0)" title="AGREGAR ACREEDOR" <?php if ($vista == 'creditogarveh') echo 'disabled'; ?>>
                                                            <span class="fa fa-plus icon"></span>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row mb-4">
                                            <div class="col-md-3">
                                                <label>Uso Vehículo:</label>
                                                <div class="input-group">
                                                    <select id="cmb_poliza_tip" name="cmb_poliza_tip" class="form-control input-sm">
                                                        <?php require_once '../vehiculouso/vehiculouso_select.php'; ?>
                                                    </select>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary btn-sm" type="button" onclick="vehiculouso_form3('I', 0)" title="AGREGAR ACREEDOR" <?php if ($vista == 'creditogarveh') echo 'disabled'; ?>>
                                                            <span class="fa fa-plus icon"></span>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Fecha Inicio:</label>
                                                    <div class='input-group date' id='datetimepicker1'>
                                                        <input type="text" class="form-control input-sm" name="txt_poliza_fecini" id="txt_poliza_fecini" autocomplete="off" value="<?php echo $poliza_fecini; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Fecha Fin:</label>
                                                    <div class='input-group date' id='datetimepicker2'>
                                                        <input type="text" class="form-control input-sm" name="txt_poliza_fecfin" id="txt_poliza_fecfin" autocomplete="off" value="<?php echo $poliza_fecfin; ?>">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Responsable Pago:</label>
                                                <select id="cmb_poliza_respag2" name="cmb_poliza_respag2" class="form-control input-sm">
                                                    <option value="">Selecciona...</option>
                                                    <option value="1" <?php if ($poliza_respag == 1) echo 'selected'; ?>>El Cliente</option>
                                                    <option value="2" <?php if ($poliza_respag == 2) echo 'selected'; ?>>La Empresa IPDN</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Precio Póliza:</label>
                                                <input type="text" class="form-control input-sm" name="txt_poliza_pre" id="txt_poliza_pre" value="<?php echo $poliza_pre; ?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Comisión Póliza:</label>
                                                <input type="text" class="form-control input-sm" name="txt_poliza_comi" id="txt_poliza_comi" value="<?php echo $poliza_comi; ?>">
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_telref" class="control-label">Moneda</label>
                                                    <select id="cmb_moneda_id" name="cmb_moneda_id" class="form-control input-sm">
                                                        <?php require_once '../moneda/moneda_select.php'; ?>
                                                    </select>
                                                    <input type="hidden" class="form-control input-sm moneda" id="txt_pagocomisiones_tipcam" name="txt_pagocomisiones_tipcam" value="<?php echo $pagocomisiones_tipcam ?>" style="font-weight: bold;font-size: 20px">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label>N° Cuotas:</label>
                                                <div class="input-group">
                                                    <input class="form-control input-sm numero" type="text" name="txt_poliza_cuo" id="txt_poliza_cuo" value="<?php echo $poliza_cuo; ?>">
                                                    <input class="form-control input-sm numero" type="hidden" name="txt_poliza_cuo2" id="txt_poliza_cuo2" value="<?php // echo $poliza_cuo;                   
                                                                                                                                                                    ?>">
                                                    <span class="input-group-btn">
                                                        <?php if ($action == 'insertar') { ?>
                                                            <button class="btn btn-primary btn-sm" type="button" id="btndetalle" onClick="agregar_detalle()">
                                                                <span class="fa fa-plus icon"></span>
                                                            </button>
                                                        <?php } ?>
                                                        <?php if ($poliza_ver == 0 && $action != 'insertar') { ?>
                                                            <button class="btn btn-success btn-sm" type="button" onClick="Validar(<?php echo $poliza_id; ?>)" title="Verificar Datos de la Poliza">
                                                                <span class="fa fa-check-square-o"></span>
                                                            </button>
                                                        <?php } ?>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="container-fluid">
                                        <div class="row mt-4 mb-4">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_telref" class="control-label">Marca</label>
                                                    <select class="form-control input-sm" id="txt_vehiculomarca_id" name="txt_vehiculomarca_id" <?php echo $disabled ?>>
                                                        <?php require_once '../vehiculomarca/vehiculomarca_select.php'; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_telref" class="control-label">Modelo</label>
                                                    <select class="form-control input-sm" id="txt_vehiculomodelo_id" name="txt_vehiculomodelo_id" <?php echo $disabled ?>>
                                                        <?php require_once '../vehiculomodelo/vehiculomodelo_select.php'; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_telref" class="control-label">Tasación de Vehículo</label>
                                                    <input type="text" class="form-control input-sm moneda" id="txt_pagocomisiones_tasa" name="txt_pagocomisiones_tasa" value="<?php echo mostrar_moneda($vehiculo_tasacion) ?>" style="font-weight: bold;font-size: 20px;text-align: center" <?php echo $disabled ?>>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_doc" class="control-label">Asesor de Crédito</label>
                                                    <select id="cbo_usuario_id" name="cbo_usuario_id" class="form-control input-sm" <?php echo $disabled ?>>
                                                        <?php require_once '../usuario/usuario_select.php'; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_doc" class="control-label">DNI Vendedor</label>
                                                    <input type="text" name="txt_vendedor_dni" id="txt_vendedor_dni" class="form-control input-sm numero" value="<?php echo $vendedor_dni; ?>" <?php echo $disabled ?>>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_nom" class="control-label">Vendedor</label>
                                                    <input type="text" name="txt_vendedor_nombre" id="txt_vendedor_nombre" class="form-control input-sm mayus" value="<?php echo $vendedor_nombre; ?>" <?php echo $disabled ?>>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_nom" class="control-label">Consecionaria</label>
                                                    <input type="text" name="txt_vendedor_tienda" id="txt_vendedor_tienda" class="form-control input-sm mayus" value="<?php echo $vendedor_tienda; ?>" <?php echo $disabled ?>>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txt_cliente_fil_cre_telref" class="control-label">Forma de Pago</label>
                                                    <textarea type="text" class="form-control input-sm mayus" id="txt_pagocomisiones_forma" name="txt_pagocomisiones_forma" <?php echo $disabled ?>><?php echo $poliza_formapago ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table width="100%" class="table" id="tabla_controlsegurodetalle">
                                <thead>
                                    <tr id="tabla_cabecera">
                                        <th id="tabla_cabecera_fila" width="10px">N°</th>
                                        <th id="tabla_cabecera_fila" width="90px">CUPON</th>
                                        <th id="tabla_cabecera_fila" width="90px">FECHA VENC.</th>
                                        <th id="tabla_cabecera_fila" width="80px">IMPORTE</th>
                                        <th id="tabla_cabecera_fila" width="70px">ESTADO</th>
                                        <th id="tabla_cabecera_fila">DETALLE</th>
                                        <th id="tabla_cabecera_fila" width="200px">OPCIONES</th>
                                    </tr>
                                </thead>
                                <tbody id="tabla_detalle" style="text-align: center;">

                                    <?php include_once 'controlseguro_form_detalle.php'; ?>

                                </tbody>
                            </table>
                            <div id="msj_controlseguro_form" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
                        </div>
                    </div>

                    <!--- MESAJES DE ALERTA AL ELIMINAR REGISTRO -->
                    <?php if ($action == 'eliminar'): ?>
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este Control de Seguro?</h4>
                        </div>
                    <?php endif; ?>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="area_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <?php if ($action != 'Leer') { ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_controlseguro">Guardar</button>
                        <?php } ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>



        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/controlseguro/controlseguro_form.js?ver=201124'; ?>"></script>