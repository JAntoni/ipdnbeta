/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '999999999.00'
    });

 $('#datetimepicker1_detalle').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy"
        //startDate: "-0d"
//        endDate: new Date()
    });
    
});


$("#form_controlsegurodetalle_edit").validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                async: true,
                dataType: "json",
                data: ({
                    action:'modificar_cupon',
                    polizadetalle_id:$("#hdd_polizadetalle_id").val(),
                    poliza_id:$("#hdd_poliza_id").val(),
                    cupon:$("#txt_controlsegurodetalle_cupon").val(),
                    fecha:$("#txt_fecha").val(),
                    monto:$("#txt_monto").val(),
                    detalle:$("#txt_detalle").val()
                }),
                beforeSend: function () {
                    
                },
                success: function (data) {
                    if (parseInt(data.estado)==1) {
                        swal_success("CORRECTO",data.msj,2500);
                        controlsegurodetalle(data.poliza_id);
                        $('#modal_controlsegurodetalle').modal('hide');
//                        $('#modal_registro_controlseguro').modal('hide');
                    } else{
                        swal_warning("AVISO",data.msj,3000);
                    }
                },
                complete: function (data) {

                }
            });
        },
        rules: {
            txt_controlsegurodetalle_cupon: {
                required: true
            }
        },
        messages: {
            txt_controlsegurodetalle_cupon: {
                required: 'N° de Cupón'
            }
        }
    });


function controlsegurodetalle(poliza_id) {

    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlseguro_form_detalle.php",
        async: true,
        dataType: "html",
        data: ({
            poliza_id:poliza_id,
            action:'editar'
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#tabla_detalle').html(html);
        },
        complete: function () {
        }
    });
}