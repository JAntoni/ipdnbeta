/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#datetimepicker1,#datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        // startDate: "-0d"
        // endDate: new Date()
    });

    $('#datetimepicker1, #datetimepicker2, #cmb_creditotipodetalle_id, #cbo_tipodetalle, #cbo_categoriadetalle').change(function (e) {
        controlsegurodetalle_str_gps_tabla();
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_ing_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });

    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_ing_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });

    $("#txt_detalleseguro_fil_cli_nom").autocomplete({
        minLength: 1,
        source: VISTA_URL + "cliente/cliente_autocomplete.php",
        select: function (event, ui) {
            $("#hdd_detalleseguro_fil_cli_id").val(ui.item.cliente_id);
            //$("#txt_seguro_fil_cli_nom").val(ui.item.nombre);
            //$("#txt_cliente_doc").val(ui.item.documento);
            controlsegurodetalle_str_gps_tabla();
        }
    });

    controlsegurodetalle_str_gps_tabla();
});


function controlsegurodetalle_str_gps_tabla() {
    var cliente_fil_id = $('#hdd_detalleseguro_fil_cli_id').val();
    var cliente_fil_nom = $('#txt_detalleseguro_fil_cli_nom').val();
    var fecha1 = $('#txt_fil_fec1').val();
    var fecha2 = $('#txt_fil_fec2').val();
    
    if (cliente_fil_nom == "") {
        cliente_fil_id = 0;
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlseguro_str_gps_tabla.php",
        async: true,
        dataType: "html",
        data:
                ({
                    fecha1: fecha1,
                    fecha2: fecha2,
                    cliente_id: cliente_fil_id,
                    poliza_tipo: $("#cbo_tipodetalle").val(),
                    poliza_categoria: $("#cbo_categoriadetalle").val(),
                    tipo_credito: $("#cmb_creditotipodetalle_id").val(),
                }),
        beforeSend: function () {
            $('#controlsegurodetalle_str_gps_mensaje').show(400);
        },
        success: function (html) {
            $('#controlsegurodetalle_str_gps_mensaje').hide(400);
            $('#div_controlsegurodetalle_str_gps_tabla').html(html);
            
        },
        complete: function () {
            estilos_datatable();
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tabla_controlsegurodetallereporte').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [], orderable: false}
        ]
    });

//    datatable_texto_filtrar();
}
