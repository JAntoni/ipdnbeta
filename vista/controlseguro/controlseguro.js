/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    controlseguro_tabla();
    $("#click").click();
    $("#txt_seguro_fil_cli_nom").autocomplete({
        minLength: 1,
        source: VISTA_URL + "cliente/cliente_autocomplete.php",
        select: function (event, ui) {
            $("#hdd_seguro_fil_cli_id").val(ui.item.cliente_id);
            //$("#txt_seguro_fil_cli_nom").val(ui.item.nombre);
            //$("#txt_cliente_doc").val(ui.item.documento);
            controlseguro_tabla();
        }
    });



    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fil_ing_fec1').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fil_ing_fec2').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });




});

$('#cbo_tipo,#cbo_categoria').change(function () {
    controlseguro_tabla();
    console.log("este es el codigo del select " + $(this).val());
});

function controlseguro_tabla() {
    var cliente_fil_id = $('#hdd_seguro_fil_cli_id').val();
    var cliente_fil_nom = $('#txt_seguro_fil_cli_nom').val();
    var fecha1 = $('#txt_fil_ing_fec1').val();
    var fecha2 = $('#txt_fil_ing_fec2').val();
    var tipo = $('#cbo_tipo').val();
    var categoria = $('#cbo_categoria').val();

    if (cliente_fil_nom == "") {
        cliente_fil_id = 0;
    }

    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlseguro_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            cliente_id: cliente_fil_id,
            fecha1: fecha1,
            fecha2: fecha2,
            tipo: tipo,
            categoria: categoria
        }),
        beforeSend: function () {
            $('#controlseguro_mensaje_tbl').show(400);
        },
        success: function (html) {
            $('#div_controlseguro_tabla').html(html);
            $('#controlseguro_mensaje_tbl').hide(400);
        },
        complete: function () {
            estilos_datatable();
        }
    });
}

function estilos_datatable() {
    datatable_global = $('#tabla_controlseguro').DataTable({
        "pageLength": 50,
        "responsive": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ninguna coincidencia para la búsquedad",
            "info": "Mostrado _END_ registros",
            "infoEmpty": "Ningún registro disponible",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        },
        order: [],
        columnDefs: [
            {targets: [13,14], orderable: false}
        ]
    });

    datatable_texto_filtrar();
}
function datatable_texto_filtrar() {
    $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

    $('#txt_datatable_fil').keyup(function (event) {
        $('#hdd_datatable_fil').val(this.value);
    });

    var text_fil = $('#hdd_datatable_fil').val();
    if (text_fil) {
        $('#txt_datatable_fil').val(text_fil);
        datatable_global.search(text_fil).draw();
    }
}


function controlseguro_form(action, poliza_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlseguro_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: action,
            poliza_id: poliza_id,
            vista: 'control'
        }),
        beforeSend: function (data) {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_controlseguro_form').html(data);
            $('#modal_registro_controlseguro').modal('show');
            modal_height_auto('modal_registro_controlseguro');
            modal_hidden_bs_modal('modal_registro_controlseguro', 'limpiar');
            modal_width_auto('modal_registro_controlseguro', 80);
            if( $.trim(action) === 'Leer' ){ form_desabilitar_elementos('form_controlseguro'); }
            $('#modal_mensaje').modal('hide');
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}
function controlsegurodetalle_listado() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlsegurodetalle_listado.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'listado',
            vista: 'control'
        }),
        beforeSend: function (data) {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_controlsegurodetalle_listado').html(data);
            $('#modal_controlsegurodetalle_listado').modal('show');
            modal_height_auto('modal_controlsegurodetalle_listado');
            modal_width_auto('modal_controlsegurodetalle_listado',90);
            modal_hidden_bs_modal('modal_controlsegurodetalle_listado', 'limpiar');
            $('#modal_mensaje').modal('hide');
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}

function facturas_listado() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlsegurodetalle_listadofacturas.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'listado',
            vista: 'facturas'
        }),
        beforeSend: function (data) {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_controlsegurodetalle_listado').html(data);
            $('#modal_controlsegurodetalle_listado').modal('show');
            modal_height_auto('modal_controlsegurodetalle_listado');
            modal_width_auto('modal_controlsegurodetalle_listado',90);
            modal_hidden_bs_modal('modal_controlsegurodetalle_listado', 'limpiar');
            $('#modal_mensaje').modal('hide');
        },
        complete: function (data) {
            //console.log(data);
        }
    });
}


function pagar_comision(poliza_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'PRESTAMOS DEL NORTE',
        content: '¿Ya se pagó la Comisión?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        poliza_id: poliza_id,
                        action: 'pagar_comision'
                    }),
                    beforeSend: function (data) {

                    },
                    success: function (data) {
                        if (parseInt(data.estado) > 0) {
                            swal_success("CORRECTO", data.msj, 2500);
                            controlseguro_tabla();
                        } else
                            swal_warning("AVISO", data, 8000);
                    },
                    complete: function (data) {
                        //console.log(data);
                    }
                });
            },
            no: function () {}
        }
    });
}

function controlseguro_anular(poliza_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'PRESTAMOS DEL NORTE',
        content: '¿Desea Anular la Poliza de seguros?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        poliza_id: poliza_id,
                        action: 'anular'
                    }),
                    beforeSend: function (data) {

                    },
                    success: function (data) {
                        if (parseInt(data.estado) > 0) {
                            swal_success("CORRECTO", data.msj, 2500);
                            controlseguro_tabla();
                        } else
                            swal_warning("AVISO", data, 8000);
                    },
                    complete: function (data) {
                        //console.log(data);
                    }
                });
            },
            no: function () {}
        }
    });
}

function controlseguro_exonerar(poliza_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'PRESTAMOS DEL NORTE',
        content: '¿Desea Exonerar al Cliente de la Poliza de seguros?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        poliza_id: poliza_id,
                        action: 'exonerar'
                    }),
                    beforeSend: function (data) {

                    },
                    success: function (data) {
                        if (parseInt(data.estado) > 0) {
                            swal_success("CORRECTO", data.msj, 2500);
                            controlseguro_tabla();
                        } else
                            swal_warning("AVISO", data, 8000);
                    },
                    complete: function (data) {
                        //console.log(data);
                    }
                });
            },
            no: function () {}
        }
    });
}

function controlseguro_his(id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlseguro_reg.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'controlcomision_his',
            poliza_id: id
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_controlseguros_his').html(html);
            $('#modal_registro_controlseguros_historial').modal('show');
            modal_width_auto('modal_registro_controlseguros_historial', 20);
            modal_height_auto('modal_registro_controlseguros_historial');
        },
        complete: function (data) {

        }
    });
}


/*FUNCION PARA SUBIR ARCHIVOS PDF DE MAXIMO 20MG PARA LOS CONTRATOS DE POLIZAS DE SEGUROS Y GPS*/
function upload_formpdf2(usuario_act, upload_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "uploadpdf/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            modulo_nom: 'seguros', //nombre de la tabla a relacionar
            modulo_id: upload_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_uploadpdf_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
        }
    });
}



function contratos_Seguros_form(controlseguros_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"controlseguro/controlseguros_contratos.php",
    async: true,
    dataType: "html",
    data: ({
      controlseguros_id: controlseguros_id
    }),
    beforeSend: function() {
    },
    success: function(data){
      $('#div_modal_contratolinea_timeline').html(data);
      $('#modal_contratoseguguro_pdf').modal('show');
      modal_height_auto('modal_contratoseguguro_pdf'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_contratoseguguro_pdf', 'limpiar'); //funcion encontrada en public/js/generales.js
      upload_pdf(controlseguros_id);
    },
    complete: function(data){
    },
    error: function(data){
      alerta_error('Error', 'ERRROR!:'+ data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}



function upload_pdf(controlseguros_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "uploadpdf/upload_contratos_seguros_gps.php",
        async: true,
        dataType: "html",
        data: ({
            modulo_nom: 'seguros', //nombre de la tabla a relacionar
            modulo_id: controlseguros_id,
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('.pdf').html(data);
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
        }
    });
}



function factura_form(usuario_act, factura_id,poliza_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "factura/factura_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            factura_id: factura_id,
            poliza_id:poliza_id,
            vista:'control_seguro'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_factura_form').html(data);
                $('#modal_registro_factura').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if (usuario_act == 'L' || usuario_act == 'E')
                    form_desabilitar_elementos('form_factura'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_factura', 'limpiar'); //funcion encontrada en public/js/generales.js
            } 
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
            console.log(data.responseText);
        }
    });
}



function pagocomisiones_pagar(usuario_act, poliza_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/pagocomisiones_pagar.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'pagar_vendedor',
            pago: usuario_act,
            poliza_id: poliza_id
        }),
        beforeSend: function () {

        },
        success: function (data) {
            $('#div_modal_pagocomisiones_pagar').html(data);
            $('#modal_pagar_poliza').modal('show');

            modal_hidden_bs_modal('modal_pagar_poliza', 'limpiar'); //funcion encontrada en public/js/generales.js
//            modal_width_auto('modal_pagar_pagocomisiones', 70);
            modal_height_auto('modal_pagar_poliza');

        },
        complete: function (data) {

        },
        error: function (data) {
            swal_error('ERROR!:',data.responseText,3000);
            console.log(data.responseText);
        }
    });
}


function STR_GPS_renovacion(){
    $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/controlseguro_str_gps.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'str_gps_renovacion',
            vista: 'str_gps_renovacion_vista'
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#div_controlsegurodetalle_str_gps').html(data);
            $('#modal_controlsegurodetalle_str_gps').modal('show');
            modal_height_auto('modal_controlsegurodetalle_str_gps');
            modal_width_auto('modal_controlsegurodetalle_str_gps',95);
            modal_hidden_bs_modal('modal_controlsegurodetalle_str_gps', 'limpiar');
            $('#modal_mensaje').modal('hide');
        },
        complete: function () {
            
        }
    });
}



function controlseguroSTR_GPS_form(poliza_id) {
    //$.alert('Contenido aquí', 'Titulo aqui');
    $.confirm({
        icon: 'fa fa-print',
        title: 'PRESTAMOS DEL NORTE',
        content: '¿Desea Finalizar Esta Poliza de STR o GPS?',
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                    async: true,
                    dataType: "JSON",
                    data: ({
                        poliza_id: poliza_id,
                        action: 'finalizar'
                    }),
                    beforeSend: function (data) {

                    },
                    success: function (data) {
                        if (parseInt(data.estado) > 0) {
                            swal_success("CORRECTO", data.msj, 2500);
                            controlsegurodetalle_str_gps_tabla();
                        } else
                            swal_warning("AVISO", data, 8000);
                    },
                    complete: function (data) {
                        //console.log(data);
                    }
                });
            },
            no: function () {}
        }
    });
}

$("#btn_generarReportExcel").click(function() {
    reporteExcel();
  });
  
  function reporteExcel(){
  
    var cliente_fil_id = $('#hdd_seguro_fil_cli_id').val();
    var cliente_fil_nom = $('#txt_seguro_fil_cli_nom').val();
    var fecha1 = $('#txt_fil_ing_fec1').val();
    var fecha2 = $('#txt_fil_ing_fec2').val();
    var tipo = $('#cbo_tipo').val();
    var categoria = $('#cbo_categoria').val();

    if (cliente_fil_nom == "") {
        cliente_fil_id = 0;
    }
      
    Swal.fire({
        title: '',
        text: 'Está seguro que desea emitir el reporte en excel',
        icon: 'question',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: true,
        confirmButtonColor: '#00a65a',
        cancelButtonColor: '#d9edf7',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            document.location.href= VISTA_URL + "controlseguro/control_seguro_reporte_excel.php?p_fecha1="+fecha1+"&p_fecha2="+fecha2+"&p_tipo="+tipo+
            "&p_categoria="+categoria+"&p_cliente_fil_id="+cliente_fil_id;
        }
    })
  }