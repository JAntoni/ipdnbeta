/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('#txt_polizadetalle_historia').focus();


    $("#for_cob").validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "controlseguro/controlseguro_reg.php",
                async: true,
                dataType: "json",
                data: $("#for_cob").serialize(),
                beforeSend: function () {
                },
                success: function (data) {
                    if(data.estado>0){
                        swal_success("PRESTAMOS DEL NORTE",data.msj,3500);
                        $('#modal_registro_comentario_polizadetalle').modal('hide');
                    }
                    else{
                        swal_success("AVISO",'Ocurrio un Error',3500);
                    }
                    

                },
                complete: function () {
                    
                }

            });
        },
        rules: {
            txt_cuo_not: {
                required: true
            }
        },
        messages: {
            txt_cuo_not: {
                required: 'Debe ingresar una nota'
            }
        }
    });
});