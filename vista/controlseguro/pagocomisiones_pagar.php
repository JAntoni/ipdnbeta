


<?php
require_once ('../../core/usuario_sesion.php');

require_once('Poliza.class.php');
$oPoliza = new Poliza();
require_once '../vendedor/Vendedor.class.php';
$oVendedor = new Vendedor();
require_once '../usuario/Usuario.class.php';
$oUsuario = new Usuario();
require_once '../egreso/Egreso.class.php';
$oEgreso = new Egreso();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$poliza_id = $_POST['poliza_id'];
$action = $_POST['action'];
$pago = intval($_POST['pago']);

$result = $oPoliza->mostarUno($poliza_id);
if ($result['estado'] == 1) {
    $vendedor_id = intval($result['data']['tb_vendedor_id']);
    $poliza_comision = $result['data']['tb_poliza_comi'];
    $moneda_id = $result['data']['tb_moneda_id'];
    $usuario_id = intval($result['data']['tb_asesor_id']);
    $credito_id = intval($result['data']['tb_credito_id']);

    if ($pago == 1) {
        $monto_pagado = $result['data']['tb_poliza_monven'];
        $titulo = "PAGAR A VENDEDORES";
    }
    if ($pago == 2) {
        $monto_pagado = $result['data']['tb_poliza_monipdn'];
        $titulo = "INGRESAR DINERO A CAJA IPDN";
    }
}
$result = NULL;

if ($moneda_id == 1) {
    $moneda = "S./ ";
}
if ($moneda_id == 2) {
    $moneda = "US$./ ";
}

$result = $oUsuario->mostrarUno($usuario_id);
if ($result['estado'] == 1) {
    $usuario = $result['data']['tb_usuario_nom'] . ' ' . $result['data']['tb_usuario_ape'];
}
$result = NULL;

$result = $oVendedor->mostrarUno($vendedor_id);
if ($result['estado'] == 1) {
    $vendedor = $result['data']['tb_vendedor_nombre'];
}
$result = NULL;

$poliza_saldo = $poliza_comision - $monto_pagado;

if ($monto_pagado >= $poliza_comision) {
    $readonly = "readonly";
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_pagar_poliza" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold"><?php echo $titulo; ?></h4>
            </div>
            <form id="form_poliza" method="post">
                <input type="hidden" name="action" value="<?php echo $action; ?>">
                <input type="hidden" name="hdd_poliza_id" id="hdd_poliza_id" value="<?php echo $poliza_id; ?>">
                <input type="hidden" name="hdd_poliza_total" id="hdd_poliza_total" value="<?php echo $poliza_comision; ?>">
                <input type="hidden" name="hdd_pago" id="hdd_pago" value="<?php echo $pago; ?>">

                <div class="modal-body">


                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Fecha :</label>
                                        <div class='input-group date' id='poliza_fil_picker111'>
                                            <input type='text' class="form-control input-sm" name="txt_poliza_fec" id="txt_poliza_fec" value="<?php echo date('d-m-Y') ?>" readonly="">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>VENDEDOR</label>
                                    <input type="text" class="form-control input-sm" id="txt_vendedor_nom" name="txt_vendedor_nom" value="<?php echo $vendedor; ?>" readonly="">
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>ASESOR</label>
                                    <input type="text" class="form-control input-sm" id="txt_asesor" name="txt_asesor" value="<?php echo $usuario; ?>" readonly="">
                                </div>
                            </div>
                            <p>
                            <div class="row">    
                                <div class="col-md-12">
                                    <label>COMISIÓN</label>
                                    <input type="text" class="form-control input-sm" id="txt_comision" name="txt_comision" value="<?php echo $moneda . ' ' . mostrar_moneda($poliza_comision); ?>" readonly="">
                                </div>
                            </div>
                            <p>
                            <p>
                            <div class="row">    
                                <div class="col-md-12" style="display: none">
                                    <label>PAGADO</label>
                                    <input type="text" class="form-control input-sm" id="txt_pagado" name="txt_pagado" value="<?php echo $moneda . ' ' . mostrar_moneda($monto_pagado); ?>" readonly="">
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_moneda" class="control-label">Moneda</label>
                                        <select id="cmb_moneda_id1" name="cmb_moneda_id1" class="form-control input-sm">
                                                    <?php require_once '../moneda/moneda_select.php'; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_moneda" class="control-label">T.Cambio</label>
                                        <input type="text" class="form-control input-sm moneda" id="txt_poliza_tipcam" name="txt_poliza_tipcam" value="1.00" style="font-weight: bold;font-size: 20px" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="col-md-12">
                                    <label>PAGAR</label>
                                    <input type="text" class="form-control input-sm moneda" id="txt_pagar2" name="txt_pagar2" value="<?php echo mostrar_moneda($poliza_saldo); ?>" readonly="" >
                                </div>
                            </div>
                            <p>
                            <div class="row">    
                                <div class="col-md-12">
                                    <label>TOTAL A PAGAR</label>
                                    <input type="text" class="form-control input-sm moneda" id="txt_pagar" name="txt_pagar" value="<?php echo mostrar_moneda($poliza_saldo); ?>" readonly="" >
                                </div>
                            </div>
                            <p>
                            <?php if($vendedor_id==0||$usuario_id==0||$credito_id==0){?>
                            <div class="row">    
                                <div class="col-md-12">
                                    <strong style="color: red">Se Debe Registrar N° de Crédito, un Asesor de Crédito y Un Venddor(a) de concesionario Para poder realizar el pago de la Comisión de la Poliza de Seguro o GPS</strong>
                                </div>
                            </div>
                        <?php }?>    
                            
                        </div>
                    </div>


                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="poliza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <?php if($vendedor_id!=0&&$usuario_id!=0&&$credito_id!=0){?>
                        <button type="submit" class="btn btn-info" id="btn_guardar_poliza">Guardar</button>
                        <?php }?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/controlseguro/pagocomisiones_pagar.js?ver=14242423'; ?>"></script>