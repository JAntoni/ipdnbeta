/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    var monto = $('#hdd_poliza_total').val();
monedacambio_tipo_cambio()
    $('.poliza').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: monto
    });
    
    $('.moneda').autoNumeric({
        aSep: ',',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0.00',
        vMax: '9999999.99'
    });

    $('#poliza_fil_picker1').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
        endDate: new Date()
    });
});


$('#form_poliza').validate({
    submitHandler: function () {
        $.ajax({
            type: "POST",
            url: VISTA_URL + "controlseguro/controlseguro_reg.php",
            async: true,
            dataType: "json",
            data: $("#form_poliza").serialize(),
            beforeSend: function () {
            },
            success: function (data) {
                if (parseInt(data.estado) > 0) {
                    swal_success("CORRECTO", data.msj, 2500);
                    setTimeout(function () {
                        controlseguro_tabla();
                        $('#modal_pagar_poliza').modal('hide');
                    }, 1000
                            );
                } else {
                    swal_warning("AVISO !!", data.msj, 3000);
                }
            },
            complete: function (data) {
                //console.log(data);
            },
            error: function (data) {
                swal_error("ALERTA DE ERROR !!", data.responseText, 3500);
            }
        });
    },
    rules: {
        txt_poliza_nom: {
            required: true,
            minlength: 2
        },
        txt_poliza_des: {
            required: true,
            minlength: 2
        }
    },
    messages: {
        txt_poliza_nom: {
            required: "Ingrese un nombre para el Usuario Grupo",
            minlength: "Como mínimo el nombre debe tener 2 caracteres"
        },
        txt_poliza_des: {
            required: "Ingrese una descripción para el Usuario Grupo",
            minlength: "La descripción debe tener como mínimo 2 caracteres"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
    }
});




function monedacambio_tipo_cambio() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "monedacambio/monedacambio_controller.php",
        async: true,
        dataType: "json",
        data: ({
            action: 'tipocambio_egreso',
            monedacambio_fec: $('#txt_poliza_fec').val(),
            moneda_id: $('#cmb_moneda_id1').val()
        }),
        beforeSend: function () {
        },
        success: function (data) {
            if (parseInt(data.estado) == 1) {
                $('#txt_poliza_tipcam').val(data.valor);
            }
//            else{
//                $('#txt_poliza_tipcam').val(1.00);
//            }
        },
        complete: function (data) {

        },
        error: function (data) {
            $('#span_error').show(300);
            $('#span_error').html('ERROR AL CONSULTAR: ' + data.responseText);
        }
    });
}




$('#cmb_moneda_id1').change(function (event) {
   
    var moneda_id = parseInt($(this).val());
    if (moneda_id == 1){
//         monedacambio_tipo_cambio();
        
        var tipo=Number($('#txt_poliza_tipcam').autoNumeric('get'));
        var pagar=$('#txt_pagar2').val();
        var total=parseFloat(tipo*pagar);
        
        console.log('TIPO='+tipo + ' pagar = '+pagar+' Total = '+total.toFixed(2));
        
        $('#txt_pagar').autoNumeric('set',total.toFixed(2));
    }
    if (moneda_id == 2){
//         monedacambio_tipo_cambio();
        
        var tipo=Number($('#txt_poliza_tipcam').autoNumeric('get'));
        var pagar=$('#txt_pagar2').val();
        var total=parseFloat(pagar);
        
        console.log('TIPO='+tipo + ' pagar = '+pagar+' Total = '+total.toFixed(2));
        
        $('#txt_pagar').autoNumeric('set',total.toFixed(2));
    }
//    else{
//        
//        var tipo=$('#txt_poliza_tipcam').val(1.00);
//        var pagar=$('#txt_pagar2').val();
//        var total=parseFloat(tipo*pagar);
//        
//        $('#txt_pagar').autoNumeric('set',total.toFixed(2));
//    }
});
