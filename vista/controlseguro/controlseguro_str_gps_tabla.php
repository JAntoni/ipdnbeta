<?php
require_once ('../../core/usuario_sesion.php');
require_once ('Poliza.class.php');
$oPoliza = new Poliza();
require_once ('Polizadetalle.class.php');
$oPolizadetalle = new Polizadetalle();
require_once ('../creditoasiveh/Creditoasiveh.class.php');
$oCreditoasiveh = new Creditoasiveh();
require_once ('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$fecha1 = fecha_mysql($_POST['fecha1']);// date('Y-m-d');
$fecha2 = fecha_mysql($_POST['fecha2']);
$cliente_id = intval($_POST['cliente_id']);
$poliza_tipo = intval($_POST['poliza_tipo']);
$poliza_categoria = intval($_POST['poliza_categoria']);

// $mes = $fecha2 / 30;
// $fechafinicial = date("d-m-Y", strtotime($fecha1 . "-8 month")); //fecha actual menos un mes
// $fechafinal = date("d-m-Y", strtotime($fecha1 . "+ " . $mes . " month")); //fecha actual mas los meses escogidos

$dts = $oPoliza->listar_todas_polizas($fecha1, $fecha2, $cliente_id, $poliza_tipo, $poliza_categoria);
?>


<table cellspacing="1" id="tabla_controlsegurodetallereporte" class="table">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" width="5%">ID</th>
            <th id="tabla_cabecera_fila" width="5%">TIPO</th>
            <th id="tabla_cabecera_fila" width="5%">CATEGORIA</th>
            <th id="tabla_cabecera_fila" width="10%">N° POLIZA</th>
            <th id="tabla_cabecera_fila" width="20%">CLIENTE</th>
            <th id="tabla_cabecera_fila" width="20%">RAZON SOCIAL</th>
            <th id="tabla_cabecera_fila" width="20%">FECHA Vencimiento</th>
            <th id="tabla_cabecera_fila" width="10%">ESTADO</th>
            <th id="tabla_cabecera_fila" width="5%">ID CREDITO</th>
            <th id="tabla_cabecera_fila" width="5%">TIPO CREDITO</th>
            <th id="tabla_cabecera_fila" width="5%">PRECIO POLIZA</th>
            <th id="tabla_cabecera_fila" width="5%">OPCIONES</th>
        </tr>
    </thead> 
    <tbody>
        <?php
        if ($dts['estado'] == 1) {
            foreach ($dts['data']as $key => $dt) {

                $tipo_cre = '' . $dt['tb_creditotipo_id'];
                $placa = '';
                $tipo = '';
                $categoria = '';
                $estado = '<strong style="color: red;">NO RENOVAR</strong>';
                $estado2 = '';
                $contar = 0;
                $mensaje='';

                if ($dt['credtipo'] == 2) {
                    $tipo_cre = 'ASI. VEH';
                    $resultasi = $oCreditoasiveh->mostrarUno($dt['tb_credito_id']);
                    if ($resultasi['estado'] == 1) {
                        $placa = $resultasi['data']['tb_credito_vehpla'];
                        $result = $oPoliza->mostarCredito_Placa('tb_creditoasiveh', $placa);
                        
                        if ($result['estado'] == 1) {
                            $estado = '<strong style="color: blue;">POR RENOVAR</strong>';

                            $resultPolizadetalle = $oPolizadetalle->listar_detalle_poliza($dt['tb_poliza_id']);

                            if ($resultPolizadetalle['estado'] == 1) {
                                foreach ($resultPolizadetalle['data']as $key => $value) {
                                    $pagada= intval($value['tb_polizadetalle_est']);
                                    if($pagada==0){
                                        $contar++;
                                        
                                    }
                                }
                                if($contar>0){
                                    $mensaje='<strong style="color: black;">NO CUMPLE REQUISITOS</strong>';
                                }
                                 if($contar==0){
                                    $mensaje='[<strong style="color: green;"> CUMPLE REQUISITOS</strong>]';
                                }
                            }
                        }
                        $result = NULL;
                    }
                }
                if ($dt['credtipo'] == 3) {
                    $tipo_cre = 'GAR. MOB';
                    $resultgar = $oCreditogarveh->mostrarUno($dt['tb_credito_id']);
                    if ($resultgar['estado'] == 1) {
                        $placa = $resultgar['data']['tb_credito_vehpla'];
                        
                        $result = $oPoliza->mostarCredito_Placa('tb_creditogarveh', $placa);
                        if ($result['estado'] == 1) {
                            $estado = '<strong style="color: blue;">POR RENOVAR</strong>';
                            
                            $resultPolizadetalle = $oPolizadetalle->listar_detalle_poliza($dt['tb_poliza_id']);
                            if ($resultPolizadetalle['estado'] == 1) {
                                foreach ($resultPolizadetalle['data']as $key => $value) {
                                    $pagada= intval($value['tb_polizadetalle_est']);
                                    if($pagada==0){
                                        $contar++;
                                    }
                                }
                                if($contar>0){
                                    $mensaje='<strong style="color: black;">NO CUMPLE REQUISITOS </strong>';
                                }
                                 if($contar==0){
                                    $mensaje='[<strong style="color: green;"> CUMPLE REQUISITOS</strong>]';
                                }
                            }
                            
                        }
                        $result = NULL;
                    }
                }


                if ($dt['tb_poliza_tipo'] == 1) {
                    $tipo = '<strong style="color: green;">STR</strong>';
                }
                if ($dt['tb_poliza_tipo'] == 2) {
                    $tipo = '<strong style="color: blue;">GPS</strong>';
                }

                if ($dt['tb_poliza_categoria'] == 1) {
                    $categoria = '<strong style="color: green;">NUEVO</strong>';
                }
                if ($dt['tb_poliza_categoria'] == 2) {
                    $categoria = '<strong style="color: blue;">RENOVACIÓN</strong>';
                }


                $fecha_inicial = date("d-m-Y", strtotime($dt['tb_poliza_fecfin']));
                $fecha_final = date('d-m-Y');
                $dias = (strtotime($fecha_inicial) - strtotime($fecha_final)) / 86400;

                $vencio = '<strong style="color: black;">Este STR o GPS vence el dia de hoy </strong>';
                if ($dias > 0) {
                    $vencio = '<strong style="color: green;">Este STR o GPS vence dentro de' . abs($dias) . ' dia(s)</strong>';
                }
                if ($dias < 0) {
                    $vencio = '<strong style="color: red;">Este STR o GPS venció hace ' . abs($dias) . ' dia(s)</strong>';
                }
                ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $dt['tb_poliza_id'] ?></td>
                    <td id="tabla_fila"><?php echo $tipo; ?></td>
                    <td id="tabla_fila"><?php echo $categoria; ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_poliza_num']; ?></td>
                    <td id="tabla_fila" style="text-align: right"><?php echo $dt['tb_cliente_nom']; ?></td>
                    <?php
                    $razon = "";
                    if ($dt['tb_cliente_tip'] == 2) {
                        $razon = $dt['tb_cliente_emprs'];
                    }
                    ?>

                    <td id="tabla_fila" style="text-align: right"><?php echo $razon; ?></td>
                    <td id="tabla_fila" style="text-align: right"><?php echo '<b>' . mostrar_fecha($dt['tb_poliza_fecfin']) . '</b> - ' . $vencio ?></td>
                    <td id="tabla_fila"><?php echo $estado.' '.$mensaje ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_credito_id']; ?></td>
                    <td id="tabla_fila"><?php echo $tipo_cre; ?></td>
                    <td id="tabla_fila"><?php echo 'US$ ' . $dt['tb_poliza_pre']; ?></td>
                    <td id="tabla_fila">
                        <a class="btn btn-info btn-xs" onClick="controlseguro_form('Leer', <?php echo $dt['tb_poliza_id']; ?>)" title="Ver Poliza"><i class="fa fa-eye"></i></a>
                        <a class="btn btn-danger btn-xs" onClick="controlseguroSTR_GPS_form(<?php echo $dt['tb_poliza_id']; ?>)" title="Ocultar Poliza"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                    </td>
                </tr> 
            <?php }
            ?>
    </table>
<?php }
?>