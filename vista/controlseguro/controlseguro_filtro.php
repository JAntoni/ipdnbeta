<form id="form_controlsefuro_filtro" role="form">
    <div class="row mt-4">
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Fecha:</label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ing_fec1" id="txt_fil_ing_fec1" value="<?php echo date('01-01-Y'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fil_ing_fec2" id="txt_fil_ing_fec2" value="<?php echo date('d-m-Y'); ?>" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="cmb_tipo">Tipo:</label>
                <!-- <div class="input-group">
                                <div class="input-group-addon">
                                    <span><i class="fa fa-filter"></i></span>
                                </div> -->
                <select class="form-control input-sm" name="cbo_tipo" id="cbo_tipo">
                    <option value="0">Todos los Tipos</option>
                    <!-- <option value="1">Seguro Todo Riezgo</option> -->
                    <option value="2" selected>GPS</option>
                </select>
                <!-- </div> -->
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="txt_seguro_fil_cli_nom">Cliente:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><i class="fa fa-user"></i></span>
                    </div>
                    <input type="text" id="txt_seguro_fil_cli_nom" class="form-control input-sm mayus" placeholder="ingrese cliente a buscar" size=50>
                    <input type="hidden" id="hdd_seguro_fil_cli_id">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="cbo_categoria">Categoria:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><i class="fa fa-filter"></i></span>
                    </div>
                    <select class="form-control input-sm" name="cbo_categoria" id="cbo_categoria">
                        <option value="0">Todas las Categorias</option>
                        <option value="1">Nuevo</option>
                        <option value="2">Renovación</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="cbo_categoria">&nbsp;</label> <br>
                <button type="button" class="btn btn-primary btn-sm" onClick="controlseguro_tabla()"><i class="fa fa-search fa-fw" aria-hidden="true"></i> Buscar</button>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <label for="cbo_categoria">&nbsp;</label> <br>

            </div>
        </div>
    </div>
</form>