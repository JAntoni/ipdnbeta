<?php
require_once ("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$polizadetalle_id = intval($_POST['polizadetalle_id']);
$poliza_id = intval($_POST['poliza_id']);

$result = $oPolizadetalle->mostarUno($polizadetalle_id);

if ($result['estado'] == 1) {
    $polizadetalle_cupon = $result['data']['tb_polizadetalle_cupon'];
    $polizadetalle_fec = $result['data']['tb_polizadetalle_fec'];
    $polizadetalle_imp = $result['data']['tb_polizadetalle_imp'];
    $polizadetalle_detalle = $result['data']['tb_polizadetalle_det'];
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_controlsegurodetalle" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold">EDITAR CUPÓN DE POLIZA</h4>
            </div>

            <form id="form_controlsegurodetalle_edit">
                <input type="hidden" name="hdd_polizadetalle_id" id="hdd_polizadetalle_id" value="<?php echo $polizadetalle_id; ?>">
                <input type="hidden" name="hdd_poliza_id" id="hdd_poliza_id" value="<?php echo $poliza_id; ?>">

                <div class="modal-body">

                    <!-- <div class="box box-primary"> -->
                        <!-- <div class="box-header"> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Cupón:</label>
                                    <input type="text" class="form-control input-sm" name="txt_controlsegurodetalle_cupon" id="txt_controlsegurodetalle_cupon" value="<?php echo $polizadetalle_cupon; ?>">
                                </div>
                            </div>
                            <p>
                        <?php // if ($_SESSION['usuario_id'] == 35 || $_SESSION['usuario_id'] == 46) { ?> 
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Fecha:</label>
                                        <div class='input-group date' id='datetimepicker1_detalle'>
                                            <input type="text" class="form-control input-sm" name="txt_fecha" id="txt_fecha"  value="<?php echo mostrar_fecha($polizadetalle_fec); ?>">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Monto:</label>
                                    <input type="text" class="form-control input-sm moneda" name="txt_monto" id="txt_monto" value="<?php echo $polizadetalle_imp; ?>">
                                </div>
                            </div>
                            <p>
                            <?php if ( $_SESSION['usuario_id'] == 35 || $_SESSION['usuario_id'] == 52 ) { ?> 
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Detalle:</label>
                                    <textarea type="text" class="form-control input-sm" name="txt_detalle" id="txt_detalle"><?php echo $polizadetalle_detalle; ?></textarea>
                                </div>
                            </div>
                             <?php } ?> 
                        <?php // } ?>    
                        <!-- </div> -->
                    <!-- </div> -->
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_controlsegurodetalle">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>



        </div>
    </div>
</div>



<script type="text/javascript" src="<?php echo 'vista/controlseguro/controlsegurodetalle_form_edit.js?ver=646464646464646'; ?>"></script>