<?php
require_once("../../core/usuario_sesion.php");
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once('../vendedor/Vendedor.class.php');
$oVendedor = new Vendedor();

require_once('../creditogarveh/Creditogarveh.class.php');
$oCreditogarveh = new Creditogarveh();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = 'insertar';

$poliza_id = intval($_POST['poliza_id']);
$cliente_id = intval($_POST['cliente_id']);
$gps_precio = intval($_POST['gps_precio']);
$num_cuotas = intval($_POST['num_cuotas']);
$poliza_fecini = date('d-m-Y');

if($poliza_id > 0){
  $action = 'editar';

  $result = $oPoliza->mostrarUno($poliza_id);
    if($result['estado'] == 1){
      $poliza_respag2 = $result['data']['tb_poliza_respag2'];
      $proveedorpoliza_id = $result['data']['tb_proveedor_id'];
      $vehiculouso_id = $result['data']['tb_poliza_tip'];
      $poliza_fecini = mostrar_fecha($result['data']['tb_poliza_fecini']);
      $poliza_fecfin = mostrar_fecha($result['data']['tb_poliza_fecfin']);
    }
  $result = NULL;
}

echo ' /// cliente: '.$cliente_id.' // gps precio: '.$gps_precio.' // num cuotas: '.$num_cuotas.' // credito hash: '.$credito_hash;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_gps_simple" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold">REGISTRO DATOS DEL GPS CONTRATADO</h4>
      </div>

      <form id="form_gps_simple">
        <input type="hidden" name="action" value="<?php echo $action; ?>">
        <input type="hidden" name="hdd_poliza_id" value="<?php echo $poliza_id;?>">
        <input type="hidden" name="hdd_poliza_cliente_id" value="<?php echo $cliente_id;?>">
        <input type="hidden" name="hdd_poliza_moneda_id" value="2">

        <div class="modal-body">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row">

                <div class="col-md-6">
                  <label>Facturas a Nombre de :</label>
                  <select id="cmb_poliza_respag" name="cmb_poliza_respag" class="form-control input-sm">
                    <option value="">Selecciona...</option>
                    <option value="1" <?php if ($poliza_respag2 == 1) echo 'selected'; ?> >Cliente</option>
                    <option value="2" <?php if ($poliza_respag2 == 2) echo 'selected'; ?> >Empresa IPDN</option>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Aseguradora:</label>
                  <select id="txt_cliente_aseg" name="txt_cliente_aseg"  class="form-control input-sm">
                    <?php require_once '../proveedorpoliza/proveedorpoliza_select.php';?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label>Uso Vehículo:</label>
                  <select id="cmb_poliza_tip" name="cmb_poliza_tip"  class="form-control input-sm">
                    <?php require_once '../vehiculouso/vehiculouso_select.php';?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label>Meses a Financiar:</label>
                  <input id="txt_poliza_meses" name="txt_poliza_meses"  class="form-control input-sm" value="<?php echo $num_cuotas?>" readonly/>
                </div>
                <div class="col-md-4">
                  <label>Precio GPS $USD:</label>
                  <input id="txt_poliza_pre" name="txt_poliza_pre"  class="form-control input-sm" value="<?php echo $gps_precio?>" readonly/>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Fecha Inicio:</label>
                    <div class='input-group date' id='polizapicker1'>
                      <input type="text" class="form-control input-sm" name="txt_poliza_fecini" id="txt_poliza_fecini" autocomplete="off" value="<?php echo $poliza_fecini; ?>">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Fecha Fin:</label>
                    <div class='input-group date' id='polizapicker2'>
                      <input type="text" class="form-control input-sm" name="txt_poliza_fecfin" id="txt_poliza_fecfin" autocomplete="off" value="<?php echo $poliza_fecfin; ?>">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_controlseguro">Guardar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/controlseguro/gps_form_simple.js?ver=1'; ?>"></script>