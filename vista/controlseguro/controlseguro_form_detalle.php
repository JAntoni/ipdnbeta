<?php
require_once("../controlseguro/Poliza.class.php");
$oPoliza = new Poliza();
require_once("../controlseguro/Polizadetalle.class.php");
$oPolizadetalle = new Polizadetalle();
require_once ('../upload/Upload.class.php');
$oUploap = new Upload();
require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$poliza_id = (isset($_POST['poliza_id']))? trim($_POST['poliza_id']) : $poliza_id;
$action = (isset($_POST['action']))? trim($_POST['action']) : $action;

if ($action == 'editar' || $action == 'Leer') {

    $dts = $oPolizadetalle->listar_detalle_poliza($poliza_id);
    if ($vista == 'creditogarveh') { $bloquearbtn = 'disabled'; }
    if ($dts['estado'] == 1) {
        $numero = 1;

        foreach ($dts['data']as $key => $dt) {
            $btn ='';
            $estado = '<strong style="color: red;">Pendiente</strong>';

            if ($dt['tb_polizadetalle_est'] == 0) {
                $btn = '<a class="btn btn-warning btn-xs" '.$bloquearbtn.' id="btn_det_' . $dt['tb_polizadetalle_id'] . '" onClick="modificar(' . $dt['tb_polizadetalle_id'] . ',' . $dt['tb_poliza_id'] . ')" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; &nbsp;'
                        . '<a class="btn btn-success btn-xs" '.$bloquearbtn.' id="btn_det_' . $dt['tb_polizadetalle_id'] . '" onClick="abrir_pago(' . $dt['tb_polizadetalle_id'] . ',' . $dt['tb_poliza_id'] . ')" title="Pagar"><i class="fa fa-money"></i></a> &nbsp; &nbsp;';
            }
            if ($dt['tb_polizadetalle_est'] == 1) {
                $estado = '<strong style="color: green;">Pagado</strong>';
                $imagen = $oUploap->mostrarUno_modulo($dt['tb_polizadetalle_id'], 'controlsegurodetalle');

                if ($_SESSION['usuario_id'] == 35 || $_SESSION['usuario_id'] == 52) {
                    $btn .= '<a class="btn btn-warning btn-xs" '.$bloquearbtn.' id="btn_det_' . $dt['tb_polizadetalle_id'] . '" onClick="modificar(' . $dt['tb_polizadetalle_id'] . ',' . $dt['tb_poliza_id'] . ')" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; &nbsp;';
                }
                $btn .= '<a class="btn btn-info btn-xs" title="Subir Imagen" '.$bloquearbtn.' onclick="upload_form(' . $dt['tb_polizadetalle_id'] . ')"><i class="fa fa-camera"></i></a> &nbsp; &nbsp;' .
                        '<a class="btn btn-facebook btn-xs" title="Ver Imagen" '.$bloquearbtn.' onclick="Abrir_imagen(' . $dt['tb_polizadetalle_id'] . ')"><i class="fa fa-picture-o"></i></a> &nbsp; &nbsp;';

                if ($dt['tb_poliza_respag2'] == 2) {
                    $PDF = $oUploap->mostrarUno_modulo($dt['tb_polizadetalle_id'], 'segurosdetalle');

                    if ($PDF['estado'] == 1) {
                        $btn .= ' <a href="javascript:void(0)" class="btn btn-primary btn-xs" '.$bloquearbtn.' onclick="contratos_SegurosDetalle_form(' . $dt['tb_polizadetalle_id'] . ')"><i class="fa fa-file-pdf-o" title="Ver Contratos"></i></a> &nbsp; &nbsp;';
                    } else {
                        $btn .= ' <a href="javascript:void(0)" class="btn btn-danger btn-xs" '.$bloquearbtn.' onclick="abrirfactura_form(\'I\',0,' . $dt['tb_polizadetalle_id'] . ')" title="Subir Pdf de la Poliza SRT o GPS"><i class="fa fa-file-pdf-o"></i></a> &nbsp; &nbsp;';
                    }
                }
            }
            $btn .= '<a href="javascript:void(0)" class="btn btn-facebook btn-xs" '.$bloquearbtn.' onclick="his_poliza(' . $dt['tb_polizadetalle_id'] . ',' . $dt['tb_polizadetalle_id'] . ')" title="Agregar Comentario"> <i class="fa fa-file"></i> <i class="fa fa-pencil fa-rotate-90"></i> Nota</a> ';

            echo '
                <tr id="tabla_cabecera_fila">
                  <td id="tabla_fila">' . $dt['tb_polizadetalle_id'] . '</td>
                  <td id="tabla_fila">' . $dt['tb_polizadetalle_cupon'] . '</td>
                  <td id="tabla_fila">' . mostrar_fecha($dt['tb_polizadetalle_fec']) . '</td>
                  <td id="tabla_fila">US$ ' . mostrar_moneda($dt['tb_polizadetalle_imp']) . '</td>
                  <td id="tabla_fila">' . $estado . '</td>
                  <td id="tabla_fila">' . $dt['tb_polizadetalle_det'] . '</td>
                  <td id="tabla_fila">' . $btn . '</td>
                </tr>';
            $numero++;
        }
    }
}
?>