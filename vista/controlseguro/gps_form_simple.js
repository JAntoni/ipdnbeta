$(document).ready(function () {
  $('#polizapicker1, #polizapicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
  //        endDate: new Date()
  });

  $("#polizapicker1").on("change", function (e) {
      var startVal = $('#txt_poliza_fecini').val();
      $('#polizapicker2').data('datepicker').setStartDate(startVal);
  });
  $("#polizapicker2").on("change", function (e) {
      var endVal = $('#txt_poliza_fecfin').val();
      $('#polizapicker1').data('datepicker').setEndDate(endVal);
  });

  $('#txt_poliza_fecini').change(function(){
    calcular_fechafin_gps();
  });

  calcular_fechafin_gps();

  $("#form_gps_simple").validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url: VISTA_URL + "controlseguro/gps_controller_simple.php",
        async: true,
        dataType: "json",
        data: $("#form_gps_simple").serialize(),
        beforeSend: function () {},
        success: function (data) {
          if(parseInt(data.estado) > 0){
            notificacion_success(data.mensaje);
            get_poliza_id(data); //funcion que estará en garveh, adenda, amortizacion
            $('#modal_gps_simple').modal('hide');
          }
        },
        complete: function (data) {
          console.log(data);
        },
      });
    },
    rules: {
      cmb_poliza_respag: {
        required: true,
      },
      txt_cliente_aseg: {
        min: 1,
      },
      cmb_poliza_tip: {
        min: 1,
      },
      txt_poliza_meses: {
        required: true,
      },
      txt_poliza_fecini: {
        required: true,
      },
      txt_poliza_fecfin: {
        required: true,
      }
    },
    messages: {
      txt_cliente_aseg: {
        required: "Aseguradora",
      },
      cmb_poliza_tip: {
        required: "Tipo de Uso del Vehículo",
      },
      txt_poliza_fecini: {
        required: "Fecha de Inicio",
      },
      txt_poliza_fecfin: {
        required: "Fecha Fin",
      },
      cmb_poliza_respag: {
        required: "Responsable del Pago",
      },
      txt_poliza_meses: {
        required: "Cuotas a financiar el GPS",
      },
    },
  });
});

function calcular_fechafin_gps(){
  // Obtener la fecha inicial
  var fechaInicialString  = $('#txt_poliza_fecini').val();

  // Convertir la cadena de fecha al formato YYYY-MM-DD
  var partesFecha = fechaInicialString.split('-');
  var fechaInicial = new Date(partesFecha[2], partesFecha[1] - 1, partesFecha[0]);

  // Obtener el número de meses ingresado
  var meses = parseInt($('#txt_poliza_meses').val(), 10) || 0; // Asegurarse de que sea un número válido

  // Calcular la fecha resultado
  var fechaResultado = new Date(fechaInicial);
  fechaResultado.setMonth(fechaResultado.getMonth() + meses);
  
  // Formatear la fecha resultado como DD-MM-YYYY
  var dia = fechaResultado.getDate();
  var mes = fechaResultado.getMonth() + 1; // Los meses comienzan desde 0
  var año = fechaResultado.getFullYear();

  // Agregar ceros a la izquierda si es necesario
  dia = dia < 10 ? '0' + dia : dia;
  mes = mes < 10 ? '0' + mes : mes;

  var resultadoFormateado = dia + '-' + mes + '-' + año;

  // Establecer la fecha resultado en el campo correspondiente
  $('#txt_poliza_fecfin').val(resultadoFormateado);
}
