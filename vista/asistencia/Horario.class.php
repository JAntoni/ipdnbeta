<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Horario extends Conexion {

  public $horariodetalle_id;
  public $horariodetalle_usureg;
  public $horariodetalle_fec;
  public $usuario_id;
  public $empresa_id;
  public $horariodetalle_horini;
  public $horariodetalle_horfin;
  public $horariodetalle_nom;

  function insertar_horariodetalle() {
    $this->dblink->beginTransaction();
    try {
      $columns = implode(', ', [
        'tb_horariodetalle_usureg',
        'tb_horariodetalle_fec', 
        'tb_usuario_id', 
        'tb_empresa_id', 
        'tb_horariodetalle_horini', 
        'tb_horariodetalle_horfin', 
        'tb_horariodetalle_nom'
      ]);

      $placeholders = implode(', ', array_map(function($key) {
        return ":$key";
        }, [
          'tb_horariodetalle_usureg',
          'tb_horariodetalle_fec', 
          'tb_usuario_id', 
          'tb_empresa_id', 
          'tb_horariodetalle_horini', 
          'tb_horariodetalle_horfin', 
          'tb_horariodetalle_nom'
        ])
      ); //function itera cada valor del array que se le asigne, devuelve: :tb_usuario_id, :tb_credito_id, :tb_cliente_id

      $sql = "INSERT INTO tb_horariodetalle($columns) VALUES ($placeholders)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_horariodetalle_usureg", $this->horariodetalle_usureg, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_horariodetalle_fec", $this->horariodetalle_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $this->usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_empresa_id", $this->empresa_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_horariodetalle_horini", $this->horariodetalle_horini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_horariodetalle_horfin", $this->horariodetalle_horfin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_horariodetalle_nom", $this->horariodetalle_nom, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function eliminar_registros_horariodetalle($usuario_id, $mes, $anio) {
    $this->dblink->beginTransaction();
    try {
      $sql = "DELETE FROM tb_horariodetalle WHERE tb_usuario_id =:usuario_id AND month(tb_horariodetalle_fec) =:mes AND year(tb_horariodetalle_fec) =:anio";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
      $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function listar_horario_usuario_mes_anio_array($usuario_id, $mes, $anio) {
    try {
      $sql = "SELECT hor.*, GROUP_CONCAT(tb_horariodetalle_nom SEPARATOR ',') as array FROM tb_horariodetalle hor 
        WHERE tb_usuario_id =:usuario_id AND month(tb_horariodetalle_fec) =:mes AND year(tb_horariodetalle_fec) =:anio 
        GROUP BY tb_horariodetalle_fec";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
      $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay horario registrado";
        $retorno["data"] = "";
      }

      return $retorno;
      } catch (Exception $e) {
          throw $e;
      }
  }
  function listar_horario_anterior_dia_semana($usuario_id, $mes, $anio) {
    try {
      $sql = "SELECT (CASE WHEN WEEKDAY(tb_horariodetalle_fec) = 0 THEN 'Lunes'
          WHEN WEEKDAY(tb_horariodetalle_fec) = 1 THEN 'Martes' 
          WHEN WEEKDAY(tb_horariodetalle_fec) = 2 THEN 'Miércoles'
          WHEN WEEKDAY(tb_horariodetalle_fec) = 3 THEN 'Jueves'
          WHEN WEEKDAY(tb_horariodetalle_fec) = 4 THEN 'Viernes'
          WHEN WEEKDAY(tb_horariodetalle_fec) = 5 THEN 'Sábado'
          WHEN WEEKDAY(tb_horariodetalle_fec) = 6 THEN 'Domingo' ELSE 'Sin dia' END) as dia_semana,
          GROUP_CONCAT(tb_horariodetalle_nom SEPARATOR ',') as array 
        FROM tb_horariodetalle hor 
        WHERE tb_usuario_id =:usuario_id AND month(tb_horariodetalle_fec) =:mes AND year(tb_horariodetalle_fec) =:anio 
        GROUP BY 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":mes", $mes, PDO::PARAM_STR);
      $sentencia->bindParam(":anio", $anio, PDO::PARAM_STR);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay horario registrado";
        $retorno["data"] = "";
      }

      return $retorno;
      } catch (Exception $e) {
          throw $e;
      }
  }

  function asignar_turno_laboral($fecha, $usuario_id){
    $this->dblink->beginTransaction();
    try {
      $sql = "CALL fn_otorgar_turno_laboral(:fecha, :usuario_id)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);

      $sentencia->execute();
      $this->dblink->commit();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "Error al asignar turno de laboresl en el día";
        $retorno["data"] = "";
      }
      return $retorno;
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
}

?>
