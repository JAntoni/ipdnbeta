
function asistencia_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "asistencia/asistencia_tabla.php",
        async: false,
        dataType: "html",
        data: $('#form_asistencia_filtro').serialize(),
        beforeSend: function () {
            $('#div_asistencia_tabla').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#div_asistencia_tabla').html(html);
        },
        complete: function (data) {
        }
    });
}


$(document).ready(function () {

});

$("#cmb_usuario_id,#cmb_asistencia_mes,#cmb_asistencia_anio").change(function (){
    asistencia_tabla();
});

function horario_form() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "asistencia/horario_form.php",
        async: true,
        dataType: "html",
        data: ({
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_horario_form').html(html);
            $('#modal_horario_form').modal('show');
            modal_width_auto('modal_horario_form', 80);
            modal_hidden_bs_modal('modal_horario_form', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            if (data.status != 200)
                console.log(data);

        }
    });
}

function asistencia_form(act, id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "asistencia/asistencia_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: act,
            asis_id: id
        }),
        beforeSend: function () {
//      $('#div_modal_asistencia_form').dialog('open');
        },
        success: function (html) {
            $('#div_modal_asistencia_form').html(html);
            $('#modal_registro_asistencia').modal('show');
            modal_hidden_bs_modal('modal_registro_asistencia', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            if (data.status != 200)
                console.log(data);

        }
    });
}

function papeleta_form(asi, usu) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "asistencia/papeleta_form.php",
        async: true,
        dataType: "html",
        data: ({
            asis_id: asi,
            usu_id: usu
        }),
        beforeSend: function () {
//      $('#div_modal_papeleta_form').dialog('open');
        },
        success: function (html) {
            $('#div_modal_papeleta_form').html(html);
            $('#modal_registro_papeleta').modal('show');
            modal_hidden_bs_modal('modal_registro_papeleta', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            if (data.status != 200) {
                //        console.log(data);
            }

        }
    });
}

function asistencia_obs_form(id, jus) {
    $.ajax({
        type: "POST",
        url: VISTA_URL+"asistencia/asistencia_obs_form.php",
        async: true,
        dataType: "html",
        data: ({
            asis_id: id,
            justifica: jus
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_modal_asistencia_obs_form').html(html);
            $('#modal_registro_asistencia_obs').modal('show');
            modal_hidden_bs_modal('modal_registro_asistencia_obs', 'limpiar'); //funcion encontrada en public/js/generales.js
        },
        complete: function (data) {
            if (data.status != 200)
                console.log(data);

        }
    });
}

function eliminar_justificacion(id, tip) {
    var eliminar = '¿Seguro de que desea eliminar justificación?';
    if (tip == 2)
        eliminar = '¿Seguro de que desea corregir el día faltado?';

    $.confirm({
        icon: 'fa fa-print',
        title: 'Imprimir',
        content: eliminar,
        type: 'blue',
        theme: 'material', // 'material', 'bootstrap'
        typeAnimated: true,
        buttons: {
            si: function () {
                //ejecutamos AJAX
                $.ajax({
                    type: "POST",
                    url: VISTA_URL+"asistencia/asistencia_reg.php",
                    async: true,
                    dataType: "json",
                    data: ({
                        action: 'eliminar_jus',
                        asis_id: id,
                        eli_tip: tip
                    }),
                    beforeSend: function () {
                        
                    },
                    success: function (data) {
                        swal_success("ELIMINADO",data.mdj,2000);
                        asistencia_tabla();
                    },
                    complete: function (data) {
                        if (data.status != 200) {
                            $('#msj_asis').text(data.responseText);
                            console.log(data);
                        }

                    }
                });
            },
            no: function () {}
        }
    });
}

function asistencia_historial_form(asistencia_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "asistencia/asistencia_historial_form.php",
        async: true,
        dataType: "html",
        data: ({
            asistencia_id: asistencia_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (html) {
            $('#div_asistencia_historial_form').html(html);
            $('#div_modal_asistencia_historial_form').modal('show');
            $('#modal_mensaje').modal('hide');

            //funcion js para limbiar el modal al cerrarlo
            modal_hidden_bs_modal('div_modal_asistencia_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
            //funcion js para agregar un largo automatico al modal, al abrirlo
            modal_height_auto('div_modal_asistencia_historial_form'); //funcion encontrada en public/js/generales.js

        },
        complete: function (html) {
        }
    });
}