<?php
require_once '../../core/usuario_sesion.php';
require_once ("../asistencia/Asistencia.class.php");
$oAsistencia = new Asistencia();

$asis_id = $_POST['asis_id'];
$justi = $_POST['justifica']; //es un numero 0 o 1, 0 no justificado y 1 si justificado, 2 para ver el detalle o historial, 3 marcar dia faltado
$label = 'Observación:';
if ($justi == 1)
    $label = 'Detalle de Justificación:';
if ($justi == 3)
    $label = 'Observación Falta:';

$dts = $oAsistencia->mostrarUno($asis_id);
if ($dts['estado'] == 1) {
    $obs = $dts['data']['tb_asistencia_obs'];
    $his = $dts['data']['tb_asistencia_his'];
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_asistencia_obs" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Papeletas de Horas Extras</h4>
            </div>
            <form id="form_asis_obs" method="post">
                <input type="hidden" name="action" value="obs_jus">
                <input type="hidden" name="hdd_asis_id" value="<?php echo $asis_id; ?>">
                <input type="hidden" name="hdd_asis_jus" id="hdd_asis_jus" value="<?php echo $justi; ?>">

                <div class="modal-body">
                    <h5 style="font-family: cambria;font-weight: bold;color: #003eff">Observaciones anteriores</h5>
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-12" style="height: 140px; overflow-y: auto;">
                                    <?php
                                    if ($justi != 2) {
                                        if (empty($obs))
                                            echo '<p>Sin Observaciones</p>';
                                        else
                                            echo $obs;
                                    }
                                    if ($justi == 2) {
                                        if (empty($his))
                                            echo '<p>Sin Historial</p>';
                                        else
                                            echo $his;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="tbl_obs">
                        <div class="col-md-3">
                            <label><?php echo $label; ?></label>
                        </div>
                        <div class="col-md-9">
                            <textarea name="txt_asis_obs" id="txt_asis_obs" rows="2" cols="50" required class="form-control input-sm"></textarea>
                        </div>
                    </div>
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="papeleta_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guar_obs">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/asistencia/asistencia_obs_form.js'; ?>"></script>