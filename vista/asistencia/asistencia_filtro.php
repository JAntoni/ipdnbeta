<?php
	$filtro_fec1 = date('01-01-Y');
	$filtro_fec2 = date('d-m-Y');
?>
<style type="text/css">
  .label-filter{
    padding-left: 1%;
    padding-right: 1%;
    font-size: 12pt;
  }
  .select-filter{
    width: 15%;
  }
</style>
<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="panel panel-info">
		  <div class="panel-body">
		    <form id="form_asistencia_filtro" class="form-inline" role="form">
          <label for="txt_fil_cliente_nom" class="control-label">Colaborador:</label>
          <div class="form-group select-filter">
            <select class="form-control form-control-sm selectpicker" id="cmb_usuario_id" name="cmb_usuario_id" data-live-search="true" data-max-options="1" data-size="12">
              <?php 
                $usuario_columna = 'tb_usuario_ofi';
                $usuario_valor = 1; // 1 para todos los usuarios que trabajan en oficina o trabajaron en oficina
                $param_tip = 'INT';
                require_once(VISTA_URL.'usuario/usuario_select.php');
              ?>
            </select>
          </div>
          <div class="form-group">
            <label for="txt_fil_cliente_nom" class="control-label">Mes:</label>
            <select class="form-control input-sm mayus" id="cmb_asistencia_mes" name="cmb_asistencia_mes">
              <?php 
                $hoy_mes = date('m');
                $mes = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $num = array('01','02','03','04','05','06','07','08','09','10','11','12');
                $sel = '';

                for ($i=0; $i < 12 ; $i++) { 
                  if($num[$i] == $hoy_mes)
                    echo '<option value="'.$num[$i].'" selected>'.$mes[$i].'</option>';
                  else
                    echo '<option value="'.$num[$i].'">'.$mes[$i].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label for="txt_fil_cliente_nom" class="control-label">Año:</label>
            <select class="form-control input-sm mayus" id="cmb_asistencia_anio" name="cmb_asistencia_anio">
              <?php 
                $anio_actu = intval(date('Y')); 
                $anio = $anio_actu - 2000;
                for ($i = 17; $i <= $anio; $i++) { 
                  $suma_anio = 2000 + $i;
                  if($anio_actu == $suma_anio)
                    echo '<option value="'.$i.'" selected="true">'.$suma_anio.'</option>';
                  else
                    echo '<option value="'.$i.'">'.$suma_anio.'</option>';
                }

              ?>
            </select>
          </div>
          <button type="button" class="btn btn-info btn-sm" onclick="asistencia_tabla()"><i class="fa fa-search"></i> Buscar</button>
          <button type="button" class="btn btn-info btn-sm" onclick="horario_form()"><i class="fa fa-search"></i> Horario Personalizado</button>
        </form>
		  </div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12" id="asistencia_mensaje_opc" style="padding-top: 5px;"> 
	</div>
</div>