<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'asistencia/Asistencia.class.php');
    require_once(VISTA_URL . 'usuario/Usuario.class.php');
    require_once(VISTA_URL . 'funciones/funciones.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../asistencia/Asistencia.class.php');
    require_once('../usuario/Usuario.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');
}


$oUsuario = new Usuario();
$oAsistencia = new Asistencia();

$usuario_id = $_POST['cmb_usuario_id'];
$asistencia_mes = $_POST['cmb_asistencia_mes'];
$asistencia_anio = $_POST['cmb_asistencia_anio'];
$mes_anio = $asistencia_mes . '-' . $asistencia_anio;

$bandera = 0;
if (!empty($usuario_id)) {
    $result = $oUsuario->mostrarUno($usuario_id);
    if ($result['estado'] == 1) {
        $usuario_ing1 = $result['data']['tb_usuario_ing1']; //hora ingreso turno mañana
        $usuario_sal1 = $result['data']['tb_usuario_sal1']; //hora salida turno mañana
        $usuario_ing2 = $result['data']['tb_usuario_ing2']; //hora ingreso turno tarde
        $usuario_sal2 = $result['data']['tb_usuario_sal2']; //hora salida turno tarde
    }
    $result = NULL;

    $asistenciaArray = NULL;
    $result = $oAsistencia->listar_asistencia_mes($usuario_id, $mes_anio);
    if ($result['estado'] == 1) {
        $asistenciaArray = $result['data'];
        $bandera = 1;
    }
    $result = NULL;
} else {
    $mensaje = '<center><h2>SELECCIONE UN COLABORADOR PARA LISTAR SUS ASISTENCIAS</h2></center>';
}
?>
<?php if ($bandera == 1): ?>
    <table id="tbl_asistencias" class="table table-bordered table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">DIA</th>
                <th id="tabla_cabecera_fila">INGRESO</th>
                <th id="tabla_cabecera_fila">SALIDA</th>
                <th id="tabla_cabecera_fila">REFRIGERIO</th>
                <th id="tabla_cabecera_fila">INGRESO</th>
                <th id="tabla_cabecera_fila">SALIDA</th>
                <th id="tabla_cabecera_fila">DE MÁS</th>
                <th id="tabla_cabecera_fila">EXTRAS</th>
                <th id="tabla_cabecera_fila">H.TRABAJADAS</th>
                <th id="tabla_cabecera_fila">OBSERVACIÓN</th>
                <th id="tabla_cabecera_fila">JUSTIFICAR</th>
                <th id="tabla_cabecera_fila">FALTAS</th>
                <th id="tabla_cabecera_fila"></th>
            </tr>
        </thead>
        <tbody> <?php foreach ($asistenciaArray as $key => $value) { 
                $borde = '#E08E0B';
                $grosor_borde = 'border-width: 1.5px !important;';
                if($value['tb_asistencia_obs']!=null || $value['tb_asistencia_obs']!=''){
                    $borde = '#00ACD7';
                    $grosor_borde = 'border-width: 3px !important;';
                }
                ?>
                <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila">
                        <?php $asistencia_cod = explode('-', $value['tb_asistencia_cod']);
                        echo $asistencia_cod[0]
                        ?>
                    </td>
                    <td id="tabla_fila"><?php echo $value['tb_asistencia_ing1']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_asistencia_sal1']; ?></td>
                    <td id="tabla_fila"><span style="color: green;">13:00 | 15:00</span></td>
                    <td id="tabla_fila"><?php echo $value['tb_asistencia_ing2']; ?></td>
                    <td id="tabla_fila"><?php echo $value['tb_asistencia_sal2']; ?></td>
                    <td id="tabla_fila" style="background-color: rgb(252,248,227);">
                        <?php
                        $hor_demas = 0;
                        if (!empty($ing1) && !empty($sal1)) {
                            if (!empty($value['tb_asistencia_ing1']) && !empty($value['tb_asistencia_sal1'])) {
                                $ing_marcado = $value['tb_asistencia_ing1'];
                                $sal_marcado = $value['tb_asistencia_sal1'];

                                if ($ing1 >= $ing_marcado) {
                                    //si tu horario de ingreso es mayor o igual que tu marcado, entonces tomamos tu horario de ing y tomamos tu salida marcada para calcular horas demás
                                    //salida marcada(hor fin) - horario de ingreso(hor ini)
                                    $hor_demas += horas_demas_base_numero_horas($ing1, $sal_marcado, 4); //minutos demás en base a un umero de horas
                                }
                            }
                        }
                        if (!empty($ing2) && !empty($sal2)) {
                            if (!empty($value['tb_asistencia_ing2']) && !empty($value['tb_asistencia_sal2'])) {
                                $ing_marcado = $value['tb_asistencia_ing2'];
                                $sal_marcado = $value['tb_asistencia_sal2'];

                                if ($ing2 >= $ing_marcado) {
                                    //si tu horario de ingreso es mayor o igual que tu marcado, entonces tomamos tu horario de ing y tomamos tu salida marcada para calcular horas demás
                                    //salida marcada(hor fin) - horario de ingreso(hor ini)
                                    $hor_demas += horas_demas_base_numero_horas($ing2, $sal_marcado, 4); //minutos demás en base a un umero de horas
                                }
                            }
                        }
                        echo $hor_demas . ' min';
                        ?>
                    </td>
                    <td id="tabla_fila" style="background-color: rgb(223,240,216);"><?php echo $value['tb_asistencia_ext'] . ' min'; ?></td>
                    <td id="tabla_fila">
                        <?php 
                            $suma1= Horas_Trabajadas($value['tb_asistencia_ing1'],$value['tb_asistencia_sal1']);
                            $suma2= Horas_Trabajadas($value['tb_asistencia_ing2'],$value['tb_asistencia_sal2']);
                            
                            echo sumahoras($suma1,$suma2);

                        ?>
                    </td>
                    <td id="tabla_fila">
                            <?php //echo substr($value['tb_asistencia_obs'], 0, 20); ?>
                        <a class="btn btn-warning btn-xs" href="javascript:void(0)" style="<?php echo $grosor_borde; ?> border-color: <?php echo $borde; ?> !important;" onClick="asistencia_obs_form(<?php echo $value['tb_asistencia_id']; ?>, 0)">Obs.</a>
                    </td> 
                    
                    <td id="tabla_fila" align="center">
                        <?php
                        if ($_SESSION['usuariogrupo_id'] == 2 || $_SESSION['usuariogrupo_id'] == 6) {
                            if ($value['tb_asistencia_jus'] == 1) {
                                echo '
                    Justificado
                    (<a href="javascript:void(0)" onClick="eliminar_justificacion(' . $value['tb_asistencia_id'] . ', 1)">Eliminar</a>)';
                            } else
                                echo '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="asistencia_obs_form(' . $value['tb_asistencia_id'] . ',1)">SI</a>';
                        } else
                            echo '<b>Consultar</b>';
                        ?>
                    </td>
                    <td id="tabla_fila" align="center">
                        <?php
                        if ($value['tb_asistencia_fal'] == 1) {
                            echo '
                  Faltó
                  (<a href="javascript:void(0)" onClick="eliminar_justificacion(' . $value['tb_asistencia_id'] . ', 2)">Eliminar</a>)';
                        } else
                            echo '<a class="btn btn-info btn-xs" href="javascript:void(0)" onClick="asistencia_obs_form(' . $value['tb_asistencia_id'] . ',3)">Falta</a>';
                        ?>
                    </td>
                    <td id="tabla_fila" align="center">
                        <a class="btn btn-primary btn-xs" href="javascript:void(0)" onClick="asistencia_form('editar',<?php echo $value['tb_asistencia_id']; ?>)" title="Editar"><i class="fa fa-edit"></i></a>
                        <!-- <a class="btn btn-success btn-xs" href="javascript:void(0)"href="javascript:void(0)" onClick="asistencia_obs_form(<?php //echo $value['tb_asistencia_id']; ?>, 2)">Det.</a> -->
                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" onClick="papeleta_form(<?php echo $value['tb_asistencia_id'] . ',' . $value['tb_usuario_id']; ?>)">Pap.</a>
                        <a class="btn btn-github btn-xs" title="Historial" onclick="asistencia_historial_form(<?php echo $value['tb_asistencia_id']; ?>)">Hist.</a>
                    </td>
                </tr> <?php
            }

            $asistenciaArray = NULL;
            ?>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($bandera == 0): ?>
    <table id="tbl_asistencias" class="table table-bordered table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">DIA</th>
                <th id="tabla_cabecera_fila">INGRESO</th>
                <th id="tabla_cabecera_fila">SALIDA</th>
                <th id="tabla_cabecera_fila">REFRIGERIO</th>
                <th id="tabla_cabecera_fila">INGRESO</th>
                <th id="tabla_cabecera_fila">SALIDA</th>
                <th id="tabla_cabecera_fila">DE MÁS</th>
                <th id="tabla_cabecera_fila">EXTRAS</th>
                <th id="tabla_cabecera_fila">OBSERVACIÓN</th>
                <th id="tabla_cabecera_fila">JUSTIFICAR</th>
                <th id="tabla_cabecera_fila">FALTAS</th>
                <th id="tabla_cabecera_fila"></th>
            </tr>
        </thead>
        <tbody>
    <?php echo $mensaje; ?>
        </tbody>
    </table>
<?php endif; ?>