$(document).ready(function () {
    validar_tipo_vista();
    
      $("#form_asis_obs").validate({
    submitHandler: function() {
      $.ajax({
        type: "POST",
        url: VISTA_URL+"asistencia/asistencia_reg.php",
        async:true,
        dataType: "json",
        data: $("#form_asis_obs").serialize(),
        beforeSend: function() {
//          $('#msj_asis').html("Guardando...");
//          $('#msj_asis').show(100);
//          $('#div_asis_obs_form').dialog('close');
        },
        success: function(data){            
            swal_success("SISTEMA",data.msj,2000);
            asistencia_tabla();
            $('#modal_registro_asistencia_obs').modal('hide');
        },
        complete: function(data){
          if(data.status != 200){
            $('#msj_asis').html("Error al guardar la observación: "+data);
            console.log(data);
          }
        }
      });
    },
    rules: {
      txt_asis_obs: {
        required: true
      }
    },
    messages: {
      txt_asis_obs: {
        required: 'Detalle'
      }
    }
  });
    
    
});

function validar_tipo_vista(){
  var jus = $('#hdd_asis_jus').val(); //si es 2 solo será para ver el historial
  if(parseInt(jus) == 2){
    $('#btn_guar_obs').hide();
    $('#tbl_obs').hide();
  }
  else
    $('#btn_guar_obs').show();
}