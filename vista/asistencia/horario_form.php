<?php
require_once '../../core/usuario_sesion.php';
require_once ("../asistencia/Asistencia.class.php");
$oAsistencia = new Asistencia();
require_once ("../funciones/fechas.php");
require_once ("../funciones/funciones.php");

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_horario_form" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title">Detalle de Asistencia</h4>
      </div>
      <form id="form_horario" method="post">
        <input type="hidden" name="action" value="insertar">

        <div class="modal-body">
          <div class="box box-primary">
            <div class="box-header form-inline">

              <div class="form-group">
                <select name="cmb_horario_empresa_id" id="cmb_horario_empresa_id" class="form-control input-sm mayus">
                  <?php require_once '../empresa/empresa_select.php';?>
                </select>
              </div>
              <div class="form-group">
                <select name="cmb_horario_usuario_id" id="cmb_horario_usuario_id" class="form-control input-sm mayus"></select>
              </div>
              <div class="form-group">
                <select name="cmb_horario_mes" id="cmb_horario_mes" class="form-control input-sm mayus">
                  <?php 
                    $valor_mes = date('m');
                    echo devuelve_option_nombre_meses($valor_mes);
                  ?>
                </select>
              </div>
                
              <div class="form-group">
                <select name="cmb_horario_anio" id="cmb_horario_anio" class="form-control input-sm mayus">
                  <?php 
                    $valor_anio = date('Y'); $formato = 4;
                    echo devuelve_option_anios($valor_anio, $formato);
                  ?>
                </select>
              </div>
            </div>
            
            <div class="box-body table-responsive no-padding horario_tabla" style="overflow-x: scroll;">
              
            </div>

          </div>
        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_asistencia">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/asistencia/horario_form.js?ver=2';?>"></script>