<?php
require_once ('../../core/usuario_sesion.php');
require_once ("../asistencia/Asistencia.class.php");
$oAsistencia = new Asistencia();

$asis_id = $_POST['asis_id'];
$usu_id = $_POST['usu_id'];

$dts = $oAsistencia->papeleta_horas_extras($asis_id, $usu_id);

if ($dts['estado'] == 1) {
    $usu_nom = $dts['data']['tb_usuario_nom'] . ' ' . $dts['data']['tb_usuario_ape'];
    $cod = $dts['data']['tb_asistencia_cod'];
    $cargo = $dts['data']['tb_usuarioperfil_nom'];
    $tip_pap = $dts['data']['tb_asistencia_pap']; //tipo de papeleta: 1 horas extra, 2 vacaiones, 3 medica, 4 fallecimiento, 5 labores
    $ext_ini = $dts['data']['tb_asistencia_extini']; //hora inicio
    $ext_fin = $dts['data']['tb_asistencia_extfin']; //hora fin
    $mot = $dts['data']['tb_asistencia_mot']; //motivo de la papeleta
    $det = $dts['data']['tb_asistencia_det']; //detalle de la papeleta
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_papeleta" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Papeletas de Horas Extras</h4>
            </div>
            <form id="form_papeleta" method="post">
                <input type="hidden" name="action" value="papeleta">
                <input type="hidden" name="hdd_asis_id" id="hdd_asis_id" value="<?php echo $asis_id; ?>">
                <input type="hidden" name="hdd_usu_id" id="hdd_usu_id" value="<?php echo $usu_id; ?>">

                <div class="modal-body">

                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Tipo de Papeleta:</label>
                                </div>
                                <div class="col-md-7">
                                    <select name="cmb_asis_pap" id="cmb_asis_pap" class="form-control input-sm" required>
                                        <option value="">--</option>
                                        <option value="1" <?php if ($tip_pap == 1) echo 'selected'; ?>>Horas Extras</option>
                                        <option value="2" <?php if ($tip_pap == 2) echo 'selected'; ?>>Vacaciones</option>
                                        <option value="3" <?php if ($tip_pap == 3) echo 'selected'; ?>>Licencia Médica</option>
                                        <option value="4" <?php if ($tip_pap == 4) echo 'selected'; ?>>Fallecimiento Familiar</option>
                                        <option value="5" <?php if ($tip_pap == 5) echo 'selected'; ?>>Labores de la Empresa</option>
                                    </select>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Nombre Colaborador:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" name="txt_usu_nom" value="<?php echo $usu_nom; ?>" class="form-control input-sm" readonly>
                                </div>

                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Cargo:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" value="<?php echo $cargo; ?>" class="form-control input-sm" readonly>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Desde:</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="time" name="txt_ext_ini" id="txt_ext_ini" value="<?php echo $ext_ini; ?>" class="form-control input-sm" required>
                                </div>
                                <div class="col-md-1">
                                    <label>Hasta:</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="time" name="txt_ext_fin" id="txt_ext_fin" value="<?php echo $ext_fin; ?>" class="form-control input-sm" required>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Autorizado por:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" value="<?php echo $_SESSION['usuario_nom'];?>" class="form-control input-sm" readonly>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Motivo:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" name="txt_ext_mot" id="txt_ext_mot" value="<?php echo $mot;?>" class="form-control input-sm" required>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Detalle:</label>
                                </div>
                                <div class="col-md-7">
                                    <textarea type="text" name="txt_ext_det" id="txt_ext_det" rows="5" value="<?php echo $mot;?>" class="form-control input-sm" required><?php echo $det;?></textarea>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="papeleta_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_papeleta">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/asistencia/papeleta_form.js?ver=20230401'; ?>"></script>