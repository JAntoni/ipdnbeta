function asistencia_horario_tabla() {
  var empresa_id = $('#cmb_horario_empresa_id').val();
  var usuario_id = $('#cmb_horario_usuario_id').val();
  var mes = $('#cmb_horario_mes').val();
  var anio = $('#cmb_horario_anio').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "asistencia/asistencia_horario_tabla.php",
    async: false,
    dataType: "html",
    data: ({
      empresa_id: empresa_id,
      usuario_id: usuario_id,
      mes: mes,
      anio: anio
    }),
    beforeSend: function () {
      
    },
    success: function (html) {
      console.log(html);
      $('.horario_tabla').html(html);

      $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck('destroy');

      $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
    },
    complete: function (html) {
      console.log(html);
    }
  });
}

function cmb_usu_id(empresa_id) {
  var mostrar = 0;
  $.ajax({
    type: "POST",
    url: VISTA_URL + "usuario/cmb_usu_id_select.php",
    async: false,
    dataType: "html",
    data: ({
      mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
      empresa_id: empresa_id
    }),
    beforeSend: function () {
      $('#cmb_horario_usuario_id').html('<option value="">Cargando...</option>');
    },
    success: function (html) {
      $('#cmb_horario_usuario_id').html(html);
    },
    complete: function (html) {

    }
  });
}

$(document).ready(function () {
  console.log('cammm 99')

  $("#cmb_horario_empresa_id").change(function () {
    var empresa_id = $("#cmb_horario_empresa_id").val();
    cmb_usu_id(empresa_id);
    asistencia_horario_tabla();
  });

  $("#cmb_horario_usuario_id, #cmb_horario_mes, #cmb_horario_anio").change(function () {
    asistencia_horario_tabla();
  });

  $("#form_horario").validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url:  VISTA_URL+"asistencia/asistencia_horario_controller.php",
        async: true,
        dataType: "json",
        data: $("#form_horario").serialize(),
        beforeSend: function () {
          notificacion_info('Guardando horario...', 3000);
        },
        success: function (data) {
          notificacion_success('Horario Guardado', 3000);
          $('#modal_horario_form').modal('hide');
        },
        complete: function (data) {
          console.log(data);
          if (data.status != 200) {
            alerta_error("ERROR AL GUARDAR",data.responseText);
            console.log(data)
          }
        }
      });
    },
    rules: {
      cmb_horario_usuario_id: {
        required: true,
        min: 1
      },
      cmb_horario_empresa_id: {
        required: true,
        min: 1
      },
      cmb_horario_mes: {
        required: true
      },
      cmb_horario_anio: {
        required: true
      }
    },
    messages: {
      cmb_horario_usuario_id: {
        required: "Selecciona un colaborador",
        min: "Selecciona un colaborador"
      },
      cmb_horario_empresa_id: {
        required: "Selecciona la sede del colaborador",
        min: "Selecciona la sede del colaborador"
      },
      cmb_horario_mes: {
        required: "Selecciona un mes para el horario"
      },
      cmb_horario_anio: {
        required: "Selecciona un año para el horario"
      }
    }
  });
});