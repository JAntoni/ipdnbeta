<?php

require_once '../../core/usuario_sesion.php';
require_once('../asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../historial/Historial.class.php');
$oHist = new Historial();

$action = $_POST['action'];

if ($action == 'ingresar') {
    $cod = date('d-m-y');
    $hor_ing = date('H:i:s');

    $oAsistencia->tb_asistencia_cod=$cod;
    $oAsistencia->tb_usuario_id=$_SESSION['usuario_id'];
    $oAsistencia->tb_asistencia_ing1=$hor_ing;
    $oAsistencia->insertar();

    $data['estado'] = 1;
    $data['msj'] = 'Su asistencia se ha registrado correctamente';

    echo json_encode($data);
}
if ($action == 'actualizar') {
    $tip_asis = $_POST['tipo']; //es el tipo ingreso o salida, es la ultima palbra de la columna en la tabla, ig1, sal1 o sal2
    $hor_asis = date('H:i:s');
    $cod = date('d-m-y');

    $oAsistencia->marcar_asistencia($cod, $tip_asis, $hor_asis, $_SESSION['usuario_id']);

    $data['estado'] = 1;
    $data['msj'] = 'Su asistencia se ha registrado correctamente';
    echo json_encode($data);
}
if ($action == 'obs_jus') {
    $asis_id = $_POST['hdd_asis_id'];
    $jus = $_POST['hdd_asis_jus']; //si es 0 no está justificado, si es 1 justificado, 3 marcar día faltado
    $obs = $_POST['txt_asis_obs'] . ' | ' . date('d-m-Y h:i a') . '<br>';

    if ($jus == 3) {
        $historial = '<span><b>' . $_SESSION['usuario_nom'] . '</b> ha marcado el día faltado para el colaborador. | ' . date('d-m-Y h:i a') . '</span><br>';
        $oAsistencia->observar_dia_faltado($obs, 1, $asis_id, $historial);
    } else {
        $historial = '<span>Justificado u observado por: <b>' . $_SESSION['usuario_nom'] . '</b> | ' . date('d-m-Y h:i a') . '</span><br>';
        $oAsistencia->observar_justificar($obs, $jus, $asis_id, $historial);
    }

    if ($jus == 1)
        $data['msj'] = 'Observación guardada';
    elseif ($jus == 3)
        $data['msj'] = 'Día faltado guardado';
    else
        $data['msj'] = 'Observación y justificación guardadas';

    echo json_encode($data);
}
if ($action == 'eliminar_jus') {
    $asis_id = $_POST['asis_id'];
    $jus = 0;
    $obs = '';

    $eli_tip = $_POST['eli_tip']; // 1 hacer cambios en justificacion, 2 cambios en faltas
    if ($eli_tip == 1) {
        $historial = '<span style="color: red;">Justificación eliminada por: <b>' . $_SESSION['usuario_nom'] . '</b> | ' . date('d-m-Y h:i a') . '</span><br>';
        $oAsistencia->observar_justificar($obs, $jus, $asis_id, $historial);
        $data['msj'] = 'Justificación eliminada';
    } else {
        $historial = '<span style="color: red;">Día faltado rectificado por: <b>' . $_SESSION['usuario_nom'] . '</b> | ' . date('d-m-Y h:i a') . '</span><br>';
        $oAsistencia->observar_dia_faltado($obs, $jus, $asis_id, $historial);
        $data['msj'] = 'Día faltado corregido';
    }

    echo json_encode($data);
}
if ($action == 'editar') {
    $asis_id = $_POST['hdd_asis_id'];

    $busqueda = $oAsistencia->mostrarUno($asis_id)["data"]; // es necesario buscar el registro pq su id va a la tabla de historial

    $historial = '<span style="color: green;">Asistencia modificada por: <b>' . $_SESSION['usuario_nom'] . '</b> | ' . date('d-m-Y h:i a') . '</span><br>';
    $ing1 = $_POST['txt_asis_ing1'];
    $sal1 = $_POST['txt_asis_sal1'];
    $ing2 = $_POST['txt_asis_ing2'];
    $sal2 = $_POST['txt_asis_sal2'];

    $evaluar[0] = $ing1 == $busqueda["tb_asistencia_ing1"] ? true : false;
    $evaluar[1] = $sal1 == $busqueda["tb_asistencia_sal1"] ? true : false;
    $evaluar[2] = $ing2 == $busqueda["tb_asistencia_ing2"] ? true : false;
    $evaluar[3] = $sal2 == $busqueda["tb_asistencia_sal2"] ? true : false;

    $son_iguales = true;
    $i = 0;
    while ($i < count($evaluar)) {
        if (!$evaluar[$i]) {
            $son_iguales = false;
        }
        $i++;
    }

    if ($son_iguales) {
        $data['msj'] = "No se realizó ninguna modificacion";
    }
    else{
        //echo 'ingre :'.$ing1.' // '.$sal1; exit();
        $oAsistencia->tb_asistencia_ing1 = $ing1;
        $oAsistencia->tb_asistencia_sal1 = $sal1;
        $oAsistencia->tb_asistencia_ing2 = $ing2;
        $oAsistencia->tb_asistencia_sal2 = $sal2;
        $oAsistencia->tb_asistencia_his = $historial;
        $oAsistencia->tb_asistencia_id = $asis_id;

        $res = $oAsistencia->editar();
        
        if($res == 1){
            $data['msj'] = 'Asistencia editada';

            $oHist->setTbHistUsureg($_SESSION['usuario_id']);
            $oHist->setTbHistNomTabla('tb_asistencia');
            $oHist->setTbHistRegmodid($asis_id);
            $mensaje = 'Modificó el horario asistencia del dia '. $busqueda["tb_asistencia_cod"]. ' al usuario ' . $busqueda["usuario"] . ':';
            if (!$evaluar[0])
				$mensaje .= '<br> - Cambió hora de ingreso 1: ' . $busqueda['tb_asistencia_ing1'] . ' => <b>' . $ing1 . '</b>';
            if (!$evaluar[1])
				$mensaje .= '<br> - Cambió hora de salida 1: ' . $busqueda['tb_asistencia_sal1'] . ' => <b>' . $sal1 . '</b>';
            if (!$evaluar[2])
				$mensaje .= '<br> - Cambió hora de ingreso 2: ' . $busqueda['tb_asistencia_ing2'] . ' => <b>' . $ing2 . '</b>';
            if (!$evaluar[3])
				$mensaje .= '<br> - Cambió hora de salida 2: ' . $busqueda['tb_asistencia_sal2'] . ' => <b>' . $sal2 . '</b>';
            
            $oHist->setTbHistDet($mensaje);
            $oHist->insertar();
        }
        //$oAsistencia->editar($ing1, $sal1, $ing2, $sal2, $historial, $asis_id);
    }
    echo json_encode($data);
}

if ($action == 'papeleta') {
    $asis_id = $_POST['hdd_asis_id'];
    $usu_id = $_POST['hdd_usu_id'];
    $tip_pap = $_POST['cmb_asis_pap'];
    $ext_ini = $_POST['txt_ext_ini'];
    $ext_fin = $_POST['txt_ext_fin'];
    $mot = $_POST['txt_ext_mot'];
    $det = $_POST['txt_ext_det'];
    $his = '<span style="color: blue;">Papeleta generada por ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</span><br>';

    if ($tip_pap == 1) {
        if ($ext_fin > $ext_ini) {
            $extra = horas_demas_base_numero_horas($ext_ini, $ext_fin, 0); //devuelve minutos, si es 0 devuelve un nuero mayor a 0
            $oAsistencia->registrar_papeleta($ext_ini, $ext_fin, $mot, $det, $extra, $his, $asis_id, $usu_id, $tip_pap);

//            header('location: www.ipdnsac.com/app/modulos/asistencia/doc_papeleta_extras.php?asis_id=' . $asis_id . '&usu_id=' . $usu_id);
//            exit();
            
            $data['estado'] = 1;
            $data['msj'] = 'Su Papeleta se ha registrado correctamente';
            echo json_encode($data);
        } else
            echo 'Error: la fecha fin no puede ser menor a la inicial';
    } else {
        $oAsistencia->registrar_papeleta($ext_ini, $ext_fin, $mot, $det, 0, $his, $asis_id, $usu_id, $tip_pap);

//        header('location: www.ipdnsac.com/app/modulos/asistencia/doc_papeleta_extras.php?asis_id=' . $asis_id . '&usu_id=' . $usu_id);
//        exit();
        $data['estado'] = 1;
        $data['msj'] = 'Su Papeleta se ha registrado correctamente';
        echo json_encode($data);
    }
    
    
}
?>