<?php
require_once ("../asistencia/Horario.class.php");
$oHorario = new Horario();

require_once ("../funciones/fechas.php");

$empresa_id = intval($_POST['empresa_id']);
$usuario_id = intval($_POST['usuario_id']);
$mes = $_POST['mes'];
$anio = $_POST['anio'];

$mes_anterior = intval($mes) - 1;
$anio_anterior = $anio;
if($mes_anterior <= 0){
  $mes_anterior = 12;
  $anio_anterior = intval($anio) - 1;
}

$numDays = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);

$columnas = '<tr>';
  for ($i = 1; $i <= $numDays; $i++) {
    $date = date("Y-m-d", strtotime("$anio-$mes-$i"));
    $columnas .= '<th>' . $date . '</th>';
  }
$columnas .= '</tr>';

$filas = '';
$dia_semana = '';
$contador = 0;

$array_horario = array(
  '08:30'=>'08:30 - 09:00', 
  '09:00'=>'09:00 - 10:00', 
  '10:00'=>'10:00 - 11:00', 
  '11:00'=>'11:00 - 12:00', 
  '12:00'=>'12:00 - 13:00', 
  '13:00'=>'13:00 - 14:00', 
  '14:00'=>'14:00 - 15:00', 
  '15:00'=>'15:00 - 16:00', 
  '16:00'=>'16:00 - 17:00', 
  '17:00'=>'17:00 - 18:00', 
  '18:00'=>'18:00 - 19:00');

//* ANTES DE GENERAR EL LISTADO DE HORAS PARA SELECCIONAR EL HORARIO DEL COLABORADOR, VAMOS A CONSULTAR SI EXISTE UN CRONOGRAMA YA GUARDADO EN ESTE MES Y AÑO, EN CASO
//* NO TENGA, CONSULTAMOS EL HORARIO DEL MES ANTERIOR PARA PODER GUARDARLO

$data_horario = array();
$horario_guardado = '<h4>Horario nuevo para guardar</h4>';
$mes_horario = 1;
$result = $oHorario->listar_horario_usuario_mes_anio_array($usuario_id, $mes, $anio);
  if($result['estado'] == 1){
    $horario_guardado = '<div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
      Este horario ya ha sido guardado este mes</div>';
    foreach ($result['data'] as $key => $value) {
      $horas_array = explode(',', $value['array']);
      $data_horario[$value['tb_horariodetalle_fec']] = $horas_array;
    }
  }
  else{
    //? en caso no tenga registrado un horario para este mes actual, consultamos del mes anterior, como una memoria de horario
    $result2 = $oHorario->listar_horario_anterior_dia_semana($usuario_id, $mes_anterior, $anio_anterior);
      if($result2['estado'] == 1){
        $mes_horario = 2;
        $horario_guardado = '<div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
          Este horario fue guardado el mes anterior. Mes: '.$mes_anterior.' y año: '.$anio_anterior.'</div>';
        foreach ($result2['data'] as $key => $value) {
          $horas_array = explode(',', $value['array']);
          $data_horario[$value['dia_semana']] = $horas_array;
        }
      }
    $result2 = NULL;
  }
$result = NULL;

foreach ($array_horario as $key => $value){
  $filas .='<tr>';
  for ($e = 1; $e <= $numDays; $e++) {
    $date = date("Y-m-d", strtotime("$anio-$mes-$e"));
    $checked = 'checked';
    $nombre_dia = get_nombre_dia($date);

    if(empty($data_horario)){
      if($value == '13:00 - 14:00' || $value == '14:00 - 15:00')
        $checked = '';
      if($nombre_dia == 'Domingo')
        $checked = '';
      if($nombre_dia == 'Sábado' && ($value == '15:00 - 16:00' || $value == '16:00 - 17:00' || $value == '17:00 - 18:00' || $value == '18:00 - 19:00'))
        $checked = '';
    }
    else{
      if($mes_horario == 1 && !in_array($value, $data_horario[$date])){
        $checked = '';
      }
      if($mes_horario == 2 && !in_array($value, $data_horario[$nombre_dia])){ //si tenemos un horario guardado del mes anterior restamos al día actual un mes para buscar los días ya marcados
        $checked = '';
      }
    }

    $filas .= '
      <td>
        <label class="checkbox-inline" style="padding-left: 0px;">
          <input type="checkbox" name="che_horario_horini[]" id="che_horario_horini" value="'.$date.'|'.$key.'" class="flat-green" '.$checked.'> '.$value.'
        </label>
      </td>';
    
    if($contador == 0)
      $dia_semana .= '<td>Día '.$nombre_dia.'</td>';
  }
  $filas .='</tr>';
  $contador ++;
}

$filas = '<tr>'.$dia_semana.'</tr>'.$filas;
$data_horario = NULL;

?>
<style>
  
  .table td, .table th {
    min-width: 105px;
  }
</style>

<?php if($usuario_id > 0):
  echo $horario_guardado;
  ?>

<table class="table table-striped table-bordered display" cellspacing="0" style="max-width: 700px;"> 
  <thead>
    <?php echo $columnas;?>
  </thead>
  <tbody>
    <?php echo $filas;?>
  </tbody>
</table>
<?php endif;?>

<?php if($usuario_id <= 0):?>
<div class="alert alert-info alert-dismissible">
  <h4><i class="icon fa fa-info"></i> Importante</h4>
  Selecciona el colaborador seguido del mes y del año
</div>
<?php endif;?>