<?php
require_once ("../asistencia/Horario.class.php");
$oHorario = new Horario();

require_once ("../funciones/fechas.php");

$empresa_id = intval($_POST['empresa_id']);
$usuario_id = intval($_POST['usuario_id']);
$mes = $_POST['mes'];
$anio = $_POST['anio'];

$mes_anterior = intval($mes) - 1;
$anio_anterior = $anio;
if($mes_anterior <= 0){
  $mes_anterior = 12;
  $anio_anterior = intval($anio) - 1;
}

$numDays = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);

$columnas = '<tr>';
  for ($i = 1; $i <= $numDays; $i++) {
    $date = date("Y-m-d", strtotime("$anio-$mes-$i"));
    $columnas .= '<th>' . mostrar_fecha($date) . '</th>';
  }
$columnas .= '</tr>';

$filas = '';
$dia_semana = '';
$contador = 0;
$hora_ingreso_am = '09:00';
$hora_salida_am = '13:00';
$hora_ingreso_pm = '15:00';
$hora_salida_pm = '19:00';

$filas .='<tr>';
  for ($e = 1; $e <= $numDays; $e++) {
    $date = date("Y-m-d", strtotime("$anio-$mes-$e"));
    $nombre_dia = get_nombre_dia($date);
    
    $filas .= '
      <td align="center">
        <label class="checkbox-inline" style="padding-left: 0px;">
          de: <input type="time" name="'.$date.'_ing1" id="" value="'.$hora_ingreso_am.'" class="input-sm form-control"> 
          a: <input type="time" name="'.$date.'_sal1" id="" value="'.$hora_salida_am.'" class="input-sm form-control">
        </label>
      </td>';
    
    if($contador == 0)
      $dia_semana .= '<td>Día '.$nombre_dia.'</td>';
  }
$filas .='</tr>';

$filas .='<tr><td colspan="'.$numDays.'" style="text-align: center; font-weight: bold; font-size: 16pt;">BREAKING BAD</td></tr>';

$filas .='<tr>';
  for ($e = 1; $e <= $numDays; $e++) {
    $date = date("Y-m-d", strtotime("$anio-$mes-$e"));

    $filas .= '
      <td align="center">
        <label class="checkbox-inline" style="padding-left: 0px;">
          de: <input type="time" name="'.$date.'_ing2" id="" value="'.$hora_ingreso_pm.'" class="input-sm form-control">
          a: <input type="time" name="'.$date.'_sal2" id="" value="'.$hora_salida_pm.'" class="input-sm form-control">
        </label>
      </td>';
  }
$filas .='</tr>';


$filas = '<tr>'.$dia_semana.'</tr>'.$filas;
$data_horario = NULL;

?>
<style>
  
  .table td, .table th {
    min-width: 105px;
  }
</style>

<?php if($usuario_id > 0):
  echo $horario_guardado;
  ?>

<table class="table table-striped table-bordered display" cellspacing="0" style="max-width: 700px;"> 
  <thead>
    <?php echo $columnas;?>
  </thead>
  <tbody>
    <?php echo $filas;?>
  </tbody>
</table>
<?php endif;?>

<?php if($usuario_id <= 0):?>
<div class="alert alert-info alert-dismissible">
  <h4><i class="icon fa fa-info"></i> Importante</h4>
  Selecciona el colaborador seguido del mes y del año
</div>
<?php endif;?>