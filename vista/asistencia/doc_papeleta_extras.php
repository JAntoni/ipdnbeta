<?php
require_once '../../core/usuario_sesion.php';
require_once('../public/librerias/html2pdf/_tcpdf_5.9.206/tcpdf.php');

require_once ("../asistencia/asistencia.js");
$oAsistencia = new Asistencia();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$asis_id = $_GET['asis_id'];
$usu_id = $_GET['usu_id'];

$dts = $oAsistencia->papeleta_horas_extras($asis_id, $usu_id);
  if($dts['estado']==1){
    $usu_nom = $dts['data']['tb_usuario_nom'].' '.$dts['data']['tb_usuario_ape'];
    $cod = $dts['data']['tb_asistencia_cod'];
    $cargo = $dts['data']['tb_usuarioperfil_nom'];
    $tip_pap = $dts['data']['tb_asistencia_pap']; //tipo de papeleta: 1 horas extra, 2 vacaiones, 3 medica, 4 fallecimiento, 5 labores
    $ext_ini = $dts['data']['tb_asistencia_extini']; //hora inicio
    $ext_fin = $dts['data']['tb_asistencia_extfin']; //hora fin
    $mot = $dts['data']['tb_asistencia_mot']; //motivo de la papeleta
    $det = $dts['data']['tb_asistencia_det']; //detalle de la papeleta
  }

$papeleta = 'horas extras';
if($tip_pap == 2)
  $papeleta = 'vaciones';
if($tip_pap == 3)
  $papeleta = 'licencia médica';
if($tip_pap == 4)
  $papeleta = 'fallecimiento de familiar';
if($tip_pap == 5)
  $papeleta = 'labores de la empresa';

$title='Papeleta de '.$papeleta;

$codigo = $cod.'_'.$usu_id.'_'.$asis_id;
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('https://www.prestamosdelnorte.com/');
  $pdf->SetTitle($title);
  $pdf->SetSubject('https://www.prestamosdelnorte.com/');
  $pdf->SetKeywords('https://www.prestamosdelnorte.com/');

  // set header and footer fonts
  //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  $pdf->setPrintHeader(false);
  $pdf->setPrintFooter(false);

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  //set margins
  $pdf->SetMargins(10, 10, 10);// left top right
  //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

  //set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, 5);

  //set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  //set some language-dependent strings
  $pdf->setLanguageArray($l);

  // ---------------------------------------------------------

  // add a page
$pdf->AddPage('P', 'A4');
//Sistem variable

$html= '
    <table width="100%" cellpadding="2" style="border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
      <tr>
        <td style="width:40%;" align="bottom"><img src="../../libreriasphp/html2pdf/_tcpdf_5.9.206/images/logo.jpg"/></td>
        <td style="width:60%;"></td>
      </tr>
      <tr>
        <td align="center" style="font-size:40px;" colspan="2">
          <strong>AUTORIZACION DE '.mb_strtoupper($papeleta, 'UTF-8').'</strong>
        </td>
      </tr>
    </table>

    <table border="1" cellpadding="4">
      <tr>
        <td style="width:20%;"><b>NOMBRE DEL TRABAJADOR</b></td>
        <td style="width:50%;">'.$usu_nom.'</td>
        <td style="width:10%;"><b>FECHA</b></td>
        <td style="width:20%;">'.$cod.'</td>
      </tr>
    </table>
    <br/>  
    <table style="width:100%;" border="1" cellpadding="4">';
    if($tip_pap == 1){
      $html .='
        <tr>
          <td width="20%"><b>CARGO</b></td>
          <td width="30%">'.$cargo.'</td>
          <td width="20%"><b>HORAS EXTRAS</b></td>
          <td width="30%">DE:'.substr($ext_ini, 0, 5).' HASTA: '.substr($ext_fin, 0, 5).'</td>
        </tr>';
    }
    else{
      $html .='
        <tr>
          <td width="20%"><b>CARGO</b></td>
          <td width="80%" colspan="3">'.$cargo.'</td>
        </tr>';
    }
    $html .='  
      <tr>
        <td colspan="4" align="center"><strong>INFORMACION DETALLADA SOBRE LAS RAZONES DE AUTORIZACIÓN DE '.mb_strtoupper($papeleta, 'UTF-8').'</strong></td>
      </tr>
      <tr>
        <td width="20%"><b>APROBADO POR:</b></td>
        <td width="30%">'.$_SESSION['usuario_nombre'].'</td>
        <td width="20%"><b>MOTIVO</b></td>
        <td width="30%">'.$mot.'</td>
      </tr>
      <tr>
        <td colspan="4">'.$det.'</td>
      </tr>
      <tr>
        <td width="20%"><b>FIRMA DEL TRABAJADOR</b></td>
        <td width="30%"></td>
        <td width="20%"><b>FIRMA DEL EMPLEADOR</b></td>
        <td width="30%"></td>
      </tr>
    </table>
';


// set core font
$pdf->SetFont('arial', '', 10);

// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);

//$pdf->Ln();

$style = array(
  'position' => 'L',
  'align' => 'L',
  'stretch' => false,
  'fitwidth' => true,
  'cellfitalign' => '',
  'border' => false,
  'padding' => 0,
  'fgcolor' => array(0,0,0),
  'bgcolor' => false,
  'text' => false
//     'font' => 'helvetica',
//     'fontsize' => 8,
//     'stretchtext' => 4
);

$pdf->SetY(-26);
// Page number
$pdf->SetFont('helvetica', '', 9);
//$this->SetTextColor(0,0,0);
$pdf->Cell(0, 0, 'Página '.$pdf->getAliasNumPage().' de '.$pdf->getAliasNbPages(), 'T', 1, 'R', 0, '', 0, false, 'T', 'M');


$pdf->write1DBarcode($codigo, 'C128', '', 271, '', 6, 0.3, $style, 'N');
$pdf->Cell(0, 0, 'www.prestamosdelnortechiclayo.com', 0, 1, 'C', 0, '', 0, false, 'T', 'M'); 

// set UTF-8 Unicode font
//$pdf->SetFont('dejavusans', '', 10);

// output the HTML content
//$pdf->writeHTML($html, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$nombre_archivo=$codigo."_".$title.".pdf";
$pdf->Output($nombre_archivo, 'I');
//============================================================+
// END OF FILE                                                
//============================================================+
?>