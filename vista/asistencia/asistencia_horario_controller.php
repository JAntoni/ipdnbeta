<?php
require_once '../../core/usuario_sesion.php';
require_once('../asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once ("../asistencia/Horario.class.php");
$oHorario = new Horario();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$action = $_POST['action'];

if ($action == 'insertar') {
    $usuario_registra = intval($_SESSION['usuario_id']);
    $mes = $_POST['cmb_horario_mes'];
    $anio = $_POST['cmb_horario_anio'];
    $usuario_id = intval($_POST['cmb_horario_usuario_id']);
    $empresa_id = intval($_POST['cmb_horario_empresa_id']);

    $numDays = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
    
    $msj = '';

    for($e = 1; $e <= $numDays; $e++){
      $asistencia_fec = date("Y-m-d", strtotime("$anio-$mes-$e"));
      $msj .= 'para el dia: '.$asistencia_fec.' ingreso 1: '.$_POST[$asistencia_fec.'_ing1'].' | salida 1: '.$_POST[$asistencia_fec.'_sal1'].' | ingreso 2: '.$_POST[$asistencia_fec.'_ing2'].' | salida 2: '.$_POST[$asistencia_fec.'_sal2'];

      $oAsistencia->tb_usuario_id = $usuario_id;
      $oAsistencia->tb_asistencia_fec = $asistencia_fec;
      $oAsistencia->tb_asistencia_horing1 = $_POST[$asistencia_fec.'_ing1']; 
      $oAsistencia->tb_asistencia_horsal1 = $_POST[$asistencia_fec.'_sal1'];
      $oAsistencia->tb_asistencia_horing2 = $_POST[$asistencia_fec.'_ing2']; 
      $oAsistencia->tb_asistencia_horsal2 = $_POST[$asistencia_fec.'_sal2'];

      $oAsistencia->asignar_horario_usuario();
    }

    echo ' HORARIO GUARDADO PARA EL MES: '.$mes.' || anio: '.$anio.' || usuario: '.$usuario_id;
}
?>