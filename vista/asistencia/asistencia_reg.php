<?php
require_once('../../core/usuario_sesion.php');
require_once('../asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../historial/Historial.class.php');
$oHist = new Historial();
require_once('../comentario/Comentario.class.php');
$oComentario = new Comentario();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

$action = $_POST['action'];

if($action == 'ingresar'){
  $cod = date('d-m-y');
  $hor_ing = date('H:i:s');

  
  $oAsistencia->tb_asistencia_cod= $cod;
  $oAsistencia->tb_usuario_id=$_SESSION['usuario_id'];
  $oAsistencia->tb_asistencia_ing1=$hor_ing;
  $oAsistencia->insertar();
  
  $data['estado'] = 1;
  $data['msj'] = 'Su asistencia se ha registrado correctamente';

  echo json_encode($data);
}
if($action == 'actualizar'){
  $tip_asis = $_POST['tipo']; //es el tipo ingreso o salida, es la ultima palbra de la columna en la tabla, ig1, sal1 o sal2
  $asistencia_obs = $_POST['asistencia_obs'];
  $hor_asis = date('H:i:s');
  $cod = date('d-m-y');
  $horario_registrado = '';

  $busqueda = $oAsistencia->listar_asistencia_codigo($cod , $_SESSION['usuario_id'])["data"]; // es necesario buscar el registro pq su id va a la tabla de historial
  $result = $oAsistencia->marcar_asistencia($cod, $tip_asis, $hor_asis, $_SESSION['usuario_id']);
  
  if($result == 1){
    //GUARDAMOS LAS OBSERVACIONES EN CASO HAYA INGRESADO EL COLABORADOR
    if(trim($asistencia_obs) != ''){
      $asistencia_obs = '<span><b>'.$_SESSION['usuario_nom'].'</b> detalla: '.$asistencia_obs.'. | '.date('d-m-Y h:i a').'</span><br>';

      $oAsistencia->observar_asistencia_codigo($cod, $_SESSION['usuario_id'], $asistencia_obs, $asistencia_obs);
    }

    $data['estado'] = 1;
    $data['msj'] = 'Su asistencia se ha registrado correctamente';

    if ($tip_asis == 'ing1'){
      $horario_registrado = 'ingreso 1';
    }
    else if($tip_asis == 'sal1'){
      $horario_registrado = 'salida 1';
    }
    else if($tip_asis == 'ing2'){
      $horario_registrado = 'ingreso 2';
    }
    else{
      $horario_registrado = 'salida 2';
    }
    $detalle_cambios = 'Registró su hora de '. $horario_registrado .' para el día '. $cod .' => <b>' . $hor_asis . '</b>';

    $oHist->setTbHistUsureg($_SESSION['usuario_id']);
    $oHist->setTbHistNomTabla('tb_asistencia');
    $oHist->setTbHistRegmodid($busqueda["tb_asistencia_id"]);
    $oHist->setTbHistDet($detalle_cambios);
    $oHist->insertar();
  }
  else{
    $data['estado'] = 0;
    $data['msj'] = 'No se pudo registrar su asistencia';
  }

  echo json_encode($data);
}
if($action == 'obs_jus'){
  $asis_id = $_POST['hdd_asis_id'];
  $jus = $_POST['hdd_asis_jus']; //si es 0 no está justificado, si es 1 justificado, 3 marcar día faltado
  $obs = $_POST['txt_asis_obs'].' | '.date('d-m-Y h:i a').'<br>';
  $mensaje = '';

  if($jus == 3){
    $historial = '<span><b>'.$_SESSION['usuario_nom'].'</b> ha marcado el día faltado para el colaborador. | '.date('d-m-Y h:i a').'</span><br>';
    $mensaje = 'Ha marcado la inasistencia del colaborador';
    $oAsistencia->observar_dia_faltado($obs, 1, $asis_id, $historial);
  }
  else{
    if($jus == 1){
      $mensaje = 'Ha justificado la inasistencia del colaborador';
      $historial = '<span>Justificado por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';
    }
    else{
      $mensaje = 'Ha hecho observacion en la asistencia del colaborador';
      $historial = '<span>Observado por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';
    }
    
    $oAsistencia->observar_justificar($obs, $jus, $asis_id, $historial);
  }

  $oHist->setTbHistUsureg($_SESSION['usuario_id']);
  $oHist->setTbHistNomTabla('tb_asistencia');
  $oHist->setTbHistRegmodid($asis_id);
  $oHist->setTbHistDet($mensaje);

  $histo_id = $oHist->insertar()['historial_id'];
  
  $oComentario->setTbComentUsureg($_SESSION['usuario_id']);
  $oComentario->setTbComentDet($_POST['txt_asis_obs']);
  $oComentario->setTbComentNomTabla('tb_hist');
  $oComentario->setTbComentRegmodid($histo_id);
  $oComentario->insertar();

  if($jus == 1)
    $data['msj'] = 'Observación guardada';
  elseif($jus == 3)
    $data['msj'] = 'Día faltado guardado';
  else
    $data['msj'] = 'Observación y justificación guardadas';

  echo json_encode($data); 
}
if($action == 'eliminar_jus'){
  $asis_id = $_POST['asis_id'];
  $jus = 0;
  $obs = '';
  $mensaje = '';

  $eli_tip = $_POST['eli_tip']; // 1 hacer cambios en justificacion, 2 cambios en faltas
  if($eli_tip == 1){
    $mensaje = 'Ha eliminado la justificacion de inasistencia';
    $historial = '<span style="color: red;">Justificación eliminada por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';
    $oAsistencia->observar_justificar($obs, $jus, $asis_id, $historial);
    $data['msj'] = 'Justificación eliminada';
  }
  else{
    $mensaje = 'Anuló la inasistencia del colaborador';
    $historial = '<span style="color: red;">Día faltado rectificado por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';
    $oAsistencia->observar_dia_faltado($obs, $jus, $asis_id, $historial);

    $data['msj'] = 'Día faltado corregido';
  }

  $oHist->setTbHistUsureg($_SESSION['usuario_id']);
  $oHist->setTbHistNomTabla('tb_asistencia');
  $oHist->setTbHistRegmodid($asis_id);
  $oHist->setTbHistDet($mensaje);

  $oHist->insertar();

  echo json_encode($data);
}
/* if($action == 'editar'){
  $asis_id = $_POST['hdd_asis_id'];
  $historial = '<span style="color: green;">Asistencia modificada por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a').'</span><br>';
  $ing1 = $_POST['txt_asis_ing1'];
  $sal1 = $_POST['txt_asis_sal1'];
  $ing2 = $_POST['txt_asis_ing2'];
  $sal2 = $_POST['txt_asis_sal2'];

  //echo 'ingre :'.$ing1.' // '.$sal1; exit();
  
  $oAsistencia->tb_asistencia_ing1=$ing1;
  $oAsistencia->tb_asistencia_sal1=$sal1;
  $oAsistencia->tb_asistencia_ing2=$ing2; 
  $oAsistencia->tb_asistencia_sal2=$sal2;
  $oAsistencia->tb_asistencia_his=$historial;
  $oAsistencia->tb_asistencia_id=$asis_id;

  $oAsistencia->editar();

  $data['msj'] = 'Asistencia editada';
  echo json_encode($data);
} */

if ($action == 'editar') {
  $asis_id = $_POST['hdd_asis_id'];

  $busqueda = $oAsistencia->mostrarUno($asis_id)["data"]; // es necesario buscar el registro pq su id va a la tabla de historial

  $historial = '<span style="color: green;">Asistencia modificada por: <b>' . $_SESSION['usuario_nom'] . '</b> | ' . date('d-m-Y h:i a') . '</span><br>';
  $ing1 = $_POST['txt_asis_ing1'];
  $sal1 = $_POST['txt_asis_sal1'];
  $ing2 = $_POST['txt_asis_ing2'];
  $sal2 = $_POST['txt_asis_sal2'];

  $evaluar[0] = $ing1 == $busqueda["tb_asistencia_ing1"] ? true : false;
  $evaluar[1] = $sal1 == $busqueda["tb_asistencia_sal1"] ? true : false;
  $evaluar[2] = $ing2 == $busqueda["tb_asistencia_ing2"] ? true : false;
  $evaluar[3] = $sal2 == $busqueda["tb_asistencia_sal2"] ? true : false;

  $son_iguales = true;
  $i = 0;
  while ($i < count($evaluar)) {
      if (!$evaluar[$i]) {
          $son_iguales = false;
      }
      $i++;
  }

  if ($son_iguales) {
      $data['msj'] = "No se realizó ninguna modificacion";
  }
  else{
      //echo 'ingre :'.$ing1.' // '.$sal1; exit();
      $oAsistencia->tb_asistencia_ing1 = $ing1;
      $oAsistencia->tb_asistencia_sal1 = $sal1;
      $oAsistencia->tb_asistencia_ing2 = $ing2;
      $oAsistencia->tb_asistencia_sal2 = $sal2;
      $oAsistencia->tb_asistencia_his = $historial;
      $oAsistencia->tb_asistencia_id = $asis_id;

      $res = $oAsistencia->editar();
      
      if($res == 1){
          $data['msj'] = 'Asistencia editada';

          $oHist->setTbHistUsureg($_SESSION['usuario_id']);
          $oHist->setTbHistNomTabla('tb_asistencia');
          $oHist->setTbHistRegmodid($asis_id);
          $mensaje = 'Modificó el horario asistencia del dia '. $busqueda["tb_asistencia_cod"]. ' al usuario ' . $busqueda["usuario"] . ':';
          if (!$evaluar[0])
      $mensaje .= '<br> - Cambió hora de ingreso 1: ' . $busqueda['tb_asistencia_ing1'] . ' => <b>' . $ing1 . '</b>';
          if (!$evaluar[1])
      $mensaje .= '<br> - Cambió hora de salida 1: ' . $busqueda['tb_asistencia_sal1'] . ' => <b>' . $sal1 . '</b>';
          if (!$evaluar[2])
      $mensaje .= '<br> - Cambió hora de ingreso 2: ' . $busqueda['tb_asistencia_ing2'] . ' => <b>' . $ing2 . '</b>';
          if (!$evaluar[3])
      $mensaje .= '<br> - Cambió hora de salida 2: ' . $busqueda['tb_asistencia_sal2'] . ' => <b>' . $sal2 . '</b>';
          
          $oHist->setTbHistDet($mensaje);
          $oHist->insertar();
      }
      //$oAsistencia->editar($ing1, $sal1, $ing2, $sal2, $historial, $asis_id);
  }
  echo json_encode($data);
}

/* if($action == 'papeleta'){
  $asis_id = $_POST['hdd_asis_id'];
  $usu_id = $_POST['hdd_usu_id'];
  $tip_pap = $_POST['cmb_asis_pap'];
  $ext_ini = $_POST['txt_ext_ini'];
  $ext_fin = $_POST['txt_ext_fin'];
  $mot = $_POST['txt_ext_mot'];
  $det = $_POST['txt_ext_det'];
  $his = '<span style="color: blue;">Papeleta generada por '.$_SESSION['usuario_nom'].' | '.date('d-m-Y h:i a').'</span><br>';
  
  if($tip_pap == 1){
    if($ext_fin > $ext_ini){
      $extra = horas_demas_base_numero_horas($ext_ini, $ext_fin, 0); //devuelve minutos, si es 0 devuelve un nuero mayor a 0
      $oAsistencia->registrar_papeleta($ext_ini, $ext_fin, $mot, $det, $extra,$his, $asis_id, $usu_id, $tip_pap);

      header('location: ../asistencia/doc_papeleta_extras.php?asis_id='.$asis_id.'&usu_id='.$usu_id);
      exit();
    }
    else
      echo 'Error: la fecha fin no puede ser menor a la inicial'; 
  }
  else{
    $oAsistencia->registrar_papeleta($ext_ini, $ext_fin, $mot, $det, 0, $his, $asis_id, $usu_id, $tip_pap);

    header('location: ../asistencia/doc_papeleta_extras.php?asis_id='.$asis_id.'&usu_id='.$usu_id);
    exit();
  }
  
} */

if ($action == 'papeleta') {
  $asis_id = $_POST['hdd_asis_id'];
  $usu_id = $_POST['hdd_usu_id'];
  $tip_pap = $_POST['cmb_asis_pap'];
  $ext_ini = $_POST['txt_ext_ini'];
  $ext_fin = $_POST['txt_ext_fin'];
  $mot = $_POST['txt_ext_mot'];
  $det = $_POST['txt_ext_det'];
  $his = '<span style="color: blue;">Papeleta generada por ' . $_SESSION['usuario_nom'] . ' | ' . date('d-m-Y h:i a') . '</span><br>';

  if ($tip_pap == 1) {
      if ($ext_fin > $ext_ini) {
          $extra = horas_demas_base_numero_horas($ext_ini, $ext_fin, 0); //devuelve minutos, si es 0 devuelve un nuero mayor a 0
          $oAsistencia->registrar_papeleta($ext_ini, $ext_fin, $mot, $det, $extra, $his, $asis_id, $usu_id, $tip_pap);

//            header('location: www.ipdnsac.com/app/modulos/asistencia/doc_papeleta_extras.php?asis_id=' . $asis_id . '&usu_id=' . $usu_id);
//            exit();
          
          $data['estado'] = 1;
          $data['msj'] = 'Su Papeleta se ha registrado correctamente';
          echo json_encode($data);
      } else
          echo 'Error: la fecha fin no puede ser menor a la inicial';
  } else {
      $oAsistencia->registrar_papeleta($ext_ini, $ext_fin, $mot, $det, 0, $his, $asis_id, $usu_id, $tip_pap);

//        header('location: www.ipdnsac.com/app/modulos/asistencia/doc_papeleta_extras.php?asis_id=' . $asis_id . '&usu_id=' . $usu_id);
//        exit();
      $data['estado'] = 1;
      $data['msj'] = 'Su Papeleta se ha registrado correctamente';
      echo json_encode($data);
  }
  
}

/* GERSON (23-03-23) */
if($action == 'detectar_faltas'){

  $falta_obs = $_POST['falta_obs'].' | '.date('d-m-Y h:i a').'<br>';
  $usu_id = $_SESSION['usuario_id'];
  $his = '<span style="color: blue;">'.$falta_obs.'</span><br>';
  $hoy = date('Y-m-d');
  $des = 'COMPROBACIÓN DE FALTAS GENERAL DEL DÍA '.date('d-m-Y');

  // Detectar si ya se hizo la comprobacion de faltas
  
  $asistencia = $oAsistencia->detectar_comprobacion_faltas($hoy);
  if($asistencia['estado']==0){

    // obtener dias faltantes en total
    $fecha_hoy = explode("-", $hoy);
    $anio = $fecha_hoy[0];
    $mes = $fecha_hoy[1];
    $fecha_ini = $anio.'-'.$mes.'-01';
    $fecha_fin = $hoy;

    $listUsuarios = $oUsuario->listar_usuarios();
    if($listUsuarios['estado'] == 1){
      foreach ($listUsuarios['data'] as $key => $value){
        
        $dias_faltados = $oAsistencia->lista_dias_no_marcados($value['tb_usuario_id'], $fecha_ini, $fecha_fin);

        if($dias_faltados['estado']==1){
          foreach ($dias_faltados['data'] as $key => $dias) {
            // registrar falta 
            $dia=intval(date("w", strtotime($dias['tb_asistencia_fec']))); // domingos
            $feriado = $oAsistencia->detectar_feriado($dias['tb_asistencia_fec'], $anio);

            if($feriado['estado']==0){ // Identifica si no se trata de un FERIADO
              if($dia>0){ // Identifica si el día no es un DOMINGO

                $vacaciones = $oAsistencia->detectar_vacaciones($value['tb_usuario_id'],$dias['tb_asistencia_fec']);
                if($vacaciones['estado']==0){ // el usuario no está de vacaciones
                  $oAsistencia->marcar_falta($dias['tb_asistencia_id'], $falta_obs, $his);
                }

              }
            }
            
          }
        }

      }

      // registrar en tabla tb_asistenciafalta
      $oAsistencia->insertar_asistencia_hoy($usu_id, $hoy, $des);

    }

  }else{
    echo 'Comprobación de faltas ya fue realizada.';
  }

  

  /* $dias_faltados = $oAsistencia->lista_dias_no_marcados($usu_id, $fecha_ini, $fecha_fin);

  if($dias_faltados['estado']==1){
    foreach ($dias_faltados['data'] as $key => $dias) {
      // registrar falta 
      $dia=intval(date("w", strtotime($dias['tb_asistencia_fec'])));
      if($dia>0){ //no contar los dias domingos
        $oAsistencia->marcar_falta($dias['tb_asistencia_id'], $falta_obs, $his);
      }
    }
  } */

}
/*  */

?>