$(document).ready(function () {


  $("#form_asistencia").validate({
    submitHandler: function () {
      $.ajax({
        type: "POST",
        url:  VISTA_URL+"asistencia/asistencia_reg.php",
        async: true,
        dataType: "json",
        data: $("#form_asistencia").serialize(),
        beforeSend: function () {
          $('#asistencia_mensaje').show(400);
        },
        success: function (data) {
          $('#msj_asis').html(data.msj);
          swal_success("SISTEMA",data.msj,2500);
          asistencia_tabla();
          $('#modal_registro_asistencia').modal('hide');
        },
        complete: function (data) {
          if (data.status != 200) {
            swal_error("ERROR AL GUARDAR",data.responseText,6000);
          }
        }
      });
    },
    rules: {
      txt_asis_obs: {
        required: true
      }
    },
    messages: {
      txt_asis_obs: {
        required: 'Detalle'
      }
    }
  });
});


