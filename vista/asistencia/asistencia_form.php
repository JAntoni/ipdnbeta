<?php
require_once '../../core/usuario_sesion.php';
require_once ("../asistencia/Asistencia.class.php");
$oAsistencia = new Asistencia();

$asis_id = $_POST['asis_id'];
$action = $_POST['action'];

$dts = $oAsistencia->mostrarUno($asis_id);
if ($dts['estado'] == 1) {
    $cod = $dts['data']['tb_asistencia_cod'];
    $ing1 = $dts['data']['tb_asistencia_ing1'];
    $sal1 = $dts['data']['tb_asistencia_sal1'];
    $ing2 = $dts['data']['tb_asistencia_ing2'];
    $sal2 = $dts['data']['tb_asistencia_sal2'];
    $obs = $dts['data']['tb_asistencia_obs'];
    $his = $dts['data']['tb_asistencia_his'];
}
if($ing1 =='00:00:00'){
    $ing1 ='08:30:05';
    $sal1 = '13:00:05';
    $ing2 = '15:00:05';
    $sal2 = '19:02:05';
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_asistencia" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">Detalle de Asistencia</h4>
            </div>
            <form id="form_asistencia" method="post">
                <input type="hidden" name="action" value="<?php echo $action; ?>">
                <input type="hidden" name="hdd_asis_id" value="<?php echo $asis_id; ?>">

                <div class="modal-body">

                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Día Asistencia:</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control input-sm" value="<?php echo $cod; ?>" readonly="">
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Ingreso Mañana:</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="time" name="txt_asis_ing1" class="form-control input-sm" value="<?php echo $ing1;?>" required>
                                </div>
    
                                <div class="col-md-3">
                                    <label>Salida Mañana:</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="time" name="txt_asis_sal1" class="form-control input-sm" value="<?php echo $sal1;?>" required>
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Ingreso Tarde:</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="time" name="txt_asis_ing2" class="form-control input-sm" value="<?php echo $ing2;?>" required>
                                </div>
                                
                                <div class="col-md-3">
                                    <label>Salida Tarde:</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="time" name="txt_asis_sal2" class="form-control input-sm" value="<?php echo $sal2;?>" required>
                                </div>
                            </div>
                            <p>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="asistencia_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_asistencia">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/asistencia/asistencia_form.js?ver=20230401';?>"></script>