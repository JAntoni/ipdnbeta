<?php
date_default_timezone_set("America/Lima");
require_once('Asistencia.class.php');
$oAsistencia = new Asistencia();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

$fecha_hoy = date('d-m-Y');
$primer_dia = date('01-m-Y');
$mes = date('m');
$anio = date('Y');
$numDays = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);

if($fecha_hoy == $primer_dia){
  //* creamos el calendario para todos los usuarios que trabajan en oficina
  $result = $oUsuario->listar_trabajadores_oficina();
    if($result['estado'] == 1){
      foreach ($result['data'] as $key => $value) {
        $usuario_id = $value['tb_usuario_id'];

        for($e = 1; $e <= $numDays; $e++){
          $asistencia_cod = date("d-m-y", strtotime("$anio-$mes-$e"));
          $asistencia_fec = date("Y-m-d", strtotime("$anio-$mes-$e"));

          $oAsistencia->insertar_calendario_usuario($usuario_id, $asistencia_cod, $asistencia_fec);
        }
      }

      echo 'Calenrario registrado para colaboradores hoy: '.$fecha_hoy.' | primer día mes: '.$primer_dia;

      //AQUI AGREGAS LA FUNCIÓN PARA MARCAR AUTOMÁTICO LA ASISTENCIA DE LOS USUARIOS
      $usuarios_auto = [20,64]; // Usuarios 20 Manuel Vargas y 64 Celfa Torres
      $mes_anio = $mes . '-' . substr($anio, 2, 4);
      $i=0;
      while($i < count($usuarios_auto)){
        $ingresoAM = "";
        $salidaAM = "";
        $ingresoPM = "";
        $salidaPM = "";
        $usuario = $oUsuario->mostrarUno($usuarios_auto[$i]); // obtenemos datos del usuario
        if($usuario['estado']==1){
          if($usuario['data']['tb_empresa_id']==1){ // identificar si pertenece a boulevard o mall
            $ingresoAM = "08:30:00";
          }else{
            $ingresoAM = "09:00:00";
          }
          $salidaAM = "13:00:00";
          $ingresoPM = "15:00:00";
          $salidaPM = "19:00:00";
        }
        $asistencias = $oAsistencia->listar_asistencia_mes($usuarios_auto[$i], $mes_anio); // obtenemos el listado de asistencia
        if($asistencias['estado']==1){
          foreach ($asistencias['data'] as $key => $asistencia) {
            $dia=intval(date("w", strtotime($asistencia['tb_asistencia_fec']))); // domingos
            if($dia>0){ // Identifica si el día no es un DOMINGO
              // marcado_automatico_usuario(usuario_id, mes, anio) -> update, y no marcar los domingos 9 a 1 y 3 a 7 
              $oAsistencia->marcado_automatico_x_dia($asistencia['tb_asistencia_id'], $ingresoAM, $salidaAM, $ingresoPM, $salidaPM);
            }
          }

        }
        $i++;

      }

    }
  $result = NULL;
}
else
  echo 'no es primer día: '.$fecha_hoy.' // '.$primer_dia;
?>