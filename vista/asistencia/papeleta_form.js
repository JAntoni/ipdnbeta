/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $("#form_papeleta").validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "asistencia/asistencia_reg.php",
                async: true,
                dataType: "json",
                data: $("#form_papeleta").serialize(),
                beforeSend: function () {
                    $('#papeleta_mensaje').show(400);
                },
                success: function (data) {
//                    $('#msj_asis').html(data.msj);
                    $('#papeleta_mensaje').hide(400);
                    swal_success("SISTEMA", data.msj, 2500);
                    asistencia_tabla();
                    $('#modal_registro_papeleta').modal('hide');
                    var asis_id = $("#hdd_asis_id").val();
                    var usu_id = $("#hdd_usu_id").val();
//                    console.log('asis_id=  '+asis_id+'  usu_id'+usu_id);

                    if (confirm("¿Desea Generar El PDF?")) {
                        window.open('http://www.ipdnsac.com/app/modulos/asistencia/doc_papeleta_extras.php?asis_id=' + asis_id + '&usu_id=' + usu_id);
                    } else {
                        return false;
                    }
//                   window.open('http://www.ipdnsac.com/app/modulos/asistencia/doc_papeleta_extras.php?asis_id='+asis_id+'&usu_id='+usu_id,'_blank');


                },
                complete: function (data) {

                    if (data.status != 200) {
                        swal_error("ERROR AL GUARDAR", data.responseText, 6000);
                    }
                }
            });
        },
        rules: {
            txt_asis_obs: {
                required: true
            }
        },
        messages: {
            txt_asis_obs: {
                required: 'Detalle'
            }
        }
    });

});

