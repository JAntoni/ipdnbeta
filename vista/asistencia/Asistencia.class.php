<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Asistencia extends Conexion
{

  public $tb_asistencia_id;
  public $tb_asistencia_cod;
  public $tb_asistencia_reg;
  public $tb_usuario_id;
  public $tb_asistencia_ing1;
  public $tb_asistencia_sal1;
  public $tb_asistencia_ing2;
  public $tb_asistencia_sal2;
  public $tb_asistencia_ext;
  public $tb_asistencia_extini;
  public $tb_asistencia_extfin;
  public $tb_asistencia_mot;
  public $tb_asistencia_obs;
  public $tb_asistencia_det;
  public $tb_asistencia_jus;
  public $tb_asistencia_pap;
  public $tb_asistencia_fal;
  public $tb_asistencia_his;

  public $tb_asistencia_fec;
  public $tb_asistencia_horing1;
  public $tb_asistencia_horsal1;
  public $tb_asistencia_horing2;
  public $tb_asistencia_horsal2;

  function insertar_calendario_usuario($usuario_id, $asistencia_cod, $asistencia_fec)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_asistencia(
          tb_usuario_id, tb_asistencia_cod, tb_asistencia_fec) 
        VALUES (
          :usuario_id, :asistencia_cod, :asistencia_fec)
      ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":asistencia_cod", $asistencia_cod, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_fec", $asistencia_fec, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function asignar_horario_usuario()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia 
        SET tb_asistencia_horing1 =:asistencia_horing1, tb_asistencia_horsal1 =:asistencia_horsal1,
        tb_asistencia_horing2 =:asistencia_horing2, tb_asistencia_horsal2 =:asistencia_horsal2 
        WHERE tb_usuario_id =:usuario_id AND tb_asistencia_fec =:asistencia_fec
      ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":asistencia_fec", $this->tb_asistencia_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":asistencia_horing1", $this->tb_asistencia_horing1, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_horsal1", $this->tb_asistencia_horsal1, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_horing2", $this->tb_asistencia_horing2, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_horsal2", $this->tb_asistencia_horsal2, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  function insertar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_asistencia(
          tb_asistencia_cod, 
          tb_asistencia_reg, 
          tb_usuario_id, 
          tb_asistencia_ing1) 
        VALUES (
          :tb_asistencia_cod, 
          NOW(),
          :tb_usuario_id, 
          :tb_asistencia_ing1)
      ";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_cod", $this->tb_asistencia_cod, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_asistencia_ing1", $this->tb_asistencia_ing1, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function editar()
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia 
                      SET tb_asistencia_ing1 =:tb_asistencia_ing1, 
                          tb_asistencia_sal1 =:tb_asistencia_sal1,
                          tb_asistencia_ing2 =:tb_asistencia_ing2, 
                          tb_asistencia_sal2 =:tb_asistencia_sal2,
                          tb_asistencia_his = CONCAT(tb_asistencia_his,:tb_asistencia_his) 
                      where 
                          tb_asistencia_id =:tb_asistencia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_ing1", $this->tb_asistencia_ing1, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_sal1", $this->tb_asistencia_sal1, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_ing2", $this->tb_asistencia_ing2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_sal2", $this->tb_asistencia_sal2, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_his", $this->tb_asistencia_his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_id", $this->tb_asistencia_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function marcar_asistencia($cod, $tip_asis, $hor_asis, $usu_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia SET tb_asistencia_$tip_asis =:tb_asistencia where tb_asistencia_cod =:tb_asistencia_cod and tb_usuario_id =:tb_usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia", $hor_asis, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_cod", $cod, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function eliminar($asistencia_id)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "DELETE FROM tb_asistencia WHERE tb_asistencia_id =:asistencia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":asistencia_id", $asistencia_id, PDO::PARAM_INT);

      $result = $sentencia->execute();

      $this->dblink->commit();

      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function mostrarUno($asistencia_id)
  {
    try {
      $sql = "SELECT a.*,
                      CONCAT(u.tb_usuario_nom,' ', u.tb_usuario_ape) as usuario
                  FROM tb_asistencia a
                  INNER JOIN tb_usuario u on a.tb_usuario_id = u.tb_usuario_id
                  where tb_asistencia_id =:tb_asistencia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_id", $asistencia_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_asistencia_codigo($asistencia_cod, $usuario_id)
  {
    try {
      $sql = "SELECT * FROM tb_asistencia where tb_asistencia_cod =:asistencia_cod and tb_usuario_id =:usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":asistencia_cod", $asistencia_cod, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_asistencia_mes($usuario_id, $mes_anio)
  {
    try {
      $sql = "SELECT * FROM tb_asistencia  where right(tb_asistencia_cod,5) =:mes_anio and tb_usuario_id =:usuario_id order by tb_asistencia_cod";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":mes_anio", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_asistencia_mes_colaborador($usuario_id, $mes_anio)
  {
    try {
      //$sql = "SELECT * FROM tb_asistencia asi  where right(tb_asistencia_cod,5) =:mes_anio and tb_usuario_id =:usuario_id and tb_asistencia_fal = 0 order by tb_asistencia_cod";
      $sql = "SELECT asi.*, usu.tb_usuario_ing1, usu.tb_usuario_ing2 FROM tb_asistencia asi inner join tb_usuario usu on usu.tb_usuario_id = asi.tb_usuario_id where right(asi.tb_asistencia_cod,5) =:mes_anio and asi.tb_usuario_id =:usuario_id and asi.tb_asistencia_fal = 0 order by asi.tb_asistencia_cod";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":mes_anio", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function observar_justificar($obs, $jus, $asis_id, $his)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia SET tb_asistencia_obs =CONCAT(IFNULL(tb_asistencia_obs, ''),:tb_asistencia_obs), tb_asistencia_jus =:tb_asistencia_jus, tb_asistencia_his = CONCAT(IFNULL(tb_asistencia_his, ''),:tb_asistencia_his) where tb_asistencia_id =:tb_asistencia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_obs", $obs, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_jus", $jus, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_asistencia_his", $his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_id", $asis_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function observar_dia_faltado($obs, $valor, $asis_id, $his)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia SET tb_asistencia_obs = CONCAT(IFNULL(tb_asistencia_obs, ''),:tb_asistencia_obs), tb_asistencia_fal =:tb_asistencia_fal, tb_asistencia_his = CONCAT(IFNULL(tb_asistencia_his, ''),:tb_asistencia_his) where tb_asistencia_id =:tb_asistencia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_obs", $obs, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_fal", $valor, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_asistencia_his", $his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_id", $asis_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function papeleta_horas_extras($asis_id, $usu_id)
  {
    try {
      $sql = "SELECT * FROM tb_asistencia asi 
                  inner join tb_usuario us on us.tb_usuario_id = asi.tb_usuario_id 
                  inner join tb_usuarioperfil per on per.tb_usuarioperfil_id = us.tb_usuarioperfil_id 
                  where tb_asistencia_id =:tb_asistencia_id and asi.tb_usuario_id =:tb_usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_id", $asis_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay tipos de crédito registrados";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function registrar_papeleta($ext_ini, $ext_fin, $mot, $det, $extra, $his, $asis_id, $usu_id, $tip_pap)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia SET tb_asistencia_extini =:tb_asistencia_extini, tb_asistencia_extfin =:tb_asistencia_extfin,
                              tb_asistencia_mot =:tb_asistencia_mot, tb_asistencia_det =:tb_asistencia_det, 
                              tb_asistencia_ext =:tb_asistencia_ext, tb_asistencia_his = CONCAT(tb_asistencia_his,:tb_asistencia_his), tb_asistencia_pap = :tb_asistencia_pap
                              where tb_asistencia_id =:tb_asistencia_id and tb_usuario_id =:tb_usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_extini", $ext_ini, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_extfin", $ext_fin, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_mot", $mot, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_det", $det, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_ext", $extra, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_asistencia_his", $his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_id", $asis_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_asistencia_pap", $tip_pap, PDO::PARAM_INT);
      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function listar_tardanzas($usu_id, $mes_anio)
  {
    try {
      $sql = "SELECT 
                      usu.tb_usuario_id,
                      tb_usuario_ing1, 
                      tb_asistencia_ing1,
                      ((tb_asistencia_ing1 - tb_usuario_ing1)/100) as tarde_am,
                      tb_usuario_ing2,
                      tb_asistencia_ing2, 
                      ((tb_asistencia_ing2 - tb_usuario_ing2)/100) as tarde_pm,
                      tb_usuario_tur,tb_asistencia_cod,
                      tb_asistencia_sal1, 
                      tb_asistencia_sal2,
                      asi.tb_asistencia_horing1,
                      asi.tb_asistencia_horsal1,
                      asi.tb_asistencia_horing2,
                      asi.tb_asistencia_horsal2,
                      usu.tb_usuario_ing1,
                      usu.tb_usuario_sal1,
                      usu.tb_usuario_ing2,
                      usu.tb_usuario_sal2,
                      asi.tb_asistencia_fec,
                      usu.tb_empresa_id,
                      asi.tb_asistencia_ext
                  FROM 
                      tb_asistencia asi 
                      inner join tb_usuario usu on usu.tb_usuario_id = asi.tb_usuario_id 
                  where 
                      usu.tb_usuario_id =:tb_usuario_id 
                      and right(tb_asistencia_cod,5) =:tb_asistencia_cod 
                      and (tb_asistencia_ing1 > tb_usuario_ing1 or tb_asistencia_ing2 > tb_usuario_ing2 OR (tb_asistencia_ing1 IS NULL OR tb_asistencia_ing1 = '00:00:00' OR tb_asistencia_ing2 IS NULL OR tb_asistencia_ing2 = '00:00:00')) 
                      and tb_asistencia_jus =0 order by tb_asistencia_cod";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_cod", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  // JUAN 29-08-2024
  function listar_tardanzas_actualizado($usu_id, $mes_anio, $personalcontrato_id)
  {
    try {
      $sql = "SELECT 
                      usu.tb_usuario_id, 
                      tb_asistencia_ing1,
                      ((tb_asistencia_ing1 - per.tb_personalcontrato_ing1)/100) as tarde_am,
                      tb_asistencia_ing2, 
                      ((tb_asistencia_ing2 - per.tb_personalcontrato_ing2)/100) as tarde_pm,
                      tb_asistencia_cod,
                      tb_asistencia_sal1, 
                      tb_asistencia_sal2,
                      asi.tb_asistencia_horing1,
                      asi.tb_asistencia_horsal1,
                      asi.tb_asistencia_horing2,
                      asi.tb_asistencia_horsal2,
                      per.tb_personalcontrato_ing1,
                      per.tb_personalcontrato_sal1,
                      per.tb_personalcontrato_ing2,
                      per.tb_personalcontrato_sal2,
                      asi.tb_asistencia_fec,
                      usu.tb_empresa_id,
                      asi.tb_asistencia_ext
                  FROM 
                      tb_asistencia asi 
                      inner join tb_usuario usu on usu.tb_usuario_id = asi.tb_usuario_id 
                      inner join tb_personalcontrato per on per.tb_usuario_id = usu.tb_usuario_id 
                  where 
                      usu.tb_usuario_id =:tb_usuario_id and per.tb_personalcontrato_id =:personalcontrato_id
                      and right(tb_asistencia_cod,5) =:tb_asistencia_cod 
                      and (tb_asistencia_ing1 > tb_personalcontrato_ing1 or tb_asistencia_ing2 > tb_personalcontrato_ing2 OR (tb_asistencia_ing1 IS NULL OR tb_asistencia_ing1 = '00:00:00' OR tb_asistencia_ing2 IS NULL OR tb_asistencia_ing2 = '00:00:00')) 
                      and tb_asistencia_jus =0 order by tb_asistencia_cod";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":personalcontrato_id", $personalcontrato_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_asistencia_cod", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  // JUAN ANTONIO: 31-07-2023
  function listar_horas_extras($usu_id, $mes_anio)
  {
    try {
      $sql = "SELECT 
                        tb_usuario_id,
                        tb_asistencia_fec,
                        tb_asistencia_ext
                    FROM 
                        tb_asistencia
                    where 
                        tb_usuario_id =:tb_usuario_id 
                        and right(tb_asistencia_cod,5) =:tb_asistencia_cod 
                        and tb_asistencia_ext > 0";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_cod", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function listar_tardanzas_turno_tarde($usu_id, $mes_anio)
  {
    try {
      $sql = "SELECT   usu.tb_usuario_id,tb_usuario_ing1, tb_asistencia_ing1,((tb_asistencia_ing1 - tb_usuario_ing2)/100) as tarde_am,tb_usuario_ing2, 
                          tb_usuario_tur,tb_asistencia_cod FROM tb_asistencia asi inner join tb_usuario usu on usu.tb_usuario_id = asi.tb_usuario_id 
                  where 
                          usu.tb_usuario_id =:tb_usuario_id and right(tb_asistencia_cod,5) =:tb_asistencia_cod and tb_asistencia_ing1 > tb_usuario_ing2 
                          and tb_asistencia_jus =0 order by tb_asistencia_cod";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_cod", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function numero_faltas($usu_id, $mes_anio)
  {
    try {
      $sql = "SELECT COUNT(*) dias_falta FROM `tb_asistencia` where tb_usuario_id =:tb_usuario_id and right(tb_asistencia_cod,5) = :tb_asistencia_cod and tb_asistencia_fal = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_cod", $mes_anio, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }


  function observar_asistencia_codigo($asis_cod, $usu_id, $obs, $his)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia SET 
                                      tb_asistencia_obs =CONCAT(IFNULL(tb_asistencia_obs, ''),:tb_asistencia_obs),
                                      tb_asistencia_his = CONCAT(IFNULL(tb_asistencia_his, ''),:tb_asistencia_his) 
                              where 
                                      tb_asistencia_cod =:tb_asistencia_cod and tb_usuario_id =:tb_usuario_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_obs", $obs, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_his", $his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_cod", $asis_cod, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  /* GERSON (17-03-23) */
  function lista_dias_no_marcados($usu_id, $fecha_ini, $fecha_fin)
  {
    try {
      $sql = "SELECT * FROM tb_asistencia 
                    WHERE tb_usuario_id =:tb_usuario_id 
                    AND tb_asistencia_fec >= :fecha_ini 
                    AND tb_asistencia_fec < :fecha_fin
                    AND tb_asistencia_ing1 IS NULL 
                    AND tb_asistencia_sal1 IS NULL 
                    AND tb_asistencia_ing2 IS NULL 
                    AND tb_asistencia_sal2 IS NULL
                    AND tb_asistencia_jus = 0
                    AND tb_asistencia_fal = 0";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function lista_dias_faltados($usu_id, $fecha_ini, $fecha_fin)
  {
    try {
      $sql = "SELECT * FROM tb_asistencia 
                    WHERE tb_usuario_id =:tb_usuario_id 
                    AND tb_asistencia_fec >= :fecha_ini 
                    AND tb_asistencia_fec < :fecha_fin
                    AND tb_asistencia_fal = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
      $sentencia->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
      $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);

      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }

  function marcar_falta($asistencia_id, $obs, $his)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia SET 
                    tb_asistencia_fal = 1,
                    tb_asistencia_obs = CONCAT(IFNULL(tb_asistencia_obs, ''),:tb_asistencia_obs),
                    tb_asistencia_his = CONCAT(IFNULL(tb_asistencia_his, ''),:tb_asistencia_his) 
                    WHERE tb_asistencia_id =:tb_asistencia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistencia_obs", $obs, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_his", $his, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistencia_id", $asistencia_id, PDO::PARAM_INT);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  /*  */

  /* GERSON (23-03-23) */
  function insertar_asistencia_hoy($usuario_id, $hoy, $des)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "INSERT INTO tb_asistenciafalta(
                    tb_asistenciafalta_fecha, 
                    tb_asistenciafalta_des, 
                    tb_usuario_id) 
                VALUES (
                    :tb_asistenciafalta_fecha,
                    :tb_asistenciafalta_des,
                    :tb_usuario_id)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_asistenciafalta_fecha", $hoy, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_asistenciafalta_des", $des, PDO::PARAM_STR);
      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  function detectar_comprobacion_faltas($hoy)
  {
    try {
      $sql = "SELECT * FROM tb_asistenciafalta 
                    WHERE tb_asistenciafalta_fecha = :tb_asistenciafalta_fecha
                    AND tb_asistenciafalta_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_asistenciafalta_fecha", $hoy, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  /* GERSON (27-03-23) */
  function detectar_feriado($hoy, $anio)
  {
    try {
      $sql = "SELECT * FROM tb_feriado 
                    WHERE tb_feriado_fecha = :tb_feriado_fecha
                    AND tb_feriado_anio = :tb_feriado_anio
                    AND tb_feriado_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_feriado_fecha", $hoy, PDO::PARAM_STR);
      $sentencia->bindParam(":tb_feriado_anio", $anio, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  /* GERSON (03-04-23) */
  function detectar_vacaciones($usuario_id, $fecha)
  {
    try {
      $sql = "SELECT con.*
                    FROM tb_contratolinea con 
                    INNER JOIN tb_vacaciones vac ON con.tb_vacaciones_id = vac.tb_vacaciones_id
                    WHERE con.tb_contratolinea_xac = 1 
                    AND vac.tb_usuario_id = :tb_usuario_id
                    AND (con.tb_contratolinea_vacini <= :tb_fecha AND con.tb_contratolinea_vacfin >= :tb_fecha)";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":tb_fecha", $fecha, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "No hay Tardanzas para este usuario";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /*  */

  /* GERSON (31-03-23) */
  function marcado_automatico_x_dia($asistencia_id, $ingresoAM, $salidaAM, $ingresoPM, $salidaPM)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia 
            SET tb_asistencia_ing1 =:asistencia_ing1, tb_asistencia_sal1 =:asistencia_sal1,
            tb_asistencia_ing2 =:asistencia_ing2, tb_asistencia_sal2 =:asistencia_sal2 
            WHERE tb_asistencia_id =:asistencia_id";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":asistencia_id", $asistencia_id, PDO::PARAM_INT);
      $sentencia->bindParam(":asistencia_ing1", $ingresoAM, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_sal1", $salidaAM, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_ing2", $ingresoPM, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_sal2", $salidaPM, PDO::PARAM_STR);

      $result = $sentencia->execute();

      $this->dblink->commit();
      return $result; //si es correcto el ingreso retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }
  /*  */

  function justificar_asistencia_por_vacaciones($usuario_id, $asistencia_fec, $asistencia_obs)
  {
    $this->dblink->beginTransaction();
    try {
      $sql = "UPDATE tb_asistencia SET 
                  tb_asistencia_fal = 0,
                  tb_asistencia_jus = 1,
                  tb_asistencia_obs = CONCAT(IFNULL(tb_asistencia_obs, ''),:asistencia_obs),
                  tb_asistencia_his = CONCAT(IFNULL(tb_asistencia_his, ''),:asistencia_obs) 
                  WHERE tb_usuario_id =:usuario_id AND tb_asistencia_fec =:asistencia_fec";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
      $sentencia->bindParam(":asistencia_fec", $asistencia_fec, PDO::PARAM_STR);
      $sentencia->bindParam(":asistencia_obs", $asistencia_obs, PDO::PARAM_STR);

      $result = $sentencia->execute();
      $this->dblink->commit();
      return $result; //si es correcto retorna 1
    } catch (Exception $e) {
      $this->dblink->rollBack();
      throw $e;
    }
  }

  /* DETECTAR FERIADO ANTHONY */
  function detectar_feriado_fecha($fecha)
  {
    try {
      $sql = "SELECT * FROM tb_feriado 
                    WHERE tb_feriado_fecha = :tb_feriado_fecha
                    AND tb_feriado_xac = 1";

      $sentencia = $this->dblink->prepare($sql);
      $sentencia->bindParam(":tb_feriado_fecha", $fecha, PDO::PARAM_STR);
      $sentencia->execute();

      if ($sentencia->rowCount() > 0) {
        $resultado = $sentencia->fetch();
        $retorno["estado"] = 1;
        $retorno["mensaje"] = "exito";
        $retorno["data"] = $resultado;
        $sentencia->closeCursor(); //para libera memoria de la consulta
      } else {
        $retorno["estado"] = 0;
        $retorno["mensaje"] = "La fecha no esta considerada como feriado";
        $retorno["data"] = "";
      }

      return $retorno;
    } catch (Exception $e) {
      throw $e;
    }
  }
  /* DETECTAR FERIADO ANTHONY */
}
