/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
  vacaciones_tabla();

});


function vacaciones_form(usuario_act, vacaciones_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vacaciones/vacaciones_form.php",
    async: true,
    dataType: "html",
    data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      vacaciones_id: vacaciones_id
    }),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (data) {
      $('#modal_mensaje').modal('hide');
      if (data != 'sin_datos') {
        $('#div_modal_vacaciones_form').html(data);
        $('#modal_registro_vacaciones').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if (usuario_act == 'L' || usuario_act == 'E')
          form_desabilitar_elementos('form_vacaciones'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_registro_vacaciones', 'limpiar'); //funcion encontrada en public/js/generales.js
      } else {
        //llamar al formulario de solicitar permiso
        var modulo = 'vacaciones';
        var div = 'div_modal_vacaciones_form';
        permiso_solicitud(usuario_act, vacaciones_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
    },
    complete: function (data) {

    },
    error: function (data) {
      //            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      //            $('#overlay_modal_mensaje').removeClass('overlay').empty();
      //            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      console.log(data.responseText);
    }
  });
}
function vacaciones_horasExtras(vacaciones_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vacaciones/vacaciones_horasExtras.php",
    async: true,
    dataType: "html",
    data: ({
      vacaciones_id: vacaciones_id
    }),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (data) {
      $('#modal_mensaje').modal('hide');

      $('#div_modal_vacaciones_form').html(data);
      $('#modal_registro_vacaciones_dias').modal('show');

      modal_hidden_bs_modal('modal_registro_vacaciones_dias', 'limpiar'); //funcion encontrada en public/js/generales.js


    },
    complete: function (data) {

    },
    error: function (data) {
      //            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      //            $('#overlay_modal_mensaje').removeClass('overlay').empty();
      //            $('#body_modal_mensaje').html('ERRROR!:' + data.responseText);
      console.log(data.responseText);
    }
  });
}

function vacaciones_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vacaciones/vacaciones_tabla.php",
    async: true,
    dataType: "html",
    data: ({estado: $("#cmb_contrato_est").val()}),
    beforeSend: function () {
      $('#vacaciones_mensaje_tbl').show(300);
    },
    success: function (data) {
      $('#div_vacaciones_tabla').html(data);
      $('#vacaciones_mensaje_tbl').hide(300);
    },
    complete: function (data) {
      //            console.log(data);
      estilos_datatable('tbl_vacacioness');
    },
    error: function (data) {
      $('#vacaciones_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function contratolinea_timeline(vacaciones_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "contratolinea/contratolinea_timeline.php",
    async: true,
    dataType: "html",
    data: ({
      vacaciones_id: vacaciones_id
    }),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (data) {
      $('#div_modal_contratolinea_timeline').html(data);
      $('#modal_contratolinea_timeline').modal('show');
      modal_height_auto('modal_contratolinea_timeline'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_contratolinea_timeline', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function (data) {
      $('#modal_mensaje').modal('hide');
    },
    error: function (data) {
      alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}

function contratos_form(vacaciones_id, usuario_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "vacaciones/contratolinea.php",
    async: true,
    dataType: "html",
    data: ({
      vacaciones_id: vacaciones_id
    }),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (data) {
      $('#div_modal_contratolinea_timeline').html(data);
      $('#modal_contratolinea_pdf').modal('show');
      modal_height_auto('modal_contratolinea_pdf'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_contratolinea_pdf', 'limpiar'); //funcion encontrada en public/js/generales.js
      //upload_pdf(vacaciones_id,usuario_id);
    },
    complete: function (data) {
      $('#modal_mensaje').modal('hide');
    },
    error: function (data) {
      alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
      console.log(data.responseText);
    }
  });
}

function eliminar_contrato(upload_id){
  $.confirm({
    title: 'Eliminar Contrato',
    icon: 'fa fa-frown-o',
    theme: 'modern',
    content: '¿Está seguro que desea eliminar este contrato?',
    type: 'red',
    typeAnimated: true,
    buttons: {
      Eliminar: function(){
        $.ajax({
          type: "POST",
          url: VISTA_URL+"uploadpdf/upload_controller.php",
          async: true,
          dataType: "json",
          data: ({
            action: 'eliminar',
            upload_id: upload_id
          }),
          beforeSend: function() {
            
          },
          success: function(data){
            if(parseInt(data.estado) == 1){
              notificacion_success(data.mensaje, 4000)
              $('#btn_eliminar'+upload_id).remove()
            }
            else
              alerta_warning('Aviso', data.mensaje); //en generales.js
            
          },
          complete: function(data){
            console.log(data);
          },
          error: function(data){
            alerta_error('Error', 'ERRROR!: '+ data.responseText); //en generales.js
            console.log(data)
          }
        });
      },
      Cerrar: function (){
        $('.btn_desembolsar').attr("disabled", false);
      }
    }
  });
}