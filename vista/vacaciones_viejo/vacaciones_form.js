/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var empresa_id = $("#cmb_empresa_id").val();
    var action = $("#action").val();
    
    if(action=="insertar"){
         cmb_usu_id(empresa_id);
    }
   

    $('#datetimepicker1, #datetimepicker2').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });
    $('#datetimepicker3').datepicker({
        language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        //startDate: "-0d"
//        endDate: new Date()
    });

    $("#datetimepicker1").on("change", function (e) {
        var startVal = $('#txt_fecha_inicio').val();
        $('#datetimepicker2').data('datepicker').setStartDate(startVal);
    });
    $("#datetimepicker2").on("change", function (e) {
        var endVal = $('#txt_fecha_fin').val();
        $('#datetimepicker1').data('datepicker').setEndDate(endVal);
    });

//    $("#datetimepicker3").on("change", function (e) {
//        var endVal = $('#txt_fecha_nuevo_fin').val();
//        $('#datetimepicker2').data('datepicker').setEndDate(endVal);
//    });


    $('#form_vacaciones').validate({
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: VISTA_URL + "vacaciones/vacaciones_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_vacaciones").serialize(),
                beforeSend: function () {
                    $('#vacaciones_mensaje').show(400);
//                    $('#btn_guardar_vacaciones').prop('disabled', true);
                },
                success: function (data) {
                    if (parseInt(data.estado) > 0) {
                        $('#vacaciones_mensaje').removeClass('callout-info').addClass('callout-success')
                        $('#vacaciones_mensaje').html(data.mensaje);
                        swal_success("SISTEMA",data.mensaje,2500);
                        setTimeout(function () {
                            vacaciones_tabla();
                            $('#modal_registro_vacaciones').modal('hide');
                        }, 1000
                                );
                    } else {
                        $('#vacaciones_mensaje').removeClass('callout-info').addClass('callout-warning')
                        $('#vacaciones_mensaje').html('Alerta: ' + data.mensaje);
                        swal_warning("AVISO",data.mensaje,7500);
//                        $('#btn_guardar_vacaciones').prop('disabled', false);
                    }
                },
                complete: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    $('#vacaciones_mensaje').removeClass('callout-info').addClass('callout-danger')
                    $('#vacaciones_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
                }
            });
        },
        rules: {
            txt_vacaciones_nom: {
                required: true,
                minlength: 2
            },
            txt_vacaciones_des: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            txt_vacaciones_nom: {
                required: "Ingrese un nombre para el Area",
                minlength: "Como mínimo el nombre debe tener 2 caracteres"
            },
            txt_vacaciones_des: {
                required: "Ingrese una descripción para el Area",
                minlength: "La descripción debe tener como mínimo 5 caracteres"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });












});


$("#cmb_empresa_id").change(function () {
    var empresa_id = $("#cmb_empresa_id").val();
    cmb_usu_id(empresa_id);
});

function cmb_usu_id(empresa_id) {
    var mostrar = 0;
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/cmb_usu_id_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar, //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
            empresa_id: empresa_id
        }),
        beforeSend: function () {
            $('#cmb_usu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_usu_id').html(html);
        },
        complete: function (html) {

        }
    });
}

function upload_formpdf(usuario_act, upload_id) {
    upload_uniq = $('#hdd_upload_uniq').val();
    vacaciones_id = $('#hdd_vacaciones_id').val();
    
    $.ajax({
        type: "POST",
        url: VISTA_URL + "uploadpdf/upload_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            upload_id: upload_id,
            upload_uniq: upload_uniq,
            modulo_nom: 'contratos', //nombre de la tabla a relacionar
            modulo_id: vacaciones_id
        }),
        beforeSend: function () {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function (data) {
            $('#modal_mensaje').modal('hide');
            if (data != 'sin_datos') {
                $('#div_modal_uploadpdf_form').html(data);
                $('#modal_registro_upload').modal('show');

                //funcion js para agregar un largo automatico al modal, al abrirlo
                modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
        },
        complete: function (data) {

        },
        error: function (data) {
            alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
        }
    });
}