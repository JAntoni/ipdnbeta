<?php
require_once('../../core/usuario_sesion.php');
require_once('../vacaciones/Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$vacaciones_id = intval($_POST['vacaciones_id']);

$lista = '';
$result = $oVacaciones->listar_contratos_documentos($vacaciones_id);
  if($result['estado'] > 0){
    $contador = 1;
    foreach ($result['data'] as $key => $value) {
      $lista .='
        <div class="col-md-10" style="margin-bottom: 5px;">
          <a href="'.$value['upload_url'].'" target="_blank" alt="Ver Contrato"><span class="info-box-icon bg-aqua"><i class="fa fa-file-pdf-o"></i></span></a>
          <div class="info-box-content">
            <span class="info-box-text">Contrato N° '.$contador.'</span>
            <span class="info-box-number">Contrato subido el '.mostrar_fecha_hora($value['upload_reg']).'</span>
          </div>
        </div>
        <div class="col-md-2" style="padding-top: 25px;">
          <button type="button" class="btn btn-danger" id="btn_eliminar'.$value['upload_id'].'" onclick="eliminar_contrato('.$value['upload_id'].')">x</button>
        </div>
      ';
      $contador ++;
    }
  }
  else
    $lista = '<div class="col-md-12"><span class="info-box-number">No hay contratos disponibles</span></>';
$result = NULL;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_contratolinea_pdf" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" onclick="">x</span>
        </button>
        <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #000099">HISTORIAL DE CONTRATO(s) EN PDF</h4>
      </div>
      <div class="modal-body">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle" style="cursor: move;">

            <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
              <div class="btn-group" data-toggle="btn-toggle">

              </div>
            </div>
          </div>

          <div class="row" id="chat-box">
            <!-- chat item -->
            <?php echo $lista; ?>
            <!-- /.item -->
          </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>