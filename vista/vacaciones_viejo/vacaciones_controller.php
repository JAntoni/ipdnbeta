<?php

require_once('../../core/usuario_sesion.php');
require_once('Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once('../uploadpdf/Upload.class.php');
$oUpload = new Upload();
require_once ('../contratolinea/Contratolinea.class.php');
$ocontratolinea = new Contratolinea();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once ('../funciones/funciones.php');
require_once ('../funciones/fechas.php');

$action = $_POST['action'];

//echo 'codigo de entrada =='.$_POST['hdd_vacaciones_id'];exit();

$oVacaciones->tb_vacaciones_id = intval($_POST['hdd_vacaciones_id']);
$oVacaciones->tb_usuario_id = $_POST['cmb_usu_id'];
$oVacaciones->tb_fecha_inicio_contrato = fecha_mysql($_POST['txt_fecha_inicio']);
$oVacaciones->tb_fecha_fin_contrato = fecha_mysql($_POST['txt_fecha_fin']);
$oVacaciones->tb_dias_tomados = 0;
$oVacaciones->tb_empresa_id = $_POST['cmb_empresa_id'];
$oVacaciones->tb_vacaciones_est = 1;
//$oVacaciones->tb_vacaciones_est=intval($_POST['cbx_vacaciones_estado']);

if ($action != 'contratos_vencidos') {
    if ($_POST['cmb_usu_id'] == 0) {
        $data['estado'] = 0;
        $data['mensaje'] = 'Debe Seleccionar un Usuario antes de Continuad';
        echo json_encode($data);
        exit();
    }
}

if ($action == 'insertar') {

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al guardar el Usuario.';
    
    //$result = $oVacaciones->mostrarUno_usuario($_POST['cmb_usu_id']);
    $result = $oVacaciones->mostrar_usuario_vigente($_POST['cmb_usu_id']);
        if ($result['estado'] == 1) {
            $usuario_nombre = $result['data']['tb_usuario_nom'];
            $usuario_apellido = $result['data']['tb_usuario_ape'];
            
            $data['estado'] = 0;
            //$data['mensaje'] = 'El Usuario '.$usuario_nombre.' '.$usuario_apellido.' ya se encuentra Registrado ';
            $data['mensaje'] = 'El Usuario '.$usuario_nombre.' '.$usuario_apellido.' aún tiene contrato VIGENTE ';
            echo json_encode($data);
            exit();
        }
    $result = NULL;

    $resultado = $oVacaciones->insertar();
        if ($resultado['estado'] > 0) {
            $data['estado'] = 1;
            $vacaciones_id = $resultado['tb_vacaciones_id'];
            $data['mensaje'] = 'Usuario registrado correctamente.';
            $data['datos'] = $resultado;

            if ($vacaciones_id > 0) {
                $upload_uniq = $_POST['hdd_upload_uniq'];
                $oUpload->modificar_modulo_id_imagen($vacaciones_id, $upload_uniq);

                $dts = $oUsuario->mostrarUno($_POST['cmb_usu_id']);
                if ($dts['estado'] == 1) {
                    $usuario_nombre = $dts['data']['tb_usuario_nom'];
                    $usuario_apellido = $dts['data']['tb_usuario_ape'];
                    $usuario_per = $dts['data']['tb_usuario_per'];
                }

                $ocontratolinea->tb_contratolinea_det = "Ha Registrado un nuevo contrato para el colaborador <b>" . $usuario_nombre . ' ' . $usuario_apellido . '</b> con fecha de inicio <b>' . mostrar_fecha($_POST['txt_fecha_inicio']) . '</b> y finaliza el dia <b>' . mostrar_fecha($_POST['txt_fecha_fin']) . '</b>.';
                $ocontratolinea->tb_vacaciones_id = $vacaciones_id;
                $ocontratolinea->tb_usuario_id = $_SESSION['usuario_id'];
                $ocontratolinea->insertar();
            }
        }
    $result = NULL;

    echo json_encode($data);
} elseif ($action == 'modificar') {

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al modificar el Contrato.';

    if (strtotime($_POST['txt_fecha_fin']) == strtotime($_POST['txt_fecha_fin2'])) {
        $upload_uniq = $_POST['hdd_upload_uniq'];
        $doc = $oUpload->modificar_modulo_id_imagen($_POST['hdd_vacaciones_id'], $upload_uniq);

        $data['estado'] = 1;
        $data['mensaje'] = 'Contrato Modificado Correctamente';
    } elseif (strtotime($_POST['txt_fecha_fin']) > strtotime($_POST['txt_fecha_fin2'])) {
        $resultados = $oVacaciones->modificar();

        if ($resultados == 1) {
            $data['estado'] = 1;
            $data['mensaje'] = 'Contrato modificado correctamente';

            $upload_uniq = $_POST['hdd_upload_uniq'];
            $oUpload->modificar_modulo_id_imagen($_POST['hdd_vacaciones_id'], $upload_uniq);

            $dts = $oUsuario->mostrarUno($_POST['cmb_usu_id']);
            if ($dts['estado'] == 1) {
                $usuario_nombre = $dts['data']['tb_usuario_nom'];
                $usuario_apellido = $dts['data']['tb_usuario_ape'];
                $usuario_per = $dts['data']['tb_usuario_per'];
            }

            $ocontratolinea->tb_contratolinea_det = "Ha Extendido un nuevo contrato para el colaborador <b>" . $usuario_nombre . ' ' . $usuario_apellido . '</b> con fecha de registro el día <b>' . date('d-m-Y') . '</b> y cuya nueva finalización es el dia <b>' . mostrar_fecha($_POST['txt_fecha_fin']) . '</b>.<br>Ademas se ha subido un nuevo documeto';
            $ocontratolinea->tb_vacaciones_id = intval($_POST['hdd_vacaciones_id']);
            $ocontratolinea->tb_usuario_id = $_SESSION['usuario_id'];
            $ocontratolinea->insertar();
        }
    } else {
        $data['estado'] = 0;
        $data['mensaje'] = 'La nueva fecha de fin de contrato ' . $_POST['txt_fecha_fin'] . ' debe ser mayor a ' . $_POST['txt_fecha_fin2'] . ' fecha de su último contrato registrado en el sistema';
    }

    echo json_encode($data);
} elseif ($action == 'eliminar') {
    $vacaciones_id = intval($_POST['hdd_vacaciones_id']);

    $data['estado'] = 0;
    $data['mensaje'] = 'Existe un error al Finalizar el Contrato';

    $resultados = $oVacaciones->modificar_campo($vacaciones_id, 'tb_vacaciones_est', 2, 'INT');

    if ($resultados == 1) {
        $data['estado'] = 1;
        $data['mensaje'] = 'Contrato Finalizado correctamente';

        $dts = $oUsuario->mostrarUno($_POST['cmb_usu_id']);
        if ($dts['estado'] == 1) {
            $usuario_nombre = $dts['data']['tb_usuario_nom'];
            $usuario_apellido = $dts['data']['tb_usuario_ape'];
            $usuario_per = $dts['data']['tb_usuario_per'];
        }

        $ocontratolinea->tb_contratolinea_det = "Ha Finalizado el contrato del colaborador <b>" . $usuario_nombre . ' ' . $usuario_apellido . '</b> hoy ' . date('d-m-Y') . ' pero cuya finalización era dia <b>' . mostrar_fecha($_POST['txt_fecha_fin2']) . '</b>.';
        $ocontratolinea->tb_vacaciones_id = intval($_POST['hdd_vacaciones_id']);
        $ocontratolinea->tb_usuario_id = $_SESSION['usuario_id'];
        $ocontratolinea->insertar();
    }

    echo json_encode($data);
} elseif ($action == 'horasextras') {
    
    $vacaciones_id = intval($_POST['hdd_vacaciones_id']);
    $fecha1=$_POST['txt_fecha_inicio'];
    $fecha2=$_POST['txt_fecha_fin'];
    
    
    $dia = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
    $dia = abs($dia);
    $dia = floor($dia)+1;
        
    $oVacaciones->modificar_dias($vacaciones_id, $dia);
    
     $dts = $oUsuario->mostrarUno($_POST['cmb_usu_id']);
        if ($dts['estado'] == 1) {
            $usuario_nombre = $dts['data']['tb_usuario_nom'];
            $usuario_apellido = $dts['data']['tb_usuario_ape'];
            $usuario_per = $dts['data']['tb_usuario_per'];
        }
    
    $ocontratolinea->tb_contratolinea_det = "Ha otorgado <b>".$dia."</b> día(s) de vacaciones al colaborador <b>" . $usuario_nombre . ' ' . $usuario_apellido . '</b> que empieza desde el dia <b>'.$fecha1.'</b> y culmina el dia <b>'.$fecha2.'</b>';
    $ocontratolinea->tb_vacaciones_id = intval($_POST['hdd_vacaciones_id']);
    $ocontratolinea->tb_usuario_id = $_SESSION['usuario_id'];
    $ocontratolinea->tb_contratolinea_vacini = fecha_mysql($fecha1);
    $ocontratolinea->tb_contratolinea_vacfin = fecha_mysql($fecha2);
    $ocontratolinea->insertarVacaciones();
    
    $data['estado'] = 1;
    $data['mensaje'] = 'Vacaciones Registradas correctamente';
    
    echo json_encode($data);

/* GERSON (22-04-23) */
} elseif ($action == 'contratos_vencidos') {
    
    $fecha = date('Y-m-d');
    $por_vencer = array();
    $vencidos = array();

    $dts1 = $oVacaciones->mostrarTodosPorVencer($fecha);
    if ($dts1['estado'] == 1) {
        $i = 0;
        foreach ($dts1['data'] as $key => $value1) {
            $v1['id'] = $value1['tb_vacaciones_id'];
            $v1['colaborador'] = $value1['colaborador'];
            $v1['fin_contrato'] = $value1['tb_fecha_fin_contrato'];
            $v1['vencimiento'] = $value1['dias_vencido'];
            $por_vencer[$i] = $v1;
            $i++;
        }
    }

    $dts2 = $oVacaciones->mostrarTodosVencidos($fecha);
    if ($dts2['estado'] == 1) {
        $j = 0;
        foreach ($dts2['data'] as $key => $value2) {
            $v2['id'] = $value2['tb_vacaciones_id'];
            $v2['colaborador'] = $value2['colaborador'];
            $v2['fin_contrato'] = $value2['tb_fecha_fin_contrato'];
            $v2['vencimiento'] = $value2['dias_vencido'];
            $vencidos[$j] = $v2;
            $j++;
        }
    }
    
    
    $data['estado'] = 1;
    $data['por_vencer'] = $por_vencer;
    $data['vencidos'] = $vencidos;
    //$data['mensaje'] = 'Vacaciones Registradas correctamente';
    
    echo json_encode($data);
}
/*  */
else {
    $data['estado'] = 0;
    $data['mensaje'] = 'No se ha identificado ningún tipo de transacción para ' . $action;

    echo json_encode($data);
}
?>