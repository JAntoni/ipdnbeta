<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                    <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="cmb_contrato_est" class="control-label">Estado:</label>
                                <select class="form-control input-sm mayus" id="cmb_contrato_est" name="cmb_contrato_est" onchange="vacaciones_tabla()">
                                    <option value="1">VIGENTES</option>
                                    <option value="2">TODOS</option>
                                </select>
                            </div>
                            <button class="btn btn-primary btn-sm" onclick="vacaciones_form('I', 0)"><i class="fa fa-plus"></i> Agregar</button>
                        </div>
                    </div>
                </div>
                                
            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="vacaciones_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_vacaciones_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('vacaciones_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_modal_vacaciones_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <div id="div_modal_uploadpdf_form"></div>
            <div id="div_modal_contratolinea_timeline"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
