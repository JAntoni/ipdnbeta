<?php
//    date_default_timezone_set("America/Lima");
require_once('../../core/usuario_sesion.php');
require_once('Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once ('../empresa/Empresa.class.php');
$oEmpresa = new Empresa();

require_once ('../funciones/funciones.php');
require_once ('../funciones/fechas.php');
$estado = $_POST['estado'];
//$result = $oVacaciones->mostrarTodos();
$result = $oVacaciones->mostrarTodosNuevo($estado);
//    echo 'hasta aki estoy entrandp normal';exit();

$tr = '';
if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        
        $fechaFin = $value['tb_fecha_fin_contrato'];
        $hoy = date('Y-m-d');

        $dts = $oUsuario->mostrarUno($value['tb_usuario_id']);
        if ($dts['estado'] == 1) {
            $usuario_nombre = $dts['data']['tb_usuario_nom'];
            $usuario_apellido = $dts['data']['tb_usuario_ape'];
            $usuario_per = $dts['data']['tb_usuario_per'];
        }
        $dts1 = $oEmpresa->mostrarUno($value['tb_empresa_id']);
        if ($dts1['estado'] == 1) {
            $empresa_nomcom = $dts1['data']['tb_empresa_nomcom'];
        }
        
        if(strtotime($hoy) > strtotime($fechaFin)){
            $hoy = $value['tb_fecha_fin_contrato'];
        }
        
        $dias = (strtotime($hoy) - strtotime($value['tb_fecha_inicio_contrato'])) / 86400;
        $dias = abs($dias);
        $dias = floor($dias);
        $dias_vacaciones = 0;
        $añoslaborando = CalcularAnios($value['tb_fecha_inicio_contrato'], $hoy); //formato Y-m-d
        if($añoslaborando>0){
            if($usuario_per==1){
                $dias_vacaciones=30*$añoslaborando;
            }
            else {
                $dias_vacaciones=15*$añoslaborando;
            }
        }
        else {
            $añoslaborando=0;
        }
        $dias_tomados = intval($value['tb_dias_tomados']);
        $dias_restantes = $dias_vacaciones - $dias_tomados;
        
        $est=$value['tb_vacaciones_est'];
        
        if($est==1){
            $estado='<span class="btn btn-success btn-xs">VIGENTE</span>';
        }
        if($est==2){
            $estado='<span class="btn btn-danger btn-xs">FINALIZADO</span>';
        }

        //if($est==1){
            $tr .= '<tr id="tabla_cabecera_fila">';
            $tr .= '
                <td id="tabla_fila"> '. $value['tb_vacaciones_id'] .'</td>
                <td id="tabla_fila"> '. $usuario_apellido . ' ' . $usuario_nombre .'</td>
                <td id="tabla_fila"> '. mostrar_fecha($value['tb_fecha_inicio_contrato']) .'</td>
                <td id="tabla_fila"> '. mostrar_fecha($value['tb_fecha_fin_contrato']) .'</td>
                <td id="tabla_fila"> '. $dias.' DIAS / '.$añoslaborando.' AÑOS</td>
                <td id="tabla_fila"> '. $dias_vacaciones .'</td>
                <td id="tabla_fila"> '. $dias_tomados .'</td>
                <td id="tabla_fila"> '. $dias_restantes .'</td>
                <td id="tabla_fila"> '. $estado .'</td>
                <td id="tabla_fila"> '. $empresa_nomcom .'</td>
                <td id="tabla_fila" align="center">';
                $usuarios_permitidos = [42, 11, 2];
                if(intval($_SESSION['usuariogrupo_id']) == 2 || intval($_SESSION['usuariogrupo_id']) == 6) {  //para todos los usuarios con grupo gerencia 
                    $tr .= '<a class="btn btn-primary btn-xs" onclick="contratos_form('.$value['tb_vacaciones_id'].','.$value['tb_usuario_id'].')"><i class="fa fa-file-pdf-o" title="Ver Contratos"></i></a>';
                    if($value['tb_vacaciones_est']==1){    
                    $tr .= '<a class="btn btn-warning btn-xs" onclick="vacaciones_form(\'M\',' . $value['tb_vacaciones_id'] . ')"><i class="fa fa-edit" title="Actualizar Contrato"></i></a>
                            <a class="btn btn-danger btn-xs" onclick="vacaciones_form(\'E\',' . $value['tb_vacaciones_id'] . ')"><i class="fa fa-clock-o" title="Finalizar Contrato"></i></a>
                            <a class="btn btn-primary btn-xs" onclick="vacaciones_horasExtras('.$value['tb_vacaciones_id'].')"><i class="fa fa-suitcase" title="vacaciones"></i></a>';
                    }    
                    $tr .= '<div class="btn-group">
                        <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                        <li><a class="btn" onclick="contratolinea_timeline( '. $value['tb_vacaciones_id'] .')"><b>Historial</b></a></li>
                        </ul>
                    </div>';
                }
                $tr .= '</td>';
            
            $tr .= '</tr>';
        //}

    }
    $result = null;
}
//  else {
//    echo $result['mensaje'];
//    $result = null;
//    exit();
//  }
?>
<table id="tbl_vacacioness" class="table table-hover">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">ID</th>
            <th id="tabla_cabecera_fila">USUARIO</th>
            <th id="tabla_cabecera_fila">INICIO DE CONTRATO</th>
            <th id="tabla_cabecera_fila">FIN DE CONTRATO</th>
            <th id="tabla_cabecera_fila">DIAS LABORADOS/AÑOS</th>
            <th id="tabla_cabecera_fila">DIAS VACACIONES</th>
            <th id="tabla_cabecera_fila">DIAS VACIONES TOMADOS</th>
            <th id="tabla_cabecera_fila">DIAS RESTANTES</th>
            <th id="tabla_cabecera_fila">CONTRATO</th>
            <th id="tabla_cabecera_fila">SEDE</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
<?php echo $tr; ?>
    </tbody>
</table>
