<?php
require_once ('../../core/usuario_sesion.php');
require_once('../vacaciones/Vacaciones.class.php');
$oVacaciones = new Vacaciones();
require_once ('../empresa/Empresa.class.php');
$oEmpresa = new Empresa();
require_once ('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$fecha_inicio_contrato = date('Y-m-d');
$fecha_fin_contrato = date('Y-m-d');
$vacaciones_id = $_POST['vacaciones_id'];

//si la accion es modificar, mostramos los datos del vacaciones por su ID
if (intval($vacaciones_id) > 0) {
    $result = $oVacaciones->mostrarUno($vacaciones_id);
    if ($result['estado'] != 1) {
        $mensaje = 'No se ha encontrado ningún registro para el vacaciones seleccionado, inténtelo nuevamente.';
        $bandera = 4;
    } else {
        $empresa_id = $result['data']['tb_empresa_id'];
        $fecha_inicio_contrato = $result['data']['tb_fecha_inicio_contrato'];
        $fecha_fin_contrato = $result['data']['tb_fecha_fin_contrato'];
        $tb_dias_tomados = $result['data']['tb_dias_tomados'];
        $usuario_id = $result['data']['tb_usuario_id'];
    }
    $result = NULL;
}


if ($usuario_id > 0) {
    $dts = $oUsuario->mostrarUno($usuario_id);
    if ($dts['estado'] == 1) {
        $usuario_nombre = $dts['data']['tb_usuario_nom'];
        $usuario_apellido = $dts['data']['tb_usuario_ape'];
        $usuario_per = $dts['data']['tb_usuario_per'];
    }
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_vacaciones_dias" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;font-weight: bold;color: #0000cc">SOLICTUD DE VACACIONES</h4>
            </div>
            <form id="form_vacaciones" method="post">
                <input type="hidden" name="action" value="horasextras">
                <input type="hidden" name="hdd_vacaciones_id" value="<?php echo $vacaciones_id; ?>">

                <div class="modal-body">

                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Colaborador:</label> 
                                    <input type="text" class="form-control input-sm" value="<?php echo $usuario_nombre . ' ' . $usuario_apellido ?>" readonly="">
                                    <input type="hidden" class="form-control input-sm" name="cmb_usu_id" id="cmb_usu_id" value="<?php echo $usuario_id ?>">
                                </div>
                            </div>
                            <p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>INICIO DE VACACIONES :</label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control input-sm" name="txt_fecha_inicio" id="txt_fecha_inicio" value="<?php echo date('d-m-Y') ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>FIN DE VACACIONES :</label>
                                        <div class='input-group date' id='datetimepicker2'>
                                            <input type='text' class="form-control input-sm" name="txt_fecha_fin" id="txt_fecha_fin" value="<?php echo date('d-m-Y') ?>"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="vacaciones_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_vacaciones">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo 'vista/vacaciones/vacaciones_horasExtras.js'; ?>"></script>
