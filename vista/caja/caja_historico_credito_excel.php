<?php

session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Lima');

if (PHP_SAPI == 'cli')
    die('Se visualiza solamente desde un navegador.');

/** Include PHPExcel */
require_once ('../../static/libreriasphp/phpexcel/Classes/PHPExcel.php');

//require_once('../../core/usuario_sesion.php');
require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("../cuotapago/Cuotapago.class.php");
$oCuotapago = new Cuotapago();
require_once ("../clientecaja/Clientecaja.class.php");
$oClientecaja = new Clientecaja();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");

$cre_id = $_GET['cre_id'];
$cre_tip = $_GET['cre_tip'];

if ($cre_tip == 2) {
    require_once("../creditoasiveh/Creditoasiveh.class.php");
    $oCredito = new Creditoasiveh();
    $cre_cod = 'CAV-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    $credito_nom = 'Crédito ASIVEH';
}
if ($cre_tip == 3) {
    require_once("../creditogarveh/Creditogarveh.class.php");
    $oCredito = new Creditogarveh();
    $cre_cod = 'CGV-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    $credito_nom = 'Crédito GARVEH';
}
if ($cre_tip == 4) {
    require_once("../creditohipo/Creditohipo.class.php");
    $oCredito = new Creditohipo();
    $cre_cod = 'CH-' . str_pad($cre_id, 4, "0", STR_PAD_LEFT);
    $credito_nom = 'Crédito HIPOTECARIO';
}

$dts = $oCredito->mostrarUno($cre_id);
if ($dts['estado']) {
    $cli_nom = $dts['data']['tb_cliente_nom'];
    if ($cre_tip == 2)
        $cre_subtip = $dts['data']['tb_credito_tip1'];
    else
        $cre_subtip = $dts['data']['tb_credito_tip'];
}
$dts = null;

$cre_subtip_nom = 'DESCONOCIDO';
if ($cre_subtip == 1 && $cre_tip == 2)
    $cre_subtip_nom = 'VENTA NUEVA';
if ($cre_subtip == 1 && $cre_tip != 2)
    $cre_subtip_nom = 'REGULAR';
if ($cre_subtip == 4)
    $cre_subtip_nom = 'RE-VENTA';

if ($cre_subtip == 2)
    $cre_subtip_nom = 'ADENDA';
if ($cre_subtip == 3)
    $cre_subtip_nom = 'ACUERDO DE PAGO';


$nombre_archivo = "Histórico de Caja " . $credito_nom;

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("srjuan54@gmail.com")
        ->setLastModifiedBy("srjuan54@gmail.com")
        ->setTitle("Reporte")
        ->setSubject("Reporte")
        ->setDescription("Reporte generado por srjuan54@gmail.com")
        ->setKeywords("")
        ->setCategory("reporte excel");

$estiloTituloColumnas = array(
    'font' => array(
        'name' => 'Arial',
        'bold' => true,
        'size' => 8,
        'color' => array(
            'rgb' => '000000'
        )
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array(
            'rgb' => 'FAFAFA')
    ),
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array(
                'rgb' => '143860'
            )
        )
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => TRUE
    )
);

//$objPHPExcel->createSheet(); //solo se crea una nueva partir de la segunda
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//establecer impresion a pagina completa
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

$c = 1;
$titulo = "Cliente:";
$objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
$objPHPExcel->getActiveSheet()->mergeCells("B$c:G$c");
$objPHPExcel->getActiveSheet()->setCellValue("B$c", $cli_nom);

$c = 2;
$titulo = "Crédito:";
$objPHPExcel->getActiveSheet()->setCellValue("A$c", $titulo);
$objPHPExcel->getActiveSheet()->mergeCells("B$c:G$c");
$objPHPExcel->getActiveSheet()->setCellValue("B$c", 'Crédito ' . $cre_cod . ' ' . $cre_subtip_nom);

$c = $c + 1;

$titulosColumnas = array(
    'FECHA CUOTA',
    'FECHA PAGO',
    'CUOTA',
    'PAGADO',
    'SALDO',
    'DEBE',
    'OBSERVACIONES'
);

$objPHPExcel->getActiveSheet()
        ->setCellValue('A' . $c, $titulosColumnas[0])
        ->setCellValue('B' . $c, $titulosColumnas[1])
        ->setCellValue('C' . $c, $titulosColumnas[2])
        ->setCellValue('D' . $c, $titulosColumnas[3])
        ->setCellValue('E' . $c, $titulosColumnas[4])
        ->setCellValue('F' . $c, $titulosColumnas[5])
        ->setCellValue('G' . $c, $titulosColumnas[6])
;

$objPHPExcel->getActiveSheet()->getStyle("A$c:G$c")->applyFromArray($estiloTituloColumnas);

$c = $c + 1;

$TOTAL_DEBE = 0;
$cond = '<=';
$fecha = date('d-m-Y');

//listamos todas las cuotas detalle
$dts = $oCuotadetalle->mostrar_cuotasdetalle_por_creditoid_condicion($cre_id, $cre_tip, $cond, fecha_mysql($fecha));
if ($dts['estado'] == 1) {
    foreach ($dts['data'] as $key => $dt) {
        $cuodet_fec = mostrar_fecha($dt['tb_cuotadetalle_fec']);
        $estado_ap = $dt['tb_cuotadetalle_estap']; //si es 1 es xq se paga con AP
        $numero_pagos = 0; //cuantos pagos se han hecho en una cuota
        if ($estado_ap == 1){
            $cuodet_fec = mostrar_fecha($dt['tb_cuotadetalle_fec']) . ' (AP)';
        }
        $dts1 = $oCuotapago->filtrar('2', $dt['tb_cuotadetalle_id']);
        if ($dts1['estado'] == 1) {
            $rows1 = count($dts1['data']);
            if ($rows1 > 0) {
                $ingresos_cuota = 0;
                foreach ($dts1['data'] as $key => $dt1) {
                    $fecha_pago = mostrar_fecha($dt1['tb_cuotapago_fec']);
                    $cuota_facturada = formato_moneda(floatval($dt['tb_cuotadetalle_cuo']) - $ingresos_cuota);
                    $monto_ingreso = 0;
                    $dts2 = $oCuotapago->mostrar_cuotapago_ingreso(30, $dt1['tb_cuotapago_id']);
                    if ($dts2['estado'] == 1) {
                        foreach ($dts2['data'] as $key => $dt2) {
                            $ingre_det = $dt2['tb_ingreso_det'];

                            $monto_ingreso += floatval($dt2['tb_ingreso_imp']);
                            $ingresos_cuota += $monto_ingreso;

                            $arr_det = explode('|', $ingre_det);
                            if (count($arr_det) > 1) {
                                $pos = strpos($arr_det[1], 'ACUERDO');
                                if ($pos !== false)
                                    $observacion .= formato_moneda($dt2['tb_ingreso_imp']) . ' -> ' . $arr_det[1] . ' | ';
                                $pos = strpos($arr_det[1], 'CON CAJA');
                                if ($pos !== false)
                                    $observacion .= formato_moneda($dt2['tb_ingreso_imp']) . ' -> ' . $arr_det[1] . ' | ';
                            } else {
                                $observacion .= formato_moneda($dt1['tb_cuotapago_mon']) . ' -> ' . 'Pago físico | ';
                            }
                        }
                    }
                    $dts2 = null;

                    $saldo_favor = 0;
                    $saldo_contra = 0;
                    $dts2 = $oCuotapago->mostrar_clientecaja($dt1['tb_cuotapago_id']);
                    if ($dts2['estado'] == 1) {
                        foreach ($dts2['data'] as $key => $dt2) {
                            if ($dt2['tb_clientecaja_tip'] == 1) {
                                $saldo_favor += floatval($dt2['tb_clientecaja_mon']);
                            } else {
                                $saldo_contra -= floatval($dt2['tb_clientecaja_mon']);
                            }
                        }
                    }
                    $dts2 = null;

                    $monto_pagado = formato_moneda($monto_ingreso + $saldo_favor);
                    $saldo = $monto_pagado - $cuota_facturada;
                    $monto_debe = $cuota_facturada - $monto_pagado;

                    if ($saldo < 0)
                        $saldo = 0;
                    if ($monto_debe < 0)
                        $monto_debe = 0;

                    if ($numero_pagos > 0)
                        $cuodet_fec = '';

                    $objPHPExcel->getActiveSheet(0)
                            ->setCellValue('A' . $c, $cuodet_fec)
                            ->setCellValue('B' . $c, $fecha_pago)
                            ->setCellValue('C' . $c, $cuota_facturada)
                            ->setCellValue('D' . $c, $monto_pagado)
                            ->setCellValue('E' . $c, $saldo)
                            ->setCellValue('F' . $c, ($monto_debe + $TOTAL_DEBE))
                            ->setCellValue('G' . $c, $observacion)
                    ;

                    $objPHPExcel->getActiveSheet(0)->getStyle('C' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                    $objPHPExcel->getActiveSheet(0)->getStyle('D' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                    $objPHPExcel->getActiveSheet(0)->getStyle('E' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                    $objPHPExcel->getActiveSheet(0)->getStyle('F' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

                    if ($estado_ap == 1 && $cuodet_fec != '')
                        cellColor("A$c", '30F908'); //color verde

                    $observacion = '';
                    $numero_pagos++;
                    $c++;
                }
                $TOTAL_DEBE += $monto_debe;
            } else {
                $TOTAL_DEBE += formato_moneda(floatval($dt['tb_cuotadetalle_cuo']));
                $objPHPExcel->getActiveSheet(0)
                        ->setCellValue('A' . $c, $cuodet_fec)
                        ->setCellValue('B' . $c, '')
                        ->setCellValue('C' . $c, formato_moneda($dt['tb_cuotadetalle_cuo']))
                        ->setCellValue('D' . $c, 0)
                        ->setCellValue('E' . $c, 0)
                        ->setCellValue('F' . $c, $TOTAL_DEBE)
                        ->setCellValue('G' . $c, '')
                ;

                $objPHPExcel->getActiveSheet(0)->getStyle('C' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                $objPHPExcel->getActiveSheet(0)->getStyle('D' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                $objPHPExcel->getActiveSheet(0)->getStyle('E' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                $objPHPExcel->getActiveSheet(0)->getStyle('F' . $c)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

                if ($estado_ap == 1 && $cuodet_fec != '')
                    cellColor("A$c", '30F908'); //color verde

                $c++;
            }
        }
        $dts1 = null;
    }
}
$dts = null;

for ($z = 'A'; $z <= 'G'; $z++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($z)->setAutoSize(TRUE);
}

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);

$objPHPExcel->getActiveSheet()->getStyle(
        'A3:' .
        $objPHPExcel->getActiveSheet()->getHighestColumn() .
        $objPHPExcel->getActiveSheet()->getHighestRow()
)->applyFromArray($styleArray);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Histórico de Caja');

//-------------------------------------------------------------------------------------------------------------------------------------------
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

function cellColor($cells, $color) {
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $nombre_archivo . '.csv"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
