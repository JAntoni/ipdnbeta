<?php
require_once('Caja.class.php');
$oCaja = new Caja();

$caja_id = (empty($caja_id))? 0 : $caja_id; //esta variable puede ser definida en un archivo en donde se haga un require_once o un include

$caja = '<option value="0">-</option>';

//PRIMER NIVEL
$result = $oCaja->listar_cajas();
	if($result['estado'] == 1)
	{
	  foreach ($result['data'] as $key => $value)
	  {
	  	$selected = '';
	  	if($caja_id == $value['tb_caja_id'])
	  		$selected = 'selected';

	    $caja .= '<option value="'.$value['tb_caja_id'].'" '.$selected.'>'.$value['tb_caja_nom'].'</option>';
	  }
	}
$result = NULL;
//FIN PRIMER NIVEL
echo $caja;
?>