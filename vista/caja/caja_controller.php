<?php
	session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../caja/Caja.class.php');
  $oCaja = new Caja();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$caja_nom = mb_strtoupper($_POST['txt_caja_nom'], 'UTF-8');
 		$caja_des = $_POST['txt_caja_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar la Caja.';
 		if($oCaja->insertar($caja_nom, $caja_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Caja registrada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$caja_id = intval($_POST['hdd_caja_id']);
 		$caja_nom = mb_strtoupper($_POST['txt_caja_nom'], 'UTF-8');
 		$caja_des = $_POST['txt_caja_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar la Caja.';

 		if($oCaja->modificar($caja_id, $caja_nom, $caja_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Caja modificada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$caja_id = intval($_POST['hdd_caja_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar la Caja.';

 		if($oCaja->eliminar($caja_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Caja eliminada correctamente.';
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>