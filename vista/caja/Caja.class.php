<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Caja extends Conexion{

    function insertar($caja_nom, $caja_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_caja(tb_caja_xac, tb_caja_nom, tb_caja_des)
          VALUES (1, :caja_nom, :caja_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":caja_nom", $caja_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":caja_des", $caja_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($caja_id, $caja_nom, $caja_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_caja SET tb_caja_nom =:caja_nom, tb_caja_des =:caja_des WHERE tb_caja_id =:caja_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":caja_id", $caja_id, PDO::PARAM_INT);
        $sentencia->bindParam(":caja_nom", $caja_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":caja_des", $caja_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($caja_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_caja WHERE tb_caja_id =:caja_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":caja_id", $caja_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($caja_id){
      try {
        $sql = "SELECT * FROM tb_caja WHERE tb_caja_id =:caja_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":caja_id", $caja_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_cajas(){
      try {
        $sql = "SELECT * FROM tb_caja where tb_caja_xac=1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
