
$(document).ready(function(){
  $('#form_caja').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"caja/caja_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_caja").serialize(),
				beforeSend: function() {
					$('#caja_mensaje').show(400);
					$('#btn_guardar_caja').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#caja_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#caja_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		caja_tabla();
		      		$('#modal_registro_caja').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#caja_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#caja_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_caja').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#caja_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#caja_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_caja_nom: {
				required: true,
				minlength: 2
			},
			txt_caja_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_caja_nom: {
				required: "Ingrese un nombre para la Caja",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_caja_des: {
				required: "Ingrese una descripción para la Caja",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
