<?php
  session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../creditogarvehfile/Creditogarvehfile.class.php');
        $oCreditogarvehfile = new Creditogarvehfile();

 	$action = $_POST['action'];
 	$cre_fil_id_eliminar = $_POST['crefil_id'];

 	if($action == 'insertar'){

    if (!empty($_FILES)){
      $creditogarveh_id = $_POST['creditogarveh_id'];
      $directorio = 'public/images/creditogarveh/';
      $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetFile = $directorio . $_FILES['Filedata']['name'];
      $fileParts = pathinfo($_FILES['Filedata']['name']);

      if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
        //move_uploaded_file($tempFile, $targetFile); insertamos las imágenes
        $oCreditogarvehfile->credito_id = $creditogarveh_id;
        $oCreditogarvehfile->creditogarvehfile_his = 'Archivo subido por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a');

        $oCreditogarvehfile->tempFile = $tempFile; //temp servicio de PHP
        $oCreditogarvehfile->directorio = $directorio; //nombre con el que irá guardado en la carpeta
        $oCreditogarvehfile->fileParts = $fileParts; //extensiones de las imágenes

        $return = $oCreditogarvehfile->insertar();
        echo trim($return);
      } else {
        // The file type wasn't allowed
        echo 'Tipo de archivo no válido.';
      }
    }
    else{
      echo 'No hay ningún archivo para subir';
    }
 	}

 	elseif($action == 'modificar'){
    $creditogarvehfile_id = intval($_POST['hdd_creditogarvehfile_id']);
    $oCreditogarvehfile->creditogarvehfile_id = $creditogarvehfile_id;

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Vehículo.';

 		if($ocreditogarvehfile->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'creditogarvehfile modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$creditogarvehfile_id = intval($cre_fil_id_eliminar);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al creditogarvehfile.';

 		if($oCreditogarvehfile->eliminar($creditogarvehfile_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Imagen de garantía eliminado correctamente.';
 		}

 		echo json_encode($data);
 	}
 	else{
	 	echo 'No se ha identificado ningún tipo de transacción para '.$action;
 	}
  
?>