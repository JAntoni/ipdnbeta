<?php
require_once('../../core/usuario_sesion.php');
require_once ("Creditogarvehfile.class.php");
$oCreditogarvehfile = new Creditogarvehfile();

?>
<table class="table table-hover" cellspacing="1" id="tabla_catalogo_imagen">
    <thead>
        <tr>
            <th></th>              
        </tr>
    </thead>
<?php
?>  
    <tbody>
	
        <tr  class="tabla_filaTabla">
            <?php
            $dts=$oCreditogarvehfile->filtrar($_POST['cre_id']);
            if($dts['estado']==1){            
            foreach ($dts['data'] as $key => $dt) {
                $servidor = '';
                if($dt['tb_creditogarvehfile_id']<2734){
                    $servidor = 'https://www.ipdnsac.com/app/';
                }
                $url = ucfirst(mb_substr($dt['tb_creditogarvehfile_url'],6,null, 'UTF-8')) ;
                    $url = strtolower($url);
                
            ?>
            <td id="tabla_fila">
                <a class="group1" href="<?php echo $servidor.$url;?>"><img data-u="image" src="<?php echo $servidor.$url;?>" alt="" width="80" height="70"></a>
                <br>
                <br>
                <a class="btn btn-primary btn-sm" onClick="credito_file_eliminar(<?php echo $dt['tb_creditogarvehfile_id']?>)">Eliminar</a>
            </td>
            <?php
            }
            $dts=null;
            ?>
        </tr>

    </tbody>
	<?php
}
?>
</table>