<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Proveedor extends Conexion {

    function insertar($proveedor_tip, $proveedor_nom, $proveedor_doc, $proveedor_dir, $proveedor_con, $proveedor_tel, $proveedor_ema) {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_proveedor(tb_proveedor_tip, tb_proveedor_nom, tb_proveedor_doc, tb_proveedor_dir, tb_proveedor_con, tb_proveedor_tel, tb_proveedor_ema)
          VALUES (:proveedor_tip, :proveedor_nom, :proveedor_doc, :proveedor_dir, :proveedor_con, :proveedor_tel, :proveedor_ema)";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_tip", $proveedor_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_nom", $proveedor_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_doc", $proveedor_doc, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_dir", $proveedor_dir, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_con", $proveedor_con, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_tel", $proveedor_tel, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_ema", $proveedor_ema, PDO::PARAM_STR);

            $result['estado'] = $sentencia->execute();
            $result['nuevo'] = $this->dblink->lastInsertId();

            $this->dblink->commit();

            return $result; //si es correcto el ingreso retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function modificar($proveedor_id, $proveedor_tip, $proveedor_nom, $proveedor_doc, $proveedor_dir, $proveedor_con, $proveedor_tel, $proveedor_ema) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_proveedor 
          SET tb_proveedor_tip =:proveedor_tip, tb_proveedor_nom =:proveedor_nom, 
          tb_proveedor_doc =:proveedor_doc, tb_proveedor_dir =:proveedor_dir, 
          tb_proveedor_con =:proveedor_con, tb_proveedor_tel =:proveedor_tel, 
          tb_proveedor_ema =:proveedor_ema WHERE tb_proveedor_id =:proveedor_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_tip", $proveedor_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":proveedor_nom", $proveedor_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_doc", $proveedor_doc, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_dir", $proveedor_dir, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_con", $proveedor_con, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_tel", $proveedor_tel, PDO::PARAM_STR);
            $sentencia->bindParam(":proveedor_ema", $proveedor_ema, PDO::PARAM_STR);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function eliminar($proveedor_id) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_proveedor WHERE tb_proveedor_id =:proveedor_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    function mostrarUno($proveedor_id) {
        try {
            $sql = "SELECT * FROM tb_proveedor WHERE tb_proveedor_id =:proveedor_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":proveedor_id", $proveedor_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function listar_proveedores() {
        try {
            $sql = "SELECT * FROM tb_proveedor";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function Proveedor_autocomplete($dato) {
        try {

            $filtro = "%" . $dato . "%";

            $sql = "SELECT *
		FROM tb_proveedor 
                WHERE tb_proveedor_nom LIKE :filtro OR tb_proveedor_doc LIKE :filtro";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay Proveedores registrados";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    function mostrar_por_dni($dni) {
        try {
            $sql = "SELECT * FROM tb_proveedor where tb_proveedor_doc =:tb_proveedor_doc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_proveedor_doc", $dni, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No se encontro ningún proveedor con el número de documento ingresado";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

}

?>
