<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Garantiafile extends Conexion{
    public $garantiafile_id;

    public $garantiafile_xac = 1;
    public $garantia_id;
    public $garantiafile_url = '...';
    public $garantiafile_his;

    public $tempFile; //temp servicio de PHP
    public $directorio; //nombre con el que irá guardado en la carpeta
    public $fileParts; //extensiones de las imágenes

    function insertar(){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_garantiafile(tb_garantiafile_xac, tb_garantia_id, tb_garantiafile_url, tb_garantiafile_his) 
                VALUES(:garantiafile_xac, :garantia_id, :garantiafile_url, :garantiafile_his)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiafile_xac", $this->garantiafile_xac, PDO::PARAM_INT);
        $sentencia->bindParam(":garantia_id", $this->garantia_id, PDO::PARAM_INT);
        $sentencia->bindParam(":garantiafile_url", $this->garantiafile_url, PDO::PARAM_STR);
        $sentencia->bindParam(":garantiafile_his", $this->garantiafile_his, PDO::PARAM_STR);

        $result1 = $sentencia->execute();
        $this->garantiafile_id = $this->dblink->lastInsertId();
        $this->dblink->commit();

        $nombre_imagen = 'garantia_'.$this->garantia_id.'_garantiafile_'. $this->garantiafile_id .'.'. $this->fileParts['extension'];
        $garantiafile_url = $this->directorio . strtolower($nombre_imagen);

        if(move_uploaded_file($this->tempFile, '../../'. $garantiafile_url)){
          //insertamos las imágenes
          $sql = "UPDATE tb_garantiafile SET tb_garantiafile_url =:garantiafile_url WHERE tb_garantiafile_id =:garantiafile_id";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":garantiafile_id", $this->garantiafile_id, PDO::PARAM_INT);
          $sentencia->bindParam(":garantiafile_url", $garantiafile_url, PDO::PARAM_STR);
          $result2 = $sentencia->execute();
        }
        else{
          $this->eliminar($this->garantiafile_id);
        }

        if($result1 == 1)
          return 'exito';
        else
          return 'Erro al insertar en Garantia File';
      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
  
    function eliminar($garantiafile_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_garantiafile WHERE tb_garantiafile_id =:garantiafile_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiafile_id", $garantiafile_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($garantiafile_id){
      try {
        $sql = "SELECT * FROM tb_garantiafile WHERE tb_garantiafile_id =:garantiafile_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantiafile_id", $garantiafile_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function listar_garantiafiles($garantia_id){
      try {
        $sql = "SELECT * FROM tb_garantiafile where tb_garantia_id =:garantia_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":garantia_id", $garantia_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay imágenes subidas";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
