<?php
  session_name("ipdnsac");
  session_start();
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../garantiafile/Garantiafile.class.php');
  $oGarantiafile = new Garantiafile();

 	$action = $_POST['action'];

 	if($action == 'insertar'){

    if (!empty($_FILES)){
      $garantia_id = $_POST['garantia_id'];
      $directorio = 'public/images/garantia/';
      $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions

      $tempFile = $_FILES['Filedata']['tmp_name'];
      $targetFile = $directorio . $_FILES['Filedata']['name'];
      $fileParts = pathinfo($_FILES['Filedata']['name']);

      if (in_array(strtolower($fileParts['extension']), $fileTypes)) {
        //move_uploaded_file($tempFile, $targetFile); insertamos las imágenes
        $oGarantiafile->garantia_id = $garantia_id;
        $oGarantiafile->garantiafile_his = 'Archivo subido por: <b>'.$_SESSION['usuario_nom'].'</b> | '.date('d-m-Y h:i a');

        $oGarantiafile->tempFile = $tempFile; //temp servicio de PHP
        $oGarantiafile->directorio = $directorio; //nombre con el que irá guardado en la carpeta
        $oGarantiafile->fileParts = $fileParts; //extensiones de las imágenes

        $return = $oGarantiafile->insertar();
        echo trim($return);
      } else {
        // The file type wasn't allowed
        echo 'Tipo de archivo no válido.';
      }
    }
    else{
      echo 'No hay ningún archivo para subir';
    }
 	}

 	elseif($action == 'modificar'){
    $garantiafile_id = intval($_POST['hdd_garantiafile_id']);
    $oGarantiafile->garantiafile_id = $garantiafile_id;

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Vehículo.';

 		if($ogarantiafile->modificar()){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'garantiafile modificado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$garantiafile_id = intval($_POST['hdd_garantiafile_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar al garantiafile.';

    $ruta_imagen = '';
    //? VAMOS A OBTENER LA URL DE LA IMAGEN PARA PODER ELIMINARLA POR COMPLETO DEL DIRECTORIO
    $result = $oGarantiafile->mostrarUno($garantiafile_id);
      if($result['estado'] == 1){
        $ruta_imagen = '../../'.$result['data']['tb_garantiafile_url'];
      }
    $result = NULL;

    if($ruta_imagen != ''){
      // Verificar si la imagen existe
      if (file_exists($ruta_imagen)) {
        // Intentar eliminar la imagen
        if (unlink($ruta_imagen)) {
          if($oGarantiafile->eliminar($garantiafile_id)){
            $data['estado'] = 1;
            $data['mensaje'] = 'Imagen de garantía eliminado correctamente.';
          }
        } else {
          $data['mensaje'] = 'Existe un error al eliminar al garantiafile, se rechaza la eliminacion de la imagen en el directorio.';
        }
      } else {
        $data['mensaje'] = "La imagen no existe en el directorio: ".$ruta_imagen;
      }
    }

 		echo json_encode($data);
 	}
 	else{
	 	echo 'No se ha identificado ningún tipo de transacción para '.$action;
 	}
  
?>