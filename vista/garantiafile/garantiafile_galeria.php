<?php
	session_name("ipdnsac");
  session_start();
  if(!isset($_SESSION['usuario_id'])){
    echo 'Terminó la sesión';
    exit();
  }
  if(defined('VISTA_URL')){
    require_once(VISTA_URL.'garantiafile/Garantiafile.class.php');
    require_once(VISTA_URL.'garantia/Garantia.class.php');
  }
  else{
    require_once('../garantiafile/Garantiafile.class.php');
    require_once('../garantia/Garantia.class.php');
  }
  $oGarantiafile = new Garantiafile();
  $oGarantia = new Garantia();

  $garantia_id = (isset($_POST['garantia_id']))? $_POST['garantia_id'] : intval($garantia_id); //esta variable puede ser definida en un archivo en donde se haga un require_once o un include
  $result = $oGarantia->garantia_credito($garantia_id);
    if($result['estado'] == 1){
      $credito_est = intval($result['data']['tb_credito_est']); // 1 pendiente, 2 aprobado, 3 vigente
    }
  $result = NULL;
  $result = $oGarantiafile->listar_garantiafiles($garantia_id);
?>
<style type="text/css">
	.garantia_img{
		cursor: pointer;
	}
</style>
<?php
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value): ?>
			<div class="col-xs-4 col-sm-4" style="text-align: center;" id="<?php echo 'garantiafileid_'.$value['tb_garantiafile_id'];?>">
			  <img class="img-responsive garantia_img" src="<?php echo $value['tb_garantiafile_url'];?>" alt="Photo" style="height:73px; width:120px;" onclick="carousel('garantiafile', <?php echo $value['tb_garantia_id'];?>)">
        <?php if($credito_est == 1 || $_SESSION['usuario_id'] == 2):?>
			    <a class="users-list-name" href="javascript:void(0)" onclick="garantiafile_form('E', 0, <?php echo $value['tb_garantiafile_id'];?>)">Eliminar</a><br>
        <?php endif;?>
        <?php if($credito_est > 1):?>
          <br>
        <?php endif;?>
			</div>
			<?php 
		endforeach;
	}
	else{ ?>
    <div class="col-md-12">
      <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
        <h4>
          <i class="icon fa fa-info"></i>Mensaje
        </h4>
        Esta garantía no tiene fotos subidas
      </div>
    </div>
    <?php
	}
?>