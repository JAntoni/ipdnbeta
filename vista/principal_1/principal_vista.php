<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
    echo 'Terminó la sesión';
    exit();
}
//
require_once('vista/asistencia/Asistencia.class.php');
$oAsistencia = new Asistencia();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Principal
            <small>Bienvenido al sistema</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="./principal"><i class="fa fa-dashboard"></i> Principal</a></li>
            <!--li class="active">Dashboard</li-->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class=" col-xs-12 col-sm-12 col-md-6 col-lg-4">	
                <!-- /.info-box -->
                <div class="box  box-material">
                    <div class="box-header with-border">
                        <h3 class="box-title">Marcar Asistencia</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="padding: 0px">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="info-box bg-gray" style="margin-bottom: 0px; border-radius: 0px ">
                                    <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">CHICLAYO</span>
                                        <span class="info-box-number"><?php
                                            $fecha = date("j/n/Y");
                                            echo $fecha;
                                            ?>
                                        </span>

                                        <div class="progress">
                                            <div class="progress-bar" style="width: 70%"></div>
                                        </div>
                                        <span class="progress-description">
                                            Actividades
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>		
                            </div>
                            <!-- /.col -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <ul class="nav nav-pills nav-stacked" style="padding-bottom: 15px">
                            <li><a href="#">Turno mañana
                                    <span class="pull-right"><h class="text-green">&nbsp;9:00 a. m.</h>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<h class="text-red">1:00 p. m.</h></span></a></li>
                            <li><a href="#">Receso <span class="pull-right"><h class="">&nbsp;1:00 p. m.&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;3:00 p. m.</h></span></a>
                            </li>
                            <li><a href="#">Turno tarde
                                    <span class="pull-right "><h class="text-green">&nbsp;3:00 p. m.</h>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<h class="text-red">7:00 p. m.</h></span></a></li> <br>



                            <?php
                            $codigo = date('d-m-y'); //codigo unico por día, viene a ser la fecha dia-mes-año
                            $columna = ''; //guardará la columna que está vacía para insertar ya sea ingreso o salida
                            $disable1="";
                            $disable2="";
                            $dts = $oAsistencia->listar_asistencia_codigo($codigo, $_SESSION['usuario_id']);

                            if($dts['estado']==1){
                                $ing1 = $dts['data']['tb_asistencia_ing1']; //ingreso turno mañana
                                $sal1 = $dts['data']['tb_asistencia_sal1']; //salida turno mañana

                                $ing2 = $dts['data']['tb_asistencia_ing2']; //ingreso turno tarde
                                $sal2 = $dts['data']['tb_asistencia_sal2']; //salida turno tarde

                                $array = array("ing1" => $ing1, "sal1" => $sal1, "ing2" => $ing2, "sal2" => $sal2);
                                $disable2="disabled=''";
                                $disable1="";
                            }

                            if ($dts['estado'] == 0) {
                               $disable1="disabled";
                               $disable2="";
                            } else {
                                foreach ($array as $key => $value) {
                                    if (empty($value) || $value == '00:00:00') {
                                        $columna = $key;
                                        break;
                                    }
                                }
                                $tipo = 'MARCAR INGRESO';
                                $turno = 'Marque su Ingreso Por Favor';
                                if ($columna == 'sal1' || $columna == 'sal2') {
                                    $tipo = 'MARCAR SALIDA';
                                    $turno = 'Marque su Salida Por Favor';
                                }
                                
                            }
                            ?>

                            <div class="  col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="">	
                                    <button type="button" class="btn btn-block btn-success btn-lg" id="btn_asi_ing_registrar" href="javascript:void(0)" onclick="marcar_asistencia('actualizar','<?php echo $columna?>')"><i class="fa fa-fw fa-sign-in"></i><?php echo $tipo;?></button>
                                </div>
                            </div>
<!--                            <div class=" col-xs-12 col-sm-6 col-md-6 col-lg-6">	
                                <div class="">
                                    <button type="button" class="btn btn-block btn-danger btn-lg" id="btn_asi_sal_registrar" href="javascript:void(0)" onclick="marcar_asistencia('actualizar','<?php echo $columna?>')" <?php echo $disable1 ;?>><i class="fa fa-fw fa-sign-out"></i><?php echo $tipo;?></button>
                                </div>
                            </div>-->
                        </ul>
                    </div>
                    <!-- /.footer -->

                </div>
                <!-- /.box -->
                <div class="row">
                    <br>
                    <div class="col-md-12 col-xs-12">
                        <div class="callout callout-info" id="prestamo_mensaje">
                            <h4>
                                <!--<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>--> 
                                Fecha y hora de hoy: <?php echo date('d-m-Y h:i a') . ' / ' . date('P'); ?></h4>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>
</div>
