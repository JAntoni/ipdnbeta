<?php
	session_name("ipdnsac");
	session_start();
	if(!isset($_SESSION['usuario_id'])){
		echo 'Terminó la sesión';
		exit();
	}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Principal
			<small>Bienvenido al sistema</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="./principal"><i class="fa fa-dashboard"></i> Principal</a></li>
			<!--li class="active">Dashboard</li-->
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">

		   <div class="row">
			    	<div class=" col-xs-12 col-sm-12 col-md-6 col-lg-4">	
						    	          <!-- /.info-box -->
				          <div class="box  box-material">
				            <div class="box-header with-border">
				              <h3 class="box-title">Marcar Asistencia</h3>
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body" style="padding: 0px">
				              <div class="row">
				                <div class="col-md-12">
														<div class="info-box bg-gray" style="margin-bottom: 0px; border-radius: 0px ">
									 	           <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

									            <div class="info-box-content">
									              <span class="info-box-text">CHICLAYO</span>
									              <span class="info-box-number"><?php $fecha= date ("j/n/Y");echo $fecha;?></span>

									              <div class="progress">
									                <div class="progress-bar" style="width: 70%"></div>
									              </div>
									                  <span class="progress-description">
									                    Actividades
									                  </span>
									            </div>
									            <!-- /.info-box-content -->
									          </div>		
				                </div>
				                <!-- /.col -->

				              </div>
				              <!-- /.row -->
				            </div>
				            <!-- /.box-body -->
				            <div class="box-footer no-padding">
				              <ul class="nav nav-pills nav-stacked" style="padding-bottom: 15px">
				                <li><a href="#">Turno mañana
				                  <span class="pull-right"><h class="text-green">&nbsp;9:00 a. m.</h>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<h class="text-red">1:00 p. m.</h></span></a></li>
				                <li><a href="#">Receso <span class="pull-right"><h class="">&nbsp;1:00 p. m.&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;3:00 p. m.</h></span></a>
				                </li>
				                <li><a href="#">Turno tarde
				                  <span class="pull-right "><h class="text-green">&nbsp;3:00 p. m.</h>&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;<h class="text-red">7:00 p. m.</h></span></a></li> <br>

						              <div class="  col-xs-12 col-sm-6 col-md-6 col-lg-6">
						                <div class="clearfix js-sweetalert">	
						            				<button type="button" class="btn btn-block btn-success btn-lg btn-material waves-effect" id="btn_asi_ing_registrar" href="#ins" onclick="asistencia_registrar('')" style="" data-type="success"><i class="fa fa-fw fa-sign-in"></i>Iniciar</button>
														</div>
													</div>
													<div class=" col-xs-12 col-sm-6 col-md-6 col-lg-6">	
						          					<button type="button" class="btn btn-block btn-danger btn-lg btn-material waves-effect" id="btn_asi_sal_registrar" href="#ins" onclick="asistencia_registrar('')"><i class="fa fa-fw fa-sign-out"></i>Terminar</button>
													</div>

													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

	                        <div class="body">
	                            <div class="row clearfix js-sweetalert">
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>A basic message</p>
	                                    <button class="btn btn-primary waves-effect" data-type="basic">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>A title with a text under</p>
	                                    <button class="btn btn-primary waves-effect" data-type="with-title">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>A success message!</p>
	                                    <button class="btn btn-primary waves-effect" data-type="success" >CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>A warning message, with a function attached to the <b>Confirm</b> button...</p>
	                                    <button class="btn btn-primary waves-effect" data-type="confirm">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>... and by passing a parameter, you can execute something else for <b>Cancel</b>.</p>
	                                    <button class="btn btn-primary waves-effect" data-type="cancel">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>A message with a custom icon</p>
	                                    <button class="btn btn-primary waves-effect" data-type="with-custom-icon">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>An HTML message</p>
	                                    <button class="btn btn-primary waves-effect" data-type="html-message">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>A message with auto close timer</p>
	                                    <button class="btn btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>A replacement for the <b>prompt</b> function</p>
	                                    <button class="btn btn-primary waves-effect" data-type="prompt">CLICK ME</button>
	                                </div>
	                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                                    <p>With a loader (for AJAX request for example)</p>
	                                    <button class="btn btn-primary waves-effect" data-type="ajax-loader">CLICK ME</button>
	                                </div>
	                            </div>
	                        </div>
                    
                				</div>
				              </ul>
				            </div>
				            <!-- /.footer -->
		
				          </div>
				          <!-- /.box -->
			    	</div>


			 </div>

	</section>
</div>
