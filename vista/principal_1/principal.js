$(document).ready(function() {
	jQuery('#wpf-loader-two').delay(200).fadeOut('slow');
//
//	console.log('corriendo');
	
});


function marcar_asistencia(act, tip){
    $.ajax({
      type: "POST",
      url: VISTA_URL+"asistencia/asistencia_reg.php",
      async:true,
      dataType: "json",                      
      data: ({
        action: act,
        tipo: tip
      }),
      beforeSend: function() {
//        $('#div_mensaje').text('Guardando asistencia...');
      },
      success: function(data){
        if(parseInt(data.estado) == 1){
                swal_success("ASISTENCIA",data.msj,3000);
                setInterval(location.reload(),5000);
        }
        else
                swal_error("SISTEMA",data.msj,5000);
      },
      complete: function(data){
            console.log(data);
        if(data.status != 200){
//          console.log(data);
          $('#div_mensaje').html('Error al guardar, repóterlo de inmediato: '+data.responseText);
          swal_error("SISTEMA",data.responseText,5000);
        }
      }
    });
  }