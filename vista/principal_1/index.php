<?php
	session_name("ipdnsac");
	session_start();
	if(!isset($_SESSION['usuario_id'])){
		echo 'Terminó la sesión';
		exit();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php include(VISTA_URL.'templates/head.php'); ?>
		<title>IPDN | Inicio</title>
    
	</head>
	<body <?php echo 'class="'.CLASE_BODY.' '.$usuario_tem.'"'; ?>>


    <div id="wpf-loader-two"  >
      <div class="wpf-loader-two-inner" style="color: #f4f4f4; font-family: verdana;font-size: 7pt;margin: 10px 0 0;    text-align: center; border: 2px;border-radius: 50%;height: 100px;left: 46%;position: absolute;top: 40%;width: 100px;text-align: center;" >    
        <img class="img-responsive fa-spin" src="public/images/logopres1.png" alt="Photo" style="width: 60px; height: 60px">
      </div>
    </div>
		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php include(VISTA_URL.'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php include(VISTA_URL.'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO -->
				<?php include('principal_vista.php'); ?>
			<!-- INCLUIR FOOTER -->
				<?php include(VISTA_URL.'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php include(VISTA_URL.'templates/script.php'); ?>

		<script type="text/javascript" src="<?php echo VISTA_URL.'principal/principal.js';?>"></script>    
	</body>
</html>
