<?php
	require_once('../../core/usuario_sesion.php');
	require_once('../permiso/Permiso.class.php');
 	$oPermiso = new Permiso();
 	require_once('../usuariogrupo/UsuarioGrupo.class.php');
  $oUsuarioGrupo = new UsuarioGrupo();

 	$action = $_POST['action'];

 	if($action == 'insertar'){
 		$usuariogrupo_nom = mb_strtoupper($_POST['txt_usuariogrupo_nom'], 'UTF-8');
 		$usuariogrupo_des = $_POST['txt_usuariogrupo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al guardar el Usuario Grupo.';
 		if($oUsuarioGrupo->insertar($usuariogrupo_nom, $usuariogrupo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo registrado correctamente.';
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'modificar'){
 		$usuariogrupo_id = intval($_POST['hdd_usuariogrupo_id']);
 		$usuariogrupo_nom = mb_strtoupper($_POST['txt_usuariogrupo_nom'], 'UTF-8');
 		$usuariogrupo_des = $_POST['txt_usuariogrupo_des'];

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al modificar el Usuario Grupo.';

 		if($oUsuarioGrupo->modificar($usuariogrupo_id, $usuariogrupo_nom, $usuariogrupo_des)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo modificado correctamente. '.$usuariogrupo_des;
 		}

 		echo json_encode($data);
 	}

 	elseif($action == 'eliminar'){
 		$usuariogrupo_id = intval($_POST['hdd_usuariogrupo_id']);

 		$data['estado'] = 0;
 		$data['mensaje'] = 'Existe un error al eliminar el Usuario Grupo.';

 		if($oUsuarioGrupo->eliminar($usuariogrupo_id)){
 			$data['estado'] = 1;
 			$data['mensaje'] = 'Usuario Grupo eliminado correctamente. '.$usuariogrupo_des;
 		}

 		echo json_encode($data);
 	}

 	else{
 		$data['estado'] = 0;
	 	$data['mensaje'] = 'No se ha identificado ningún tipo de transacción para '.$action;

	 	echo json_encode($data);
 	}
  
?>