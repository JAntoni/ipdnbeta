<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class UsuarioGrupo extends Conexion{

    function insertar($usuariogrupo_nom, $usuariogrupo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_usuariogrupo(tb_usuariogrupo_xac, tb_usuariogrupo_nom, tb_usuariogrupo_des)
          VALUES (1, :usuariogrupo_nom, :usuariogrupo_des)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuariogrupo_nom", $usuariogrupo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":usuariogrupo_des", $usuariogrupo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($usuariogrupo_id, $usuariogrupo_nom, $usuariogrupo_des){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_usuariogrupo SET tb_usuariogrupo_nom =:usuariogrupo_nom, tb_usuariogrupo_des =:usuariogrupo_des WHERE tb_usuariogrupo_id =:usuariogrupo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuariogrupo_id", $usuariogrupo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":usuariogrupo_nom", $usuariogrupo_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":usuariogrupo_des", $usuariogrupo_des, PDO::PARAM_STR);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function eliminar($usuariogrupo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_usuariogrupo WHERE tb_usuariogrupo_id =:usuariogrupo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuariogrupo_id", $usuariogrupo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        echo 'Error!: '.$e->getMessage();
      }
    }
    function mostrarUno($usuariogrupo_id){
      try {
        $sql = "SELECT * FROM tb_usuariogrupo WHERE tb_usuariogrupo_id =:usuariogrupo_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":usuariogrupo_id", $usuariogrupo_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }
    }
    function listar_usuariogrupos(){
      try {
        $sql = "SELECT * FROM tb_usuariogrupo";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        echo 'Error!: '.$e->getMessage();
      }

    }
  }

?>
