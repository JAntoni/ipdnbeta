
$(document).ready(function(){
  $('#form_usuariogrupo').validate({
		submitHandler: function() {
	    $.ajax({
				type: "POST",
				url: VISTA_URL+"usuariogrupo/usuariogrupo_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_usuariogrupo").serialize(),
				beforeSend: function() {
					$('#usuariogrupo_mensaje').show(400);
					$('#btn_guardar_usuariogrupo').prop('disabled', true);
				},
				success: function(data){
					if(parseInt(data.estado) > 0){
						$('#usuariogrupo_mensaje').removeClass('callout-info').addClass('callout-success')
		      	$('#usuariogrupo_mensaje').html(data.mensaje);
		      	setTimeout(function(){ 
		      		usuariogrupo_tabla();
		      		$('#modal_registro_usuariogrupo').modal('hide');
		      		 }, 1000
		      	);
					}
					else{
		      	$('#usuariogrupo_mensaje').removeClass('callout-info').addClass('callout-warning')
		      	$('#usuariogrupo_mensaje').html('Alerta: ' + data.mensaje);
		      	$('#btn_guardar_usuariogrupo').prop('disabled', false);
		      }
				},
				complete: function(data){
					//console.log(data);
				},
				error: function(data){
					$('#usuariogrupo_mensaje').removeClass('callout-info').addClass('callout-danger')
	      	$('#usuariogrupo_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
	  },
	  rules: {
			txt_usuariogrupo_nom: {
				required: true,
				minlength: 2
			},
			txt_usuariogrupo_des: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			txt_usuariogrupo_nom: {
				required: "Ingrese un nombre para el Usuario Grupo",
				minlength: "Como mínimo el nombre debe tener 2 caracteres"
			},
			txt_usuariogrupo_des: {
				required: "Ingrese una descripción para el Usuario Grupo",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
