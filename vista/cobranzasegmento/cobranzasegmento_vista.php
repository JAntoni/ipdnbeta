<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Reporte</a></li>
			<li class="active"><?php echo ucwords(strtolower($menu_tit)); ?></li>
		</ol>
	</section>
	<style type="text/css">
		div.dataTables_wrapper div.dataTables_filter input {
			width: 90%;
			font-weight: bold;
		}

		div.dataTables_filter label {
			width: 80%;
		}
	</style>
	<!-- Main content -->
	<section class="content">

		<div class="box box-primary">
			<!-- <div class="box-header with-border">
				< ?php require_once('reportecreditomejorado_filtro.php'); ?>
			</div> -->
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="reportecredito_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> <span id="span_cargando">Cargando...</span></h4>
				</div>

				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs" style="font-family: cambria;font-weight: bold">
						<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" id="panel1">1 - 3 meses</a></li>
						<li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" id="panel2">3 - 9 meses</a></li>
						<li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" id="panel3">9 - 18 meses</a></li>
						<li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false" id="panel4">18 a más</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="row">
								<div class="col-md-12">
									<div id="lista_creditos01"> </div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab_2">
							3 - 9 meses
						</div>
						<div class="tab-pane" id="tab_3">
							9 - 18 meses
						</div>
						<div class="tab-pane" id="tab_4">
							18 a más
						</div>
					</div>
				</div>

				<!-- REPORTE FACTURADO ASISTENCIA VEHICULAR-->
				<div class="box box-success class_comportamiento">
					<div class="box-header with-border">
						<h3 class="box-title">COMPORTAMIENTO DE PAGO DE LOS CLIENTES</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<!-- TABLA PRINCIPAL DEL REPORTE MEJORADO -->
						<div class="table-responsive">
							<table id="tbl_comportamiento" class="table table-striped table-bordered dataTable display" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID CREDITO</th>
										<th>NOMBRES CLIENTE</th>
										<th>REPRESENTANTE</th>
										<th class="no-sort">DIRECCIÓN</th>
										<th>TIPO CREDITO</th>
										<th class="no-sort">CUOTA ACTUAL</th>
										<th>MONEDA</th>
										<th>CAPITAL DESEMBOLSADO</th>
										<th>CAPITAL RESTANTE</th>
										<th>RESTANTE EN S/.</th>
										<th>ESTADO CUOTA (TR)</th>
										<th>MONTO CUOTA</th>
										<th>MONTO CUOTA EN S/.</th>
										<th>CUOTA PAGO S/.</th>
										<th>FECHA DE CUOTA</th>
										<th>FEC. ULTIMO PAGO</th>
										<th>DIAS ATRASO</th>
										<th>INACTIVIDAD</th>
										<th>% ULTIMO PAGO</th>
										<th>DEUDA ACUMULADA S/.</th>
										<th>Opciones</th>
										<th>Resultado</th>
										<th class="no-sort">Comentarios</th>
										<th>Asesor</th>
										<th class="no-sort">Activades</th>
									</tr>
								</thead>
								<tbody id="lista_creditos"> </tbody>
							</table>
						</div>
						<!-- FIN REPORTE MEJORADO -->
					</div>
				</div>

				<?php
				$fecha = new DateTime();
				$fecha->modify('last day of this month');

				$chart_fec1 = date('01-m-Y');
				$chart_fec2 = $fecha->format('d-m-Y');
				?>

				<!-- GRÁFICO DEUDA ACUMULADA POR DIA -->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">DEUDA ACUMULADA DIARIA</h3>
						<form id="form_chart_filtro" class="form-inline" role="form">
							<div class="form-group">
								<div class='input-group date' id='datetimepicker1'>
									<input type='text' class="form-control input-sm" name="txt_deudadiaria_fec1" id="txt_deudadiaria_fec1" value="<?php echo $chart_fec1; ?>" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							<div class="form-group">
								<div class='input-group date' id='datetimepicker2'>
									<input type='text' class="form-control input-sm" name="txt_deudadiaria_fec2" id="txt_deudadiaria_fec2" value="<?php echo $chart_fec2; ?>" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							<div class="form-group" style="margin-left:9px">
								<a href="javascript:void(0)" onClick="valores_chart_deudadiaria()" class="btn btn-success btn-sm">Buscar</a>
							</div>
						</form>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div id="chart-deudadiaria" style="height: 400px;">

						</div>
					</div>
				</div>



				<!-- REPORTE DE ASISTENCIA VEHICULAR-->
				<div id="div_asiveh_tabla">
					<?php //require_once('reportecredito_asiveh_tabla.php');
					?>
				</div>

				<!-- REPORTE FACTURADO GARANTIA VEHICULAR-->
				<div id="div_garveh_tabla">
					<?php //require_once('reportecredito_garveh_tabla.php');
					?>
				</div>

				<!-- REPORTE FACTURADO GARANTIA HIPOTECARIO-->
				<div id="div_hipo_tabla">
					<?php //require_once('reportecredito_hipo_tabla.php');
					?>
				</div>

			</div>
			<div id="div_modal_opcionesgc_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISthO DEL TIPO-->
			</div>

			<div id="div_modal_opcionesgc_asignar_form">
			</div>

			<div id="div_modal_opcionesgc_timeline">

			</div>

			<div id="div_modal_cliente_contacto"></div>

			<div id="div_modal_deuda_acumulada"></div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>