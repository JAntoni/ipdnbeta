/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    console.log('Compra colaborador 04-09-2024')
    compracolaborador_tabla();
});


function compracolaborador_tabla() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "compracolaborador/compracolaborador_tabla.php",
        async: true,
        dataType: "html",
        data: ({
            usu_id: $('#cmb_usu_id').val(),
            mes: $('#cmb_pla_mes').val(),
            anio: $('#cmb_pla_anio').val()
        }),
        beforeSend: function () {
            $('#compracolaborador_mensaje_tbl').show(300);
        },
        success: function (data) {
            $('#div_compracolaborador_tabla').html(data);
            $('#compracolaborador_mensaje_tbl').hide(300);
        },
        complete: function (data) {

        },
        error: function (data) {
        }
    });
}

$("#cmb_usu_id,#cmb_pla_mes,#cmb_pla_anio").change(function () {
    compracolaborador_tabla();
});


function pagar_ventainterna(ven,pago_parcial) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "menorgarantia/ventainterna_pago_form.php", //formulario de pago de compra colaborador
        async: true,
        dataType: "html",
        data: ({
            venin_id: ven,
            vista: 'credito',
            tipo: 0,
            pago_parcial: pago_parcial
        }),
        beforeSend: function () {
        },
        success: function (html) {
            $('#div_compracolaborador_form').html(html);
            $('#modal_pagarventainterna_registro').modal('show');
            modal_hidden_bs_modal('modal_pagarventainterna_registro', 'limpiar'); //funcion encontrada en public/js/generales.js
            modal_width_auto('modal_pagarventainterna_registro', 25);
        },
        complete: function (data) {

        }
    });
}

function cmb_usu_id(mostrar)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/usuario_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
        }),
        beforeSend: function () {
            $('#cmb_usu_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_usu_id').html(html);
        }
    });
}

function compracolaborador_form() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "compracolaborador/compracolaborador_form.php",
        async: true,
        dataType: "html",
        data: ({

        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_compracolaborador_form').html(html);
            $('#modal_registro_ventainterna').modal('show');
            modal_width_auto('modal_registro_ventainterna', 20);
            modal_height_auto('modal_registro_ventainterna');
        },
        complete: function (data) {

        }
    });
}

function compracolaborador_his(id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "menorgarantia/menorgarantia_controller.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'ventainterna',
            venin_id: id
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_compracolaborador_his').html(html);
            $('#modal_registro_ventainterna_historial').modal('show');
            modal_width_auto('modal_registro_ventainterna_historial', 20);
            modal_height_auto('modal_registro_ventainterna_historial');
        },
        complete: function (data) {
            
        }
    });
}