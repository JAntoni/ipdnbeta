<?php

//date_default_timezone_set('America/Lima');
require_once('../../core/usuario_sesion.php');
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once('../menorgarantia/Ventainterna.class.php'); //el registro de la venta interna debe hacerlo desde la lista de remates
$oVentainterna = new Ventainterna();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();
require_once('../garantia/Garantia.class.php');
$oGarantia = new Garantia();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$action = $_POST['action'];
$venin_id = $_POST['venin_id'];

$fecha_hoy = date('d-m-Y');

if ($action == 'insertar') {
    
    $colabo_id = intval($_POST['cmb_colaborador_id']);
    $array_cuo = $_POST['txt_venta_cuo'];
    $array_fec = $_POST['txt_venta_fec'];
    $producto = strtoupper($_POST['txt_venta_des']);
    $total = moneda_mysql($_POST['txt_venta_mon']);
    $cuotas = intval($_POST['txt_venta_numcuo']);

    $data['est'] = 0;
    $data['msj'] = 'Ingrese los datos completos';
    if (count($array_cuo) > 0) {

        $oGarantia->garantia_xac = 1;
        $oGarantia->credito_id = 0;
        $oGarantia->garantiatipo_id = 6;
        $oGarantia->garantia_can = 1;
        $oGarantia->garantia_pro = $producto;
        $oGarantia->garantia_val = $total;
        $oGarantia->garantia_valtas = $total;
        $oGarantia->garantia_ser = '';
        $oGarantia->garantia_kil = ''; //kilates para oro
        $oGarantia->garantia_pes = ''; //peso para oro
        $oGarantia->garantia_grab = ''; //grabado
        $oGarantia->garantia_med = ''; //medidas
        $oGarantia->garantia_tas = ''; //tasador de oro
        $oGarantia->garantia_det = $producto; //detalle
        $oGarantia->garantia_web = '';
        //$oGarantia->insertar();
        
        $result=$oGarantia->insertar();
        if(intval($result['estado']) == 1){
            $gar_id = $result['garantia_id'];
        }


        $his = 'Garantía autorizada por: <b>' . $_SESSION['usuario_nom'] . '</b>. Pago en ' . $cuotas . ' cuotas. | ' . date('d-m-Y h:i a') . '<br>';

        for ($i = 0; $i < count($array_cuo); $i++) {
            $oVentainterna->tb_ventainterna_fecpag=fecha_mysql($array_fec[$i]);
            $oVentainterna->tb_garantia_id=$gar_id; 
            $oVentainterna->tb_usuario_id=$colabo_id; 
            $oVentainterna->tb_ventainterna_mon=moneda_mysql($array_cuo[$i]); 
            $oVentainterna->tb_ventainterna_his= $his;
            $oVentainterna->tb_ventainterna_nrocuo= ($i+1).'/'. $cuotas;
            //$oVentainterna->insertar(fecha_mysql($array_fec[$i]), $gar_id, $colabo_id, moneda_mysql($array_cuo[$i]), $his);
            $oVentainterna->insertar();
        }

        $data['est'] = 1;
        $data['msj'] = 'Se ha registrado el nuevo Producto correctamente';
        echo json_encode($data);
    } else {
        echo json_encode($data);
    }
}
if ($action == 'cronograma') {
    $monto_total = moneda_mysql($_POST['monto_total']);
    $cuotas = intval($_POST['cuotas']);
    if ($cuotas <= 0) {
        echo 'Ingrese Número de Cuotas';
        exit();
    }
    $fecha = date('d-m-Y');
    $cuota_mon = $monto_total / $cuotas;

    $tabla = '<table class="table" cellspacing="1" id="tb_simulador">';
    $tabla .= '
			<tr id="tabla_cabecera">
				<th id="tabla_cabecera_fila">N°</th>
				<th id="tabla_cabecera_fila">CUOTA</th>
				<th id="tabla_cabecera_fila">FECHA PAGO</th>
			</tr>';
    for ($i = 0; $i < $cuotas; $i++) {
        $fecha = strtotime('+30 day', strtotime($fecha));
        $fecha = date('d-m-Y', $fecha);
        $tabla .= '
	      <tr id="tabla_cabecera_fila">
	        <td id="tabla_fila">' . ($i + 1) . '</td>
	        <td id="tabla_fila">
	          <input type="text" name="txt_venta_cuo[]" id="txt_venta_cuo" class="venta_cuo form-control input-sm" value="' . mostrar_moneda($cuota_mon) . '"  style="text-align: center">
	        </td>
	        <td id="tabla_fila">
	          <input type="text" name="txt_venta_fec[]" id="txt_venta_fec' . ($i + 1) . '" class="venta_fec' . ($i + 1) . '  form-control input-sm" value="' . $fecha . '" style="text-align: center">
	        </td>
	      </tr>
	    ';
    }
    $tabla .= '</table>';
    echo $tabla;
}
?>