<?php
require_once ('../../core/usuario_sesion.php');
require_once('../menorgarantia/Ventainterna.class.php'); //lista la tabla desde el modulo de remates (menorgarantia)
$oVentainterna = new Ventainterna();
require_once("../usuario/Usuario.class.php");
$oUsuario = new Usuario();

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");

$usu_id = intval($_POST['usu_id']);
$mes = ($_POST['mes']);
$anio = ($_POST['anio']);

$dts = $oVentainterna->listar_ventainterna($usu_id, $mes, $anio); //lista todas las compras que ha hecho el usuario
// echo '$usu_id='.$usu_id.' - mes='.$mes.' - año='.$anio.' -$dts='.$dts['estado'].' - $data='.$dts['data'];exit();
?>

<?php if ($dts['estado'] == 1) {
    ?>
    <table cellspacing="1" id="tabla_ventainterna" class="table table-hover">
        <thead>
            <tr id="tabla_cabecera">
                <th id="tabla_cabecera_fila">FECHA</th>
                <th id="tabla_cabecera_fila">CREDITO ID</th>
                <th id="tabla_cabecera_fila">GARANTÍA ID</th>
                <th id="tabla_cabecera_fila">GARANTÍA</th>
                <th id="tabla_cabecera_fila">COLABORADOR</th>
                <th id="tabla_cabecera_fila">MONTO PAGAR</th>
                <th id="tabla_cabecera_fila">NRO CUOTA</th>
                <th id="tabla_cabecera_fila">ESTADO</th>
                <th id="tabla_cabecera_fila">OPCIONES</th>
            </tr>
        </thead>
        <tbody> <?php
    foreach ($dts['data']as $key => $dt) {

        // fecha de actualizacion para pago parcial 07/06/2023
        $procede_parcial = 0; // para distinguir que registro puede usar pago parcial
        $fecha_corte = '2023-06-30';
        $dts2 = $oVentainterna->lista_garantia_fuera_actua($usu_id, $fecha_corte);
        if ($dts2['estado'] == 1) {
            foreach ($dts2['data']as $key => $dt2) {
                if($dt2['tb_ventainterna_id'] == $dt['tb_ventainterna_id']){
                    $procede_parcial = 1; // procede pago parcial de venta interna
                }
            }
        }
        //

        //ver pagos parciales 27/06/23
        $dts3 = $oVentainterna->lista_pagos_ventainterna($dt['tb_ventainterna_id']);
        if ($dts3['estado'] == 1) {
            foreach ($dts3['data'] as $key => $dt3) {
                if($dts3['estado'] == 1){
                    $pagado_venta_interna = $pagado_venta_interna + $dt3['tb_ingreso_imp'];
                }
            }
        }
        //
        ?>
            <tr id="tabla_cabecera_fila" style="font-family: cambria">
                    <td id="tabla_fila"><?php echo mostrar_fecha($dt['tb_ventainterna_fecpag']) . '(' . $dt['tb_ventainterna_id'] . ')'; ?></td>
                    <td id="tabla_fila"><?php echo 'CM-' . $dt['tb_credito_id']; ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_garantia_id'] ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_garantia_pro']; ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_usuario_nom']; ?></td>
                    <td id="tabla_fila"><?php echo 'S/. ' . mostrar_moneda($dt['tb_ventainterna_mon']) . ' | ' . 'S/. ' . mostrar_moneda($dt['tb_ventainterna_monpag']); ?></td>
                    <td id="tabla_fila"><?php echo $dt['tb_ventainterna_nrocuo']; ?></td>
                    <td id="tabla_fila">
                        <?php
                        $estado = '<b>POR PAGAR</b>';
                        if ($dt['tb_ventainterna_est'] == 1)
                            $estado = '<span style="color: green;"><b>PAGADO</b></span>';
                        echo $estado;
                        ?>    
                    </td>
                    <td id="tabla_fila">
                        <?php
                        if ($dt['tb_ventainterna_est'] != 1)
                            echo '<a href="javascript:void(0)" class="btn btn-success btn-xs" onclick="pagar_ventainterna(' . $dt['tb_ventainterna_id'] . ','.$procede_parcial.')" title="PAGAR"><i class="fa fa-money"></i></a>';
                        echo '
                                <a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="compracolaborador_his(' . $dt['tb_ventainterna_id'] . ')" title="HISTORIAL"><i class="fa fa-history"></i></a>';
                        ?>
                    </td>
                </tr> <?php
            }
    ?>
        </tbody>
    </table>
    <?php
} else {
    echo '<center><h2>NO HAY REGISTRO DE COMPRAS INTERNAS PARA LO SELECCIONADO</h2></center>';
}
?>
