<?php

if (defined('APP_URL'))
    require_once(APP_URL . 'datos/conexion.php');
else
    require_once('../../datos/conexion.php');

class Compracolaborador extends Conexion {

    // public $tb_ventainterna_id;
    // public $tb_ventainterna_xac;
    // public $tb_ventainterna_fecpag;
    // public $tb_garantia_id;
    // public $tb_usuario_id;
    // public $tb_ventainterna_mon;
    // public $tb_ventainterna_monpag;
    // public $tb_ventainterna_est;
    // public $tb_ventainterna_tip;
    // public $tb_ingreso_id;
    // public $tb_ventainterna_his;

    // function insertar() {
    //     $this->dblink->beginTransaction();
    //     try {
    //         $sql = "INSERT INTO tb_ventainterna(
    //                                     tb_ventainterna_fecpag, 
    //                                     tb_garantia_id, 
    //                                     tb_usuario_id, 
    //                                     tb_ventainterna_mon, 
    //                                     tb_ventainterna_his,
    //                                     tb_ventainterna_nrocuo) 
    //                             VALUES (
    //                                     :tb_ventainterna_fecpag,
    //                                     :tb_garantia_id,
    //                                     :tb_usuario_id,
    //                                     :tb_ventainterna_mon,
    //                                     :tb_ventainterna_his,
    //                                     :tb_ventainterna_nrocuo)";

    //         $sentencia = $this->dblink->prepare($sql);
    //         $sentencia->bindParam(":tb_ventainterna_fecpag", $this->tb_ventainterna_fecpag, PDO::PARAM_STR);
    //         $sentencia->bindParam(":tb_garantia_id", $this->tb_garantia_id, PDO::PARAM_INT);
    //         $sentencia->bindParam(":tb_usuario_id", $this->tb_usuario_id, PDO::PARAM_INT);
    //         $sentencia->bindParam(":tb_ventainterna_mon", $this->tb_ventainterna_mon, PDO::PARAM_STR);
    //         $sentencia->bindParam(":tb_ventainterna_his", $this->tb_ventainterna_his, PDO::PARAM_STR);
    //         $sentencia->bindParam(":tb_ventainterna_nrocuo", $this->tb_ventainterna_nrocuo, PDO::PARAM_INT);

    //         $result = $sentencia->execute();
    //         $ventagarantia_id = $this->dblink->lastInsertId();
    //         $this->dblink->commit();

    //         $data['estado'] = $result; //si es correcto el ingreso retorna 1
    //         $data['ventagarantia_id'] = $ventagarantia_id;
    //         return $data;
    //     } catch (Exception $e) {
    //         $this->dblink->rollBack();
    //         throw $e;
    //     }
    // }

    function anular_ventainterna($gar_id, $valor, $his) {
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_ventainterna set tb_ventainterna_xac =:tb_ventainterna_xac, tb_ventainterna_his = CONCAT(tb_ventainterna_his,:tb_ventainterna_his) where tb_garantia_id =:tb_garantia_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_ventainterna_xac", $valor, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_ventainterna_his", $his, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_garantia_id", $gar_id, PDO::PARAM_INT);

            $result = $sentencia->execute();

            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    // function listar_ventainterna($usu_id, $mes, $anio) {
    //     try {
    //         $sql = "SELECT * FROM tb_ventainterna ve 
    //             inner join tb_garantia ga on ga.tb_garantia_id = ve.tb_garantia_id 
    //             inner join tb_usuario usu on usu.tb_usuario_id = ve.tb_usuario_id 
    //             where tb_ventainterna_xac =1";
    //         if (!empty($usu_id))
    //             $sql .= " and ve.tb_usuario_id =" . $usu_id;
    //         if (!empty($mes))
    //             $sql .= " and left(tb_ventainterna_fecpag, 7) ='" . $anio . "-" . $mes . "'";
    //         if (empty($mes))
    //             $sql .= " and YEAR(tb_ventainterna_fecpag) =" . $anio;
    //         $sql .= ' order by tb_ventainterna_fecpag';

    //         $sentencia = $this->dblink->prepare($sql);
    //         $sentencia->execute();

    //         if ($sentencia->rowCount() > 0) {
    //             $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
    //             $retorno["estado"] = 1;
    //             $retorno["mensaje"] = "exito";
    //             $retorno["data"] = $resultado;
    //             $sentencia->closeCursor(); //para libera memoria de la consulta
    //         } else {
    //             $retorno["estado"] = 0;
    //             $retorno["mensaje"] = "No hay tipos de crédito menores registrados";
    //             $retorno["data"] = "";
    //         }

    //         return $retorno;
    //     } catch (Exception $e) {
    //         throw $e;
    //     }
    // }

}
