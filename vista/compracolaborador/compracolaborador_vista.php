<?php ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-1">
                        <label>&nbsp;</label>
                        <a class="btn btn-primary btn-sm" id="btn_nuevo" href="javascript:void(0)" onclick="compracolaborador_form()" style="font-family: cambria">Nuevo Producto</a>
                    </div>
                    <div class="col-md-3">
                        <label for="cmb_usu_id">Colaborador:</label>
                        <select name="cmb_usu_id" id="cmb_usu_id" class="form-control form-control-sm selectpicker" data-live-search="true" data-max-options="1" data-size="12">
                            <?php require_once 'vista/usuario/usuario_select.php'; ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="cmb_pla_mes">Mes:</label>
                        <select name="cmb_pla_mes" id="cmb_pla_mes" class="form-control input-sm">
                          <?php 
                            $hoy_mes = date('m');
                            $mes = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                            $num = array('01','02','03','04','05','06','07','08','09','10','11','12');
                            $sel = '';

                            for ($i=0; $i < 12 ; $i++) { 
                              if($num[$i] == $hoy_mes)
                                echo '<option value="'.$num[$i].'" selected>'.$mes[$i].'</option>';
                              else
                                echo '<option value="'.$num[$i].'">'.$mes[$i].'</option>';
                            }
                            echo '<option value="">Todos..</option>';
                          ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="cmb_pla_anio">Año:</label>
                        <select name="cmb_pla_anio" id="cmb_pla_anio" class="form-control input-sm">
                            <?php 
                              $anio = intval(date('Y'));
                              for ($i = 2016; $i < ($anio + 5); $i++) { 
                                if($anio == $i)
                                  echo '<option value="'.$i.'" selected="true">'.$i.'</option>';
                                else
                                  echo '<option value="'.$i.'">'.$i.'</option>';
                              }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <!--- MESAJES DE GUARDADO -->
                <div class="callout callout-info" id="compracolaborador_mensaje_tbl" style="display: none;">
                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="div_compracolaborador_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
                            <?php // require_once('compracolaborador_tabla.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="div_modal_compracolaborador_form">
                <!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
            </div>
            <!--<div id="div_compracolaborador_tabla"></div>-->
            <div id="div_compracolaborador_his"></div>
            <div id="div_pagar_compracolaborador"></div>
            <div id="div_compracolaborador_form"></div>
            <!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
            <?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
