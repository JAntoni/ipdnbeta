/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    cmb_usu_id(1);
    $('.moneda').autoNumeric({
        aSep: '',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '0',
        vMax: '9999.99'
    });
    $('#txt_venta_numcuo').autoNumeric({
        aSep: '',
        aDec: '.',
        //aSign: 'S/. ',
        //pSign: 's',
        vMin: '1',
        vMax: '48'
    });


    $('#txt_venta_numcuo').keyup(function (e) {
        cuota_simulador();
    });

});

function cmb_usu_id(mostrar)
{
    $.ajax({
        type: "POST",
        url: VISTA_URL + "usuario/usuario_select.php",
        async: false,
        dataType: "html",
        data: ({
            mos: mostrar //victor y jesi son usuarios mos = 2, y tambien son almacen = 2
        }),
        beforeSend: function () {
            $('#cmb_colaborador_id').html('<option value="">Cargando...</option>');
        },
        success: function (html) {
            $('#cmb_colaborador_id').html(html);
        }
    });
}


function cuota_simulador() {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "compracolaborador/compracolaborador_controller.php",
        async: true,
        dataType: "html",
        data: ({
            action: 'cronograma',
            monto_total: $('#txt_venta_mon').val(),
            cuotas: $('#txt_venta_numcuo').val()
        }),
        beforeSend: function () {

        },
        success: function (html) {
            $('#div_venta_crono').html(html);
            var i = 0;
            $("#tb_simulador tr").each(function () {
                //console.log('fecha num: ' + i)
                $(this).find('.venta_cuo').autoNumeric({aSep: ',',
                    aDec: '.',
                    vMin: '0.00',
                    vMax: '99999999.99'
                });
                $(this).find('.venta_fec' + i).datepicker({
                    language: 'es',
                    autoclose: true,
                    format: "dd-mm-yyyy"
                });
                i = i + 1;
            });
            modal_height_auto('modal_registro_ventainterna');

        },
        complete: function (data) {

        }
    });
}



$("#form_ventainterna").validate({
    submitHandler: function () {
        $.ajax({
            type: "POST",
            url: VISTA_URL + "compracolaborador/compracolaborador_controller.php",
            async: true,
            dataType: "json",
            data: $('#form_ventainterna').serialize(),
            beforeSend: function () {
            },
            success: function (data) {
                if (parseInt(data.est) > 0) {
                    $('#modal_registro_ventainterna').modal('hide');
                    swal_success('CORRECTO', data.msj, 3000);
                    compracolaborador_tabla();
                } else
                    swal_warning('SISTEMA', data.msj, 3000);
            },
            complete: function (data) {

            }
        });
    },
    rules: {
        cmb_colaborador_id: {
            required: true
        },
        txt_venta_des: {
            required: true
        },
        txt_venta_mon: {
            required: true
        },
        txt_venta_numcuo: {
            required: true
        }
    },
    messages: {
        cmb_colaborador_id: {
            required: 'Seleccione Colaborador'
        },
        txt_venta_des: {
            required: 'Ingrese descripción del Producto'
        },
        txt_venta_mon: {
            required: 'Monto total del Producto'
        },
        txt_venta_numcuo: {
            required: 'Cuotas a Pagar'
        },
    }
});
