<?php
require_once ('../../core/usuario_sesion.php');

require_once("../funciones/funciones.php");
require_once("../funciones/fechas.php");
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_ventainterna" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-family: cambria;"><b>REGISTRO DE NUEVO PRODUCTO</b></h4>
            </div>
            <form id="form_ventainterna">
                <input type="hidden" name="action" value="insertar">

                <div class="modal-body">
                    <div class="">
                        <div class="box box-primary">
                            <div class="box-header">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Colaborador:</label>
                                        <select name="cmb_colaborador_id" id="cmb_colaborador_id" class="form-control input-sm"></select>
                                    </div>
                                </div>
                                <p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Producto:</label>
                                        <input type="text" name="txt_venta_des" id="txt_venta_des" class="form-control input-sm mayus">
                                    </div>
                                </div>
                                <p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Monto Total S/:</label>
                                        <input type="text" name="txt_venta_mon" id="txt_venta_mon" class="form-control input-sm moneda" value="">
                                    </div>
                                </div>
                                <p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Cuotas:</label>
                                        <input type="text" name="txt_venta_numcuo" id="txt_venta_numcuo" value="" class="form-control input-sm">
                                    </div>
                                </div>
                                <p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="div_venta_crono"></div>
                                    </div>
                                </div>
                                <p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="msj_veta_nueva" class="ui-state-highlight ui-corner-all" style="width:auto; float:right; padding:2px; display:none"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="area_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_area">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/compracolaborador/compracolaborador_form.js';?>"></script>