<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class sugerir extends Conexion{

    function insertar($credito_id, $credito_vehpla, $sugerir_soatval, $sugerir_strval, $sugerir_gpsval, $sugerir_kil, $sugerir_det, $creditotipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "
          INSERT INTO tb_sugerir(
          tb_credito_id, tb_credito_vehpla, tb_sugerir_soatval, 
          tb_sugerir_strval, tb_sugerir_gpsval, tb_sugerir_kil, 
          tb_sugerir_det, tb_creditotipo_id) 
          VALUES (
            :credito_id, :credito_vehpla, :sugerir_soatval, 
            :sugerir_strval, :sugerir_gpsval, :sugerir_kil, 
            :sugerir_det, :creditotipo_id)
        ";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_soatval", $sugerir_soatval, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_strval", $sugerir_strval, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_gpsval", $sugerir_gpsval, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_kil", $sugerir_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_det", $sugerir_det, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto el ingreso retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function modificar($sugerir_id, $credito_id, $credito_vehpla, $sugerir_soatval, $sugerir_strval, $sugerir_gpsval, $sugerir_kil, $sugerir_det, $creditotipo_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_sugerir SET 
          tb_credito_id =:credito_id, tb_credito_vehpla =:credito_vehpla, tb_sugerir_soatval =:sugerir_soatval, 
          tb_sugerir_strval =:sugerir_strval, tb_sugerir_gpsval =:sugerir_gpsval, 
          tb_sugerir_kil =:sugerir_kil, tb_sugerir_det =:sugerir_det, 
          tb_creditotipo_id =:creditotipo_id
          WHERE tb_sugerir_id =:sugerir_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":sugerir_id", $sugerir_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_soatval", $sugerir_soatval, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_strval", $sugerir_strval, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_gpsval", $sugerir_gpsval, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_kil", $sugerir_kil, PDO::PARAM_STR);
        $sentencia->bindParam(":sugerir_det", $sugerir_det, PDO::PARAM_STR);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($sugerir_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_sugerir WHERE tb_sugerir_id =:sugerir_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":sugerir_id", $sugerir_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUnoVehiculoPlaca($vehiculo_pla){
      try {
        $sql = "SELECT ven.*, cli.tb_cliente_id, cli.tb_cliente_nom FROM tb_sugerir ven 
        INNER JOIN tb_cliente cli on cli.tb_cliente_id = ven.tb_cliente_id
        WHERE upper(tb_vehiculo_pla) = upper(:vehiculo_pla) and tb_sugerir_xac = 1;";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":vehiculo_pla", $vehiculo_pla, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /* function mostrar_datos_stock_placa($credito_id, $credito_vehpla){
      try {
        $sql = "SELECT * FROM tb_sugerir where tb_credito_vehpla =:credito_vehpla and tb_credito_id =:credito_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $credito_id, PDO::PARAM_STR);
        $sentencia->bindParam(":credito_id", $credito_tip, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    } */
    function mostrar_sugerir_placa($credito_vehpla){
      try {
        $sql = "SELECT * FROM tb_sugerir where UPPER(tb_credito_vehpla) = UPPER(:credito_vehpla)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    function mostrar_fotos_credito($tabla, $credito_vehpla){
      try {
        $tabla_file = $tabla.'file';
        $sql = "SELECT cre.tb_credito_id, ".$tabla_file."_url as img_url FROM $tabla cre inner join $tabla_file fil on fil.tb_credito_id = cre.tb_credito_id where tb_credito_vehpla =:credito_vehpla and tb_credito_xac = 1 limit 5";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_vehpla", $credito_vehpla, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /* function sugerires_tabla(){
      try {
        $sql = "SELECT * FROM tb_sugerir where tb_sugerir_est !=0";
        if(intval($cliente_id) > 0)
          $sql .= " and re.tb_cliente_id =:cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        if(intval($cliente_id) > 0)
          $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    } */
    function mostrar_datos_sugerir_credito($credito_id, $creditotipo_tip, $tipo){
      try
      {
        $sql = "SELECT * FROM tb_sugerir WHERE tb_credito_id =:credito_id AND tb_creditotipo_id =:creditotipo_tip AND tb_sugerir_tip =:tipo";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_tip", $creditotipo_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tipo", $tipo, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      }
      catch (Exception $e)
      {
        throw $e;
      }

    }
  }

?>
