<form id="form_gasto_filtro" role="form">
    <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="txt_fil_cliente_nom">Cliente: </label>
                <input type="text" name="txt_fil_cliente_nom" id="txt_fil_cliente_nom" placeholder="Ingrese DNI o Nombre de Cliente a buscar" class="form-control input-sm ui-autocomplete-input" autocomplete="off" size="48" value>
                <input type="hidden" name="hdd_fil_cliente_id" id="hdd_fil_cliente_id">
            </div>
        </div>
        <div class="col-lg-1 col-md-2 col-sm-4 col-xs-8">
            <div class="form-group">
                <label for="txt_fil_veh_placa">Placa Veh.: </label>
                <input type="text" name="txt_fil_veh_placa" id="txt_fil_veh_placa" class="form-control input-sm ui-autocomplete-input" placeholder="Placa Vehicular" autocomplete="off" size="48">
                <input type="hidden" name="hdd_fil_veh_placa" id="hdd_fil_veh_placa">
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="form-group">
                <label for="txt_ventavehiculo_fec" class="control-label">&nbsp;</label> <br>
                <button id="btn_buscar" class="btn btn-info btn-sm" type="button" onclick="cgv_tabla()" title="Buscar">
                    <span class="fa fa-search fa-fw icon"></span> Buscar
                </button>
            </div>
        </div>

        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
            <table class="table datatable">
                <thead>
                    <th align="center" style="padding: 3px 10px; font-size: 11px;">Color</th>
                    <th align="center" style="padding: 3px 10px; font-size: 11px;">Tipo credito</th>
                </thead>
                <tbody>
                    <tr>
                        <td style="padding: 3px 10px;" align="center">
                            <div class="square garmob" style="width:15px; height: 13px; border:1px solid black;"></div>
                        </td>
                        <td style="padding: 3px 10px; font-size: 10px;">COMPRA VENTA / GARANTIA MOB</td>
                    </tr>
                    <tr>
                        <td style="padding: 3px 10px;" align="center">
                            <div class="square adenda" style="width:15px; height: 13px; border:1px solid black;"></div>
                        </td>
                        <td style="padding: 3px 10px; font-size: 10px;">ADENDA</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</form>