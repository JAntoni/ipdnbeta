<?php
  require_once('Gasto.class.php');
  $oGasto = new Gasto();
  require_once('../funciones/funciones.php');

  $array_gasto_id = $_POST['array_gasto_id'];
  $cliente_nom = $_POST['cliente_nom'];
  $cliente_id = intval($_POST['cliente_id']);
  $tipo_cambio = $_POST['tipo_cambio'];
  $credito_id = $_POST['credito_id'];
  $vehiculo_pla = $_POST['vehiculo_pla'];
  $gastos_totales = 0;
  $devolucion_previo = 0;
  $valores_gasto_id = implode(',', $array_gasto_id);

  if (is_array($array_gasto_id) && count($array_gasto_id) > 0) {
    foreach ($array_gasto_id as $gasto_id) {
      $result = $oGasto->mostrarUno($gasto_id);
        $gasto_des = $result['data']['tb_gasto_des'];
        $credito_id = $result['data']['tb_credito_id'];
        $gasto_his = $result['data']['tb_gasto_his'];
        $gasto_ptl = formato_numero($result['data']['tb_gasto_ptl']);
        $gasto_devo = formato_numero($result['data']['tb_gasto_devo']);

        $gastos_totales += $gasto_ptl;
        $devolucion_previo += $gasto_devo;
      $result = NULL;
    }
  }
  

  $falta = formato_numero($gastos_totales - $devolucion_previo);
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_gasto_devolucion" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title" style="font-weight: bold;font-family: cambria">Ingreso de Devolución de Gastos</h4>
      </div>
      <form id="form_devolucion" method="post">
        <input type="hidden" name="action" value="devolucion">
        <input type="hidden" name="hdd_gasto_id" id="hdd_gasto_id" value="<?php echo $gasto_id;?>">
        <input type="hidden" name="hdd_devolucion_tipocambio" id="hdd_devolucion_tipocambio" value="<?php echo $tipo_cambio;?>">
        <input type="hidden" name="hdd_devolucion_cliente_nom" id="hdd_devolucion_cliente_nom" value="<?php echo $cliente_nom;?>">
        <input type="hidden" name="hdd_devolucion_cliente_id" id="hdd_devolucion_cliente_id" value="<?php echo $cliente_id;?>">
        <input type="hidden" name="hdd_devolucion_credito_id" id="hdd_devolucion_credito_id" value="<?php echo $credito_id;?>">
        <input type="hidden" name="hdd_devolucion_credito_pla" id="hdd_devolucion_credito_pla" value="<?php echo $vehiculo_pla;?>">
        <input type="hidden" name="hdd_devolucion_array" id="hdd_devolucion_array" value="<?php echo $valores_gasto_id;?>">
        <input type="hidden" name="hdd_cuenta_id" id="hdd_cuenta_id" value="1">
        <input type="hidden" name="hdd_subcuenta_id" id="hdd_subcuenta_id" value="93">
        
        <div class="modal-body">
          <div class="box box-primary shadow">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <label>Nombre Producto:</label>
                  <input name="txt_devolucion_pro" type="text" id="txt_devolucion_pro" class="form-control input-sm mayus" value="CANCEL CUOTAS / INGRESO CLIENTE" readonly="">
                </div>
                <div class="col-md-6">
                  <label>Nombre Producto:</label>
                  <input name="txt_devolucion_pro" type="text" id="txt_devolucion_pro" class="form-control input-sm mayus" value="GASTOS OPERATIVOS Y RETENCIONES" readonly="">
                </div>
              </div>
              <p> 
              <div class="row">
                <div class="col-md-4">
                  <label>Suma Total Gastos:</label>
                  <input name="" type="text" id="" class="form-control input-sm" value="<?php echo $gastos_totales;?>" readonly="">
                </div>
                <div class="col-md-4">
                  <label>Devolución Previa:</label>
                  <input name="txt_devolucion_devo" type="text" id="txt_devolucion_devo" class="form-control input-sm" value="<?php echo $devolucion_previo;?>" readonly="">
                </div>
                <div class="col-md-4">
                  <label>Falta Por Devolver:</label>
                  <input name="txt_devolucion_falta" type="text" id="txt_devolucion_falta" class="form-control input-sm" value="<?php echo $falta;?>" readonly="">
                </div>
              </div>
              <p> 
              <div class="row">
                <div class="col-md-8">
                  <label>Lugar Pago:</label>
                  <select name="cmb_gasto_lugpag" id="cmb_gasto_lugpag" class="form-control input-sm">
                    <option value="1">En Oficinas</option>
                    <option value="2">Con depósito</option>
                  </select>
                </div>
                <div class="col-md-4" id="ingreso_oficina">
                  <label>Monto a Ingresar:</label>
                  <input name="txt_devolucion_imp" type="text" id="txt_devolucion_imp" class="form-control input-sm moneda3" value="" style="text-align:right;" class="moneda2">
                </div>
              </div>
              <p> 
              <div class="row devolucion_banco"  style="display: none;">
                <div class="col-md-6">
                  <label>N° Cuenta:</label>   
                  <select name="cmb_cuedep_id" id="cmb_cuedep_id" class="form-control input-sm">
                    <?php require_once '../cuentadeposito/cuentadeposito_select.php';?>
                  </select>
                </div>
                <div class="col-md-6">
                  <label for="txt_devolucion_fec" class="control-label">Fecha</label>
                  <div class="input-group date" id="devolucion_picker">
                    <input name="txt_devolucion_fec" type="text" id="txt_devolucion_fec" value="" class="form-control input-sm">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <p>
              <div class="row devolucion_banco" style="display: none;">
                <div class="col-md-6">
                  <label>Monto Depósito:</label>   
                  <input style="text-align:right;" class="form-control input-sm mayus moneda3" name="txt_devolucion_mon" type="text" id="txt_devolucion_mon">
                </div>
                <div class="col-md-6">
                  <label>Comisión Banco:</label>
                  <input style="text-align:right;" class="form-control input-sm moneda3" name="txt_devolucion_comi" type="text" id="txt_devolucion_comi" value="0.00">
                </div>
              </div>
              <p>
              <div class="row devolucion_banco"  style="display: none;">
                <div class="col-md-6">
                  <label>Monto Pagado:</label>
                  <input style="text-align:right;" class="form-control input-sm moneda3" name="txt_devolucion_montot" type="text" id="txt_devolucion_montot" value="0" readonly>
                </div>
                <div class="col-md-6">
                  <label>N° Operación:</label>
                  <input type="text" name="txt_devolucion_numope" id="txt_devolucion_numope" class="form-control input-sm">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <div class="f1-buttons">
            <button type="submit" class="btn btn-info" id="btn_guardar_menorgarantia">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>         
        </div>
      </form>
    </div>
  </div>
</div>
 
<script type="text/javascript" src="<?php echo 'vista/gasto/gasto_devolucion.js?ver=123344';?>"></script>
