<?php
    if (defined('VISTA_URL')) {
        require_once(APP_URL . 'core/usuario_sesion.php');
    } else {
        require_once('../../core/usuario_sesion.php');
    }
    require_once('Gasto.class.php');
    require_once('../funciones/funciones.php');
    require_once('../funciones/fechas.php');

    $oGasto = new Gasto();

    $cliente_id = intval($_POST['cliente_id']);
    $placa = trim(strtoupper($_POST['placa']));

    $result = $oGasto->listar_cgv($cliente_id, $placa);

    $tr = '';
    if ($result['estado'] == 1) {
        foreach ($result['data'] as $key => $value) {
            switch ($value['tb_credito_est']) {
                case 1:
                    $estado_credito = 'PENDIENTE';
                    break;
                case 2:
                    $estado_credito = 'APROBADO';
                    break;
                case 3:
                    $estado_credito = 'VIGENTE';
                    break;
                case 4:
                    $estado_credito = 'PARALIZADO';
                    break;
                case 5:
                    $estado_credito = 'REFINANCIADO AMORTIZADO';
                    break;
                case 6:
                    $estado_credito = 'REFINANCIADO SIN AMORTIZADO';
                    break;
                case 7:
                    $estado_credito = 'LIQUIDADO';
                    break;
                case 8:
                    $estado_credito = 'RESUELTO';
                    break;
                case 9:
                    $estado_credito = 'N° TÍTULO';
                    break;
                case 10:
                    $estado_credito = 'DOCUMENTOS';
                    break;
                default:
                    $estado_credito = 'SIN ESTADO, REPÓRTELO';
                    break;
            }
            // ,'.$value['tb_cuotatipo_id'].'
            $class = $value['tb_credito_tip'] == 2 ? 'adenda' : 'garmob';
            $tr .= "<tr class='$class'>";
            $tr .=
                '
                <td style="vertical-align: middle;" id="tabla_fila" class="text-center">' . mostrar_fecha($value['tb_credito_feccre']) . '</td>
                <td style="vertical-align: middle;" class="text-center" id="tabla_fila">' . 'CGV-' . completar_numero_ceros($value['tb_credito_id'], 5) . '</td>
                <td style="vertical-align: middle;" id="tabla_fila">' . $value['tb_cliente_nom'] . '</td>
                <td style="vertical-align: middle;" id="tabla_fila">' . $value['tb_vehiculomarca_nom'] . ' ' . $value['tb_vehiculomodelo_nom'] . '</td>
                <td style="vertical-align: middle;" class="text-center" id="tabla_fila">' . $value['tb_credito_vehpla'] . '</td>
                <td style="vertical-align: middle;" class="text-center" id="tabla_fila">' . $value['tb_credito_vehano'] . '</td>
                <td style="vertical-align: middle;" class="text-right" id="tabla_fila">' . $value['tb_moneda_nom'] . ' ' . $value['tb_credito_preaco'] . '</td>
                <td style="vertical-align: middle;" class="text-center" id="tabla_fila">' . $value['tb_cuotatipo_nom'] . '</td>
                <td style="vertical-align: middle;" class="text-center" id="tabla_fila">' . $estado_credito . '</td>
                <td style="vertical-align: middle;" id="tabla_fila" align="center">
                    <a class="btn btn-primary btn-xs" title="Preparar" onclick="preparar_form('.$value['tb_credito_id'].')"><i class="fa fa-car fa-fw"></i> Preparar</a> &nbsp;
                    <a class="btn btn-success btn-xs" title="Ajustar" onclick="ajustar_form('.$value['tb_credito_id'].',3)"><i class="fa fa-retweet fa-fw"></i> Ajustar</a> &nbsp;
                    <a class="btn btn-info btn-xs" title="Gastos" onclick="tabla_gastos_form('.$value['tb_credito_id'].')"><i class="fa fa-coins fa-fw"></i> Gastos</a>
                    <!-- <a class="btn btn-github btn-xs" title="Venta" onclick="venta_form( $value[ tb_credito_id ] )"><i class="fa fa-file-text fa-fw"></i> Venta</a> -->
                </td>';
            $tr .= '</tr>';
        }
        $result = null;
    } else {
        echo $result['mensaje'];
        $result = null;
        exit();
    }
?>
<table id="tbl_cgv" class="table table-hover dataTable display">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">F. Crédito</th>
            <th id="tabla_cabecera_fila">Crédito ID</th>
            <th id="tabla_cabecera_fila">Cliente</th>
            <th id="tabla_cabecera_fila">Vehículo</th>
            <th id="tabla_cabecera_fila">Placa</th>
            <th id="tabla_cabecera_fila">Año</th>
            <th id="tabla_cabecera_fila">Monto</th>
            <th id="tabla_cabecera_fila">Tipo</th>
            <th id="tabla_cabecera_fila">Estado crédito</th>
            <th id="tabla_cabecera_fila">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>