$(document).ready(function () {
	disabled($('#form_gasto').find($('.disabled')));

	$('.cant').autoNumeric({
        aSep: '',
        aDec: '.',
        vMin: '1',
        vMax: '10000'
    });

	$('.moneda').focus(function(){
		formato_moneda(this);
	});

	$("#txt_proveedor").autocomplete({
		minLength: 1,
		source: function (request, response) {
			$.getJSON(
					VISTA_URL + "proveedor/proveedor_autocomplete.php",
					{term: request.term}, //
					response
					);
		},
		select: function (event, ui) {
			$('#hdd_proveedor_id').val(ui.item.tb_proveedor_id);
			$('#txt_proveedor').val(ui.item.tb_proveedor_doc+' - '+ui.item.tb_proveedor_nom);
			event.preventDefault();
			disabled($('input[id*="txt_proveedor"]'));
			disabled($('#btn_agr'));
			$('#txt_gasto_cantidad').focus();
		}
	});

	$('#form_gasto').validate({
		submitHandler: function () {
			if (parseInt($('#hdd_proveedor_id').val()) < 1) {
				alerta_error("SISTEMA", "ELIJA UN PROVEEDOR VALIDO");
				return;
			}
			$.ajax({
				type: "POST",
				url: VISTA_URL + "gasto/gasto_controller.php",
				async: true,
				dataType: "json",
				data: serializar_form(),
				beforeSend: function () {
					$('#gasto_mensaje').show(400);
					$('#btn_guardar_gasto').prop('disabled', true);
				},
				success: function (data) {
					if (parseInt(data.estado) > 0) {
						$('#gasto_mensaje').removeClass('callout-info').addClass('callout-success');
						$('#gasto_mensaje').html(`<h4>${data.mensaje}</h4>`);
						if($.inArray($('#action').val(), ['insertar', 'modificar']) > -1){
							disabled($('#form_gasto').find("button[class*='btn-sm'], select, input, textarea"));
						}
						
						setTimeout(function () {
							if(parseInt(data.estado) != 2){
								completar_tabla_gastos();
							}
							$('#modal_gasto_form').modal('hide');
						}, 2800);
					}
					else {
						$('#gasto_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#gasto_mensaje').html('Alerta: ' + data.mensaje);
						$('#btn_guardar_gasto').prop('disabled', false);
					}
				},
				error: function (data) {
					$('#gasto_mensaje').removeClass('callout-info').addClass('callout-danger')
					$('#gasto_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
		},
		rules: {
			cbo_gasto_tipo: {
				min: 1
			},
			txt_proveedor:{
				required: function(){
					return parseInt($('#hdd_proveedor_id').val()) < 1;
				}
			},
			txt_gasto_cantidad: {
				required: function(){
					var cantidad = parseInt($('#txt_gasto_cantidad').val());
					return isNaN(cantidad) || cantidad < 1;
				}
			},
			txt_gasto_preunit: {
				required: function(){
					var gasto_preunit = parseFloat($('#txt_gasto_preunit').val());
					return isNaN(gasto_preunit) || gasto_preunit < 0.01;
				}
			},
			txt_descripcion: {
				required: true,
				minlength: 5
			}
		},
		messages: {
			cbo_gasto_tipo: {
				min: "Elija Tipo gasto"
			},
			txt_proveedor:{
				required: "Ingrese proveedor"
			},
			txt_gasto_cantidad: {
				required: "Ingrese cantidad"
			},
			txt_gasto_preunit: {
				required: "Ingrese precio unitario"
			},
			txt_descripcion: {
				required: "Ingrese descripción para el Gasto",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.parent("label"));
			} else if (element.parent("div.input-group").length == 1) {
				error.insertAfter(element.parent("div.input-group")[0]);
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

	$('.para_calc').keyup(function(){
		var cant = quitar_comas_miles($('#txt_gasto_cantidad').val());
		var pre_unit = quitar_comas_miles($('#txt_gasto_preunit').val());

		$('#txt_gasto_pretotal').val(new Intl.NumberFormat('es-PE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(cant * pre_unit));
	});

	if ($('#action').val() == 'leer' || $('#action').val() == 'eliminar') {
        disabled($('#form_gasto').find("button[class*='btn-sm'], select, input, textarea"));
        $('#form_gasto').find("input").css("cursor", "text");

        if ($('#action').val() == 'eliminar') {
            $('#gasto_mensaje').removeClass('callout-info').addClass('callout-warning');
            mens = '<h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar este gasto?</h4>';
            $('#gasto_mensaje').html(mens);
            $('#gasto_mensaje').show();
        }
    } else {
        if ($('#action').val() == 'modificar') {
            if(parseInt($('#hdd_proveedor_id').val()) > 0){
                disabled($('#txt_proveedor, #btn_agr'));
            }
        }
    }
});

function limpiar_proveedor(){
	$('#hdd_proveedor_id').val(0);
	$('#txt_proveedor').val('');

	$('#txt_proveedor').prop('disabled', false);
	$('#btn_agr').removeAttr('disabled');
	$('#txt_proveedor').focus();
}

function proveedor_form(usuario_act, proveedor_id) {
	var doc_proveedor_id = Number($('#hdd_proveedor_id').val());

	if (parseInt(doc_proveedor_id) <= 0 && usuario_act == 'M') {
		alerta_warning('Información', 'Debes buscar por documento o nombres');
		return false;
	}

	$.ajax({
		type: "POST",
		url: VISTA_URL + "proveedor/proveedor_form.php",
		async: true,
		dataType: "html",
		data: ({
			action: usuario_act, // PUEDE SER: L, I, M , E
			proveedor_id: doc_proveedor_id,
			vista: 'gasto'
		}),
		beforeSend: function () {
			$('#h3_modal_title').text('Cargando Formulario');
			$('#modal_mensaje').modal('show');
		},
		success: function (data) {
			$('#modal_mensaje').modal('hide');
			if (data != 'sin_datos') {
				$('#div_modal_proveedor_form').html(data);
				$('#modal_registro_proveedor').modal('show');

				//desabilitar elementos del form si es L (LEER)
				if (usuario_act == 'L' || usuario_act == 'E')
					form_desabilitar_elementos('form_proveedor'); //funcion encontrada en public/js/generales.js

				//funcion js para limbiar el modal al cerrarlo
				modal_hidden_bs_modal('modal_registro_proveedor', 'limpiar'); //funcion encontrada en public/js/generales.js
				//funcion js para agregar un largo automatico al modal, al abrirlo
				modal_height_auto('modal_registro_proveedor'); //funcion encontrada en public/js/generales.js
				//funcion js para agregar un ancho automatico al modal, al abrirlo
				modal_width_auto('modal_registro_proveedor', 75); //funcion encontrada en public/js/generales.js
			}
			else {
				//llamar al formulario de solicitar permiso
				var modulo = 'proveedor';
				var div = 'div_modal_proveedor_form';
				permiso_solicitud(usuario_act, proveedor_id, modulo, div); //funcion ubicada en public/js/permiso.js
			}
		},
		error: function (data) {
			alerta_error('ERROR', data.responseText)
		}
	});
}

function llenar_datos_proveedor(data){
	$('#hdd_proveedor_id').val(data.registro['tb_proveedor_id']);
	$('#txt_proveedor').val(data.registro['tb_proveedor_doc']+' - '+data.registro['tb_proveedor_nom']);

	disabled($('input[id*="txt_proveedor"]'));
	disabled($('#btn_agr'));

	$('#txt_gasto_cantidad').focus();
	$('#txt_proveedor-error').hide(50);
}

function serializar_form() {
	var extra = `&moneda_nom=${$('#cmb_moneda_id option:selected').text()}`;

    var elementos_disabled = $('#form_gasto').find('input:disabled, select:disabled').removeAttr('disabled');
    var form_serializado = $('#form_gasto').serialize() + extra;
    elementos_disabled.attr('disabled', 'disabled');
    return form_serializado;
}