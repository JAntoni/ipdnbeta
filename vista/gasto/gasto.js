var datatable_global_cgv;

$(document).ready(function () {

  pintar_adenda_garmob();

  $("#click").click();

  cgv_tabla();

  $("#txt_fil_cliente_nom").focus();

  $("#txt_fil_cliente_nom").autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "cliente/cliente_autocomplete.php",
        { term: request.term },
        response
      );
    },
    select: function (event, ui) {
      $("#hdd_fil_cliente_id").val(ui.item.cliente_id);
      $("#txt_fil_cliente_nom").val(ui.item.cliente_nom);
      cgv_tabla();
      event.preventDefault();
      $("#txt_fil_cliente_nom").focus();
    },
  });

  $("#txt_fil_cliente_nom, #txt_fil_veh_placa").keyup(function () {
    if ($(this).val() == "") {
      if (
        $(this)[0].id == "txt_fil_cliente_nom" &&
        parseInt($("#hdd_fil_cliente_id").val()) > 0
      ) {
        $("#hdd_fil_cliente_id").val("");
        cgv_tabla();
      } else if ($(this)[0].id == "txt_fil_veh_placa") {
        cgv_tabla();
      }
    }
  });

});

function pintar_adenda_garmob() {
  $(".garmob").css("background-color", "#ffffff"); // ffffff
  $(".adenda").css("background-color", "#c9ecf2"); // c9ecf2
}

function cgv_tabla() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gasto/cgv_tabla.php",
    async: true,
    dataType: "html",
    data: {
      cliente_id: $("#hdd_fil_cliente_id").val(),
      placa: $("#txt_fil_veh_placa").val(),
    },
    beforeSend: function () {
      $("#cgv_mensaje_tbl").show(300);
    },
    success: function (data) {
      $("#div_cgv_tabla").html(data);
      $("#cgv_mensaje_tbl").hide(300);
    },
    complete: function (data) {
      pintar_adenda_garmob();
      estilos_datatable_cgv();
    },
    error: function (data) {
      $("#cgv_mensaje_tbl").html(
        "ERROR AL CARGAR DATOS DE CRÉDITO: " + data.responseText
      );
    }
  });
}

function estilos_datatable_cgv() {
  datatable_global_cgv = $("#tbl_cgv").DataTable({
    pageLength: 50,
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsqueda",
      info: "Mostrado _END_ registros",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar:",
    },
    order: [],
    columnDefs: [{ targets: [7, 8], orderable: false }],
  });
  datatable_texto_filtrar_cgv();
}

function datatable_texto_filtrar_cgv() {
  $('input[aria-controls*="tbl_cgv"]')
    .attr("id", "txt_datatable_fil")
    .attr("placeholder", "escriba para buscar");

  $("#txt_datatable_fil").keyup(function (event) {
    $("#hdd_datatable_fil").val(this.value);
  });

  var text_fil = $("#hdd_datatable_fil").val();
  if (text_fil) {
    $("#txt_datatable_fil").val(text_fil);
    datatable_global_cgv.search(text_fil).draw();
  }
}

function preparar_form(credito_id) {
  var fecha_hoy = $("#txt_fecha_ref").val();

  $.confirm({
    title: "Fecha de Liquidación",
    content:
      "" +
      '<form action="" class="formName">' +
      '<div class="form-group">' +
      "<label>Ingresa tu Fecha Aquí:</label>" +
      '<input type="date" class="name form-control" required />' +
      "</div>" +
      "</form>",
    buttons: {
      formSubmit: {
        text: "Generar",
        btnClass: "btn-blue",
        action: function () {
          var fecha_liquidacion = this.$content.find(".name").val();
          if (!fecha_liquidacion) {
            $.alert("Debes ingresar una fecha valida");
            return false;
          }
          var tipo_cambio = $("#hdd_tipo_cambio_venta").val();

          $.ajax({
            type: "POST",
            url: VISTA_URL + "liquidacion/liquidacion_formato_form.php",
            async: true,
            dataType: "html",
            data: {
              credito_id: credito_id,
              creditotipo_id: 3, //estamos en la carpeta de garveh, por ende es el 3
              fecha_liquidacion: fecha_liquidacion,
              tipo_cambio: tipo_cambio,
              vista: "gasto",
            },
            beforeSend: function () {
              $("#modal_mensaje").modal("show");
            },
            success: function (html) {
              // console.log(html)
              $("#modal_mensaje").modal("hide");
              $("#div_modal_liquidacion").html(html);
              modal_width_auto("modal_liquidacion_formato", 92);
              modal_height_auto("modal_liquidacion_formato");
              $("#modal_liquidacion_formato").modal("show");
              modal_hidden_bs_modal("modal_liquidacion_formato", "limpiar"); //funcion encontrada en public/js/generales.js              
            },
            complete: function () {
              //credito_tabla();
            },
          });
        },
      },
      Cancelar: function () {
        //close
      },
    },
    onContentReady: function () {
      // bind to events
      var jc = this;
      this.$content.find("form").on("submit", function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger("click"); // reference the button and click it
      });
    },
  });
}

function ajustar_form(credito_id, tipo_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "stockunidad/stockunidad_ajustar_form.php",
    async: true,
    dataType: "html",
    data: {
      credito_id: credito_id,
      tipo_id: tipo_id,
      vista: 'gasto'
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando ...");
      $("#modal_mensaje").modal("show");
      console.log("/Gasto/ Credito ID: "+credito_id+ " - Credito tipo ID: "+tipo_id);
    },
    success: function (html) {
      $("#div_modal_ajustar_form").html(html);
      $("#div_modal_ajustar").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("div_modal_ajustar", 30);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("div_modal_ajustar"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("div_modal_ajustar", "limpiar");
      modal_width_auto("div_modal_ajustar", 60);
      modal_height_auto("div_modal_ajustar");
    },
    complete: function (data) {
      // console.log(data);
    },
    error: function (data) {
      $("#modal_mensaje").modal("hide");
      alerta_error("Advertencia", "" + data.responseText);
    },
  });
}

function tabla_gastos_form(credito_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gasto/gasto_tabla_form.php",
    async: true,
    dataType: "html",
    data: {
      credito_id: credito_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_tbl_gastos").html(html);
      $("#div_modal_gasto_tabla").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("div_modal_gasto_tabla", 70);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("div_modal_gasto_tabla"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal("div_modal_gasto_tabla", "limpiar");
    },
  });
}

// ----------------------------------------------------------------------------------- //

// EL BOTON FUE COULTADO PORQUE YA NO SE USARÁ
// function venta_form(credito_id) {
//   $.ajax({
//     type: "POST",
//     url: VISTA_URL + "gasto/cgv_venta_form.php",
//     async: true,
//     dataType: "html",
//     data: {
//       credito_id: credito_id,
//     },
//     beforeSend: function () {
//       $("#h3_modal_title").text("Cargando Formulario");
//       $("#modal_mensaje").modal("show");
//     },
//     success: function (html) {
//       $("#div_modal_venta_form").html(html);
//       $("#div_modal_venta").modal("show");
//       $("#modal_mensaje").modal("hide");

//       modal_width_auto("div_modal_venta", 30);
//       //funcion js para agregar un largo automatico al modal, al abrirlo
//       modal_height_auto("div_modal_venta"); //funcion encontrada en public/js/generales.js
//       modal_hidden_bs_modal("div_modal_venta", "limpiar");
//     },
//   });
// }