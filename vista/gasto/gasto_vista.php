<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<!-- <small>< ?php echo $menu_des; ?></small> -->
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Operaciones</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8')); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-md-12">
						<span><b>Filtros</b></span>
						<div class="panel panel-primary" id="div_filtros_cgv">
							<div class="panel-body">
								<?php include VISTA_URL . 'gasto/gasto_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="cgv_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>
				<div class="row">
					<input type="hidden" id="hdd_datatable_fil">
					<div class="col-sm-12">
						<div id="div_cgv_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
							<?php //require_once('cgv_tabla.php'); ?>
						</div>
					</div>
				</div>
			</div>

			<div id="div_modal_liquidacion">
				<!-- INCLUIMOS EL MODAL PARA VENTA POR CREDITO ID-->
			</div>

			<!-- <div id="div_modal_preparar_form">  || ANULADO POR MOTIVO DE CAMBIO DE LA FUNCION -->
				<!-- INCLUIMOS EL MODAL PARA VENTA POR CREDITO ID-->
			<!-- </div> -->

			<div id="div_modal_ajustar_form">
				<!-- INCLUIMOS EL MODAL PARA VENTA POR CREDITO ID-->
			</div>

			<div id="div_modal_venta_form">
				<!-- INCLUIMOS EL MODAL PARA VENTA POR CREDITO ID-->
			</div>
			<div id="div_modal_tbl_gastos">
				<!-- INCLUIMOS EL MODAL PARA TABLA GASTOS POR CREDITO ID-->
			</div>
			<div id="div_modal_tbl_gastopagos">
				<!-- INCLUIMOS EL MODAL PARA TABLA PAGOS DE GASTO POR GASTO ID-->
			</div>
			<div id="div_modal_gasto_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DE GASTO-->
			</div>
			<div id="div_modal_proveedor_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO/EDICION DE PROVEEDOR-->
			</div>
			<div id="div_modal_pago_form">
				<!-- INCLUIMOS EL MODAL PARA EL FORMULARIO DE PAGAR-->
			</div>
			<div id="div_modal_gasto_historial_form">
				<!-- INCLUIMOS EL MODAL PARA EL HISTORIAL DE CADA GASTO-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<div id="div_modal_upload_form">
				<!-- INCLUIMOS EL MODAL PARA SUBIR IMAGEN EN GASTOPAGO-->
			</div>
      		<div id="div_modal_imagen_form">
				<!-- INCLUIMOS EL MODAL PARA VER IMAGEN EN GASTOPAGO-->
			</div>
			<div id="div_modal_gasto_devolucion">
				<!-- INCLUIMOS EL MODAL PARA VER IMAGEN EN GASTOPAGO-->
			</div>
			
			<div id="div_filestorage_form"> <!-- FILESTORAGE_FORM -->
				<div id="modal_registro_filestorage"> </div>
			</div>

			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>