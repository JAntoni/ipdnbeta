$(document).ready(function () {
  console.log('aqiii devol  2222');
  $('.moneda3').autoNumeric({
    aSep: ',',
    aDec: '.',
    //aSign: 'S/. ',
    //pSign: 's',
    vMin: '0.00',
    vMax: '99999.99'
  });

  $('#devolucion_picker').datepicker({
		language: 'es',
		autoclose: true,
		format: "dd-mm-yyyy",
		endDate : new Date()
  });

  $("#cmb_gasto_lugpag").change(function(){
    var lugar = parseInt($(this).val());
    console.log('luugar ' + lugar)
    if(lugar == 2){
      $('.devolucion_banco').show(300);
      $('#ingreso_oficina').hide(300);
    }
    else{
      $('.devolucion_banco').hide(300);
      $('#ingreso_oficina').show(300);
    }
  });

  $('#txt_devolucion_mon, #txt_devolucion_comi').change(function(event) {
    var monto_deposito = Number($('#txt_devolucion_mon').autoNumeric('get'));
    var monto_comision = Number($('#txt_devolucion_comi').autoNumeric('get'));
    var saldo = 0;

    saldo = monto_deposito - monto_comision;

    if(monto_deposito > 0 && monto_comision >= 0 && saldo > 0){
      $('#txt_devolucion_montot').autoNumeric('set',saldo.toFixed(2));
    }
    else{
      $('#txt_devolucion_montot').autoNumeric('set',0);
    }
  });

  $("#form_devolucion").validate({
    submitHandler: function() {    
      var lugar = parseInt($('#cmb_gasto_lugpag').val());
      var importe = $('#txt_devolucion_imp').val() || 0;
      var deposito = Number($('#txt_devolucion_montot').val());
      var faltante = Number($('#txt_devolucion_falta').val());

      console.log('lugar: ' + lugar + ' // importe: ' + importe + ' // deposito: ' + deposito);
      

      if(lugar == 1 && importe <= 0){
        swal_warning("AVISO", 'DEBES INGRESAR UN MONTO PARA LA DEVOLUCION', 5000);
        return false;
      }
      if(lugar == 2 && deposito <= 0){
        swal_warning("AVISO", 'DEBES INGRESAR EL MONTO DEPOSITADO PARA VALIDAR', 5000);
        return false;
      }

      if(lugar == 1 && importe > faltante){
        swal_warning("AVISO", 'INGRESO EN OFICINA: Estás ingresando un monto mayor al monto restante, ingresa el valor restante. Restante: ' + faltante +', ingresado: ' + importe, 9000);
        return false;
      }
      if(lugar == 2 && deposito > faltante){
        swal_warning("AVISO", 'DEPOSITO: Estás ingresando un monto de DEPOSITO mayor al monto restante, ingresa el valor restante. Restante: ' + faltante +', ingresado: ' + deposito, 9000);
        return false;
      }

      $.ajax({
        type: "POST",
        url:  VISTA_URL+"gasto/gasto_controller.php",
        async:true,
        dataType: "json",
        data: $("#form_devolucion").serialize(),
        beforeSend: function() {
        },
        success: function(data){						
          if(parseInt(data.estado ) > 0){
            $('#modal_gasto_devolucion').modal('hide');
            notificacion_success('MENSAJE IMPORTANTE: ' + data.mensaje, 6000)
            completar_tabla_gastos();
          }
          else{
            swal_warning("AVISO",data.mensaje, 5000);
          }
        },
        complete: function(data){
          console.log(data);
        },
        error: function(data){
          swal_warning("ERROR REVISA LA CONSOLA", 'REVISA CON SISTEMAS', 3000);
          console.log(data)
        }
      });
		},
		rules: {
			txt_devolucion_imp: {
				required: function(){
          var lugar = parseInt($('#cmb_gasto_lugpag').val());
          if(lugar == 1)
            return true;
          else
            return false;
        }
			},
      cmb_cuedep_id: {
        required: function(){
          var lugar = parseInt($('#cmb_gasto_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      },
      txt_devolucion_mon: {
        required: function(){
          var lugar = parseInt($('#cmb_gasto_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      },
      txt_devolucion_comi: {
        required: function(){
          var lugar = parseInt($('#cmb_gasto_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      },
      txt_devolucion_numope: {
        required: function(){
          var lugar = parseInt($('#cmb_gasto_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      },
      txt_devolucion_fec: {
        required: function(){
          var lugar = parseInt($('#cmb_gasto_lugpag').val());
          if(lugar == 1)
            return false;
          else
            return true;
        }
      }
		},
    messages: {
      txt_devolucion_imp: {
        required: '* Ingresa el monto'
      },
      cmb_cuedep_id: {
        required: 'Numero de cuenta'
      },
      txt_devolucion_mon: {
        required: 'Monto depositado'
      },
      txt_devolucion_comi: {
        required: 'Comisión cobrada'
      },
      txt_devolucion_numope: {
        required: 'Número de operación'
      },
      txt_devolucion_fec: {
        required: 'Ingresa la fecha de Depósito'
      }
		}
	});
});