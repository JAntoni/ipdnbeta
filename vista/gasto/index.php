<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
	echo 'Terminó la sesión';
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php include(VISTA_URL . 'templates/head.php'); ?>
		<title><?php echo ucwords(mb_strtolower($menu_tit)); ?></title>
		<style>
			.panel-body {
				padding-top: 10px;
				padding-bottom: 0px;
			}

			.product-list-in-box>.item {
				border-bottom: 2px solid #5A7D6F;
			}

			.two-fields {
				width: 100%;
			}

			.two-fields .input-group {
				width: 100%;
			}
			input[type="file"]::file-selector-button{
				font-weight: bold;
				background-color: #4264ad;
				color: #fff;
				padding: 0.9em;
				border: none;
				border-radius: 3px;
				float: right;
				cursor: pointer;
				transition: 1s;
				margin-top: -5px;
				margin-right: -11px;
				font-size: 10px;
			}
		</style>
		<link href="./public/css/font-awesome-all-5.css" rel="stylesheet" type="text/css" />
	</head>
	<body <?php echo 'class="' . CLASE_BODY . ' ' . $usuario_tem . '"'; ?>>

		<!-- value="?php echo date('Y-m-d'); ?>" -->

		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
			<?php include(VISTA_URL . 'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
			<?php include(VISTA_URL . 'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
			<?php include('gasto_vista.php'); ?>
			<!-- INCLUIR FOOTER-->
			<?php include(VISTA_URL . 'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php include(VISTA_URL . 'templates/script.php'); ?>
		
		<script type="text/javascript" src="<?php echo VISTA_URL . 'gasto/gasto.js?ver=111223'; ?>"></script>
	</body>
</html>