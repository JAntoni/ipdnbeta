<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('Gasto.class.php');
$oGasto = new Gasto();
$listo = 1;

$usuario_action = $_POST['action'];
$titulo = '';
if ($usuario_action == 'I') {
    $titulo = 'Registrar gasto';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar gasto';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar gasto';
} elseif ($usuario_action == 'L') {
    $titulo = 'Gasto registrado';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
$action = devuelve_nombre_usuario_action($usuario_action);

$gasto_id = intval($_POST["gasto_id"]);
$fecha_gasto = date('d-m-Y');

$credito_id = intval($_POST['credito_id']);
$moneda_id = intval($_POST['moneda_id']);
$moneda_nom = trim($_POST['moneda_nom']);
$proveedor_id = 0;

if ($gasto_id > 0) {
    $where[0]['column_name'] = 'g.tb_gasto_id';
    $where[0]['param0'] = $gasto_id; //solo creditos garveh
    $where[0]['datatype'] = 'INT';

    $inner[0]['alias_columnasparaver'] = 'p.tb_proveedor_nom, p.tb_proveedor_doc';
    $inner[0]['tipo_union'] = 'LEFT';
    $inner[0]['tabla_alias'] = 'tb_proveedor p';
    $inner[0]['columna_enlace'] = 'g.tb_proveedor_id';
    $inner[0]['alias_columnaPK'] = 'p.tb_proveedor_id';

    $inner[1]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
    $inner[1]['tipo_union'] = 'LEFT';
    $inner[1]['tabla_alias'] = 'tb_moneda mon';
    $inner[1]['columna_enlace'] = 'g.tb_moneda_id';
    $inner[1]['alias_columnaPK'] = 'mon.tb_moneda_id';

    $res = $oGasto->listar_todos($where, $inner, array());
    if($res['row_count'] > 0){
        $gasto = $res['data'][0];
        if($gasto['tb_gasto_tipgas'] == 1){
            $tipo_gasto[1] = 'selected';
        } elseif ($gasto['tb_gasto_tipgas'] == 2) {
            $tipo_gasto[2] = 'selected';
        }
        $proveedor_id = $gasto['tb_proveedor_id'];
        $proveedor_nom = $gasto['tb_proveedor_doc'].' - '.$gasto['tb_proveedor_nom'];
        $cantidad = floatval($gasto['tb_gasto_can']);
        $moneda_id = $gasto['tb_moneda_id'];
        $pre_unit = mostrar_moneda($gasto['tb_gasto_pvt']);
        $pre_total = mostrar_moneda($gasto['tb_gasto_ptl']);
        $desc = $gasto['tb_gasto_des'];
    }
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_gasto_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php if ($listo == 0) { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4>EN DESARROLLO. PRONTO ACTIVO</h4>
                </div>
            <?php } else { ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title"><?php echo "$titulo del crédito CGV-$credito_id"; ?></h4>
                </div>
                <form id="form_gasto" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="action" id="action" value="<?php echo $action;?>">
                        <input type="hidden" name="hdd_gasto_id" id="hdd_gasto_id" value="<?php echo $gasto_id;?>">
                        <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id;?>">
                        <input type="hidden" name="hdd_gasto_reg" id="hdd_gasto_reg" value='<?php echo json_encode($gasto);?>'>

                        <div class="box box-primary shadow">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="cbo_gasto_tipo">Tipo de Gasto:</label>
                                    <select name="cbo_gasto_tipo" id="cbo_gasto_tipo" class="form-control input-sm">
                                        <option value="0">..SELECCIONE..</option>
                                        <option value="1" style="font-weight: bold;" <?php echo $tipo_gasto[1];?>>Producto</option>
                                        <option value="2" style="font-weight: bold;" <?php echo $tipo_gasto[2];?>>Servicio</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="hidden" id="hdd_proveedor_id" name="hdd_proveedor_id" value="<?php echo $proveedor_id;?>">
                                    <label style="font-size: 11.5px;">Proveedor:</label>
                                    <div id="proveedor-grp" class="input-group">
                                        <input type="text" id="txt_proveedor" name="txt_proveedor" placeholder="Ruc o Nombre del Proveedor" class="form-control mayus input-sm ui-autocomplete-input" value="<?php echo $proveedor_nom;?>" autocomplete="off">
                                        <?php
                                        if (in_array($_SESSION['usuariogrupo_id'], array(2, 6))) { //2: grupo gerencia, 6: grupo contabilidad ?>
                                            <span class="input-group-btn">
                                                <button id="btn_mod" class="btn btn-primary btn-sm" type="button" onclick="proveedor_form('M', 0)" title="MODIFICAR PROVEEDOR">
                                                    <span class="fa fa-pencil icon"></span>
                                                </button>
                                            </span>
                                            <span class="input-group-btn">
                                                <button id="btn_agr" class="btn btn-primary btn-sm" type="button" onclick="proveedor_form('I', 0)" title="AGREGAR PROVEEDOR">
                                                    <span class="fa fa-plus icon"></span>
                                                </button>
                                            </span>
                                        <?php } ?>
                                        <span class="input-group-btn">
                                            <button id="btn_limpiar_prov" class="btn btn-primary btn-sm" type="button" onclick="limpiar_proveedor()" title="LIMPIAR DATOS DEL PROVEEDOR">
                                                <span class="fa fa-eraser icon"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="txt_gasto_cantidad">Cantidad:</label>
                                    <input type="text" name="txt_gasto_cantidad" id="txt_gasto_cantidad" class="form-control mayus input-sm cant para_calc" placeholder="Cantidad" value="<?php echo $cantidad;?>">
                                </div>
                                <div class="col-md-4">
                                    <label for="cmb_moneda_id" class="control-label">Moneda:</label>
                                    <select class="form-control input-sm" name="cmb_moneda_id" id="cmb_moneda_id">
                                        <?php require_once('../moneda/moneda_select.php') ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="txt_gasto_preunit">Precio Unitario:</label>
                                    <input type="text" name="txt_gasto_preunit" id="txt_gasto_preunit" class="form-control mayus input-sm moneda para_calc" placeholder="P. Unitario" value="<?php echo $pre_unit;?>">
                                </div>
                                <div class="col-md-4">
                                    <label for="txt_gasto_pretotal">Precio Total:</label>
                                    <input type="text" name="txt_gasto_pretotal" id="txt_gasto_pretotal" class="form-control mayus input-sm moneda disabled" placeholder="P. Total" value="<?php echo $pre_total;?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="txt_descripcion">Descripción:</label>
                                    <textarea class="form-control mayus input-sm" name="txt_descripcion" id="txt_descripcion" rows="2" cols="85" placeholder="Descripción"><?php echo $desc;?></textarea>
                                </div>
                            </div>
                        </div>

                        <!--- MESAJES DE GUARDADO -->
                        <div class="callout callout-info" id="gasto_mensaje" style="display: none;">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="f1-buttons">
                            <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_gasto">Guardar</button>
                            <?php endif ?>
                            <?php if ($usuario_action == 'E') : ?>
                                <button type="submit" class="btn btn-info" id="btn_guardar_gasto">Aceptar</button>
                            <?php endif ?>
                            <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/gasto/gasto_form.js?ver=617856687'; ?>"></script>