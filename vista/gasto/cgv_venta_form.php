<?php
require_once('../funciones/funciones.php');
$credito_id = intval($_POST['credito_id']);
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_venta" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo "Venta de CGV-$credito_id"?></h4>
                <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id; ?>">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info shadow" style="padding: 0px;">
                            <div class="panel-heading">DATOS CLIENTE</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="cmb_mediocom_id" class="control-label">Cliente:</label>
                                    <input type="text" name="txt_cliente_nom" id="txt_cliente_nom" class="input-sm form-control mayus disabled" value="DANIEL SALOMON ODAR CORDOVA">
                                </div>
                                <div class="form-group">
                                    <label for="cmb_creditotipo_id" class="control-label">Crédito de Interés</label>
                                    <select name="cmb_creditotipo_id" id="cmb_creditotipo_id" class="form-control input-sm">
                                        <option value="0"></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="f1-buttons">
                    <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo "vista/gasto/cgv_venta_form.js?ver=087";?>"></script>