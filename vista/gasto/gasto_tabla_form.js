var datatable_global_gastos;

$(document).ready(function () {
  console.log("asjjasja saquiiiii 33333");
  $("#cbo_gastofil_moneda").change(function () {
    completar_tabla_gastos();
  });

  setTimeout(function () {
    $("#cbo_gastofil_moneda").val(1).change();
  }, 150);
});

function completar_tabla_gastos() {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gasto/gasto_tabla_form_vista.php",
    async: true,
    dataType: "html",
    data: {
      credito_id: $("#hdd_credito_id").val(),
      moneda_id: $("#cbo_gastofil_moneda option:selected").val(),
      moneda_nom: $("#cbo_gastofil_moneda option:selected").text(),
    },
    success: function (data) {
      $("#div_html_tablagasto").html(data);

      $(
        'input[type="checkbox"].flat-green, input[type="radio"].flat-green'
      ).iCheck({
        checkboxClass: "icheckbox_flat-green",
        radioClass: "iradio_flat-green",
      });

      estilos_datatable_gastotabla();
    },
  });
}

function estilos_datatable_gastotabla() {
  datatable_global_gastos = $("#tbl_gastos").DataTable({
    pageLength: 25,
    responsive: true,
    language: {
      lengthMenu: "Mostrar _MENU_ registros por página",
      zeroRecords: "Ninguna coincidencia para la búsqueda",
      info: "Mostrado _END_ registros",
      infoEmpty: "Ningún registro disponible",
      infoFiltered: "(filtrado de _MAX_ registros totales)",
      search: "Buscar:",
    },
    order: [],
    columnDefs: [{ targets: [8, 9, 10, 11], orderable: false }],
  });
  datatable_texto_filtrar_gastotabla();
}

function datatable_texto_filtrar_gastotabla() {
  $('input[aria-controls*="tbl_gastos"]')
    .attr("id", "txt_datatable_gasto_fil")
    .attr("placeholder", "escriba para buscar");

  $("#txt_datatable_gasto_fil").keyup(function (event) {
    $("#hdd_datatable_gastos_fil").val(this.value);
  });

  var text_fil = $("#hdd_datatable_gastos_fil").val();
  if (text_fil) {
    $("#txt_datatable_gasto_fil").val(text_fil);
    datatable_global_gastos.search(text_fil).draw();
  }
}

function gasto_historial_form(gasto_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gasto/gasto_hist_form.php",
    async: true,
    dataType: "html",
    data: {
      gasto_id: gasto_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_gasto_historial_form").html(html);
      $("#modal_gasto_historial_form").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("modal_gasto_historial_form", 40);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("modal_gasto_historial_form"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_gasto_historial_form', 'limpiar');
    },
  });
}

function form_gasto(action, gasto_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gasto/gasto_form.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      gasto_id: gasto_id,
      credito_id: $("#hdd_credito_id").val(),
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_gasto_form").html(html);
      $("#modal_gasto_form").modal("show");
      $("#modal_mensaje").modal("hide");

      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal("modal_gasto_form", "limpiar"); //funcion encontrada en public/js/generales.js
      modal_width_auto("modal_gasto_form", 40);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("modal_gasto_form"); //funcion encontrada en public/js/generales.js
    },
  });
}

function pagar(action, gasto_id, gastopago_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gastopago/gastopago_form.php",
    async: true,
    dataType: "html",
    data: {
      action: action,
      gasto_id: gasto_id,
      gastopago_id: gastopago_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_pago_form").html(html);
      $("#modal_gastopago_form").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_hidden_bs_modal("modal_gastopago_form", "limpiar"); //funcion encontrada en public/js/generales.js
      modal_width_auto("modal_gastopago_form", 30);
      modal_height_auto("modal_gastopago_form"); //funcion encontrada en public/js/generales.js
    },
  });
}

function listar_pagos(gasto_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "gastopago/gastopago_tabla_form.php",
    async: true,
    dataType: "html",
    data: {
      gasto_id: gasto_id,
    },
    beforeSend: function () {
      $("#h3_modal_title").text("Cargando Formulario");
      $("#modal_mensaje").modal("show");
    },
    success: function (html) {
      $("#div_modal_tbl_gastopagos").html(html);
      $("#div_modal_gastopago_tabla").modal("show");
      $("#modal_mensaje").modal("hide");

      modal_width_auto("div_modal_gastopago_tabla", 50);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto("div_modal_gastopago_tabla"); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('div_modal_gastopago_tabla', 'limpiar');
    },
  });
}

function gasto_devolucion(){
  
  var valoresMarcados = [];
  var tipo_cambio = $('#hdd_tipo_cambio_venta').val();
  
  if(parseInt(tipo_cambio) <= 0){
    alerta_warning('AVISO', 'Debes registrar el TIPO DE CAMBIO para poder realizar el INGRESO POR DEVOLUCIÓN DE GASTO VEHICULAR');
    return false;
  }

  $('input[type="checkbox"].flat-green:checked').each(function() {
    valoresMarcados.push($(this).val());
  });

  if(valoresMarcados.length <= 0){
    alerta_warning('AVISO', 'Debes seleccionar al menos un gasto para poder realizar el INGRESO POR DEVOLUCIÓN DE GASTO VEHICULAR');
    return false;
  }

  $.ajax({
    type: "POST",
    url: VISTA_URL + "gasto/gasto_devolucion.php",
    async: true,
    dataType: "html",
    data: ({
      array_gasto_id: valoresMarcados,
      cliente_nom: $('#hdd_gasto_cliente_nom').val(),
      cliente_id: $('#hdd_gasto_cliente_id').val(),
      tipo_cambio: tipo_cambio,
      credito_id: $('#hdd_credito_id').val(),
      vehiculo_pla: $('#hdd_gasto_credito_pla').val()
    }),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (html) {
      $('#modal_mensaje').modal('hide');
      $('#div_modal_gasto_devolucion').html(html);
      $('#modal_gasto_devolucion').modal('show');
      modal_hidden_bs_modal('modal_gasto_devolucion', 'limpiar');
    }
  });
}
