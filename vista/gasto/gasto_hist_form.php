<?php
require_once('Gasto.class.php');
$oGasto = new Gasto();

$gasto_id = intval($_POST['gasto_id']);

$where[0]['column_name'] = 'g.tb_gasto_id';
$where[0]['param0'] = $gasto_id;
$where[0]['datatype'] = 'INT';

$res = $oGasto->listar_todos($where, array(), array());

$historial = '';

if($res['row_count'] > 0){
    $gasto = $res['data'][0];
    $gasto_hist = $gasto['tb_gasto_his'];
    $gasto_hist = explode('<br>',$gasto_hist);
    for($i = 0; $i < count($gasto_hist) - 1; $i++){
        $historial .=
            "<li class=\"item\">
                <span style=\"font-size: 11px;\">
                    {$gasto_hist[$i]}
                </span>
            </li>";
    }
    $gasto = null;
    $gasto_hist = null;
}
$res = null;
?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_gasto_historial_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">INFORMACION DE GASTOS DE UNIDAD</h4>
            </div>

            <div class="modal-body">
                <div>
                    <div class="box box-danger shadow">
                        <div class="box-body">
                            <ul id="ul_historial_gastos" name="ul_historial_gastos" class="products-list product-list-in-box">
                                <?php echo $historial;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>