<?php
require_once("../creditogarveh/Creditogarveh.class.php");
$oCredito = new Creditogarveh();
require_once("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once('Gasto.class.php');
$oGasto = new Gasto();
require_once('../funciones/funciones.php');

$credito_id = intval($_POST['credito_id']);
//$creditotipo_id = 3;
$cliente_nom = '';
$cliente_id = 0;
$vehiculo_pla = 'SIN PLACA';

$result = $oCredito->mostrarUno($credito_id);
if ($result['estado'] == 1) {
  $cliente_nom = $result['data']['tb_cliente_nom'] . ' | ' . $result['data']['tb_cliente_emprs'];
  $cliente_id = $result['data']['tb_cliente_id'];
  $vehiculo_pla = $result['data']['tb_credito_vehpla'];
}
$result = null;

?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_gasto_tabla" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">x</span>
        </button>
        <h4 class="modal-title"><?php echo "Gastos en CGV-$credito_id" ?></h4>
        <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id; ?>">
        <input type="hidden" id="hdd_gasto_cliente_nom" value="<?php echo $cliente_nom; ?>" />
        <input type="hidden" id="hdd_gasto_cliente_id" value="<?php echo $cliente_id; ?>" />
        <input type="hidden" id="hdd_gasto_credito_pla" value="<?php echo $vehiculo_pla; ?>" />
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <span><b>Filtros</b></span>
            <div class="panel panel-danger" id="div_filtros_gastos">
              <div class="panel-body">
                <div class="row">
                  <div class="form-group form-inline col-md-10">
                    <button onclick="form_gasto('I', 0)" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Agregar</button>

                    <label for="cbo_gastofil_moneda">Moneda: </label>
                    <select name="cbo_gastofil_moneda" id="cbo_gastofil_moneda" class="form-control input-sm">
                      <?php require_once('../moneda/moneda_select.php'); ?>
                    </select>

                  </div>
                  <div class="col-md-2">
                    <button onclick="gasto_devolucion()" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Ingresar Devolución</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <input type="hidden" id="hdd_datatable_gastos_fil">

          <div class="col-md-12">
            <!-- tabla -->
            <div id="div_html_tablagasto" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="f1-buttons">
          <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/gasto/gasto_tabla_form.js?ver=111233'; ?>"></script>