<?php
require_once('Gasto.class.php');
$oGasto = new Gasto();

$gasto_est[1] = "pendiente";
$gasto_est[2] = "pagado";
$gasto_est[3] = "en pago parcial";

$creditotipo_id = (empty($creditotipo_id))? intval($_POST['creditotipo_id']) : intval($creditotipo_id);
$credito_id = (empty($credito_id))? intval($_POST['credito_id']) : intval($credito_id);
if(empty($_POST["gasto_id"]) && empty($gasto_id)){
	$gasto_id = 0;
} elseif (!empty($gasto_id)){
	$gasto_id = intval($gasto_id);
} else {
	$gasto_id = intval($_POST["gasto_id"]);
}

$option = '';

$parametros[0]['column_name'] = 'g.tb_gasto_cretip';
$parametros[0]['param0'] = $creditotipo_id;
$parametros[0]['datatype'] = 'INT';

$parametros[1]['conector'] = 'AND';
$parametros[1]['column_name'] = 'g.tb_gasto_est';
$parametros[1]['param1'] = '> 1';

if(!empty($credito_id)){
	$parametros[2]['conector'] = 'AND';
	$parametros[2]['column_name'] = 'g.tb_credito_id';
	$parametros[2]['param2'] = $credito_id;
	$parametros[2]['datatype'] = 'INT';
}

$inner[0]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
$inner[0]['tipo_union'] = 'INNER';
$inner[0]['tabla_alias'] = 'tb_moneda mon';
$inner[0]['columna_enlace'] = 'g.tb_moneda_id';
$inner[0]['alias_columnaPK'] = 'mon.tb_moneda_id';

$inner[1]['alias_columnasparaver'] = 'p.tb_proveedor_doc';
$inner[1]['tipo_union'] = 'INNER';
$inner[1]['tabla_alias'] = 'tb_proveedor p';
$inner[1]['columna_enlace'] = 'g.tb_proveedor_id';
$inner[1]['alias_columnaPK'] = 'p.tb_proveedor_id';

$otros["orden"]["column_name"] = 'tb_gasto_fecreg';
$otros["orden"]["value"] = 'DESC';

//PRIMER NIVEL
$result = $oGasto->listar_todos($parametros, $inner, $otros);

if ($result['estado'] == 1) {
	$option = '<option value="0" style="font-size: 10px">..SELECCIONE..</option>';
	foreach ($result['data'] as $key => $value) {
		$selected = '';
		$detalle = (strlen($value['tb_gasto_des']) > 150) ? substr($value['tb_gasto_des'], 0, 180)."[...]" : $value['tb_gasto_des'];

		if ($gasto_id == $value['tb_gasto_id'])
			$selected = 'selected';

		//los valores de este option son usados en compracontadoc_elegirgasto.js, no mover
		$option .= '<option value="' . $value['tb_gasto_id'] . '" style="font-size: 10px" ' . $selected . '>'.$value['tb_moneda_nom'].$value['tb_gasto_ptl']." | Estado: {$gasto_est[$value['tb_gasto_est']]} | $detalle | Proveedor id: {$value['tb_proveedor_id']} | Proveedor RUC: {$value['tb_proveedor_doc']}".'</option>';
	}
}
$result = NULL;

//FIN PRIMER NIVEL
echo $option;
?>