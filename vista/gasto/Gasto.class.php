<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Gasto extends Conexion{
    public $tb_gasto_id;//def
    public $tb_gasto_fecreg;//def
    public $tb_gasto_tipgas;//NEEDED: 1 producto, 2 servicios
    public $tb_gasto_tippro;//NEEDED: def siempre 1 tipo vehicular
    public $tb_credito_id;//NEEDED
    public $tb_gasto_cretip;//NEEDED: def siempre 3 garveh
    public $tb_gasto_des;//NEEDED
    public $tb_proveedor_id;//NEEDED
    public $tb_gasto_can;//NEEDED
    public $tb_gasto_pvt;//NEEDED
    public $tb_gasto_ptl;//NEEDED
    public $tb_gasto_est;//def 1 pendiente pago, 2 pagado, 3 pago parcial
    public $tb_gasto_his;//NEEDED
    public $tb_moneda_id;//NEEDED def 1 sol, 2 dolares

    public function listar_todos($array, $array1, $otros_param){
        //array: bidimensional con formato $column_name, $param.$i, $datatype, $conector (and o or) cuando sea mayor que cero
        //array1: $iner o left, $nombretb alias, $columna_deenlace, $alias.columnaPK, $alias_columnasparaver
        //otros_param: arreglo para funciones diversas como el order by, limit, etc. los keys se definen de acuerdo al uso (ejm: ordenar: ["orden"]["column_name"], ["orden"]["value"])
        try {
            $sql = "SELECT g.*";
            if (!empty($array1)) {
                for ($j = 0; $j < count($array1); $j++) {
                    $sql .= ",\n" . $array1[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_gasto g\n";
            if (!empty($array1)) {
                for ($k = 0; $k < count($array1); $k++) {
                    $sql .= $array1[$k]['tipo_union'] . " JOIN " . $array1[$k]['tabla_alias'] . " ON " . $array1[$k]['columna_enlace'] . " = " . $array1[$k]['alias_columnaPK'] . "\n";
                }
            }
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i])) {
                    $sql .= ($i > 0) ? " {$array[$i]["conector"]} " : "WHERE ";

                    if(!empty($array[$i]["column_name"])){
                        if (stripos($array[$i]["column_name"], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$array[$i]["column_name"]}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$array[$i]["column_name"]}";
                        }

                        if(!empty($array[$i]["datatype"])){
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " ". $array[$i]["param$i"];
                        }
                    } else {
                        $sql .= " ". $array[$i]["param$i"];
                    }
                }
            }
            if (!empty($otros_param["group"])) {
                $sql .= "\nGROUP BY {$otros_param["group"]["column_name"]}";
            }
            if (!empty($otros_param["orden"])) {
                $sql .= "\nORDER BY {$otros_param["orden"]["column_name"]} {$otros_param["orden"]["value"]}";
            }
            if (!empty($otros_param["limit"])) {
                $sql .= "\nLIMIT {$otros_param["limit"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i]["column_name"]) && !empty($array[$i]["datatype"])) {
                    $_PARAM = strtoupper($array[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $array[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            $retorno["sql"] = $sql;
            $retorno["row_count"] = $sentencia->rowCount();
            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }
    /* daniel odar 24-05-23 */
    public function listar_cgv($cliente_id, $placa){
        try {
            $where_cliente  = empty($cliente_id) ? '' : ' AND cli.tb_cliente_id = :param_opc1';
            $where_placa    = empty($placa) ? '' : ' AND cgv.tb_credito_vehpla = :param_opc2';
            $sql = "SELECT
                        cgv.tb_credito_feccre, cgv.tb_credito_vehpla, cgv.tb_credito_vehano,
                        cgv.tb_credito_id, cgv.tb_cuotatipo_id, cgv.tb_credito_preaco, cli.tb_cliente_nom,
                        mo.tb_moneda_nom, vma.tb_vehiculomarca_nom, vmo.tb_vehiculomodelo_nom,
                        ct.tb_cuotatipo_nom, cgv.tb_credito_tip, cgv.tb_credito_est
                    FROM tb_creditogarveh cgv
                    LEFT JOIN tb_cliente cli on cli.tb_cliente_id = cgv.tb_cliente_id
                    LEFT JOIN tb_moneda mo on mo.tb_moneda_id = cgv.tb_moneda_id
                    LEFT JOIN tb_vehiculomarca vma on cgv.tb_vehiculomarca_id = vma.tb_vehiculomarca_id
                    LEFT JOIN tb_vehiculomodelo vmo on cgv.tb_vehiculomodelo_id = vmo.tb_vehiculomodelo_id
                    LEFT JOIN tb_cuotatipo ct on ct.tb_cuotatipo_id = cgv.tb_cuotatipo_id
                    WHERE tb_credito_xac = 1 $where_cliente$where_placa
                    AND cgv.tb_credito_tip in (1, 2, 4)
                    ORDER BY cgv.tb_credito_feccre ASC;";
            
            $sentencia = $this->dblink->prepare($sql);
            if(!empty($cliente_id)){
                $sentencia->bindParam(':param_opc1',$cliente_id,PDO::PARAM_INT);
            }
            if(!empty($placa)){
                $sentencia->bindParam(':param_opc2',$placa,PDO::PARAM_STR);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function insertar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO tb_gasto (
                tb_gasto_tipgas,
                tb_gasto_tippro,
                tb_credito_id,
                tb_gasto_cretip,
                tb_gasto_des,
                tb_proveedor_id,
                tb_gasto_can,
                tb_gasto_pvt,
                tb_gasto_ptl,
                tb_gasto_his,
                tb_moneda_id)
              VALUES (
                :param0,
                1,
                :param1,
                :param2,
                :param3,
                :param4,
                :param5,
                :param6,
                :param7,
                :param8,
                :param9);";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tb_gasto_tipgas, PDO::PARAM_INT);
            $sentencia->bindParam(":param1", $this->tb_credito_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param2", $this->tb_gasto_cretip, PDO::PARAM_INT);
            $sentencia->bindParam(":param3", $this->tb_gasto_des, PDO::PARAM_STR);
            $sentencia->bindParam(":param4", $this->tb_proveedor_id, PDO::PARAM_INT);
            $sentencia->bindParam(":param5", $this->tb_gasto_can, PDO::PARAM_STR);
            $sentencia->bindParam(":param6", $this->tb_gasto_pvt, PDO::PARAM_STR);
            $sentencia->bindParam(":param7", $this->tb_gasto_ptl, PDO::PARAM_STR);
            $sentencia->bindParam(":param8", $this->tb_gasto_his, PDO::PARAM_STR);
            $sentencia->bindParam(":param9", $this->tb_moneda_id, PDO::PARAM_INT);

            $resultado['estado'] = $sentencia->execute();

            if ($resultado['estado'] == 1) {
                $resultado['nuevo'] = $this->dblink->lastInsertId();
                $resultado['mensaje'] = 'Gasto registrado correctamente';
                $this->dblink->commit();
            } else {
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    public function modificar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_gasto
              SET
                tb_gasto_tipgas = :param0,
                tb_proveedor_id = :param1,
                tb_gasto_can = :param2,
                tb_moneda_id = :param3,
                tb_gasto_pvt = :param4,
                tb_gasto_ptl = :param5,
                tb_gasto_des = :param6
              WHERE
                tb_gasto_id = :paramx;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->tb_gasto_tipgas, PDO::PARAM_INT);
            $sentencia->bindParam(':param1', $this->tb_proveedor_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param2', $this->tb_gasto_can, PDO::PARAM_STR);
            $sentencia->bindParam(':param3', $this->tb_moneda_id, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->tb_gasto_pvt, PDO::PARAM_STR);
            $sentencia->bindParam(':param5', $this->tb_gasto_ptl, PDO::PARAM_STR);
            $sentencia->bindParam(':param6', $this->tb_gasto_des, PDO::PARAM_STR);
            $sentencia->bindParam(':paramx', $this->tb_gasto_id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function modificar_campo($gasto_id, $gasto_columna, $gasto_valor, $param_tip){
        $this->dblink->beginTransaction();
        try {
            if (!empty($gasto_columna) && ($param_tip == 'INT' || $param_tip == 'STR')) {
                $sql = "UPDATE tb_gasto SET " . $gasto_columna . " = :gasto_valor WHERE tb_gasto_id = :gasto_id";

                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":gasto_id", $gasto_id, PDO::PARAM_INT);
                if ($param_tip == 'INT')
                    $sentencia->bindParam(":gasto_valor", $gasto_valor, PDO::PARAM_INT);
                else
                    $sentencia->bindParam(":gasto_valor", $gasto_valor, PDO::PARAM_STR);

                $result = $sentencia->execute();

                if ($result == 1)
                    $this->dblink->commit();

                return $result; //si es correcto el egreso retorna 1
            } else
                return 0;
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    public function eliminar_delete(){
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM tb_gasto WHERE tb_gasto_id = :param0;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tb_gasto_id, PDO::PARAM_INT);
            $result = $sentencia->execute();
            $this->dblink->commit();

            return $result; //si es correcto retorna 1
        } catch (Exception $e) {
            $this->dblink->rollBack();
            throw $e;
        }
    }

    /* Juan 08-08-2023 */
    public function listar_gastos_credito_tipo($credito_id, $creditotipo_id){
        try {
            $sql = "SELECT * FROM tb_gasto WHERE tb_credito_id =:credito_id AND tb_gasto_cretip =:creditotipo_id";
            
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':credito_id', $credito_id,PDO::PARAM_INT);
            $sentencia->bindParam(':creditotipo_id', $creditotipo_id,PDO::PARAM_INT);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay tipos de crédito registrados";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function mostrarUno($gasto_id){
      try {
        $sql = "SELECT * FROM tb_gasto WHERE tb_gasto_id =:gasto_id";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(':gasto_id', $gasto_id,PDO::PARAM_INT);

        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }
        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    public function agregar_historial($gasto_id, $historial){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_gasto
              SET
                tb_gasto_his = CONCAT(tb_gasto_his, '', :historial)
              WHERE
                tb_gasto_id = :gasto_id;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':gasto_id', $gasto_id, PDO::PARAM_INT);
            $sentencia->bindParam(':historial', $historial, PDO::PARAM_STR);

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    function gasto_total_por_credito_moneda($cre_id, $cre_tip, $mon_id)
    {
        try {
            $sql = "SELECT SUM(
                        CASE 
                                WHEN g.tb_moneda_id = 1 THEN (g.tb_gasto_ptl - g.tb_gasto_devo)
                                WHEN g.tb_moneda_id = 2 THEN (g.tb_gasto_ptl - g.tb_gasto_devo) * mc.tb_monedacambio_val
                                ELSE 0
                        END ) as gasto_total
                    FROM tb_gasto g
                    INNER JOIN tb_monedacambio mc ON DATE(g.tb_gasto_fecreg) = mc.tb_monedacambio_fec
                    WHERE g.tb_credito_id = :credito_id
                    AND g.tb_gasto_cretip = :credito_tip
                    AND g.tb_moneda_id = :moneda_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":credito_id", $cre_id, PDO::PARAM_INT);
            $sentencia->bindParam(":credito_tip", $cre_tip, PDO::PARAM_INT);
            $sentencia->bindParam(":moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetch();
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay gastos";
                $retorno["data"] = "";
            }

            return $retorno;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
//$retorno["mensaje"] = "exito || $sql";return $retorno;exit();
?>
