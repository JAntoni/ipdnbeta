$(document).ready(function () {
    //cargar_creditos()


    $("#txt_cliente_doc").autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON(
                    VISTA_URL + "cliente/cliente_autocomplete.php",
                    {term: request.term}, //
                    response
                    );
        },
        select: function (event, ui) {
            let nombrecompletos = ui.item.cliente_doc + " - " + ui.item.cliente_nom
            let clienteid = ui.item.cliente_id
            $('#hdd_cliente_id').val(clienteid);
            $('#txt_cliente_doc').val(nombrecompletos);
            //$('#txt_pro_nom').val(ui.item.tb_proveedor_nom);
            event.preventDefault();
            //$('#txt_pro_nom').focus();
            cargar_creditos(clienteid)
        }
    });

    $("#txt_proveedor").autocomplete({
		minLength: 1,
		source: function (request, response) {
			$.getJSON(
					VISTA_URL + "proveedor/proveedor_autocomplete.php",
					{term: request.term}, //
					response
					);
		},
		select: function (event, ui) {
			$('#hdd_proveedor_id').val(ui.item.tb_proveedor_id);
			$('#txt_proveedor').val(ui.item.tb_proveedor_doc+' - '+ui.item.tb_proveedor_nom);
			event.preventDefault();
			disabled($('input[id*="txt_proveedor"]'));
			disabled($('#btn_agr'));
			$('#txt_gasto_cantidad').focus();
		}
	});


    $('#form_gasto').validate({
		submitHandler: function () {
			if (parseInt($('#hdd_proveedor_id').val()) < 1) {
				alerta_error("SISTEMA", "ELIJA UN PROVEEDOR VALIDO");
				return;
			}
			$.ajax({
				type: "POST",
				url: VISTA_URL + "gasto/gasto_controller.php",
				async: true,
				dataType: "json",
				data: $("#form_gasto").serialize(),
				beforeSend: function () {
					$('#gasto_mensaje').show(400);
					$('#btn_guardar_gasto').prop('disabled', true);
				},
				success: function (data) {
					console.log(data);
					
					if (parseInt(data.estado) > 0) {
						//$('#gasto_mensaje').removeClass('callout-info').addClass('callout-success');
						//$('#gasto_mensaje').html(`<h4>${data.mensaje}</h4>`);
						if($.inArray($('#action').val(), ['insertar_gasto_rendicion']) > -1){
							disabled($('#form_gasto').find("button[class*='btn-sm'], select, input, textarea"));
						}
                        if(data.estado_new > 0){
							$('#gasto_mensaje').removeClass('callout-info').addClass('callout-success');
							$('#gasto_mensaje').html('EXITO: ' + data.mensaje2);
                            alerta_success("EXITO", data.mensaje2)
                            rendicioncuentas_tabla()
                        }else{
                            alerta_error("ERROR","Se registró el gasto pero no se asignó a rendicion")
                        }
					}
					else {
						$('#gasto_mensaje').removeClass('callout-info').addClass('callout-warning');
						$('#gasto_mensaje').html('Alerta: ' + data.mensaje);
						$('#btn_guardar_gasto').prop('disabled', false);
					}
				},
				error: function (data) {
					console.log(data);
					
					$('#gasto_mensaje').removeClass('callout-info').addClass('callout-danger')
					$('#gasto_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
				}
			});
		},
		rules: {
			cbo_gasto_tipo: {
				min: 1
			},
			txt_proveedor:{
				required: function(){
					return parseInt($('#hdd_proveedor_id').val()) < 1;
				}
			},
			txt_gasto_cantidad: {
				required: function(){
					var cantidad = parseInt($('#txt_gasto_cantidad').val());
					return isNaN(cantidad) || cantidad < 1;
				}
			},
			txt_gasto_preunit: {
				required: function(){
					var gasto_preunit = parseFloat($('#txt_gasto_preunit').val());
					return isNaN(gasto_preunit) || gasto_preunit < 0.01;
				}
			},
			txt_descripcion: {
				required: true,
				minlength: 5
			},
			cbo_cuenta_id: {
				required: true
			},
			cbo_subcuenta_id: {
				required: true
			}
		},
		messages: {
			cbo_gasto_tipo: {
				min: "Elija Tipo gasto"
			},
			txt_proveedor:{
				required: "Ingrese proveedor"
			},
			txt_gasto_cantidad: {
				required: "Ingrese cantidad"
			},
			txt_gasto_preunit: {
				required: "Ingrese precio unitario"
			},
			txt_descripcion: {
				required: "Ingrese descripción para el Gasto",
				minlength: "La descripción debe tener como mínimo 5 caracteres"
			},
			cbo_cuenta_id: {
				required: "Debe seleccionar cuenta"
			},
			cbo_subcuenta_id: {
				required: "Debe seleccionar subcuenta"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.parent("label"));
			} else if (element.parent("div.input-group").length == 1) {
				error.insertAfter(element.parent("div.input-group")[0]);
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});

    $('#cbo_cuenta_id').change(function () {
        SubCuenta_select_form($('#cbo_cuenta_id').val());
    });

    if ($("#cbo_forma_egreso option:selected").val() == 2) {
        $("div .num_ope").show();
    } else {
        $("div .num_ope").hide();
    }

    $("#cbo_forma_egreso").change(function () {
        var forma_id = $("#cbo_forma_egreso option:selected").val();
        if (forma_id == "2") {
            $("div .num_ope").show(200);
            $('#txt_numope').focus();
        } else {
            $("div .num_ope").hide(200);
            $('#txt_numope').val('');
        }
    });

    if ($('#che_emite_docsunat').prop('checked')) {
        $("div .emite_doc_sunat").show();
    } else {
        $("div .emite_doc_sunat").hide();
    }

    $("#che_emite_docsunat").on('ifChanged', function () {
        if (this.checked) {
            $(".emite_doc_sunat").show(200);
            $('#txt_nro_documento').focus();
        }
        else {
            $(".emite_doc_sunat").hide(200);
            $("#txt_nro_documento").val(null);
        }
    });

    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: `icheckbox_flat-green`,
        radioClass: `iradio_flat-green`
    });
});

function cargar_creditos(cliente_id) {
    console.log("CREDITOS");
    $.ajax({
      type: "POST",
      url: VISTA_URL + "creditogarveh/creditogarveh_select_retencion.php",
      async: true,
      dataType: "html",
      data: {
        cliente_id: cliente_id
      },
      beforeSend: function () {
        $("#cbo_credito_id").selectpicker('destroy');
        $("#cbo_credito_id").html('<option value="">Cargando...</option>');
      },
      success: function (html) {
        $("#cbo_credito_id").html(html);
      },
    });
}

function limpiar_proveedor(){
	$('#hdd_proveedor_id').val(0);
	$('#txt_proveedor').val('');

	$('#txt_proveedor').prop('disabled', false);
	$('#btn_agr').removeAttr('disabled');
	$('#txt_proveedor').focus();
}

function proveedor_form(usuario_act, proveedor_id) {
	var doc_proveedor_id = Number($('#hdd_proveedor_id').val());

	if (parseInt(doc_proveedor_id) <= 0 && usuario_act == 'M') {
		alerta_warning('Información', 'Debes buscar por documento o nombres');
		return false;
	}

	$.ajax({
		type: "POST",
		url: VISTA_URL + "proveedor/proveedor_form.php",
		async: true,
		dataType: "html",
		data: ({
			action: usuario_act, // PUEDE SER: L, I, M , E
			proveedor_id: doc_proveedor_id,
			vista: 'gasto'
		}),
		beforeSend: function () {
			$('#h3_modal_title').text('Cargando Formulario');
			$('#modal_mensaje').modal('show');
		},
		success: function (data) {
			$('#modal_mensaje').modal('hide');
			if (data != 'sin_datos') {
				$('#div_modal_proveedor_form').html(data);
				$('#modal_registro_proveedor').modal('show');

				//desabilitar elementos del form si es L (LEER)
				if (usuario_act == 'L' || usuario_act == 'E')
					form_desabilitar_elementos('form_proveedor'); //funcion encontrada en public/js/generales.js

				//funcion js para limbiar el modal al cerrarlo
				modal_hidden_bs_modal('modal_registro_proveedor', 'limpiar'); //funcion encontrada en public/js/generales.js
				//funcion js para agregar un largo automatico al modal, al abrirlo
				modal_height_auto('modal_registro_proveedor'); //funcion encontrada en public/js/generales.js
				//funcion js para agregar un ancho automatico al modal, al abrirlo
				modal_width_auto('modal_registro_proveedor', 75); //funcion encontrada en public/js/generales.js
			}
			else {
				//llamar al formulario de solicitar permiso
				var modulo = 'proveedor';
				var div = 'div_modal_proveedor_form';
				permiso_solicitud(usuario_act, proveedor_id, modulo, div); //funcion ubicada en public/js/permiso.js
			}
		},
		error: function (data) {
			alerta_error('ERROR', data.responseText)
		}
	});
}

function SubCuenta_select_form(cuenta_id) {
    $.ajax({
        type: "POST",
        url: VISTA_URL + "subcuenta/subcuenta_select.php",
        async: true,
        dataType: "html",
        data: ({
            cuenta_id: cuenta_id
        }),
        beforeSend: function () {
            $('#cbo_subcuenta_id').html('<option>Cargando...</option>');
        },
        success: function (data) {
            $('#cbo_subcuenta_id').html(data);
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
}