<?php
require_once '../../core/usuario_sesion.php';
require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');
require_once('Gasto.class.php');
$oGasto = new Gasto();

$credito_id = intval($_POST['credito_id']);
$moneda_id = intval($_POST['moneda_id']);
$moneda_nom = trim($_POST['moneda_nom']);

$where[0]['column_name'] = 'g.tb_gasto_cretip';
$where[0]['param0'] = 3; //solo creditos garveh
$where[0]['datatype'] = 'INT';

$where[1]['conector'] = 'AND';
$where[1]['column_name'] = 'g.tb_credito_id';
$where[1]['param1'] = $credito_id;
$where[1]['datatype'] = 'INT';

$where[2]['conector'] = 'AND';
$where[2]['column_name'] = 'g.tb_moneda_id';
$where[2]['param2'] = $moneda_id;
$where[2]['datatype'] = 'INT';

$inner[0]['alias_columnasparaver'] = 'p.tb_proveedor_nom';
$inner[0]['tipo_union'] = 'LEFT';
$inner[0]['tabla_alias'] = 'tb_proveedor p';
$inner[0]['columna_enlace'] = 'g.tb_proveedor_id';
$inner[0]['alias_columnaPK'] = 'p.tb_proveedor_id';

$inner[1]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
$inner[1]['tipo_union'] = 'INNER';
$inner[1]['tabla_alias'] = 'tb_moneda mon';
$inner[1]['columna_enlace'] = 'g.tb_moneda_id';
$inner[1]['alias_columnaPK'] = 'mon.tb_moneda_id';

$otros['orden']['column_name'] = 'g.tb_gasto_id';
$otros['orden']['value'] = 'ASC';

$lista = '';
$data['total_gastos'] = 0;
$data['total_devuelto'] = 0;
$global_devolver = 0;

$res = $oGasto->listar_todos($where, $inner, $otros);
if ($res['row_count'] > 0) {
  $data['table_foot'] = 'TOTAL DE INGRESOS';
  $tip_gas = array(null, 'PRODUCTO', 'SERVICIO');
  $est = array(null, 'Pendiente Pago', 'Pagado', 'Pago parcial');

  foreach ($res['data'] as $key => $gasto) {
    $data['total_gastos'] += floatval($gasto['tb_gasto_ptl']);
    $data['total_devuelto'] += floatval($gasto['tb_gasto_devo']);

    if ($gasto['tb_gasto_est'] == 3) {
      $color = 'black';
    } elseif ($gasto['tb_gasto_est'] == 2) {
      $color = 'green';
    } else {
      $color = 'red';
    }
    $saldo_devolver = $gasto['tb_gasto_ptl'] - $gasto['tb_gasto_devo'];
    $lista .=
      '<tr id="tabla_cabecera_fila">
        <td id="tabla_fila">' . $gasto['tb_gasto_id'] . '</td>
        <td id="tabla_fila">' . mostrar_fecha($gasto['tb_gasto_fecreg']) . '</td>
        <td id="tabla_fila">' . $tip_gas[$gasto['tb_gasto_tipgas']] . '</td>
        <td id="tabla_fila" align="left">' . strtoupper($gasto['tb_gasto_des']) . '</td>
        <td id="tabla_fila" align="left">' . $gasto['tb_proveedor_nom'] . '</td>
        <td id="tabla_fila">' . $gasto['tb_gasto_can'] . '</td>
        <td id="tabla_fila" align="left">' . $gasto['tb_moneda_nom'] . $gasto['tb_gasto_pvt'] . '</td>
        <td id="tabla_fila" align="left">' . $gasto['tb_moneda_nom'] . $gasto['tb_gasto_ptl'] . '</td>
        <td id="tabla_fila" align="left" class="success"><b>' . $gasto['tb_moneda_nom'] . $gasto['tb_gasto_devo'] . '</b></td>
        <td id="tabla_fila" align="left" class="warning"><b>'. $gasto['tb_moneda_nom'] .mostrar_moneda($saldo_devolver).'</b></td>
        <td id="tabla_fila" align="left">' .
          "<a href='javascript:void(0)' onclick='gasto_historial_form({$gasto['tb_gasto_id']})' style='color: $color; font-weight: bold;'>{$est[$gasto['tb_gasto_est']]}</a>" . '
        </td>
        <td id="tabla_fila">'; //se abre la casilla -> condicional
          if (in_array($gasto['tb_gasto_est'], array(1, 3))) { //pendiente de pago entonces colocar 3 botones
            if ($gasto['tb_gasto_est'] == 1) {
              //<button class="btn btn-info btn-xs" title="Ver" onclick="form_gasto(\'L\', '.$gasto['tb_gasto_id'].')"><i class="fa fa-eye"></i></button>
              $lista .= '<button class="btn btn-warning btn-xs" title="Editar" onclick="form_gasto(\'M\', ' . $gasto['tb_gasto_id'] . ')"><i class="fa fa-edit"></i></button>';
              //if (in_array(intval($_SESSION['usuariogrupo_id']), array(2, 6))) {//grupo de gerencia 2, y contabilidad 6
              if (in_array(intval($_SESSION['usuariogrupo_id']), array(2))) {//solo grupo gerencia 2
                $lista .= ' <button class="btn btn-danger btn-xs" title="Eliminar" onclick="form_gasto(\'E\', ' . $gasto['tb_gasto_id'] . ')"><i class="fa fa-trash"></i></button>';
              }
            }
            $lista .= ' <button class="btn btn-primary btn-xs" title="Pagar" onclick="pagar(\'I\', ' . $gasto['tb_gasto_id'] . ', 0)"><i class="fa fa-plus"></i> Pagar</button>';
          }
          if (in_array($gasto['tb_gasto_est'], array(2, 3))) {
            $lista .= '<button class="btn btn-info btn-xs" title="Ver Pagos" onclick="listar_pagos(' . $gasto['tb_gasto_id'] . ')"><i class="fa fa-eye"></i></button>';
          }

          $lista .= '
        </td>
        <td id="tabla_fila">';
          if($saldo_devolver > 0){
            $lista .= '
              <input type="checkbox" name="che_gasto_id" id="che_gasto_id" value="'.$gasto['tb_gasto_id'].'" class="flat-green" />';
          }
          $lista .= '
        </td>
      </tr>
    ';
  }
} else {
  $data['table_foot'] = '0 registros';
}
$res = NULL;

$global_devolver = mostrar_moneda($data['total_gastos'] - $data['total_devuelto']);

$data['total_gastos'] = mostrar_moneda($data['total_gastos']);
$data['total_devuelto'] = mostrar_moneda($data['total_devuelto']);
?>
<table id="tbl_gastos" class="table table-hover" style="word-wrap:break-word;">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila" style="width: 2%;">Id</th>
      <th id="tabla_cabecera_fila" style="width: 6%;">Fecha Reg.</th>
      <th id="tabla_cabecera_fila" style="width: 6%;">Tipo Gasto</th>
      <th id="tabla_cabecera_fila" style="width: 27.5%;">Descripcion</th>
      <th id="tabla_cabecera_fila" style="width: 27%;">Proveedor</th>
      <th id="tabla_cabecera_fila" style="width: 2%;">Cantidad</th>
      <th id="tabla_cabecera_fila" style="width: 6.5%;">P. Unidad</th>
      <th id="tabla_cabecera_fila" style="width: 6%;">P. Total</th>
      <th id="tabla_cabecera_fila" style="width: 6%;">Devuelto</th>
      <th id="tabla_cabecera_fila" style="width: 6%;">Falta Devolver</th>
      <th id="tabla_cabecera_fila" style="width: 5%;">Estado</th>
      <th id="tabla_cabecera_fila" style="width: 12%;">Opciones</th>
      <th id="tabla_cabecera_fila" style="width: 12%;">Devolver</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $lista; ?>
  </tbody>
  <tfoot>
    <tr style="font-family: cambria;font-size: 12px;background-color:#135896;color:#FFFFFF;">
      <td colspan="7" id="tabla_cabecera_fila" class="tfoot"><?php echo $data['table_foot']; ?></td>
      <td id="tabla_cabecera_fila" class="total_gasto"><?php echo $moneda_nom . $data['total_gastos']; ?></td>
      <td id="tabla_cabecera_fila" class="total_gasto"><?php echo $moneda_nom . $data['total_devuelto']; ?></td>
      <td id="tabla_cabecera_fila" class="total_gasto"><?php echo $moneda_nom . $global_devolver; ?></td>
      <td colspan="3" id="tabla_cabecera_fila"></td>
    </tr>
  </tfoot>
</table>