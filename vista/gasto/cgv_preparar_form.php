<?php
require_once('../funciones/funciones.php');
$credito_id = intval($_POST['credito_id']);
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_preparar" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo "Venta de CGV-$credito_id" ?></h4>
                <input type="hidden" name="hdd_credito_id" id="hdd_credito_id" value="<?php echo $credito_id; ?>">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info shadow" style="padding: 0px;">
                            <div class="panel-heading" style="text-align: center; font-size:15px;">Preparar precios de Unidad</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="txtmontoresolucion" class="control-label">Monto de Resolución US$ :</label>
                                            <input type="text" name="txtmontoresolucion" id="txtmontoresolucion" class="input-sm form-control mayus">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="txtgastosreparacion" class="control-label">Gastos Reparación US$ :</label>
                                            <input type="text" name="txtgastosreparacion" id="txtgastosreparacion" class="input-sm form-control mayus">
                                        </div>
                                    </div>
                                </div>

                                <!---------------------------------------MODALES UNIDOS------------------------------------------------------->

                                <!-- Modales juntos -->

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="txtvalorsoat" class="control-label">Valor SOAT US$ :</label>
                                            <input type="text" name="txtvalorsoat" id="txtvalorsoat" class="input-sm form-control mayus">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="txtfechasoat" class="control-label">Fecha SOAT : </label>
                                            <input type="text" name="txtfechasoat" id="txtfechasoat" class="input-sm form-control mayus">
                                        </div>

                                    </div>
                                </div>

                                <!-- Modales juntos -->

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="txtvalorstr" class="control-label">Valor STR US$ :</label>
                                            <input type="text" name="txtvalorstr" id="txtvalorstr" class="input-sm form-control mayus">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="txtfechastr" class="control-label">Fecha STR : </label>
                                            <input type="text" name="txtfechastr" id="txtfechastr" class="input-sm form-control mayus">
                                        </div>

                                    </div>
                                </div>

                                <!-- Modales juntos -->

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="txtvalorgps" class="control-label">Valor GPS US$ :</label>
                                            <input type="text" name="txtvalorgps" id="txtvalorgps" class="input-sm form-control mayus">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="txtfechagps" class="control-label">Fecha GPS : </label>
                                            <input type="text" name="txtfechagps" id="txtfechagps" class="input-sm form-control mayus">
                                        </div>

                                    </div>
                                </div>

                                <!-- Modales juntos -->

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="txtvalorgas" class="control-label">Valor GAS US$ :</label>
                                            <input type="text" name="txtvalorgas" id="txtvalorgas" class="input-sm form-control mayus">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="txtfechaimpuesto" class="control-label">Fecha Impuesto : </label>
                                            <input type="text" name="txtfechaimpuesto" id="txtfechaimpuesto" class="input-sm form-control mayus">
                                        </div>

                                    </div>
                                </div>

                                <!-----------------------------------------FIN MODALES UNIDOS-------------------------------------------->
                                <hr>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="txtconsolidado" class="control-label">Consolidado US$ :</label>
                                            <input type="text" name="txtconsolidado" id="txtconsolidado" class="input-sm form-control mayus">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="f1-buttons">
                    <button type="submit" class="btn btn-info" id="btn_guardar">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo "vista/gasto/cgv_venta_form.js?ver=087"; ?>"></script>