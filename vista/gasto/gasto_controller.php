<?php
if (defined('VISTA_URL')) {
	require_once(APP_URL . 'core/usuario_sesion.php');
} else {
	require_once('../../core/usuario_sesion.php');
}
require_once('Gasto.class.php');
$oGasto = new Gasto();
require_once('../egreso/Egreso.class.php');
$oEgreso = new Egreso();
require_once ("../ingreso/Ingreso.class.php");
$oIngreso = new Ingreso();
require_once("../deposito/Deposito.class.php");
$oDeposito = new Deposito();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once ("../rendicioncuentas/RendicionCuentas.class.php");
$oRendicionCuentas = new RendicionCuentas();
require_once ("../gastopago/Gastopago.class.php");
$oGastopago = new Gastopago();

require_once('../funciones/fechas.php');
require_once('../funciones/funciones.php');

$fecha_hora = date("d-m-Y h:i a");
$fecha_hoy = date("d-m-Y");
$action = $_POST['action'];
$gasto_id = intval($_POST['hdd_gasto_id']);
$data['estado'] = 0;
$data['mensaje'] = "Hubo un error al $action el gasto";
$reg_origin = (array) json_decode($_POST['hdd_gasto_reg']);

if($action == 'eliminar'){
	$oGasto->tb_gasto_id = $gasto_id;
	
	$res = $oGasto->eliminar_delete();

	if($res == 1){
		$data['estado'] = 1;
		$data['mensaje'] = 'Gasto eliminado correctamente';
	}
}
elseif($action == 'insertar' || $action == 'modificar'){
	$oGasto->tb_gasto_cretip	= 3; //garveh
	$oGasto->tb_credito_id		= intval($_POST['hdd_credito_id']);
	$oGasto->tb_gasto_tipgas	= intval($_POST['cbo_gasto_tipo']);
	$oGasto->tb_proveedor_id	= intval($_POST['hdd_proveedor_id']);
	$oGasto->tb_gasto_can		= intval($_POST['txt_gasto_cantidad']);
	$oGasto->tb_moneda_id		= intval($_POST['cmb_moneda_id']);
	$oGasto->tb_gasto_pvt		= moneda_mysql($_POST['txt_gasto_preunit']);
	$oGasto->tb_gasto_ptl		= moneda_mysql($_POST['txt_gasto_pretotal']);
	$oGasto->tb_gasto_des		= strtoupper(trim($_POST['txt_descripcion']));
	$oGasto->tb_gasto_his		= "Datos de gasto ingresados por: <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b> | <b>$fecha_hora</b><br>";

	if ($action == 'insertar') {
		$res = $oGasto->insertar();
		if ($res['estado'] == 1) {
			$data['estado'] = 1;
			$data['mensaje'] = $res['mensaje'] . ". ID del gasto: " . $res['nuevo'];
		}
	} else { //action = modificar
		$igual[0] = boolval($oGasto->tb_gasto_tipgas == $reg_origin['tb_gasto_tipgas']);
		$igual[1] = boolval($oGasto->tb_proveedor_id == $reg_origin['tb_proveedor_id']);
		$igual[2] = boolval($oGasto->tb_gasto_can == intval($reg_origin['tb_gasto_can']));
		$igual[3] = boolval($oGasto->tb_moneda_id == $reg_origin['tb_moneda_id']);
		$igual[4] = boolval($oGasto->tb_gasto_pvt == $reg_origin['tb_gasto_pvt']);
		$igual[5] = boolval($oGasto->tb_gasto_ptl == $reg_origin['tb_gasto_ptl']);
		$igual[6] = boolval($oGasto->tb_gasto_des == $reg_origin['tb_gasto_des']);

		$son_iguales = true;
		$i = 0;
		while ($i < count($igual)) {
			if (!$igual[$i]) {
				$son_iguales = false;
			}
			$i++;
		}
		$i = 0;

		if ($son_iguales) {
			$data['estado'] = 2;
			$data['mensaje'] = "No se realizó ninguna modificacion";
		} else {
			$oGasto->tb_gasto_id = $gasto_id;

			$res = $oGasto->modificar();

			if ($res == 1) {
				$data['estado'] = 1;
				$data['mensaje'] = 'Gasto editado correctamente';

				$tipo_gasto = array(null, 'Producto', 'Servicio');
				$mensaje = "Datos de gasto editados por <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b>";
				if(!$igual[0])
					$mensaje .= ". Editó tipo gasto({$tipo_gasto[$reg_origin['tb_gasto_tipgas']]} => <b>{$tipo_gasto[$oGasto->tb_gasto_tipgas]}</b>)";
				if(!$igual[1])
					$mensaje .= ". Editó proveedor(\"{$reg_origin['tb_proveedor_doc']} - {$reg_origin['tb_proveedor_nom']}\" => \"<b>{$_POST['txt_proveedor']}</b>\")";
				if(!$igual[2])
					$mensaje .= ". Editó cantidad(".intval($reg_origin['tb_gasto_can'])." => <b>{$oGasto->tb_gasto_can}</b>)";
				if(!$igual[3])
					$mensaje .= ". Editó moneda(\"{$reg_origin['tb_moneda_nom']}\" => \"<b>{$_POST['moneda_nom']}</b>\")";
				if(!$igual[4])
					$mensaje .= ". Editó precio unitario({$reg_origin['tb_gasto_pvt']} => <b>{$oGasto->tb_gasto_pvt}</b>)";
				if(!$igual[5])
					$mensaje .= ". Editó precio total({$reg_origin['tb_gasto_ptl']} => <b>{$oGasto->tb_gasto_ptl}</b>)";
				if(!$igual[6])
					$mensaje .= ". Editó descripción(\"{$reg_origin['tb_gasto_des']}\" => \"<b>{$oGasto->tb_gasto_des}</b>\")";

				$mensaje.= " | <b>$fecha_hora</b><br>";

				$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $reg_origin['tb_gasto_his'].$mensaje, 'STR');
			}
		}
	}
}
elseif($action == 'devolucion'){
	$tipocambio_dia = $_POST['hdd_devolucion_tipocambio'];
	$ingreso_mon = moneda_mysql($_POST['txt_devolucion_imp']);
	$credito_id = intval($_POST['hdd_devolucion_credito_id']);
	$vehiculo_pla = $_POST['hdd_devolucion_credito_pla'];
	$devolucion_falta = moneda_mysql($_POST['txt_devolucion_falta']);

	$cadena_array = $_POST['hdd_devolucion_array'];
	$array_gasto_id = explode(',', $cadena_array);

	//? 1. VALIDAMOS QUE EL NÚMERO DE OPERACIÓN INGRESADO SEA VÁLIDO
	if ($_POST['cmb_gasto_lugpag'] == 2) {
		
		$deposito_mon = 0;
		$ingreso_mon = moneda_mysql($_POST['txt_devolucion_montot']);

		$dts = $oDeposito->validar_num_operacion($_POST['txt_devolucion_numope']);
		if ($dts['estado'] == 1) {
			foreach ($dts['data'] as $key => $dt) {
				$deposito_mon += floatval($dt['tb_deposito_mon']);
			}
		}
		$dts = NULL;

		$ingreso_importe = 0;
		$dts = $oIngreso->lista_ingresos_num_operacion($_POST['txt_devolucion_numope']);
		if ($dts['estado'] == 1) {
			foreach ($dts['data'] as $key => $dt) {
				$tb_ingreso_imp = floatval($dt['tb_ingreso_imp']);
				$ingreso_mon_id = intval($dt['tb_moneda_id']);
				if ($ingreso_mon_id == 2) //si el ingreso se hizo en dólares
					$tb_ingreso_imp = moneda_mysql($tb_ingreso_imp * $tipocambio_dia);
				$ingreso_importe += $tb_ingreso_imp;
			}
		}

		$monto_usar = abs($deposito_mon) - $ingreso_importe;
		$monto_usar = moneda_mysql($monto_usar);

		if ($ingreso_mon > $monto_usar) {
			$data['estado'] = 0;
			$data['mensaje'] = 'El MONTO ingresado es mayor que el MONTO disponible, no puede pagar mas de lo depositado. Monto ingresado: ' . mostrar_moneda($ingreso_mon) . ', monto disponible: ' . mostrar_moneda($monto_usar) . ', TOTAL DEL DEPOSITO: ' . mostrar_moneda($deposito_mon) . ' | validar: ' . $ingreso_mon . ' | usar: ' . $monto_usar . ' | ingre: ' . $ingreso_importe . ' | cambio: ' . $tipocambio_dia . ' / mond ing: ' . $ingreso_mon_id;
			echo json_encode($data);
			exit();
		}
	}

	if($ingreso_mon < $devolucion_falta && count($array_gasto_id) > 1){
		$data['estado'] = 0;
		$data['mensaje'] = 'Solo puedes ingresar MONTOS PARCIALES a un solo gasto, selecciona uno y vuelve a ingresar el gasto';
		echo json_encode($data);
		exit();
	}

	//? 2. REALIZAMOS EL INGRESO PARA CAJA
	$numdoc = '71-3-'.str_pad($credito_id, 3, "0", STR_PAD_LEFT);
	$ingreso_det = 'Ingreso por devolucón de Gastos Vehiculares, del Cliente: '.$_POST['hdd_devolucion_cliente_nom'].', del Crédito ID: CGV-'.$credito_id.' y de Placa de Rodaje: '.$vehiculo_pla;

	//generamos el ingreso de la venta siempre y cuando no sea colaborador
	$oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
	$oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
	$oIngreso->ingreso_fec = date('Y-m-d');
	$oIngreso->documento_id = 8; // otros ingresos
	$oIngreso->ingreso_numdoc = $numdoc;
	$oIngreso->ingreso_det = $ingreso_det;
	$oIngreso->ingreso_imp = $ingreso_mon;
	$oIngreso->cuenta_id = 1; // LAS DEVOLUCIONES SIEMPRE SERÁN EN CUENTA: INGRESO CLIENTE
	$oIngreso->subcuenta_id = 93; // Y EN LA SUBCUENTA SIEEMPRE SERÁ 93: GASTOS OPERATIVOS RETENCIONES;
	$oIngreso->cliente_id = intval($_POST['hdd_devolucion_cliente_id']);
	$oIngreso->caja_id = 1;
	$oIngreso->moneda_id = 1;
	//valores que pueden ser cambiantes según requerimiento de ingreso
	$oIngreso->modulo_id = 71; //modulo descrito en DETALLE de la tabla del TB_INGRESO, 71 será para gastos de GARVEH
	$oIngreso->ingreso_modide = $credito_id; // 71 y el ID del CREDITO GARVEH permitirá sumar los ingresos por devolución
	$oIngreso->empresa_id = $_SESSION['empresa_id'];
	$oIngreso->ingreso_fecdep = '';
	$oIngreso->ingreso_numope = '';
	$oIngreso->ingreso_mondep = 0;
	$oIngreso->ingreso_comi = 0;
	$oIngreso->cuentadeposito_id = 0;
	$oIngreso->banco_id = 0;
	$oIngreso->ingreso_ap = 0;
	$oIngreso->ingreso_detex = ''; //guardo el detalle extra aquí que es el array de los GASTOS ID ayudará para cuando se elimine el INGRESO;
	$result = $oIngreso->insertar();

	if (intval($result['estado']) == 1) {
		$ing_id = $result['ingreso_id'];

		//? agregamos el array de gastos a la tabla INGRESO para poder tener control de qué se ha pagado
		$oIngreso->modificar_campo($ing_id, 'tb_ingreso_array', $cadena_array, 'STR');
	}

	//? 3. GUARDAMOS EL HISTORIAL DEL INGRESO PARA MOSTRARLO
	$oCreditolinea->insertar(3, $credito_id, $_SESSION['usuario_id'], $ingreso_det);

	//? 4. VERIFICAMOS QUE EL LUGAR DE PAGO SEA BANCO PARA PODER REALIZAR EL EGRESO RESPECTIVO
	if ($_POST['cmb_gasto_lugpag'] == 2) {
		//generamos un egreso con el monto pagado para no descuadrar la caja, ya que el dinero está en el banco
		$oIngreso->ingreso_fecdep = fecha_mysql($_POST['txt_devolucion_fec']);
		$oIngreso->ingreso_numope = $_POST['txt_devolucion_numope'];
		$oIngreso->ingreso_mondep = moneda_mysql($_POST['txt_devolucion_mon']);
		$oIngreso->ingreso_comi = moneda_mysql($_POST['txt_devolucion_comi']);
		$oIngreso->cuentadeposito_id = $_POST['cmb_cuedep_id'];
		$oIngreso->banco_id = 1;
		$oIngreso->ingreso_id = $ing_id;

		$oIngreso->registrar_datos_deposito_banco(); // parametro 1 de BANCO BCP

		$subcue_id = 71; // 71 es cuenta en soles

		$xac = 1;
		$cue_id = 28; // cuenta 28 PAGO EN CUENTA DE BANCO	
		$pro_id = 1; //sistema
		$mod_id = 71; //71 es el modulo_id en ingreso, representa LA DEVOLUCION QUE HACE EL CLIENTE, será igual para EGRESO servira para cuando se anule el pago
		$modide = $credito_id; //el modide de egreso guardará el id del CREDITO VEHICULAR
		$egr_det = 'EGRESO POR: devolucón de GASTOS VEHICULARES, del Cliente: '.$_POST['hdd_devolucion_cliente_nom'].', del Crédito ID: CGV-'.$credito_id.' y de Placa de Rodaje: '.$vehiculo_pla.'. Pago abonado en el banco, la fecha de depósito fue: ' . $_POST['txt_devolucion_fec'] . ', el N° de operación fue: ' . $_POST['txt_devolucion_numope'] . ', el monto depositado fue: S/. ' . $_POST['txt_devolucion_mon'] . ', la comisión del banco fue: S/. ' . $_POST['txt_devolucion_comi'] . ', el monto validado fue: S/. ' . $_POST['txt_devolucion_montot'];
		$caj_id = 1; //caja id 14 ya que es CAJA AP
		$numdoc = '71-' . str_pad($mod_id, 3, "0", STR_PAD_LEFT) . '-' . str_pad($gasto_id, 3, "0", STR_PAD_LEFT);

		$oEgreso->egreso_usureg = $_SESSION['usuario_id'];
		$oEgreso->egreso_usumod = $_SESSION['usuario_id'];
		$oEgreso->egreso_fec = date('Y-m-d');
		$oEgreso->documento_id = 9; //otros egresos
		$oEgreso->egreso_numdoc = $numdoc;
		$oEgreso->egreso_det = $egr_det;
		$oEgreso->egreso_imp = $ingreso_mon;
		$oEgreso->egreso_tipcam = 0;
		$oEgreso->egreso_est = 1;
		$oEgreso->cuenta_id = $cue_id;
		$oEgreso->subcuenta_id = $subcue_id;
		$oEgreso->proveedor_id = $pro_id;
		$oEgreso->cliente_id = 0;
		$oEgreso->usuario_id = 0;
		$oEgreso->caja_id = $caj_id;
		$oEgreso->moneda_id = 1;
		$oEgreso->modulo_id = $mod_id;
		$oEgreso->egreso_modide = $modide;
		$oEgreso->empresa_id = $_SESSION['empresa_id'];
		$oEgreso->egreso_ap = 0; //si
		$oEgreso->insertar();

		$historial = 'La devolición se hizo mediante depósito bancario los datos son: Pago abonado en el banco, la fecha de depósito fue: ' . $_POST['txt_devolucion_fec'] . ', el N° de operación fue: ' . $_POST['txt_devolucion_numope'] . ', el monto depositado fue: S/. ' . $_POST['txt_devolucion_mon'] . ', la comisión del banco fue: S/. ' . $_POST['txt_devolucion_comi'] . ', el monto validado fue: S/. ' . $_POST['txt_devolucion_montot'].' | <b>'.$fecha_hora.'</b><br>';

		$oCreditolinea->insertar(3, $credito_id, $_SESSION['usuario_id'], $historial);
	}

	//? 5. MEDIANTE UN FOREACH VAMOS A RECORRER CADA GASTO ID PARA REGISTRAR EL HISTORIAL A CADA GASTO Y AGREGAR SU VALOR DEVUELTO A CADA GASTO

	foreach ($array_gasto_id as $gasto_id){
		$result = $oGasto->mostrarUno($gasto_id);
			$gasto_des = $result['data']['tb_gasto_des'];
			$gasto_his = $result['data']['tb_gasto_his'];
			$gasto_ptl = formato_numero($result['data']['tb_gasto_ptl']);
			$gasto_devo = formato_numero($result['data']['tb_gasto_devo']);
		$result = NULL;

		$devolucion_real = $gasto_ptl;
		$devolucion_historial = $gasto_ptl - $gasto_devo;

		if(count($array_gasto_id) == 1)
			$devolucion_historial = $ingreso_mon;

		$historial = $gasto_his.'SE REALIZÓ UNA DEVOLUCIÓN DE ESTE GASTO: <b>'.$gasto_des.'</b> MONTO INGRESADO: '.mostrar_moneda($devolucion_historial).', REALIZADO POR EL USUARIO: <b>'.$_SESSION['usuario_nom'].'</b> | <b>'.$fecha_hora.'</b><br>';

		$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $historial, 'STR');

		if(count($array_gasto_id) == 1){
			$devolucion_real = $gasto_devo + $ingreso_mon;
		}

		$oGasto->modificar_campo($gasto_id, 'tb_gasto_devo', $devolucion_real, 'STR');

		if ($_POST['cmb_gasto_lugpag'] == 2) {
			$historial .= 'La devolición se hizo mediante depósito bancario los datos son: Pago abonado en el banco, la fecha de depósito fue: ' . $_POST['txt_devolucion_fec'] . ', el N° de operación fue: ' . $_POST['txt_devolucion_numope'] . ', el monto depositado fue: S/. ' . $_POST['txt_devolucion_mon'] . ', la comisión del banco fue: S/. ' . $_POST['txt_devolucion_comi'] . ', el monto validado fue: S/. ' . $_POST['txt_devolucion_montot'].' | <b>'.$fecha_hora.'</b><br>';

			$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $historial , 'STR');
		}
	}

	$data['estado'] = 1;
	$data['mensaje'] = 'INGRESO POR DEVOLUCION INGRESADO CORRECTAMENTE';
}
elseif($action == 'anular_devolucion'){
	$oIngreso->anular_ingresos_por_modulo(0, 71, $gasto_id);
	$oEgreso->anular_egresos_por_modulo(0, 71, $gasto_id);

	$modulo_id = 71; // el modulo para egreso e ingreso es el 71
	$ingreso_modide = $gasto_id;
	$monto_valido = 0;

	$result = $oIngreso->mostrar_ingresos_modulo($modulo_id, $ingreso_modide);
		if($result['estado'] == 1){
			foreach ($result['data'] as $key => $value) {
				$monto_valido = $value['tb_ingreso_imp'];
			}
		}
	$result = NULL;

	$oGasto->modificar_campo($gasto_id, 'tb_gasto_devo', $monto_valido, 'STR');

	$data['estado'] = 1;
	$data['mensaje'] = 'INGRESO POR DEVOLUCION ANULADO CORRECTAMENTE';
}
elseif($action == 'verificar_deuda'){
	$credito_id = intval($_POST['credito_id']);
	$creditotipo = intval($_POST['creditotipo']);
	$result = $oGasto->listar_gastos_credito_tipo($credito_id, $creditotipo);
	$gasto_total = 0;
	$devolucion_total = 0;
	$saldo_devolver = 0;
	
	if($result['estado']==1){
		foreach ($result['data']as $key=>$value){
			//$gasto_total += formato_moneda($value['tb_gasto_ptl']);
			//$devolucion_total += formato_moneda($value['tb_gasto_devo']);
					$gasto_des = $value['tb_gasto_des'];
					$credito_id = $value['tb_credito_id'];
					$gasto_his = $value['tb_gasto_his'];
					/*$gasto_ptl = formato_numero($value['tb_gasto_ptl']);
					$gasto_devo = formato_numero($value['tb_gasto_devo']);
			
					$gastos_totales += $gasto_ptl;
					$devolucion_previo += $gasto_devo;*/
					$saldo_devolver += ($value['tb_gasto_ptl'] - $value['tb_gasto_devo']);
		}
		//$falta = formato_numero($gastos_totales - $devolucion_previo);
	}
	$result = NULL;

	if($saldo_devolver > 0){
		$data['estado'] = 1;
		$data['saldo_total'] = $saldo_devolver;
		$data['mensaje'] = 'TIENE DEVOLUCION DE GASTOS PENDIENTES';
	}else{
		$data['estado'] = 0;
		$data['saldo_total'] = $saldo_devolver;
		$data['mensaje'] = 'NO TIENE DEVOLUCION DE GASTOS PENDIENTES';
	}
}
elseif($action == 'insertar_gasto_rendicion'){

	$moneda_nom	= array(1 => 'S/.', 2 => 'US$');
	$usuario_id = intval($_SESSION['usuario_id']);
	$monedapago_id = intval($_POST['cmb_moneda_id']);
	$rendicioncuenta_id = intval($_POST['hdd_rendicioncuenta_id']);
	$monto_pagar = moneda_mysql($_POST['txt_gasto_pretotal']);
	$cuenta_id = intval($_POST['cbo_cuenta_id']);
	$subcuenta_id = intval($_POST['cbo_subcuenta_id']);
	$forma_pago	= intval($_POST['cbo_forma_egreso']);
	$emite_doc_sunat = intval($_POST['che_emite_docsunat']);
	
	$oGasto->tb_gasto_cretip	= 3; //garveh
	$oGasto->tb_credito_id		= intval($_POST['cbo_credito_id']);
	$oGasto->tb_gasto_tipgas	= intval($_POST['cbo_gasto_tipo']);
	$oGasto->tb_proveedor_id	= intval($_POST['hdd_proveedor_id']);
	$oGasto->tb_gasto_can		= intval($_POST['txt_gasto_cantidad']);
	$oGasto->tb_moneda_id		= intval($_POST['cmb_moneda_id']);
	$oGasto->tb_gasto_pvt		= moneda_mysql($_POST['txt_gasto_preunit']);
	$oGasto->tb_gasto_ptl		= moneda_mysql($_POST['txt_gasto_pretotal']);
	$oGasto->tb_gasto_des		= strtoupper(trim($_POST['txt_descripcion']));
	$oGasto->tb_gasto_his		= "Datos de gasto ingresados por: <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b> | <b>$fecha_hora</b><br>";

	$res = $oGasto->insertar();
	if ($res['estado'] == 1) {
		$gasto_id = $res['nuevo'];
		$data['estado'] = 1;
		$data['estado_new'] = 0;
		$data['mensaje'] = $res['mensaje'] . ". ID del gasto: " . $res['nuevo'];

		/** PAGO DE GASTO **/
			$where[0]['column_name'] = 'g.tb_gasto_id';
            $where[0]['param0'] = $gasto_id; //solo creditos garveh
            $where[0]['datatype'] = 'INT';

            $inner[0]['alias_columnasparaver'] = 'p.tb_proveedor_nom, p.tb_proveedor_doc';
            $inner[0]['tipo_union'] = 'LEFT';
            $inner[0]['tabla_alias'] = 'tb_proveedor p';
            $inner[0]['columna_enlace'] = 'g.tb_proveedor_id';
            $inner[0]['alias_columnaPK'] = 'p.tb_proveedor_id';

            $inner[1]['alias_columnasparaver'] = 'mon.tb_moneda_nom';
            $inner[1]['tipo_union'] = 'LEFT';
            $inner[1]['tabla_alias'] = 'tb_moneda mon';
            $inner[1]['columna_enlace'] = 'g.tb_moneda_id';
            $inner[1]['alias_columnaPK'] = 'mon.tb_moneda_id';

            $inner[2]['alias_columnasparaver'] = 'cgv.tb_cliente_id, cgv.tb_credito_vehpla';
            $inner[2]['tipo_union'] = 'LEFT';
            $inner[2]['tabla_alias'] = 'tb_creditogarveh cgv';
            $inner[2]['columna_enlace'] = 'g.tb_credito_id';
            $inner[2]['alias_columnaPK'] = 'cgv.tb_credito_id';

            $inner[3]['alias_columnasparaver'] = 'cli.tb_cliente_nom';
            $inner[3]['tipo_union'] = 'LEFT';
            $inner[3]['tabla_alias'] = 'tb_cliente cli';
            $inner[3]['columna_enlace'] = 'cgv.tb_cliente_id';
            $inner[3]['alias_columnaPK'] = 'cli.tb_cliente_id';

            $res = $oGasto->listar_todos($where, $inner, array());
			unset($where, $inner);
            if ($res['row_count'] > 0) {
                $gasto_reg = $res['data'][0];
                $fecha_gasto = fecha_mysql($gasto_reg['tb_gasto_fecreg']);
			}

			// DATOS PARA EL EGRESO
			$gasto_id = intval($gasto_reg['tb_gasto_id']);
			$prod_serv = intval($gasto_reg['tb_gasto_tipgas']) == 2 ? 'SERVICIO' : 'PRODUCTO';
			$proveedor_id = intval($gasto_reg['tb_proveedor_id']);
			$cliente = strtoupper(trim($gasto_reg['tb_cliente_nom']));
			$vehiculo = strtoupper(trim($gasto_reg['tb_credito_vehpla']));
			$gasto_desc = $gasto_reg['tb_gasto_des'] . ". Del Cliente: $cliente y vehiculo placa: $vehiculo. Comentario: [$obs]";

			$egr_det = "EGRESO CRÉDITO GARVEH para pagar el $prod_serv de $gasto_desc. $documento";
			//generar egreso
			$oEgreso->egreso_usureg	= $usuario_id;
			$oEgreso->egreso_fec	= fecha_mysql($fecha_hoy);
			$oEgreso->documento_id	= 9; //9: OE - OTROS EGRESOS, unica opcion documento
			$oEgreso->egreso_numdoc = 0; //al insertar se actualiza en automatico este numdoc
			$oEgreso->egreso_det	= $egr_det;
			$oEgreso->moneda_id		= $monedapago_id;
			$oEgreso->egreso_tipcam = $monedapago_id == 2 ? $valor_tc_compra_hoy : 1;
			$oEgreso->egreso_imp	= $monto_pagar;
			$oEgreso->egreso_est	= 1; // TODOS LOS VALORES EN BD SON 1, NO DICE COMENTARIO
			$oEgreso->cuenta_id		= $cuenta_id;
			$oEgreso->subcuenta_id	= $subcuenta_id; //subcuenta de OTROS
			$oEgreso->proveedor_id = $proveedor_id;
			$oEgreso->cliente_id = 0; //id de cliente solo cuando el egreso se entrega a un cliente.
			$oEgreso->usuario_id = 0; //id de colaborador solo cuando el egreso es para pago a un colaborador.
			$oEgreso->caja_id = 1; // 1: CAJA
			$oEgreso->modulo_id = 10; //10 egreso para pago de gastos vehiculares
			$oEgreso->egreso_modide = $gasto_id; //tb_egreso_modide el id de tb_gasto
			$oEgreso->empresa_id = $_SESSION['empresa_id'];
			$oEgreso->egreso_ap = 0; //guarda el id de cuotapago de un acuerdo de pago

			$result = $oEgreso->insertar();
			if ($result['estado'] > 0) {
				$egreso_id = $result['egreso_id'];
				unset($result);
				
				// en gasto actualizar historial
				$mensaje = " Se hizo un pago de {$moneda_nom[$monedapago_id]} ". mostrar_moneda($monto_pagar);
				$mensaje .= ", registrado por <b>{$_SESSION['usuario_nom']} {$_SESSION['usuario_ape']}</b>. ";
				$mensaje .= "Egreso id : $egreso_id | <b>$fecha_hora</b><br>";
				$mensaje .= "Se registró el egreso con el detalle: $egr_det | <b>$fecha_hora</b><br>";
				$oGasto->modificar_campo($gasto_id, 'tb_gasto_his', $gasto_reg['tb_gasto_his'].$mensaje, 'STR');
		
				//en gasto actualizar est PAGA COMPLETO
				$oGasto->modificar_campo($gasto_id, 'tb_gasto_est', 2, 'INT');
			}
			/**EL INGRESO QUE COMPENSE**/
			$oIngreso->ingreso_usureg = intval($_SESSION['usuario_id']);
			$oIngreso->ingreso_usumod = intval($_SESSION['usuario_id']);
			$oIngreso->ingreso_fec = fecha_mysql($fecha_hoy); 
			$oIngreso->documento_id = 8; 
			$oIngreso->ingreso_numdoc = 0;
			$oIngreso->ingreso_det = "INGRESO PARA COMPENSAR EL EGRESO: $egreso_id"; 
			$oIngreso->ingreso_imp = moneda_mysql($_POST['txt_gasto_pretotal']);  
			$oIngreso->cuenta_id = 2;
			$oIngreso->subcuenta_id = 9; 
			$oIngreso->cliente_id = intval($_POST['hdd_cliente_id']); 
			$oIngreso->caja_id = 1; 
			$oIngreso->moneda_id = intval($monedapago_id);
			//valores que pueden ser cambiantes según requerimiento de ingreso
			$oIngreso->modulo_id = 0; 
			$oIngreso->ingreso_modide = 0; 
			$oIngreso->empresa_id = $_SESSION['empresa_id'];
			$oIngreso->ingreso_fecdep = ''; 
			$oIngreso->ingreso_numope = ''; 
			$oIngreso->ingreso_mondep = 0; 
			$oIngreso->ingreso_comi = 0; 
			$oIngreso->cuentadeposito_id = 0; 
			$oIngreso->banco_id = 0; 
			$oIngreso->ingreso_ap = 0; 
			$oIngreso->ingreso_detex = '';

			$result=$oIngreso->insertar();
			/**EL INGRESO QUE COMPENSE**/
			
			//ahora registrar el gastopago
			$oGastopago->tb_gastopago_monto = $monto_pagar;
			$oGastopago->tb_empresa_idfk = $_SESSION['empresa_id'];
			$oGastopago->tb_gasto_idfk = $gasto_id;
			$oGastopago->tb_gastopago_formapago = $forma_pago;
			if($forma_pago == 2){
				$oGastopago->tb_gastopago_numope = $num_operacion;
			}
			$oGastopago->tb_gastopago_emitedocsunat = $emite_doc_sunat;
			if($emite_doc_sunat == 1){
				$oGastopago->tb_gastopago_documentossunat = strtoupper(trim($_POST['txt_nro_documento']));
			}
			$oGastopago->tb_gastopago_usureg = $usuario_id;
			$oGastopago->tb_gastopago_obs = $gasto_desc;
			$oGastopago->tb_moneda_idfk = $monedapago_id;
			$oGastopago->tb_egreso_idfk = $egreso_id;
			$oGastopago->tb_gastopago_fecha = date("Y-m-d");

			$res = $oGastopago->insertar();//ahora registrar el gastopago

		/** PAGO DE GASTO **/
		$result = $oRendicionCuentas->agregar_gasto($rendicioncuenta_id, $gasto_id);
		if($result){
			$data['estado_new'] = 1;
			$data['mensaje2'] = "GASTO AGREGADO A RENDICION";
		}
	}
}

echo json_encode($data);