<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
} else {
    require_once('../../core/usuario_sesion.php');
}
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('Gasto.class.php');
$oGasto = new Gasto();
require_once ("../rendicioncuentas/RendicionCuentas.class.php");
$oRendicionCuentas = new RendicionCuentas();
require_once ("../egreso/Egreso.class.php");
$oEgreso = new Egreso();
$listo = 1;

$usuario_action = $_POST['action'];
$gasto_id = $_POST['gasto_id'];
$rendicion_id = $_POST['rendicion_id'];
$titulo = '';
if ($usuario_action == 'I') {
    $titulo = 'Registrar gasto';
} elseif ($usuario_action == 'M') {
    $titulo = 'Editar gasto';
} elseif ($usuario_action == 'E') {
    $titulo = 'Eliminar gasto';
} elseif ($usuario_action == 'L') {
    $titulo = 'Gasto registrado';
} else {
    $titulo = 'Acción de Usuario Desconocido';
}
//$action = devuelve_nombre_usuario_action($usuario_action);
$action = "insertar_gasto_rendicion";

$fecha_gasto = date('d-m-Y');

$proveedor_egreso_id = 0;

$result = $oRendicionCuentas->mostrarUno($rendicion_id);
if($result['estado']==1){
    $egreso_id = $result['data']['tb_egreso_id'];
    $gasto_id = $result['data']['tb_gasto_id'];
    $resultEgreso = $oEgreso->mostrarUno($egreso_id);
    if($resultEgreso['estado']==1){
        $proveedor_egreso_id = $resultEgreso['data']['tb_proveedor_id'];
        $moneda_id = $resultEgreso['data']['tb_moneda_id'];
        $importe = $resultEgreso['data']['tb_egreso_imp'];
        $tipcam = $resultEgreso['data']['tb_egreso_tipcam'];
    }
}

?>
<div class="modal fade" tabindex="-1" role="dialog" id="modal_gasto_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><?php echo "$titulo para Rendicion"; ?></h4>
            </div>
            <form id="form_gasto" method="post">
                <div class="modal-body">
                    <input type="hidden" name="action" id="action" value="<?php echo $action;?>">
                    <input type="hidden" name="hdd_gasto_id" id="hdd_gasto_id" value="<?php echo $gasto_id;?>">
                    <input type="hidden" name="hdd_rendicioncuenta_id" id="hdd_rendicioncuenta_id" value="<?php echo $rendicion_id;?>">
                    
                    <input type="hidden" name="hdd_moneda_id" id="hdd_moneda_id" value="<?php echo $moneda_id;?>">
                    <input type="hidden" name="hdd_egreso_id" id="hdd_egreso_id" value="<?php echo $egreso_id;?>">
                    <input type="hidden" name="hdd_tipcam" id="hdd_tipcam" value="<?php echo $tipcam;?>">
                    <input type="hidden" name="hdd_importe" id="hdd_importe" value="<?php echo $importe;?>">
                    
                    <div class="box box-primary shadow">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                GASTO
                            </div>
                            <div class="panel-body">
                                <div class="shadow">
                                    <div class="row form-group">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                            <input type="hidden" name="hdd_cliente_id" id="hdd_cliente_id" value="<?php echo $cliente_id;?>">
                                                <label for="">CLIENTE </label>
                                                <input name="txt_cliente_doc" type="text" id="txt_cliente_doc" class="form-control input-sm mayus" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="cbo_credito_id">Creditos:</label>
                                            <select name="cbo_credito_id" id="cbo_credito_id" class="form-control input-sm">
                                                <option value="0">..SELECCIONE..</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <label for="cbo_gasto_tipo">Tipo de Gasto:</label>
                                            <select name="cbo_gasto_tipo" id="cbo_gasto_tipo" class="form-control input-sm">
                                                <option value="0">..SELECCIONE..</option>
                                                <option value="1" style="font-weight: bold;" <?php echo $tipo_gasto[1];?>>Producto</option>
                                                <option value="2" style="font-weight: bold;" <?php echo $tipo_gasto[2];?>>Servicio</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input type="hidden" id="hdd_proveedor_id" name="hdd_proveedor_id" value="<?php echo $proveedor_id;?>">
                                            <label style="font-size: 11.5px;">Proveedor:</label>
                                            <div id="proveedor-grp" class="input-group">
                                                <input type="text" id="txt_proveedor" name="txt_proveedor" placeholder="Ruc o Nombre del Proveedor" class="form-control mayus input-sm ui-autocomplete-input" value="<?php echo $proveedor_nom;?>" autocomplete="off">
                                                <?php
                                                if (in_array($_SESSION['usuariogrupo_id'], array(2, 6))) { //2: grupo gerencia, 6: grupo contabilidad ?>
                                                    <span class="input-group-btn">
                                                        <button id="btn_mod" class="btn btn-primary btn-sm" type="button" onclick="proveedor_form('M', 0)" title="MODIFICAR PROVEEDOR">
                                                            <span class="fa fa-pencil icon"></span>
                                                        </button>
                                                    </span>
                                                    <span class="input-group-btn">
                                                        <button id="btn_agr" class="btn btn-primary btn-sm" type="button" onclick="proveedor_form('I', 0)" title="AGREGAR PROVEEDOR">
                                                            <span class="fa fa-plus icon"></span>
                                                        </button>
                                                    </span>
                                                <?php } ?>
                                                <span class="input-group-btn">
                                                    <button id="btn_limpiar_prov" class="btn btn-primary btn-sm" type="button" onclick="limpiar_proveedor()" title="LIMPIAR DATOS DEL PROVEEDOR">
                                                        <span class="fa fa-eraser icon"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <label for="txt_gasto_cantidad">Cantidad:</label>
                                            <input type="text" name="txt_gasto_cantidad" id="txt_gasto_cantidad" class="form-control mayus input-sm cant para_calc" placeholder="Cantidad" value="<?php echo $cantidad;?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="cmb_moneda_id" class="control-label">Moneda:</label>
                                            <select class="form-control input-sm" name="cmb_moneda_id" id="cmb_moneda_id">
                                                <?php $tipocuenta = 2; ?>
                                                <?php require_once('../moneda/moneda_select.php') ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <label for="txt_gasto_preunit">Precio Unitario:</label>
                                            <input type="text" name="txt_gasto_preunit" id="txt_gasto_preunit" class="form-control mayus input-sm moneda para_calc" placeholder="P. Unitario" value="<?php echo $pre_unit;?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="txt_gasto_pretotal">Precio Total:</label>
                                            <input type="text" name="txt_gasto_pretotal" id="txt_gasto_pretotal" class="form-control mayus input-sm moneda disabled" placeholder="P. Total" value="<?php echo $pre_total;?>">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <label for="txt_descripcion">Descripción:</label>
                                            <textarea class="form-control mayus input-sm" name="txt_descripcion" id="txt_descripcion" rows="2" cols="85" placeholder="Descripción"><?php echo $desc;?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                PAGO GASTO
                            </div>
                            <div class="panel-body">
                                <div class="shadow">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <label for="cbo_cuenta_id">Cuenta:</label>
                                            <select name="cbo_cuenta_id" id="cbo_cuenta_id" class="form-control input-sm">
                                                <?php require_once ('../cuenta/cuenta_select.php') ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <label for="cbo_subcuenta_id">Subcuenta:</label>
                                            <select name="cbo_subcuenta_id" id="cbo_subcuenta_id" class="form-control input-sm">
                                                <?php require_once '../subcuenta/subcuenta_select.php'; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="cbo_forma_egreso">Forma de egreso:</label>
                                            <select name="cbo_forma_egreso" id="cbo_forma_egreso" class="form-control input-sm mayus">
                                                <option value="1" <?php echo $forma_pago[0] ?>>Egreso caja Efectivo</option>
                                                <option value="2" <?php echo $forma_pago[1] ?>>Ingreso/Egreso Banco</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <div class="num_ope">
                                                <label for="txt_numope">N° Operación:</label>
                                                <input type="text" name="txt_numope" id="txt_numope" class="form-control input-sm mayus" placeholder="Num. operación" value="<?php echo $numope; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6" style="padding: 6px 0px;">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="che_emite_docsunat" name="che_emite_docsunat" class="flat-green" value="1" <?php echo $emite_doc_sunat; ?>><b>&nbsp; Emite comprobante</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="emite_doc_sunat">
                                                <label for="txt_nro_documento">N° Documento:</label>
                                                <input type="text" name="txt_nro_documento" id="txt_nro_documento" class="form-control input-sm mayus" placeholder="E001-00000001" value="<?php echo $nro_doc;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="gasto_mensaje" style="display: none;">
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_gasto">Guardar</button>
                        <?php endif ?>
                        <?php if ($usuario_action == 'E') : ?>
                            <button type="submit" class="btn btn-info" id="btn_guardar_gasto">Aceptar</button>
                        <?php endif ?>
                        <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/gasto/gasto_retencion_form.js?ver=111'; ?>"></script>