$(document).ready(function() {
  $( "#txt_fil_cli" ).autocomplete({
    minLength: 1,
    source: function(request, response){
      $.getJSON(
        VISTA_URL+"cliente/cliente_autocomplete.php",
        {term: request.term}, //
        response
      );
    },
    select: function(event, ui){
      $('#hdd_fil_cli_id').val(ui.item.cliente_id);
      $('#txt_fil_cli').val(ui.item.cliente_nom);
      cobranza_tabla();
      event.preventDefault();
      $('#txt_fil_cli').focus();
    }
  });

  $( "#txt_cli_gestion" ).autocomplete({
    minLength: 1,
    source: function(request, response){
      $.getJSON(
        VISTA_URL+"cliente/cliente_autocomplete.php",
        {term: request.term}, //
        response
      );
    },
    select: function(event, ui){
      $('#hdd_cli_id_gestion').val(ui.item.cliente_id);
      $('#txt_cli_gestion').val(ui.item.cliente_nom);
      cobranza_tabla_gestion_2();
      event.preventDefault();
      $('#txt_cli_gestion').focus();
    }
  });
  
  $('#datetimepicker1').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $('#datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy",
    //startDate: "-0d"
    //endDate : new Date()
  });

  $("#datetimepicker1").on("changeDate", function (e) {
    var startVal = $('#txt_fil_cre_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);
  });
  $("#datetimepicker2").on("changeDate", function (e) {
    var endVal = $('#txt_fil_cre_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);
  });
  

});

function recordatorio_cobranza(credito_id, cuota_id){
  $.confirm({
    columnClass: 'medium',
    title: 'Recordatorio de Pago',
    type: 'purple',
    typeAnimated: true,
    buttons: {
      close: {
        text: 'Cerrar',
        action: function(){}
      }
    },
    content: function () {
      var self = this;
      return $.ajax({
        data: ({
          action: 'recordatorio_cmenor', // PUEDE SER: L, I, M , E
          credito_id: credito_id,
          cuota_id: cuota_id,
          vista: 'creditomenor'
        }),
        url: VISTA_URL + "cobranza/cobranza_recordatorio.php",
        dataType: 'html',
        method: 'POST'
      }).done(function (response) {
        self.setContent(response);
        // self.setContentAppend('<br>Version: ' + response.version);
        // self.setTitle(response.name);
        //console.log(response);
      }).fail(function(data){
        self.setContent('Something went wrong.');
        console.log(data);
      });
    }
  });
}

function creditomenor_form(usuario_act, creditomenor_id){
  $.ajax({
        type: "POST",
        url: VISTA_URL+"creditomenor/creditomenor_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            creditomenor_id: creditomenor_id,
            vista: 'creditomenor'
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
                $('#div_modal_creditomenor_form').html(data);
                $('#modal_registro_creditomenor').modal('show');

                //desabilitar elementos del form si es L (LEER)
                if(usuario_act == 'L' || usuario_act == 'E')
                  form_desabilitar_elementos('form_creditomenor'); //funcion encontrada en public/js/generales.js

                //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
                modal_width_auto('modal_registro_creditomenor', 95);
                modal_height_auto('modal_registro_creditomenor'); //funcion encontrada en public/js/generales.js
                modal_hidden_bs_modal('modal_registro_creditomenor', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else{
              //llamar al formulario de solicitar permiso
              var modulo = 'creditomenor';
              var div = 'div_modal_creditomenor_form';
              permiso_solicitud(usuario_act, creditomenor_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){

        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
            console.log(data.responseText);
        }
	});
}

function carousel(modulo_nom, modulo_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"templates/carousel.php",
    async: true,
    dataType: "html",
    data: ({
      modulo_nom: modulo_nom,
      modulo_id: modulo_id
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Galería');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      $('#div_modal_carousel').html(data);
      $('#modal_carousel_galeria').modal('show');

      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_carousel_galeria'); //funcion encontrada en public/js/generales.js
      modal_hidden_bs_modal('modal_carousel_galeria', 'limpiar'); //funcion encontrada en public/js/generales.js
    },
    complete: function(data){

    },
    error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
      console.log(data.responseText);
    }
  });
}

function cobranza_form(idf, id2, notif_id){
	var cliente = $('#span_cli'+idf).text();
  //console.log('cliente: ' + cliente);
	$.ajax({
		type: "POST",
		url: VISTA_URL+"cobranza/cobranza_form.php",
		async:true,
		dataType: "html",                      
		data: ({
			cuo_id:	idf,
			cuo_tip:id2,
			cli_nom: cliente,
			vista:	'remate',
			vistas:	'cobranza',
      notif_id: notif_id
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_cobranza_form').html(html);		
      $('#modal_registro_cobranza').modal('show');
      $('#modal_mensaje').modal('hide');
          
      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal('modal_registro_cobranza', 'limpiar'); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('modal_registro_cobranza'); //funcion encontrada en public/js/generales.js
      //funcion js para agregar un ancho automatico al modal, al abrirlo
      //modal_width_auto('modal_registro_cobranza', 75); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

//daniel 20-02-23
$('#ver_notificaciones').on('click', function(){
  cobranza_notificacion_form();
});

function cobranza_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_tabla.php",
    async: true,
    dataType: "html",
    data: $("#for_fil_cre").serialize(),
    beforeSend: function() {
      $('#cobranza_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_cobranza_tabla').html(data);
      $('#cobranza_mensaje_tbl').hide(300);
      //estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#cliente_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function visualizacionCobranza(){
  $('#main').addClass('hidden');
  $('#content_gestion').addClass('hidden');
  $('#content_visualizacion').removeClass('hidden');

  cobranza_notificacion_form();
}

function gestion_cobranza(){

  $('#main').addClass('hidden');
  $('#content_visualizacion').addClass('hidden');
  $('#content_gestion').removeClass('hidden');

  //confirmGenerarCartera(); // generacion de cartera automatica
  cobranza_notificacion_form();
  confirmGenerarCartera(); // generacion de cartera automatica

  //verResponsable($( "#cmb_fase_cobranza").val());

}

function verResponsable(id){
  //console.log('aqui');
  let fase_id = parseInt(id);
  if(fase_id>0){
    $('#div_generar_cartera').removeClass('hidden');
    //detectar_cartera_hoy();
    confirmGenerarCartera(); // generacion de cartera automatica
  }else{
    $('#div_generar_cartera').addClass('hidden');
  }
}

function cobranza_tabla_gestion(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_tabla_gestion.php",
    async: true,
    dataType: "html",
    data: $("#for_fil_cre_gestion").serialize(),
    beforeSend: function() {
      $('#cobranza_mensaje_tbl_gestion').show(300);
      if($('#txt_cli_gestion').val()=='' || $('#txt_cli_gestion').val()==null){
        $('#hdd_cli_id_gestion').val('');
      }
    },
    success: function(data){
      $('#div_cobranza_tabla_gestion').html(data);
      $('#cobranza_mensaje_tbl_gestion').hide(300);
      //estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#cliente_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function cobranza_tabla_gestion_2(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_tabla_gestion_2.php",
    async: true,
    dataType: "html",
    data: $("#for_fil_cre_gestion").serialize(),
    beforeSend: function() {
      $('#cobranza_mensaje_tbl_gestion').show(300);
      if($('#txt_cli_gestion').val()=='' || $('#txt_cli_gestion').val()==null){
        $('#hdd_cli_id_gestion').val('');
      }
    },
    success: function(data){
      $('#div_cobranza_tabla_gestion').html(data);
      $('#cobranza_mensaje_tbl_gestion').hide(300);
      //estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#cliente_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function detectar_cartera_hoy(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_controller.php",
    async: true,
    dataType: "JSON",
    data: ({
      action: 'buscar_cartera',
      fase_id: $("#cmb_fase_cobranza").val(),
      fecha: $("#txt_fecha_hoy").val()
    }),
    beforeSend: function() {
      //$('#div_user_tema').html("<span class='badge bg-aqua'>Guardando Tema...</span>");
      //$('#div_user_tema').show(300);
    },
    success: function(data){
      /* console.log(data);
      if(parseInt(data.estado) == 1){
        $('#btn_generar_cartera').addClass("hidden");
        $('#btn_buscar_cartera').removeClass("hidden");
        //$('#div_filtro_cartera').removeClass("hidden");

      }else if(parseInt(data.estado) == 2){
        $('#btn_buscar_cartera').addClass("hidden");
        $('#btn_generar_cartera').removeClass("hidden");

      } */
    },
    complete: function(data){
      verCartera();
    },
    error: function(data){
      //$('#div_user_tema').html("<span class='badge bg-red'>"+data.responseText+"</span>");
    }
  }); 
  
}

function verCartera() {
  var cli_id = $('#hdd_cli_id_gestion').val();
  var cli_nom = $('#txt_cli_gestion').val();
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_controller.php",
    async: true,
    dataType: "JSON",
    data: ({
      action: 'detectar_responsable_cartera',
      fase_id: $("#cmb_fase_cobranza").val(),
      fecha: $("#txt_fecha_hoy").val()
    }),
    beforeSend: function() {
    },
    success: function(data){
      if(parseInt(data.estado) == 1){
        $('#div_filtro_cartera').removeClass("hidden");
        $('#div_cobranza_tabla_gestion').removeClass("hidden");

        if(cli_nom == null || cli_nom == ''){
          cobranza_tabla_gestion();
        }else{
          cobranza_tabla_gestion_2();
        }

      }else{
        $('#div_filtro_cartera').addClass("hidden");
        $('#div_cobranza_tabla_gestion').addClass("hidden");

        Swal.fire(
          '¡Aviso!',
          data.mensaje,
          'warning'
        )
      }
    },
    complete: function(data){
      console.log(data);
    },
    error: function(data){
    }
  });
}

function generarCartera() {
  var hoy = $("#txt_fecha_hoy").val();
  var txt = "¿Desea generar su cartera del día de hoy <strong>"+hoy+"</strong>?";
  Swal.fire({
    title: '¿DESEA GENERAR CARTERA DEL DÍA DE HOY?',
    icon: 'warning',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> OK',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> Cancelar',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#DD3333',
  }).then((result) => {

    if (result.isConfirmed) {
      confirmGenerarCartera();
    }
  });
  
}

function confirmGenerarCartera() {
  $.ajax({
    type: "POST",
    url: VISTA_URL+"cobranza/cobranza_controller.php",
    async: true,
    dataType: "JSON",
    data: ({
      action: 'generar_cartera_hoy',
      fase_id: $("#cmb_fase_cobranza").val(),
      fecha: $("#txt_fecha_hoy").val()
    }),
    beforeSend: function() {
    },
    success: function(data){
      if(data.estado==0){
        swal_warning('SISTEMA',data.mensaje,3000);
      }
      if(data.estado==1){
        swal_success('SISTEMA',data.mensaje,3000);
      }
    },
    complete: function(data){
      //detectar_cartera_hoy();
      verCartera();
    },
    error: function(data){
    }
  });
}

/* GERSON (10-02-23) */
function estadoCuentaCobranza(credito_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"cobranza/cobranza_estado_cuenta.php",
		async:true,
		dataType: "html",                      
		data: ({
			credito_id: credito_id
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_estado_cuenta').html(html);		
      $('#modal_cobranza_estado_cuenta').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_cobranza_estado_cuenta', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_cobranza_estado_cuenta'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

/* GERSON (13-02-23) */
function estadoCuentaPDF(credito_id){
	window.open("vista/cobranza/pdf_cobranza_estado_cuenta.php?credito_id="+credito_id,"_blank");
}

/*  */

// daniel odar 14-02
function cobranza_notificacion_form(){
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cobranza/cobranza_notificacion_form.php",
    async: true,
    dataType: "html",
    data: ({}),
    beforeSend: function () {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function (html) {
      $('#div_cobranza_notificacion_form').html(html);
      $('#div_modal_cobranza_notificacion_form').modal('show');
      $('#modal_mensaje').modal('hide');

      //funcion js para limbiar el modal al cerrarlo
      modal_hidden_bs_modal('div_modal_cobranza_notificacion_form', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_width_auto('div_modal_cobranza_notificacion_form', 40);
      //funcion js para agregar un largo automatico al modal, al abrirlo
      modal_height_auto('div_modal_cobranza_notificacion_form'); //funcion encontrada en public/js/generales.js
    },
    complete: function (html) {
    }
  });
}

/* GERSON (14-02-23) */
function compromisoPagoCobranza(cuota_id, credito_id, vista){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"cobranza/cobranza_compromiso_pago.php",
		async:true,
		dataType: "html",                      
		data: ({
			cuota_id: cuota_id,
			credito_id: credito_id,
      vista: vista
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_compromiso_pago').html(html);		
      $('#modal_cobranza_compromiso_pago').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_cobranza_compromiso_pago', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_cobranza_compromiso_pago'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

/* GERSON (17-02-23) */
function remateGarantia2Cobranza(credito_id){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"cobranza/cobranza_remate_garantia.php",
		async:true,
		dataType: "html",                      
		data: ({
			credito_id: credito_id
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_remate_garantia').html(html);		
      $('#modal_cobranza_remate_garantia').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_cobranza_remate_garantia', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_cobranza_remate_garantia'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

function garantia_resumen(garantia_id){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"garantia/garantia_resumen.php",
    async: true,
    dataType: "html",
    data: ({
      garantia_id: garantia_id,
      vista: 'cobranza',
      action: 'I',
      formenor_rec: $('#hdd_formenor_rec').val(),
      formenor_pro: $('#hdd_formenor_pro').val()
    }),
    beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
    success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
        $('#div_modal_garantia_resumen').html(data);
        $('#modal_garantia_resumen').modal('show');
        //funcion js para agregar un largo automatico al modal, y un ancho al abrirlo
        modal_height_auto('modal_garantia_resumen'); //funcion encontrada en public/js/generales.js
        modal_hidden_bs_modal('modal_garantia_resumen', 'limpiar'); //funcion encontrada en public/js/generales.js
      }
    },
    complete: function(data){
      
    },
    error: function(data){
      alerta_error('Error', data.responseText);
      console.log(data);
    }
	});
}

function remateGarantia1Cobranza(cuota_id, credito_id, vista){

  Swal.fire({
    title: '¿SEGURO QUE DESEA PASAR EL CRÉDITO A REMATE?',
    icon: 'warning',
    showCloseButton: true,
    showCancelButton: false,
    focusConfirm: false,
    confirmButtonText:'<i class="fa fa-check"></i> OK',
    showDenyButton: true,
    denyButtonText: '<i class="fa fa-band"></i> Cancelar',
    confirmButtonColor: '#3b5998', 
    denyButtonColor: '#DD3333',
    }).then((result) => {

    if (result.isConfirmed) {
        $.ajax({
          type: "POST",
          url: VISTA_URL + "cobranza/cobranza_controller.php",
          async: false,
          dataType: "json",
          data: ({
            action: 'pasar_remate',
            cuota_id: cuota_id,
            credito_id: credito_id
          }),
          beforeSend: function () {
          },
          success: function (html) {
            //$('#div_inventarioproducto_tabla').html(html);
          },
          complete: function (html) {
            swal_success("¡Hecho!","Crédito pasado a Remate.",1500);
            if(vista == '1'){
              cobranza_tabla_gestion();
            }else{
              cobranza_tabla_gestion_2();
            }
          }
        });
      }
  });
}

/* GERSON (18-02-23) */
function prorrogaCuotaCobranzaProceso(cobranza_id, cuota_id, credito_id, vista){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"cobranza/cobranza_prorroga.php",
		async:true,
		dataType: "html",                      
		data: ({
			cobranza_id: cobranza_id,
			cuota_id: cuota_id,
			credito_id: credito_id,
      vista: vista
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_prorroga').html(html);		
      $('#modal_cobranza_prorroga').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_cobranza_prorroga', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_cobranza_prorroga'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {

    }
	});
}

/* GERSON (22-02-23) */
function prorrogaCuotaCobranza(cobranza_id, cuota_id, credito_id, hddvista){
	$.ajax({
		type: "POST",
		url: VISTA_URL+"claverapida/claverapida.php",
		async:true,
		dataType: "html",                      
		data: ({
			vista: 'cobranza',
			cobranza_id: cobranza_id,
			cuota_id: cuota_id,
			credito_id: credito_id,
      hddvista: hddvista
		}),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
    },
		success: function(html){
      $('#div_modal_claverapida').html(html);		
      $('#modal_claverapida').modal('show');
      $('#modal_mensaje').modal('hide');
          
      modal_hidden_bs_modal('modal_claverapida', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_height_auto('modal_claverapida'); //funcion encontrada en public/js/generales.js
		},
    complete: function (html) {
      //prorrogaCuotaCobranzaProceso(cobranza_id, cuota_id, credito_id);
    }
	});
}

//Gerson 25-02-23
function upload_cobranza_form(usuario_act, upload_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "upload/upload_form.php",
      async: true,
      dataType: "html",
      data: ({
          action: usuario_act, // PUEDE SER: L, I, M , E
          upload_id: upload_id,
          modulo_nom: 'cobranza', //nombre de la tabla a relacionar
          modulo_id: upload_id, //aun no se guarda este modulo
          //modulo_id: 0, //aun no se guarda este modulo
          upload_uniq: $('#upload_uniq').val() //ID temporal
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (data) {
          $('#modal_mensaje').modal('hide');
          if (data != 'sin_datos') {
              $('#div_modal_upload_form').html(data);
              $('#modal_registro_upload').modal('show');

              //funcion js para agregar un largo automatico al modal, al abrirlo
              modal_height_auto('modal_registro_upload'); //funcion encontrada en public/js/generales.js
              modal_hidden_bs_modal('modal_registro_upload', 'limpiar'); //funcion encontrada en public/js/generales.js
          }
      },
      complete: function (data) {
        upload_cobranza_galeria(upload_id)
      },
      error: function (data) {
          alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
          swal_error('ERRROR', data.responseText, 5000);
      }
  });
}

function upload_cobranza_galeria(cobranza_id) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "upload/upload_galeria.php",
      async: true,
      dataType: "html",
      data: ({
          modulo_nom: 'cobranza', //nombre de la tabla a relacionar
          modulo_id: cobranza_id
      }),
      beforeSend: function () {

      },
      success: function (data) {
          $('.galeria_cobranza').html(data);
      },
      complete: function (data) {

      },
      error: function (data) {
          alerta_error('Error', 'ERRROR!:' + data.responseText); //en generales.js
          swal_error('ERRROR', data.responseText, 5000);
      }
  });
}
//