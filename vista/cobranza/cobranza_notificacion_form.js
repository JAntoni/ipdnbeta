contador = 1;

$(document).ready(function () {
  consultar_datos();
});

function consultar_datos() {
  var lista_porhacer_actual = $("#ul_lista_por_hacer").html();
  var lista_hecho = $("#ul_lista_hecho").html();
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cobranza/cobranza_notificacion_form_vista.php",
    async: true,
    dataType: "json",
    data: {},
    beforeSend: function () {},
    success: function (data) {
      if (lista_porhacer_actual !== data.por_hacer || lista_hecho !== data.hecho) {
        $("#ul_lista_por_hacer").html(data.por_hacer);
        $("#ul_lista_hecho").html(data.hecho);
        if (contador > 1) {
          notificacion_info("Se actualizaron las notificaciones", 2000);
        }
      }
    },
    complete: function (html) {},
  });
  console.log("executing");
}

interval_reload_notif = setInterval(function () {
  consultar_datos();
  contador++;
}, 1000 * 7.5); //se actualizará cada 7.5 segundos

$("#div_cobranza_notificacion_form").on("hidden.bs.modal", function () {
  clearInterval(interval_reload_notif);
  interval_reload_notif = null;
  contador = 1;
  console.log("stop");
});

function boton_ok(noti_id) {
  $.ajax({
    type: "POST",
    url: VISTA_URL + "cobranza/cobranza_controller.php",
    async: true,
    dataType: "html",
    data: {
      action: "notificacion_ok",
      noti_id: noti_id,
    },
    beforeSend: function () {},
    success: function (data) {
      data = JSON.parse(data);
      console.log("result: " + data.estado);
      if (data.estado == 1) {
        swal_success("SISTEMA", data.mensaje, 4000);
        consultar_datos();
      } else {
        swal_error("SISTEMA", data.mensaje, 4000);
      }
    },
    complete: function (html) {},
  });
}
