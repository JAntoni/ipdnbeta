<?php
require_once('../../core/usuario_sesion.php');

require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$vista = $_POST['vista'];
$cobranza_id = $_POST['cobranza_id'];
$cuota_id = $_POST['cuota_id'];
$credito_id = $_POST['credito_id'];
$color = '';

$cobranza = $oCobranza->mostrarUno($cobranza_id);
$cuota = $oCuota->mostrarUno($cuota_id);
if(date('Y-m-d')<=$cuota['data']['tb_cuota_fec']){
    $color = '#00A65A';
}else{
    $color = '#DD4B39';
}
//$fecha_cuota = mostrar_fecha($cuota['data']['tb_cuota_fec']);
$fecha_cuota = $cuota['data']['tb_cuota_fec'];
$fecha_cobranza = $cobranza['data']['tb_cobranza_feccuota'];
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_cobranza_prorroga" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">AMPLIAR VENCIMIENTO DE CUOTA</h4>
            </div>
            <form id="form_prorroga" method="post">
                <input name="action_cobranza" id="action_cobranza" type="hidden" value="prorroga">
                <input name="hdd_cobranza_id" id="hdd_cobranza_id" type="hidden" value="<?php echo $cobranza_id; ?>">
                <input name="hdd_cuota_id" id="hdd_cuota_id" type="hidden" value="<?php echo $cuota_id; ?>">
                <input name="hdd_credito_id" id="hdd_credito_id" type="hidden" value="<?php echo $credito_id; ?>">
                <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $vista ?>">

                <div class="modal-body">
                    <div class="row">
                        <?php if(strtotime($fecha_cuota) == strtotime($fecha_cobranza)){ ?>
                            <div class="col-md-12 pb-3">
                                <div class="form-group">
                                    <label>Fecha vencimiento cuota:</label>
                                    <h5 style="color:<?php echo $color; ?>;"><strong><?php echo mostrar_fecha($fecha_cuota); ?></strong></h5>
                                </div>
                            </div>
                        <?php }else{ ?>
                            <div class="col-md-12 pb-3">
                                <div class="form-group">
                                    <label>Fecha vencimiento cuota:</label>
                                    <h5 style="color:<?php echo $color; ?>;"><strong><?php echo mostrar_fecha($fecha_cuota); ?></strong></h5>
                                </div>
                            </div>
                            <div class="col-md-12 pb-3">
                                <div class="form-group">
                                    <label>Última fecha vencimiento:</label>
                                    <h5 style="color:<?php echo $color; ?>;"><strong><?php echo mostrar_fecha($fecha_cobranza); ?></strong></h5>
                                </div>
                            </div>
                        <?php } ?>
                        

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fec_prorr" class="control-label">Nueva fecha vencimiento: </label>
                                <div class="input-group date" id="">
                                    <input type="text" name="txt_fec_prorr" id="txt_fec_prorr" class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" placeholder="DD-MM-YYYY">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>

                    </div>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_menorgarantia">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cobranza/cobranza_form.js?ver==20230321';?>"></script>