<?php //require_once('cobranza_notificacion_form_vista.php'); 
?>
<div class="modal fade" tabindex="-1" role="dialog" id="div_modal_cobranza_notificacion_form" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">NOTIFICACIONES DE COBRANZA</h4>
            </div>

            <div class="modal-body">
                <div>
                    <div class="box box-danger shadow">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tareas por hacer</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="box-body">
                            <ul id="ul_lista_por_hacer" name="ul_lista_por_hacer" class="products-list product-list-in-box"><?php //echo $listas["por_hacer"];?></ul>
                        </div>
                    </div>
                </div>
                <br>
                <div>
                    <div class="box box-success shadow">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tareas hechas hoy</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="box-body">
                            <ul id="ul_lista_hecho" name="ul_lista_hecho" class="products-list product-list-in-box"><?php //echo $listas["hecho"];?></ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btn_cerrar" class="btn btn-default" onclick="" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cobranza/cobranza_notificacion_form.js?ver=612127877132314781'; ?>"></script>