<?php
 if(defined('VISTA_URL')){
        require_once(APP_URL.'core/usuario_sesion.php');
        require_once (VISTA_URL.'cuotapago/Cuotapago.class.php');
        require_once (VISTA_URL.'cliente/Cliente.class.php');
        require_once (VISTA_URL.'funciones/funciones.php');
        require_once (VISTA_URL.'funciones/fechas.php');
  }
  else{
        require_once('../../core/usuario_sesion.php');
        require_once ("../cuotapago/Cuotapago.class.php");
        require_once ("../cliente/Cliente.class.php");
        require_once ("../funciones/funciones.php");
        require_once ("../funciones/fechas.php");
  }
require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
$oCuotapago = new Cuotapago();
$oCliente = new Cliente();

$fecha_hoy = date('d-m-Y');

//CONTADORES GENERALES
$NUMERO_ORDEN = 0;
$ARRAY_CLIENTES = array();
$TOTAL_SOLES = 0;
$TOTAL_DOLARES = 0;
$NUMERO_CUOTAS = 0;

$subtotal_soles = 0;
$subtotal_dolares = 0;

//<!-- CREDITOS MENORES-->

  if($_POST['cbm_fil_cre_gru'] == "-" || $_POST['cbm_fil_cre_gru'] == "CM"){

    $dts=$oCobranza->filtrar_menor($_POST['hdd_fil_cli_id'],fecha_mysql($_POST['txt_fil_cre_fec1']),fecha_mysql($_POST['txt_fil_cre_fec2']),$_POST['cbm_fil_cuo_cond']); ?>

    <h1 style="text-align:left; padding:10px 0 0 5px;margin-bottom:2px;">CRÉDITO MENOR</h1>
    <table id="tabla_cobranza" class="table ">
      <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">N</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">TIPO</th>
            <th id="tabla_cabecera_fila" align="center">MONTO</th>
            <th id="tabla_cabecera_fila" align="center">VENCIMIENTO</th>
            <th id="tabla_cabecera_fila" align="center">CUOTAS</th>
            <th id="tabla_cabecera_fila">ASESOR</th>
            <th id="tabla_cabecera_fila">NOTAS</th>
            <th id="tabla_cabecera_fila">VER</th>
            <th id="tabla_cabecera_fila">ID</th>
        </tr>
      </thead>
      <?php
        if($dts['estado']==1){ ?>
          <tbody> <?php
            foreach($dts['data']as $key=>$dt){
              //contadores generales
              $NUMERO_ORDEN ++;
              array_push($ARRAY_CLIENTES, $dt['tb_cliente_id']);
              $NUMERO_CUOTAS ++;

              if($dt['tb_moneda_id']==1)$moneda="S/. ";
              if($dt['tb_moneda_id']==2)$moneda="US$ ";
              $vencida = 1;
              if(strtotime($fecha_hoy) <= strtotime($dt['tb_cuota_fec'])){$vencida = 0;}; ?>

              <tr id="tabla_cabecera_fila">
                <td id="tabla_fila"><?php echo $NUMERO_ORDEN;?></td>
                <td id="tabla_fila">
                  <?php
                    echo '<span id="span_cli'.$dt['tb_cuota_id'].'">'.$dt['tb_cliente_nom']."</span> | ".$dt['tb_cliente_doc'];
                    echo '<br><span style="font-weight: normal;">'.$dt['tb_cliente_cel'].'('.$dt['tb_cliente_procel'].')'.' | '.$dt['tb_cliente_tel'].'('.$dt['tb_cliente_protel'].')'.' | '.$dt['tb_cliente_telref'].'('.$dt['tb_cliente_protelref'].')</span>';
                  ?>
                </td>
                <td id="tabla_fila"><?php echo $dt['tb_cuotatipo_nom']?></td>
                <?php
                  $pagos_cuota = 0;
                  $mod_id=1;
                  $dts3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$dt['tb_cuota_id']);
                  if($dts3['estado']==1)
                  {
                      foreach($dts3['data']as $key=>$dt3)
                    {
                      $pagos_cuota += moneda_mysql($dt3['tb_ingreso_imp']);
                    }
                  }
                  if($dt['tb_cuotatipo_id'] == 1)
                    $saldo = moneda_mysql($dt['tb_cuota_int']-$pagos_cuota);
                  else
                    $saldo = moneda_mysql($dt['tb_cuota_cuo']-$pagos_cuota);

                  if($dt['tb_moneda_id']==1){
                    if($dt['tb_cuotatipo_id'] == 1)
                      $TOTAL_SOLES += moneda_mysql($dt['tb_cuota_int']-$pagos_cuota);
                    else
                      $TOTAL_SOLES += moneda_mysql($dt['tb_cuota_cuo']-$pagos_cuota);
                    $subtotal_soles += $saldo;
                  }
                  if($dt['tb_moneda_id']==2){
                    if($dt['tb_cuotatipo_id'] == 1)
                      $TOTAL_DOLARES += moneda_mysql($dt['tb_cuota_int']-$pagos_cuota);
                    else
                      $TOTAL_DOLARES += moneda_mysql($dt['tb_cuota_cuo']-$pagos_cuota);
                    $subtotal_dolares += $saldo;
                  }

                  $string = str_replace("-", "", restaFechas($fecha_hoy,mostrar_fecha($dt['tb_cuota_fec'])));
                  $string = trim($string);
                ?>
                <td  id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $moneda . mostrar_moneda($saldo);?></td>
                <td  id="tabla_fila"align="center" <?php if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}?>>
                  <?php
                    if($vencida == 1){
                      echo mostrar_fecha($dt['tb_cuota_fec']) . " (Vencida hace " . str_replace("-", "", $string) . " días)";
                      echo '<button type="button" class="btn btn-danger btn-sm" onclick="recordatorio_cobranza('.$dt['tb_credito_id'].', '.$dt['tb_cuota_id'].')">Recordatorio</button>';
                    }
                    else{ 
                      echo mostrar_fecha($dt['tb_cuota_fec']) . " (Vence en " . $string . " días)";
                      if(intval($string) <= 5)
                        echo '<button type="button" class="btn btn-info btn-sm" onclick="recordatorio_cobranza('.$dt['tb_credito_id'].', '.$dt['tb_cuota_id'].')">Recordatorio</button>';
                    }
                    
                  ?>
                </td>
                <td  id="tabla_fila" align="center">
                  <?php
                    if($dt['tb_cuotatipo_id']==2){
                      echo $dt['tb_cuota_num'].'/'. $dt['tb_credito_numcuo'];
                    }
                    else{
                      echo $dt['tb_cuota_num'].'/'. $dt['tb_credito_numcuomax'];
                    }
                  ?>
                </td>
                <td id="tabla_fila"><?php echo $dt['tb_usuario_nom'] . ' ' . $dt['tb_usuario_ape'];?></td>
                <td id="tabla_fila">
                  <button class="btn btn-success btn-xs" href="javascript:void(0)" onClick="cobranza_form('<?php echo $dt['tb_cuota_id']?>','1')">+</button><span id="cuota_id_<?php echo $dt['tb_cuota_id'];?>"> <?php echo $dt['tb_cuota_not'] ?></span>
                </td>
                <td id="tabla_fila">
                  <a class="btn btn-warning btn-xs" href="javascript:void(0)" onClick="creditomenor_form('L','<?php echo $dt['tb_credito_id']?>')">Ver</a>
                </td>
                <td id="tabla_fila">CM-<?php echo str_pad($dt['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></td>
              </tr>
              <?php
            }
                ?>
          </tbody>
          <tfoot>
            <tr class="even">
              <td  id="tabla_fila"colspan="3">Sub Total</td>
              <td id="tabla_fila"><?php echo 'S/. '. mostrar_moneda($subtotal_soles); ?></td>
              <td  id="tabla_fila"colspan="7"></td>
            </tr>
          </tfoot> <?php
        }
      ?>
    </table> <?php
  }
?>
<!-- FIN CREDITO MENOR-->