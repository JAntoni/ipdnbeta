<?php
session_name("ipdnsac");
session_start();
if (!isset($_SESSION['usuario_id'])) {
	echo 'Terminó la sesión';
	exit();
}
?>
<!DOCTYPE html>
<html>

<head>
	<!-- TODOS LOS ESTILOS-->
	<?php include(VISTA_URL . 'templates/head.php'); ?>
	<title><?php echo ucwords(mb_strtolower($menu_tit)); ?></title>
	<style type="text/css">
		.product-list-in-box>.item {
			border-bottom: 2px solid #5A7D6F;
		}
	</style>
</head>

<body <?php echo 'class="' . CLASE_BODY . ' ' . $usuario_tem . '"'; ?>>

	<div class="wrapper">
		<!-- INCLUIR EL HEADER-->
		<?php include(VISTA_URL . 'templates/header.php'); ?>
		<!-- INCLUIR ASIDE, MENU LATERAL -->
		<?php include(VISTA_URL . 'templates/aside.php'); ?>
		<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
		<div id="main" class="">
			<?php include('cobranza_vista_main.php'); ?>
		</div>
		<div id="content_visualizacion" class="hidden">
			<?php include('cobranza_vista.php'); ?>
		</div>
		<div id="content_gestion" class="hidden">
			<?php include('cobranza_vista_gestion.php'); ?>
		</div>
		<div id="div_modal_creditomenor_form"></div>
		<div id="div_cobranza_notificacion_form"></div>
		<div id="div_cobranza_form">
			<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
		</div>
		<div id="div_modal_carousel">
            <!-- INCLUIMOS EL MODAL PARA LA GALERIA DE LAS FOTOS DE LOS PRODUCTOS-->
		</div>
		<!-- INCLUIR FOOTER-->
		<?php include(VISTA_URL . 'templates/footer.php'); ?>
		<div class="control-sidebar-bg"></div>
	</div>

	<!-- TODOS LOS SCRIPTS-->
	<?php include(VISTA_URL . 'templates/script.php'); ?>

	<script type="text/javascript" src="<?php echo VISTA_URL . 'cobranza/cobranza.js?ver=020623'; ?>"></script>
</body>

</html>
