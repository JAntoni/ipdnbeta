<?php
    $fec1 = date('d-m-Y');
    $fec2 = strtotime ( '+30 day' , strtotime ( $fec1 ) ) ;
    $fec2 = date ( 'd-m-Y' , $fec2 );
?>


<form name="for_fil_cre_gestion" id="for_fil_cre_gestion" target="_blank" action="" method="post">

    <div class="row">

        <div class="col-md-2 hidden">
            <label for="cmb_fase_cobranza">Fases:</label>
            <select id="cmb_fase_cobranza" name="cmb_fase_cobranza" class="form-control input-sm" onchange="verResponsable(this.value);">
                <?php //require_once 'fase_cobranza_select.php';?>
                <!-- <option value="0">-- Seleccione fase --</option> -->
                <option value="2" style="font-weight: bold;">Fase 1</option>
            </select>
        </div>

        <div class="col-md-3 hidden" id="div_generar_cartera">
            <div class="col-md-6 hidden">
                <div class="form-group">
                    <label for="">Fecha: </label>
                    <div class="input-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control input-sm" readonly name="txt_fecha_hoy" id="txt_fecha_hoy" value="<?php echo $fec1;?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <h4 for="">Cartera del <?php echo date('d-m-Y'); ?> </h4>
                </div>
            </div>
            <div class="col-md-5 hidden">
                <label for="cmb_dia_vencido">Días vencido:</label>
                <select id="cmb_dia_vencido" name="cmb_dia_vencido" class="form-control input-sm" onchange="cobranza_tabla_gestion();">
                    <option value="all">-- Todos --</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16+</option>
                </select>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for=""> </label>
                    <!-- <div id="btn_generar_cartera" class="input-group">
                        <button type="button" class="brn btn-sm btn-success" onclick="generarCartera();"><i class="fa fa-plus"></i> GENERAR CARTERA</button>
                    </div> -->
                    <!-- <div id="btn_buscar_cartera" class="input-group">
                        <button type="button" class="brn btn-sm btn-primary" onclick="verCartera();"><i class="fa fa-search"></i> VER CARTERA</button>
                    </div> -->
                </div>
            </div>
        </div>

        <div class="col-md-9 hidden" id="div_filtro_cartera">
            <div class="col-md-3">
                <label for="txt_cli_id_gestion">Cliente:</label>
                    <input type="hidden" id="hdd_cli_id_gestion" name="hdd_cli_id_gestion"  class="form-control input-sm" onchange="//cobranza_tabla_gestion_2()">
                    <input type="text" id="txt_cli_gestion" name="txt_cli_gestion"  class="form-control input-sm" onchange="//cobranza_tabla_gestion_2()">
            </div>
            <div class="col-md-3">
                <label for="cmb_fil_cuo_cond">Condición:</label>
                <select id="cmb_fil_cuo_cond" name="cmb_fil_cuo_cond" class="form-control input-sm" onchange="cobranza_tabla_gestion()">
                    <option value="-">TODAS</option>
                    <option value="VE">VENCIDAS</option>
                    <option value="PV">POR VENCER</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="cmb_grupo">Grupo:</label>
                <select id="cmb_estado_cobranza" name="cmb_estado_cobranza" class="form-control input-sm" onchange="cobranza_tabla_gestion()">
                    <?php require_once 'estado_cobranza_select.php';?>
                </select>
            </div>
            <div class="col-md-3 hidden">
                <label for="cmb_grupo">Crédito:</label>
                <select id="cmb_grupo" name="cmb_grupo" class="form-control input-sm" onchange="cobranza_tabla_gestion()">
                    <!-- <option value="-">TODAS</option> -->
                    <option value="CM">CREDITO MENOR</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                <label for=""> </label>
                <div class="input-group">
                    <!-- GERSON (11-03-23) -->
                    <div id="btn_buscar_cartera" class="input-group">
                        <button type="button" class="brn btn-sm btn-primary" onclick="verCartera();"><i class="fa fa-search"></i> VER CARTERA</button>
                    </div>
                    <!--  -->
                    <!-- <button type="button" class="btn btn-primary" onclick="cobranza_tabla_gestion()" title="Filtrar"><i class="fa fa-search"></i></button> -->
                    <!--<span class="input-group-addon"></span>-->
                    <!-- <button type="button" class="btn btn-success btn-xs" onclick="cobranza_filtro()" title="Reestablecer"><i class="fa fa-refresh"></i></button> -->
                </div>
                </div>
            </div>
        </div>
    </div>
</form>