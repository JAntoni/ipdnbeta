<?php
require_once('../../core/usuario_sesion.php');

/* require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle(); */
require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$credito_id = $_POST['credito_id'];

$data = $oCobranza->cobranza_estado_cuenta($credito_id);
$cant_cuota = count($data['data']);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_cobranza_estado_cuenta" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">ESTADO DE CUENTA</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="box-body">
                        <div class="row">
                            <!--- MESAJES DE CONSULTANDO TABLA GARANTIAS-->
                            <div class="col-md-12">
                                <div class="callout callout-info" id="cronograma_mensaje_tbl" style="display: none;">
                                    <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Listando cronograma...</h4>
                                </div>
                            </div>
                            <div class="col-md-12" id="div_creditomenor_cronograma">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr id="tabla_cabecera">
                                        <th id="tabla_cabecera_fila">N°</th>
                                        <th id="tabla_cabecera_fila">DESCRIPCIÓN</th>
                                        <th id="tabla_cabecera_fila">FECHA VENC.</th>
                                        <th id="tabla_cabecera_fila">CUOTA</th>
                                        <th id="tabla_cabecera_fila">TIP. CAMBIO</th>
                                        <th id="tabla_cabecera_fila">MONTO PAGADO</th>
                                        <th id="tabla_cabecera_fila">ESTADO</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $estado = '';
                                        $background = '';
                                        $sum_total_credito = 0.00;
                                        $sum_total_pagado = 0.00;
                                        foreach ($data['data'] as $key => $value): 
                                            $suma_monto = 0.00;

                                            if($value['tb_cuota_est']==1){
                                                $estado = 'PENDIENTE';
                                                $background = '#FC1414';
                                                $suma_monto = 0.00;
                                            }elseif($value['tb_cuota_est']==2){
                                                $estado = 'PAGADO';
                                                $background = '#01E804';
                                                $suma_monto = $value['tb_cuota_cuo'];
                                            }elseif($value['tb_cuota_est']==3){
                                                $estado = 'PAGO PARCIAL';
                                                $background = '#FCDC14';

                                                $data2 = $oCobranza->cobranza_monto_pagado($value['tb_cuota_id']);
                                                foreach ($data2['data'] as $key => $v): 
                                                    $tipo_cambio = 0.00;
                                                    $dt_mon = $oMonedacambio->consultar($v['tb_ingreso_fec']);
                                                    if($dt_mon['data']!=null || $dt_mon['data']!=''){
                                                        $tipo_cambio = $dt_mon['data']['tb_monedacambio_val'];
                                                    }

                                                    if($value['tb_moneda_id']==1){ // soles
                                                        $suma_monto = $suma_monto + $v['tb_ingreso_imp'];
                                                    }else{ // dolares
                                                        $suma_monto = $suma_monto + $v['tb_ingreso_imp']*$tipo_cambio;
                                                    }
                                                endforeach;
                                            }
                                            
                                    ?>
                                        <tr id="tabla_cabecera_fila">
                                            <td id="tabla_fila"><?php echo $value['tb_cuota_num'];?></td>
                                            <td id="tabla_fila"><?php echo 'Cuota '.$value['tb_cuota_num'].' de '.$cant_cuota;?></td>
                                            <td id="tabla_fila"><?php echo mostrar_fecha($value['tb_cuota_fec']);?></td>
                                            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($value['tb_cuota_cuo']);?></td>
                                            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($tipo_cambio);?></td>
                                            <td id="tabla_fila"><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($suma_monto);?></td>
                                            <td id="tabla_fila" style="background-color: <?php echo $background;?>; color: #FFFFFF;"><strong><?php echo $estado;?></strong></td>
                                        </tr>
                                        <?php
                                        $sum_total_credito = $sum_total_credito + $value['tb_cuota_cuo'];
                                        $sum_total_pagado = $sum_total_pagado + $suma_monto;
                                        endforeach;
                                        $result = NULL;
                                    ?>
                                        <tr id="tabla_cabecera_fila">
                                            <td id="tabla_fila"></td>
                                            <td id="tabla_fila"></td>
                                            <td id="tabla_fila"></td>
                                            <td id="tabla_fila"><strong><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($sum_total_credito);?></strong></td>
                                            <td id="tabla_fila"></td>
                                            <td id="tabla_fila"><strong><?php echo $value['tb_moneda_nom'] .' '. mostrar_moneda($sum_total_pagado);?></strong></td>
                                            <td id="tabla_fila"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn bg-maroon" onclick="estadoCuentaPDF(<?php echo $credito_id; ?>)"><i class="fa fa-fw fa-file-pdf-o"></i> Exportar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>         
                </div>
            
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cobranza/cobranza_form.js?ver==20230321';?>"></script>