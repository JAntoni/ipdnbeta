<?php

$fec1 = date('d-m-Y');
$fec2 = strtotime ( '+30 day' , strtotime ( $fec1 ) ) ;
$fec2 = date ( 'd-m-Y' , $fec2 );
?>


<form name="for_fil_cre" id="for_fil_cre" target="_blank" action="" method="post">
    <div class="row">
       <div class="col-md-3">
            <div class="form-group">
                <label for="">Fecha entre: </label>
                <div class="input-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control input-sm" name="txt_fil_cre_fec1" id="txt_fil_cre_fec1" value="<?php echo $fec1;?>"/>
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="input-group-addon">-</span>
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' class="form-control input-sm" name="txt_fil_cre_fec2" id="txt_fil_cre_fec2" value="<?php echo $fec2;?>"/>
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <label for="txt_fil_cli_id">Cliente:</label>
                <input type="hidden" id="hdd_fil_cli_id" name="hdd_fil_cli_id"  class="form-control input-sm">
                <input type="text" id="txt_fil_cli" name="txt_fil_cli"  class="form-control input-sm">
        </div>
        <div class="col-md-1">
            <label for="cbm_fil_cuo_cond">Condición:</label>
            <select id="cbm_fil_cuo_cond" name="cbm_fil_cuo_cond" class="form-control input-sm" >
              <option value="-">TODAS</option>
             <option value="VC">VENCIDAS</option>
              <option value="PV">POR VENCER</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="cbm_fil_cuo_cond">Grupo:</label>
            <select id="cbm_fil_cre_gru" name="cbm_fil_cre_gru" class="form-control input-sm">
                <option value="-">TODAS</option>
                <option value="CM">CREDITO MENOR</option>
<!--                <option value="AV">ASISTENCIA VEHICULAR</option>
                <option value="GV">GARANTÍA VEHICULAR</option>
                <option value="CH">CREDITO HIPOTECARIO</option>-->
            </select>
        </div>
        <div class="col-md-1">
            <div class="form-group">
              <label for=""> </label>
              <div class="input-group">
                  <button type="button" class="btn btn-primary btn-xs" onclick="cobranza_tabla()" title="Filtrar"><i class="fa fa-search"></i></button>
                  <!--<span class="input-group-addon"></span>-->
                  <button type="button" class="btn btn-success btn-xs" onclick="cobranza_filtro()" title="Reestablecer"><i class="fa fa-refresh"></i></button>
              </div>
            </div>
        </div>
        &nbsp;
        <div class="col-md-2">
            <div class="form-group">
                <label for=""> </label>
                <button type="button" class="btn btn-primary btn-sm" id="ver_notificaciones" name="ver_notificaciones"><i class="fa fa-eye"></i> Ver notificaciones</button>
            </div>
        </div>
    </div>
</form>