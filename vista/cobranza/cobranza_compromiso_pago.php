<?php
require_once('../../core/usuario_sesion.php');

/* require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle(); */
require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$cuota_id = $_POST['cuota_id'];
$credito_id = $_POST['credito_id'];
$vista = $_POST['vista'];
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_cobranza_compromiso_pago" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">COMPROMISO DE PAGO</h4>
            </div>
            <form id="form_compromiso" method="post">
                <input name="action_cobranza" id="action_cobranza" type="hidden" value="compromiso_pago">
                <input name="hdd_cuota_id" id="hdd_cuota_id" type="hidden" value="<?php echo $cuota_id; ?>">
                <input name="hdd_credito_id" id="hdd_credito_id" type="hidden" value="<?php echo $credito_id; ?>">
                <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $vista ?>">
                <!-- <input type="hidden" name="hdd_vistas" id="hdd_vistas" value="<?php echo $vistas ?>"> -->

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 pb-3">
                            <div class="form-group">
                                <label>Comentario:</label>
                                <textarea id="txt_com_comp" name="txt_com_comp" class="form-control input-sm mayus" rows="4"; style="resize: none;" placeholder="Registre comentario breve sobre el compromiso de pago realizado por el cliente"></textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_fec_comp" class="control-label">Fecha de compromiso: </label>
                                <div class="input-group date" id="">
                                    <input type="text" name="txt_fec_comp" id="txt_fec_comp" class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" placeholder="DD-MM-YYYY">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>

                    </div>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_menorgarantia">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cobranza/cobranza_form.js?ver==20230321';?>"></script>