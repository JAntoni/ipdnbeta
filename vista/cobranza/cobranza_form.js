$(document).ready(function() {
    $('#txt_cuo_not').focus();
    $('#txt_com_comp').focus();
    
    $('#txt_cuo_fecref').datepicker({
		language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        startDate: "-0d"
	});
    //Gerson (15-02-23)
    $('#txt_fec_comp').datepicker({
		language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        startDate: "-0d"
	});
    //Gerson (18-02-23)
    $('#txt_fec_prorr').datepicker({
		language: 'es',
        autoclose: true,
        format: "dd-mm-yyyy",
        startDate: "-0d",
        endDate: "+5d"
	});
    //
    $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green'
    });

    $('input[name*="che_notificacion"]').on('ifChanged',function(){
		if (this.checked) {
			$('#row_notificacion').show(200);
		} else {
			$('#row_notificacion').hide(200);
		}
	});
    
    $("#for_cob").validate({
        submitHandler: function () {
            var hist = $('#div_historial_cuota').html();
            $.ajax({
                type: "POST",
                url: VISTA_URL + "cobranza/cobranza_controller.php",
                async: true,
                dataType: "json",
                data: $("#for_cob").serialize(),
                beforeSend: function () {
                    $("#cobranza_mensaje").show(400);
                    $("#btn_guardar_nota_cobranza").prop("disabled", true);
                },
                success: function (data) {
                    if(data.ven_msj.includes('Se registró')){
                      var cuo_id = $("#hdd_cuo_id").val();
                      swal_success("SISTEMA", data.ven_msj, 5000);
                      $("#span_" + cuo_id).html(data.nota);
                      var vista = $("#hdd_vista").val();
                      if (vista == "cmb_ven_id") {
                        vista + "." + data.ven_id;
                      }
                      var vistas = $("#hdd_vistas").val();
                      if (vistas == "cobranza") {
                        //cobranza_tabla(); //cuando se guarda la nota no se recarga la tabla, solo se concatena el comentario
                        cuo_id = $("#hdd_cuo_id").val();
                        texto_actual = $("#cuota_id_" + cuo_id).html();
                        texto_nuevo = " <b>" + $("#hdd_usu_nom").val() + ": " + $("#txt_cuo_not").val() + " | " + data.fecha_hora + "</b><br>";
                        $("#cuota_id_" + cuo_id).html(texto_nuevo + texto_actual);
                      }

                      var notif_id = parseInt($("#hdd_notif_id").val());
                      if (notif_id > 0 &&data.ven_msj.includes("Se registró")) {
                        console.log("aqui debe actualizar");
                        boton_ok(notif_id);
                      }
                      $('#modal_registro_cobranza').modal('hide');
                    }
                    else{
                        $("#cobranza_mensaje").hide(400);
                        swal_error("SISTEMA", data.ven_msj, 5000);
                        $("#btn_guardar_nota_cobranza").prop("disabled", false);
                    }

                },
                complete: function (data) {
                    console.log(data);
                    var not = $('#txt_cuo_not').val();
                    if (data.statusText == 'OK') {
                        //$('#btn_'+cuo_id).hide();
                        //                              $("#div_cobranza_form" ).dialog( "close" );
                        //$('#modal_registro_cobranza').modal('hide');
                    }
                    else
                        swal_error("Error al guardar nota: ", data.responseText, 5000);
                }

            });
        },
        rules: {
            txt_cuo_not: {
                required: true
            }
        },
        messages: {
            txt_cuo_not: {
                required: 'Debe ingresar una nota'
            }
        }
    });

    //Gerson (15-02-23)
    $("#form_compromiso").validate({
        submitHandler: function (vista) {
            //var hist = $('#div_historial_cuota').html();
            var vista = $('#hdd_vista').val();
            $.ajax({
                type: "POST",
                url: VISTA_URL + "cobranza/cobranza_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_compromiso").serialize(),
                beforeSend: function () {
                    //                            $('#msj_cobranza').html("Guardando...");
                    //                            $('#msj_cobranza').show(100);
                },
                success: function (data) {
                    var cuo_id = $('#hdd_cuota_id').val();
                    swal_success("SISTEMA", data.ven_msj, 4000);
                    //$('#span_' + cuo_id).html(data.nota);
                    /* var vista = $("#hdd_vista").val();
                    if (vista == "cmb_ven_id") {
                        vista + '.' + (data.ven_id);
                    }
                    var vistas = $("#hdd_vistas").val(); */
                    if (vista == '1') {
                        cobranza_tabla_gestion();
                    }else{
                        cobranza_tabla_gestion_2();
                    }

                },
                complete: function (data) {
                    if (data.statusText == 'OK') {
                        $('#modal_cobranza_compromiso_pago').modal('hide');
                    }
                    else
                        swal_error("Error al guardar nota: ", data.responseText, 5000);
                }

            });
        },
        rules: {
            txt_com_comp: {
                required: true
            },
            txt_fec_comp: {
                required: true/* ,
                dateITA: true */
            }
        },
        messages: {
            txt_com_comp: {
                required: 'Debe ingresar un comentario'
            },
            txt_fec_comp: {
                required: 'Debe ingresar una fecha válida'
            }
        }
    });
    //

    //Gerson (21-02-23)
    $("#form_prorroga").validate({
        submitHandler: function () {
            //var hist = $('#div_historial_cuota').html();
            var vista = $('#hdd_vista').val();
            $.ajax({
                type: "POST",
                url: VISTA_URL + "cobranza/cobranza_controller.php",
                async: true,
                dataType: "json",
                data: $("#form_prorroga").serialize(),
                beforeSend: function () {
                    //$('#msj_cobranza').html("Guardando...");
                    //$('#msj_cobranza').show(100);
                },
                success: function (data) {
                    //var cuo_id = $('#hdd_cuota_id').val();
                    swal_success("SISTEMA", data.ven_msj, 4000);
                    //$('#span_' + cuo_id).html(data.nota);
                    /* var vista = $("#hdd_vista").val();
                    if (vista == "cmb_ven_id") {
                        vista + '.' + (data.ven_id);
                    }
                    var vistas = $("#hdd_vistas").val(); */
                    if (vista == '1') {
                        cobranza_tabla_gestion();
                    }else{
                        cobranza_tabla_gestion_2();
                    }

                },
                complete: function (data) {
                    if (data.statusText == 'OK') {
                        $('#modal_cobranza_prorroga').modal('hide');
                    }else{
                        swal_error("Error al guardar nota: ", data.responseText, 5000);
                    }
                }

            });
        },
        rules: {
            txt_fec_prorr: {
                required: true
            }
        },
        messages: {
            txt_fec_prorr: {
                required: 'Debe ingresar una fecha válida'
            }
        }
    });
    //

});