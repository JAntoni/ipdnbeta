<?php
require_once('../../core/usuario_sesion.php');

require_once("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();

$array_usu = explode(' ', $_SESSION['usuario_nom']);
$credito_id = 0;
$creditotipo_id = 0;
if ($_POST['cuo_tip'] == "1") {
    $dts = $oCuota->mostrarUno($_POST['cuo_id']);
    if ($dts['estado'] == 1) {
        $not = $dts['data']['tb_cuota_not'];
        $credito_id = $dts['data']['tb_credito_id'];
        $creditotipo_id = $dts['data']['tb_credito_id'];
    }
    $dts = NULL;
}
if ($_POST['cuo_tip'] == "2") {
    $dts = $oCuotadetalle->mostrarUno($_POST['cuo_id']);
    if ($dts['estado'] == 1) {
        $not = $dts['data']['tb_cuotadetalle_not'];
    }
    $dts = NULL;
}

$vista = $_POST['vista'];
$vistas = $_POST['vistas'];
if ($vista == 'remate') {
    $dts = $oCuota->filtrar($creditotipo_id, $credito_id);
    if ($dts['estado'] == 1) {
        foreach ($dts['data'] as $key => $dt) {
            $not += $dt['tb_cuota_not'];
        }
    }
    $dts = NULL;
}

?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_cobranza" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">INFORMACION DE NOTA</h4>
            </div>
            <form id="for_cob" method="post">
                <input name="action_cobranza" id="action_cobranza" type="hidden" value="<?php echo $_POST['cuo_tip'] ?>">
                <input name="hdd_cuo_id" id="hdd_cuo_id" type="hidden" value="<?php echo $_POST['cuo_id'] ?>">
                <input type="hidden" name="hdd_cliente_nom" value="<?php echo $_POST['cli_nom'] ?>">
                <input type="hidden" name="hdd_vista" id="hdd_vista" value="<?php echo $vista ?>">
                <input type="hidden" name="hdd_vistas" id="hdd_vistas" value="<?php echo $vistas ?>">
                <input type="hidden" name="hdd_notif_id" id="hdd_notif_id" value="<?php echo intval($_POST["notif_id"]); ?>">
                <input type="hidden" name="hdd_usu_nom" id="hdd_usu_nom" value="<?php echo $array_usu[0]; ?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Notas:</label>
                                <textarea id="txt_cuo_not" name="txt_cuo_not" class="form-control input-sm mayus" placeholder="Registre una nota o un aucerdo con el que llegó con el cliente, si tiene una fecha dada por el cliente, regítrela sino déjela en blanco"></textarea>
                            </div>
                        </div>
                    </div>
                    <p>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="che_notificacion" name="che_notificacion" class="form-control flat-green" style="position: relative;" value="1"><b>&nbsp; Activar notificación compromiso de pago</b>
                                </label>
                            </div>
                        </div>
                    </div>

                    <p>
                    <div class="row" id="row_notificacion" style="display:none;">
                        <div class="col-md-1" style="width:4%"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_cuo_fecref" class="control-label">Fecha de notificación: </label>
                                <div class="input-group date" id="menorgarantia_picker1">
                                    <input type="text" name="txt_cuo_fecref" id="txt_cuo_fecref" class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" placeholder="DD-MM-YYYY">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold;font-family: cambria">HISTORIAL</label>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php require_once("cobranza_notas.php"); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="submit" class="btn btn-info" id="btn_guardar_nota_cobranza">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cobranza/cobranza_form.js?ver==20230321'; ?>"></script>