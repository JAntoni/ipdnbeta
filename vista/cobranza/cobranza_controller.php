<?php
require_once('../../core/usuario_sesion.php');

require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cobranza/TareaCobranza.class.php");
$oTarea = new TareaCobranza();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle();
require_once("../lineatiempo/LineaTiempo.class.php");
$oLineaTiempo = new LineaTiempo();
require_once ("../cliente/Cliente.class.php");
$oCliente = new Cliente();
require_once ('../cuotapago/Cuotapago.class.php');
$oCuotapago = new Cuotapago();
require_once ('../cobranza/Cobranza.class.php');
$oCobranza = new Cobranza();
require_once ('../claverapida/Claverapida.class.php');
$oClaverapida = new Claverapida();
require_once ('../creditomenor/Creditomenor.class.php');
$oCreditomenor = new Creditomenor();
require_once('../creditolinea/Creditolinea.class.php');
$oCreditolinea = new Creditolinea();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

require_once ("../funciones/funciones.php");
require_once ("../funciones/fechas.php");
$usuario_id = intval($_SESSION['usuario_id']);

$dts["ven_msj"] = "ERROR EN LA ACCION";//echo json_encode($dts);exit();

if($_POST['action_cobranza']=="1")
{
	$dts = $oCuota->mostrarUno($_POST['hdd_cuo_id']);
	if ($dts['estado'] == 1) {
		$cuota_fec = mostrar_fecha($dts['data']['tb_cuota_fec']);
		$credito_tipo = $dts['data']['tb_creditotipo_id'];
		$credito_id = $dts['data']['tb_credito_id'];
	}
	$dts = NULL;

	$dts = $oCuota->informacion_credito_cuota($credito_tipo, $credito_id);
	if ($dts['estado'] == 1) {
		$cliente_id = $dts['data']['tb_cliente_id'];
		$credito_subtip = $dts['data']['tb_credito_tip'];
		if ($credito_tipo == 2)
			$credito_subtip = $dts['data']['tb_credito_tip1'];
	}
	$dts = NULL;

	$array_usu = explode(' ', $_SESSION['usuario_nom']);
	$nota = '';
	$hora = date('d-m-Y h:i a');

	if ($_SESSION['usuariogrupo_id'] == 2)
		$nota = '<b>' . $array_usu[0] . ': ' . $_POST['txt_cuo_not'] . ' | ' . $hora . '</b><br>';
	else
		$nota = '<b style="color: green;">' . $array_usu[0] . ': ' . $_POST['txt_cuo_not'] . ' | ' . $hora . '</b><br>';

  $data['nota'] = $nota;
  
	$oCuota->agregar_nota_cuota($_POST['hdd_cuo_id'],$nota);
  //ver el tema de que si se agrega una nota con fecha, obligatorio esa cuota debe estar registrado en tarea
  $oTarea->registrar_nota_fecha($nota,fecha_mysql($_POST['txt_cuo_fecref']),$_POST['hdd_cuo_id']);

	$dts = $oCuota->cliente_por_cuotaid($_POST['hdd_cuo_id']);
	if ($dts['estado'] == 1) {
		$credito = $dts['data']['credito'];
		$cli_nom = $dts['data']['tb_cliente_nom'];
	}
	$dts = NULL;

  //el número 1 al final para que se muestre en mi GC, HISTORIAL DEL CLIENTE
  $nota_det = 'Nota GC para: <b>'.$cli_nom.':</b> '.$nota.' | Nota de la cuota de fecha: <b>'.$cuota_fec.'</b>, del Crédito: <b>'.$credito.'</b>';
  $oCliente->insertar_nota_cliente($cliente_id, $_SESSION['usuario_id'], $credito_tipo, $credito_subtip, $credito_id, $nota_det, 1);
  //-------------------------------------

  /* $line_det = '<b>'.$_SESSION['usuario_nom'].'</b> Ha registrado una nota en Gestión de Cobranza: '.$_POST['txt_cuo_not'].'. Del cliente: '.$cli_nom.', del crédito: '.$credito.' && <b>'.date('d-m-Y h:i a').'</b>';
  $oLineaTiempo->tb_usuario_id=$_SESSION['usuario_id'];
  $oLineaTiempo->tb_lineatiempo_det=$line_det;
  $oLineaTiempo->insertar(); */

	//DANIEL ODAR INSERTAR NOTIFICACIONES CUANDO EL CHECKBOX ESTÁ ACTIVO
	$mensaje_extra = "";
	$activar_notificacion = intval($_POST["che_notificacion"]);
	if($activar_notificacion == 1 && !empty($_POST['txt_cuo_fecref'])){
		$nota = 'Cliente: <b>' . $cli_nom . '</b> ' . $_POST['txt_cuo_not'];
		$resp = $oTarea->insertar_tarea_notificacion_cobranza($_SESSION['usuario_id'], $nota, $_POST['hdd_cuo_id'], fecha_mysql($_POST['txt_cuo_fecref']));
		if($resp == 1){
			$mensaje_extra = "y la notificacion ";
		}

		/* $line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Ha creado una Notificación en Gestión de Cobranza, la nota fue: ' . $_POST['txt_cuo_not'] . ' && <b>' . date('d-m-Y h:i a') . '</b>';
		$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
		$oLineaTiempo->tb_lineatiempo_det = $line_det;
		$oLineaTiempo->insertar(); */
	}

	$data['ven_msj']="Se registró la nota {$mensaje_extra}correctamente.";
	$data['fecha_hora'] = $hora;
	
	echo json_encode($data);
}
if($_POST['action_cobranza']=="2")
{

	$oCuotadetalle->modificar_campo($_POST['hdd_cuo_id'],'tb_cuotadetalle_not',$_POST['txt_cuo_not'],'STR');
	$data['ven_msj']='Se registró la nota correctamente.';
	
	echo json_encode($data);
}

if($_POST['action_cobranza']=="compromiso_pago")
{
	$cuota_id = $_POST['hdd_cuota_id'];
	$credito_id = $_POST['hdd_credito_id'];
	$vista = $_POST['hdd_vista'];
	$fecha = fecha_mysql($_POST['txt_fec_comp']);
	$comentario = $_POST['txt_com_comp'];
	$estado = '';

	$cobranza = $oCobranza->insertar_compromiso_pago($cuota_id, $credito_id);
	
	if($cobranza['data']!=''){
		// Gerson (06-03-23) // detectar si tiene el estado 3
		$estados = [];
		$flag = 0; // estado de bandera que aun no tiene un compromiso de pago
		$estados = explode(",", $cobranza['data'][0]['tb_cobranza_estado']);
		for ($i=0; $i < count($estados); $i++) { 
			if($estados[$i] == 3){
				$flag = 1; // ya tiene un compromiso de pago
				break;
			}
		}
		if($flag>0){
			$estado = $cobranza['data'][0]['tb_cobranza_estado'];
		}else{
			$estado = $cobranza['data'][0]['tb_cobranza_estado'].',3';
		}
		//

		$oCobranza->modificar_compromiso_cobranza($cobranza['data'][0]['tb_cobranza_id'], $fecha, $comentario, $estado);
		// Guardar nota compromiso
		$array_usu = explode(' ', $_SESSION['usuario_nom']);
		$nota = '';
		$hora = date('d-m-Y h:i a');
		if ($_SESSION['usuariogrupo_id'] == 2){
			$nota = '<b>' . $array_usu[0] . ': (Compromiso de Pago) ' .$comentario. ' | ' . $hora . '</b><br>';
		}else{
			$nota = '<b style="color: green;">' . $array_usu[0] . ': ' .$comentario. ' | ' . $hora . '</b><br>';
		}
		$oCuota->agregar_nota_cuota($cuota_id,$nota);
		// crear notificacion
		$nota = 'Cliente: <b>' . $cobranza['data'][0]['tb_cliente_nom'] . '</b> Compromiso de pago: pdp para la fecha '. $_POST['txt_fec_comp'];
		$resp = $oTarea->insertar_tarea_notificacion_cobranza($_SESSION['usuario_id'], $nota, $cuota_id, date('Y-m-d'));
		$line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Ha creado una Notificación en Gestión de Cobranza, la nota fue: ' .$nota. ' && <b>' . date('d-m-Y h:i a') . '</b>';
		$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
		$oLineaTiempo->tb_lineatiempo_det = $line_det;
		$oLineaTiempo->insertar();
		//
		$data['ven_msj']='Se registró el compromiso de pago correctamente.';
	}else{
		$data['ven_msj']='No se pudo registrar el compromiso de pago correctamente.';
	}
	
	echo json_encode($data);
}

if($_POST['action_cobranza']=="prorroga")
{
	$cuota_id = $_POST['hdd_cuota_id'];
	$credito_id = $_POST['hdd_credito_id'];
	$fecha = fecha_mysql($_POST['txt_fec_prorr']);

	$cobranza = $oCobranza->insertar_compromiso_pago($cuota_id, $credito_id);

	if($cobranza['data']!=''){
		$oCobranza->editar_dato_simple($cobranza['data'][0]['tb_cobranza_id'], 'tb_cobranza_feccuota', $fecha);
		// Guardar nota prorroga
		$array_usu = explode(' ', $_SESSION['usuario_nom']);
		$nota = '';
		$hora = date('d-m-Y h:i a');
		if ($_SESSION['usuariogrupo_id'] == 2){
			$nota = '<b>' . $array_usu[0] . ': (Prórroga de Pago) Prórroga para realizar pago | ' . $hora . '</b><br>';
		}else{
			$nota = '<b style="color: green;">' . $array_usu[0] . ': Prórroga para realizar pago | ' . $hora . '</b><br>';
		}
		$oCuota->agregar_nota_cuota($cuota_id,$nota);
		// crear notificacion
		$nota = 'Cliente: <b>' . $cobranza['data'][0]['tb_cliente_nom'] . '</b> Prórroga de pago: pdp para la fecha '. $_POST['txt_fec_prorr'];
		$resp = $oTarea->insertar_tarea_notificacion_cobranza($_SESSION['usuario_id'], $nota, $cuota_id, date('Y-m-d'));
		$line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Ha creado una Notificación en Gestión de Cobranza, la nota fue: ' .$nota. ' && <b>' . date('d-m-Y h:i a') . '</b>';
		$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
		$oLineaTiempo->tb_lineatiempo_det = $line_det;
		$oLineaTiempo->insertar();
		//
		$data['estado']='1';
		$data['ven_msj']='Se modificó la fecha de vencimiento correctamente.';
	}else{
		$data['estado']='0';
		$data['ven_msj']='No se pudo modificar la fecha de vencimiento correctamente.';
	}
	
	echo json_encode($data);
}

if($_POST['action']=="eliminar")
{
	if(!empty($_POST['cuopag_id']))
	{
		$oCuotapago->modificar_campo($_POST['cuopag_id'],'tb_cuotapago_xac','0','INT');
		echo 'Se envió a la papelera correctamente.';
	}
	else
	{
		echo 'Intentelo nuevamente.';
	}
}
/* if($_POST['action']=="notificacion"){
	$noti = '';
	$dts = $oTarea->listar_notificaciones_cobranza($_SESSION['usuario_id']);
		if($dts['estado']==1){
			$noti .='
				<fieldset class="fiel_notificacion">
				  <legend>Mensajes de Vencimiento de Cuotas Libres</legend>
				  <table width="100%">';
			foreach ($dts['estado']as $key=>$dt) {
				$noti .= '
                            <tr class="tr_tareas">
                                <td align="justify" width="90%">'.$dt['tb_cobranzanoti_not'].' | Nota programada para el: <b>'.mostrar_fecha($dt['tb_cobranzanoti_fec']).'</b></td>
                                <td width="10%" align="right">
                                  <a href="#vis" class="btn_listo" id="btn_listo'.$dt['tb_cobranzanoti_id'].'" onclick="visto_cobranza_noti('.$dt['tb_cobranzanoti_id'].')">OK</a>
                                </td>
                            </tr>';
			}
			$noti .='
					</table>
				</fieldset>';
			echo $noti;
		}
		else
			echo '';
} */
if($_POST['action'] == "notificacion_vista"){
	$noti_id = $_POST['noti_id'];
	//$oTarea->modificar_estado_notificacion_cobranza($noti_id, 1); // parametro 1 es visto
	$dts = $oTarea->mostrar_una_notificacion($noti_id);
            if($dts['estado']==1){
		$cuo_id = $dts['data']['tb_cuota_id'];
            }

	$oTarea->modificar_estado_notificacion_cobranza_cuota_id($cuo_id, 1);
	echo 1;
}

if($_POST["action"] == 'notificacion_ok'){
	$data["mensaje"] = "Hubo un error al actualizar la notificacion";

	$resultado = $oTarea->modificar_estado_notificacion_noti_id(1, $usuario_id, intval($_POST['noti_id']));
	$data["estado"] = $resultado;
	if($resultado == 1){
		$data["mensaje"] = "Ok";
	}
	echo json_encode($data);
}



if($_POST['action'] == 'buscar_cartera'){
	$fase_id = intval($_POST['fase_id']);
	$fecha = fecha_mysql($_POST['fecha']);
	
	$fase = $oCobranza->obtener_fase($fase_id);

	$usuario_id = intval($_SESSION['usuario_id']);
	$usuario_id_permiso = intval($fase['data'][0]['tb_usuario_id']);


	if($usuario_id == $usuario_id_permiso){
		$existe_cartera = $oCobranza->cartera_cobranza_hoy($fecha, $fase_id);

		if($existe_cartera['data']>0){ // existe cartera
			$data['estado'] = 1;
			$data['mensaje'] = 'Ya existe cartera generada el día de hoy.';
			//var_dump($existe_cartera['data']);

		}else{ // no existe cartera
			$data['estado'] = 2;
			$data['mensaje'] = 'No existe cartera generada el día de hoy.';
			//var_dump('no ai');

		}
	}else{
		$data['estado'] = 0;
		$data['mensaje'] = 'El usuario no tiene permisos para esta acción.';
	}

	echo json_encode($data);

}

if($_POST['action'] == 'detectar_responsable_cartera'){
	$fase_id = intval($_POST['fase_id']);
	$fecha = fecha_mysql($_POST['fecha']);
	
	$fase = $oCobranza->obtener_fase($fase_id);

	$usuario_id = intval($_SESSION['usuario_id']);
	// Gerson (01-03-23)
	$usuarios = [];
	$usuario_id_permiso = 0;
	$flag = 0;
	$usuarios = explode(",", $fase['data'][0]['tb_usuario_id']);
	for ($i=0; $i < count($usuarios); $i++) { 
		if($usuarios[$i] == $usuario_id){
			$flag = 1;
			break;
		}
	}
	//
	//$usuario_id_permiso = intval($fase['data'][0]['tb_usuario_id']);

	//if($usuario_id == $usuario_id_permiso){
	if($flag == 1){
		$existe_cartera = $oCobranza->cartera_cobranza_hoy($fecha, $fase_id);

		if($existe_cartera['data']>0){ // existe cartera
			$data['estado'] = 1;
			$data['mensaje'] = 'Ya existe cartera generada el día de hoy.';
		}else{ // no existe cartera
			$data['estado'] = 2;
			$data['mensaje'] = 'No existe cartera generada el día de hoy.';
		}
	}else{
		$data['estado'] = 0;
		$data['mensaje'] = 'El usuario no tiene permisos para esta acción. Fase ID: '.$fase_id;
	}

	echo json_encode($data);
}

if($_POST['action'] == 'generar_cartera_hoy'){
	$fase_id = intval($_POST['fase_id']);
	if($_POST['fecha']!='' || $_POST['fecha']!=null){
		$fecha = fecha_mysql($_POST['fecha']);
	}else{
		$fecha = date('Y-m-d');
	}
	
	$fase = $oCobranza->obtener_fase($fase_id); // fase seleccionada

	$lista_cobranza = $oCobranza->lista_cobranza_hoy($fecha, intval($fase['data'][0]['tb_config_valor']), intval($fase['data'][0]['tb_config_valor2'])); // lista de garantias a cobrar al dia de hoy 
	
	$existe_cartera = $oCobranza->existeCobranzaCartera($fecha, $fase_id);

	if(!empty($lista_cobranza['data']) || $lista_cobranza['data'] != ""){
		
		foreach ($lista_cobranza['data'] as $key => $value) {

			//$existe = $oCobranza->existeCobranza($value['tb_credito_id'], $value['tb_cuota_id'], $fase_id);
			$existe = $oCobranza->existeCobranza($value['tb_credito_id'], $value['tb_cuota_id'], $value['tb_cuota_fec'], $fase_id);
			if($existe['estado']==0){
				$oCobranza->registrarCobranzaCartera($value['tb_cuota_fec'], $fecha, $value['tb_cuota_cuo'], null, null, $fase_id, $value['tb_cliente_id'], $value['tb_credito_id'], $value['tb_cuota_id'], $_SESSION['usuario_id'], 1);
			}
			
		}

		if($existe_cartera['estado']==0){ // no existe cartera creada el dia de hoy 
			$oCobranza->registrarCartera($fecha, 'CARTERA CON DATOS', $fase_id, $_SESSION['usuario_id'], 1);

			$data['estado'] = 1;
			$data['mensaje'] = 'La cartera del día de hoy a sido actualizada.';

		}else{
			$data['estado'] = 2;
			$data['mensaje'] = 'Ya existe cartera creada.';
		}

	}else{

		if($existe_cartera['estado']==0){ // no existe cartera creada el dia de hoy 
			$oCobranza->registrarCartera($fecha, 'CARTERA SIN DATOS', $fase_id, $_SESSION['usuario_id'], 1);

			$data['estado'] = 0;
			$data['mensaje'] = 'No se encontraron datos nuevos para añadir a la cartera el día de hoy '.$_POST['fecha'].'.';

		}else{
			$data['estado'] = 2;
			$data['mensaje'] = 'Ya existe cartera creada.';
		}
		
	}

	echo json_encode($data);

}

if($_POST['action'] == 'pasar_remate'){
	$cuota_id = intval($_POST['cuota_id']);
	$credito_id = intval($_POST['credito_id']);

 	$data['estado'] = 0;
 	$data['mensaje'] = 'Existe un error al eliminar el Inventario.';
		 
	// pasado a remate
	$cobranza = $oCobranza->insertar_compromiso_pago($cuota_id,$credito_id); // obtener cobranza
	if($cobranza['data']!=''){
		if($oCobranza->modificar_remate_cobranza($cobranza['data'][0]['tb_cobranza_id'], date('Y-m-d h:i:s'), $usuario_id,4)){

			// Modificar estado credito
			$oCreditomenor->modificar_campo($credito_id, 'tb_credito_est', 5, 'INT');
			$oCreditomenor->modificar_campo($credito_id, 'tb_credito_fecrem', date('Y-m-d'),'STR'); //fecha en que pasa a remate
			$creditolinea_det = '<span style="color: red;">Este crédito ha pasado a remate por el área de cobranza. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
			$oCreditolinea->insertar(1, $credito_id, $usuario_id, $creditolinea_det); //tipo credito 1,  usuario detectado por sistema
	
			// Guardar nota prorroga
			$array_usu = explode(' ', $_SESSION['usuario_nom']);
			$nota = '';
			$hora = date('d-m-Y h:i a');
			if ($_SESSION['usuariogrupo_id'] == 2){
				$nota = '<b>' . $array_usu[0] . ': (Remate) Garantía pasada a remate | ' . $hora . '</b><br>';
			}else{
				$nota = '<b style="color: green;">' . $array_usu[0] . ': Garantía pasada a remate | ' . $hora . '</b><br>';
			}
			$oCuota->agregar_nota_cuota($cuota_id,$nota);
			// crear notificacion
			$nota = 'Cliente: <b>' . $cobranza['data'][0]['tb_cliente_nom'] . '</b> Garantía pasada a remate con fecha '.date('d-m-Y');
			$resp = $oTarea->insertar_tarea_notificacion_cobranza($_SESSION['usuario_id'], $nota, $cuota_id, date('Y-m-d'));
			$line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Ha creado una Notificación en Gestión de Cobranza, la nota fue: ' .$nota. ' && <b>' . date('d-m-Y h:i a') . '</b>';
			$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
			$oLineaTiempo->tb_lineatiempo_det = $line_det;
			$oLineaTiempo->insertar();
			//

			$data['estado'] = 1;
			$data['mensaje'] = 'Crédito pasado a Remate.';
		}
	}

 	echo json_encode($data);

}

/* GERSON (07-03-23) */
if($_POST['action'] == 'retornar_remate'){
	$cuota_id = intval($_POST['cuota_id']);
	$credito_id = intval($_POST['credito_id']);

	// retornar de remate
	$cobranza = $oCobranza->consultar_cobranza_por_credito($cuota_id,$credito_id); // obtener cobranza
	$data['estado'] = 0;
	$data['mensaje'] = 'No se encontró registro de cobranza con el crédito consultado.';
	if($cobranza['estado']==1){

		foreach ($cobranza['data'] as $key => $v) {
			
			if($oCobranza->modificar_remate_cobranza($v['tb_cobranza_id'], null, null,1)){ // Regresar credito pendiente

				// Modificar estado credito
				$oCreditomenor->modificar_campo($credito_id, 'tb_credito_est', 3, 'INT');
				$oCreditomenor->modificar_campo($credito_id, 'tb_credito_fecrem', '0000-00-00','STR'); //fecha en que pasa a remate
				$creditolinea_det = '<span style="color: red;">Este crédito ha sido retornado a cobranza desde remate. | <b>'.date('d-m-Y h:i a').'</b></span><br>';
				$oCreditolinea->insertar(1, $credito_id, $usuario_id, $creditolinea_det); //tipo credito 1, usuario detectado por sistema
		
				// Guardar nota prorroga
				$array_usu = explode(' ', $_SESSION['usuario_nom']);
				$nota = '';
				$hora = date('d-m-Y h:i a');
				if ($_SESSION['usuariogrupo_id'] == 2){
					$nota = '<b>' . $array_usu[0] . ': (Retorno) Garantía retornada a cobranza | ' . $hora . '</b><br>';
				}else{
					$nota = '<b style="color: green;">' . $array_usu[0] . ': Garantía retornada a cobranza | ' . $hora . '</b><br>';
				}
				$oCuota->agregar_nota_cuota($v['tb_cuota_id'],$nota);
				// crear notificacion
				$nota = 'Cliente: <b>' . $v['tb_cliente_nom'] . '</b> Garantía retornada a cobranza con fecha '.date('d-m-Y');
				$resp = $oTarea->insertar_tarea_notificacion_cobranza($_SESSION['usuario_id'], $nota, $v['tb_cuota_id'], date('Y-m-d'));
				$line_det = '<b>' . $_SESSION['usuario_nom'] . '</b> Ha creado una Notificación en Disponible para Remate, la nota fue: ' .$nota. ' && <b>' . date('d-m-Y h:i a') . '</b>';
				$oLineaTiempo->tb_usuario_id = $_SESSION['usuario_id'];
				$oLineaTiempo->tb_lineatiempo_det = $line_det;
				$oLineaTiempo->insertar();
				//
	
				$data['estado'] = 1;
				$data['mensaje'] = 'Crédito retornado a Cobranza.';
			}

		}
		
	}

 	echo json_encode($data);

}
/*  */

/* GERSON (25-02-23) */
if($_POST['action'] == 'clave_rapida'){
	$clave_input = $_POST['txt_clave_rapida'];
	$vista = intval($_POST['hdd_vista']);
	$cobranza_id = intval($_POST['cobranza_id']);
	$cuota_id = intval($_POST['cuota_id']);
	$credito_id = intval($_POST['credito_id']);
	
	//obtener clave de perfil
	$usuario = $oUsuario->mostrarUno($usuario_id);
	$cod = intval($usuario['data']['tb_usuarioperfil_id'])+3;
	//
	$clave = $oClaverapida->mostrarClaveActiva($cod,$usuario['data']['tb_usuarioperfil_id']);

 	$data['estado'] = 0;
 	$data['mensaje'] = 'Clave incorrecta.';
	$data['vista'] = '';
	$data['value1'] = '';
	$data['value2'] = '';
	$data['value3'] = '';
	$data['value4'] = '';

	if($clave['data']!=''){
		if($clave['data']['tb_config_valor'] == $clave_input){
			$data['estado'] = 1;
			$data['mensaje'] = 'Clave correcta.';
			$data['vista'] = 'cobranza';
			$data['value1'] = $cobranza_id;
			$data['value2'] = $cuota_id;
			$data['value3'] = $credito_id;
			$data['value4'] = $vista;
		}
	}

 	echo json_encode($data);

}
/*  */
?>