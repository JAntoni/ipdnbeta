<?php
 if(defined('VISTA_URL')){
        require_once(APP_URL.'core/usuario_sesion.php');
        require_once (VISTA_URL.'cuotapago/Cuotapago.class.php');
        require_once (VISTA_URL.'cuota/Cuota.class.php');
        require_once (VISTA_URL.'cuotatipo/Cuotatipo.class.php');
        require_once (VISTA_URL.'cliente/Cliente.class.php');
        require_once (VISTA_URL.'usuario/Usuario.class.php');
        require_once (VISTA_URL."creditomenor/Creditomenor.class.php");
        require_once (VISTA_URL.'funciones/funciones.php');
        require_once (VISTA_URL.'funciones/fechas.php');
  }
  else{
        require_once('../../core/usuario_sesion.php');
        require_once ("../cuotapago/Cuotapago.class.php");
        require_once ("../cuota/Cuota.class.php");
        require_once ("../cuotatipo/Cuotatipo.class.php");
        require_once ("../cliente/Cliente.class.php");
        require_once ("../usuario/Usuario.class.php");
        require_once ("../creditomenor/Creditomenor.class.php");
        require_once ("../funciones/funciones.php");
        require_once ("../funciones/fechas.php");
  }
require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
$oCuotapago = new Cuotapago();
$oCuota = new Cuota();
$oCuotatipo = new Cuotatipo();
$oCliente = new Cliente();
$oUsuario = new Usuario();
$oCreditomenor = new Creditomenor();

//$fecha_hoy = date('d-m-Y');
$fecha_hoy = date('Y-m-d');

// clave rapido
$clave = $oCobranza->getConfig('1');
$valorClave='';
if($clave['data']!='' || $clave['data']!=null){
  $valorClave=$clave['data']['tb_config_valor'];
}

//<!-- CREDITOS MENORES-->

  $cliente_id = intval($_POST['hdd_cli_id_gestion']);
  $cliente = $_POST['txt_cli_gestion'];
  if($cliente==""){ // si el nombre del cliente es vacio se pasa a quitar el id
    $cliente_id = 0;
  }
  //$condicion = intval($_POST['cmb_estado_cobranza']);
  $cond = $_POST['cmb_fil_cuo_cond'];
  $condicion = $_POST['cmb_estado_cobranza']; //grupo
  $grupo = $_POST['cmb_grupo']; // credito
  $fase_id = $_POST['cmb_fase_cobranza'];
  $dias_vencido = 0;
  if($_POST['cmb_dia_vencido']=='all'){
    $dias_vencido = -1;
  }else{
    $dias_vencido = intval($_POST['cmb_dia_vencido']);
  }
  
  $fase = $oCobranza->obtener_fase($fase_id); // fase seleccionada

  // GERSON (11-03-23)
  $ver_vencidos = "";
  $ver_por_vencer = "";
  if($cond!="-"){
    if($cond=="PV"){
      $ver_vencidos = "hidden";
    }else{
      $ver_por_vencer = "hidden";
    }
  }

  if($grupo == "-" || $grupo == "CM"){
    $dts=$oCobranza->filtrar_gestion_menor(intval($cliente_id), $condicion, intval($fase['data'][0]['tb_config_valor']), intval($fase['data'][0]['tb_config_valor2']), $dias_vencido, $cond); ?>

    <?php if($dts['estado']==1){  ?>
      <?php
      //CONTADORES GENERALES
        $NUMERO_ORDEN = 0;
        $ARRAY_CLIENTES = array();
        $TOTAL_SOLES = 0;
        $TOTAL_DOLARES = 0;
        $NUMERO_CUOTAS = 0;
        $subtotal_soles = 0;
        $subtotal_dolares = 0;  
      ?>
      <!-- TABLA DE 2 DIAS HASTA RECIEN VENCIDO -->
      <div class="box box-success collapsed-box <?php echo $ver_por_vencer; ?>">
        <div class="box-header with-border" style="background-color: #00E37B;">
            <h3 class="box-title"><strong>Créditos por vencer (Desde 2 días por vencer hasta vencidos el día de hoy)</strong></h3>
            <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="box-body no-padding">
          <div id="div_tabla_vencido_0" class="table-responsive dataTables_wrapper form-inline dt-bootstrap" style="padding: 10px;">
            <table id="tabla_cobranza" class="table ">
              <thead>
                <tr id="tabla_cabecera">
                    <th id="tabla_cabecera_fila">N</th>
                    <th id="tabla_cabecera_fila">CLIENTE</th>
                    <th id="tabla_cabecera_fila">TIPO</th>
                    <th id="tabla_cabecera_fila" align="center">MONTO</th>
                    <th id="tabla_cabecera_fila" align="center">VENCIMIENTO</th>
                    <th id="tabla_cabecera_fila" align="center">CUOTAS</th>
                    <th id="tabla_cabecera_fila">ASESOR</th>
                    <th id="tabla_cabecera_fila">NOTAS</th>
                    <th id="tabla_cabecera_fila">ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                <?php  foreach($dts['data']as $key=>$dt0){ 
                  if($dt0['dias_diferencia']<=0){

                  //cuota
                  $cuota0 = $oCuota->mostrarUno($dt0['tb_cuota_id']);

                  //cuotatipo
                  $cuotatipo0 = $oCuotatipo->mostrarUno($dt0['tb_cuotatipo_id']);

                  //usario
                  $usuario = $oUsuario->mostrarUno($dt0['tb_credito_usureg']);

                  //contadores generales
                  $NUMERO_ORDEN ++;
                  array_push($ARRAY_CLIENTES, $dt0['tb_cliente_id']);
                  $NUMERO_CUOTAS ++;

                  if($dt0['tb_moneda_id']==1)$moneda="S/. ";
                  if($dt0['tb_moneda_id']==2)$moneda="US$ ";

                  $vencida = 1;
                  if(strtotime($fecha_hoy) <= strtotime($dt0['tb_cobranza_fec_compromiso'])){$vencida = 0;}; ?>

                  <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $NUMERO_ORDEN;?></td>
                    <td id="tabla_fila">
                      <?php
                        echo '<span id="span_cli'.$dt0['tb_cuota_id'].'">'.$dt0['tb_cliente_nom']."</span> | ".$dt0['tb_cliente_doc'];
                        echo '<br><span style="font-weight: normal;">'.$dt0['tb_cliente_cel'].'('.$dt0['tb_cliente_procel'].')'.' | '.$dt0['tb_cliente_tel'].'('.$dt0['tb_cliente_protel'].')'.' | '.$dt0['tb_cliente_telref'].'('.$dt0['tb_cliente_protelref'].')</span>';
                      ?>
                    </td>
                    <td id="tabla_fila"><?php echo $cuotatipo0['data']['tb_cuotatipo_nom']?></td>
                    <?php
                      $pagos_cuota = 0;
                      $mod_id=1;
                      $dts3_0=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$dt['tb_cuota_id']);
                      if($dts3_0['estado']==1)
                      {
                          foreach($dts3_0['data']as $key=>$dt3_0)
                        {
                          $pagos_cuota += moneda_mysql($dt3_0['tb_ingreso_imp']);
                        }
                      }
                      if($cuotatipo0['data']['tb_cuotatipo_id'] == 1)
                        $saldo = moneda_mysql($cuota0['data']['tb_cuota_int']-$pagos_cuota);
                      else
                        $saldo = moneda_mysql($cuota0['data']['tb_cuota_cuo']-$pagos_cuota);

                      if($dt0['tb_moneda_id']==1){
                        if($cuotatipo0['data']['tb_cuotatipo_id'] == 1)
                          $TOTAL_SOLES += moneda_mysql($cuota0['data']['tb_cuota_int']-$pagos_cuota);
                        else
                          $TOTAL_SOLES += moneda_mysql($cuota0['data']['tb_cuota_cuo']-$pagos_cuota);
                        $subtotal_soles += $saldo;
                      }
                      if($dt0['tb_moneda_id']==2){
                        if($cuotatipo0['data']['tb_cuotatipo_id'] == 1)
                          $TOTAL_DOLARES += moneda_mysql($cuota0['data']['tb_cuota_int']-$pagos_cuota);
                        else
                          $TOTAL_DOLARES += moneda_mysql($cuota0['data']['tb_cuota_cuo']-$pagos_cuota);
                        $subtotal_dolares += $saldo;
                      }

                      $string = str_replace("-", "", restaFechas($fecha_hoy,mostrar_fecha($dt0['tb_cobranza_fec_compromiso'])));
                      $string = trim($string);
                    ?>
                    <td  id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $moneda . mostrar_moneda($saldo);?></td>
                    <td  id="tabla_fila"align="center" <?php if($dt0['tb_cobranza_estado']==2){ echo 'style="color:white;font-weight:bold; background-color:green;"'; }else{ if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}}?>>
                      <?php
                      if($dt0['tb_cobranza_estado']==2){
                        echo "PAGADO";
                      }else{
                        if($vencida == 1){
                          echo mostrar_fecha($dt0['tb_cobranza_fec_compromiso']) . " (Vencida hace " . str_replace("-", "", $string) . " días)";
                          echo '<button type="button" class="btn btn-danger btn-sm" onclick="recordatorio_cobranza('.$dt0['tb_credito_id'].', '.$dt0['tb_cuota_id'].')">Recordatorio</button>';
                        }
                        else{ 
                          echo mostrar_fecha($dt0['tb_cobranza_fec_compromiso']) . " (Vence en " . $string . " días)";
                          if(intval($string) <= 5)
                            echo '<button type="button" class="btn btn-info btn-sm" onclick="recordatorio_cobranza('.$dt0['tb_credito_id'].', '.$dt0['tb_cuota_id'].')">Recordatorio</button>';
                        }
                      }
                      ?>
                    </td>
                    <td  id="tabla_fila" align="center">
                      <?php
                        if($cuotatipo0['data']['tb_cuotatipo_id']==2){
                          echo $cuota0['data']['tb_cuota_num'].'/'. $dt0['tb_credito_numcuo'];
                        }
                        else{
                          echo $cuota0['data']['tb_cuota_num'].'/'. $dt0['tb_credito_numcuomax'];
                        }
                      ?>
                    </td>
                    <td id="tabla_fila"><?php echo $usuario['data']['tb_usuario_nom'] . ' ' . $usuario['data']['tb_usuario_ape'];?></td>
                    <td id="tabla_fila">
                      <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="cobranza_form('<?php echo $dt0['tb_cuota_id']?>','1')">+</a><span id="cuota_id_<?php echo $dt0['tb_cuota_id'];?>"> <?php echo $cuota0['data']['tb_cuota_not'] ?></span>
                    </td>
                    <td id="tabla_fila">
                      <a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="creditomenor_form('L','<?php echo $dt0['tb_credito_id']?>')">Ver</a>
                      <div class="btn-group">
                        <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="#" onclick="estadoCuentaCobranza(<?php echo $dt0['tb_credito_id']; ?>)"><i class="fa fa-fw fa-file-pdf-o"></i> Estado de Cuenta</a></li>
                          <?php //if($fase_id==2){ ?>
                            <li><a href="#" onclick="compromisoPagoCobranza(<?php echo $dt0['tb_cuota_id']; ?>, <?php echo $dt0['tb_credito_id']; ?>,'1')"><i class="fa fa-calendar"></i> Generar Compromiso</a></li>
                          <?php //} ?>
                          <li><a href="#" onclick="upload_cobranza_form('I',<?php echo $dt0['tb_cobranza_id']; ?>)"><i class="fa fa-upload"></i> Subir Imagen</a></li>
                          <?php //if($fase_id==3){ ?>
                            <li><a href="#" onclick="prorrogaCuotaCobranza(<?php echo $dt0['tb_cobranza_id']; ?>, <?php echo $dt0['tb_cuota_id']; ?>, <?php echo $dt0['tb_credito_id']; ?>,'1')"><i class="fa fa-calendar-plus-o"></i> Dar Prórroga</a></li>
                            <li><a href="#" onclick="remateGarantia1Cobranza(<?php echo $dt0['tb_cuota_id']; ?>, <?php echo $dt0['tb_credito_id']; ?>,'1')"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li>
                          <?php //} ?>
                          <!-- <li><a href="#" onclick="(<?php echo $dt0['tb_credito_id']; ?>)"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li> -->
                        </ul>
                      </div>
                    </td>
                    <!-- <td id="tabla_fila">CM-<?php echo str_pad($dt0['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></td> -->
                  </tr>                    

                <?php } } ?>
              </tbody>
              <tfoot>
                <tr class="even">
                  <td  id="tabla_fila"colspan="3"><strong>Sub Total</strong></td>
                  <td id="tabla_fila"><strong><?php echo 'S/. '. mostrar_moneda($subtotal_soles); ?></strong></td>
                  <td  id="tabla_fila"colspan="7"></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>

      <?php $i=0; $opacidad=0.20; $sum_opa=0.00;?>
      <?php while($i<15){ ?>
        <?php
          //CONTADORES GENERALES
          $NUMERO_ORDEN = 0;
          $ARRAY_CLIENTES = array();
          $TOTAL_SOLES = 0;
          $TOTAL_DOLARES = 0;
          $NUMERO_CUOTAS = 0;
          $subtotal_soles = 0;
          $subtotal_dolares = 0;  
        ?>
        <!-- TABLA DE VENCIDOS DEL 1 AL 15 DIA -->
        <div class="box box-danger collapsed-box <?php echo $ver_vencidos; ?>">
          <div class="box-header with-border" style="background-color: rgba(221,75,57, <?php echo $opacidad+$sum_opa; ?>);">
              <h3 class="box-title"><strong>Créditos vencidos <?php echo $i+1; ?> día</strong></h3>
              <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
              </div>
          </div>
          <div class="box-body no-padding">
            <div id="div_tabla_vencido_<?php echo $i+1; ?>" class="table-responsive dataTables_wrapper form-inline dt-bootstrap" style="padding: 10px;">
              <table id="tabla_cobranza" class="table ">
                <thead>
                  <tr id="tabla_cabecera">
                      <th id="tabla_cabecera_fila">N</th>
                      <th id="tabla_cabecera_fila">CLIENTE</th>
                      <th id="tabla_cabecera_fila">TIPO</th>
                      <th id="tabla_cabecera_fila" align="center">MONTO</th>
                      <th id="tabla_cabecera_fila" align="center">VENCIMIENTO</th>
                      <th id="tabla_cabecera_fila" align="center">CUOTAS</th>
                      <th id="tabla_cabecera_fila">ASESOR</th>
                      <th id="tabla_cabecera_fila">NOTAS</th>
                      <th id="tabla_cabecera_fila">ACCIONES</th>
                  </tr>
                </thead>
                <?php  foreach($dts['data']as $key=>${"dt".($i+1)}){ 
                  if(${"dt".($i+1)}['dias_diferencia']==($i+1)){ 
                    //cuota
                    ${"cuota".($i+1)} = $oCuota->mostrarUno(${"dt".($i+1)}['tb_cuota_id']);

                    //cuotatipo
                    ${"cuotatipo".($i+1)} = $oCuotatipo->mostrarUno(${"dt".($i+1)}['tb_cuotatipo_id']);

                    //usario
                    $usuario = $oUsuario->mostrarUno(${"dt".($i+1)}['tb_credito_usureg']);

                    //contadores generales
                    $NUMERO_ORDEN ++;
                    array_push($ARRAY_CLIENTES, ${"dt".($i+1)}['tb_cliente_id']);
                    $NUMERO_CUOTAS ++;

                    if(${"dt".($i+1)}['tb_moneda_id']==1)$moneda="S/. ";
                    if(${"dt".($i+1)}['tb_moneda_id']==2)$moneda="US$ ";

                    $vencida = 1;
                    if(strtotime($fecha_hoy) <= strtotime(${"dt".($i+1)}['tb_cobranza_fec_compromiso'])){$vencida = 0;}; ?>
                    <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $NUMERO_ORDEN;?></td>
                    <td id="tabla_fila">
                      <?php
                        echo '<span id="span_cli'.${"dt".($i+1)}['tb_cuota_id'].'">'.${"dt".($i+1)}['tb_cliente_nom']."</span> | ".${"dt".($i+1)}['tb_cliente_doc'];
                        echo '<br><span style="font-weight: normal;">'.${"dt".($i+1)}['tb_cliente_cel'].'('.${"dt".($i+1)}['tb_cliente_procel'].')'.' | '.${"dt".($i+1)}['tb_cliente_tel'].'('.${"dt".($i+1)}['tb_cliente_protel'].')'.' | '.${"dt".($i+1)}['tb_cliente_telref'].'('.${"dt".($i+1)}['tb_cliente_protelref'].')</span>';
                      ?>
                    </td>
                    <td id="tabla_fila"><?php echo ${"cuotatipo".($i+1)}['data']['tb_cuotatipo_nom']?></td>
                    <?php
                      $pagos_cuota = 0;
                      $mod_id=1;
                      ${"dts3_".($i+1)}=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$dt['tb_cuota_id']);
                      if(${"dts3_".($i+1)}['estado']==1)
                      {
                          foreach(${"dts3_".($i+1)}['data']as $key=>${"dt3_".($i+1)})
                        {
                          $pagos_cuota += moneda_mysql(${"dt3_".($i+1)}['tb_ingreso_imp']);
                        }
                      }
                      if(${"cuotatipo".($i+1)}['data']['tb_cuotatipo_id'] == 1)
                        $saldo = moneda_mysql(${"cuota".($i+1)}['data']['tb_cuota_int']-$pagos_cuota);
                      else
                        $saldo = moneda_mysql(${"cuota".($i+1)}['data']['tb_cuota_cuo']-$pagos_cuota);

                      if(${"dt".($i+1)}['tb_moneda_id']==1){
                        if(${"cuotatipo".($i+1)}['data']['tb_cuotatipo_id'] == 1)
                          $TOTAL_SOLES += moneda_mysql(${"cuota".($i+1)}['data']['tb_cuota_int']-$pagos_cuota);
                        else
                          $TOTAL_SOLES += moneda_mysql(${"cuota".($i+1)}['data']['tb_cuota_cuo']-$pagos_cuota);
                        $subtotal_soles += $saldo;
                      }
                      if(${"dt".($i+1)}['tb_moneda_id']==2){
                        if(${"cuotatipo".($i+1)}['data']['tb_cuotatipo_id'] == 1)
                          $TOTAL_DOLARES += moneda_mysql(${"cuota".($i+1)}['data']['tb_cuota_int']-$pagos_cuota);
                        else
                          $TOTAL_DOLARES += moneda_mysql(${"cuota".($i+1)}['data']['tb_cuota_cuo']-$pagos_cuota);
                        $subtotal_dolares += $saldo;
                      }

                      $string = str_replace("-", "", restaFechas($fecha_hoy,mostrar_fecha(${"dt".($i+1)}['tb_cobranza_fec_compromiso'])));
                      $string = trim($string);
                    ?>
                    <td  id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $moneda . mostrar_moneda($saldo);?></td>
                    <td  id="tabla_fila"align="center" <?php if(${"dt".($i+1)}['tb_cobranza_estado']==2){ echo 'style="color:white;font-weight:bold; background-color:green;"'; }else{ if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}}?>>
                      <?php
                      if(${"dt".($i+1)}['tb_cobranza_estado']==2){
                        echo "PAGADO";
                      }else{
                        if($vencida == 1){
                          echo mostrar_fecha(${"dt".($i+1)}['tb_cobranza_fec_compromiso']) . " (Vencida hace " . str_replace("-", "", $string) . " días)";
                          echo '<button type="button" class="btn btn-danger btn-sm" onclick="recordatorio_cobranza('.${"dt".($i+1)}['tb_credito_id'].', '.${"dt".($i+1)}['tb_cuota_id'].')">Recordatorio</button>';
                        }
                        else{ 
                          echo mostrar_fecha(${"dt".($i+1)}['tb_cobranza_fec_compromiso']) . " (Vence en " . $string . " días)";
                          if(intval($string) <= 5)
                            echo '<button type="button" class="btn btn-info btn-sm" onclick="recordatorio_cobranza('.${"dt".($i+1)}['tb_credito_id'].', '.${"dt".($i+1)}['tb_cuota_id'].')">Recordatorio</button>';
                        }
                      }
                      ?>
                    </td>
                    <td  id="tabla_fila" align="center">
                      <?php
                        if(${"cuotatipo".($i+1)}['data']['tb_cuotatipo_id']==2){
                          echo ${"cuota".($i+1)}['data']['tb_cuota_num'].'/'. ${"dt".($i+1)}['tb_credito_numcuo'];
                        }
                        else{
                          echo ${"cuota".($i+1)}['data']['tb_cuota_num'].'/'. ${"dt".($i+1)}['tb_credito_numcuomax'];
                        }
                      ?>
                    </td>
                    <td id="tabla_fila"><?php echo $usuario['data']['tb_usuario_nom'] . ' ' . $usuario['data']['tb_usuario_ape'];?></td>
                    <td id="tabla_fila">
                      <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="cobranza_form('<?php echo ${'dt'.($i+1)}['tb_cuota_id']?>','1')">+</a><span id="cuota_id_<?php echo ${'dt'.($i+1)}['tb_cuota_id'];?>"> <?php echo ${"cuota".($i+1)}['data']['tb_cuota_not'] ?></span>
                    </td>
                    <td id="tabla_fila">
                      <a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="creditomenor_form('L','<?php echo ${'dt'.($i+1)}['tb_credito_id']?>')">Ver</a>
                      <div class="btn-group">
                        <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="#" onclick="estadoCuentaCobranza(<?php echo ${'dt'.($i+1)}['tb_credito_id']; ?>)"><i class="fa fa-fw fa-file-pdf-o"></i> Estado de Cuenta</a></li>
                          <?php //if($fase_id==2){ ?>
                            <li><a href="#" onclick="compromisoPagoCobranza(<?php echo ${'dt'.($i+1)}['tb_cuota_id']; ?>, <?php echo ${'dt'.($i+1)}['tb_credito_id']; ?>,'1')"><i class="fa fa-calendar"></i> Generar Compromiso</a></li>
                          <?php //} ?>
                          <li><a href="#" onclick="upload_cobranza_form('I',<?php echo ${'dt'.($i+1)}['tb_cobranza_id']; ?>)"><i class="fa fa-upload"></i> Subir Imagen</a></li>
                          <?php //if($fase_id==3){ ?>
                            <li><a href="#" onclick="prorrogaCuotaCobranza(<?php echo ${'dt'.($i+1)}['tb_cobranza_id']; ?>, <?php echo ${'dt'.($i+1)}['tb_cuota_id']; ?>, <?php echo ${'dt'.($i+1)}['tb_credito_id']; ?>,'1')"><i class="fa fa-calendar-plus-o"></i> Dar Prórroga</a></li>
                            <li><a href="#" onclick="remateGarantia1Cobranza(<?php echo ${'dt'.($i+1)}['tb_cuota_id']; ?>, <?php echo ${'dt'.($i+1)}['tb_credito_id']; ?>,'1')"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li>
                          <?php //} ?>
                          <!-- <li><a href="#" onclick="remateGarantia2Cobranza(<?php echo ${'dt'.($i+1)}['tb_credito_id']; ?>)"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li> -->
                        </ul>
                      </div>
                    </td>
                    <!-- <td id="tabla_fila">CM-<?php echo str_pad(${"dt".($i+1)}['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></td> -->
                  </tr>
                <?php } } ?>
                </tbody>
                <tfoot>
                  <tr class="even">
                    <td  id="tabla_fila"colspan="3"><strong>Sub Total</strong></td>
                    <td id="tabla_fila"><strong><?php echo 'S/. '. mostrar_moneda($subtotal_soles); ?></strong></td>
                    <td  id="tabla_fila"colspan="7"></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      <?php $sum_opa=$sum_opa+0.05; $i++; } ?>


      <?php
      //CONTADORES GENERALES
        $NUMERO_ORDEN = 0;
        $ARRAY_CLIENTES = array();
        $TOTAL_SOLES = 0;
        $TOTAL_DOLARES = 0;
        $NUMERO_CUOTAS = 0;
        $subtotal_soles = 0;
        $subtotal_dolares = 0;  
      ?>
      <!-- TABLA DE VENCIDOS 16 DIA -->
      <div class="box box-danger collapsed-box <?php echo $ver_vencidos; ?>">
        <div class="box-header with-border" style="background-color: rgba(221,75,57, .95);">
            <h3 class="box-title"><strong>Créditos vencidos de 16 días a más</strong></h3>
            <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="box-body no-padding">
          <div id="div_tabla_vencido_16" class="table-responsive dataTables_wrapper form-inline dt-bootstrap" style="padding: 10px;">
            <table id="tabla_cobranza" class="table ">
              <thead>
                <tr id="tabla_cabecera">
                    <th id="tabla_cabecera_fila">N</th>
                    <th id="tabla_cabecera_fila">CLIENTE</th>
                    <th id="tabla_cabecera_fila">TIPO</th>
                    <th id="tabla_cabecera_fila" align="center">MONTO</th>
                    <th id="tabla_cabecera_fila" align="center">VENCIMIENTO</th>
                    <th id="tabla_cabecera_fila" align="center">CUOTAS</th>
                    <th id="tabla_cabecera_fila">ASESOR</th>
                    <th id="tabla_cabecera_fila">NOTAS</th>
                    <th id="tabla_cabecera_fila">ACCIONES</th>
                </tr>
              </thead>
              <?php  foreach($dts['data']as $key=>$dt16){ 
                if($dt16['dias_diferencia']>=16){ 

                  //cuota
                  $cuota16 = $oCuota->mostrarUno($dt16['tb_cuota_id']);

                  //cuotatipo
                  $cuotatipo16 = $oCuotatipo->mostrarUno($dt16['tb_cuotatipo_id']);

                  //usario
                  $usuario = $oUsuario->mostrarUno($dt16['tb_credito_usureg']);

                  //contadores generales
                  $NUMERO_ORDEN ++;
                  array_push($ARRAY_CLIENTES, $dt16['tb_cliente_id']);
                  $NUMERO_CUOTAS ++;

                  if($dt16['tb_moneda_id']==1)$moneda="S/. ";
                  if($dt16['tb_moneda_id']==2)$moneda="US$ ";

                  $vencida = 1;
                  if(strtotime($fecha_hoy) <= strtotime($dt16['tb_cobranza_fec_compromiso'])){$vencida = 0;}; ?>

                  <tr id="tabla_cabecera_fila">
                    <td id="tabla_fila"><?php echo $NUMERO_ORDEN;?></td>
                    <td id="tabla_fila">
                      <?php
                        echo '<span id="span_cli'.$dt16['tb_cuota_id'].'">'.$dt16['tb_cliente_nom']."</span> | ".$dt16['tb_cliente_doc'];
                        echo '<br><span style="font-weight: normal;">'.$dt16['tb_cliente_cel'].'('.$dt16['tb_cliente_procel'].')'.' | '.$dt16['tb_cliente_tel'].'('.$dt16['tb_cliente_protel'].')'.' | '.$dt16['tb_cliente_telref'].'('.$dt16['tb_cliente_protelref'].')</span>';
                      ?>
                    </td>
                    <td id="tabla_fila"><?php echo $cuotatipo16['data']['tb_cuotatipo_nom']?></td>
                    <?php
                      $pagos_cuota = 0;
                      $mod_id=1;
                      $dts3_16=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$dt['tb_cuota_id']);
                      if($dts3_16['estado']==1)
                      {
                          foreach($dts3_16['data']as $key=>$dt3_16)
                        {
                          $pagos_cuota += moneda_mysql($dt3_16['tb_ingreso_imp']);
                        }
                      }
                      if($cuotatipo16['data']['tb_cuotatipo_id'] == 1)
                        $saldo = moneda_mysql($cuota16['data']['tb_cuota_int']-$pagos_cuota);
                      else
                        $saldo = moneda_mysql($cuota16['data']['tb_cuota_cuo']-$pagos_cuota);

                      if($dt16['tb_moneda_id']==1){
                        if($cuotatipo16['data']['tb_cuotatipo_id'] == 1)
                          $TOTAL_SOLES += moneda_mysql($cuota16['data']['tb_cuota_int']-$pagos_cuota);
                        else
                          $TOTAL_SOLES += moneda_mysql($cuota16['data']['tb_cuota_cuo']-$pagos_cuota);
                        $subtotal_soles += $saldo;
                      }
                      if($dt16['tb_moneda_id']==2){
                        if($cuotatipo16['data']['tb_cuotatipo_id'] == 1)
                          $TOTAL_DOLARES += moneda_mysql($cuota16['data']['tb_cuota_int']-$pagos_cuota);
                        else
                          $TOTAL_DOLARES += moneda_mysql($cuota16['data']['tb_cuota_cuo']-$pagos_cuota);
                        $subtotal_dolares += $saldo;
                      }

                      $string = str_replace("-", "", restaFechas($fecha_hoy,mostrar_fecha($dt16['tb_cobranza_fec_compromiso'])));
                      $string = trim($string);
                    ?>
                    <td  id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $moneda . mostrar_moneda($saldo);?></td>
                    <td  id="tabla_fila"align="center" <?php if($dt16['tb_cobranza_estado']==2){ echo 'style="color:white;font-weight:bold; background-color:green;"'; }else{ if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}}?>>
                      <?php
                      if($dt16['tb_cobranza_estado']==2){
                        echo "PAGADO";
                      }else{
                        if($vencida == 1){
                          echo mostrar_fecha($dt16['tb_cobranza_fec_compromiso']) . " (Vencida hace " . str_replace("-", "", $string) . " días)";
                          echo '<button type="button" class="btn btn-danger btn-sm" onclick="recordatorio_cobranza('.$dt16['tb_credito_id'].', '.$dt16['tb_cuota_id'].')">Recordatorio</button>';
                        }
                        else{ 
                          echo mostrar_fecha($dt16['tb_cobranza_fec_compromiso']) . " (Vence en " . $string . " días)";
                          if(intval($string) <= 5)
                            echo '<button type="button" class="btn btn-info btn-sm" onclick="recordatorio_cobranza('.$dt16['tb_credito_id'].', '.$dt16['tb_cuota_id'].')">Recordatorio</button>';
                        }
                      }
                      ?>
                    </td>
                    <td  id="tabla_fila" align="center">
                      <?php
                        if($cuotatipo16['data']['tb_cuotatipo_id']==2){
                          echo $cuota16['data']['tb_cuota_num'].'/'. $dt16['tb_credito_numcuo'];
                        }
                        else{
                          echo $cuota16['data']['tb_cuota_num'].'/'. $dt16['tb_credito_numcuomax'];
                        }
                      ?>
                    </td>
                    <td id="tabla_fila"><?php echo $usuario['data']['tb_usuario_nom'] . ' ' . $usuario['data']['tb_usuario_ape'];?></td>
                    <td id="tabla_fila">
                      <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="cobranza_form('<?php echo $dt16['tb_cuota_id']?>','1')">+</a><span id="cuota_id_<?php echo $dt16['tb_cuota_id'];?>"> <?php echo $cuota16['data']['tb_cuota_not'] ?></span>
                    </td>
                    <td id="tabla_fila">
                      <a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="creditomenor_form('L','<?php echo $dt16['tb_credito_id']?>')">Ver</a>
                      <div class="btn-group">
                        <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="#" onclick="estadoCuentaCobranza(<?php echo $dt16['tb_credito_id']; ?>)"><i class="fa fa-fw fa-file-pdf-o"></i> Estado de Cuenta</a></li>
                          <?php //if($fase_id==2){ ?>
                            <li><a href="#" onclick="compromisoPagoCobranza(<?php echo $dt16['tb_cuota_id']; ?>, <?php echo $dt16['tb_credito_id']; ?>,'1')"><i class="fa fa-calendar"></i> Generar Compromiso</a></li>
                          <?php //} ?>
                          <li><a href="#" onclick="upload_cobranza_form('I',<?php echo $dt16['tb_cobranza_id']; ?>)"><i class="fa fa-upload"></i> Subir Imagen</a></li>
                          <?php //if($fase_id==3){ ?>
                            <li><a href="#" onclick="prorrogaCuotaCobranza(<?php echo $dt16['tb_cobranza_id']; ?>, <?php echo $dt16['tb_cuota_id']; ?>, <?php echo $dt16['tb_credito_id']; ?>,'1')"><i class="fa fa-calendar-plus-o"></i> Dar Prórroga</a></li>
                            <li><a href="#" onclick="remateGarantia1Cobranza(<?php echo $dt16['tb_cuota_id']; ?>, <?php echo $dt16['tb_credito_id']; ?>,'1')"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li>
                          <?php //} ?>
                          <!-- <li><a href="#" onclick="remateGarantia2Cobranza(<?php echo $dt16['tb_credito_id']; ?>)"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li> -->
                        </ul>
                      </div>
                    </td>
                    <!-- <td id="tabla_fila">CM-<?php echo str_pad($dt16['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></td> -->
                  </tr>

              <?php } } ?>
              </tbody>
              <tfoot>
                <tr class="even">
                  <td  id="tabla_fila"colspan="3"><strong>Sub Total</strong></td>
                  <td id="tabla_fila"><strong><?php echo 'S/. '. mostrar_moneda($subtotal_soles); ?></strong></td>
                  <td  id="tabla_fila"colspan="7"></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>

    <?php } ?>

 <?php } ?>
<!-- FIN CREDITO MENOR-->