<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  
  class Cobranza extends Conexion{
      
    function mostrarUnoMenor($id){
        try {
            $sql="SELECT *
                    FROM tb_creditomenor cm
                    INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                    INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
                    INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                    INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
                    INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                    WHERE cm.tb_credito_xac = 1 AND cu.tb_cuota_id=:tb_cuota_id";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
      
    function  mostrarUnoAsiveh($id, $fec_1, $fec_2, $cond){
        try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
              }
            }
		$sql = "SELECT 
                                tb_cuotadetalle_id, tb_cuotadetalle_fec, tb_cuotadetalle_cuo, tb_cuotadetalle_num, cu.tb_moneda_id 
                        FROM 
                                tb_cuota cu 
                                INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id 
                        WHERE 
                                cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est <> 2 
                                AND cd.tb_cuotadetalle_estap = 0 
                                AND cd.tb_cuotadetalle_fec 
                                BETWEEN :fecha1 AND :fecha2 ".$cond_sql;

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
      
    function  mostrarUnoGarveh($id, $fec_1, $fec_2, $cond){
        try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
              }
            }
		$sql = "SELECT 
                            tb_cuotadetalle_id, tb_cuotadetalle_fec, tb_cuotadetalle_cuo, tb_cuotadetalle_num, cu.tb_moneda_id 
                        FROM 
                            tb_cuota cu 
                            INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id 
                        WHERE 
                            cu.tb_cuota_id=$id and cd.tb_cuotadetalle_est <> 2 
                            AND cd.tb_cuotadetalle_estap = 0 
                            AND cd.tb_cuotadetalle_fec 
                            BETWEEN :fecha1 AND :fecha2".$cond_sql;


            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
      
    function  mostrarUnoHipotecario($id, $fec_1, $fec_2, $cond){
        try {
            $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
                  }else{
                    $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
                  }
                }
		$sql = "SELECT 
                                tb_cuotadetalle_id, tb_cuotadetalle_fec, tb_cuotadetalle_cuo, tb_cuotadetalle_num, cu.tb_moneda_id, tb_cuota_int 
                        FROM    tb_cuota cu 
                                INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id 
                        WHERE 
                                cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est <> 2 
                                AND cd.tb_cuotadetalle_estap = 0 
                                AND cd.tb_cuotadetalle_fec 
                                BETWEEN :fecha1 AND :fecha2".$cond_sql;
		
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetch();
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

          return $retorno;
        } catch (Exception $e) {
          throw $e;
        }
    }
    
    function filtrar_credito_cliente($tabla,$tipo,$cli_id,$fec_1,$fec_2,$cond){
      try {
                $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
                  }else{
                    $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
                  }
                }
            $sql=" SELECT cd.tb_cuotadetalle_id
            FROM $tabla cm
            INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
            INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
            INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id

            WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN(1,2,4,7,8) AND cu.tb_creditotipo_id =:tb_creditotipo_id
            AND cu.tb_cuota_est != 2 AND cu.tb_cuota_xac = 1 AND cd.tb_cuotadetalle_est != 2 AND cd.tb_cuotadetalle_estap = 0
            AND cm.tb_cliente_id = :tb_cliente_id
            AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2" .$cond_sql;

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_creditotipo_id", $tipo, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_menor($cli_id, $fec_1, $fec_2, $cond){
      try {
            $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cu.tb_cuota_fec >= date(now())";
                  }else{
                    $cond_sql = " AND cu.tb_cuota_fec < date(now())";
                  }
                }
            $sql = " SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_int, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ct.tb_cuotatipo_id, ct.tb_cuotatipo_nom, ur.tb_usuario_nom, ur.tb_usuario_ape
                    FROM tb_creditomenor cm
                    INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                    INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                    INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
                    INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
                    WHERE cm.tb_credito_xac = 1 and cm.tb_credito_est = 3 and cu.tb_creditotipo_id = 1
                    AND cu.tb_cuota_fec BETWEEN :fecha1 AND :fecha2
                    AND cu.tb_cuota_est IN (1,3) AND tb_cuota_xac = 1".$cond_sql;
            if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
            $sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_asiveh($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now())";
              }
            }
            $sql=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id

                WHERE cm.tb_credito_xac = 1 AND (cm.tb_credito_tip1 = 1 OR cm.tb_credito_tip1 = 4) AND cu.tb_creditotipo_id = 2
                AND cm.tb_credito_est NOT IN(1,2,4,7,8) AND cd.tb_cuotadetalle_estap = 0 AND cd.tb_cuotadetalle_acupag = 0
                AND cu.tb_cuota_acupag = 0 AND cu.tb_cuota_est != 2
                AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 " .$cond_sql;

		if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc, tb_cuota_cuo desc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_asiveh_acupag($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())  ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
		$sql=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cu.tb_creditotipo_id = 2 AND cm.tb_credito_est NOT IN(4,8)
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 and cu.tb_cuota_acupag = 1 AND cu.tb_cuota_est in(1,3)" .$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_asiveh_adendas($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
             $cond_sql = "";
                if($cond != "-"){
                  if ($cond == "PV") {
                    $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now())  ";
                  }else{
                    $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
                  }
                }
		$sql=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cu.tb_creditotipo_id = 2 AND cm.tb_credito_est NOT IN(1,2,4,7,8)
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 and cm.tb_credito_tip1 = 2 AND cu.tb_cuota_est in(1,3)" .$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_garveh($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
		$sql=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_int, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN (1,2,4,7,8)  AND cu.tb_creditotipo_id = 3
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 AND cd.tb_cuotadetalle_estap = 0 AND cd.tb_cuotadetalle_acupag = 0
		AND cu.tb_cuota_acupag = 0 AND cu.tb_cuota_est IN(1,3) ".$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_garveh_acupag($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
		$sql=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN (1,2,4,7,8)  AND cu.tb_creditotipo_id = 3 AND cu.tb_cuota_acupag = 1
		AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2
		AND cu.tb_cuota_est IN(1,3) ".$cond_sql;

		if($cli_id > 0)$sql.=" AND cm.tb_cliente_id = $cli_id";
		if($clientes != '')
			$sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

		$sql.=" GROUP BY cu.tb_cuota_id";
		$sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_hipotecario($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
            $sql=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_int,cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
              FROM tb_creditohipo cm
              INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
              INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
              INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
              INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
              INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
              INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
              WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN(1,2,4,7,8) AND cu.tb_creditotipo_id = 4
              AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 AND cd.tb_cuotadetalle_estap = 0 AND cd.tb_cuotadetalle_acupag = 0 AND cu.tb_cuota_acupag = 0 AND cu.tb_cuota_est != 2 ".$cond_sql;

            if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
            if($clientes != '')
                $sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

            $sql.=" GROUP BY cu.tb_cuota_id";
            $sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function filtrar_hipotecario_acupag($cli_id,$fec_1,$fec_2,$cond, $clientes){
      try {
            $cond_sql = "";
            if($cond != "-"){
              if ($cond == "PV") {
                $cond_sql = " AND cd.tb_cuotadetalle_fec > date(now()) ";
              }else{
                $cond_sql = " AND cd.tb_cuotadetalle_fec <= date(now()) ";
              }
            }
            $sql=" SELECT cm.tb_credito_id, cm.tb_credito_numcuo, cm.tb_credito_numcuomax, cm.tb_moneda_id, c.*, cu.tb_cuota_id, cu.tb_cuota_cuo, cu.tb_cuota_fec, cu.tb_cuota_num, cu.tb_cuota_not, cu.tb_cuota_persubcuo, ur.tb_usuario_nom, ur.tb_usuario_ape, ct.*
              FROM tb_creditohipo cm
              INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
              INNER JOIN tb_moneda m  ON cm.tb_moneda_id = m.tb_moneda_id
              INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
              INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
              INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
              INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
              WHERE cm.tb_credito_xac = 1 AND cm.tb_credito_est NOT IN(4,7,8) AND cu.tb_creditotipo_id = 4
              AND cd.tb_cuotadetalle_fec BETWEEN :fecha1 AND :fecha2 AND cu.tb_cuota_acupag = 1 AND cu.tb_cuota_est != 2 ".$cond_sql;

            if($cli_id>0)$sql.=" AND cm.tb_cliente_id = $cli_id";
            if($clientes != '')
                $sql.=" AND cm.tb_cliente_id IN(".$clientes.")";

            $sql.=" GROUP BY cu.tb_cuota_id";
            $sql.=" ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha1", $fec_1, PDO::PARAM_STR);
            $sentencia->bindParam(":fecha2", $fec_2, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function verificarAlerta($tipCre, $dia){
      try {
            $sql="SELECT *
		FROM tb_creditoalerta
		WHERE tb_creditotipo_id = :tb_creditotipo_id AND tb_creditoalerta_val = :tb_creditoalerta_val";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_creditotipo_id", $tipCre, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_creditoalerta_val", $dia, PDO::PARAM_STR);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
    function verificarEnvioAlerta($tipCuo, $dia, $idcuo){
      try {
            $sql="SELECT *
                    FROM 
                        tb_cuotaalerta c1
                        INNER JOIN tb_creditoalerta c2 ON c1.tb_creditoalerta_id = c2.tb_creditoalerta_id
                    WHERE 
                        c1.tb_cuotaalerta_tip = :tb_cuotaalerta_tip AND c2.tb_creditoalerta_val = :tb_creditoalerta_val AND c1.tb_cuotaalerta_cuoid = :tb_cuotaalerta_cuoid";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuotaalerta_tip", $tipCuo, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_creditoalerta_val", $dia, PDO::PARAM_STR);
            $sentencia->bindParam(":tb_cuotaalerta_cuoid", $idcuo, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
     function registrarEnvioAlerta($cuoale, $tipCuo, $idcuo){
         $tb_cuotaalerta_est=1;
      $this->dblink->beginTransaction();
      try {
        $sql="
		INSERT INTO tb_cuotaalerta
		(
		tb_creditoalerta_id,
		tb_cuotaalerta_tip,
		tb_cuotaalerta_cuoid,
		tb_cuotaalerta_est
		)
		VALUES
		(
                :tb_creditoalerta_id,
		:tb_cuotaalerta_tip,
		:tb_cuotaalerta_cuoid,
		:tb_cuotaalerta_est);";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_creditoalerta_id", $cuoale, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaalerta_tip", $tipCuo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaalerta_cuoid", $idcuo, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuotaalerta_est", $tb_cuotaalerta_est, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function mostrarDetalleAsiveh($id, $id2){
      try {
          $sql="SELECT *
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_id=:tb_cuotadetalle_id and cd.tb_cuotadetalle_est <> 2";  
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_id", $id2, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function mostrarDetalleGarveh($id, $id2){
      try {
          $sql="SELECT *
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_id=:tb_cuotadetalle_id and cd.tb_cuotadetalle_est <> 2";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuotadetalle_id", $id2, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function mostrarPagadoAsiveh($id){
      try {
          $sql="SELECT *
		FROM tb_creditoasiveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function mostrarPagadoGarveh($id){
      try {
          $sql="SELECT *
		FROM tb_creditogarveh cm
		INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
		INNER JOIN tb_moneda m	ON cm.tb_moneda_id = m.tb_moneda_id
		INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
		INNER JOIN tb_cuotadetalle cd ON cu.tb_cuota_id = cd.tb_cuota_id
		INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
		INNER JOIN tb_usuario ur ON cm.tb_credito_usureg=ur.tb_usuario_id
		WHERE cu.tb_cuota_id=:tb_cuota_id and cd.tb_cuotadetalle_est = 2";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuota_id", $id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function filtrar_cuotasvencidas_cliente_creditoasiveh($mon_id, $tip1){
      try {
          $sql="SELECT 
                    cl.tb_cliente_id,cl.tb_cliente_nom, 
                    COUNT(cud.tb_cuotadetalle_id) AS num_cuotasdetalle,
                    SUM(cud.tb_cuotadetalle_cuo) AS mon_total_cuota, 
                    SUM(IFNULL((SELECT SUM(ing.tb_ingreso_imp) 
                FROM 
                    tb_cuotapago cp inner join tb_ingreso ing on ing.tb_ingreso_modide = cp.tb_cuotapago_id 
                    where 
                        cp.tb_modulo_id = 2 and ing.tb_ingreso_xac =1 
                        and tb_cuotapago_xac=1 and cp.tb_cuotapago_modid = cud.tb_cuotadetalle_id 
                        GROUP by cp.tb_cuotapago_modid), 0)) AS pagos_parciales 
                    FROM 
                        tb_cliente cl
                        inner join tb_creditoasiveh cre on cre.tb_cliente_id = cl.tb_cliente_id
                        inner join tb_cuota cuo on cuo.tb_credito_id = cre.tb_credito_id
                        inner join tb_cuotadetalle cud on cud.tb_cuota_id = cuo.tb_cuota_id
                where 
                        tb_credito_est NOT IN(1,2,4,7,8) and tb_credito_xac = 1 
                        and cuo.tb_creditotipo_id = 2 
                        and cud.tb_cuotadetalle_fec <= NOW() 
                        and cud.tb_cuotadetalle_est != 2 
                        and cud.tb_cuotadetalle_estap = 0 
                        and cre.tb_moneda_id =:tb_moneda_id";
          
                if($tip1 != '')
                    $sql.=' and tb_credito_tip1 in ('.$tip1.')';
                    $sql .=' GROUP BY 1';
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_moneda_id", $mon_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }
    
     function asignar_cliente_colaborador($cli_id, $usu_id, $cre_tip, $det){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cobranzacliente(
                                            tb_cliente_id, 
                                            tb_usuario_id, 
                                            tb_cobranzacliente_cretip,
                                            tb_cobranzacliente_det) 
                                    VALUES (
                                            tb_cliente_id, 
                                            tb_usuario_id, 
                                            tb_cobranzacliente_cretip,
                                            tb_cobranzacliente_det)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzacliente_cretip", $cre_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzacliente_det", $det, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
     function eliminar_cliente_colaborador($cli_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_cobranzacliente where tb_cliente_id =:tb_cliente_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cliente_id", $cli_id, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    
    function clientes_asignados_colaborador($usu_id){
      try {
          $sql = "SELECT GROUP_CONCAT(tb_cliente_id) as cliente_asignado, tb_usuario_id 
                  FROM tb_cobranzacliente where tb_usuario_id =:tb_usuario_id group by tb_usuario_id";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_usuario_id", $usu_id, PDO::PARAM_INT);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    /* GERSON (20-01-2023) */
    function lista_fases_cobranza(){
      try {
          $sql = "SELECT * FROM tb_agendadetalle where tb_agendadetalle_codtab='001'";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    /* GERSON (23-01-2023) */
    function lista_estados_cobranza(){
      try {
          $sql = "SELECT * FROM tb_agendadetalle where tb_agendadetalle_codtab='002'";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    function obtener_fase($fase_id){
      try {
        $sql = "SELECT * FROM tb_config where tb_config_codigo =:tb_config_codigo LIMIT 0,1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_config_codigo", $fase_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;

      } catch (Exception $e) {
        //$this->dblink->rollBack();
        throw $e;
      }
    }

    /* GERSON (24-01-2023) */
    function cartera_cobranza_hoy($fecha, $fase_id){
      try {
          $sql = "SELECT * FROM tb_cobranzacartera where tb_cobranzacartera_fecreg=:tb_cobranzacartera_fecreg AND tb_fase_id=:tb_fase_id AND tb_cobranzacartera_estado=1";
          
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_cobranzacartera_fecreg", $fecha, PDO::PARAM_STR);
          $sentencia->bindParam(":tb_fase_id", $fase_id, PDO::PARAM_STR);
          $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              //$resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $sentencia->rowCount();
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = 0;
            }

            return $retorno;
              } catch (Exception $e) {
                throw $e;
              }
    }

    /* GERSON (25-01-23) */
    function lista_cobranza_hoy($fecha, $desde, $hasta){
      try {
          $sql = "SELECT
          DATEDIFF(:fecha_hoy, cu.tb_cuota_fec) as dias_diferencia,
          cm.tb_credito_id,
          cm.tb_credito_numcuo,
          cm.tb_credito_numcuomax,
          cm.tb_moneda_id,
          c.*,
          cu.tb_cuota_id,
          cu.tb_cuota_cuo,
          cu.tb_cuota_int,
          cu.tb_cuota_fec,
          cu.tb_cuota_num,
          cu.tb_cuota_not,
          cu.tb_cuota_persubcuo,
          ct.tb_cuotatipo_id,
          ct.tb_cuotatipo_nom,
          ur.tb_usuario_nom,
          ur.tb_usuario_ape 
        FROM
          tb_creditomenor cm
          INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
          INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
          INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
          INNER JOIN tb_usuario ur ON cm.tb_credito_usureg = ur.tb_usuario_id 
        WHERE
          cm.tb_credito_xac = 1 
          AND cm.tb_credito_est = 3 
          AND cu.tb_creditotipo_id = 1";
          if($hasta>0){
            $sql.=" AND (DATEDIFF(:fecha_hoy, cu.tb_cuota_fec) >= :desde AND DATEDIFF(:fecha_hoy, cu.tb_cuota_fec) <= :hasta)";
          }else{
            $sql.=" AND (DATEDIFF(:fecha_hoy, cu.tb_cuota_fec) >= :desde)";
          }
          
          $sql.=" AND cu.tb_cuota_est IN ( 1, 3 ) 
          AND tb_cuota_xac = 1 
        ORDER BY
          c.tb_cliente_nom,
          cu.tb_cuota_fec ASC";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha_hoy", $fecha, PDO::PARAM_STR);
            $sentencia->bindParam(":desde", $desde, PDO::PARAM_INT);
            if($hasta>0){
              $sentencia->bindParam(":hasta", $hasta, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (21-03-23) */
    function lista_cobranza_cliente($cliente_id, $desde, $hasta){
      try {
          $sql = "SELECT
          DATEDIFF(NOW(), cu.tb_cuota_fec) as dias_diferencia,
          cm.tb_credito_id,
          cm.tb_credito_numcuo,
          cm.tb_credito_numcuomax,
          cm.tb_moneda_id,
          c.*,
          cu.tb_cuota_id,
          cu.tb_cuota_cuo,
          cu.tb_cuota_int,
          cu.tb_cuota_fec,
          cu.tb_cuota_num,
          cu.tb_cuota_not,
          cu.tb_cuota_persubcuo,
          ct.tb_cuotatipo_id,
          ct.tb_cuotatipo_nom,
          ur.tb_usuario_nom,
          ur.tb_usuario_ape 
        FROM
          tb_creditomenor cm
          INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
          INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
          INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
          INNER JOIN tb_usuario ur ON cm.tb_credito_usureg = ur.tb_usuario_id 
        WHERE
          cm.tb_credito_xac = 1 
          AND cm.tb_credito_est = 3 
          AND cu.tb_creditotipo_id = 1
          AND cm.tb_cliente_id = :cliente_id";
          if($hasta>0){
            $sql.=" AND (DATEDIFF(NOW(), cu.tb_cuota_fec) >= :desde AND DATEDIFF(NOW(), cu.tb_cuota_fec) <= :hasta)";
          }else{
            $sql.=" AND (DATEDIFF(NOW(), cu.tb_cuota_fec) >= :desde)";
          }
          $sql.=" AND cu.tb_cuota_est IN ( 1, 3 ) 
          AND tb_cuota_xac = 1 
        ORDER BY
          c.tb_cliente_nom,
          cu.tb_cuota_fec ASC";
          
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":cliente_id", $cliente_id, PDO::PARAM_INT);
            $sentencia->bindParam(":desde", $desde, PDO::PARAM_INT);
            if($hasta>0){
              $sentencia->bindParam(":hasta", $hasta, PDO::PARAM_INT);
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (06-03-23) */
    function existeCobranzaCartera($fecha, $fase_id){
      try {
        $sql = "SELECT * FROM tb_cobranzacartera where tb_cobranzacartera_fecreg =:tb_cobranzacartera_fecreg AND tb_fase_id =:tb_fase_id LIMIT 0,1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cobranzacartera_fecreg", $fecha, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_fase_id", $fase_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No existe cartera creada";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (26-01-23) */
    function existeCobranzaOLD($id, $cuota_id, $fase_id){
      try {
        $sql = "SELECT * FROM tb_cobranza where tb_credito_id =:tb_credito_id AND tb_cuota_id =:tb_cuota_id AND tb_fase_id =:tb_fase_id LIMIT 0,1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_fase_id", $fase_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (02-06-23) */
    function existeCobranza($id, $cuota_id, $cobranza_feccuota, $fase_id){
      try {
        $sql = "SELECT * FROM tb_cobranza where tb_credito_id =:tb_credito_id AND tb_cuota_id =:tb_cuota_id AND tb_cobranza_feccuota =:tb_cobranza_feccuota AND tb_fase_id =:tb_fase_id LIMIT 0,1";
        
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_credito_id", $id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranza_feccuota", $cobranza_feccuota, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_fase_id", $fase_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    function registrarCartera($tb_cobranzacartera_fecreg, $tb_cobranzacartera_des, $tb_fase_id, $tb_usuario_id, $tb_cobranzacartera_estado){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cobranzacartera(
                              tb_cobranzacartera_id, 
                              tb_cobranzacartera_xac, 
                              tb_cobranzacartera_fecreg, 
                              tb_cobranzacartera_des, 
                              tb_fase_id, 
                              tb_usuario_id, 
                              tb_cobranzacartera_estado) 
                      VALUES (
                              NULL,
                              1,
                              :tb_cobranzacartera_fecreg, 
                              :tb_cobranzacartera_des, 
                              :tb_fase_id,
                              :tb_usuario_id,
                              :tb_cobranzacartera_estado)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cobranzacartera_fecreg", $tb_cobranzacartera_fecreg, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cobranzacartera_des", $tb_cobranzacartera_des, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_fase_id", $tb_fase_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_usuario_id", $tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranzacartera_estado", $tb_cobranzacartera_estado, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    function registrarCobranzaCartera($tb_cobranza_feccuota, $tb_cobranza_fecreg, $tb_cobranza_monto, $tb_cobranza_fec_compromiso, $tb_cobranza_des_compromiso, $tb_fase_id, $tb_cliente_id, $tb_credito_id, $tb_cuota_id, $tb_usuario_id, $tb_cobranza_estado){
      $this->dblink->beginTransaction();
      try {
        $sql = "INSERT INTO tb_cobranza(
                              tb_cobranza_id, 
                              tb_cobranza_xac, 
                              tb_cobranza_feccuota, 
                              tb_cobranza_fecreg, 
                              tb_cobranza_monto, 
                              tb_cobranza_fec_compromiso, 
                              tb_cobranza_des_compromiso, 
                              tb_fase_id, 
                              tb_cliente_id, 
                              tb_credito_id, 
                              tb_cuota_id, 
                              tb_usuario_id, 
                              tb_cobranza_estado) 
                      VALUES (
                              NULL,
                              1,
                              :tb_cobranza_feccuota, 
                              :tb_cobranza_fecreg, 
                              :tb_cobranza_monto,
                              :tb_cobranza_fec_compromiso,
                              :tb_cobranza_des_compromiso,
                              :tb_fase_id,
                              :tb_cliente_id,
                              :tb_credito_id,
                              :tb_cuota_id,
                              :tb_usuario_id,
                              :tb_cobranza_estado)";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":tb_cobranza_feccuota", $tb_cobranza_feccuota, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cobranza_fecreg", $tb_cobranza_fecreg, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cobranza_monto", $tb_cobranza_monto, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranza_fec_compromiso", $tb_cobranza_feccuota, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_cobranza_des_compromiso", $tb_cobranza_des_compromiso, PDO::PARAM_STR);
        $sentencia->bindParam(":tb_fase_id", $tb_fase_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cliente_id", $tb_cliente_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_credito_id", $tb_credito_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cuota_id", $tb_cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_usuario_id", $tb_usuario_id, PDO::PARAM_INT);
        $sentencia->bindParam(":tb_cobranza_estado", $tb_cobranza_estado, PDO::PARAM_INT);

        $result = $sentencia->execute();
        $this->dblink->commit();
        return $result;

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }

    /* GERSON (25-01-23) */
    function filtrar_gestion_menor($cliente_id, $condicion, $desde, $hasta, $dias_vencido, $cond){
      try {
          $cond_sql = "";
          if($cond != "-"){
            if ($cond == "PV") {
              $cond_sql = " AND co.tb_cobranza_fec_compromiso >= date(now())";
            }else{
              $cond_sql = " AND co.tb_cobranza_fec_compromiso < date(now())";
            }
          }
          /* $sql = "SELECT
                    DATEDIFF(:fecha_hoy, cu.tb_cuota_fec) as dias_diferencia,
                    cm.tb_credito_id,
                    cm.tb_credito_numcuo,
                    cm.tb_credito_numcuomax,
                    cm.tb_moneda_id,
                    c.*,
	                  co.*,
                    cu.tb_cuota_id,
                    cu.tb_cuota_cuo,
                    cu.tb_cuota_int,
                    cu.tb_cuota_fec,
                    cu.tb_cuota_num,
                    cu.tb_cuota_not,
                    cu.tb_cuota_persubcuo,
                    ct.tb_cuotatipo_id,
                    ct.tb_cuotatipo_nom,
                    ur.tb_usuario_nom,
                    ur.tb_usuario_ape 
                  FROM
                    tb_cobranza co
	                  INNER JOIN tb_creditomenor cm ON co.tb_credito_id = cm.tb_credito_id
                    INNER JOIN tb_cliente c ON cm.tb_cliente_id = c.tb_cliente_id
                    INNER JOIN tb_cuota cu ON cm.tb_credito_id = cu.tb_credito_id
                    INNER JOIN tb_cuotatipo ct ON cm.tb_cuotatipo_id = ct.tb_cuotatipo_id
                    INNER JOIN tb_usuario ur ON cm.tb_credito_usureg = ur.tb_usuario_id 
                    INNER JOIN tb_cobranza cb ON cm.tb_credito_id = cb.tb_credito_id 
                  WHERE
                    cm.tb_credito_xac = 1 
                    AND cm.tb_credito_est = 3 
                    AND cu.tb_creditotipo_id = 1 
                    AND (DATEDIFF(:fecha_hoy, co.tb_cobranza_feccuota) >= :desde AND DATEDIFF(:fecha_hoy, co.tb_cobranza_feccuota) <= :hasta)
                    AND cu.tb_cuota_est IN ( 1, 3 ) 
                    AND tb_cuota_xac = 1"; */
          $hoy = date('Y-m-d');
          $filtro = "%".$condicion."%";
          $sql = "SELECT
                    DISTINCT (co.tb_cobranza_id),
                    DATEDIFF(NOW(), co.tb_cobranza_fec_compromiso) as dias_diferencia,
                    co.tb_credito_id,
                    co.tb_cuota_id,
                    co.tb_cobranza_fec_compromiso,
                    co.tb_cobranza_estado,
                    cm.tb_cuotatipo_id,
                    cm.tb_credito_usureg,
                    cm.tb_credito_numcuo,
                    cm.tb_credito_numcuomax,
                    cm.tb_moneda_id,
                    c.*,
                    ur.tb_usuario_nom,
                    ur.tb_usuario_ape 
                  FROM
                    tb_cobranza co
                    INNER JOIN tb_creditomenor cm ON co.tb_credito_id = cm.tb_credito_id
                    INNER JOIN tb_cliente c ON co.tb_cliente_id = c.tb_cliente_id
                    INNER JOIN tb_cuota cu ON co.tb_credito_id = cu.tb_credito_id
                    INNER JOIN tb_usuario ur ON co.tb_usuario_id = ur.tb_usuario_id 
                  WHERE
                    co.tb_cobranza_xac = 1 
                    AND cu.tb_creditotipo_id = 1"; 
                    if($hasta>0){
                      $sql.=" AND (DATEDIFF(:fecha_hoy, tb_cobranza_fec_compromiso) >= :desde AND DATEDIFF(:fecha_hoy, tb_cobranza_fec_compromiso) <= :hasta)";
                    }else{

                      // GERSON 07-03-23
                      // Filtros por dias vencidos 
                      // -1 = sin filtros
                      // 0 = -2 <= dias vencidos >= 0
                      // 1 al 15 numero exacto de vencimiento
                      // 16 = dias vencidos >= 16 
                      /* if($dias_vencido>=0){
                        if($dias_vencido==0){
                          $sql.=" AND ( DATEDIFF(:fecha_hoy, tb_cobranza_fec_compromiso) >= :desde AND DATEDIFF(:fecha_hoy, tb_cobranza_fec_compromiso) <= :dias_vencido)";
                        }else if($dias_vencido>0 && $dias_vencido<16){
                          $sql.=" AND ( DATEDIFF(:fecha_hoy, tb_cobranza_fec_compromiso) = :dias_vencido)";
                        }else if($dias_vencido==16){
                          $sql.=" AND ( DATEDIFF(:fecha_hoy, tb_cobranza_fec_compromiso) >= :dias_vencido)";
                        }
                      }else{ */
                        $sql.=" AND (DATEDIFF(:fecha_hoy, tb_cobranza_fec_compromiso) >= :desde)";
                      /* } */
                    }
                    $sql.=" AND cu.tb_cuota_est IN ( 1, 3 ) AND tb_cuota_xac = 1";
                    /* co.tb_cobranza_xac = 1 
                    AND cu.tb_creditotipo_id = 1 
                    AND (DATEDIFF(:fecha_hoy, co.tb_cobranza_feccuota) >= :desde AND DATEDIFF(:fecha_hoy, co.tb_cobranza_feccuota) <= :hasta)
                    AND cu.tb_cuota_est IN ( 1, 3 ) 
                    AND tb_cuota_xac = 1"; */

                  if($cliente_id>0){
                    $sql.=" AND c.tb_cliente_id = $cliente_id";
                  } 
                  if($condicion>0){
                    $sql.=" AND co.tb_cobranza_estado LIKE :filtro";
                  }else{
                    //$sql.=" AND co.tb_cobranza_estado IN ( 1, 2, 3 )"; // Busqueda de los estados de PENDIENTE DE PAGO, PAGADOS, COMPROMISO DE PAGO
                  }
                  $sql.= $cond_sql." ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";
                  
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":fecha_hoy", $hoy, PDO::PARAM_STR);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);
            /* if($dias_vencido>0 && $dias_vencido<=16){
            }else{ */
              $sentencia->bindParam(":desde", $desde, PDO::PARAM_INT);
            /* } */
            if($hasta>0){
              $sentencia->bindParam(":hasta", $hasta, PDO::PARAM_INT);
            }
            /* if($dias_vencido>=0){
              $sentencia->bindParam(":dias_vencido", $dias_vencido, PDO::PARAM_INT);
            } */

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    /* GERSON (25-01-23) */
    function filtrar_gestion_menor_2($cliente_id, $condicion, $desde, $hasta, $dias_vencido, $cond){
      try {
          $cond_sql = "";
          if($cond != "-"){
            if ($cond == "PV") {
              $cond_sql = " AND co.tb_cobranza_fec_compromiso >= date(now())";
            }else{
              $cond_sql = " AND co.tb_cobranza_fec_compromiso < date(now())";
            }
          }
          $hoy = date('Y-m-d');
          $filtro = "%".$condicion."%";
          $sql = "SELECT
                    DISTINCT (co.tb_cobranza_id),
                    DATEDIFF(NOW(), co.tb_cobranza_fec_compromiso) as dias_diferencia,
                    co.tb_credito_id,
                    co.tb_cuota_id,
                    co.tb_cobranza_fec_compromiso,
                    co.tb_cobranza_estado,
                    cm.tb_cuotatipo_id,
                    cm.tb_credito_usureg,
                    cm.tb_credito_numcuo,
                    cm.tb_credito_numcuomax,
                    cm.tb_moneda_id,
                    c.*,
                    ur.tb_usuario_nom,
                    ur.tb_usuario_ape 
                  FROM
                    tb_cobranza co
                    INNER JOIN tb_creditomenor cm ON co.tb_credito_id = cm.tb_credito_id
                    INNER JOIN tb_cliente c ON co.tb_cliente_id = c.tb_cliente_id
                    INNER JOIN tb_cuota cu ON co.tb_credito_id = cu.tb_credito_id
                    INNER JOIN tb_usuario ur ON co.tb_usuario_id = ur.tb_usuario_id 
                  WHERE
                    co.tb_cobranza_xac = 1 
                    AND cu.tb_creditotipo_id = 1"; 
                    
                    $sql.=" AND cu.tb_cuota_est IN ( 1, 3 ) AND tb_cuota_xac = 1";
                   

                  if($cliente_id>0){
                    $sql.=" AND c.tb_cliente_id = $cliente_id";
                  } 
                  if($condicion>0){
                    $sql.=" AND co.tb_cobranza_estado LIKE :filtro";
                  }
                  $sql.= $cond_sql." ORDER BY c.tb_cliente_nom, cu.tb_cuota_fec asc";
                  
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":filtro", $filtro, PDO::PARAM_STR);

            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
              $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
              $retorno["estado"] = 1;
              $retorno["mensaje"] = "exito";
              $retorno["data"] = $resultado;
              $sentencia->closeCursor(); //para libera memoria de la consulta
            }
            else{
              $retorno["estado"] = 0;
              $retorno["mensaje"] = "No hay tipos de crédito registrados";
              $retorno["data"] = "";
            }

            return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    /* GERSON (21-02-23) */
    function mostrarUno($cobranza_id){
      try {
          $sql="SELECT * FROM tb_cobranza WHERE tb_cobranza_xac = 1 AND tb_cobranza_id=:tb_cobranza_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_cobranza_id", $cobranza_id, PDO::PARAM_INT);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay tipos de crédito registrados";
            $retorno["data"] = "";
          }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    /* GERSON (13-02-23) */
    function cobranza_estado_cuenta($credito_id){
      try {
        $sql = "SELECT *FROM tb_cuota cuo 
          LEFT JOIN tb_moneda mon on mon.tb_moneda_id = cuo.tb_moneda_id
          WHERE cuo.tb_credito_id =:credito_id and cuo.tb_cuota_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (13-02-23) */
    function cobranza_monto_pagado($cuota_id){
      try {
        $sql = "SELECT * FROM tb_ingreso ing 
          INNER JOIN tb_cuotapago cup on ing.tb_ingreso_modide = cup.tb_cuotapago_id 
          INNER JOIN tb_cuota cuo on cup.tb_cuotapago_modid = cuo.tb_cuota_id 
          WHERE cuo.tb_cuota_id =:cuota_id and tb_cuota_xac = 1 and cup.tb_modulo_id = 1 and ing.tb_modulo_id = 30";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (15-02-23) */
    function insertar_compromiso_pago($cuota_id, $credito_id){
      try {
        $sql = "SELECT co.*, c.tb_cliente_nom FROM tb_cobranza co
          INNER JOIN tb_cliente c ON co.tb_cliente_id = c.tb_cliente_id
          WHERE co.tb_cuota_id =:cuota_id and co.tb_credito_id =:credito_id and co.tb_cobranza_xac = 1 LIMIT 0,1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (20-03-23) */
    function obtener_cobranza($cuota_id, $credito_id){
      try {
        $sql = "SELECT co.*, c.tb_cliente_nom FROM tb_cobranza co
          INNER JOIN tb_cliente c ON co.tb_cliente_id = c.tb_cliente_id
          WHERE co.tb_cuota_id =:cuota_id and co.tb_credito_id =:credito_id and co.tb_cobranza_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (07-03-23) */
    function consultar_cobranza_por_credito($cuota_id,$credito_id){
      try {
        $sql = "SELECT co.*, c.tb_cliente_nom FROM tb_cobranza co
                INNER JOIN tb_cliente c ON co.tb_cliente_id = c.tb_cliente_id
                WHERE co.tb_cuota_id = :cuota_id and co.tb_credito_id =:credito_id and co.tb_cobranza_xac = 1";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":cuota_id", $cuota_id, PDO::PARAM_INT);
        $sentencia->bindParam(":credito_id", $credito_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }

    /* GERSON (21-02-23) */
    function editar_dato_simple($cobranza_id, $campo, $valor){
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_cobranza SET $campo=:valor WHERE tb_cobranza_id =:cobranza_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":valor", $valor, PDO::PARAM_STR);
          $sentencia->bindParam(":cobranza_id", $cobranza_id, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();
          return $result; //si es correcto retorna 1
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
    }
    /*  */

    /* GERSON (16-02-23) */
    function modificar_compromiso_cobranza($cobranza_id, $fecha, $compromiso, $estado){
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_cobranza SET tb_cobranza_fec_compromiso=:cobranza_fec_compromiso, tb_cobranza_des_compromiso=:cobranza_des_compromiso, tb_cobranza_estado=:cobranza_estado WHERE tb_cobranza_id =:cobranza_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cobranza_fec_compromiso", $fecha, PDO::PARAM_STR);
          $sentencia->bindParam(":cobranza_des_compromiso", $compromiso, PDO::PARAM_STR);
          $sentencia->bindParam(":cobranza_estado", $estado, PDO::PARAM_STR);
          $sentencia->bindParam(":cobranza_id", $cobranza_id, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();
          return $result; //si es correcto retorna 1
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
    }
    /*  */

    /* GERSON (17-02-23) */
    function modificar_remate_cobranza($cobranza_id, $fecha, $usuario, $estado){
      $this->dblink->beginTransaction();
      try {
          $sql = "UPDATE tb_cobranza SET tb_cobranza_fec_remate=:cobranza_fec_remate, tb_cobranza_use_remate=:cobranza_use_remate, tb_cobranza_estado=:cobranza_estado WHERE tb_cobranza_id =:cobranza_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":cobranza_fec_remate", $fecha, PDO::PARAM_STR);
          $sentencia->bindParam(":cobranza_use_remate", $usuario, PDO::PARAM_INT);
          $sentencia->bindParam(":cobranza_estado", $estado, PDO::PARAM_INT);
          $sentencia->bindParam(":cobranza_id", $cobranza_id, PDO::PARAM_INT);
          $result = $sentencia->execute();

          $this->dblink->commit();
          return $result; //si es correcto retorna 1
      } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
      }
    }
    /*  */

    /* GERSON (21-02-23) */
    function getConfig($codigo){
      try {
          $sql="SELECT * FROM tb_config WHERE tb_config_xac = 1 AND tb_config_codigo=:codigo";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":codigo", $codigo, PDO::PARAM_INT);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay configuración registrada";
            $retorno["data"] = "";
          }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    /* GERSON (21-02-23) */
    function mostrarUnoByCuotaId($cuota_id){
      try {
          $sql="SELECT * FROM tb_cobranza WHERE tb_cobranza_xac = 1 AND tb_cuota_id=:tb_cuota_id";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":tb_cuota_id", $cuota_id, PDO::PARAM_INT);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetch();
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay cobranza registrada";
            $retorno["data"] = "";
          }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    /* GERSON (06-10-23) */
    function mostrarEstadoCuota($hoy){
      try {
          $sql="SELECT 
                  cuo.tb_cuota_id, 
                  cuo.tb_cuota_xac, 
                  cuo.tb_cuota_fec, 
                  cuo.tb_creditotipo_id, 
                  cuo.tb_cuota_cuo, 
                  cuo.tb_cuota_est AS estado_cuota, 
                  cob.tb_cobranza_id, 
                  cob.tb_cobranza_estado AS estado_cobranza, 
                  cob.tb_cobranza_xac, 
                  DATEDIFF(:hoy,cuo.tb_cuota_fec) AS dias_vencido, 
                  cre.tb_credito_id, 
                  cre.tb_credito_est
                FROM tb_cuota cuo
                INNER JOIN tb_cobranza cob ON cuo.tb_cuota_id = cob.tb_cuota_id
                INNER JOIN tb_creditomenor cre ON cuo.tb_credito_id = cre.tb_credito_id
                WHERE (cuo.tb_cuota_xac != cob.tb_cobranza_xac OR cuo.tb_cuota_est != cob.tb_cobranza_estado)
                AND cuo.tb_cuota_est IN (1, 2, 3)
                AND cob.tb_cobranza_xac > 0
                AND cuo.tb_creditotipo_id = 1 
                AND DATEDIFF(:hoy,cuo.tb_cuota_fec) >= -2
                ORDER BY cuo.tb_cuota_fec DESC";
          
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":hoy", $hoy, PDO::PARAM_STR);
          $sentencia->execute();

          if ($sentencia->rowCount() > 0) {
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $retorno["estado"] = 1;
            $retorno["mensaje"] = "exito";
            $retorno["data"] = $resultado;
            $sentencia->closeCursor(); //para libera memoria de la consulta
          }
          else{
            $retorno["estado"] = 0;
            $retorno["mensaje"] = "No hay cuotas registradas";
            $retorno["data"] = "";
          }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    /*  */

    // ANTONIO 05-07-20204
    function lista_cuotas_vencidas_30dias(){
      try {
        
        $sql = "SELECT co.tb_cobranza_id, cre.tb_credito_id,cuo.tb_cuota_id, cuo.tb_cuota_fec, co.tb_cobranza_fec_compromiso, DATEDIFF(CURDATE(), tb_cuota_fec) AS dias_diferencia,cuo.tb_cuota_est, co.tb_cobranza_estado, cre.tb_credito_est FROM `tb_cobranza` co
          INNER JOIN tb_cuota cuo ON (cuo.tb_cuota_id = co.tb_cuota_id) 
          INNER JOIN tb_creditomenor cre ON (cre.tb_credito_id = cuo.tb_credito_id AND cuo.tb_creditotipo_id = 1)
          INNER JOIN tb_garantia gar ON (gar.tb_credito_id = cre.tb_credito_id)
          WHERE tb_cuota_xac = 1 AND co.tb_cobranza_xac = 1 AND cuo.tb_cuota_est IN(1,3) AND cre.tb_credito_est = 3 AND gar.tb_garantiatipo_id IN(1,2,3) AND DATEDIFF(CURDATE(), tb_cuota_fec) >= 30
          GROUP BY 1;";
        $sentencia = $this->dblink->prepare($sql);
        $sentencia->execute();

        $retorno["sql"] = $sql;
        $retorno["cantidad"] = $sentencia->rowCount();
        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor();
        } else {
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "error";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
