<?php
require_once('../../core/usuario_sesion.php');
require_once('../funciones/fechas.php');
require_once('TareaCobranza.class.php');
$oTareaCobranza = new TareaCobranza();
$fecha_hoy = date('Y-m-d');
$listas = array();
//echo $fecha_hoy; exit();

//aqui hacer la consulta php de todas las notificaciones con estado 0
$consulta_por_hacer = $oTareaCobranza->listar_notificaciones_cobranza('', $fecha_hoy);
$consulta_hecho_hoy = $oTareaCobranza->listar_notificaciones_cobranza('_hace', $fecha_hoy);

if ($consulta_por_hacer["estado"] == 1) {
    foreach ($consulta_por_hacer["data"] as $key => $notificacion) {
        $listas["por_hacer"] .= "<li class=\"item\">".
                                    "<div class=\"col-md-9\" style=\"width: 78%\">".
                                        "{$notificacion['tb_cobranzanoti_not']} | Tarea programada para el <b>" . mostrar_fecha($notificacion['tb_cobranzanoti_fec']) . "</b> por <b>{$notificacion['nombre_usuario']}</b>".
                                    "</div>".
                                    "<div class=\"col-md-3\" style=\"width: 22%\">".
                                        "<button class=\"btn bg-light-blue btn-xs btn-block\" onclick=\"boton_ok({$notificacion['tb_cobranzanoti_id']})\"><i class=\"fa fa-check\"></i> &nbsp; Ok</button>".
                                        "<button class=\"btn bg-aqua btn-xs btn-block\" onclick=\"cobranza_form({$notificacion['tb_cuota_id']},'1', {$notificacion['tb_cobranzanoti_id']})\"><i class=\"fa fa-bell\"></i> Nueva noti</button>".
                                    "</div>".
                                "</li>";
    }
} else {
    $listas["por_hacer"] = $consulta_por_hacer["mensaje"];
}
$consulta_por_hacer = NULL;

if ($consulta_hecho_hoy["estado"] == 1) {
    foreach ($consulta_hecho_hoy["data"] as $key => $notificacion) {
        $listas["hecho"] .= "<li class=\"item\">".
                                "<div class=\"col-md-9\" style=\"width: 78%\">".
                                    "{$notificacion['tb_cobranzanoti_not']} | Tarea realizada el <b>" . mostrar_fecha($notificacion['tb_cobranzanoti_fec_hace']) . "</b> por <b>{$notificacion['nombre_usuario']}</b>".
                                "</div>".
                                "<div class=\"col-md-3\" style=\"width: 22%\">".
                                    "<button class=\"btn bg-green btn-xs btn-block\" onclick=\"cobranza_form({$notificacion['tb_cuota_id']},'1')\"><i class=\"fa fa-plus\"></i></button>".
                                "</div>".
                            "</li>";
    }
} else {
    $listas["hecho"] = $consulta_hecho_hoy["mensaje"];
}
$consulta_hecho_hoy = NULL;

//return $listas;
echo json_encode($listas);

?>