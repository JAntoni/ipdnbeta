<?php
require_once('../../core/usuario_sesion.php');

/* require_once ("../cuota/Cuota.class.php");
$oCuota = new Cuota();
require_once ("../cuota/Cuotadetalle.class.php");
$oCuotadetalle = new Cuotadetalle(); */
require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');
require_once('../formulaventa/Formulamenor.class.php');
$oFormula = new Formulamenor();

$dts1 = $oFormula->mostrar_vigente();

if($dts1['estado']==1){
    $formenor_id =  $dts1['data']['tb_formula_id'];
    $formenor_nom = $dts1['data']['tb_formula_nom']; //nombre de la formula
    $formenor_rec = $dts1['data']['tb_formula_rec']; //valor de recuperacion, generalmente es 1.5 a mas
    $formenor_pro = $dts1['data']['tb_formula_pro']; //numero de meses de proyeccion
}


$credito_id = $_POST['credito_id'];

$dts = $oGarantia->listar_garantias($credito_id);
?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_cobranza_remate_garantia" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">x</span>
              </button>
                <h4 class="modal-title" style="font-weight: bold;font-family: cambria">LISTA DE GARANTÍAS</h4>
            </div>
            <input type="hidden" class="form-control input-sm" id="hdd_formenor_rec" name="hdd_formenor_rec" class="form-control input-sm" value="<?php echo $formenor_rec;?>">
            <input type="hidden" class="form-control input-sm" id="hdd_formenor_pro" name="hdd_formenor_pro" class="form-control input-sm" value="<?php echo $formenor_pro;?>">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="" class="table">
                                <thead>
                                    <tr id="tabla_cabecera">
                                        <th id="tabla_cabecera_fila">N°</th>
                                        <th id="tabla_cabecera_fila">ID GARANTÍA</th>
                                        <th id="tabla_cabecera_fila">GARANTÍA</th>
                                        <th id="tabla_cabecera_fila"></th>
                                    </tr>
                                </thead>  
                                <tbody> 
                                    <?php
                                    $cont = 1;
                                    foreach ($dts['data']as $key => $dt) {
                                    ?>
                                        <tr class="" name="tabla_cabecera_fila_det" id="garantia<?php echo $dt['tb_garantia_id']; ?>" onclick="">
                                            <td id="tabla_fila"><?php echo $cont ?></td>
                                            <td align="left" id="tabla_fila"><?php echo $dt['tb_garantia_id']; ?></td>
                                            <!-- <td id="tabla_fila"><?php echo 'CM-' . str_pad($dt['tb_credito_id'], 4, '0', STR_PAD_LEFT) ?></td> -->
                                            <td align="left" id="tabla_fila"><?php echo $dt['tb_garantia_pro'] ?></td>
                                            <!-- <td align="left" id="tabla_fila"><?php echo $dt['tb_cliente_nom'] ?></td> -->
                                            <td id="tabla_fila">
                                                <a class="btn btn-info btn-xs" onclick="garantia_resumen(<?php echo $dt['tb_garantia_id'];?>)"><i class="fa fa-eye"></i> Info</a>
                                            </td>
                                        </tr> 
                                    <?php
                                        $res = null;
                                        $cont++;
                                    } 
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    
                    </div>
                 
                    <!--- MESAJES DE GUARDADO -->
                    <div class="callout callout-info" id="cobranza_mensaje" style="display: none;">
                        <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo 'vista/cobranza/cobranza_form.js?ver==20230321';?>"></script>