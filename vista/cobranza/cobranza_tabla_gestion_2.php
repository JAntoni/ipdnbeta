<?php
 if(defined('VISTA_URL')){
        require_once(APP_URL.'core/usuario_sesion.php');
        require_once (VISTA_URL.'cuotapago/Cuotapago.class.php');
        require_once (VISTA_URL.'cuota/Cuota.class.php');
        require_once (VISTA_URL.'cuotatipo/Cuotatipo.class.php');
        require_once (VISTA_URL.'cliente/Cliente.class.php');
        require_once (VISTA_URL.'usuario/Usuario.class.php');
        require_once (VISTA_URL."creditomenor/Creditomenor.class.php");
        require_once (VISTA_URL.'funciones/funciones.php');
        require_once (VISTA_URL.'funciones/fechas.php');
  }
  else{
        require_once('../../core/usuario_sesion.php');
        require_once ("../cuotapago/Cuotapago.class.php");
        require_once ("../cuota/Cuota.class.php");
        require_once ("../cuotatipo/Cuotatipo.class.php");
        require_once ("../cliente/Cliente.class.php");
        require_once ("../usuario/Usuario.class.php");
        require_once ("../creditomenor/Creditomenor.class.php");
        require_once ("../funciones/funciones.php");
        require_once ("../funciones/fechas.php");
  }
require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
$oCuotapago = new Cuotapago();
$oCuota = new Cuota();
$oCuotatipo = new Cuotatipo();
$oCliente = new Cliente();
$oUsuario = new Usuario();
$oCreditomenor = new Creditomenor();

//$fecha_hoy = date('d-m-Y');
$fecha_hoy = date('Y-m-d');

// clave rapido
$clave = $oCobranza->getConfig('1');
$valorClave='';
if($clave['data']!='' || $clave['data']!=null){
  $valorClave=$clave['data']['tb_config_valor'];
}

//CONTADORES GENERALES
$NUMERO_ORDEN = 0;
$ARRAY_CLIENTES = array();
$TOTAL_SOLES = 0;
$TOTAL_DOLARES = 0;
$NUMERO_CUOTAS = 0;

$subtotal_soles = 0;
$subtotal_dolares = 0;

//<!-- CREDITOS MENORES-->

  $cliente_id = intval($_POST['hdd_cli_id_gestion']);
  $cliente = $_POST['txt_cli_gestion'];
  //$condicion = intval($_POST['cmb_estado_cobranza']);
  $cond = $_POST['cmb_fil_cuo_cond'];
  $condicion = $_POST['cmb_estado_cobranza'];
  $grupo = $_POST['cmb_grupo'];
  $fase_id = $_POST['cmb_fase_cobranza'];
  $dias_vencido = 0;
  if($_POST['cmb_dia_vencido']=='all'){
    $dias_vencido = -1;
  }else{
    $dias_vencido = intval($_POST['cmb_dia_vencido']);
  }
  
  $fase = $oCobranza->obtener_fase($fase_id); // fase seleccionada

  // GERSON (20-03-23)
  $ver_vencidos = "";
  $ver_por_vencer = "";
  if($cond!="-"){
    if($cond=="PV"){
      $ver_vencidos = "hidden";
    }else{
      $ver_por_vencer = "hidden";
    }
  }

  /* GERSON (11-07-23) ACTUALIZADO */ 
  $existe_cartera = $oCobranza->existeCobranzaCartera($fecha, $fase_id);
  // si ya se generó la cartera el día de hoy en la gestion para todos los cliente, ya no es necesario evaluar al cliente de manera individual
  if($existe_cartera['estado']==0){ 
    $lista_cobranza = $oCobranza->lista_cobranza_cliente($cliente_id, intval($fase['data'][0]['tb_config_valor']), intval($fase['data'][0]['tb_config_valor2'])); // lista de cuotas del cliente
    if($lista_cobranza['estado']==1){ // existen cuotas
      foreach ($lista_cobranza['data'] as $key => $value) {
        $existe = $oCobranza->existeCobranza($value['tb_credito_id'], $value['tb_cuota_id'], $value['tb_cuota_fec'], 2); // buscar si existen en la cartera (por defecto se maneja una fase = 2)
        if($existe['estado']==0){ // Si no no existe se registrará en la 'tb_cobranza'
          $oCobranza->registrarCobranzaCartera($value['tb_cuota_fec'], date('Y-m-d'), $value['tb_cuota_cuo'], null, null, 2, $value['tb_cliente_id'], $value['tb_credito_id'], $value['tb_cuota_id'], $_SESSION['usuario_id'], 1);
        }
      }
    }
  }
  
  /*  */

  if($grupo == "-" || $grupo == "CM"){
    $dts=$oCobranza->filtrar_gestion_menor_2(intval($cliente_id), $condicion, intval($fase['data'][0]['tb_config_valor']), intval($fase['data'][0]['tb_config_valor2']), $dias_vencido, $cond); ?>

    <h1 style="text-align:left; padding:10px 0 0 5px;margin-bottom:2px;">CRÉDITO MENOR</h1>
    <table id="tabla_cobranza" class="table ">
      <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila">N</th>
            <th id="tabla_cabecera_fila">CLIENTE</th>
            <th id="tabla_cabecera_fila">TIPO</th>
            <th id="tabla_cabecera_fila" align="center">MONTO</th>
            <th id="tabla_cabecera_fila" align="center">VENCIMIENTO</th>
            <th id="tabla_cabecera_fila" align="center">CUOTAS</th>
            <th id="tabla_cabecera_fila">ASESOR</th>
            <th id="tabla_cabecera_fila">NOTAS</th>
            <th id="tabla_cabecera_fila">ACCIONES</th>
            <!-- <th id="tabla_cabecera_fila">ID</th> -->
        </tr>
      </thead>
      <?php
        if($dts['estado']==1){ ?>
          <tbody> <?php
            foreach($dts['data']as $key=>$dt){

              //cuota
              $cuota = $oCuota->mostrarUno($dt['tb_cuota_id']);

              //cuotatipo
              $cuotatipo = $oCuotatipo->mostrarUno($dt['tb_cuotatipo_id']);

              //usario
              $usuario = $oUsuario->mostrarUno($dt['tb_credito_usureg']);

              //contadores generales
              $NUMERO_ORDEN ++;
              array_push($ARRAY_CLIENTES, $dt['tb_cliente_id']);
              $NUMERO_CUOTAS ++;

              if($dt['tb_moneda_id']==1)$moneda="S/. ";
              if($dt['tb_moneda_id']==2)$moneda="US$ ";

              $vencida = 1;
              if(strtotime($fecha_hoy) <= strtotime($dt['tb_cobranza_fec_compromiso'])){$vencida = 0;}; ?>

              <tr id="tabla_cabecera_fila">
                <td id="tabla_fila"><?php echo $NUMERO_ORDEN;?></td>
                <td id="tabla_fila">
                  <?php
                    echo '<span id="span_cli'.$dt['tb_cuota_id'].'">'.$dt['tb_cliente_nom']."</span> | ".$dt['tb_cliente_doc'];
                    echo '<br><span style="font-weight: normal;">'.$dt['tb_cliente_cel'].'('.$dt['tb_cliente_procel'].')'.' | '.$dt['tb_cliente_tel'].'('.$dt['tb_cliente_protel'].')'.' | '.$dt['tb_cliente_telref'].'('.$dt['tb_cliente_protelref'].')</span>';
                  ?>
                </td>
                <td id="tabla_fila"><?php echo $cuotatipo['data']['tb_cuotatipo_nom']?></td>
                <?php
                  $pagos_cuota = 0;
                  $mod_id=1;
                  $dts3=$oCuotapago->mostrar_cuotatipo_ingreso($mod_id,$dt['tb_cuota_id']);
                  if($dts3['estado']==1)
                  {
                      foreach($dts3['data']as $key=>$dt3)
                    {
                      $pagos_cuota += moneda_mysql($dt3['tb_ingreso_imp']);
                    }
                  }
                  if($cuotatipo['data']['tb_cuotatipo_id'] == 1)
                    $saldo = moneda_mysql($cuota['data']['tb_cuota_int']-$pagos_cuota);
                  else
                    $saldo = moneda_mysql($cuota['data']['tb_cuota_cuo']-$pagos_cuota);

                  if($dt['tb_moneda_id']==1){
                    if($cuotatipo['data']['tb_cuotatipo_id'] == 1)
                      $TOTAL_SOLES += moneda_mysql($cuota['data']['tb_cuota_int']-$pagos_cuota);
                    else
                      $TOTAL_SOLES += moneda_mysql($cuota['data']['tb_cuota_cuo']-$pagos_cuota);
                    $subtotal_soles += $saldo;
                  }
                  if($dt['tb_moneda_id']==2){
                    if($cuotatipo['data']['tb_cuotatipo_id'] == 1)
                      $TOTAL_DOLARES += moneda_mysql($cuota['data']['tb_cuota_int']-$pagos_cuota);
                    else
                      $TOTAL_DOLARES += moneda_mysql($cuota['data']['tb_cuota_cuo']-$pagos_cuota);
                    $subtotal_dolares += $saldo;
                  }

                  $string = str_replace("-", "", restaFechas($fecha_hoy,mostrar_fecha($dt['tb_cobranza_fec_compromiso'])));
                  $string = trim($string);
                ?>
                <td  id="tabla_fila" align="right" style="padding-right:10px;"><?php echo $moneda . mostrar_moneda($saldo);?></td>
                <td  id="tabla_fila"align="center" <?php if($dt['tb_cobranza_estado']==2){ echo 'style="color:white;font-weight:bold; background-color:green;"'; }else{ if($vencida == 1){ echo 'style="color:red;font-weight:bold;"';}}?>>
                  <?php
                  if($dt['tb_cobranza_estado']==2){
                    echo "PAGADO";
                  }else{
                    if($vencida == 1){
                      echo mostrar_fecha($dt['tb_cobranza_fec_compromiso']) . " (Vencida hace " . str_replace("-", "", $string) . " días)";
                      echo '<button type="button" class="btn btn-danger btn-sm" onclick="recordatorio_cobranza('.$dt['tb_credito_id'].', '.$dt['tb_cuota_id'].')">Recordatorio</button>';
                    }
                    else{ 
                      echo mostrar_fecha($dt['tb_cobranza_fec_compromiso']) . " (Vence en " . $string . " días)";
                      if(intval($string) <= 5)
                        echo '<button type="button" class="btn btn-info btn-sm" onclick="recordatorio_cobranza('.$dt['tb_credito_id'].', '.$dt['tb_cuota_id'].')">Recordatorio</button>';
                    }
                  }
                  ?>
                </td>
                <td  id="tabla_fila" align="center">
                  <?php
                    if($cuotatipo['data']['tb_cuotatipo_id']==2){
                      echo $cuota['data']['tb_cuota_num'].'/'. $dt['tb_credito_numcuo'];
                    }
                    else{
                      echo $cuota['data']['tb_cuota_num'].'/'. $dt['tb_credito_numcuomax'];
                    }
                  ?>
                </td>
                <td id="tabla_fila"><?php echo $usuario['data']['tb_usuario_nom'] . ' ' . $usuario['data']['tb_usuario_ape'];?></td>
                <td id="tabla_fila">
                  <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="cobranza_form('<?php echo $dt['tb_cuota_id']?>','1')">+</a><span id="cuota_id_<?php echo $dt['tb_cuota_id'];?>"> <?php echo $cuota['data']['tb_cuota_not'] ?></span>
                </td>
                <td id="tabla_fila">
                  <a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="creditomenor_form('L','<?php echo $dt['tb_credito_id']?>')">Ver</a>
                  <div class="btn-group">
                    <button type="button" class="btn bg-olive dropdown-toggle btn-xs" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li><a href="#" onclick="estadoCuentaCobranza(<?php echo $dt['tb_credito_id']; ?>)"><i class="fa fa-fw fa-file-pdf-o"></i> Estado de Cuenta</a></li>
                      <?php //if($fase_id==2){ ?>
                        <li><a href="#" onclick="compromisoPagoCobranza(<?php echo $dt['tb_cuota_id']; ?>, <?php echo $dt['tb_credito_id']; ?>,'2')"><i class="fa fa-calendar"></i> Generar Compromiso</a></li>
                      <?php //} ?>
                      <li><a href="#" onclick="upload_cobranza_form('I',<?php echo $dt['tb_cobranza_id']; ?>)"><i class="fa fa-upload"></i> Subir Imagen</a></li>
                      <?php //if($fase_id==3){ ?>
                        <li><a href="#" onclick="prorrogaCuotaCobranza(<?php echo $dt['tb_cobranza_id']; ?>, <?php echo $dt['tb_cuota_id']; ?>, <?php echo $dt['tb_credito_id']; ?>,'2')"><i class="fa fa-calendar-plus-o"></i> Dar Prórroga</a></li>
                        <li><a href="#" onclick="remateGarantia1Cobranza(<?php echo $dt['tb_cuota_id']; ?>, <?php echo $dt['tb_credito_id']; ?>,'2')"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li>
                      <?php //} ?>
                      <!-- <li><a href="#" onclick="remateGarantia2Cobranza(<?php echo $dt['tb_credito_id']; ?>)"><i class="fa fa-mail-forward"></i> Pasar a Remate</a></li> -->
                    </ul>
                  </div>
                </td>
                <!-- <td id="tabla_fila">CM-<?php echo str_pad($dt['tb_credito_id'], 4, "0", STR_PAD_LEFT);?></td> -->
              </tr>
              <?php
            }
                ?>
          </tbody>
          <tfoot>
            <tr class="even">
              <td  id="tabla_fila"colspan="3"><strong>Sub Total</strong></td>
              <td id="tabla_fila"><strong><?php echo 'S/. '. mostrar_moneda($subtotal_soles); ?></strong></td>
              <td  id="tabla_fila"colspan="7"></td>
            </tr>
          </tfoot> <?php
        }
      ?>
    </table> <?php
  }
?>
<!-- FIN CREDITO MENOR-->