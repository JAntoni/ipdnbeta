<?php
date_default_timezone_set("America/Lima");
require_once('Cobranza.class.php');
$oCobranza = new Cobranza();
require_once('../usuario/Usuario.class.php');
$oUsuario = new Usuario();

$fecha_hoy = date('Y-m-d');
$primer_dia = date('01-m-Y');
$mes = date('m');
$anio = date('Y');
$numDays = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);

$lista_cuotas = $oCobranza->mostrarEstadoCuota($fecha_hoy);

$msj = '';
$i = 0;

if($lista_cuotas['estado'] == 1){

  foreach ($lista_cuotas['data'] as $key => $value) {
    
    if($value['tb_cuota_xac'] == 0){ // si la cuota ya se encuentra eliminada

      if($oCobranza->editar_dato_simple($value['tb_cobranza_id'], 'tb_cobranza_xac', 0)){ // se procede a eliminar el registro de cobranza
        $msj .= "<span>Crédito CM-".$value['tb_credito_id']." <strong>CUOTA ELIMINADA</strong>. Se procede a eliminar la COBRANZA de la cuota ".$value['tb_cuota_id']."</span><br>";
        $i++;
      }

    
    }
    if($value['estado_cuota'] != $value['estado_cobranza'] && $value['tb_credito_est'] == 3){ // si el estado de la cobranza es distinta al de la cuota

      // tb_cuota_est == 1 -> PENDIENTE
      // tb_cuota_est == 2 -> PAGADO
      // tb_cuota_est == 3 -> PAGO PARCIAL
      $estado = 0;
      if($value['estado_cuota'] == 1 || $value['estado_cuota'] == 3){
        $estado = 1; // tb_cobranza_est == 1 -> PENDIENTE
      }elseif($value['estado_cuota'] == 2){
        $estado = 2; // tb_cobranza_est == 2 -> PAGADO
      }

      if($oCobranza->editar_dato_simple($value['tb_cobranza_id'], 'tb_cobranza_estado', $estado)){ // se procede a actualizar el estado de la cobraza a que sea correspondiente con la cuota
        $msj .= "<span>Actualizar CM-".$value['tb_credito_id']." / cobranza ".$value['tb_cobranza_id']." con su respectivo estado de cuota ".$value['tb_cuota_id']." / estado: ".$estado."</span><br>";
        $i++;
      }
    
    }
    if($value['tb_credito_est'] == 4 || $value['tb_credito_est'] == 6 || $value['tb_credito_est'] == 7){ // si el credito fue 4=liquidado, 6=vendido, 7=liquidado pendiente de envio

      if($value['estado_cuota'] != 2){

        $detalle = '';
        if($value['tb_credito_est'] == 4){
          $detalle = 'LIQUIDADO';
        }elseif($value['tb_credito_est'] == 6){
          $detalle = 'VENDIDO';
        }elseif($value['tb_credito_est'] == 7){
          $detalle = 'LIQUIDADO PENDIENTE DE ENVÍO';
        }
        if($oCobranza->editar_dato_simple($value['tb_cobranza_id'], 'tb_cobranza_xac', 0)){ // se procede a eliminar el registro de COBRANZA
          $msj .= "<span>Crédito CM-".$value['tb_credito_id']." <strong>".$detalle."</strong>. Se procede a eliminar COBRANZA de la cuota ".$value['tb_cuota_id']."</span><br>";
          $i++;
        }

      }else{

        if($oCobranza->editar_dato_simple($value['tb_cobranza_id'], 'tb_cobranza_estado', 2)){ // se procede a actualizar el estado de la cobraza a que sea correspondiente con la cuota
          $msj .= "<span>Actualizar cobranza ".$value['tb_cobranza_id']." con su respectivo estado de cuota ".$value['tb_cuota_id']."</span><br>";
          $i++;
        }

      }

    }

    /* }elseif($value['tb_cuota_xac'] == 0 && $value['tb_cobranza_xac'] == 1){ // si la cuota está eliminada y la cobranzaz correspondiente no lo está

      if($oCobranza->editar_dato_simple($value['tb_cobranza_id'], 'tb_cobranza_xac', 0)){ // se procede a eliminar el registro de cuota
        $msj .= "<span>Crédito CM-".$value['tb_credito_id']." <strong>LIQUIDADO</strong>. Se procede a eliminar la cuota ".$value['tb_cuota_id']."</span><br>";
        $i++;
      }

    } */

  }

  if($i > 0){
    echo "Se modificaron un total de: ".$i." registros de cobranza";
    echo $msj;
  }else{
    echo "No se encontraron cuotas para ser modificadas";
  }

}else{
  echo "No se encontraron cuotas vencidas";
}

?>