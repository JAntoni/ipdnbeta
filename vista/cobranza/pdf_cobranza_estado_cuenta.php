<?php
session_name("ipdnsac");
session_start();

define('FPDF_FONTPATH','font/'); 
include('../../static/fpdf/fpdf.php');

require_once ("Cobranza.class.php");
$oCobranza = new Cobranza();
require_once ("../creditomenor/Creditomenor.class.php");
$oCreditomenor = new Creditomenor();
require_once ("../garantia/Garantia.class.php");
$oGarantia = new Garantia();
require_once ("../monedacambio/Monedacambio.class.php");
$oMonedacambio = new Monedacambio();
require_once('../funciones/funciones.php');
require_once('../funciones/fechas.php');

$credito_id = "";
if(isset($_GET['credito_id'])){
	$credito_id=$_GET['credito_id'];
}

$dts0 = $oCreditomenor->mostrarUno($credito_id);
$dts1 = $oGarantia->listar_garantias($credito_id);


$responsable = $dts0['data']['tb_cliente_nom'];
$fecha = mostrar_fecha($dts0['data']['tb_credito_feccre']);
$tipo_cuota = $dts0['data']['tb_cuotatipo_nom'];
$monto_prestado = $dts0['data']['tb_moneda_nom']." ".$dts0['data']['tb_credito_preaco'];
$moneda = $dts0['data']['tb_moneda_nom'];

$dts = $oCobranza->cobranza_estado_cuenta($credito_id);
$cant_cuota = count($dts['data']);

//


$pdf=new FPDF();
$pdf->AddPage();

$pdf->SetFont('Arial','B',14);
$pdf->SetFillColor(220,220,220);
$alto=7;
$pdf->Ln(8);
$pdf->Cell(190,5,utf8_decode("INVERSIONES Y PRÉSTAMOS DEL NORTE S.A.C."),0,1,'C');
$pdf->Ln();

$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,6,utf8_decode("ESTADO DE CUENTA"),0,1,'C');

$pdf->Ln();
$pdf->SetFont('Arial','B',9);
$pdf->Cell(190,6,utf8_decode("CLIENTE: ".$responsable),0,1,'L');
$pdf->Cell(190,6,utf8_decode("GARANTÍAS: "),0,1,'L');
$pdf->SetFont('Arial','B',7);
foreach ($dts1['data'] as $key => $v) {
    $pdf->Cell(190,6,utf8_decode("     - ".$v['tb_garantia_pro']),0,1,'L');
}
$pdf->SetFont('Arial','B',9);
$pdf->Cell(190,6,utf8_decode("FECHA: ".$fecha),0,1,'L');
$pdf->Cell(190,6,utf8_decode("TIPO CUOTA: ".$tipo_cuota),0,1,'L');
$pdf->Cell(190,6,utf8_decode("MONTO ACORDADO: ".$monto_prestado),0,1,'L');

$pdf->Ln();
$pdf->SetFont('Arial','B',8);
$pdf->Cell(5,$alto*2,'',0,0,'C');
$pdf->Cell(10,8,utf8_decode("#"),1,0,'C',1);
$pdf->Cell(25,8,utf8_decode("DESCRIPCIÓN"),1,0,'C',1);
$pdf->Cell(25,8,utf8_decode("FECHA VENC."),1,0,'C',1);
$pdf->Cell(25,8,utf8_decode("CUOTA"),1,0,'C',1);
$pdf->Cell(25,8,utf8_decode("TIP. CAMBIO"),1,0,'C',1);
$pdf->Cell(30,8,utf8_decode("MONTO PAGADO"),1,0,'C',1);
$pdf->Cell(40,8,utf8_decode("ESTADO"),1,0,'C',1);

$i=1;
$pdf->SetFont('Arial','',7);
$pdf->Ln();
$pag_anterior=$pdf->PageNo();


	/* VALORES */
    if($dts['estado'] == 1){
        $sum_total_credito = 0.00;
        $sum_total_pagado = 0.00;
        foreach ($dts['data'] as $key => $dt) {

            $suma_monto = 0.00;

            if($dt['tb_cuota_est']==1){
                $estado = 'PENDIENTE';
                $suma_monto = 0.00;
            }elseif($dt['tb_cuota_est']==2){
                $estado = 'PAGADO';
                $suma_monto = $dt['tb_cuota_cuo'];
            }elseif($dt['tb_cuota_est']==3){
                $estado = 'PAGO PARCIAL';

                $dts2 = $oCobranza->cobranza_monto_pagado($dt['tb_cuota_id']);
                foreach ($dts2['data'] as $key => $v): 
                    $tipo_cambio = 0.00;
                    $dt_mon = $oMonedacambio->consultar($v['tb_ingreso_fec']);
                    if($dt_mon['data']!=null || $dt_mon['data']!=''){
                        $tipo_cambio = $dt_mon['data']['tb_monedacambio_val'];
                    }

                    if($dt['tb_moneda_id']==1){ // soles
                        $suma_monto = $suma_monto + $v['tb_ingreso_imp'];
                    }else{ // dolares
                        $suma_monto = $suma_monto + $v['tb_ingreso_imp']*$tipo_cambio;
                    }
                endforeach;
            }

            $pdf->Cell(13,$alto*2,'',0,0,'C');

            if($pag_anterior!=$pdf->PageNo()){
                $pdf->SetFont('Arial','B',8);
                $pag_anterior=$pdf->PageNo();
                $pdf->Ln(12);
                $pdf->Cell(10,8,utf8_decode("#"),1,0,'C',1);
                $pdf->Cell(25,8,utf8_decode("DESCRIPCIÓN"),1,0,'C',1);
                $pdf->Cell(25,8,utf8_decode("FECHA VENC."),1,0,'C',1);
                $pdf->Cell(25,8,utf8_decode("CUOTA"),1,0,'C',1);
                $pdf->Cell(25,8,utf8_decode("TIP. CAMBIO"),1,0,'C',1);
                $pdf->Cell(30,8,utf8_decode("MONTO PAGADO"),1,0,'C',1);
                $pdf->Cell(40,8,utf8_decode("ESTADO"),1,0,'C',1);
                $pdf->Ln();
            }else{
                $pdf->Cell(-8,$alto,'',0,0,'C');
            }

            $pdf->SetFont('Arial','',7);

            $pdf->Cell(10,$alto,$dt['tb_cuota_num'],'T',0,'L');
            $pdf->Cell(25,$alto, 'Cuota '.$dt['tb_cuota_num'].' de '.$cant_cuota,'T',0,'L');
            $pdf->Cell(25,$alto, mostrar_fecha($dt['tb_cuota_fec']),'T',0,'L');
            $pdf->Cell(25,$alto, $dt['tb_moneda_nom'] .' '. mostrar_moneda($dt['tb_cuota_cuo']),'T',0,'R');
            $pdf->Cell(25,$alto, $dt['tb_moneda_nom'] .' '. mostrar_moneda($tipo_cambio),'T',0,'R');
            $pdf->Cell(30,$alto, $dt['tb_moneda_nom'] .' '. mostrar_moneda($suma_monto),'T',0,'R');
            $pdf->Cell(40,$alto, $estado,'T',0,'C');
            
            $sum_total_credito = $sum_total_credito + $dt['tb_cuota_cuo'];
            $sum_total_pagado = $sum_total_pagado + $suma_monto;

            /* $pdf->Cell(-220);
            $y1 = $pdf->GetY();
            $pdf->MultiCell(80,$alto,utf8_decode($dt['tb_garantia_pro']),'T','L',0);
            $y2 = $pdf->GetY();
            $alto1 = $y2-$y1;
            $pdf->Ln(-1*$alto1);
            $pdf->Cell(210);
            $pdf->MultiCell(60,$alto,utf8_decode($dt['tb_inventario_detalle_des']),'T','L',0);
            $y3 = $pdf->GetY();
            $alto2 = $y3 - $y1;

            if($alto1>$alto2){
                $pdf->Ln($alto1-$alto2);
            } */
            $pdf->Ln();

            $i++;

        }


        // TOTALES
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(5,$alto*2,'',0,0,'C');
        $pdf->Cell(10,$alto, '','T',0,'L');
        $pdf->Cell(25,$alto, '','T',0,'L');
        $pdf->Cell(25,$alto, '','T',0,'L');
        $pdf->Cell(25,$alto, $moneda .' '. mostrar_moneda($sum_total_credito),'T',0,'R');
        $pdf->Cell(25,$alto, '','T',0,'R');
        $pdf->Cell(30,$alto, $moneda .' '. mostrar_moneda($sum_total_pagado),'T',0,'R');
        $pdf->Cell(40,$alto, '','T',0,'C');

    }
	
$pdf->SetAutoPageBreak('auto',2); 
$pdf->SetDisplayMode(75);
$pdf->Output();
?>