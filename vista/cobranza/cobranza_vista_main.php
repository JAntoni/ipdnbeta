<?php
	
?>
<style>
    .content{
        display: flex;
        align-items: center;
        justify-content: center;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
        <h1>
            <?php echo $menu_tit; ?>
            <small><?php echo $menu_des; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
        <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
            <li class="active"><?php echo ucwords(mb_strtolower($menu_tit, 'UTF-8'));?></li>
        </ol>
	</section>

	<section class="content">
        <div class="row">
            <div align="center" class="col-md-6 p-3">
                <button type="button" class="btn btn-primary" onclick="visualizacionCobranza();">Visualización</button>
            </div>
            <div align="center" class="col-md-6 p-3">
                <button type="button" class="btn btn-primary" onclick="gestion_cobranza();">Gestión</button>
            </div>
        </div>
	</section>
</div>
