<?php
if (defined('VISTA_URL')) {
  require_once(APP_URL . 'core/usuario_sesion.php');
} else {
  require_once('../../core/usuario_sesion.php');
}
require_once('../plancontable/Plancontable.class.php');
$oPlancontable = new Plancontable();
require_once('../historial/Historial.class.php');
$oHist = new Historial();

$action = $_POST['action'];
$cuentaconta_cod = $_POST['tb_cuentaconta_cod'];
$cuentaconta_nom = strtoupper($_POST['txt_cuentaconta_nom']);
$cuentaconta_id = intval($_POST['hdd_cuentaconta_id']);
$usuario_id = intval($_SESSION['usuario_id']);

$oPlancontable->tb_cuentaconta_cod = $cuentaconta_cod;
$oPlancontable->tb_cuentaconta_usumod = $usuario_id;

$data['estado'] = 0;

if ($action == 'eliminar') {
  $data['mensaje'] = 'No se pudo eliminar la cuenta contable';

  if($oPlancontable->eliminar()){
    $data['estado'] = 1;
    $data['mensaje'] = 'Cuenta contable eliminada.';

    //insertar historial
    $oHist->setTbHistUsureg($usuario_id);
    $oHist->setTbHistNomTabla('tb_cuentaconta');
    $oHist->setTbHistRegmodid($cuentaconta_id);//debe ser numero entero
    $oHist->setTbHistDet('Eliminó la cuenta contable: '.$cuentaconta_cod .' - '.$cuentaconta_nom);
    $oHist->insertar();
  }
}
else{
  $array[0]["column_name"] = 'cc.tb_cuentaconta_cod';
  $array[0]["param0"] = $cuentaconta_cod;
  $array[0]["datatype"] = 'STR';
  $otros["group"]["column_name"] = "cc.tb_cuentaconta_id";
  $result = $oPlancontable->listar_todos($array, array(), $otros);

  $existe = $result['estado'];
  $registro_coincide = $result['data'][0];
  $evaluar0 = $cuentaconta_id == intval($registro_coincide['tb_cuentaconta_id']);

  $detalle = $_POST['txt_detalle'] == '' ? null : strtoupper($_POST['txt_detalle']);
  $xac_no_conta = intval($_POST['rad_cuentaconta_xac_noconta']);

  $oPlancontable->tb_cuentaconta_nom = $cuentaconta_nom;
  $oPlancontable->tb_cuentaconta_detalle = $detalle;
  if(strlen($cuentaconta_cod) > 2){
    $cuentaconta_idfk = intval($_POST['hdd_ccpadre_id']);
    $oPlancontable->tb_cuentaconta_idfk = $cuentaconta_idfk;
  }
  $oPlancontable->tb_cuentaconta_xac_noconta = $xac_no_conta;
  
  if(strpos($action,'_verificar_existe') && !$evaluar0){
    $data['estado'] = $existe;
    $data['mensaje'] = 'La Cuenta contable ya está registrada: ' . $cuentaconta_cod . ' - ' . $registro_coincide['tb_cuentaconta_nom'];
    if($existe > 0){
      echo json_encode($data);
		  exit();
    }
  }
  elseif ($action == 'modificar') {
    $registro_origin = (array) json_decode($_POST['hdd_cuentaconta_registro']);
    $padre_cod_length = intval($registro_origin["ccpadre_codlength"]);

    if(($padre_cod_length < 1 && strlen($cuentaconta_cod)!= 2) || ($padre_cod_length > 1 && (strlen($cuentaconta_cod)<=$padre_cod_length || strlen($cuentaconta_cod)>$padre_cod_length+2))){
      $mensaje_length = "La cantidad de caracteres del codigo de cuenta debe ser ";
      if($padre_cod_length < 1){
        $mensaje_length = "como mínimo 2";
      } else{
        $mensaje_length = "entre ". ($padre_cod_length+1) ." y ". ($padre_cod_length+2);
      }
      $data['mensaje'] = $mensaje_length;
      echo json_encode($data);
      exit();
    }
    
    $evaluar[0] = $cuentaconta_cod == $registro_origin['tb_cuentaconta_cod'];
    $evaluar[1] = $cuentaconta_nom == $registro_origin['tb_cuentaconta_nom'];
    $evaluar[2] = $detalle == $registro_origin['tb_cuentaconta_detalle'];
    $evaluar[3] = $xac_no_conta == intval($registro_origin['tb_cuentaconta_xac_noconta']);
    if(strlen($cuentaconta_cod) > 2){
      $evaluar[4] = $cuentaconta_idfk == intval($registro_origin['tb_cuentaconta_idfk']);
    }

    $son_iguales = true;
    $i = 0;
    while ($i < count($evaluar)) {
      if (!$evaluar[$i]) {
        $son_iguales = false;
      }
      $i++;
    }
    if ($son_iguales) {
      $data['estado'] = 2;
      $data['mensaje'] = "No se realizó ninguna modificacion";
    }
    else if ($existe == 0 || ($existe == 1 && $evaluar0)) {
      $oPlancontable->tb_cuentaconta_id = $cuentaconta_id;

      $data['estado'] = intval($oPlancontable->modificar());
      $mensaje = $data['estado'] == 1 ? 'Se modificó correctamente' : 'Hubo un error en la modificación';
      $data['mensaje'] = $mensaje;

      if ($data['estado'] == 1) {
        $ARRAY[0] = 'NO VISIBLE';
				$ARRAY[1] = 'VISIBLE';
        //insertar historial
        $oHist->setTbHistUsureg($usuario_id);
        $oHist->setTbHistNomTabla('tb_cuentaconta');
        $oHist->setTbHistRegmodid($cuentaconta_id); //debe ser numero entero
        $mensaje = 'Modificó la cuenta contable con id: ' . $cuentaconta_id . ':';
        if(strlen($cuentaconta_cod) > 2 && !$evaluar[4]){
          $mensaje .= '<br> - Cambió la cuenta contable padre: ' . "{$registro_origin['ccpadre_cod']} - {$registro_origin['ccpadre_nom']}" . ' => <b>' . "{$_POST['txt_cuentaconta_codfk']} - ". strtoupper($_POST['txt_cuentaconta_nomfk']) . '</b>';
        }
        if (!$evaluar[0])
          $mensaje .= '<br> - Cambió el código de la cuenta: ' . $registro_origin['tb_cuentaconta_cod'] . ' => <b>' . $cuentaconta_cod . '</b>';
        if (!$evaluar[1])
          $mensaje .= '<br> - Cambió el nombre de la cuenta: ' . $registro_origin['tb_cuentaconta_nom'] . ' => <b>' . $cuentaconta_nom . '</b>';
        if (!$evaluar[2])
          $mensaje .= "<br> - Cambió el comentario: \"" . $registro_origin['tb_cuentaconta_detalle'] . '" => <b>"' . $detalle . '"</b>';
        if (!$evaluar[3])
          $mensaje .= '<br> - Cambió la opción visible a otras áreas: ' . $ARRAY[intval($registro_origin['tb_cuentaconta_xac_noconta'])] . ' => <b>' . $ARRAY[$xac_no_conta] . '</b>';
        $oHist->setTbHistDet($mensaje);
        $oHist->insertar();
      }
    }
    else{
      $data['mensaje'] = "Los datos que ingresó ya están registrados, en la cuenta contable con id: {$registro_coincide['tb_cuentaconta_id']}. Codigo: {$registro_coincide['tb_cuentaconta_cod']}. Nombre: {$registro_coincide['tb_cuentaconta_nom']}";
    }
  }
  elseif($action == 'insertar'){
    if ($existe == 0) {
      $oPlancontable->tb_cuentaconta_usureg = $usuario_id;
      $res = $oPlancontable->insertar();

      if ($res['estado'] == 1) {
        $data['estado'] = 1;
        $data['mensaje'] = $res['mensaje'] . '. Su ID: ' . $res['nuevo'];
        $data['nuevo'] = $res['nuevo'];

        //insertar historial
        $oHist->setTbHistUsureg($usuario_id);
        $oHist->setTbHistNomTabla('tb_cuentaconta');
        $oHist->setTbHistRegmodid(intval($data['nuevo']));
        $oHist->setTbHistDet("Registró la nueva cuenta contable. ID: {$data['nuevo']}. Codigo: ". strtoupper($cuentaconta_cod) .". Nombre: $cuentaconta_nom");
        $oHist->insertar();
      }
    } else {
      $data['mensaje'] = "La cuenta contable con codigo \"$cuentaconta_cod\" ya está registrada, con id: {$registro_coincide['tb_cuentaconta_id']}, y nombre: \"{$registro_coincide['tb_cuentaconta_nom']}\"";
    }
  }
  else {
    $data['estado'] = 0;
    $data['mensaje'] = 'Accion no identificada para: ' . $action;  
  }
}
echo json_encode($data);
//$data["mensaje"] = var_export($result); $data["estado"]=1;echo json_encode($data);exit();
//$data["mensaje"] = "$cuentaconta_id || ". intval($registro_coincide['tb_cuentaconta_id']); $data["estado"]=1;echo json_encode($data);exit();