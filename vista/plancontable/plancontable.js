var datatable_global;

function plancontable_form(usuario_act, id){
  $.ajax({
		type: "POST",
		url: VISTA_URL+"plancontable/plancontable_form.php",
    async: true,
		dataType: "html",
		data: ({
      action: usuario_act, // PUEDE SER: L, I, M , E
      id: id
    }),
		beforeSend: function() {
      $('#h3_modal_title').text('Cargando Formulario');
      $('#modal_mensaje').modal('show');
		},
		success: function(data){
      $('#modal_mensaje').modal('hide');
      if(data != 'sin_datos'){
      	$('#div_modal_plancontable_form').html(data);
      	$('#modal_registro_plancontable').modal('show');

        //desabilitar elementos del form si es L (LEER)
        if(usuario_act == 'L' || usuario_act == 'E'){
          disabled($('#form_plancontable').find("button[class*='btn-sm']")); //attr('disabled', 'disabled') // aqui esta el error
          disabled($('#form_plancontable').find("select"));
          disabled($('#form_plancontable').find("input, textarea"));
          disabled($('#form_plancontable').find("checkbox"));
          disabled($('#form_plancontable').find("a"));
          $('#form_plancontable').find("input").css("cursor", "text");
        }
      }
      else{
      	//llamar al formulario de solicitar permiso
      	var modulo = 'plancontable';
      	var div = 'div_modal_plancontable_form';
      	permiso_solicitud(usuario_act, plancontable_id, modulo, div); //funcion ubicada en public/js/permiso.js
      }
      modal_hidden_bs_modal('modal_registro_plancontable', 'limpiar'); //funcion encontrada en public/js/generales.js
      modal_width_auto('modal_registro_plancontable', 50);
      modal_height_auto('modal_registro_plancontable');
		},
		complete: function(data){
		},
		error: function(data){
      $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
      $('#overlay_modal_mensaje').removeClass('overlay').empty();
      $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
			console.log(data.responseText);
		}
	});
}

function plancontable_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"plancontable/plancontable_tabla.php",
    async: true,
    dataType: "html",
    data: $('#form_plancontable_filtro').serialize(),
    beforeSend: function() {
      $('#plancontable_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_plancontable_tabla').html(data);
      $('#plancontable_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    error: function(data){
      $('#plancontable_mensaje_tbl').html('ERROR AL LISTAR LOS MENÚS: ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_plancontables').DataTable({
    "pageLength": 50,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Página mostrada _PAGE_ de _PAGES_",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [3,4], orderable: false }
    ]
  });

  datatable_texto_filtrar();
}

function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}

$(document).ready(function() {
  $('#txt_fil_cuentaconta').keyup(function(){
    if($('#txt_fil_cuentaconta').val() === ''){
      $('#hdd_fil_cuentaconta_selected').val(null);
      $('#cbo_tipocuentacont_id').val(0).prop(disabled, false);
      $('#cbo_tipocuentacont_id').removeAttr('disabled');
    }
  });
  
  //autocompletar del planconta
	$("#txt_fil_cuentaconta").autocomplete({
		minLength: 1,
    source: function (request, response) {
      $.getJSON(
        VISTA_URL + "plancontable/plancontable_autocomplete.php",
        { term: request.term },
        response
      );
    },
		select: function (event, ui) {
      $('#txt_fil_cuentaconta').val(ui.item.value);
			$('#hdd_fil_cuentaconta_selected').val(JSON.stringify(ui.item));
      $('#cbo_tipocuentacont_id').val(parseInt(ui.item.tb_cuentaconta_cod.substring(0, 1)));
      disabled($('#cbo_tipocuentacont_id'));
			event.preventDefault();
		}
	});
});

function plancontable_historial_form(cuentacontable_id, tabla_nom) {
  $.ajax({
      type: "POST",
      url: VISTA_URL + "plancontable/plancontable_historial_form.php",
      async: true,
      dataType: "html",
      data: ({
        cuentacontable_id:cuentacontable_id,
        tabla_nom : tabla_nom
      }),
      beforeSend: function () {
          $('#h3_modal_title').text('Cargando Formulario');
          $('#modal_mensaje').modal('show');
      },
      success: function (html) {
          $('#div_plancontable_historial_form').html(html);
          $('#div_modal_plancontable_historial_form').modal('show');
          $('#modal_mensaje').modal('hide');

          //funcion js para limbiar el modal al cerrarlo
          modal_hidden_bs_modal('div_modal_plancontable_historial_form', 'limpiar'); //funcion encontrada en public/js/generales.js
          //funcion js para agregar un largo automatico al modal, al abrirlo
          modal_height_auto('div_modal_plancontable_historial_form'); //funcion encontrada en public/js/generales.js

      },
      complete: function (html) {
      }
  });
}