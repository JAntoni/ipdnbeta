<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $menu_tit; ?>
			<small><?php echo $menu_des; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Mantenimiento</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>Usuarios</a></li>
			<li class="active"><?php echo ucwords(mb_strtolower($menu_tit)); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box box-primary">
			<div class="box-header">
				<div class="row">
					<div class="col-md-1" style="width: 1%;"></div>
					<div name="btn_nuevo" id="btn_nuevo" style="width: 6.5%;" class="col-md-1">
						<p></p>
						<label for="btn_nuevo" class="control-label" style="color: white;">Boton</label>
						<button class="btn btn-primary btn-sm" onclick="plancontable_form('I',0)"><i class="fa fa-plus"></i> Nuevo</button>
					</div>
					<div class="col-md-10" style="width: 91%;">
						<label for="txt_filtro" class="control-label"> &nbsp; FILTROS (*Si desea generar el listado de todo el plan contable, no añada filtros, y presione el botón de búsqueda)</label>
						<div class="panel panel-danger">
							<div class="panel-body">
								<?php include VISTA_URL . 'plancontable/plancontable_filtro.php'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<!--- MESAJES DE GUARDADO -->
				<div class="callout callout-info" id="plancontable_mensaje_tbl" style="display: none;">
					<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Cargando datos...</h4>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<!-- Input para guardar el valor ingresado en el search de la tabla-->
						<input type="hidden" id="hdd_datatable_fil">

						<div id="div_plancontable_tabla" class="table-responsive dataTables_wrapper form-inline dt-bootstrap">
						</div>
					</div>
				</div>
			</div>
			<div id="div_modal_plancontable_form">
				<!-- INCLUIMOS EL MODAL PARA EL REGISTRO DEL TIPO-->
			</div>
			<div id="div_plancontable_historial_form">
				<!-- INCLUIMOS EL MODAL PARA EL PROVEEDOR-->
			</div>
			<!-- INCLUIMOS EL MODAL DE CARGANDO DATOS-->
			<?php require_once(VISTA_URL . 'templates/modal_mensaje.php'); ?>
		</div>
	</section>
	<!-- /.content -->
</div>