<?php
require_once('../funciones/fechas.php');
require_once("Plancontable.class.php");
$oPlan = new Plancontable();

//defino una clase que voy a utilizar para generar los elementos sugeridos en autocompletar
class ElementoAutocompletar{
  var $id;
  var $tb_cuentaconta_cod;
  var $tb_cuentaconta_nom;
  var $tb_cuentaconta_idfk;
  var $value;
  var $label;

  function __construct($id, $tb_cuentaconta_cod, $tb_cuentaconta_nom, $tb_cuentaconta_idfk, $value, $label){
    $this->id = $id;
    $this->tb_cuentaconta_cod = $tb_cuentaconta_cod;
    $this->tb_cuentaconta_nom = $tb_cuentaconta_nom;
    $this->tb_cuentaconta_idfk = $tb_cuentaconta_idfk;
    $this->value = $value;
    $this->label = $label;
  }
}

//recibo el dato que deseo buscar sugerencias
$datoBuscar = $_GET["term"];
$datoBuscar = mb_strtoupper($datoBuscar, 'UTF-8');

$array[0]["column_name"] = "cc.tb_cuentaconta_xac_conta";
$array[0]["param0"] = 1;
$array[0]["datatype"] = "INT";

$array[1]["conector"] = "AND";
$array[1]["column_name"] = "(cc.tb_cuentaconta_cod";
$array[1]["param1"] = "LIKE '$datoBuscar%'";

$array[2]["conector"] = "OR";
$array[2]["column_name"] = "cc.tb_cuentaconta_nom";
$array[2]["param2"] = "LIKE '$datoBuscar%')";

$otros["group"]["column_name"] = "cc.tb_cuentaconta_id";
$otros["orden"]["column_name"] = "cc.tb_cuentaconta_cod";
$otros["orden"]["value"] = "ASC";
$otros["limit"]["value"] = 25;

//busco un valor aproximado al dato escrito
$result = $oPlan->listar_todos($array, array(), $otros);

//creo el array de los elementos sugeridos
$arrayElementos = array();

foreach ($result['data'] as $key => $value) {
  array_push(
    $arrayElementos,
    new ElementoAutocompletar(
      $value["tb_cuentaconta_id"],
      $value["tb_cuentaconta_cod"],
      $value["tb_cuentaconta_nom"],
      $value["tb_cuentaconta_idfk"],
      $value["tb_cuentaconta_cod"] . '-' . $value["tb_cuentaconta_nom"],
      $value["tb_cuentaconta_cod"] . '-' . $value["tb_cuentaconta_nom"]
    )
  );
}
print_r(json_encode($arrayElementos));
$result = NULL;
?>
