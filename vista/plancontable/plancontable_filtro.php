<form id="form_plancontable_filtro" class="form-inline" role="form">
  <div class="form-group">
    <label for="cbo_tipocuentacont_id">Elemento: </label>
    <select name="cbo_tipocuentacont_id" id="cbo_tipocuentacont_id" class="form-control input-sm mayus" onchange="plancontable_tabla()">
      <?php require_once(VISTA_URL.'tipocuentacont/tipocuentacont_select.php');?>
    </select>
  </div>
  &nbsp;
  <div class="form-group">
    <label for="txt_fil_cuentaconta">Cuenta Contable: </label>
    <input type="text" name="txt_fil_cuentaconta" id="txt_fil_cuentaconta" placeholder="Escribe para buscar cuenta contable" class="form-control mayus input-sm ui-autocomplete-input" autocomplete="off" size="48" value>
    <input type="hidden" name="hdd_fil_cuentaconta_selected" id="hdd_fil_cuentaconta_selected" value="<?php echo $cuenta_selected;?>">
  </div>
  &nbsp;
  <button type="button" class="btn btn-info btn-sm" onclick="plancontable_tabla()"><i class="fa fa-search"></i></button>
</form>