<?php
session_name("ipdnsac");
session_start();
require_once('../perfilmenu/PerfilMenu.class.php');
$oPerfilMenu = new PerfilMenu();
require_once('../permiso/Permiso.class.php');
$oPermiso = new Permiso();
require_once('../plancontable/Plancontable.class.php');
$oPlancontable = new Plancontable();
require_once('../funciones/funciones.php');

$direc = 'plancontable';
$usuarioperfil_id = $_SESSION['usuarioperfil_id'];
$usuario_action = $_POST['action']; //tipo de accion que desea hacer el usuario en este directorio: L(leer), I(insertar), M(modificar), E(eliminar)
$id = intval($_POST['id']);

$titulo = '';
if ($usuario_action == 'L')
  $titulo = 'Cuenta Contable Registrada';
elseif ($usuario_action == 'I')
  $titulo = 'Registrar Cuenta Contable';
elseif ($usuario_action == 'M')
  $titulo = 'Editar Cuenta Contable';
elseif ($usuario_action == 'E')
  $titulo = 'Eliminar Cuenta Contable';
else
  $titulo = 'Acción de Usuario Desconocido';

$action = devuelve_nombre_usuario_action($usuario_action); //funcion encontrada en funciones/funciones.php

//antes de mostrar el formulario, tenemos que consultar si el usuario tiene permisos al tipo de accion que desea hacer en plancontable
$result = $oPerfilMenu->accesso_directorio_perfilmenu($direc, $usuarioperfil_id);
$bandera = 0;
$mensaje = '';

if ($result['estado'] == 1) {
  foreach ($result['data'] as $key => $value) {
    $permisos = $value['tb_perfilmenu_per']; //array de permisos para el directorio separados por - (L-I-M-E)
  }

  $result = NULL;
  $array_permisos = explode('-', $permisos);

  if (in_array($usuario_action, $array_permisos))
    $bandera = 1;

  //si no tiene ningún permiso, verificamos si se ha solicitado un permiso previo y el estado del mismo, la tabla_id puede ser 0 ya que solo se puede requerir insertar
  if ($bandera == 0) {
    $usuario_id = $_SESSION['usuario_id'];
    $modulo = 'plancontable';
    $modulo_id = $cuentaconta_id;
    $tipo_permiso = $usuario_action;
    $estado = 1;

    $result = $oPermiso->verificar_permiso_usuario_tabla_tipo($usuario_id, $modulo, $modulo_id, $tipo_permiso, $estado);
    if ($result['estado'] == 1) {
      $bandera = 1; // si tiene el permiso para la accion que desea hacer
    } else {
      $result = NULL;
      echo 'sin_datos'; // el usuario no tiene permisos para su transaccion o no le han aprobado su solicitud
      exit();
    }
    $result = NULL;
  }

  $visible = '';
  $novisible = 'checked';
  $cant_subcuentas = 0;
  //si la accion es modificar, mostramos los datos del plancontable por su ID
  if ($id > 0) {
    $array[0]["column_name"] = "cc.tb_cuentaconta_id";
    $array[0]["param0"] = $id;
    $array[0]["datatype"] = "INT";

    $inner[0]['tipo_union'] = 'LEFT';
    $inner[0]['tabla_alias'] = '(SELECT * FROM tb_cuentaconta) as ccpadre';
    $inner[0]['columna_enlace'] = 'cc.tb_cuentaconta_idfk';
    $inner[0]['alias_columnaPK'] = 'ccpadre.tb_cuentaconta_id';
    $inner[0]['alias_columnasparaver'] = 'ccpadre.tb_cuentaconta_cod as ccpadre_cod, ccpadre.tb_cuentaconta_nom as ccpadre_nom, CHAR_LENGTH(ccpadre.tb_cuentaconta_cod) as ccpadre_codlength';

    $otros["group"]["column_name"] = "cc.tb_cuentaconta_id";
    $result = $oPlancontable->listar_todos($array, $inner, $otros)["data"][0];
    
    $ccpadre_id = intval($result["tb_cuentaconta_idfk"]);
    if (!empty($ccpadre_id)){
      $cuentaconta_codfk = $result['ccpadre_cod'];
      $cuentaconta_nomfk = $result['ccpadre_nom'];
    }
    $cuentaconta_id = $result['tb_cuentaconta_id'];
    $cuentaconta_cod = $result['tb_cuentaconta_cod'];
    $cuentaconta_nom = $result['tb_cuentaconta_nom'];
    $detalle = $result['tb_cuentaconta_detalle'];
    $visible_usuarios_noconta = intval($result['tb_cuentaconta_xac_noconta']);
    if ($visible_usuarios_noconta == 1) {
      $visible = 'checked';
      $novisible = '';
    }
    $cant_subcuentas = intval($result['cant_subcuentas']);
  }
} else {
  $mensaje =  $result['mensaje'];
  $bandera = 4;
  $result = NULL;
}
?>
<?php if ($bandera == 1) : ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modal_registro_plancontable" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title mayus"><?php echo $titulo; ?></h4>
        </div>
        <form id="form_plancontable" method="post">
          <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
          <input type="hidden" id="hdd_cuentaconta_registro" name="hdd_cuentaconta_registro" value='<?php echo json_encode($result) ?>'>

          <div class="modal-body">
              <?php if ($action == 'modificar')
                echo '<label style="color: green; font-size: 12px">- No se podrán modificar los campos bloqueados.<br></label>' ?>
            <div class="box box-primary shadow">
              <div class="row form-group" <?php echo (strlen($cuentaconta_cod) < 3 && $action !== 'insertar') ? 'hidden' : ''; ?>>
                <input type="hidden" name="hdd_ccpadre_id" id="hdd_ccpadre_id" value="<?php echo $ccpadre_id;?>">
                <div class="col-md-3">
                  <label for="txt_cuentaconta_codfk">COD. CTA. CONTABLE PADRE:</label><br>
                  <input type="text" name="txt_cuentaconta_codfk" id="txt_cuentaconta_codfk" placeholder="cuenta contable padre" class="form-control input-sm mayus ui-autocomplete-input" autocomplete="off" value="<?php echo $cuentaconta_codfk; ?>">
                </div>

                <div class="col-md-8">
                  <label for="txt_cuentaconta_nomfk" class="control-label">NOMBRE CTA. CONTABLE PADRE:</label>
                  <input type="text" name="txt_cuentaconta_nomfk" id="txt_cuentaconta_nomfk" placeholder="cuenta contable padre" class="form-control mayus input-sm ui-autocomplete-input" autocomplete="off" value="<?php echo $cuentaconta_nomfk; ?>">
                </div>

                <div class="col-md-1">
                  <label for="btn-limpiar-1" style="color:white;">l</label>
                  <span class="input-group-btn">
                    <button id="btn-limpiar-1" class="btn btn-primary btn-sm" type="button" onclick="limpiar_cuentaconta_padre()" title="LIMPIAR CTA. CONTABLE PADRE">
                      <span class="fa fa-eraser icon"></span>
                    </button>
                  </span>
                </div>
              </div>

              <div class="row form-group">
                <input type="hidden" name="hdd_cuentaconta_id" id="hdd_cuentaconta_id" value="<?php echo $cuentaconta_id; ?>">
                <input type="hidden" name="hdd_cant_subcuentas" id="hdd_cant_subcuentas" value="<?php echo $cant_subcuentas;?>">
                <div class="col-md-3">
                  <label for="tb_cuentaconta_cod">COD. CTA. CONTABLE:</label><br>
                  <input type="text" name="tb_cuentaconta_cod" id="tb_cuentaconta_cod" placeholder="Codigo de cuenta contable" class="form-control input-sm mayus ui-autocomplete-input" autocomplete="off" value="<?php echo $cuentaconta_cod; ?>">
                </div>
                <div class="col-md-8">
                  <label for="txt_cuentaconta_nom" class="control-label">NOMBRE CTA. CONTABLE:</label>
                  <input type="text" name="txt_cuentaconta_nom" id="txt_cuentaconta_nom" placeholder="Nombre de cuenta contable" class="form-control input-sm mayus" value="<?php echo $cuentaconta_nom; ?>">
                </div>
              </div>
              <div class="row">
                <div class="col-md-11">
                  <div class="form-group">
                    <label for="txt_detalle" class="control-label">COMENTARIO:</label>
                    <textarea class="form-control input-sm mayus" name="txt_detalle" id="txt_detalle" placeholder="INSERTE UN COMENTARIO"><?php echo $detalle ?></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="rad_cuentaconta_xac_noconta" class="control-label">VISIBLE A OTRAS AREAS:</label>
                    <div class="radio">
                      <label><input type="radio" name="rad_cuentaconta_xac_noconta" class="flat-green" value="1" <?php echo $visible; ?>> Visible</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="rad_cuentaconta_xac_noconta" class="flat-green" value="0" <?php echo $novisible; ?>> No visible</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!--- MESAJES DE ALERTA AL ELIMINAR -->
            <?php if ($action == 'eliminar') : ?>
              <div class="callout callout-warning">
                <h4><i class="icon fa fa-warning"></i> ¿Está seguro de que desea eliminar esta Cuenta Contable?</h4>
              </div>
            <?php endif; ?>

            <!--- MESAJES DE GUARDADO -->
            <div class="callout callout-info" id="plancontable_mensaje" style="display: none;">
              <h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>
            </div>
          </div>
          <div class="modal-footer">
            <div class="f1-buttons">
              <?php if ($usuario_action == 'I' || $usuario_action == 'M') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_plancontable">Guardar</button>
              <?php endif; ?>
              <?php if ($usuario_action == 'E') : ?>
                <button type="submit" class="btn btn-info" id="btn_guardar_plancontable">Aceptar</button>
              <?php endif; ?>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ($bandera == 4) : ?>
  <div class="modal modal-danger" tabindex="-1" role="dialog" id="modal_registro_plancontable">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
          <h4 class="modal-title">Mensaje Importante</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <p><?php echo $mensaje; ?></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/javascript" src="<?php echo 'vista/plancontable/plancontable_form.js?ver=240423'; ?>"></script>