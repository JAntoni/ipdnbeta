<?php
if (defined('VISTA_URL')) {
    require_once(APP_URL . 'core/usuario_sesion.php');
    require_once(VISTA_URL . 'plancontable/Plancontable.class.php');
    require_once(VISTA_URL . 'funciones/fechas.php');
} else {
    require_once('../../core/usuario_sesion.php');
    require_once('../plancontable/Plancontable.class.php');
    require_once('../funciones/fechas.php');
}
$oPlancontable = new Plancontable();
$tr = '';

$cuenta_selected = (array) json_decode($_POST['hdd_fil_cuentaconta_selected']);
$elemento = intval($_POST["cbo_tipocuentacont_id"]);

$array[0]["column_name"] = "cc.tb_cuentaconta_xac_conta";
$array[0]["param0"] = 1;
$array[0]["datatype"] = "INT";

if(!empty($cuenta_selected)){
    $array[1]["conector"] = "AND";
    $array[1]["column_name"] = "cc.tb_cuentaconta_cod";
    $array[1]["param1"] = "LIKE '{$cuenta_selected["tb_cuentaconta_cod"]}%'";

    if(!empty($elemento)){
        $array[2]["conector"] = "AND";
        $array[2]["column_name"] = "cc.tb_tipocuentacont_idfk";
        $array[2]["param2"] = $elemento;
        $array[2]["datatype"] = "INT";
    }
} elseif (!empty($elemento)) {
    $array[1]["conector"] = "AND";
    $array[1]["column_name"] = "cc.tb_tipocuentacont_idfk";
    $array[1]["param1"] = $elemento;
    $array[1]["datatype"] = "INT";
}

$otros["group"]["column_name"] = "cc.tb_cuentaconta_id";
$otros["orden"]["column_name"] = "cc.tb_cuentaconta_cod";
$otros["orden"]["value"] = "ASC";


$result = $oPlancontable->listar_todos($array, array(), $otros);

if ($result['estado'] == 1) {
    foreach ($result['data'] as $key => $value) {
        do_tr($value);
    }
    $result = null;
}
else {
    echo $result['mensaje'];
    $result = null;
    exit();
}

function do_tr($value){
    global $tr;
    $caret = 'fa-angle-right';
    $arreglo[0]= 'NO';
    $arreglo[1]= 'SI';
    $offset = '' ;
    $opcion_eliminar = '';
    if ($value['cant_subcuentas'] > 0) {
        $caret = 'fa-chevron-circle-down';
    }
    else{
        $opcion_eliminar = '<a class="btn btn-danger btn-xs" title="Eliminar"' . " onclick='plancontable_form(\"E\",".$value['tb_cuentaconta_id']. ")'>".'<i class="fa fa-trash"></i></a>';
    }
    $tr .= '<tr id="tabla_cabecera_fila">'.
                '<td id="tabla_fila" align="left">' . $value['tb_cuentaconta_cod'] . '</td>'.
                '<td id="tabla_fila" align="left"><div '.$offset.'>'. '<span class="fa fa-fw ' . $caret . '"></span>' . $value['tb_cuentaconta_nom'] . '</div></td>'.
                '<td id="tabla_fila">' . $value['cant_subcuentas'] . '</td>'.
                '<td id="tabla_fila">' . $arreglo[ intval( $value[ 'tb_cuentaconta_xac_noconta'] ) ] . '</td>'.
                '<td id="tabla_fila">'.
                    '<button class="btn btn-info btn-xs" title="Ver"' . " onclick='plancontable_form(\"L\",".$value['tb_cuentaconta_id']. ")'>".'<i class="fa fa-eye"></i></button>
                    <button class="btn btn-warning btn-xs" title="Editar"' . " onclick='plancontable_form(\"M\",".$value['tb_cuentaconta_id']. ")'>".'<i class="fa fa-edit"></i></button>
                    '.$opcion_eliminar.'
                    <button class="btn btn-primary btn-xs" title="Historial" onclick="plancontable_historial_form(' . $value['tb_cuentaconta_id'] . ', \'tb_cuentaconta\')"><i class="fa fa-history"></i></button>'.
                '</td>'.
            '</tr>';
}
?>
<table id="tbl_plancontables" class="table table-hover" style="word-wrap:break-word;">
    <thead>
        <tr id="tabla_cabecera">
            <th id="tabla_cabecera_fila" style="width: 8.5%;">Cod. Cuenta</th>
            <th id="tabla_cabecera_fila" style="width: 64%;">Nombre</th>
            <th id="tabla_cabecera_fila" style="width: 7.5%;"># Subcuentas</th>
            <th id="tabla_cabecera_fila" style="width: 8%;">Visible Otras áreas</th>
            <th id="tabla_cabecera_fila" style="width: 12%;">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $tr; ?>
    </tbody>
</table>