<?php
	if (defined('VISTA_URL')){
		require_once(APP_URL.'core/usuario_sesion.php');
	} else {
		require_once('../../core/usuario_sesion.php');
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- TODOS LOS ESTILOS-->
		<?php include(VISTA_URL.'templates/head.php'); ?>
		<title><?php echo ucwords(mb_strtolower($menu_tit));?></title>
		<style>
			.iradio_flat-green.checked.disabled {
				background-position: -110px 0;
				opacity: 0.7;
			}
		</style>
	</head>
	<body <?php echo 'class="'.CLASE_BODY.' '.$usuario_tem.'"'; ?>>
		<div class="wrapper">
			<!-- INCLUIR EL HEADER-->
				<?php include(VISTA_URL.'templates/header.php'); ?>
			<!-- INCLUIR ASIDE, MENU LATERAL -->
				<?php include(VISTA_URL.'templates/aside.php'); ?>
			<!-- INCLUIR EL CONTENIDO, TABLAS, VISTA DEL DORECTORIO-->
				<?php include('plancontable_vista.php');?>
			<!-- INCLUIR FOOTER-->
				<?php include(VISTA_URL.'templates/footer.php'); ?>
			<div class="control-sidebar-bg"></div>
		</div>

		<!-- TODOS LOS SCRIPTS-->
		<?php include(VISTA_URL.'templates/script.php'); ?>
		
		<script type="text/javascript" src="<?php echo VISTA_URL.'plancontable/plancontable.js?ver=489314';?>"></script>
	</body>
</html>

