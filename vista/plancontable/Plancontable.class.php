<?php
if (defined('APP_URL'))
  require_once(APP_URL . 'datos/conexion.php');
else
  require_once('../../datos/conexion.php');

class Plancontable extends Conexion{
    public $tb_cuentaconta_id;
    public $tb_cuentaconta_cod;
    public $tb_cuentaconta_nom;
    public $tb_cuentaconta_detalle;
    public $tb_cuentaconta_idfk;
    public $tb_tipocuentacont_idfk;
    public $tb_tipocuentacont_codfk;
    public $tb_cuentaconta_fecreg;
    public $tb_cuentaconta_usureg;
    public $tb_cuentaconta_fecmod;
    public $tb_cuentaconta_usumod;
    public $tb_cuentaconta_xac_conta;
    public $tb_cuentaconta_xac_noconta;

    public function listar_todos($array, $array1, $otros_param){
        //array: bidimensional con formato $column_name, $param.$i, $datatype, $conector (and o or) cuando sea mayor que cero
        //array1: $iner o left, $nombretb alias, $columna_deenlace, $alias.columnaPK, $alias_columnasparaver
        //otros_param: arreglo para funciones diversas como el order by, limit, etc. los keys se definen de acuerdo al uso (ejm: ordenar: ["orden"]["column_name"], ["orden"]["value"])
        try {
            $sql = "SELECT cc.*, SUM(IF(subcc.tb_cuentaconta_xac_conta = 1, 1, 0)) as cant_subcuentas";
            if (!empty($array1)) {
                $sql .= ",\n";
                for ($j = 0; $j < count($array1); $j++) {
                    $sql .= $array1[$j]['alias_columnasparaver'];
                }
            }
            $sql .= "\nFROM tb_cuentaconta cc\n";
            $sql .= "LEFT JOIN (SELECT * FROM tb_cuentaconta) as subcc ON cc.tb_cuentaconta_id = subcc.tb_cuentaconta_idfk\n";
            if (!empty($array1)) {
                for ($k = 0; $k < count($array1); $k++) {
                    $sql .= $array1[$k]['tipo_union'] . " JOIN " . $array1[$k]['tabla_alias'] . " ON " . $array1[$k]['columna_enlace'] . " = " . $array1[$k]['alias_columnaPK'] . "\n";
                }
            }
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i])) {
                    $sql .= ($i > 0) ? " {$array[$i]["conector"]} " : "WHERE ";

                    if(!empty($array[$i]["column_name"])){
                        if (stripos($array[$i]["column_name"], 'fec') !== FALSE) {
                            $sql .= "DATE_FORMAT({$array[$i]["column_name"]}, '%Y-%m-%d')";
                        } else {
                            $sql .= "{$array[$i]["column_name"]}";
                        }

                        if(!empty($array[$i]["datatype"])){
                            $sql .= " = :param$i";
                        } else {
                            $sql .= " ". $array[$i]["param$i"];
                        }
                    } else {
                        $sql .= " ". $array[$i]["param$i"];
                    }
                }
            }
            if (!empty($otros_param["group"])) {
                $sql .= "\nGROUP BY {$otros_param["group"]["column_name"]}";
            }
            if (!empty($otros_param["orden"])) {
                $sql .= "\nORDER BY {$otros_param["orden"]["column_name"]} {$otros_param["orden"]["value"]}";
            }
            if (!empty($otros_param["limit"])) {
                $sql .= "\nLIMIT {$otros_param["limit"]["value"]}";
            }

            $sentencia = $this->dblink->prepare($sql);
            for ($i = 0; $i < count($array); $i++) {
                if (!empty($array[$i]["column_name"]) && !empty($array[$i]["datatype"])) {
                    $_PARAM = strtoupper($array[$i]["datatype"]) == 'INT' ? PDO::PARAM_INT : PDO::PARAM_STR;
                    $sentencia->bindParam(":param$i", $array[$i]["param$i"], $_PARAM);
                }
            }
            $sentencia->execute();

            if ($sentencia->rowCount() > 0) {
                $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
                $retorno["estado"] = 1;
                $retorno["mensaje"] = "exito";
                $retorno["data"] = $resultado;
                $retorno["row_count"] = $sentencia->rowCount();
                $sentencia->closeCursor(); //para libera memoria de la consulta
            } else {
                $retorno["estado"] = 0;
                $retorno["mensaje"] = "No hay registros";
                $retorno["data"] = "";
            }
            return $retorno;
        } catch (Exception $th) {
            throw $th;
        }
    }
    
    public function insertar(){
        $this->dblink->beginTransaction();
        try {
            $opcion_cuenta_padre = (!empty($this->tb_cuentaconta_idfk)) ? "tb_cuentaconta_idfk,\n" : '';
            $param_opcion = (!empty($this->tb_cuentaconta_idfk)) ? ":param_opcional," : '';
            $tipo_cuenta = substr($this->tb_cuentaconta_cod, 0, 1);//el primer digito del codigo de cuenta

            $sql = "INSERT INTO tb_cuentaconta (
                    tb_cuentaconta_cod,
                    tb_cuentaconta_nom,
                    tb_cuentaconta_detalle,
                    $opcion_cuenta_padre
                    tb_tipocuentacont_idfk,
                    tb_tipocuentacont_codfk,
                    tb_cuentaconta_usureg,
                    tb_cuentaconta_usumod,
                    tb_cuentaconta_xac_noconta
                  )
                  VALUES(
                    :param0,
                    :param1,
                    :param2,
                    $param_opcion
                    :param7,
                    :param8,
                    :param4,
                    :param5,
                    :param6
                  );";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":param0", $this->tb_cuentaconta_cod, PDO::PARAM_STR);
            $sentencia->bindParam(":param1", $this->tb_cuentaconta_nom, PDO::PARAM_STR);
            $sentencia->bindParam(":param2", $this->tb_cuentaconta_detalle, PDO::PARAM_STR);
            if (!empty($opcion_cuenta_padre))
                $sentencia->bindParam(':param_opcional', $this->tb_cuentaconta_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(":param4", $this->tb_cuentaconta_usureg, PDO::PARAM_INT);
            $sentencia->bindParam(":param5", $this->tb_cuentaconta_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(":param6", $this->tb_cuentaconta_xac_noconta, PDO::PARAM_INT);
            $sentencia->bindParam(":param7", $tipo_cuenta, PDO::PARAM_INT);
            $sentencia->bindParam(":param8", $tipo_cuenta, PDO::PARAM_INT);

            $resultado['estado'] = $sentencia->execute();
            $resultado['nuevo'] = $this->dblink->lastInsertId();

            if ($resultado['estado'] == 1) {
                $this->dblink->commit();
                $resultado['mensaje'] = 'Registrado correctamente';
            } else {
                $this->dblink->rollBack();
                $resultado['mensaje'] = 'No se pudo registrar';
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }

    public function modificar(){
        $this->dblink->beginTransaction();
        try {
            $opcion_cuenta_padre = (!empty($this->tb_cuentaconta_idfk)) ? "tb_cuentaconta_idfk = :param_opcional,\n" : '';

            $sql = "UPDATE tb_cuentaconta
                  SET
                    tb_cuentaconta_cod = :param0,
                    tb_cuentaconta_nom = :param1,
                    tb_cuentaconta_detalle = :param2,
                    " . $opcion_cuenta_padre . "
                    tb_cuentaconta_usumod = :param3,
                    tb_cuentaconta_xac_noconta = :param4
                  WHERE
                    tb_cuentaconta_id = :param5;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(':param0', $this->tb_cuentaconta_cod, PDO::PARAM_STR);
            $sentencia->bindParam(':param1', $this->tb_cuentaconta_nom, PDO::PARAM_STR);
            $sentencia->bindParam(':param2', $this->tb_cuentaconta_detalle, PDO::PARAM_STR);
            if (!empty($opcion_cuenta_padre))
                $sentencia->bindParam(':param_opcional', $this->tb_cuentaconta_idfk, PDO::PARAM_INT);
            $sentencia->bindParam(':param3', $this->tb_cuentaconta_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(':param4', $this->tb_cuentaconta_xac_noconta, PDO::PARAM_INT);
            $sentencia->bindParam(':param5', $this->tb_cuentaconta_id, PDO::PARAM_INT);

            $resultado = $sentencia->execute();
            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
    
    public function eliminar(){
        $this->dblink->beginTransaction();
        try {
            $sql = "UPDATE tb_cuentaconta SET tb_cuentaconta_xac_conta = 0, tb_cuentaconta_xac_noconta = 0, tb_cuentaconta_usumod = :tb_cuentaconta_usumod WHERE tb_cuentaconta_cod = :tb_cuentaconta_cod;";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":tb_cuentaconta_usumod", $this->tb_cuentaconta_usumod, PDO::PARAM_INT);
            $sentencia->bindParam(":tb_cuentaconta_cod", $this->tb_cuentaconta_cod, PDO::PARAM_STR);
            $resultado = $sentencia->execute(); //retorna 1 si es correcto

            if ($resultado == 1) {
                $this->dblink->commit();
            }
            return $resultado;
        } catch (Exception $th) {
            $this->dblink->rollBack();
            throw $th;
        }
    }
}
//$retorno["mensaje"] = "exito || $sql";return $retorno;exit();
?>