var cod_cuenta_padre = '';

$(document).ready(function () {
	cod_cuenta_padre = $("#txt_cuentaconta_codfk").val();

	//autocompletar de cuenta conta padre
	$("#txt_cuentaconta_codfk, #txt_cuentaconta_nomfk").autocomplete({
		minLength: 1,
		source: function (request, response) {
			$.getJSON(
				VISTA_URL + "plancontable/plancontable_autocomplete.php",
				{ term: request.term },
				response
			);
		},
		select: function (event, ui) {
			cod_cuenta_padre = ui.item.tb_cuentaconta_cod;
			$('#txt_cuentaconta_codfk').val(cod_cuenta_padre);
			$('#hdd_ccpadre_id').val(ui.item.id);
			$('#txt_cuentaconta_nomfk').val(ui.item.tb_cuentaconta_nom);
			$('#tb_cuentaconta_cod').val(cod_cuenta_padre);
			event.preventDefault();
			disabled($('input[id*="fk"]'));
			$('#tb_cuentaconta_cod').focus();
		}
	});

	$('#tb_cuentaconta_cod')
	.blur(function(){
		if ($('#action').val() == 'insertar' || $('#action').val() == 'modificar') {
			verificar_cuenta_existente();
		}
	});

	if ($('#action').val() === 'modificar') {
		disabled($('input[id*="fk"], #tb_cuentaconta_cod, #btn-limpiar-1'));
	}

	$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	$('#tb_cuentaconta_cod')
	.keydown(function(event){
		var codigo = event.which || event.keyCode;
		largo_maximo_hijo = $('#txt_cuentaconta_codfk').val() === '' ? 2 : $('#txt_cuentaconta_codfk').val().length +2;
		
		if ($('#tb_cuentaconta_cod').val() === cod_cuenta_padre) {
			if ((codigo == 8 || codigo == 46)) {
				return false;
			}
		}
		else { // maximo de caracteres
			if ($('#tb_cuentaconta_cod').val().length+1 > largo_maximo_hijo){
				if (codigo !== 8 && codigo !== 46 && codigo !== 9 && !(codigo > 36 && codigo < 41)) // 8: tecla borrar, 9: tecla TAB, 36 al 41 son flechas
					return false;
			}
		}
	})
	.keyup(function(){
		//console.log("up: " + $('#tb_cuentaconta_cod').val());
		if($('#tb_cuentaconta_cod').val() === ''){
			$('#tb_cuentaconta_cod').val(cod_cuenta_padre);
		}
	});

	$('#form_plancontable').validate({
		submitHandler: function () {
			var elementos_disabled = $('#form_plancontable').find('input:disabled, select:disabled').removeAttr('disabled');
			var form_serializado = $('#form_plancontable').serialize();
			elementos_disabled.attr('disabled', 'disabled');
			console.log(form_serializado); //return;
			$.ajax({
				type: "POST",
				url: VISTA_URL + "plancontable/plancontable_controller.php",
				async: true,
				dataType: "json",
				data: form_serializado,
				beforeSend: function () {
					$('#plancontable_mensaje').show(400);
					$('#btn_guardar_plancontable').prop('disabled', true);
				},
				success: function (data) {
					console.log(data);
					if (parseInt(data.estado) > 0) {
						$('#plancontable_mensaje').removeClass('callout-info').addClass('callout-success')
						$('#plancontable_mensaje').html('<h4>' + data.mensaje +'</h4>');
						setTimeout(function () {
							if(parseInt(data.estado) != 2){
								plancontable_tabla();
							}
							$('#modal_registro_plancontable').modal('hide');
						}, 2500
						);
					}
					else {
						$('#plancontable_mensaje').removeClass('callout-info').addClass('callout-warning')
						$('#plancontable_mensaje').html('<h4>Alerta: ' + data.mensaje +'</h4>');
						$('#btn_guardar_plancontable').prop('disabled', false);
					}
				},
				complete: function (data) {
					//console.log(data);
				},
				error: function (data) {
					$('#plancontable_mensaje').removeClass('callout-info').addClass('callout-danger')
					$('#plancontable_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
					$('#btn_guardar_plancontable').prop('disabled', false);
				}
			});
		},
		rules: {
			tb_cuentaconta_cod: {
				minlength: function(){
					codfk = $('#txt_cuentaconta_codfk').val();
					cod_length = (codfk === "") ? 2 : codfk.length +1;
					return cod_length;
				},
				required: true
			},
			txt_cuentaconta_nom: {
				required: true
			}
		},
		messages: {
			tb_cuentaconta_cod: {
				minlength: "Cantidad de caracteres",
				required: "Campo obligatorio"
			},
			txt_cuentaconta_nom: {
				required: "Campo obligatorio"
			}
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			// Add the `help-block` class to the error element
			error.addClass("help-block");
			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.parent("label"));
			} else {
				error.insertAfter(element);
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
		}
	});
});

function limpiar_cuentaconta_padre() {
	$('#hdd_ccpadre_id').val('');
	$('#txt_cuentaconta_codfk').val('');
	$('#txt_cuentaconta_nomfk').val('');
	$('#tb_cuentaconta_cod').val('');
	cod_cuenta_padre = '';

	$('#txt_cuentaconta_codfk').prop('disabled', false);
	$('#txt_cuentaconta_nomfk').prop('disabled', false);
	$('#txt_cuentaconta_codfk').focus();
}

function verificar_cuenta_existente(){
	const action_original = $('#action').val();
	$('#action').val(action_original +'_verificar_existe');
	var elementos_disabled = $('#form_plancontable').find('input:disabled').removeAttr('disabled');
	var form_serializado = $('#form_plancontable').serialize();
	elementos_disabled.attr('disabled', 'disabled');
	$('#action').val(action_original);
	$.ajax({
		type: "POST",
		url: VISTA_URL + "plancontable/plancontable_controller.php",
		async: true,
		dataType: "json",
		data: form_serializado,
		beforeSend: function () {
			$('#plancontable_mensaje').removeClass('callout-warning').addClass('callout-info').html('<h4><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i> Espere un momento por favor...</h4>').show(400);
			$('#btn_guardar_plancontable').prop('disabled', true);
		},
		success: function (data) {
			if (parseInt(data.estado) > 0) {
				// cambiar TOAST por SWAL
				setTimeout(function () {
					//notificacion_warning(data.mensaje, 7000);
					swal_warning('VERIFIQUE', data.mensaje,6000);
					return;
				}, 1000
				);
			}
		},
		complete: function (data) {
			$('#plancontable_mensaje').hide(400);
			$('#btn_guardar_plancontable').prop('disabled', false);
		},
		error: function (data) {
			$('#plancontable_mensaje').removeClass('callout-info').addClass('callout-danger')
			$('#plancontable_mensaje').html('ALERTA DE ERROR: ' + data.responseText);
		}
	});
}