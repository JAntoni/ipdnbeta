<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Recaudoseguro extends Conexion{

    function lista_cuotas_pagadas_seguro($fecha_ini, $fecha_fin){
      try {
        $sql = "SELECT 
            cre.tb_credito_id,
            tb_credito_int,
            tb_cliente_nom,
            tb_cliente_emprs,
            tb_credito_preaco,
            tb_credito_preseguro,
            tb_cuota_cap,
            tb_cuota_cuo,
            tb_cuota_int,
            det.tb_cuotadetalle_id,
            tb_cuotapago_fec,
            tb_cuotapago_mon,
            tb_cuotapago_id
          FROM
          tb_creditogarveh cre 
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cre.tb_cliente_id 
          INNER JOIN tb_cuota cuo ON (cuo.tb_credito_id = cre.tb_credito_id AND cuo.tb_creditotipo_id = 3) 
          INNER JOIN tb_cuotadetalle det ON det.tb_cuota_id = cuo.tb_cuota_id
          INNER JOIN tb_cuotapago pag ON (pag.tb_cuotapago_modid = det.tb_cuotadetalle_id AND pag.tb_modulo_id = 2)
          WHERE tb_credito_preseguro > 0 AND tb_credito_xac = 1 AND tb_cuota_xac = 1 AND tb_cuota_est = 2 AND tb_cuotapago_xac = 1 
          AND tb_cuotapago_fec BETWEEN :fecha_ini AND :fecha_fin
          GROUP BY det.tb_cuotadetalle_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
        $sentencia->bindParam(":fecha_fin", $fecha_fin, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LO SELECCIONADO: opc: ";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>