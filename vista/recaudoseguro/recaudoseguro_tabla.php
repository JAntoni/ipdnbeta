<?php
  require_once('../recaudoseguro/Recaudoseguro.class.php');

  require_once('../funciones/fechas.php');
  require_once('../funciones/funciones.php');

  $oRecaudo = new Recaudoseguro();

  $fecha_ini = fecha_mysql($_POST['fecha_ini']);
  $fecha_fin = fecha_mysql($_POST['fecha_fin']);
  $total_recaudado = 0;

  $result = $oRecaudo->lista_cuotas_pagadas_seguro($fecha_ini, $fecha_fin);
    if($result['estado'] == 1){
      $return['estado'] = 1;
      foreach ($result['data'] as $key => $value) {
        $valor_seguro = formato_numero($value['tb_credito_preseguro'] * $value['tb_cuota_cap'] / 100);
        $interes_credito = $value['tb_credito_int'] - $value['tb_credito_preseguro'];
        $total_recaudado += $valor_seguro;

        $data .= '
          <tr>
            <td>'.$value['tb_credito_id'].'</td>
            <td>'.$value['tb_cliente_nom'].'</td>
            <td>'.$value['tb_cuota_cap'].'</td>
            <td>'.$value['tb_cuota_cuo'].'</td>
            <td>'.$interes_credito.'</td>
            <td>'.$value['tb_cuota_int'].'</td>
            <td>'.mostrar_fecha($value['tb_cuotapago_fec']).'</td>
            <td class="success">'.$value['tb_credito_preseguro'].'</td>
            <td class="info">'.$valor_seguro.'</td>
          </tr>
        ';
      }
      
    }
    else{
      $return['estado'] = 0;
      $data = '
        <tr>
          <td colspan="9">'.$result['mensaje'].'</td>
        </tr>
      ';
    }
  $result = NULL;

  $return['tabla'] = $data;
  $return['tota_recaudo'] = $total_recaudado;
  
  echo json_encode($return);
?>