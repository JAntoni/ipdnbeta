

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="row">
                    <div class="col-lg-6">
                        <label>RUC :</label>
                        <input type="text" class="form-control input-sm mayus" id="txt_empresa_ruc" name="txt_empresa_ruc">
                    </div>
                    <div class="col-lg-6">
                        <label>NOMBRE COMERCIAL :</label>
                        <input type="text" class="form-control input-sm mayus" id="txt_empresa_nomcom" name="txt_empresa_nomcom">
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-lg-6">
                        <label>RAZON SOCIAL :</label>
                        <input type="text" class="form-control input-sm mayus" id="txt_empresa_razsoc" name="txt_empresa_razsoc">
                    </div>
                    <div class="col-lg-6">
                        <label>DIRECCIÓN :</label>
                        <input type="text" class="form-control input-sm mayus" id="txt_empresa_dir" name="txt_empresa_dir">
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-lg-6">
                        <label>DIRECCIÓN 2 :</label>
                        <input type="text" class="form-control input-sm mayus" id="txt_empresa_dir2" name="txt_empresa_dir2">
                    </div>
                    <div class="col-lg-6">
                        <label>TELÉFONO :</label>
                        <input type="text" class="form-control input-sm mayus" id="txt_empresa_tel" name="txt_eempresa_tel">
                    </div>
                </div>
                <p>
                <div class="row">
                    <div class="col-lg-6">
                        <label>EMAIL :</label>
                        <input type="email" class="form-control input-sm mayus" id="txt_empresa_ema" name="txt_empresa_ema">
                    </div>
                    <div class="col-lg-6">
                        <label>REPRESENTANTE :</label>
                        <input type="text" class="form-control input-sm mayus" id="txt_empresa_rep" name="txt_empresa_rep">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
