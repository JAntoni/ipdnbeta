/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
empresa_tabla();
});


function empresa_form(usuario_act, empresa_id){ 
  $.ajax({
        type: "POST",
        url: VISTA_URL+"empresa/empresa_form.php",
        async: true,
        dataType: "html",
        data: ({
            action: usuario_act, // PUEDE SER: L, I, M , E
            empresa_id: empresa_id
        }),
        beforeSend: function() {
            $('#h3_modal_title').text('Cargando Formulario');
            $('#modal_mensaje').modal('show');
        },
        success: function(data){
            $('#modal_mensaje').modal('hide');
            if(data != 'sin_datos'){
              $('#div_modal_empresa_form').html(data);
              $('#modal_registro_empresa').modal('show');

              //desabilitar elementos del form si es L (LEER)
              if(usuario_act == 'L' || usuario_act == 'E')
                form_desabilitar_elementos('form_empresa'); //funcion encontrada en public/js/generales.js
              modal_hidden_bs_modal('modal_registro_empresa', 'limpiar'); //funcion encontrada en public/js/generales.js
            }
            else{
              //llamar al formulario de solicitar permiso
              var modulo = 'empresa';
              var div = 'div_modal_empresa_form';
              permiso_solicitud(usuario_act, empresa_id, modulo, div); //funcion ubicada en public/js/permiso.js
            }
        },
        complete: function(data){

        },
        error: function(data){
            $('#box_modal_mensaje').addClass('box-danger').removeClass('box-info');
            $('#overlay_modal_mensaje').removeClass('overlay').empty();
            $('#body_modal_mensaje').html('ERRROR!:'+ data.responseText);
            console.log(data.responseText);
        }
	});
}

function empresa_tabla(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"empresa/empresa_tabla.php",
    async: true,
    dataType: "html",
    data: 
//            ({})
    $('#empresa_filtro').serialize(),

    beforeSend: function() {
      $('#empresa_mensaje_tbl').show(300);
    },
    success: function(data){
      $('#div_empresa_tabla').html(data);
      $('#empresa_mensaje_tbl').hide(300);
      estilos_datatable();
    },
    complete: function(data){
      
    },
    error: function(data){
      $('#empresa_mensaje_tbl').html('ERROR AL CARGAR DATOS DE EMPRESA : ' + data.responseText);
    }
  });
}
function estilos_datatable(){
  datatable_global = $('#tbl_empresas').DataTable({
    "pageLength": 50,
    "responsive": true,
    "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Ninguna coincidencia para la búsquedad",
          "info": "Mostrado _END_ registros",
          "infoEmpty": "Ningún registro disponible",
          "infoFiltered": "(filtrado de _MAX_ registros totales)",
          "search": "Buscar"
      },
    order: [],
    columnDefs: [
      { targets: [], orderable: false }
    ]
  });

  datatable_texto_filtrar();
}
function datatable_texto_filtrar(){
  $('.dataTables_filter input').attr('id', 'txt_datatable_fil');

  $('#txt_datatable_fil').keyup(function(event) {
    $('#hdd_datatable_fil').val(this.value);
  });

  var text_fil = $('#hdd_datatable_fil').val();
  if(text_fil){
    $('#txt_datatable_fil').val(text_fil);
    datatable_global.search(text_fil).draw();
  }
}
