<?php
    if(defined('APP_URL')){
        require_once(APP_URL.'core/usuario_sesion.php');
    }
    else{
        require_once('../../core/usuario_sesion.php');
    }
  require_once('Empresa.class.php');
  $oEmpresa = new Empresa();

  $result = $oEmpresa->listar_Sucursales();

  $tr = '';
  if($result['estado'] == 1){
    foreach ($result['data'] as $key => $value) {
      $tr .='<tr id="tabla_cabecera_fila">';
        $tr.='
          <td id="tabla_fila">'.$value['tb_empresa_id'].'</td>
          <td id="tabla_fila">'.$value['tb_empresa_ruc'].'</td>
          <td id="tabla_fila">'.$value['tb_empresa_nomcom'].'</td>
          <td id="tabla_fila">'.$value['tb_empresa_razsoc'].'</td>
          <td id="tabla_fila">'.$value['tb_empresa_dir'].'</td>
          <td id="tabla_fila">'.$value['tb_empresa_tel'].'</td>
          <td id="tabla_fila">'.$value['tb_empresa_ema'].'</td>
          <td id="tabla_fila">'.$value['tb_empresa_rep'].'</td>
          <td  id="tabla_fila" align="center">
            <a class="btn btn-info btn-xs" title="Ver" onclick="empresa_form(\'L\','.$value['tb_empresa_id'].')"><i class="fa fa-eye"></i></a>
            <a class="btn btn-warning btn-xs" title="Editar" onclick="empresa_form(\'M\','.$value['tb_empresa_id'].')"><i class="fa fa-edit"></i></a>
            <a class="btn btn-danger btn-xs" title="Eliminar" onclick="empresa_form(\'E\','.$value['tb_empresa_id'].')"><i class="fa fa-trash"></i></a>
            <a class="btn btn-success btn-xs" title="Informe"><i class="fa fa-info"></i>nfo</a>
          </td>
        ';
      $tr.='</tr>';
    }
    $result = null;
  }
  else {
    echo $result['mensaje'];
    $result = null;
    exit();
  }

?>
<table id="tbl_empresas" class="table table-hover">
  <thead>
    <tr id="tabla_cabecera">
      <th id="tabla_cabecera_fila">ID</th>
      <th id="tabla_cabecera_fila">RUC</th>
      <th id="tabla_cabecera_fila">NOMBRE COMERCIAL</th>
      <th id="tabla_cabecera_fila">RAZON SOCIAL</th>
      <th id="tabla_cabecera_fila">DIRECCIÓN</th>
      <th id="tabla_cabecera_fila">TELÉFONO</th>
      <th id="tabla_cabecera_fila">EMAIL</th>
      <th id="tabla_cabecera_fila">REPRESENTANTE</th>
      <th id="tabla_cabecera_fila">OPCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tr;?>
  </tbody>
</table>
