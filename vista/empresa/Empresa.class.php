<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class Empresa extends Conexion{
      
      
      public $tb_empresa_id;
      public $tb_empresa_ruc;
      public $tb_empresa_nomcom;
      public $tb_empresa_razsoc;
      public $tb_empresa_dir;
      public $tb_empresa_dir2;
      public $tb_empresa_tel;
      public $tb_empresa_ema;
      public $tb_empresa_rep;
      public $tb_empresa_fir;
              
      
      
    function insertar($empresa_nom, $empresa_tip, $creditotipo_id, $empresa_fec1, $empresa_fec2, $empresa_can, $usuario_id){
        $this->dblink->beginTransaction();
        try {
          $sql = "INSERT INTO tb_empresa(tb_empresa_nom, tb_empresa_tip, tb_creditotipo_id, tb_empresa_fec1, tb_empresa_fec2, tb_empresa_can, tb_usuario_id)
            VALUES (:empresa_nom, :empresa_tip, :creditotipo_id, :empresa_fec1, :empresa_fec2, :empresa_can, :usuario_id)";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":empresa_nom", $empresa_nom, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_tip", $empresa_tip, PDO::PARAM_INT);
          $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
          $sentencia->bindParam(":empresa_fec1", $empresa_fec1, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_fec2", $empresa_fec2, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_can", $empresa_can, PDO::PARAM_STR);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }
    function insertar2(){
        $this->dblink->beginTransaction();
        try {
          $sql = "INSERT INTO tb_empresa(tb_empresa_nom, tb_empresa_tip, tb_creditotipo_id, tb_empresa_fec1, tb_empresa_fec2, tb_empresa_can, tb_usuario_id)
            VALUES (:empresa_nom, :empresa_tip, :creditotipo_id, :empresa_fec1, :empresa_fec2, :empresa_can, :usuario_id)";

          $sentencia = $this->dblink->prepare($sql);
          $sentencia->bindParam(":empresa_nom", $empresa_nom, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_tip", $empresa_tip, PDO::PARAM_INT);
          $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
          $sentencia->bindParam(":empresa_fec1", $empresa_fec1, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_fec2", $empresa_fec2, PDO::PARAM_STR);
          $sentencia->bindParam(":empresa_can", $empresa_can, PDO::PARAM_STR);
          $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

          $result = $sentencia->execute();

          $this->dblink->commit();

          return $result; //si es correcto el ingreso retorna 1

        } catch (Exception $e) {
          $this->dblink->rollBack();
          throw $e;
        }
    }
    function modificar($empresa_id, $empresa_nom, $empresa_tip, $creditotipo_id, $empresa_fec1, $empresa_fec2, $empresa_can, $usuario_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "UPDATE tb_empresa SET tb_empresa_nom =:empresa_nom, tb_empresa_tip =:empresa_tip, tb_creditotipo_id =:creditotipo_id, tb_empresa_fec1 =:empresa_fec1, tb_empresa_fec2 =:empresa_fec2, tb_empresa_can =:empresa_can, tb_usuario_id =:usuario_id WHERE tb_empresa_id =:empresa_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
        $sentencia->bindParam(":empresa_nom", $empresa_nom, PDO::PARAM_STR);
        $sentencia->bindParam(":empresa_tip", $empresa_tip, PDO::PARAM_INT);
        $sentencia->bindParam(":creditotipo_id", $creditotipo_id, PDO::PARAM_INT);
        $sentencia->bindParam(":empresa_fec1", $empresa_fec1, PDO::PARAM_STR);
        $sentencia->bindParam(":empresa_fec2", $empresa_fec2, PDO::PARAM_STR);
        $sentencia->bindParam(":empresa_can", $empresa_can, PDO::PARAM_STR);
        $sentencia->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function eliminar($empresa_id){
      $this->dblink->beginTransaction();
      try {
        $sql = "DELETE FROM tb_empresa WHERE tb_empresa_id =:empresa_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);

        $result = $sentencia->execute();

        $this->dblink->commit();

        return $result; //si es correcto retorna 1

      } catch (Exception $e) {
        $this->dblink->rollBack();
        throw $e;
      }
    }
    function mostrarUno($empresa_id){
      try {
        $sql = "SELECT * FROM tb_empresa WHERE tb_empresa_id =:empresa_id";

        $sentencia = $this->dblink->prepare($sql);
        $sentencia->bindParam(":empresa_id", $empresa_id, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetch();
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
    
    function listar_empresas($empresa_mes, $empresa_anio){
      try {
        $empresa_mes = intval($empresa_mes);
        $empresa_anio = intval($empresa_anio);

        $where_mes = '';
        $where_anio = '';
        if($empresa_mes > 0)
          $where_mes = ' AND MONTH(tb_empresa_fec1) =:empresa_mes';
        if($empresa_anio > 0)
          $where_anio = ' AND YEAR(tb_empresa_fec1) =:empresa_anio';

        $sql = "SELECT * FROM tb_empresa me left join tb_creditotipo cretip on cretip.tb_creditotipo_id = me.tb_creditotipo_id left join tb_usuario usu on usu.tb_usuario_id = me.tb_usuario_id WHERE 1=1".$where_mes.''.$where_anio;

        $sentencia = $this->dblink->prepare($sql);
        if($empresa_mes > 0)
          $sentencia->bindParam(":empresa_mes", $empresa_mes, PDO::PARAM_INT);
        if($empresa_anio > 0)
          $sentencia->bindParam(":empresa_anio", $empresa_anio, PDO::PARAM_INT);
        
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
    
    function listar_Sucursales(){
      try {
        

        $sql = "SELECT * FROM tb_empresa";

        $sentencia = $this->dblink->prepare($sql);
//          $sentencia->bindParam(":empresa_mes", $empresa_mes, PDO::PARAM_INT);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "No hay tipos de crédito registrados";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }

    }
  }

?>
