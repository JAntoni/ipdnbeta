<?php
  if(defined('APP_URL'))
    require_once(APP_URL.'datos/conexion.php');
  else
    require_once('../../datos/conexion.php');
  class ReporteInfocorp extends Conexion{

    function temp_cobranza_infocorp_lista(){
      try {
        $sql = "SELECT 1 AS lista, cob.*, cli.tb_ubigeo_cod FROM temp_cobranza cob
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cob.cliente_id
          
          UNION ALL
          
          SELECT 2 AS lista, cob.*, cli.tb_ubigeo_cod 
          FROM (
              SELECT *
              FROM temp_cobranza
              WHERE representante_dni != ''
          ) AS cob
          INNER JOIN tb_cliente cli ON cli.tb_cliente_id = cob.cliente_id;";

        $sentencia = $this->dblink->prepare($sql);
        //$sentencia->bindParam(":fecha_filtro2", $fecha_filtro2, PDO::PARAM_STR);
        $sentencia->execute();

        if ($sentencia->rowCount() > 0) {
          $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
          $retorno["estado"] = 1;
          $retorno["mensaje"] = "exito";
          $retorno["data"] = $resultado;
          $sentencia->closeCursor(); //para libera memoria de la consulta
        }
        else{
          $retorno["estado"] = 0;
          $retorno["mensaje"] = "NO HAY HISTORIAL DE PAGOS PARA LO SELECCIONADO: opc: ";
          $retorno["data"] = "";
        }

        return $retorno;
      } catch (Exception $e) {
        throw $e;
      }
    }
  }

?>
