var datatable_global;


function temp_cobranza_infocorp_lista(){
  $.ajax({
    type: "POST",
    url: VISTA_URL+"reporteinfocorp/reporteinfocorp_tabla.php", //EJECUTAMOS LA MISMA FUNCION QUE SE ENCUENTRA EN REPORTECREDITO, PARA USAR UN SOLO ARCHIVO
    async: true,
    dataType: "JSON",
    data: {},
    beforeSend: function() {
      $('#reporteinfocorp_mensaje_tbl').show(300);
      $('#span_cargando').text('Cargando Tabla GARVEH...');
      $('#lista_infocorp').html('');
    },
    success: function(data){
      if(parseInt(data.estado) == 1)
        datatable_global.destroy();
      
      $('#reporteinfocorp_mensaje_tbl').hide(300);
      
      $('#lista_infocorp').html(data.tabla);

      if(parseInt(data.estado) == 1){
        
        estilos_datatable();
        //mostrar_sumas_totales(data.inputs);
      }

      // console.log(data.extra + ' // ' + data.inputs.alerta_deudadiaria);
      // console.log(data.inputs)
    },
    complete: function(data){
      //console.log(data)
    },
    error: function(data){
      console.log(data);
      $('#reporteinfocorp_mensaje_tbl').html('ERROR AL CARGAR DATOS DE CRÉDITO: ' + data.responseText);
    }
  });
}

function estilos_datatable(){
  datatable_global = $('#tbl_infocorp').DataTable({
    "pageLength": 50,
    "responsive": true,
    "bScrollCollapse": true,
    "bPaginate": false,
    "bJQueryUI": true,
    "scrollX": true,
    scrollY: 700,
    //"scrollY": "80vh",
    //dom: '<"row"<"col-md-6"l><"col-md-6"f>><"row"<"col-md-6"B><"col-md-6"p>><"row"<"col-md-12"t>><"row"<"col-md-12"i>>',
    dom: '<"row"<"col-md-6"l>><"row"<"col-md-6"B><"col-md-6"f>><"row"<"col-md-12"t>><"row"<"col-md-6"i><"col-md-6"p>>',
    buttons: [
      { extend: 'copy', text: '<i class="fa fa-copy"></i> Copiar Todo', className: 'btn btn-primary' },
      { extend: 'csv', text: '<i class="fa fa-file-text"></i> Exportar CSV', className: 'btn btn-warning' },
      { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Exportar Excel', className: 'btn btn-success' },
      { extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> Exportar PDF', className: 'btn btn-danger' },
      { extend: 'print', text: '<i class="fa fa-print"></i> Imprimir', className: 'btn btn-default' }
    ],
    "language": {
      "lengthMenu": "Mostrar _MENU_ registros por página",
      "zeroRecords": "Ninguna coincidencia para la búsquedad",
      "info": "Mostrado _END_ registros",
      "infoEmpty": "Ningún registro disponible",
      "infoFiltered": "(filtrado de _MAX_ registros totales)",
      "search": "Buscar"
    },
    order: [],
    columnDefs: [
      { targets: "no-sort", orderable: false}
    ]
    /*drawCallback: function () {
      var sum_desembolsado = $('#tbl_infocorp').DataTable().column(13).data().sum(); //* columna 13 capital desembolsado
      var sum_capital_rest_sol = $('#tbl_infocorp').DataTable().column(15).data().sum(); //* columna 13 capital restante en soles

      $('#total').html(sum);
    }*/
  });
}

$(document).ready(function() {
  console.log('Mejorado corre 111 ');

  $('#datetimepicker1, #datetimepicker2').datepicker({
    language: 'es',
    autoclose: true,
    format: "dd-mm-yyyy"
    //startDate: "-0d"
    //endDate : new Date()
  });
  
  $("#datetimepicker1").on("change", function (e) {
    var startVal = $('#txt_deudadiaria_fec1').val();
    $('#datetimepicker2').data('datepicker').setStartDate(startVal);

  });
  $("#datetimepicker2").on("change", function (e) {
    var endVal = $('#txt_deudadiaria_fec2').val();
    $('#datetimepicker1').data('datepicker').setEndDate(endVal);

  });

  estilos_datatable();
  
  temp_cobranza_infocorp_lista()

});